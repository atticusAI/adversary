﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace Adversary.Web.Pages
{
    public class MasterBase : System.Web.UI.MasterPage
    {
        private string _appPath = null;

        public string AppPath
        {
            get
            {
                if (_appPath == null)
                {
                    _appPath = this.Request.ApplicationPath.TrimEnd('/');
                }
                return _appPath;
            }
        }

        public void AddIncludes(HtmlContainerControl head)
        {
            head.Controls.AddAt(0, this.GetIncludeJavascript()); // want this first no matter what
            head.Controls.AddAt(head.Controls.Count, this.GetThemeOverrideCSS()); // want this last no matter what
        }

        public void HighlightCurrentTab(HtmlTableCell td)
        {
            foreach (Control ctl in td.Controls)
            {
                if ((ctl is LinkButton))
                {
                    LinkButton btn = (LinkButton)ctl;
                    if (btn.PostBackUrl.ToLowerInvariant().EndsWith(this.Request.FilePath.ToLowerInvariant()))
                    {
                        btn.CssClass += " activetab";
                        break;
                    }
                }
            }
        }

        private Literal GetIncludeJavascript()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(string.Format("<script src=\"{0}/Scripts/jquery-1.7.1.min.js\" type=\"text/javascript\"></script>\r\n", this.AppPath));
            sb.Append(string.Format("<script src=\"{0}/Scripts/jquery.validate.min.js\" type=\"text/javascript\"></script>\r\n", this.AppPath));
            sb.Append(string.Format("<script src=\"{0}/Scripts/additional-methods.min.js\" type=\"text/javascript\"></script>\r\n", this.AppPath));
            sb.Append(string.Format("<script src=\"{0}/Scripts/HH.js\" type=\"text/javascript\"></script>\r\n", this.AppPath));

            return new Literal()
            {
                 Text = sb.ToString()
            };
        }

        private Literal GetThemeOverrideCSS()
        {
            StringBuilder sb = new StringBuilder();

            if (!string.IsNullOrWhiteSpace(this.Page.Theme))
            {
                sb.Append(string.Format("<!--[if IE 6]><link href=\"{0}/App_Themes/{1}/{2}.cssie6\" rel=\"stylesheet\" type=\"text/css\" /><![endif]-->\r\n",
                    this.AppPath, this.Page.Theme, this.Page.Theme));
                sb.Append(string.Format("<!--[if IE 7]><link href=\"{0}/App_Themes/{1}/{2}.cssie7\" rel=\"stylesheet\" type=\"text/css\" /><![endif]-->\r\n",
                    this.AppPath, this.Page.Theme, this.Page.Theme));
                sb.Append(string.Format("<!--[if IE 8]><link href=\"{0}/App_Themes/{1}/{2}.cssie8\" rel=\"stylesheet\" type=\"text/css\" /><![endif]-->\r\n",
                    this.AppPath, this.Page.Theme, this.Page.Theme));
            }

            return new Literal()
            {
                Text = sb.ToString()
            };
        }
    }
}