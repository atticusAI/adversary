﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Adversary.DataLayer;
using System.Text;
using Adversary.Screening;
using System.Net.Mail;
using System.Configuration;
using Adversary.DataLayer.HRDataService;
using Adversary.Web.Admin;
using System.Web.UI.WebControls;
using Adversary.Utils;

namespace Adversary.Web.Pages
{
    public class AdminBasePage : Adversary.Web.Pages.TrackBasePage
    {
        private Adversary.DataLayer.Approval _approvalDataLayer = null;

        protected Adversary.DataLayer.Approval ApprovalDataLayer
        {
            get
            {
                if (_approvalDataLayer == null)
                {
                    _approvalDataLayer = new Adversary.DataLayer.Approval();
                }
                return _approvalDataLayer;
            }
        }

        private Memo _thisMemo = null;

        public Memo ThisMemo
        {
            get
            {
                return _thisMemo;
            }
            set
            {
                _thisMemo = value;
            }
        }

        private ScreeningEmail _emailer = null;

        public ScreeningEmail Emailer
        {
            get
            {
                if (_emailer == null)
                {
                    _emailer = new ScreeningEmail();
                }
                return _emailer;
            }
        }

        public int ScrMemID
        {
            get
            {
                int scrMemID = -1;
                if (!string.IsNullOrWhiteSpace(this.Request.QueryString["ScrMemID"])){
                    int.TryParse(this.Request.QueryString["ScrMemID"], out scrMemID);
                }
                return scrMemID;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            LoadMemo(this.ScrMemID);
        }

        /// <summary>
        /// handle saving data that does not have an explicit "save" 
        /// will work with both the "back" button and navigation within the admin/approval pages
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (this.PreviousPage != null)
            {
                if (this.PreviousPage is Adversary.Web.Admin.Approval)
                {
                    Adversary.Web.Admin.Approval appPage = (Adversary.Web.Admin.Approval)this.PreviousPage;
                    this.UpdateTracking(appPage);
                }
            }
        }

        public void UpdateTracking(Adversary.Web.Admin.Approval appPage)
        {
            Tracking tracking = this.ApprovalDataLayer.GetTrackingRecord(appPage.TrackingID);
            if (tracking != null)
            {
                this.UpdateTracking(tracking, appPage);
            }
        }

        public void UpdateTracking(Tracking tracking, Adversary.Web.Admin.Approval appPage)
        {
            // general
            tracking.Notes = appPage.Notes;
            // tracking.RetainerAcknowledged = appPage.APRetainerAcknowledged || appPage.PGMRetainerAcknowledged;
            
            // ap 
            tracking.ARApprovalReceived = appPage.ARApproval;
            tracking.APARSignature = appPage.APARSignature;
            tracking.APExceptionSignature = appPage.APExceptionSignature;
            tracking.APNotes = appPage.APNotes;

            // pgm
            tracking.PGMARSignature = appPage.PGMARSignature;
            tracking.PGMExceptionSignature = appPage.PGMExceptionSignature;
            tracking.PGMNotes = appPage.PGMNotes;

            // adversary
            tracking.Opened = appPage.Opened;
            tracking.Conflicts = appPage.Conflicts;
            tracking.FeeSplits = appPage.FeeSplits;
            tracking.FinalCheck = appPage.FinalCheck;

            this.ApprovalDataLayer.UpdateTracking(tracking);
            this.LoadMemo(tracking.ScrMemID ?? -1);
        }

        public void LoadMemo(int scrMemID)
        {
            ThisMemo = AdversaryDataLayer.GetMemo(scrMemID);
        }

        public override void Dispose()
        {
            if (_approvalDataLayer != null)
            {
                _approvalDataLayer.Dispose();
                _approvalDataLayer = null;
            }
            base.Dispose();
            GC.SuppressFinalize(this);
        }

        private Adversary.Web.Controls.ErrorSummary _errorSummary = null;

        public Adversary.Web.Controls.ErrorSummary ErrorSummary
        {
            get
            {
                if (this._errorSummary == null && ThisMasterPage != null)
                {
                    this._errorSummary = (Adversary.Web.Controls.ErrorSummary)ThisMasterPage.FindControl("master_ErrorSummary");
                }
                return this._errorSummary;
            }
        }

        private AdminMaster _thisMasterPage = null;

        public AdminMaster ThisMasterPage
        {
            get
            {
                if (_thisMasterPage == null && this.Master is AdminMaster)
                {
                    _thisMasterPage = this.Master as AdminMaster;
                }
                return _thisMasterPage;
            }
        }

        #region Email Utilities
        /// <summary>
        /// TODO: FS 4 this code is so ugly you'll wana punch your mama
        /// </summary>
        protected void HandleStatusNotification(Memo memo)
        {
            try
            {
                bool error = false;
                string feedback = string.Empty;
                ScreeningEmail emailer = new ScreeningEmail();

                try
                {
                    emailer.SendStatusEmail(memo, this.BaseURL);
                    feedback += "Email notification sent to originating party.<br/>";
                }
                catch (Exception x)
                {
                    feedback += "<span style='color: red;'> * An error occurred while sending the status notification to the originating party, one or more of the intended recipients may not have been notified.</span><br />" +
                        "Error Message: " + x.Message + "<br />"; // +
                        // "Stack Trace: " + x.StackTrace.Replace("\r\n", "<br />");
                    error = true;
                }

                if (memo.Trackings[0].StatusTypeID == TrackingStatusType.AdversaryGroupProcessing ||
                    memo.Trackings[0].StatusTypeID == TrackingStatusType.Completed)
                {
                    MailAddressCollection to = new MailAddressCollection();
                    StringBuilder msg = new StringBuilder();
                    msg.AppendLine("A new matter screening issue requires your attention:");
                    msg.AppendLine();
                    msg.AppendLine();

                    if (!string.IsNullOrEmpty(memo.RespAttID))
                    {
                        try
                        {
                            string billerEmail = null;
                            Employee biller = this.AdversaryDataLayer.GetBiller(memo.RespAttID);
                            if (biller != null)
                                billerEmail = biller.CompanyEmailAddress;
                            else
                            {
                                feedback += "<br/><br/><span style='color: red;'> * A biller was not notified, no biller could be found for billing attorney " + memo.RespAttID + "</span>";
                                error = true;
                            }

                            if (!string.IsNullOrEmpty(billerEmail))
                            {
                                if (WebUtils.TestMode)
                                {
                                    to.Add(new MailAddress(billerEmail, "Biller"));
                                }
                                else
                                {
                                    to.Add(billerEmail);
                                }
                                msg.AppendLine("* Biller Action Required");
                                msg.AppendLine();
                                msg.AppendLine();
                                feedback += "<br/><br/> - Biller " + biller.DisplayName + "(" + billerEmail + ") notified.";
                            }
                        }
                        catch (Exception x)
                        {
                            feedback += "<br/><br/><span style='color: red;'> * An error occurred while attempting to lookup biller information, the biller was not notified.</span><br />" +
                                "Error Message: " + x.Message + "<br />"; // +
                                // "Stack Trace: " + x.StackTrace.Replace("\r\n", "<br />");
                            error = true;
                        }
                    }
                    else if (memo.ScrMemType != 6)
                    {
                        feedback += "<br/><br/><span style='color: red;'> * An error occurred while attempting to lookup biller information, no billing attorney was specified.</span><br />" +
                            "Error Message: Unexpected Screening Memo Type";
                        error = true;
                    }

                    bool nDrive = memo.NDrive.HasValue && memo.NDrive.Value;
                    bool docketing = DocketingInfoRequired(memo.PracCode);
                    if (nDrive || docketing)
                    {
                        string pracSupportEmails = ConfigurationManager.AppSettings["EmailPracticeSupport"];
                        string[] list = pracSupportEmails.Split(new char[] { ';' });
                        foreach (string email in list)
                        {
                            if (WebUtils.TestMode)
                            {
                                to.Add(new MailAddress(email, "Practice Support"));
                            }
                            else
                            {
                                to.Add(email);
                            }
                        }

                        if (nDrive)
                        {
                            msg.AppendLine("* Please create an N Drive folder for this matter");
                            msg.AppendLine();

                            try
                            {
                                if (!memo.NDriveType.HasValue || memo.NDriveType == 0)
                                    msg.AppendLine("NO N DRIVE TYPE SPECIFIED FOR THIS MATTER, PLEASE CONTACT THE MEMO ORIGINATOR FOR INSTRUCTIONS");
                                else
                                {
                                    msg.AppendLine("Folder Type:\t\t\t\t" + this.AdversaryDataLayer.GetNDriveTypeDescription(memo.NDriveType.Value));
                                    msg.AppendLine();

                                    if (memo.NDriveType == 4 || memo.NDriveType == 5) //per PS request on 7/28/2011, added "Patent Record" NDrive option at index 5 and enabled NDrive access list if option is selected 
                                    {
                                        if (memo.NDriveAccesses.Count == 0)
                                            msg.AppendLine("NO ACCESS LIST SPECIFIED FOR MATTER FOLDER, PLEASE CONTACT THE MEMO ORIGINATOR FOR INSTRUCTIONS");
                                        else
                                        {
                                            msg.AppendLine("Folder access required for:");
                                            msg.AppendLine();
                                            foreach (NDriveAccess ndrive in memo.NDriveAccesses)
                                                msg.AppendLine(" - " + AdversaryDataLayer.GetEmployee(ndrive.PersonnelID).EmployeeName + "(" + ndrive.PersonnelID + ")");
                                        }
                                    }
                                }
                            }
                            catch(Exception x)
                            {
                                msg.AppendLine("AN ERROR OCCURRED WHILE ATTEMPTING TO RETRIEVE N DRIVE INFORMATION, PLEASE CONTACT THE MEMO ORIGINATOR FOR INSTRUCTIONS");
                            }

                            msg.AppendLine();
                        }

                        if (docketing)
                        {
                            if (nDrive)
                                msg.AppendLine();

                            msg.AppendLine("* Docketing information supplied for this matter");
                            msg.AppendLine();

                            if (string.IsNullOrEmpty(memo.DocketingAtty))
                                msg.AppendLine("NO DOCKETING ATTORNEY SPECIFIED FOR THIS MATTER, PLEASE CONTACT THE MEMO ORIGINATOR FOR INSTRUCTIONS");
                            else
                                msg.AppendLine("Docketing Attorney:\t\t\t" + AdversaryDataLayer.GetEmployee(memo.DocketingAtty).EmployeeName);
                            msg.AppendLine();

                            if (string.IsNullOrEmpty(memo.DocketingStaff))
                                msg.AppendLine("NO DOCKETING STAFF SPECIFIED FOR THIS MATTER, PLEASE CONTACT THE MEMO ORIGINATOR FOR INSTRUCTIONS");
                            else
                                msg.AppendLine("Docketing Staff Member:\t\t\t" + AdversaryDataLayer.GetEmployee(memo.DocketingStaff).EmployeeName);
                            msg.AppendLine();

                            if (memo.DocketingTeams.Count == 0)
                            {
                                msg.AppendLine("No docketing team was specified");
                                msg.AppendLine();
                            }
                            else
                            {
                                msg.AppendLine("Docketing Team:");
                                msg.AppendLine();
                                foreach (DocketingTeam dt in memo.DocketingTeams)
                                    msg.AppendLine(" - " + AdversaryDataLayer.GetEmployee(dt.PersonnelID).EmployeeName + "(" + dt.PersonnelID + ")");
                                msg.AppendLine();
                            }

                            msg.AppendLine();
                        }
                        feedback += "<br/><br/> - Practice Support Notified";
                    }

                    if (to.Count > 0)
                    {
                        msg.AppendLine();
                        msg.AppendLine(emailer.BuildMemoStatusEmail(memo));
                        msg.AppendLine();
                        msg.AppendLine(emailer.BuildMemoApproverEmail(memo));
                        msg.AppendLine();
                        msg.AppendLine("Click the link below to view the print version of this memo:");
                        msg.AppendLine();
                        msg.AppendLine(BaseURL + "Screening/PrintView.aspx?ScrMemID=" + memo.ScrMemID);
                        msg.AppendLine();
                        msg.AppendLine();
                        msg.AppendLine("Click the link below to view the summary of this memo:");
                        msg.AppendLine();
                        msg.AppendLine(BaseURL + "Screening/Summary.aspx?ScrMemID=" + memo.ScrMemID);

                        string subject = "Screening Memo #" + memo.ScrMemID + " - ACTION REQUIRED";

                        MailAddress from = new MailAddress(ConfigurationManager.AppSettings["EmailSendAddress"],
                            ConfigurationManager.AppSettings["EmailSendDisplayName"]);
                        emailer.SendEmail(to, from, subject, msg.ToString(), false);
                    }

                    if (memo.ClientMatterNumbers.Count > 0)
                    {
                        memo.Trackings[0].Locked = true;
                        memo.Trackings[0].CMNumbersSentDate = DateTime.Now;
                        feedback += "<br/><br/> - Client Matter Numbers Sent";
                    }
                }
                if (error)
                    this.ErrorMessage(feedback);
                else
                    this.InfoMessage(feedback);
            }
            catch (Exception x)
            {
                this.ErrorMessage("An error occurred while attempting to send the notification, one or more of the intended recipients may not have been notified.<br />" +
                    "Error Message: " + x.Message + "<br />"); // +
                    // "Stack Trace: " + x.StackTrace.Replace("\r\n", "<br />"));
            }
        }


        /// <summary>
        /// Global error handling method, displays error message and emails exception if Debug flag is false
        /// </summary>
        /// <param name="ex"></param>
        public void HandleError(Exception ex)
        {
            string error = ex.HH_ErrorMessage();

            try
            {
                if (Page != null)
                    error += "<br/><br/>File Path: " + Page.Request.CurrentExecutionFilePath + "<br/><br/>";
                try
                {
                    if (ThisMemo == null)
                        error += "<br/><br/>No Memo Loaded";
                    else
                    {
                        try
                        {
                            error += "<br/><br/>" + Emailer.BuildMemoStatusEmail(ThisMemo);
                        }
                        catch (Exception emailEx)
                        {
                            error += "<br/><br/>An error occurred while attempting to build the memo status portion of this error report:<br/><br/>" + emailEx.HH_ErrorMessage();
                            error += "<br/><br/>Screening Memo #:" + ThisMemo.ScrMemID + "<br/>";
                        }
                    }
                }
                catch { }

                Emailer.SendErrorEmail(error.Replace("<br/>", "\r\n"));
            }
            catch { }
        }
        #endregion

        #region Message Utilities
        public void ClearMessage()
        {
            if (this.Master is Admin.AdminMaster)
            {
                ((Admin.AdminMaster)this.Master).ClearMessage();
            }
        }

        public void ErrorMessage(string message)
        {
            if (this.Master is Admin.AdminMaster)
            {
                ((Admin.AdminMaster)this.Master).ErrorMessage(message);
            }
        }

        public void InfoMessage(string message)
        {
            if (this.Master is Admin.AdminMaster)
            {
                ((Admin.AdminMaster)this.Master).InfoMessage(message);
            }
        }

        public void WarnMessage(string message)
        {
            if (this.Master is Admin.AdminMaster)
            {
                ((Admin.AdminMaster)this.Master).WarnMessage(message);
            }
        }
        #endregion 

        #region Memo Data Wiring
        public string GetMemoTypeDescription(Memo memo)
        {
            return AdversaryDataLayer.GetMemoTypeDescription(memo);
        }

        public string GetPracticeCodeName(Memo memo)
        {
            string pracCodeName = "";
            Adversary.DataLayer.AdversaryDataService.PracticeCode practiceCode = AdversaryDataLayer.GetPracticeCode(memo.PracCode);
            if (practiceCode != null)
            {
                pracCodeName = practiceCode.matt_cat_desc + "(" + memo.PracCode + ")";
            }
            return pracCodeName;
        }

        public string GetOfficeName(Memo memo)
        {
            string officeName = "";
            Adversary.DataLayer.HRDataService.FirmOffice office = AdversaryDataLayer.GetMemoOffice(memo);            
            if (office != null)
            {
                officeName = office.City;
            }
            return officeName;
        }

        public string GetEnteringAttorneyName(Memo memo)
        {
            string attorneyName = "";
            Adversary.DataLayer.FinancialDataService.FinancialDataEmployee emp = AdversaryDataLayer.GetEmployee(memo.AttEntering);
            if (emp != null)
            {
                attorneyName = emp.EmployeeName + " (" + memo.AttEntering + ")";
            }
            else
            {
                attorneyName = "(" + memo.AttEntering + ")";
            }

            return attorneyName;
        }

        public string GetPersonnelName(Memo memo)
        {
            string personnelName = "";
            Adversary.DataLayer.FinancialDataService.FinancialDataEmployee emp = AdversaryDataLayer.GetEmployee(memo.PersonnelID);
            if (emp != null)
            {
                personnelName = emp.EmployeeName + " (" + memo.PersonnelID + ")";
            }
            else
            {
                personnelName = "(" + memo.PersonnelID + ")";
            }

            return personnelName;
        }

        public string GetClientDisplayName()
        {
            return GetClientDisplayName(ThisMemo);
        }

        public string GetClientDisplayName(Memo memo)
        {
            string name = "";
            if (memo.ScrMemType == 2 ||
                ((memo.ScrMemType == 3 || memo.ScrMemType == 6) && !string.IsNullOrWhiteSpace(memo.ClientNumber)))
            {
                name = GetClientName(memo.ClientNumber);
            }
            else
            {
                name = ((memo.Company ?? false))
                    ? memo.NewClients[0].CName
                    : memo.NewClients[0].FName + " " + memo.NewClients[0].MName + " " + memo.NewClients[0].LName;
            }
            return name;
        }

        public string GetClientName(string clientCode)
        {
            string name = "";
            try
            {
                name = string.Format("{0} ({1})",
                    this.ApprovalDataLayer.GetClientName(clientCode),
                    clientCode);
            }
            catch
            {
                name = string.Format("<span class=\"error\">Name lookup failed. ({0})</span>", clientCode);
            }
            return name;
        }

        #endregion 
    }
}