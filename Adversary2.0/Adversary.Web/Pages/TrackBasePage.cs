﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI.WebControls;
using Adversary.DataLayer;
using Adversary.DataLayer.FinancialDataService;
using AjaxControlToolkit;
using Adversary.Utils;
using System.Text;
using System.Configuration;

namespace Adversary.Web.Pages
{
    public class CurrentUser
    {
        AdversaryData AdversaryDataLayer;

        public CurrentUser(string userLogin)
        {
            _CurrentUserLogin = userLogin;
            AdversaryDataLayer = new AdversaryData();
        }
        
        private readonly string _CurrentUserLogin;
        
        public string CurrentUserLogin
        {
            get { 
                return _CurrentUserLogin; 
            }
        }
        
        private string _CurrentUserPayrollID;

        public string CurrentUserPayrollID
        {
            get
            {
                if (_CurrentUserPayrollID == null)
                {
                    FinancialDataEmployee emp = AdversaryDataLayer.GetEmployee(CurrentUserLogin);
                    _CurrentUserPayrollID = (emp == null) ? null : emp.EmployeeCode;
                }
                return _CurrentUserPayrollID;
            }
        }

        private List<DataLayer.VW_UserPageNavigation> _userPageNavigation = null;

        public List<DataLayer.VW_UserPageNavigation> UserPageNavigation
        {
            get
            {
                if (this._userPageNavigation == null)
                {
                    if (this.AdminLogin != null)
                    {
                        this._userPageNavigation = this.AdversaryDataLayer.GetUserPageNavigation(this.AdminLogin.UserID);
                    }
                    else
                    {
                        this._userPageNavigation = this.AdversaryDataLayer.GetUserPageNavigation(-1);
                    }
                }
                return this._userPageNavigation;
            }
        }

        public bool HasAccessToPage(string pageUrl)
        {
            return this.UserPageNavigation == null || this.UserPageNavigation.Count == 0
                ? false
                : this.UserPageNavigation.Any(upn => upn.PageUrl.ToLowerInvariant() == pageUrl.ToLowerInvariant());
        }
        
        private AdminLogin _adminLogin = null;

        public AdminLogin AdminLogin
        {
            get
            {
                if (this._adminLogin == null)
                {
                    this._adminLogin = this.AdversaryDataLayer.GetAdminLogin(this.CurrentUserLogin);
                }
                return this._adminLogin;
            }
        }

        public bool HasAdminLogin
        {
            get
            {
                return !(this.AdminLogin == null);
            }
        }

        public bool IsAdversary
        {
            get
            {
                return this.HasAdminLogin ? (this.AdminLogin.IsAdversary ?? false) : false;
            }
        }

        public bool IsAdversaryAdmin
        {
            get
            {
                return this.HasAdminLogin ? (this.AdminLogin.IsAdversaryAdmin ?? false) : false;
            }
        }

        public bool IsAP
        {
            get
            {
                return this.HasAdminLogin ? (this.AdminLogin.IsAP ?? false) : false;
            }
        }

        public bool IsPrimaryAP
        {
            get
            {
                return this.HasAdminLogin ? (this.AdminLogin.IsPrimaryAP ?? false) : false;
            }
        }

        public bool IsPGM
        {
            get
            {
                return this.HasAdminLogin ? (this.AdminLogin.IsPGM ?? false) : false;
            }
        }

        public bool IsFinancialAdmin
        {
            get
            {
                return this.HasAdminLogin ? (this.AdminLogin.IsFinancialAdmin ?? false) : false;
            }
        }

        public string LoginEmailAddress
        {
            get
            {
                return this.HasAdminLogin ? this.AdminLogin.Email : "";
            }
        }

    }

    public class TrackBasePage : System.Web.UI.Page
    {
        #region web/script methods

        [WebMethod]
        [ScriptMethod]
        public static string[] TimekeeperService(string prefixText, int count)
        {
            List<string> foundTimekeepers = new List<string>();
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            List<FinancialDataEmployee> emps = new List<FinancialDataEmployee>();
            string[] employees = new string[1];

            using (AdversaryData data = new AdversaryData())
            {
                emps = data.SearchEmployees(prefixText).OrderBy(M => M.EmployeeName).Take(count).ToList();
            }

            //int i = 0;
            int returnSetCount = (emps.Count > count ? count : emps.Count);
            if (emps.Count > 0)
            {
                foreach (FinancialDataEmployee emp in emps)
                {
                    foundTimekeepers.Add(AutoCompleteExtender.CreateAutoCompleteItem(
                        emp.EmployeeName + " (" + emp.EmployeeCode + ")", emp.EmployeeCode));
                }
                return foundTimekeepers.ToArray();
            }
            else
            {
                employees = new string[] { "<<no suggestions found>>" };
                return employees;
            }
        }

        [WebMethod]
        [ScriptMethod]
        public static string[] ClientService(string prefixText, int count)
        {

            List<string> foundClients = new List<string>();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            List<Client> clts = new List<Client>();

            string[] clients = new string[1];

            using (AdversaryData data = new AdversaryData())
            {
                clts = data.SearchClients(prefixText).Take(count).ToList();
            }

            if (clts.Count > 0)
            {
                if (prefixText.Substring(0, 1).IsNumeric())
                {
                    clts.OrderBy(M => M.ClientNumber);
                }
                else
                {
                    clts.OrderBy(M => M.ClientName);
                }
                foreach (Client clt in clts)
                {
                    foundClients.Add(AutoCompleteExtender.CreateAutoCompleteItem(
                        clt.ClientName + "(" + clt.ClientCode + ")", clt.ClientCode));
                }
            }
            else
            {
                foundClients.Add(AutoCompleteExtender.CreateAutoCompleteItem("<<no suggestions found>>", ""));
            }

            return foundClients.ToArray();
        }

        [WebMethod]
        [ScriptMethod]
        public static bool DocketingInfoRequired(string pracCode)
        {
            bool required = true;

            if (string.IsNullOrWhiteSpace(pracCode))
            {
                required = false;
            }
            else if (pracCode.Contains("E") || pracCode.Contains("T"))
            {
                required = false;
            }
            else if (pracCode == "501L" || pracCode == "502L")
            {
                required = false;
            }

            return required;
        }

        [WebMethod]
        [ScriptMethod]
        public static string[] MatterNameAutoCompleteService(string prefixText, int count)
        {
            List<string> foundMatterNames = new List<string>();
            using (ScreeningSearch dl = new ScreeningSearch())
            {
                List<Memo> memos = dl.SearchScreeningMemosByMatterStartsWith(prefixText).Take(count).ToList();
                if (memos.Count > 0)
                {
                    foundMatterNames.AddRange(memos.Select(m => m.MatterName));
                    foundMatterNames.Sort();
                }
                else
                {
                    foundMatterNames.Add("<<no suggestions found>>");
                }
            }
            return foundMatterNames.ToArray();
        }

        [WebMethod]
        [ScriptMethod]
        public static string[] ClientMatterNoAutoCompleteService(string prefixText, int count)
        {
            List<string> foundClientMatterNos = new List<string>();
            using (ScreeningSearch dl = new ScreeningSearch())
            {
                List<Memo> memos = dl.SearchScreeningMemosByClientMatterNumberStartsWith(prefixText).Take(count).ToList();
                if (memos.Count > 0)
                {
                    foundClientMatterNos.AddRange(memos.Select(m => m.ClientMatterNumbers[0].ClientMatterNumber1));
                    foundClientMatterNos.Sort();
                }
                else
                {
                    foundClientMatterNos.Add("<<no suggestions found>>");
                }
            }
            return foundClientMatterNos.ToArray();
        }

        [WebMethod]
        [ScriptMethod]
        public static string[] ScreeningMemoNoAutoCompleteService(string prefixText, int count)
        {
            List<string> foundScreeningMemoNos = new List<string>();
            using (ScreeningSearch dl = new ScreeningSearch())
            {
                List<Memo> memos = dl.SearchScreeningMemosByScreeningMemoNumberStartsWith(prefixText).Take(count).ToList();
                if (memos.Count > 0)
                {
                    foundScreeningMemoNos.AddRange(memos.Select(m => m.ScrMemID.ToString()));
                    foundScreeningMemoNos.Sort();
                }
                else
                {
                    foundScreeningMemoNos.Add("<<no suggestions found>>");
                }
            }
            return foundScreeningMemoNos.ToArray();
        }

        [WebMethod]
        [ScriptMethod]
        public static string[] AdminLoginAutoCompleteService(string prefixText, int count)
        {
            List<string> foundAdminLogins = new List<string>();

            using (AdversaryData dl = new AdversaryData())
            {
                List<AdminLogin> logins = dl.SearchAdminLogins(prefixText);
                if (logins.Count > 0)
                {
                    for (int i = 0; i < count; i++)
                    {
                        if (i >= logins.Count)
                        {
                            break;
                        }

                        FinancialDataEmployee emp = dl.GetEmployee(logins[i].Login);
                        if (emp != null)
                        {
                            foundAdminLogins.Add(emp.EmployeeName + " (" + emp.EmployeeCode + ") - " + logins[i].Login);
                        }
                        else
                        {
                            foundAdminLogins.Add(logins[i].Login);
                        }
                    }
                }
            }

            return foundAdminLogins.ToArray();
        }

        #endregion

        private AdversaryData __adversaryDataLayer = null;

        public AdversaryData AdversaryDataLayer
        {
            get
            {
                if (__adversaryDataLayer == null)
                {
                    __adversaryDataLayer = new AdversaryData();
                }
                return __adversaryDataLayer;
            }
        }

        private string _BaseURL = null;

        public string BaseURL
        {
            get
            {
                if (_BaseURL == null)
                {
                    StringBuilder url = new StringBuilder();
                    url.Append(Request.Url.Scheme);
                    url.Append("://");
                    url.Append(Request.Url.Host);
                    if (Request.Url.Port != 80)
                    {
                        url.Append(":");
                        url.Append(Request.Url.Port);
                    }
                    url.Append(Request.ApplicationPath);
                    if (!url.ToString().Substring(url.ToString().Length - 1, 1).Equals("/"))
                    {
                        url.Append("/");
                    }
                    _BaseURL = url.ToString();
                }
                return _BaseURL;
            }
        }

        private CurrentUser _CurrentUser = null;

        public CurrentUser CurrentUser
        {
            get
            {
                if (this._CurrentUser == null)
                {
                    this._CurrentUser = new CurrentUser(ThisCurrentUserLogin);
                }
                return this._CurrentUser;
            }
        }

        private string _ThisCurrentUserLogin;
        private string ThisCurrentUserLogin
        {
            get
            {
                if (_ThisCurrentUserLogin == null)
                {
                    _ThisCurrentUserLogin = HttpContext.Current.User.Identity.Name.HH_StripDomain();
                    //_ThisCurrentUserLogin = HttpContext.Current.User.Identity.Name.ToString();
                    //int __intFirstIndexOfSlash = _ThisCurrentUserLogin.IndexOf("\\") + 1; //the +1 includes the slash itself.
                    //int __intLengthAfterFirstIndex = _ThisCurrentUserLogin.Length - __intFirstIndexOfSlash;
                    //_ThisCurrentUserLogin = _ThisCurrentUserLogin.Substring(__intFirstIndexOfSlash, __intLengthAfterFirstIndex);
                    //#if DEBUG
                    //_ThisCurrentUserLogin = "am_reimer";
                    //#endif
                }
                return _ThisCurrentUserLogin;
            }
        }
        //protected string CurrentUser
        //{
        //    get
        //    {
                
        //    }
        //}

        //private string _CurrentUserPayrollID;
        //protected string CurrentUserPayrollID
        //{
        //    get
        //    {
        //        if (_CurrentUserPayrollID == null)
        //        {
        //            _CurrentUserPayrollID = AdversaryDataLayer.GetEmployee(CurrentUser).EmployeeCode;
        //        }
        //        return _CurrentUserPayrollID;
        //    }
        //}

        private string _FileUploadDirectory = "C:\\";

        protected string FileUploadDirectory
        {
            get
            {
                try
                {
                    _FileUploadDirectory = System.Configuration.ConfigurationManager.AppSettings["FileUploadDirectory"].ToString();
                }
                catch { }
                return _FileUploadDirectory;
            }
        }

        protected string GetCookie(string key)
        {
            string value = null;
            HttpCookie cookie = this.Request.Cookies["Adversary"];
            if (cookie != null)
            {
                value = cookie[key];
            }
            return value;
        }

        protected void SetCookie(string key, string value)
        {
            HttpCookie cookie = this.Response.Cookies["Adversary"];
            if (cookie == null)
            {
                cookie = new HttpCookie("Adversary");
                this.Response.Cookies.Add(cookie);
            }
            cookie.Expires = DateTime.Now.AddDays(1);
            cookie[key] = value;
        }

        //protected void AddAccordionPaneJavascript(ref RadioButtonList rblButtons, string accordionPaneClientID)
        //{
        //    rblButtons.Items[0].Attributes.Add("onclick", "javascript:setAccordionPane('" + accordionPaneClientID + "', 0);");
        //    rblButtons.Items[1].Attributes.Add("onclick", "javascript:setAccordionPane('" + accordionPaneClientID + "', 1);");
        //}
        
        public override void Dispose()
        {
            if (__adversaryDataLayer != null)
            {
                __adversaryDataLayer.Dispose();
                __adversaryDataLayer = null;
            }
            base.Dispose();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (this.PreviousPage != null)
            {
                if (this.PreviousPage is Adversary.Web.Admin.AdminProcessing
                    && !(this.Page is Adversary.Web.Admin.AdminProcessing))
                {
                    Adversary.Web.Admin.AdminProcessing procPage = (Adversary.Web.Admin.AdminProcessing)this.PreviousPage;
                    this.UnlockMemo(procPage);
                }
            }
        }

        private void UnlockMemo(Adversary.Web.Admin.AdminProcessing procPage)
        {
            Adversary.Web.Controls.AdversaryLocking.Instance.RemoveLock(procPage.LockedScrMemID);
        }

        /// TODO: FS LEVEL 1 Do it like this but only if you have to.
        /// TODO: FS LEVEL 2 Don't do it you'll just have to redo it later.
        /// TODO: FS LEVEL 3 Never do it like this.
        /// TODO: FS LEVEL 4 Hannibal Lecter would do it like this Clarice.
        /// TODO: FS LEVEL 5 View this code only in the reflection of your shield.
    }
}