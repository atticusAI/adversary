﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI.WebControls;
using Adversary.DataLayer;
using Adversary.DataLayer.FinancialDataService;
using AjaxControlToolkit;
using Adversary.Utils;
using System.Text;


namespace Adversary.Web
{
    public class ScreeningBasePage : Adversary.Web.Pages.TrackBasePage
    {
        //#region web/script methods
        //[WebMethod]
        //[ScriptMethod]
        //public static string[] TimekeeperService(string prefixText, int count)
        //{
        //    List<string> foundTimekeepers = new List<string>();
        //    JavaScriptSerializer serializer = new JavaScriptSerializer();
            
        //    List<FinancialDataEmployee> emps = new List<FinancialDataEmployee>();
        //    string[] employees = new string[1];

        //    using (AdversaryData data = new AdversaryData())
        //    {
        //        emps = data.SearchEmployees(prefixText).OrderBy(M => M.EmployeeName).Take(count).ToList();
        //    }

        //    //int i = 0;
        //    int returnSetCount = (emps.Count > count ? count : emps.Count);
        //    if (emps.Count > 0)
        //    {
        //        foreach (FinancialDataEmployee emp in emps)
        //        {
        //            foundTimekeepers.Add(AutoCompleteExtender.CreateAutoCompleteItem(
        //                emp.EmployeeName + " (" + emp.EmployeeCode + ")", emp.EmployeeCode));
        //        }
        //        return foundTimekeepers.ToArray();
        //    }
        //    else
        //    {
        //       employees = new string[] { "<<no suggestions found>>" };
        //       return employees;
        //    }
        //}

        //[WebMethod]
        //[ScriptMethod]
        //public static string[] ClientService(string prefixText, int count)
        //{
            
        //    List<string> foundClients = new List<string>();
        //    JavaScriptSerializer serializer = new JavaScriptSerializer();
        //    List<Client> clts = new List<Client>();

        //    string[] clients = new string[1];

        //    using (AdversaryData data = new AdversaryData())
        //    {
        //        clts = data.SearchClients(prefixText).Take(count).ToList();
        //    }

        //    if (clts.Count > 0)
        //    {
        //        if (prefixText.Substring(0, 1).IsNumeric())
        //        {
        //            clts.OrderBy(M => M.ClientNumber);
        //        }
        //        else
        //        {
        //            clts.OrderBy(M => M.ClientName);
        //        }
        //        foreach (Client clt in clts)
        //        {
        //            foundClients.Add(AutoCompleteExtender.CreateAutoCompleteItem(
        //                clt.ClientName + "(" + clt.ClientCode + ")", clt.ClientCode));
        //        }
        //    }
        //    else
        //    {
        //        foundClients.Add(AutoCompleteExtender.CreateAutoCompleteItem("<<no suggestions found>>", ""));
        //    }

        //    return foundClients.ToArray();
        //}

        //[WebMethod]
        //[ScriptMethod]
        //public static bool DocketingInfoRequired(string pracCode)
        //{
        //    bool required = true;

        //    if (string.IsNullOrWhiteSpace(pracCode))
        //    {
        //        required = false;
        //    }
        //    else if (pracCode.Contains("E") || pracCode.Contains("T"))
        //    {
        //        required = false;
        //    }
        //    else if (pracCode == "501L" || pracCode == "502L")
        //    {
        //        required = false;
        //    }

        //    return required;
        //}
        //#endregion

        public bool DocketingInfoRequired()
        {
            bool required = true;
            if (ThisMemo != null)
            {
                required = DocketingInfoRequired(ThisMemo.PracCode);
            }
            return required;
        }

        private DisplayMode _displayMode = DisplayMode.WEB;
        public DisplayMode DisplayMode
        {
            get { return _displayMode; }
            set { _displayMode = value; }
        }

        private Memo _thisMemo = null;

        public Memo ThisMemo
        {
            get {
                if (_thisMemo == null)
                {
                    _thisMemo = AdversaryDataLayer.GetMemo(ScreeningMemoID);
                    if (_thisMemo == null)
                    {
                        _thisMemo = new Memo();
                    }
                }
                return _thisMemo;
            }
        }

        private int? _screeningMemoID;

        public int ScreeningMemoID
        {
            get 
            {
                if (this._screeningMemoID == null)
                {
                    int scrMemID = -1;
                    int.TryParse(Request.QueryString["ScrMemID"], out scrMemID);
                    _screeningMemoID = scrMemID;
                }
                return this._screeningMemoID ?? -1;
            }
        }

        private ScreeningMasterPage _thisMasterPage = null;

        public ScreeningMasterPage ThisMasterPage
        {
            get
            {
                if (_thisMasterPage == null)
                {
                    if (this.Master is ScreeningMasterPage)
                    {
                        _thisMasterPage = this.Master as ScreeningMasterPage;
                    }
                }
                return _thisMasterPage;
            }
        }

        private Adversary.Web.Controls.ErrorSummary _errorSummary = null;

        public Adversary.Web.Controls.ErrorSummary ErrorSummary
        {
            get
            {
                if (this._errorSummary == null && ThisMasterPage != null)
                {
                    this._errorSummary = (Adversary.Web.Controls.ErrorSummary)ThisMasterPage.FindControl("master_ErrorSummary");
                }
                return this._errorSummary;
            }
        }

        private string _absolutePath = null;

        public string AbsolutePath
        {
            get
            {
                if (this._absolutePath == null)
                {
                    this._absolutePath = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
                }
                return this._absolutePath;
            }
        }

        private string _pageFileName = null;

        //get the page file name for putting together the next/previous buttons
        public string PageFileName
        {
            get
            {
                if (this._pageFileName == null)
                {
                    System.IO.FileInfo oInfo = new System.IO.FileInfo(this.AbsolutePath);
                    this._pageFileName = oInfo.Name;
                }
                return this._pageFileName;
            }
        }

        private bool _HasScreeningMemoError;

        public bool HasScreeningMemoError
        {
            get { return _HasScreeningMemoError; }
            private set { _HasScreeningMemoError = value; }
        }

        //private string _BaseURL;

        //public string BaseURL
        //{
        //    get
        //    {
        //        StringBuilder url = new StringBuilder();
        //        url.Append(Request.Url.Scheme);
        //        url.Append("://");
        //        url.Append(Request.Url.Host);
        //        if (Request.Url.Port != 80)
        //        {
        //            url.Append(":");
        //            url.Append(Request.Url.Port);
        //        }
        //        url.Append(Request.ApplicationPath);
        //        if (!url.ToString().Substring(url.ToString().Length - 1, 1).Equals("/"))
        //        {
        //            url.Append("/");
        //        }
        //        _BaseURL = url.ToString();
        //        return _BaseURL; 
        //    }
        //}

        /// <summary>
        /// force the memo to reload the next time it is accessed
        /// </summary>
        public void ReloadMemo()
        {
            _thisMemo = null;
        }        

        //public void LoadMemo()
        //{
        //    // _ThisMemo = new Memo();
        //    //AdversaryData data = new AdversaryData();
        //    //try
        //    //{
        //    //    _ThisMemo = data.GetMemo(_ScreeningMemoID);
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    throw ex;
        //    //}
        //    // ThisMemo = AdversaryDataLayer.GetMemo(ScreeningMemoID);
        //    // empty instead of null
        //    // if (ThisMemo == null)
        //    // {
        //    //     ThisMemo = new Memo();
        //    // }
        //}

        //protected string GetCookie(string key)
        //{
        //    string value = null;
        //    HttpCookie cookie = this.Request.Cookies["Adversary"];
        //    if (cookie != null)
        //    {
        //        value = cookie[key];
        //    }
        //    return value;
        //}

        //protected void SetCookie(string key, string value)
        //{
        //    HttpCookie cookie = this.Response.Cookies["Adversary"];
        //    if (cookie == null)
        //    {
        //        cookie = new HttpCookie("Adversary");
        //        this.Response.Cookies.Add(cookie);
        //    }
        //    cookie.Expires = DateTime.Now.AddDays(1);
        //    cookie[key] = value;                        
        //}

        //protected void AddAccordionPaneJavascript(ref RadioButtonList rblButtons, string accordionPaneClientID)
        //{
        //    rblButtons.Items[0].Attributes.Add("onclick", "javascript:setAccordionPane('" + accordionPaneClientID + "', 0);");
        //    rblButtons.Items[1].Attributes.Add("onclick", "javascript:setAccordionPane('" + accordionPaneClientID + "', 1);");
        //}

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            string applicationPath = "http://" + Request.Url.Authority;
            _HasScreeningMemoError = false;

            if (this.ThisMasterPage != null)
            {
                this.InitScreeningMasterPage();
            }
        }

        private void InitScreeningMasterPage()
        {
            if(Request.QueryString["scrmemid"] != null)
            //get the screening memo, or create a new record/entry
            //if (scrmemIDFromQuerystring != null)
            {
                try
                {
                    //this will throw an error if the scrmemid is missing
                    int scrmemIDFromQuerystring = Int32.Parse(Request.QueryString["scrmemid"].ToString());

                    if (ScreeningMemoID > 0)
                    {
                        ReloadMemo();
                        this.ShowDocketing = DocketingInfoRequired(ThisMemo.PracCode);
                    }
                }
                catch
                {
                    this.ErrorSummary.DisplayMessage = "You have entered an invalid screening memo";
                    this.ErrorSummary.DisplayType = Web.Controls.MessageDisplayType.DisplayError;
                    this.ErrorSummary.ErrorList.Add(String.Format("{0} is an invalid screening memo number. Please evaluate your input and try again.", Request.QueryString["scrmemid"]));
                    this.HasScreeningMemoError = true;
                }
            }
            else if (Request.QueryString["scrmemid"] == null && (!this.AbsolutePath.ToLower().Contains("default") && !this.AbsolutePath.ToLower().Contains("search")))
            {
                this.ErrorSummary.DisplayMessage = "You have not selected a screening memo";
                this.ErrorSummary.DisplayType = Web.Controls.MessageDisplayType.DisplayError;
                this.ErrorSummary.ErrorList.Add("Please select a screening memo in order to continue.");
                this.HasScreeningMemoError = true;
            }

            if (_HasScreeningMemoError && !this.AbsolutePath.ToLower().Contains("default") && !this.AbsolutePath.ToLower().Contains("search"))
            {
                Response.Redirect(Page.TemplateSourceDirectory + "/default.aspx");
                return;
            }

            if (ThisMasterPage != null)
            {
                ThisMasterPage.CurrentPageFileName = this.PageFileName;
                if (ThisMemo != null)
                {
                    ThisMasterPage.CurrentTrack = Convert.ToInt32(ThisMemo.ScrMemType);
                    ThisMasterPage.ScreeningMemoID = ThisMemo.ScrMemID;
                }
                ThisMasterPage.ValidationGroupName = "valsumErrorSummary";
            }
            
            //indicate error if the screening memo ID passed in the query string is invalid somehow
            if (ThisMemo != null)
            {
                bool cmNumsSent = false;
                //see if the memo is locked
                if(ThisMemo.HH_IsMemoLocked() || ThisMemo.HH_IsMemoArchived())
                {
                    ///TODO: this could use some refactoring to make use of extension methods - perhaps some extra "why"
                    ///the memo is locked. Extension properties... .if only.
                    if (ThisMemo.Trackings.FirstOrDefault() != null)
                    {
                        //cmNumsSent = (ThisMemo.Trackings.First().CMNumbersSentDate.HasValue &&
                        //    ThisMemo.Trackings.First().CMNumbersSentDate.Value > DateTime.MinValue);
                        cmNumsSent = ThisMemo.HH_IsClientMatterSent();
                    }
                    if (cmNumsSent)
                    {
                        this.ErrorSummary.DisplayMessage = "Client matter numbers have been issued for this memo. The memo is locked and cannot be edited.";
                        //this.ErrorSummary.DisplayType = Web.Controls.MessageDisplayType.DisplayInfo;
                    }
                    //else if (ThisMemo.ScrMemID < 100000 && ThisMemo.ScrMemID != 0)
                    else if(ThisMemo.HH_IsMemoArchived())
                    {
                        this.ErrorSummary.DisplayMessage = "This memo is archived and cannot be edited.";
                        //this.ErrorSummary.DisplayType = Web.Controls.MessageDisplayType.DisplayInfo;
                        //this.ErrorSummary.HideIcon = false;
                    }
                    else
                    {
                        this.ErrorSummary.DisplayMessage = "This memo is currently locked and cannot be edited.";
                        //this.ErrorSummary.DisplayType = Web.Controls.MessageDisplayType.DisplayInfo;
                    }
                    //disable all the track toolbar buttons here.
                    this.ErrorSummary.DisplayType = Web.Controls.MessageDisplayType.DisplayInfo;
                }
                else
                {
                    if (ThisMemo.Trackings.Count > 0)
                    {
                        if (ThisMemo.Trackings.First().StatusTypeID > 0)
                        {
                            this.ErrorSummary.DisplayMessage = AdversaryDataLayer.GetTrackingStatusTypes().Where(T => T.StatusTypeID.Equals(Convert.ToInt32(ThisMemo.Trackings.First().StatusTypeID))).First().StatusMessage;
                            switch (ThisMemo.Trackings.First().StatusTypeID)
                            {
                                case 1:
                                    //this.ErrorSummary.ValidIconPath = "~/Images/e_mail.png";
                                    this.ErrorSummary.DisplayType = Web.Controls.MessageDisplayType.DisplayMessage;
                                    break;
                                case 200:
                                    this.ErrorSummary.DisplayType = Web.Controls.MessageDisplayType.DisplayWarning;
                                    this.ErrorSummary.ErrorList = this.GetRejectionDisplayMessage(ThisMemo.Trackings[0]);
                                    break;
                                case 3:
                                    this.ErrorSummary.DisplayType = Web.Controls.MessageDisplayType.DisplayMessage;
                                    break;
                                case 100:
                                    this.ErrorSummary.DisplayType = Web.Controls.MessageDisplayType.DisplayInfo;

                                    ///TODO: add a piece of logic here to disable toolbar buttons
                                    _thisMasterPage.DisableTrackButtons = true;
                                    break;
                                case 600:
                                    this.ErrorSummary.DisplayType = Web.Controls.MessageDisplayType.DisplayWarning;
                                    this.ErrorSummary.ErrorList = this.GetRejectionDisplayMessage(ThisMemo.Trackings[0]);
                                    break;
                                default:
                                    this.ErrorSummary.DisplayType = Web.Controls.MessageDisplayType.DisplayNone;
                                    break;
                            }
                        }
                    }
                    else
                    {
                        this.ErrorSummary.DisplayType = Web.Controls.MessageDisplayType.DisplayNone;
                    }
                }
            }
        }

        private List<string> GetRejectionDisplayMessage(Tracking trk)
        {
            List<string> reasons = new List<string>();
            if (!string.IsNullOrWhiteSpace(trk.APRejectionReason)) reasons.Add(trk.APRejectionReason);
            if (!string.IsNullOrWhiteSpace(trk.PGMRejectionReason)) reasons.Add(trk.PGMRejectionReason);
            if (!string.IsNullOrWhiteSpace(trk.AdversaryRejectionReason)) reasons.Add(trk.AdversaryRejectionReason);
            return reasons;
        }
       
        public bool ShowDocketing
        {
            get
            {
                bool show = true;
                if (this.Master is ScreeningMasterPage)
                {
                    return ((ScreeningMasterPage)this.Master).ShowDocketing;
                }
                return show;
            }

            set
            {
                if (this.Master is ScreeningMasterPage)
                {
                    ((ScreeningMasterPage)this.Master).ShowDocketing = value;
                }
            }
        }

        //private AdversaryData __adversaryDataLayer = null;

        //public AdversaryData AdversaryDataLayer
        //{
        //    get
        //    {
        //        if (__adversaryDataLayer == null)
        //        {
        //            __adversaryDataLayer = new AdversaryData();
        //        }
        //        return __adversaryDataLayer;
        //    }
        //}

        //protected override void OnUnload(EventArgs e)
        //{
        //    base.OnUnload(e);
        //    if (__adversaryDataLayer != null)
        //    {
        //        __adversaryDataLayer.Dispose();
        //        __adversaryDataLayer = null;
        //    }
        //}

        protected void HideErrorSummary()
        {
            if (this.ErrorSummary != null)
            {
                this.ErrorSummary.Visible = false;
            }
        }

    }
}