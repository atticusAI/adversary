﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.Web.Pages
{
    public interface IMemoTrack
    {
        int CurrentLocation { set; }
        int CurrentTrack { set; }
    }
}
