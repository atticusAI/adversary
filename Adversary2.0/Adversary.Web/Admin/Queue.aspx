﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true"
    CodeBehind="Queue.aspx.cs" Inherits="Adversary.Web.Admin.Queue" Theme="HH" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../Controls/QueueRepeater.ascx" TagName="QueueRepeater" TagPrefix="uc1" %>
<%@ Register src="../Controls/AutoRefresh.ascx" tagname="AutoRefresh" tagprefix="uc2" %>
<asp:Content ID="cntHead" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" language="javascript">
        function toggleQueue(cls, btn) {
            $(cls).toggle("slow");
            $(btn).toggleClass("hide");
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="cntMain" ContentPlaceHolderID="cphMainContent" runat="server">
    <asp:Label ID="lblImpError" runat="server" OnPreRender="lblImpError_PreRender" CssClass="error" />
    <asp:Label ID="lblRefresh" runat="server" Visible="false" />
    <uc2:AutoRefresh ID="refQueue" runat="server" OnRefresh="refQueue_Refresh" IntervalSeconds="60" />
    <asp:LinkButton ID="lbFilter" runat="server" Text="Filter Your Queue" />
    <asp:ModalPopupExtender ID="mpxFilter" runat="server" BackgroundCssClass="pop-back"
        TargetControlID="lbFilter" PopupControlID="pnlFilter" CancelControlID="btnClose" />
    <asp:Panel ID="pnlFilter" runat="server" CssClass="pop" Style="display: none">
        <table>
            <tr>
                <td class="lbl">
                    Office
                </td>
                <td>
                    <asp:DropDownList ID="ddlAPOffice" runat="server" AutoPostBack="true" AppendDataBoundItems="true"
                        OnInit="ddlAPOffice_Init" OnDataBound="ddlAPOffice_DataBound">
                        <asp:ListItem Text="All Offices" Value="" />
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="lbl">
                    Practice Group
                </td>
                <td>
                    <asp:DropDownList ID="ddlPracGroup" runat="server" AutoPostBack="true" AppendDataBoundItems="true"
                        OnInit="ddlPracGroup_Init">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="lbl">
                    Status
                </td>
                <td>
                    <asp:DropDownList ID="ddlStatus" runat="server" AutoPostBack="true" AppendDataBoundItems="true"
                        OnInit="ddlStatus_Init">
                        <asp:ListItem Text="All Open Memos" Value="all" />
                    </asp:DropDownList>
                </td>
            </tr>
            <tr id="trOpen" runat="server" visible="false">
                <td class="lbl">
                    Open Memo Visibility
                </td>
                <td>
                    <asp:DropDownList ID="ddlOpen" runat="server" AutoPostBack="true">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="lbl">
                    Assigned To
                </td>
                <td>
                    <asp:DropDownList ID="ddlAssignedTo" runat="server" AutoPostBack="true">
                        <asp:ListItem Text="Anyone" Value="Anyone" />
                        <asp:ListItem Text="Me" Value="me" />
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="lbl">
                    Sort Order
                </td>
                <td>
                    <asp:DropDownList ID="ddlSort" runat="server" AutoPostBack="true">
                        <asp:ListItem Text="Oldest First" Value="true" />
                        <asp:ListItem Text="Newest First" Value="false" />
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="btnRemove" runat="server" Text="Remove Filter" OnClick="btnRemove_Click" />
                    <asp:Button ID="btnClose" runat="server" Text="Close" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlHelp" runat="server" OnPreRender="pnlHelp_PreRender">
        <a runat="server" class="queue about hide" target="aboutPage" href="#aboutPage" onclick="return toggleQueue('.queuewrap.about', '.queue.about');">
            About This Screen</a>
        <asp:Panel ID="pnlAbout" runat="server" CssClass="queuewrap about" Style="display: none;">
            <ul style="font-size: 10pt; text-align: left;">
                <li><strong>Primary Queue</strong><br />
                    Items in this queue require your approval as Primary Approver <strong>(OR)</strong>
                    are items over four days old that require your approval as Backup Approver.<br />
                    <br />
                </li>
                <li><strong>Backup Queue</strong><br />
                    Items in this queue are awaiting approval by the Primary Approver.<br />
                    <br />
                </li>
                <li><strong>Approved Queue</strong><br />
                    Items in this queue have already been approved by you or another approver and are
                    awaiting further approval and/or processing by Adversary.<br />
                    <br />
                </li>
            </ul>
        </asp:Panel>
    </asp:Panel>
    <asp:Label ID="lblCount" runat="server" OnPreRender="lblCount_PreRender" />
    <asp:Panel ID="pnlBackup" runat="server" Visible="true" OnPreRender="pnlBackup_PreRender">
        <a id="aBackup" runat="server" class="queue backup hide" target="backupQueue" href="#backupQueue"
            onclick="return toggleQueue('.queuewrap.backup', '.queue.backup');">Show items in
            your backup queue</a>
        <asp:Panel CssClass="queuewrap backup" ID="pnlBackupRepeater" runat="server" Style="display: none;">
            <uc1:QueueRepeater ID="repBackup" OnPreRender="repBackup_PreRender" runat="server" />
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlPrimary" runat="server" Visible="true" OnPreRender="pnlPrimary_PreRender">
        <a id="aPrimary" runat="server" class="queue primary" target="primaryQueue" href="#primaryQueue"
            onclick="return toggleQueue('.queuewrap.primary', '.queue.primary');">Show items
            in your primary queue</a>
        <asp:Panel CssClass="queuewrap primary" ID="pnlPrimaryRepeater" runat="server">
            <uc1:QueueRepeater ID="repPrimary" OnPreRender="repPrimary_PreRender" runat="server" />
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlProcessing" runat="server" Visible="true" OnPreRender="pnlProcessing_PreRender">
        <a id="aProcessing" runat="server" class="queue processing hide" target="processingQueue"
            href="#processingQueue" onclick="return toggleQueue('.queuewrap.processing', '.queue.processing');">
            Show items being processed by Adversarys</a>
        <asp:Panel CssClass="queuewrap processing" ID="pnlProcessingRepeater" runat="server" style="display:none;">
            <uc1:QueueRepeater ID="repProcessing" OnPreRender="repProcessing_PreRender" runat="server" />
        </asp:Panel>
    </asp:Panel>
</asp:Content>
