﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Adversary.Web.Pages;

namespace Adversary.Web.Admin
{
    public partial class ApprovalDefault : TrackBasePage
    {
        protected void dtApproval_PreRender(object sender, EventArgs e)
        {
            ((HtmlGenericControl)sender).Visible = this.CurrentUser.IsAdversary;
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (this.Page.Master is BaseMaster)
            {
                ((BaseMaster)this.Page.Master).TabPageUrl = "/Admin/ApprovalDefault.aspx";
                ((BaseMaster)this.Page.Master).SubTabPageUrl = "/Admin/ApprovalDefault.aspx";
            }
        }
    }
}