﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.DataLayer;

namespace Adversary.Web.Admin
{
    public partial class DailyList : Pages.AdminBasePage
    {
        DateTime startDate;
        DateTime endDate;
        bool includeConfidential;
        string userID;

        protected void Page_Load(object sender, EventArgs e)
        {
            userID = CurrentUser.CurrentUserLogin;
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (!Page.IsPostBack)
            {
                tbStartDate.Text = DateTime.Today.AddDays(-1).ToShortDateString();
                tbEndDate.Text = DateTime.Today.ToShortDateString();
            }
            base.Render(writer);
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            //do validation
            DateTime __startDate;
            DateTime __endDate;

            try
            {
                bool __startDateSuccess = DateTime.TryParse(tbStartDate.Text, out __startDate);
                bool __endDateSuccess = DateTime.TryParse(tbEndDate.Text, out __endDate);

                this.includeConfidential = cbAllMemos.Checked;

                if (rblAllIndividual.SelectedValue.Equals("Everyone"))
                    userID = string.Empty;

                if (__startDateSuccess && __endDateSuccess)
                {
                    startDate = __startDate;
                    endDate = __endDate;

                    GetDailyListData();
                }
                else
                {
                    ucErrors.DisplayMessage = "Please enter valid dates and try your request again.";
                    ucErrors.DisplayType = Web.Controls.MessageDisplayType.DisplayError;
                    upnlErrors.Update();
                }

            }
            catch (Exception ex)
            {
                ucErrors.DisplayMessage = String.Format("There was an error in your request: {0}", ex.Message);
                ucErrors.DisplayType = Web.Controls.MessageDisplayType.DisplayError;
                upnlErrors.Update();
            }
        }

        private void GetDailyListData()
        {
            List<Request> _Requests = AdversaryDataLayer.GetDailyList(startDate, endDate.AddDays(1).AddMinutes(-1), includeConfidential, userID);

            TableRow tr = new TableRow();
            TableCell tc;
            bool isExisting = false;
            foreach (Request _request in _Requests)
            {
                if (_request.ClientCategory.ToUpperInvariant().Equals("EXISTING") && !isExisting)
                {
                    tc = new TableCell();
                    tr = new TableRow();
                    tc.Text = "SEARCHES DONE ON EXISTING CLIENTS";
                    tr.Cells.Add(tc);
                    this.tblParties.Rows.Add(tr);
                    isExisting = true;
                }

                if (!String.IsNullOrEmpty(_request.ClientName))
                {
                    tc = new TableCell();
                    tr = new TableRow();
                    tc.Text = CreatePartiesString(_request.ClientName);

                    //tack on the practice code
                    if (!String.IsNullOrEmpty(_request.PracticeCode))
                        tc.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + _request.PracticeCode;

                    tc.Font.Bold = true;
                    tr.Cells.Add(tc);
                    this.tblParties.Rows.Add(tr);
                }
                if (!String.IsNullOrEmpty(_request.RelatedParties) || !String.IsNullOrEmpty(_request.RelatedCoDefendants))
                {
                    tc = new TableCell();
                    tr = new TableRow();
                    tc.Text = "Related: " + CreatePartiesString(_request.RelatedParties) + CreatePartiesString(_request.RelatedCoDefendants);
                    tr.Cells.Add(tc);
                    this.tblParties.Rows.Add(tr);
                }
                if (!String.IsNullOrEmpty(_request.AdverseParties) || !String.IsNullOrEmpty(_request.AffiliatesOfAdverseParties))
                {
                    tc = new TableCell();
                    tr = new TableRow();
                    tc.Text = "Adverse: " + CreatePartiesString(_request.AdverseParties) + CreatePartiesString(_request.AffiliatesOfAdverseParties);
                    tr.Cells.Add(tc);
                    this.tblParties.Rows.Add(tr);
                }
                if (!String.IsNullOrEmpty(_request.PartiesToMediationArbitration))
                {
                    tc = new TableCell();
                    tr = new TableRow();
                    tc.Text = CreatePartiesString(_request.PartiesToMediationArbitration);
                    tr.Cells.Add(tc);
                    this.tblParties.Rows.Add(tr);
                }
                if (!String.IsNullOrEmpty(_request.AttyLogin))
                {
                    tc = new TableCell();
                    tr = new TableRow();
                    try
                    {
                        tc.Text = "(" + GetAttorneyName(_request.AttyLogin).ToUpper() + ")";
                    }
                    catch
                    {
                        tc.Text = "Attorney " + _request.AttyLogin + " was not found";
                    }

                    tr.Cells.Add(tc);
                    this.tblParties.Rows.Add(tr);
                }
                tr = new TableRow();
                tc = new TableCell();
                tc.Text = "&nbsp;";
                tr.Cells.Add(tc);
                this.tblParties.Rows.Add(tr);
            }
        }
        
        protected string CreatePartiesString(string targetString)
        {
            if (String.IsNullOrEmpty(targetString)) return "<br/>";
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(@"(\r\n|\r|\n)+");
            string newText = RepeatString("&nbsp;", 9) + regex.Replace(targetString, "<br/>");
            return newText;
        }

        private string RepeatString(string stringToRepeat, int numberOfTimes)
        {
            string strReturn = stringToRepeat;
            for (int i = 0; i < numberOfTimes - 1; i++)
            {
                strReturn += stringToRepeat;
            }
            return stringToRepeat;
        }

        protected string GetAttorneyName(string payrollID)
        {
            Adversary.DataLayer.HRDataService.Employee hrEmployee = AdversaryDataLayer.GetHREmployee(payrollID);
            return hrEmployee.SortLastName + ", " + hrEmployee.SortFirstName + " " + (hrEmployee.PreferredMiddleName ?? "");
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (this.Page.Master is AdminMaster)
            {
                ((AdminMaster)this.Page.Master).TabPageUrl = "/Admin/AdminDefault.aspx";
                ((AdminMaster)this.Page.Master).SubTabPageUrl = "/Admin/AdminDefault.aspx";
            }
        }
    }
}