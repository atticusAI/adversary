﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true"
    CodeBehind="AdminSearch.aspx.cs" Inherits="Adversary.Web.Admin.AdminSearch" Theme="HH" %>

<%@ Register Src="../Controls/ScreeningMemoList.ascx" TagName="ScreeningMemoList"
    TagPrefix="uc3" %>
<%@ Register Src="../Controls/ScreeningMemoSearch.ascx" TagName="ScreeningMemoSearch"
    TagPrefix="uc5" %>
<asp:Content ID="cntMain" ContentPlaceHolderID="cphMainContent" runat="server">
    <uc5:ScreeningMemoSearch ID="screeningMemoSearch" runat="server" OnSearchClick="btnSearch_Click"
        OnClearClick="btnClear_Click" />
    <asp:Panel ID="pnlSearch" runat="server" Visible="false">
        <uc3:ScreeningMemoList ID="smlSearch" runat="server" TypeName="Adversary.DataLayer.ScreeningSearch"
            SelectMethod="SearchScreeningMemos" SortParameterName="sortExpression" AllowSorting="true"
            AllowPaging="true" Mode="AdminSearch">
            <SelectParameters>
                <asp:ControlParameter Name="client" ControlID="screeningMemoSearch" PropertyName="SelectedClientCode"
                    Type="String" />
                <asp:ControlParameter Name="matter" ControlID="screeningMemoSearch" PropertyName="SelectedMatter"
                    Type="String" />
                <asp:ControlParameter Name="clientMatterNumber" ControlID="screeningMemoSearch" PropertyName="SelectedClientMatter"
                    Type="String" />
                <asp:ControlParameter Name="attorney" ControlID="screeningMemoSearch" PropertyName="SelectedAttorneyCode"
                    Type="String" />
                <asp:ControlParameter Name="employee" ControlID="screeningMemoSearch" PropertyName="SelectedEmployeeCode"
                    Type="String" />
                <asp:ControlParameter Name="screeningMemoNumber" ControlID="screeningMemoSearch"
                    PropertyName="SelectedMemoNumber" Type="String" />
            </SelectParameters>
        </uc3:ScreeningMemoList>
    </asp:Panel>
</asp:Content>
