﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true"
    CodeBehind="AdminProcessing.aspx.cs" Inherits="Adversary.Web.Admin.AdminProcessing"
    Theme="HH" %>

<%@ Register Src="~/Controls/ARTable.ascx" TagName="ARTable" TagPrefix="adv" %>
<%@ Register Src="~/Controls/ApprovalTable.ascx" TagName="ApprovalTable" TagPrefix="adv" %>
<%@ Register Src="~/Controls/ApproverComboBox.ascx" TagName="ApproverComboBox" TagPrefix="adv" %>
<%@ Register Src="~/Controls/ListContainerControl.ascx" TagName="ListContainerControl"
    TagPrefix="adv" %>
<%@ Register Src="~/Controls/ScreeningMemoLock.ascx" TagName="ScreeningMemoLock"
    TagPrefix="adv" %>
<%@ Register Src="~/Controls/PrintViewLink.ascx" TagName="PrintViewLink" TagPrefix="adv" %>
<asp:Content ID="cntHead" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="cntMain" ContentPlaceHolderID="cphMainContent" runat="server">
    <adv:ScreeningMemoLock ID="scrLock" runat="server" />
    <asp:FormView ID="frmAdminProc" runat="server" DefaultMode="Edit" OnPreRender="frmAdminProc_PreRender">
        <EmptyDataTemplate>
            <span class="error">No screening memo has been selected.</span>
        </EmptyDataTemplate>
        <EditItemTemplate>
            <div>
                 <asp:HyperLink ID="hypApprovalRedir" runat="server" Text="Go to the Approval Page for this Memo"
                     NavigateUrl='<%#GetApprovalUrl(Container.DataItem)%>' />
                <asp:HiddenField ID="hidLockedScrMemID" runat="server" Value='<%#Eval("ScrMemID")%>' />
                <asp:HiddenField ID="hidTrackingID" runat="server" Value='<%#GetTrackingID(Container.DataItem)%>' />
                <adv:ARTable ID="arBal" runat="server" OnPreRender="arBal_PreRender" />
                <table runat="server" id="tableSubmittedOn">
                    <tr id="_trAR" runat="server" visible="false">
                        <td id="_tdAR" runat="server">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div>
                                <adv:ApprovalTable ID="apprTable" runat="server" OnPreRender="apprTable_PreRender" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="app">
                                <tr>
                                    <td>
                                        Confidential Memo?
                                    </td>
                                    <td>
                                        <asp:Label ID="lblConfidential" runat="server" Text='<%#GetConfidential(Container.DataItem)%>' />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Client
                                    </td>
                                    <td>
                                        <asp:Label ID="lblClient" runat="server" Text='<%#GetClientDisplayName(Container.DataItem)%>' />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Matter Name
                                    </td>
                                    <td>
                                        <asp:Label ID="lblMatter" runat="server" Text='<%#GetMatterName(Container.DataItem)%>' />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Attorney
                                    </td>
                                    <td>
                                        <asp:Label ID="lblAtty" runat="server" Text='<%#GetEnteringAttorneyName(Container.DataItem)%>' />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Notes<br />
                                        <asp:TextBox ID="tbNotes" runat="server" TextMode="MultiLine" Rows="6" Text='<%#GetNotes(Container.DataItem)%>' />
                                    </td>
                                </tr>
                            </table>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="app">
                                <tr>
                                    <td>
                                        Adversary Group Processing - Processed By
                                    </td>
                                    <td>
                                        <adv:ApproverComboBox ID="approverCombo" runat="server" ApproverComboBoxType="AdversaryGroup"
                                            OnPreRender="approverCombo_PreRender" OnSelectedIndexChanged="approverCombo_SelectedIndexChanged"
                                            AutoPostBack="true" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Adversary Approved for Processing
                                    </td>
                                    <td>
                                        <asp:Label ID="lblAdvAdminApproval" runat="server" Text='<%#GetApproval(Container.DataItem)%>' />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Opened
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbOpened" runat="server" Text='<%#GetOpened(Container.DataItem)%>'
                                            MaxLength="2000" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Conflicts<br />
                                        <asp:TextBox ID="tbConflicts" runat="server" TextMode="MultiLine" Rows="4" Text='<%#GetConflicts(Container.DataItem)%>'
                                            MaxLength="2000" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Fee Splits<br />
                                        <asp:TextBox ID="tbFeeSplits" runat="server" TextMode="MultiLine" Rows="4" Text='<%#GetFeeSplits(Container.DataItem)%>'
                                            MaxLength="2000" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        Final Check<br />
                                        <asp:TextBox ID="tbFinalCheck" runat="server" TextMode="MultiLine" Rows="4" Text='<%#GetFinalCheck(Container.DataItem)%>' />
                                    </td>
                                </tr>
                            </table>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnComplete" runat="server" Text="Mark Complete" CommandArgument="Complete"
                                OnClick="btnComplete_Click" Enabled='<%#(GetCompleteEnabled(Container.DataItem))%>' 
                                OnClientClick="return confirm('Are you sure you would like to set this memo\'s status to complete and remove it form the queue?');" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblCMTimeStamp" runat="server" Visible='<%#ShowClientMatterNumberStatus(Container.DataItem)%>'
                                Text='<%#GetClientMatterNumberStatus(Container.DataItem)%>' /><br />
                            <adv:ListContainerControl ID="cmList" runat="server" Text="Add Client Matter Number"
                                AutoGenerateColumns="false" OnPreRender="cmList_PreRender" OnRowCommand="cmList_RowCommand"
                                OnAddClick="cmList_AddClick" OnChangeClick="cmList_ChangeClick" OnDeleteClick="cmList_DeleteClick"
                                PopupCssClass="pop" BackgroundCssClass="pop-back">
                                <EmptyDataTemplate>
                                    No Client Matter Numbers found for this memo.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:BoundField DataField="ClientMatterNumber1" HeaderText="Client Matter Number"
                                        ShowHeader="true" />
                                </Columns>
                                <InsertItemTemplate>
                                    <div>
                                        Add a new Client Matter Number to this Screening Memo<br />
                                        Client Matter Number:
                                        <asp:TextBox ID="txtClientMatterNumber" runat="server" />
                                    </div>
                                </InsertItemTemplate>
                                <DeleteItemTemplate>
                                    <div>
                                        <asp:HiddenField ID="hidClientMatterID" runat="server" Value='<%#Eval("ClientMatterID")%>' />
                                        Are you sure you want to delete this Client Matter Number?<br />
                                        Client Matter Number:
                                        <asp:Label ID="lblDel" runat="server" Text='<%#Eval("ClientMatterNumber1")%>' />
                                    </div>
                                </DeleteItemTemplate>
                                <EditItemTemplate>
                                    <div>
                                        <asp:HiddenField ID="hidClientMatterID" runat="server" Value='<%#Eval("ClientMatterID")%>' />
                                        Client Matter Number:
                                        <asp:TextBox ID="txtClientMatterNumber" runat="server" Text='<%#Eval("ClientMatterNumber1")%>' />
                                    </div>
                                </EditItemTemplate>
                            </adv:ListContainerControl>
                            <asp:LinkButton ID="lbSend" runat="server" OnClick="lbSend_Click" Text='<%#GetSendButtonText(Container.DataItem)%>' /><br />
                            <br />
                            <adv:PrintViewLink ID="lnkPrintView" runat="server" ScrMemID='<%#Eval("ScrMemID") %>' />
                        </td>
                    </tr>
                </table>
            </div>
        </EditItemTemplate>
    </asp:FormView>
</asp:Content>
