﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true"
    CodeBehind="ApprovalSummary.aspx.cs" Inherits="Adversary.Web.Admin.ApprovalSummary" Theme="HH" %>

<%@ Register Src="~/Controls/ScreeningMemoSummary.ascx" TagName="ScreeningMemoSummary" TagPrefix="adv" %>
<%@ Register Src="~/Controls/ApprovalTable.ascx" TagName="ApprovalTable" TagPrefix="adv" %>

<asp:Content ID="cntMain" ContentPlaceHolderID="cphMainContent" runat="server">
    <adv:ApprovalTable ID="aptSummary" runat="server" />
    <adv:ScreeningMemoSummary ID="smsView" runat="server" />
</asp:Content>
