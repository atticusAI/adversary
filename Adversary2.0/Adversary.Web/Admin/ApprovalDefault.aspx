﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BaseMaster.Master" AutoEventWireup="true"
    CodeBehind="ApprovalDefault.aspx.cs" Inherits="Adversary.Web.Admin.ApprovalDefault"
    Theme="HH" %>

<asp:Content ID="cntMain" ContentPlaceHolderID="cphMainContent" runat="server">
    <div class="sidebar">
        <div class="menu">
            <h4>
                Holland &amp; Hart New Business Intake Website</h4>
            <dl>
                <dt>
                    <asp:LinkButton ID="lnkScreening" runat="server" PostBackUrl="~/Screening/Default.aspx"
                        Text="Create Screening Memo" />
                </dt>
                <dt>
                    <asp:LinkButton ID="lnkSearch" runat="server" PostBackUrl="~/Admin/ApprovalSearch.aspx"
                        Text="Search Screening Memos" />
                </dt>
                <dt>
                    <asp:LinkButton ID="lnkRequest" runat="server" PostBackUrl="~/Requests/Default.aspx"
                        Text="Adversary Requests" />
                </dt>
                <dt id="dtApproval" runat="server" onprerender="dtApproval_PreRender">
                    <asp:LinkButton ID="lnkApproval" runat="server" PostBackUrl="~/Admin/Queue.aspx"
                        Text="View Your Approval Queue" />
                </dt>
                <dt>
                    <asp:LinkButton ID="lnkLogout" runat="server" PostBackUrl="~/Default.aspx?logout=true"
                        Text="Logout of New Business Intake" />
                </dt>
            </dl>
        </div>
    </div>
</asp:Content>
