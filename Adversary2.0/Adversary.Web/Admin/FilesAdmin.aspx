﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="FilesAdmin.aspx.cs" Inherits="Adversary.Web.Admin.FilesAdmin" Theme="HH" %>

<%@ Register TagPrefix="uc" TagName="FilesControl" Src="~/Controls/FileUpload.ascx" %>
<%@ Register TagPrefix="uc" TagName="ExistingFiles" Src="~/Controls/ExistingFiles.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMainContent" runat="server">
    <asp:Panel ID="pnlMain" runat="server">
        <table width="600">
            <tr>
                <td width="75%" align="left"><strong>Screening Memo #</strong></td>
                <td><strong>
                    <asp:Label ID="lblScreeningMemoNumber" runat="server"></asp:Label></strong></td>
            </tr>
            <tr>
                <td width="75%" align="left"><strong>Submitted On</strong></td>
                <td><strong>
                    <asp:Label ID="lblSubmittedOn" runat="server"></asp:Label></strong></td>
            </tr>
            <tr>
                <td colspan="2">Attach additional files to this Screening Memo
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <uc:ExistingFiles ID="ucExistingFiles" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <uc:FilesControl ID="ucFilesControl" runat="server" FileUploadMode="None" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlError" runat="server" Visible="false">
        <asp:Label id="lblError" runat="server" CssClass="error">No Screening Memo Selected</asp:Label>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLowerContent" runat="server">
</asp:Content>
