﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.Web.Pages;

namespace Adversary.Web.Admin
{
    public partial class AdminMaster : MasterBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.AddIncludes(this.head1);


        }

        protected void tabNav_PreRender(object sender, EventArgs e)
        {
            if (this.Page is TrackBasePage)
            {
                this.tabNav.CurrentUser = ((TrackBasePage)this.Page).CurrentUser;
            }
        }

        public string TabPageUrl
        {
            get
            {
                return this.tabNav.PageUrl;
            }
            set
            {
                this.tabNav.PageUrl = value;
            }
        }

        public string SubTabPageUrl
        {
            get
            {
                return this.subTabNav.PageUrl;
            }
            set
            {
                this.subTabNav.PageUrl = value;
            }
        }

        public void ClearMessage()
        {
            this.pageMessage.ClearMessage();
        }

        public void InfoMessage(string message)
        {
            this.pageMessage.InfoMessage(message);
        }

        public void ErrorMessage(string message)
        {
            this.pageMessage.ErrorMessage(message);
        }

        public void WarnMessage(string message)
        {
            this.pageMessage.WarnMessage(message);
        }
    }
}