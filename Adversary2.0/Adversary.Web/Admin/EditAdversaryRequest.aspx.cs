﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.Web.Pages;
using System.Drawing;
using Adversary.Web.Extensions;

namespace Adversary.Web.Admin
{
    public partial class EditAdversaryRequest : AdminBasePage
    {
        private int? _requestID = null;

        public int RequestID
        {
            get
            {
                if (this._requestID == null)
                {
                    if (!string.IsNullOrWhiteSpace(this.Request.QueryString["RequestID"]))
                    {
                        int i = -1;
                        if (int.TryParse(this.Request.QueryString["RequestID"], out i))
                        {
                            this._requestID = i;
                        }
                    }
                }
                return this._requestID ?? -1;
            }
        }

        private DataLayer.Request _adversaryRequest = null;

        public DataLayer.Request AdversaryRequest
        {
            get
            {
                if (this._adversaryRequest == null)
                {
                    this._adversaryRequest = this.AdversaryDataLayer.GetRequest(this.RequestID);
                }
                return this._adversaryRequest;
            }
        }

        private DataLayer.Admin.EditAdversaryRequest _requestDataLayer = null;

        public DataLayer.Admin.EditAdversaryRequest RequestDataLayer
        {
            get
            {
                if (this._requestDataLayer == null)
                {
                    this._requestDataLayer = new DataLayer.Admin.EditAdversaryRequest();
                }
                return this._requestDataLayer;
            }
        }
        
        protected void lbRemoveComplete_Click(object sender, EventArgs e)
        {
            this.RemoveComplete();
        }
        
        private void RemoveComplete()
        {
            if (this.Page.IsValid)
            {
                this.RequestDataLayer.SetPending(this.RequestID);
            }
        }

        protected void btnComplete_Click(object sender, EventArgs e)
        {
            this.CompleteRequest();
        }

        private void CompleteRequest()
        {
            if (this.Page.IsValid)
            {
                this.AdversaryRequest.CompletionDate = DateTime.Now;
                this.AdversaryRequest.WorkCompletedBy = ((DropDownList)this.frmAdvRequest.FindControl("ddlWorkCompletedBy")).SelectedValue;
                this.UpdateRequest();
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            this.UpdateRequest();
        }

        private void UpdateRequest()
        {
            if (this.Page.IsValid)
            {
                this.AdversaryRequest.AccountingFirm = ((CheckBox)this.frmAdvRequest.FindControl("chkAccounting")).Checked;
                this.AdversaryRequest.AdverseParties = ((TextBox)this.frmAdvRequest.FindControl("txtAdverseParties")).Text;
                this.AdversaryRequest.AttyLogin = ((TextBox)this.frmAdvRequest.FindControl("txtAttyLogin")).Text;
                this.AdversaryRequest.Bankruptcy = ((CheckBox)this.frmAdvRequest.FindControl("chkBackruptcy")).Checked;
                this.AdversaryRequest.ClientCategory = ((DropDownList)this.frmAdvRequest.FindControl("ddlClientCategory")).SelectedValue;
                this.AdversaryRequest.ClientMatter = ((TextBox)this.frmAdvRequest.FindControl("txtClientMatter")).Text;
                this.AdversaryRequest.ClientName = ((TextBox)this.frmAdvRequest.FindControl("txtClientName")).Text;
                this.AdversaryRequest.Communications = ((CheckBox)this.frmAdvRequest.FindControl("chkCommunications")).Checked;
                this.AdversaryRequest.ElectricUtility = ((CheckBox)this.frmAdvRequest.FindControl("chkElectricUtility")).Checked;
                this.AdversaryRequest.Government = ((CheckBox)this.frmAdvRequest.FindControl("chkGovernment")).Checked;
                this.AdversaryRequest.IndianTribe = ((CheckBox)this.frmAdvRequest.FindControl("chkIndianTribe")).Checked;
                this.AdversaryRequest.Insurance = ((CheckBox)this.frmAdvRequest.FindControl("chkInsurance")).Checked;
                this.AdversaryRequest.LawFirm = ((CheckBox)this.frmAdvRequest.FindControl("chkLawFirm")).Checked;
                this.AdversaryRequest.LegalFees = ((CheckBox)this.frmAdvRequest.FindControl("chkLegalFees")).Checked;
                this.AdversaryRequest.NoneOfTheAbove = ((CheckBox)this.frmAdvRequest.FindControl("chkNoneOfTheAbove")).Checked;
                this.AdversaryRequest.OilGas = ((CheckBox)this.frmAdvRequest.FindControl("chkOilGas")).Checked;
                this.AdversaryRequest.OKforNewMatter = ((CheckBox)this.frmAdvRequest.FindControl("chkOKforNewMatter")).Checked;
                this.AdversaryRequest.OpposingCounsel = ((TextBox)this.frmAdvRequest.FindControl("txtOpposingCounsel")).Text;
                this.AdversaryRequest.PartiesToMediationArbitration = ((TextBox)this.frmAdvRequest.FindControl("txtPartiesToMediationArbitration")).Text;
                this.AdversaryRequest.PracticeCode = ((DropDownList)this.frmAdvRequest.FindControl("ddlPracticeCode")).SelectedValue;
                this.AdversaryRequest.RelatedCoDefendants = ((TextBox)this.frmAdvRequest.FindControl("txtRelatedCoDefendants")).Text;
                this.AdversaryRequest.RelatedParties = ((TextBox)this.frmAdvRequest.FindControl("txtRelatedParties")).Text;
                this.AdversaryRequest.SkiResort = ((CheckBox)this.frmAdvRequest.FindControl("chkSkiResort")).Checked;
                this.AdversaryRequest.TypistLogin = ((TextBox)this.frmAdvRequest.FindControl("txtTypistLogin")).Text;
                this.AdversaryRequest.WorkBegunBy = ((DropDownList)this.frmAdvRequest.FindControl("ddlWorkBegunBy")).SelectedValue;
                this.RequestDataLayer.UpdateRequest(this.AdversaryRequest);
            }
        }

        protected void frmAdvRequest_PreRender(object sender, EventArgs e)
        {
            List<DataLayer.Request> requests = new List<DataLayer.Request>();
            if (this.AdversaryRequest != null)
            {
                requests.Add(this.AdversaryRequest);
            }
            this.frmAdvRequest.DataSource = requests;
            this.frmAdvRequest.DataBind();
        }

        protected void ddlPracticeCode_PreRender(object sender, EventArgs e)
        {
            DropDownList ddlPracType = (DropDownList)sender;
            ddlPracType.DataSource = this.AdversaryDataLayer.GetPracticeGroupsList().OrderBy(p => p.cat_plus_desc);
            ddlPracType.DataTextField = "cat_plus_desc";
            ddlPracType.DataValueField = "matt_cat_code";
            ddlPracType.DataBind();
        }

        protected void ddlPracticeCode_DataBound(object sender, EventArgs e)
        {
            DropDownList ddlPracType = (DropDownList)sender;
            if (ddlPracType.Items != null && ddlPracType.Items.Count > 0 && this.AdversaryRequest != null)
            {
                var found = ddlPracType.Items.FindByValue(this.AdversaryRequest.PracticeCode);
                if (found != null)
                {
                    ddlPracType.SelectedValue = this.AdversaryRequest.PracticeCode;
                }
            }
        }

        protected void ddlWork_PreRender(object sender, EventArgs e)
        {
            DropDownList ddlWork = (DropDownList)sender;
            ddlWork.DataSource = this.AdversaryDataLayer.GetAdversaryAdmins().OrderBy(a => a.Login);
            ddlWork.DataTextField = "Login";
            ddlWork.DataValueField = "Login";
            ddlWork.DataBind();
        }

        protected void ddlWorkBegunBy_DataBound(object sender, EventArgs e)
        {
            DropDownList ddlWorkBegunBy = (DropDownList)sender;
            if (ddlWorkBegunBy.Items != null && ddlWorkBegunBy.Items.Count > 0 && this.AdversaryRequest != null)
            {
                var found = ddlWorkBegunBy.Items.FindByValue(this.AdversaryRequest.WorkBegunBy);
                if (found != null)
                {
                    ddlWorkBegunBy.SelectedValue = this.AdversaryRequest.WorkBegunBy;
                }
                ddlWorkBegunBy.Enabled = (this.AdversaryRequest.CompletionDate == null);
            }
        }

        protected void ddlWorkCompletedBy_DataBound(object sender, EventArgs e)
        {
            DropDownList ddlWorkCompletedBy = (DropDownList)sender;
            if (ddlWorkCompletedBy.Items != null && ddlWorkCompletedBy.Items.Count > 0 && this.AdversaryRequest != null)
            {
                var found = ddlWorkCompletedBy.Items.FindByValue(this.AdversaryRequest.WorkCompletedBy);
                if (found != null)
                {
                    ddlWorkCompletedBy.SelectedValue = this.AdversaryRequest.WorkCompletedBy;
                }
                ddlWorkCompletedBy.Enabled = (this.AdversaryRequest.CompletionDate == null);
            }
        }

        protected void ddlClientCategory_DataBound(object sender, EventArgs e)
        {
            DropDownList ddlClientCategory = (DropDownList)sender;
            if (ddlClientCategory.Items != null && ddlClientCategory.Items.Count > 0 && this.AdversaryRequest != null)
            {
                var found = ddlClientCategory.Items.FindByValue(this.AdversaryRequest.ClientCategory);
                if (found != null)
                {
                    ddlClientCategory.SelectedValue = this.AdversaryRequest.ClientCategory;
                }
            }
        }

        protected void valComplete_ServerValidate(object sender, ServerValidateEventArgs e)
        {
            this.pnlErrors.HH_ClearError();
            DropDownList ddlWorkBegunBy = (DropDownList)this.frmAdvRequest.FindControl("ddlWorkBegunBy");
            DropDownList ddlWorkCompletedBy = (DropDownList)this.frmAdvRequest.FindControl("ddlWorkCompletedBy");

            if (string.IsNullOrWhiteSpace(ddlWorkBegunBy.SelectedValue))
            {
                e.IsValid = false;
                ddlWorkBegunBy.HH_SetError(this.pnlErrors);
            }

            if (string.IsNullOrWhiteSpace(ddlWorkCompletedBy.SelectedValue))
            {
                e.IsValid = false;
                ddlWorkCompletedBy.HH_SetError(this.pnlErrors);
            }
        }

        protected void valUpdate_ServerValidate(object sender, ServerValidateEventArgs e)
        {
            this.pnlErrors.HH_ClearError();
            DropDownList ddlClientCategory = (DropDownList)this.frmAdvRequest.FindControl("ddlClientCategory");
            DropDownList ddlPracticeCode = (DropDownList)this.frmAdvRequest.FindControl("ddlPracticeCode");

            if (string.IsNullOrWhiteSpace(ddlClientCategory.SelectedValue))
            {
                e.IsValid = false;
                ddlClientCategory.HH_SetError(this.pnlErrors);
            }

            if (string.IsNullOrWhiteSpace(ddlPracticeCode.SelectedValue))
            {
                e.IsValid = false;
                ddlPracticeCode.HH_SetError(this.pnlErrors);
            }
        }
        
        public override void Dispose()
        {
            if (this._requestDataLayer != null)
            {
                this._requestDataLayer.Dispose();
                this._requestDataLayer = null;
            }
            base.Dispose();
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (this.Page.Master is AdminMaster)
            {
                ((AdminMaster)this.Page.Master).TabPageUrl = "/Admin/AdminDefault.aspx";
                ((AdminMaster)this.Page.Master).SubTabPageUrl = "/Admin/AdminDefault.aspx";
            }
        }
    }
}