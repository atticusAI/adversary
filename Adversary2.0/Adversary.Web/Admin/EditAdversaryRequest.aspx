﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true"
    CodeBehind="EditAdversaryRequest.aspx.cs" Inherits="Adversary.Web.Admin.EditAdversaryRequest"
    Theme="HH" %>

<%@ Register Src="../Controls/HelpLabel.ascx" TagName="HelpLabel" TagPrefix="uc1" %>
<asp:Content ID="cntHead" runat="server" ContentPlaceHolderID="head">
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            formValidation = hh.validateWithjQuery('#form1');

            $('.submitComplete').click(function () {
                formValidation.settings.ignore = '.vgrUpdate';
            });

            $('.submitUpdate').click(function () {
                formValidation.settings.ignore = '.vgrComplete';
            });
        });
    </script>
</asp:Content>
<asp:Content ID="cntMain" ContentPlaceHolderID="cphMainContent" runat="server">
    <asp:FormView ID="frmAdvRequest" runat="server" DefaultMode="Edit" OnPreRender="frmAdvRequest_PreRender">
        <EmptyDataTemplate>
            <span class="error">An adversary request was not selected for editing.</span>
        </EmptyDataTemplate>
        <EditItemTemplate>
            <table>
                <tbody>
                    <tr>
                        <td>
                            <uc1:HelpLabel ID="hlp" runat="server" Text="Status"
                            HelpText="An employee login must be selected in the &quot;Assigned To&quot; and &quot;Work Completed By&quot; drop down lists in order to mark the request as complete. Select an employee and then click the Mark Complete button to complete the request. The status can be switched back to pending by clicking the &quot;Click to remove Completed Status&quot; link when visible." />
                        </td>
                        <td>
                            <asp:PlaceHolder ID="phPending" runat="server" Visible='<%#Eval("CompletionDate") == null%>'>
                                Pending </asp:PlaceHolder>
                            <asp:PlaceHolder ID="phComplete" runat="server" Visible='<%#Eval("CompletionDate") != null%>'>
                                Completed
                                <%#Eval("CompletionDate")%><br />
                                <asp:LinkButton ID="lbRemoveComplete" runat="server" Text="(Click to remove Completed Status)"
                                    OnClick="lbRemoveComplete_Click" OnClientClick="return confirm('This request has already been marked as complete. Are you sure that you want to mark as pending and re-open it?')"
                                    CssClass="cancel" />
                            </asp:PlaceHolder>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Request ID #
                        </td>
                        <td>
                            <span>
                                <%#Eval("ReqID")%></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Submitted On
                        </td>
                        <td>
                            <span>
                                <%#Eval("SubmittalDate")%></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Deadline Date
                        </td>
                        <td>
                            <span>
                                <%#Eval("DeadlineDate")%></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblAttyLogin" runat="server" AssociatedControlID="txtAttyLogin" Text="Attorney Payroll ID" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtAttyLogin" runat="server" Text='<%#Eval("AttyLogin")%>' />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblTypistLogin" runat="server" AssociatedControlID="txtTypistLogin" Text="Submitted By (Payroll ID)" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtTypistLogin" runat="server" Text='<%#Eval("TypistLogin")%>' />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblClientName" runat="server" AssociatedControlID="txtClientName" Text="Client Name" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtClientName" runat="server" Text='<%#Eval("ClientName")%>' TextMode="MultiLine" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc1:HelpLabel ID="hlpClientCategory" runat="server" Text="Client Status Category" 
                                AssociatedControlID="ddlClientCategory"
                                HelpText="Please select whether the client is an existing or potential client." />
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlClientCategory" runat="server" OnDataBound="ddlClientCategory_DataBound"
                                CssClass="required vgrUpdate" title="* Client Status Category must be selected.">
                                <asp:ListItem Text="Please Select a Client Type..." Value="" />
                                <asp:ListItem Text="Potential" Value="Potential" />
                                <asp:ListItem Text="Existing" Value="Existing" />
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblClientMatter" runat="server" AssociatedControlID="txtClientMatter" Text="Client Matter Number (if existing client)" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtClientMatter" runat="server" Text='<%#Eval("ClientMatter")%>' />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblPracticeCode" runat="server" AssociatedControlID="ddlPracticeCode" Text="Practice Type/Specialty Code" />
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlPracticeCode" runat="server" OnPreRender="ddlPracticeCode_PreRender"
                                OnDataBound="ddlPracticeCode_DataBound" CssClass="required vgrUpdate" title="* Practice Type/Speciality Code must be selected." />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblOKforNewMatter" runat="server" AssociatedControlID="chkOKforNewMatter" Text="Keep this adversary request confidential?" />
                        </td>
                        <td>
                            <asp:CheckBox ID="chkOKforNewMatter" runat="server" Checked='<%#Eval("OKForNewMatter") ?? false%>' />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblAdverseParties" runat="server" AssociatedControlID="txtAdverseParties" Text="Adverse Parties" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtAdverseParties" runat="server" TextMode="MultiLine" Text='<%#Eval("AdverseParties")%>' />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblRelatedCoDefendants" runat="server" AssociatedControlID="txtRelatedCoDefendants" Text="Related Co-Defendants" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtRelatedCoDefendants" runat="server" TextMode="MultiLine" Text='<%#Eval("RelatedCoDefendants")%>' />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblRelatedParties" runat="server" AssociatedControlID="txtRelatedParties" Text="Related Parties (not including co-defendants)" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtRelatedParties" runat="server" TextMode="MultiLine" Text='<%#Eval("RelatedParties")%>' />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblPartiesToMediationArbitration" runat="server" AssociatedControlID="txtPartiesToMediationArbitration" Text="Parties to Mediation and Arbitration" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtPartiesToMediationArbitration" runat="server" TextMode="MultiLine"
                                Text='<%#Eval("PartiesToMediationArbitration")%>' />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblOpposingCounsel" runat="server" AssociatedControlID="txtOpposingCounsel" Text="Opposing Counsel" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtOpposingCounsel" runat="server" TextMode="MultiLine" Text='<%#Eval("OpposingCounsel")%>' />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblBankruptcy" runat="server" AssociatedControlID="chkBankruptcy" Text="This is a bankruptcy matter." />
                        </td>
                        <td>
                            <asp:CheckBox ID="chkBankruptcy" runat="server" Checked='<%#Eval("Bankruptcy") ?? false%>' />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblCommunications" runat="server" AssociatedControlID="chkCommunications" Text="This is a telecommunications client or matter." />
                        </td>
                        <td>
                            <asp:CheckBox ID="chkCommunications" runat="server" Checked='<%#Eval("Communications") ?? false%>' />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblInsurance" runat="server" AssociatedControlID="chkInsurance" Text="This client is an insurance company." />
                        </td>
                        <td>
                            <asp:CheckBox ID="chkInsurance" runat="server" Checked='<%#Eval("Insurance") ?? false%>' />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblGovernment" runat="server" AssociatedControlID="chkGovernment" Text="This client is a governmental entity." />
                        </td>
                        <td>
                            <asp:CheckBox ID="chkGovernment" runat="server" Checked='<%#Eval("Government") ?? false%>' />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblElectricUtility" runat="server" AssociatedControlID="chkElectricUtility" Text="This client is an electric utility." />
                        </td>
                        <td>
                            <asp:CheckBox ID="chkElectricUtility" runat="server" Checked='<%#Eval("ElectricUtility") ?? false%>' />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblLegalFees" runat="server" AssociatedControlID="chkLegalFees" Text="This client's legal fees will be paid by an insurance company <em>or</em> otherthird party." />
                        </td>
                        <td>
                            <asp:CheckBox ID="chkLegalFees" runat="server" Checked='<%#Eval("LegalFees") ?? false%>' />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblLawFirm" runat="server" AssociatedControlID="chkLawFirm" Text="This client <em>or</em> adversary is a lawyer or a law firm." />
                        </td>
                        <td>
                            <asp:CheckBox ID="chkLawFirm" runat="server" Checked='<%#Eval("LawFirm") ?? false%>' />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblSkiResort" runat="server" AssociatedControlID="chkSkiResort" Text="This client <em>or</em> adversary is a ski resort." />
                        </td>
                        <td>
                            <asp:CheckBox ID="chkSkiResort" runat="server" Checked='<%#Eval("SkiResort") ?? false%>' />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblIndianTribe" runat="server" AssociatedControlID="chkIndianTribe" Text="This client <em>or</em> adversary is an Indian tribe." />
                        </td>
                        <td>
                            <asp:CheckBox ID="chkIndianTribe" runat="server" Checked='<%#Eval("IndianTribe") ?? false%>' />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblOilGas" runat="server" AssociatedControlID="chkOilGas" Text="This client <em>or</em> adversary is in any segment of the oil and gas industry." />
                        </td>
                        <td>
                            <asp:CheckBox ID="chkOilGas" runat="server" Checked='<%#Eval("OilGas") ?? false%>' />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblAccounting" runat="server" AssociatedControlID="chkAccounting" Text="This adversary is an accounting firm." />
                        </td>
                        <td>
                            <asp:CheckBox ID="chkAccounting" runat="server" Checked='<%#Eval("AccountingFirm") ?? false%>' />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblNoneOfTheAbove" runat="server" AssociatedControlID="chkNoneOfTheAbove" Text="None of the above" />
                        </td>
                        <td>
                            <asp:CheckBox ID="chkNoneOfTheAbove" runat="server" Checked='<%#Eval("NoneOfTheAbove") ?? false%>' />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblWorkBegunBy" runat="server" AssociatedControlID="ddlWorkBegunBy" Text="Assigned To" />
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlWorkBegunBy" runat="server" OnPreRender="ddlWork_PreRender"
                                OnDataBound="ddlWorkBegunBy_DataBound" AppendDataBoundItems="true" title="* Assigned To must be selected to mark this request as complete."
                                CssClass="required vgrComplete">
                                <asp:ListItem Text="(unassigned)" Value="" />
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Work Begun Date
                        </td>
                        <td>
                            <span>
                                <%#Eval("WorkBegunDate")%></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblWorkCompletedBy" runat="server" AssociatedControlID="ddlWorkCompletedBy" Text="Work Completed By" />
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlWorkCompletedBy" runat="server" OnPreRender="ddlWork_PreRender"
                                OnDataBound="ddlWorkCompletedBy_DataBound" AppendDataBoundItems="true" title="* Work Completed By must be selected to mark this request as complete."
                                CssClass="required vgrComplete">
                                <asp:ListItem Text="(unassigned)" Value="" />
                            </asp:DropDownList>
                        </td>
                    </tr>
                </tbody>
            </table>
            <asp:Button ID="btnUpdate" runat="server" Text="Save Work" OnClick="btnUpdate_Click"
                CssClass="submitUpdate" ValidationGroup="update" />
            <asp:CustomValidator ID="valUpdate" runat="server" OnServerValidate="valUpdate_ServerValidate"
                ValidationGroup="update" />
            <asp:Button ID="btnComplete" runat="server" Text="Mark Complete" OnClick="btnComplete_Click"
                Visible='<%#Eval("CompletionDate") == null%>' ValidationGroup="complete" CssClass="submitComplete" />
            <asp:CustomValidator ID="valComplete" runat="server" OnServerValidate="valComplete_ServerValidate"
                ValidationGroup="complete" />
        </EditItemTemplate>
    </asp:FormView>
    <asp:Panel ID="pnlErrors" runat="server" CssClass="errdiv" />
</asp:Content>
