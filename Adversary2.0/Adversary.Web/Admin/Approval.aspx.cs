﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.Web.Controls;
using Adversary.Web.Pages;
using Adversary.DataLayer;
using Adversary.DataLayer.HRDataService;
using System.Text;
using System.Net.Mail;
using System.Web.Services;
using AjaxControlToolkit;
using Adversary.Web.Extensions;
using System.Configuration;
using Adversary.Screening;
using Adversary.DataLayer.AdversaryDataService;
using System.Web.UI.HtmlControls;

namespace Adversary.Web.Admin
{
    public partial class Approval : AdminBasePage
    {
        /// <summary>
        /// scrub user messages they will be reloaded
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.ClearMessage();
        }

        /// <summary>
        /// run rules against this memo
        /// </summary>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (ThisMemo != null)
            {
                if (this.HasRuleViolations)
                {
                    base.ErrorSummary.ErrorList.AddRange(this.ViolatedRules);
                    base.ErrorSummary.DisplayMessage = "This screening memo has errors that require attention.";
                    base.ErrorSummary.DisplayType = Web.Controls.MessageDisplayType.DisplayError;
                }
            }
        }

        /// <summary>
        /// list of violated rules, not for human consumption
        /// </summary>
        private List<string> _violatedRules = null;

        /// <summary>
        /// lazily loaded list of violated rules, use this one
        /// </summary>
        public List<string> ViolatedRules
        {
            get
            {
                if (this._violatedRules == null)
                {
                    this._violatedRules = this.RunRules();
                }
                return this._violatedRules;
            }
        }

        /// <summary>
        /// rule runner, not for human consumption
        /// </summary>
        /// <returns>list of rules violations</returns>
        private List<string> RunRules()
        {
            List<string> errorMessages = new List<string>();
            MemoRuleEngine memoRule = new MemoRuleEngine(ThisMemo);
            memoRule.RunRules();
            if (memoRule.HasErrors)
            {
                errorMessages.AddRange(memoRule.RuleViolations.Select(r => r.RuleText));
            }
            return errorMessages;
        }

        /// <summary>
        /// true when rule violations for the current memo exist
        /// </summary>
        public bool HasRuleViolations
        {
            get
            {
                return (this.ViolatedRules.Count > 0);
            }
        }

        /// <summary>
        /// does the memo have a tracking record
        /// </summary>
        /// <returns>true when memo has been submitted else false</returns>
        private bool HasTrackingRecord()
        {
            return (ThisMemo.Trackings != null && ThisMemo.Trackings.Count > 0);
        }

        /// <summary>
        /// has the retainer acknowledge checkbox been ticked
        /// </summary>
        /// <param name="o">the tracking object from the gridview</param>
        /// <returns></returns>
        protected bool RetainerAcknowledged(object o)
        {
            return ((Tracking)o).RetainerAcknowledged ?? false;
        }

        protected bool IsRejectedMemo(object o)
        {
            return (Convert.ToInt32(((Tracking)o).StatusTypeID) == TrackingStatusType.Rejected);
        }

        protected bool IsLockedMemo(object o)
        {
            return ((Tracking)o).Locked ?? false;
        }

        protected bool ShowTRARApproval(object o)
        {
            Tracking trk = (Tracking)o;
            return ((trk.ARApprovalReceived ?? false) || RequiresARApproval(ThisMemo.ClientNumber));
        }

        private bool RequiresARApproval(string clientNumber)
        {
            bool required = false;
            using (Adversary.DataLayer.ARTable dal = new DataLayer.ARTable())
            {
                ARBalance bal = dal.GetClientARBalance(clientNumber);
                if (bal != null)
                {
                    required = (bal.Days_91_to_120 > 0) || (bal.Days_121_Plus > 0);
                }
            }
            return required;
        }

        /// <summary>
        /// 
        /// </summary>
        protected string GetOfficeCode(Memo memo)
        {
            string officeCode = "";
            FirmOffice office = ((AdminBasePage)Page).AdversaryDataLayer.GetAttorneyOffice(memo.RespAttID);
            if (office == null)
            {
                officeCode = ThisMemo.OrigOffice;
            }
            else
            {
                officeCode = office.LocationCode;
            }
            return officeCode;
        }

        /// <summary>
        /// TODO: replace this with OfficeAttributes
        /// TODO: move to business layer
        /// </summary>
        private bool? _isDenver = null;

        private bool IsDenver
        {
            get
            {
                if (this._isDenver == null)
                {
                    this._isDenver = this.IsDenverMemo();
                }
                return this._isDenver ?? false;
            }
        }        
        
        protected bool IsDenverMemo()
        {
            FirmOffice office = this.ApprovalDataLayer.GetFirmOffice(GetOfficeCode(ThisMemo));
            return office == null ? false : (office.City == "Denver");
        }

        protected string GetRetainerLabel()
        {
            return (!string.IsNullOrEmpty(ThisMemo.RetainerDescYes)) 
                ? "Approval for retainer less than $5000." 
                : "Approval for no retainer fee.";
        }

        protected bool RequiresRetainerApproval()
        {
            return (ThisMemo.ScrMemType == 1 || ThisMemo.ScrMemType == 2) &&
                    (!(ThisMemo.Retainer ?? false) || !string.IsNullOrEmpty(ThisMemo.RetainerDescYes));
        }

        protected string GetAPStatus(object o)
        {
            Tracking tr = (Tracking)o;
            string status = "No response by AP";
            if (tr.APApproved ?? false)
            {
                status = "Approved by AP";
            }
            else if (tr.APAcknowledged ?? false)
            {
                status = "Acknowledged by AP";
            }
            return status;
        }

        protected string GetPGLStatus(object o)
        {
            Tracking tr = (Tracking)o;
            string status = "No response by PGL";
            if (tr.PGMApproved ?? false)
            {
                status = "Approved by PGL";
            }
            else if (tr.PGMAcknowledged ?? false)
            {
                status = "Acknowledged by PGL";
            }
            return status;
        }

        protected string GetADVStatus(object o)
        {
            Tracking tr = (Tracking)o;
            string status = "";
            if (tr.StatusTypeID == TrackingStatusType.Completed)
            {
                status = "Processing Completed";
            }
            else if (tr.AdversaryAdminApproved ?? false)
            {
                if (tr.AdversaryAcknowledged ?? false)
                {
                    status = "Adversary Group Processing";
                }
                else
                {
                    status = "Approved for Processing";
                }
            }
            else if (tr.AdversaryAdminAcknowledged ?? false)
            {
                status = "Acknowledged by Adversary";
            }
            return status;
        }

        protected string GetMatterName(object o)
        {
            string name = "";
            Tracking tr = (Tracking)o;
            if (ThisMemo.ScrMemType == 6)
            {
                name = "REJECTED MATTER";
            }
            else
            {
                name = ThisMemo.MatterName;
            }

            if (!string.IsNullOrWhiteSpace(Convert.ToString(tr.SubmittedOn)))
            {
                name += string.Format("<br />( Submitted:&nbsp;&nbsp;{0} )", Convert.ToString(tr.SubmittedOn));
            }
            return name;
        }

        private void SetSelectedStatus()
        {
            SetSelectedStatus((DropDownList)frmApproval.FindControl("ddlStatus"));
        }

        private void SetSelectedStatus(DropDownList ddlStatus)
        {
            Tracking tr = (Tracking)frmApproval.DataItem;
            string statusTypeID = Convert.ToString(tr.StatusTypeID);
            SetSelectedStatus(ddlStatus, statusTypeID);
        }

        private void SetSelectedStatus(DropDownList ddlStatus, string statusTypeID)
        {
            ListItem itm = (from ListItem i in ddlStatus.Items
                            where i.Value == statusTypeID
                            select i).FirstOrDefault();
            if (itm != null)
            {
                ddlStatus.SelectedValue = itm.Value;
            }
        }

        protected void spnMemo_PreRender(object sender, EventArgs e)
        {
            if (ThisMemo != null)
            {
                ((HtmlGenericControl)sender).InnerHtml = this.AdversaryDataLayer.GetMemoTypeDescription(ThisMemo) +
                    " (Memo # " + ThisMemo.ScrMemID + ")<br />";
            }
        }

        /// <summary>
        /// change the message in the empty data template based on current memo state
        /// </summary>
        protected void spnEmpty_PreRender(object sender, EventArgs e)
        {
            if (ThisMemo == null)
            {
                ((HtmlGenericControl)sender).InnerText = "No screening memo selected.";
            }
            else 
            {
                ((HtmlGenericControl)sender).InnerText = "The approval screen is not available for this memo, the memo has not been submitted.";
            }
            
        }

        protected void arBal_PreRender(object sender, EventArgs e)
        {
            Web.Controls.ARTable arTable = (Web.Controls.ARTable)sender;
            if (ThisMemo != null && !string.IsNullOrWhiteSpace(ThisMemo.ClientNumber))
            {
                arTable.ClientNumber = ThisMemo.ClientNumber;
                arTable.Visible = true;
            }
            else
            {
                arTable.ClientNumber = "-1";
                arTable.Visible = false;
            }
        }

        protected void frmApproval_PreRender(object sender, EventArgs e)
        {
            if (ThisMemo != null && ThisMemo.Trackings != null)
            {
                ((FormView)sender).DataSource = ThisMemo.Trackings;
            }
            ((FormView)sender).DataBind();
        }

        /// <summary>
        /// bind up the satus drop down lists
        /// </summary>
        protected void ddlStatus_PreRender(object sender, EventArgs e)
        {
            DropDownList ddlStatus = (DropDownList)sender;
            ddlStatus.DataSource = TrackingStatusType.TrackingStatusTypes;
            ddlStatus.DataTextField = "Status";
            ddlStatus.DataValueField = "StatusTypeID";
            ddlStatus.DataBind();
            SetSelectedStatus(ddlStatus);
        }


        protected void lblLocked_PreRender(object sender, EventArgs e)
        {
            ((Label)sender).Visible = ((CheckBox)frmApproval.FindControl("cbLocked")).Checked;
        }

        protected void btnAPApprove_PreRender(object sender, EventArgs e)
        {
            Button btnAPApprove = (Button)sender;
            Tracking tr = (Tracking)frmApproval.DataItem;
            if (tr.APApproved ?? false)
            {
                btnAPApprove.Text = "Remove AP Approval";
                btnAPApprove.CommandName = "remove";
                btnAPApprove.OnClientClick = "this.value='Removing approval, please wait';";
            }
            else
            {
                btnAPApprove.Text = "Approve";
                btnAPApprove.CommandName = "add";
                btnAPApprove.OnClientClick = "this.value='Approving, please wait';";
            }
        }

        protected void btnPGMApprove_PreRender(object sender, EventArgs e)
        {
            Button btnPGMApprove = (Button)sender;
            Tracking tr = (Tracking)frmApproval.DataItem;
            if (tr.PGMApproved ?? false)
            {
                btnPGMApprove.Text = "Remove PGL Approval";
                btnPGMApprove.CommandName = "remove";
                btnPGMApprove.OnClientClick = "this.value='Removing approval, please wait';";
            }
            else
            {
                btnPGMApprove.Text = "Approve";
                btnPGMApprove.CommandName = "add";
                btnPGMApprove.OnClientClick = "this.value='Approving, please wait';";
            }
        }

        protected void btnAdvAdminApprove_PreRender(object sender, EventArgs e)
        {
            Button btnAdvAdminApprove = (Button)sender;
            Tracking tr = (Tracking)frmApproval.DataItem;
            btnAdvAdminApprove.Text = "Approve for Processing";
            btnAdvAdminApprove.CommandName = "add";
            btnAdvAdminApprove.OnClientClick = "this.value='Approving, please wait';";

            if (tr.AdversaryAdminApproved ?? false)
            {
                btnAdvAdminApprove.Text = "Remove Adversary Approval";
                btnAdvAdminApprove.CommandName = "remove";
                btnAdvAdminApprove.OnClientClick = "this.value='Removing approval, please wait';";
            }

            btnAdvAdminApprove.Visible = this.CurrentUser.IsAdversary && !IsSubmittedToAdversary() && !IsComplete();
            btnAdvAdminApprove.Enabled = this.CurrentUser.IsAdversaryAdmin;
        }

        protected void lbSend_PreRender(object sender, EventArgs e)
        {
            if (ThisMemo != null)
            {
                ((LinkButton)sender).Text = (ThisMemo.ClientMatterNumbers != null && ThisMemo.ClientMatterNumbers.Count > 0)
                    ? "Send Client Matter Numbers to Originating Party"
                    : "Send Status Update to Orignating Party";
            }
        }

        protected void pnlReject_PreRender(object sender, EventArgs e)
        {
            if (ThisMemo != null && ThisMemo.Trackings != null && ThisMemo.Trackings.Count > 0)
            {
                this.trAPRejectionReason.Visible = ShowAPRejectionReason(ThisMemo.Trackings[0]);
                this.tbAPRejectionReason.Enabled = this.CurrentUser.IsAP;
                this.tbAPRejectionReason.Text = ThisMemo.Trackings[0].APRejectionReason;
                this.trPGMRejectionReason.Visible = ShowPGMRejectionReason(ThisMemo.Trackings[0]);
                this.tbPGMRejectionReason.Enabled = this.CurrentUser.IsPGM;
                this.tbPGMRejectionReason.Text = ThisMemo.Trackings[0].PGMRejectionReason;
                this.trAdvRejectionReason.Visible = ShowAdversaryRejectionReason(ThisMemo.Trackings[0]);
                this.tbAdvRejectionReason.Enabled = this.CurrentUser.IsAdversary;
                this.tbAdvRejectionReason.Text = ThisMemo.Trackings[0].AdversaryRejectionReason;
            }
        }

        protected bool IsPGMEnabled()
        {
            return !IsSubmittedToAdversary() && !IsComplete() && (this.CurrentUser.IsPGM || this.CurrentUser.IsAdversary);
        }

        protected bool IsAPEnabled()
        {
            return !IsSubmittedToAdversary() && !IsComplete() && (this.CurrentUser.IsAP || this.CurrentUser.IsAdversary);
        }

        protected bool IsSubmittedToAdversary()
        {
            Tracking tr = (Tracking)frmApproval.DataItem;
            return (tr.StatusTypeID == TrackingStatusType.AdversaryGroupProcessing);
        }

        protected bool IsComplete()
        {
            Tracking tr = (Tracking)frmApproval.DataItem;
            return ((tr.StatusTypeID == TrackingStatusType.Completed)
                || ((tr.StatusTypeID == TrackingStatusType.Rejected) && (!(tr.RejectionHold ?? false)))
                );
        }

        /// <summary>
        /// inject the current memo into the approver combo boxen
        /// </summary>
        protected void approverCombo_PreRender(object sender, EventArgs e)
        {
            ((Web.Controls.ApproverComboBox)sender).ThisMemo = ThisMemo;
        }

        /// <summary>
        /// bind up the client matter list
        /// </summary>
        protected void cmList_PreRender(object sender, EventArgs e)
        {
            LoadCMList((ListContainerControl)sender);
        }

        /// <summary>
        /// rebind the client matter list when a row is clicked
        /// </summary>
        protected void cmList_RowCommand(object sender, EventArgs e)
        {
            ListContainerControl cmList = (ListContainerControl)frmApproval.FindControl("cmList");
            cmList.DataSource = ThisMemo.ClientMatterNumbers;
        }

        /// <summary>
        /// add a new client matter
        /// </summary>
        protected void cmList_AddClick(object sender, EventArgs e)
        {
            ListContainerControl cmList = (ListContainerControl)frmApproval.FindControl("cmList");
            TextBox txtClientMatterNumber = (TextBox)cmList.FindAddControl("txtClientMatterNumber");
            this.ApprovalDataLayer.AddClientMatterNumber(ThisMemo.ScrMemID, txtClientMatterNumber.Text);
            this.UpdateTracking(ThisMemo.Trackings[0]);
            this.InfoMessage("Added a new client matter number to this memo.");
            this.LoadCMList(cmList);
        }

        /// <summary>
        /// do the data binding on the client matter list
        /// </summary>
        private void LoadCMList(ListContainerControl cmList)
        {
            cmList.DataSource = ThisMemo.ClientMatterNumbers;
            cmList.DataBind();
        }

        /// <summary>
        /// delete item from cm list
        /// </summary>
        protected void cmList_DeleteClick(object sender, EventArgs e)
        {
            ListContainerControl cmList = (ListContainerControl)frmApproval.FindControl("cmList");
            HiddenField hidClientMatterID = (HiddenField)cmList.FindDeleteControl("hidClientMatterID");
            int clientMatterID = Convert.ToInt32(hidClientMatterID.Value);
            this.ApprovalDataLayer.DeleteClientMatterNumber(clientMatterID);
            this.UpdateTracking(ThisMemo.Trackings[0]);
            this.WarnMessage("Deleted a client matter number from this memo.");
            this.LoadCMList(cmList);
        }

        /// <summary>
        /// send status update link click
        /// </summary>
        protected void btnStatus_Click(object sender, EventArgs e)
        {
            Tracking trk = ThisMemo.Trackings[0];
            DropDownList ddlStatus = (DropDownList)frmApproval.FindControl("ddlStatus");
            this.ApprovalDataLayer.UpdateTrackingStatus(trk, Convert.ToInt32(ddlStatus.SelectedValue));
            this.UpdateTracking(trk);
            this.InfoMessage("Status updated to \"" + ddlStatus.SelectedItem.Text + "\"");
        }

        /// <summary>
        /// check/uncheck of the rejection hold checkbox
        /// </summary>
        protected void cbRejectionHold_CheckedChanged(object sender, EventArgs e)
        {
            Tracking trk = ThisMemo.Trackings[0];
            CheckBox cbRejectionHold = (CheckBox)frmApproval.FindControl("cbRejectionHold");
            trk.RejectionHold = cbRejectionHold.Checked;
            this.UpdateTracking(trk);
            this.InfoMessage("A modification request hold for this memo has been " + ((cbRejectionHold.Checked) ? "applied" : "removed"));
        }


        /// <summary>
        /// lock unlock the memo
        /// </summary>
        protected void cbLocked_CheckedChanged(object sender, EventArgs e)
        {
            Tracking trk = ThisMemo.Trackings[0];
            CheckBox cbLocked = (CheckBox)frmApproval.FindControl("cbLocked");
            trk.Locked = cbLocked.Checked;
            this.UpdateTracking(trk);
            this.InfoMessage("This memo has been " + ((cbLocked.Checked) ? "Locked" : "Unlocked"));
        }

        /// <summary>
        /// handle approval combo changes
        /// </summary>
        protected void combo_SelectedIndexChanged(object sender, EventArgs e)
        {
            Tracking trk = ThisMemo.Trackings[0];
            Controls.ApproverComboBox combo = (Controls.ApproverComboBox)sender;

            string msg = string.Empty;
            bool unassigned = combo.SelectedValue == "unassigned";
            int? userID = (unassigned) ? null : (int?)Convert.ToInt32(combo.SelectedValue);

            switch (combo.ApproverComboBoxType)
            {
                case ApproverComboBoxType.AdministrativePartner:
                    this.ApprovalDataLayer.UpdateAPAcknowledged(trk, userID, !unassigned);
                    msg = (unassigned) ? "Administrative Partner Assignment Removed" : "Assigned to Administrative Partner " + combo.SelectedItem.Text;
                    break;
                case ApproverComboBoxType.PracticeGroupLeader:
                    this.ApprovalDataLayer.UpdatePGMAcknowledged(trk, userID, !unassigned);
                    msg = (unassigned) ? "Practice Group Leader Assignment Removed" : "Assigned to Practice Group Leader " + combo.SelectedItem.Text;
                    break;
                case ApproverComboBoxType.AdversaryGroup:
                    this.ApprovalDataLayer.UpdateADVAcknowledged(trk, userID, !unassigned, !unassigned & this.CurrentUser.IsAdversaryAdmin);
                    msg = (unassigned) ? "Adversary Group Assignment Removed" : "Assigned to Adversary Group Member " + combo.SelectedItem.Text;
                    break;
            }
            this.UpdateTracking(trk);
            this.InfoMessage(msg);
        }

        /// <summary>
        /// check / uncheck retainer approval 
        /// </summary>
        protected void cbRetainer_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cbRetainer = (CheckBox)sender;
            Tracking trk = ThisMemo.Trackings[0];
            trk.RetainerAcknowledged = cbRetainer.Checked;
            this.UpdateTracking(trk);
            this.InfoMessage("You have " + ((cbRetainer.Checked) ? "added" : "removed") + " approval for this memo's retainer.");
        }

        /// <summary>
        /// handle approve/unapprove button clicks
        /// </summary>
        protected void btnApproval_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            bool approving = (btn.CommandName == "add");
            Tracking trk = ThisMemo.Trackings[0];

            // exit the approval process if retainer has not been acknowledged
            if (approving && RequiresRetainerApproval() && (btn.CommandArgument == "AP" || btn.CommandArgument == "PGM") &&
                (!(trk.RetainerAcknowledged ?? false)))
            {
                string msg = "You have not approved the retainer for this memo. " +
                             "Please review all retainer information on the memo " +
                             "summary page and check the \"" + GetRetainerLabel() + "\" " +
                             "box when finished";
                this.ErrorMessage(msg);
            }
            else
            {
                if (btn.CommandName == "add")
                {
                    this.Approve(btn.CommandArgument, trk);
                    string msg = "You have added approval for this memo.";
                    if (trk.StatusTypeID == TrackingStatusType.AdversaryGroupProcessing)
                    {
                        if (this.NotifyAdversary())
                        {
                            msg += " The adversary group has been notified that this memo is ready for processing.";
                        }
                    }
                    this.InfoMessage(msg);
                }
                else if (btn.CommandName == "remove")
                {
                    this.UnApprove(btn.CommandArgument, trk);
                    this.WarnMessage("You have removed approval for this memo.");
                }
            }
        }


        /// <summary>
        /// throw emails at adversary users
        /// TODO: move to business layer
        /// </summary>
        /// <returns>true if email sent successfully else false</returns>
        private bool NotifyAdversary()
        {
            //send email notifications to adv group as necessary
            bool messageSent = false;
            try
            {
                Emailer.SendApproversEmail(ThisMemo, BaseURL);
                messageSent = true;
            }
            catch (Exception x)
            {
                HandleError(x);
            }
            return messageSent;
        }

        /// <summary>
        /// handle approval business rules
        /// TODO: move to business layer
        /// </summary>
        /// <param name="approvalType">who is approving</param>
        /// <param name="trk">the tracking record for the memo</param>
        private void Approve(string approvalType, Tracking trk)
        {
            int? userID = null;
            switch (approvalType)
            {
                case "AP":
                    Controls.ApproverComboBox acbAP = (Controls.ApproverComboBox)frmApproval.FindControl("acbAP");
                    if (trk.APUserID == null &&  acbAP.Items.FindByValue(this.CurrentUser.AdminLogin.UserID.ToString()) != null)
                    {
                        userID = this.CurrentUser.AdminLogin.UserID;
                        acbAP.SelectedValue = this.CurrentUser.AdminLogin.UserID.ToString();
                    }
                    trk.APUserID = userID;
                    trk.APAcknowledged = true;
                    trk.APApproved = true;
                    trk.APDate = DateTime.Now;
                    trk.APSignature = this.CurrentUser.AdminLogin.Login.ToUpper();
                    break;

                case "PGM":
                    Controls.ApproverComboBox acbPGM = (Controls.ApproverComboBox)frmApproval.FindControl("acbPGM");
                    if (trk.PGMUserID == null && acbPGM.Items.FindByValue(this.CurrentUser.AdminLogin.UserID.ToString()) != null)
                    {
                        userID = this.CurrentUser.AdminLogin.UserID;
                        acbPGM.SelectedValue = this.CurrentUser.AdminLogin.UserID.ToString();
                    }
                    trk.PGMUserID = userID;
                    trk.PGMAcknowledged = true;
                    trk.PGMApproved = true;
                    trk.PGMDate = DateTime.Now;
                    trk.PGMSignature = this.CurrentUser.AdminLogin.Login.ToUpper();
                    break;

                case "AdvAdmin":
                    trk.RejectionHold = null;
                    trk.AdversaryAdminAcknowledged = true;
                    trk.AdversaryAdminApproved = true;
                    trk.AdversaryAdminDate = DateTime.Now;
                    trk.AdversarySignature = this.CurrentUser.AdminLogin.Login.ToUpper();
                    break;
            }

            trk.StatusTypeID = this.GetNewStatusOnApproval(trk);
            this.UpdateTracking(trk);
        }

        /// <summary>
        /// get the new status for a memo after approvals happen
        /// TODO: move to business layer
        /// </summary>
        /// <param name="trk">the tracking record for the memo</param>
        /// <returns>the new status</returns>
        private int? GetNewStatusOnApproval(Tracking trk)
        {
            int? statusTypeID = trk.StatusTypeID;
            if (IsRejectedMemo(trk) && (trk.AdversaryAdminApproved ?? false))
            {
                statusTypeID = TrackingStatusType.AdversaryGroupProcessing;
            }
            else if (this.IsDenver && (trk.PGMApproved ?? false) && (trk.AdversaryAdminApproved ?? false))
            {
                statusTypeID = TrackingStatusType.AdversaryGroupProcessing;
            }
            else if (!this.IsDenver && (trk.APApproved ?? false) && (trk.PGMApproved ?? false) && (trk.AdversaryAdminApproved ?? false))
            {
                statusTypeID = TrackingStatusType.AdversaryGroupProcessing;
            }
            return statusTypeID;
        }

        /// <summary>
        /// handle unapproval business rules
        /// TODO: move to business layer
        /// </summary>
        /// <param name="approvalType"></param>
        /// <param name="trk"></param>
        private void UnApprove(string approvalType, Tracking trk)
        {
            int? userID = null;
            switch (approvalType)
            {
                case "AP":
                    Controls.ApproverComboBox acbAP = (Controls.ApproverComboBox)frmApproval.FindControl("acbAP");
                    if (trk.APUserID == null && acbAP.Items.FindByValue(this.CurrentUser.AdminLogin.UserID.ToString()) != null)
                    {
                        userID = this.CurrentUser.AdminLogin.UserID;
                        acbAP.SelectedValue = this.CurrentUser.AdminLogin.UserID.ToString();
                    }
                    trk.APUserID = userID;
                    trk.APAcknowledged = true;
                    trk.APApproved = false;
                    trk.APDate = null;
                    trk.APSignature = null;
                    break;

                case "PGM":
                    Controls.ApproverComboBox acbPGM = (Controls.ApproverComboBox)frmApproval.FindControl("acbPGM");
                    if (trk.PGMUserID == null && acbPGM.Items.FindByValue(this.CurrentUser.AdminLogin.UserID.ToString()) != null)
                    {
                        userID = this.CurrentUser.AdminLogin.UserID;
                        acbPGM.SelectedValue = this.CurrentUser.AdminLogin.UserID.ToString();
                    }
                    trk.PGMUserID = userID;
                    trk.PGMAcknowledged = true;
                    trk.PGMApproved = false;
                    trk.PGMDate = null;
                    trk.PGMSignature = null;
                    break;

                case "AdvAdmin":
                    trk.RejectionHold = null;
                    trk.AdversaryAdminAcknowledged = true;
                    trk.AdversaryAdminApproved = false;
                    trk.AdversaryAdminDate = null;
                    trk.AdversarySignature = null;
                    break;
            }
            
            trk.StatusTypeID = this.GetNewStatusOnUnApproval(trk);
            this.UpdateTracking(trk);
        }

        /// <summary>
        /// get the new status for a memo after unapprovals happen
        /// TODO: move to business layer
        /// </summary>
        /// <param name="trk"></param>
        /// <returns></returns>
        private int? GetNewStatusOnUnApproval(Tracking trk)
        {
            int? statusTypeID = trk.StatusTypeID;
            if (IsRejectedMemo(trk))
            {
                statusTypeID = TrackingStatusType.AdversaryGroupProcessing;
            }
            else if (this.IsDenver && !(trk.PGMApproved ?? false))
            {
                statusTypeID = TrackingStatusType.SubmittedForApproval;
            }
            else if (!this.IsDenver && (!(trk.APApproved ?? false) && !(trk.PGMApproved ?? false)))
            {
                statusTypeID = TrackingStatusType.SubmittedForApproval;
            }
            else
            {
                statusTypeID = TrackingStatusType.SubmittedForApproval;
            }
            return statusTypeID;
        }

        protected bool IsAdversaryAdmin()
        {
            return this.CurrentUser.IsAdversaryAdmin;
        }

        protected bool IsAdversary()
        {
            return this.CurrentUser.IsAdversary;
        }

        protected bool ShowAPRejectionReason(object o)
        {
            Tracking trk = (Tracking)o;
            return ((this.CurrentUser.IsAP || !String.IsNullOrWhiteSpace(trk.APRejectionReason)) && ShowAP(o));
        }

        protected bool ShowAP(object o)
        {
            return (!IsRejectedMatter() && !this.IsDenver);
        }

        protected bool IsRejectedMatter()
        {
            return (ThisMemo.ScrMemType == 6);
        }

        protected bool ShowPGMRejectionReason(object o)
        {
            Tracking trk = (Tracking)o;
            return ((this.CurrentUser.IsPGM || !string.IsNullOrWhiteSpace(trk.PGMRejectionReason)) && ShowPGM(o));
        }

        protected bool ShowPGM(object o)
        {
            return (!IsRejectedMatter());
        }

        protected bool ShowAdversaryRejectionReason(object o)
        {
            Tracking trk = (Tracking)o;
            return (this.CurrentUser.IsAdversary || !string.IsNullOrWhiteSpace(trk.AdversaryRejectionReason));
        }

        protected bool ShowClientMatterNumberStatus(object o)
        {
            Tracking tracking = (Tracking)o;
            return (ThisMemo.ClientMatterNumbers.Count > 0 || tracking.CMNumbersSentDate.HasValue);
        }

        /// <summary>
        /// get the client matter number sent status
        /// </summary>
        /// <param name="o">the tracking record</param>
        /// <returns>html snippett of the status</returns>
        protected string GetClientMatterNumberStatus(object o)
        {
            string cmStatus = "";
            Tracking tracking = (Tracking)o;
            if (!tracking.CMNumbersSentDate.HasValue || tracking.CMNumbersSentDate.Value == DateTime.MinValue)
            {
                cmStatus = "<span class=\"error\">Client Matter Numbers Have Not Been Sent</span>";
            }
            else
            {
                cmStatus = string.Format("<span class=\"good\">Client Matter Numbers Sent: {0}</span>", tracking.CMNumbersSentDate);
            }
            return cmStatus;
        }

        /// <summary>
        /// get the current user name for the from email address
        /// </summary>
        protected void lblFrom_PreRender(object sender, EventArgs e)
        {
            ((Label)sender).Text = this.CurrentUser.LoginEmailAddress;
        }
        
        /// <summary>
        /// handle "modification request" clicks
        /// </summary>
        protected void btnRejectionSubmit_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(this.tbAPRejectionReason.Text) 
                || !string.IsNullOrWhiteSpace(this.tbPGMRejectionReason.Text)
                || !string.IsNullOrWhiteSpace(this.tbAdvRejectionReason.Text))
            {
                Tracking trk = ThisMemo.Trackings[0];
                string userName = this.Page.User.Identity.Name.HH_StripDomain();
                string userNameLine = string.Format("\r\n{0}\r\n{1}", userName, DateTime.Now.ToString());

                trk.APRejectionReason = (this.CurrentUser.IsAP) ? this.tbAPRejectionReason.Text + userNameLine : trk.APRejectionReason;
                trk.PGMRejectionReason = (this.CurrentUser.IsPGM) ? this.tbPGMRejectionReason.Text + userNameLine : trk.PGMRejectionReason;
                trk.AdversaryRejectionReason = (this.CurrentUser.IsAdversary) ? this.tbAdvRejectionReason.Text + userNameLine : trk.AdversaryRejectionReason;
                trk.StatusTypeID = TrackingStatusType.ModificationRequested;
                trk.RejectionHold = this.cbModalRejectionHold.Checked;
                trk.AdversaryUserID = (this.CurrentUser.IsAdversary) ?  (int?)this.CurrentUser.AdminLogin.UserID : null;
                this.UpdateTracking(trk);

                this.Emailer.SendStatusEmail(ThisMemo, BaseURL, userName);
                this.InfoMessage("This memo requires modification and notification has been sent. Status updated to \"Modification Requested\".");
            }
            else
            {
                if (this.trAdvRejectionReason.Visible == true)
                {
                    this.tbAdvRejectionReason.HH_SetError(pnlRejectErrors);
                }

                if (this.trAPRejectionReason.Visible == true)
                {
                    this.tbAPRejectionReason.HH_SetError(pnlRejectErrors);
                }

                if (this.trPGMRejectionReason.Visible == true)
                {
                    this.tbPGMRejectionReason.HH_SetError(pnlRejectErrors);
                }
                this.mpxReject.Show();
            }
        }

        /// <summary>
        /// handle email ar for approval clicks
        /// </summary>
        protected void btnARSubmit_Click(object sender, EventArgs e)
        {
            MailAddress fromAddr = null;
            MailAddressCollection toAddresses = new MailAddressCollection();
            bool success = true;

            if (string.IsNullOrWhiteSpace(this.tbTo.Text))
            {
                success = false;
                this.tbTo.HH_SetError(pnlARErrors);
            }

            if (string.IsNullOrWhiteSpace(this.tbSubject.Text))
            {
                success = false;
                this.tbSubject.HH_SetError(pnlARErrors);
            }

            if (string.IsNullOrWhiteSpace(this.tbARApprovalEmail.Text))
            {
                success = false;
                this.tbARApprovalEmail.HH_SetError(pnlARErrors);
            }

            try
            {
                fromAddr = new MailAddress(this.lblFrom.Text, this.lblFrom.Text);
            }
            catch (Exception x)
            {
                success = false;
                this.btnARSubmit.HH_SetError(pnlARErrors, 
                    string.Format("* A/R Approval Request failed. From Address ({0}) is not a valid email address.", this.lblFrom.Text));
            }
            
            foreach (string recipient in tbTo.Text.Split(';'))
            {
                try
                {
                    if (string.IsNullOrWhiteSpace(recipient))
                    {
                        continue;
                    }
                    toAddresses.Add(new MailAddress(recipient));
                }
                catch (Exception x)
                {
                    success = false;
                    this.tbTo.HH_SetError(pnlARErrors, 
                        string.Format("* A/R Approval Request failed. To Address ({0}) is not a valid email address.", recipient));
                }
            }
            
            if (!success)
            {
                mpxAR.Show();
            }
            else 
            {
                this.Emailer.SendEmail(toAddresses, fromAddr, tbSubject.Text, tbARApprovalEmail.Text, false);
                this.InfoMessage("A/R Approval Request Sent");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected void btnComplete_PreRender(object sender, EventArgs e)
        {
            ((Button)sender).OnClientClick = string.Format("showMPX('{0}'); return false;", this.msgComplete.BehaviorID);
        }

        /// <summary>
        /// complete the memo
        /// inform the user
        /// rebind the data
        /// </summary>
        protected void msgComplete_OKClick(object sender, EventArgs e)
        {
            Tracking trk = ThisMemo.Trackings[0];
            trk.StatusTypeID = TrackingStatusType.Completed;
            this.UpdateTracking(trk);
            this.InfoMessage("This memo has been marked as completed.");
        }

        /// <summary>
        /// send status update click
        /// </summary>
        protected void lbSend_Click(object sender, EventArgs e)
        {
            Tracking trk = ThisMemo.Trackings[0];
            trk.CMNumbersSentDate = DateTime.Now;
            this.UpdateTracking(trk);
            this.HandleStatusNotification(ThisMemo);
        }

        /// <summary>
        /// handle ar approval check/uncheck
        /// </summary>
        protected void cbARApproval_CheckedChanged(object sender, EventArgs e)
        {
            Tracking trk = ThisMemo.Trackings[0];
            trk.ARApprovalReceived = ((CheckBox)sender).Checked;
            this.UpdateTracking(trk);
            if (((CheckBox)sender).Checked)
            {
                this.InfoMessage("Approval received for A/R over 90 days.");
            }
            else
            {
                this.WarnMessage("Removed approval for A/R over 90 days.");
            }
        }

        public int TrackingID
        {
            get
            {
                int trackingID = -1;
                HiddenField hidTrackingID = (HiddenField)this.frmApproval.FindControl("hidTrackingID");
                if (hidTrackingID != null)
                {
                    trackingID = Convert.ToInt32(hidTrackingID.Value);
                }
                return trackingID;
            }
        }

        public string Notes
        {
            get
            {
                string notes = "";
                TextBox tbNotes = (TextBox)this.frmApproval.FindControl("tbNotes");
                if (tbNotes != null)
                {
                    notes = tbNotes.Text;
                }
                return notes;
            }
        }

        public string Conflicts
        {
            get
            {
                string conflicts = "";
                TextBox tbConflicts = (TextBox)this.frmApproval.FindControl("txtConflicts");
                if (tbConflicts != null)
                {
                    conflicts = tbConflicts.Text;
                }
                return conflicts;
            }
        }

        public string FinalCheck
        {
            get
            {
                string finalcheck = "";
                TextBox tbFinalCheck = (TextBox)this.frmApproval.FindControl("txtFinalCheck");
                if (tbFinalCheck != null)
                {
                    finalcheck = tbFinalCheck.Text;
                }
                return finalcheck;
            }
        }

        public string FeeSplits
        {
            get
            {
                string feesplits = "";
                TextBox tbFeeSplits = (TextBox)this.frmApproval.FindControl("txtFeeSplits");
                if (tbFeeSplits != null)
                {
                    feesplits = tbFeeSplits.Text;
                }
                return feesplits;
            }
        }

        public bool APRetainerAcknowledged
        {
            get
            {
                bool chk = false;
                CheckBox cbAPRetainerAcknowledged = (CheckBox)frmApproval.FindControl("cbAPRetainerAcknowledged");
                if (cbAPRetainerAcknowledged != null)
                {
                    chk = cbAPRetainerAcknowledged.Checked;
                }
                return chk;
            }
        }

        public bool PGMRetainerAcknowledged
        {
            get
            {
                bool chk = false;
                CheckBox cbPGMRetainerAcknowledged = (CheckBox)frmApproval.FindControl("cbPGMRetainerAcknowledged");
                if (cbPGMRetainerAcknowledged != null)
                {
                    chk = cbPGMRetainerAcknowledged.Checked;
                }
                return chk;
            }
        }

        public bool? ARApproval
        {
            get
            {
                bool arApproval = false;
                CheckBox cbArApproval = (CheckBox)frmApproval.FindControl("cbARApproval");
                if (cbArApproval != null)
                {
                    arApproval = cbArApproval.Checked;
                }
                return arApproval;
            }
        }

        public string APARSignature
        {
            get
            {
                string apARSignature = null;
                TextBox txtAPARSignature = (TextBox)frmApproval.FindControl("txtAPARSignature");
                if (txtAPARSignature != null)
                {
                    apARSignature = txtAPARSignature.Text;
                }
                return apARSignature;
            }
        }

        public string APExceptionSignature
        {
            get
            {
                string apExceptionSignature = null;
                TextBox txtAPExceptionSignature = (TextBox)frmApproval.FindControl("txtAPExceptionSignature");
                if (txtAPExceptionSignature != null)
                {
                    apExceptionSignature = txtAPExceptionSignature.Text;
                }
                return apExceptionSignature;
                
            }
        }

        public string APNotes
        {
            get
            {
                string apNotes = null;
                TextBox txtAPNotes = (TextBox)frmApproval.FindControl("txtAPNotes");
                if (txtAPNotes != null)
                {
                    apNotes = txtAPNotes.Text;
                }
                return apNotes;
            }
        }

        public string PGMARSignature
        {
            get
            {
                string pgmARSignature = null;
                TextBox txtPGMARSignature = (TextBox)frmApproval.FindControl("txtPGMARSignature");
                if (txtPGMARSignature != null)
                {
                    pgmARSignature = txtPGMARSignature.Text;
                }
                return pgmARSignature;
            }
        }

        public string PGMExceptionSignature
        {
            get
            {
                string pgmExceptionSignature = null;
                TextBox txtPGMExceptionSignature = (TextBox)frmApproval.FindControl("txtPGMExceptionSignature");
                if (txtPGMExceptionSignature != null)
                {
                    pgmExceptionSignature = txtPGMExceptionSignature.Text;
                }
                return pgmExceptionSignature;
            }
        }

        public string PGMNotes
        {
            get
            {
                string pgmNotes = null;
                TextBox txtPGMNotes = (TextBox)frmApproval.FindControl("txtPGMNotes");
                if (txtPGMNotes != null)
                {
                    pgmNotes = txtPGMNotes.Text;
                }
                return pgmNotes;
            }
        }

        public string Opened
        {
            get
            {
                string opened = null;
                TextBox txtOpened = (TextBox)frmApproval.FindControl("txtOpened");
                if (txtOpened != null)
                {
                    opened = txtOpened.Text;
                }
                return opened;
            }
        }

        private void UpdateTracking(Tracking tracking)
        {
            base.UpdateTracking(tracking, this);
        }

        /// <summary>
        /// set tab/subtab page names
        /// </summary>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (this.Page.Master is AdminMaster)
            {
                ((AdminMaster)this.Page.Master).TabPageUrl = "/Admin/ApprovalDefault.aspx";
                ((AdminMaster)this.Page.Master).SubTabPageUrl = "/Admin/ApprovalDefault.aspx";
            }
        }

        protected void pnlAR_PreRender(object sender, EventArgs e)
        {
            if (ThisMemo != null)
            {
                tbTo.Text = string.Empty;
                List<ARApprovalUser> recipients = AdversaryDataLayer.GetARApprovalUsers();
                ARBalance bal = new ARBalance();
                using (Adversary.DataLayer.ARTable dal = new DataLayer.ARTable())
                {
                    bal = dal.GetClientARBalance(ThisMemo.ClientNumber);
                }

                foreach (ARApprovalUser user in recipients)
                {
                    tbTo.Text += user.EmailAddress + ";";
                }
                tbSubject.Text = "A/R Approval Required";
                StringBuilder sb = new StringBuilder();
                sb.AppendLine();
                sb.AppendLine();
                sb.AppendLine("Additional approval required for client with A/R past 90 days:");
                sb.AppendLine();
                try
                {
                    sb.AppendLine("Client: " + AdversaryDataLayer.SearchClients(ThisMemo.ClientNumber).First().ClientName);
                }
                catch (Exception ex)
                {
                    sb.AppendLine("***Client name could not be determined");
                }
                sb.AppendLine();
                sb.AppendLine("Current:            $" + bal.Current.ToString("0.00"));
                sb.AppendLine("31  to 60 Days:     $" + bal.Days_31_to_60.ToString("0.00"));
                sb.AppendLine("61 to 90 Days:      $" + bal.Days_61_to_90.ToString("0.00"));
                sb.AppendLine("91 to 120 Days:     $" + bal.Days_91_to_120.ToString("0.00"));
                sb.AppendLine("Over 121 Days:      $" + bal.Days_121_Plus.ToString("0.00"));
                sb.AppendLine("Total A/R:          $" + bal.TotalAR.ToString("0.00"));
                sb.AppendLine();
                if (!String.IsNullOrWhiteSpace(this.Notes.Trim()))
                {
                    sb.AppendLine("Notes:");
                    sb.AppendLine();
                    sb.AppendLine(this.Notes);
                    sb.AppendLine();
                }
                tbARApprovalEmail.Text = sb.ToString();
            }
        }
    }
}