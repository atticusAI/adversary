﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.Web.Pages;

namespace Adversary.Web.Admin
{
    public partial class ApprovalSummary : AdminBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.smsView.ThisMemo = ThisMemo;
            this.aptSummary.Visible = this.TrackingVisible();
            this.aptSummary.ThisMemo = ThisMemo;
        }

        protected bool TrackingVisible()
        {
            bool show = false;
            if (ThisMemo != null && ThisMemo.Trackings != null && ThisMemo.Trackings.Count > 0)
            {
                show = ThisMemo.Trackings[0].StatusTypeID != 0;
            }
            return show;
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (this.Page.Master is AdminMaster)
            {
                ((AdminMaster)this.Page.Master).TabPageUrl = "/Admin/ApprovalDefault.aspx";
                ((AdminMaster)this.Page.Master).SubTabPageUrl = "/Admin/ApprovalDefault.aspx";
            }
        }
    }
}