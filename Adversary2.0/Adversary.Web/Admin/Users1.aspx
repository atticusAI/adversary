﻿<%@ Page Theme="HH" Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="Users1.aspx.cs" Inherits="Adversary.Web.Admin.Users1" %>
<%@ Register TagPrefix="uc" TagName="ListContainerControl" Src="~/Controls/ListContainerControl.ascx" %>
<%@ Register TagPrefix="uc" TagName="PersonAutoComplete" Src="~/Controls/PersonAutoComplete.ascx" %>
<%@ Register TagPrefix="advcontrols" Namespace="Adversary.Controls" Assembly="Adversary.Controls" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMainContent" runat="server">
    <script type="text/javascript">
        $('document').ready(function () {
            $(".pglCheckbox").change(function () {
                $(".trPracticeGroups").toggle();
            });
            $(".trPracticeGroups").hide();
        });
    </script>
    <uc:ListContainerControl ID="ucUsers" runat="server" OnPreRender="ucUsers_PreRender" OnRowDataBound="ucUserAccounts_RowDataBound"
        AutoGenerateColumns="false" OnAddClick="ucUsers_AddClick" PopupCssClass="pop" BackgroundCssClass="pop-back">
        <Columns>
            <asp:BoundField HeaderText="User Name" DataField="Login" />
            <asp:BoundField HeaderText="Email" DataField="Email" />
            <asp:BoundField HeaderText="Office" /><%--will be an int, convert to office name--%>
            <asp:BoundField HeaderText="Adversary Group" /><%--will show a checkbox and indicate membership in the adversary group --%>
            <asp:BoundField HeaderText="AP" /><%--will indicate whether the users is an AP, and put a (P) for primary or (S) for secondary. Fed off IsAP/IsPrimaryAP fields. --%>
            <asp:BoundField HeaderText="PGL" /><%--like the AP field, will indicate whether the user is a PGL (no primary or secondary here, that's determined by practice code) --%>
            <asp:BoundField HeaderText="Practice Groups" />
            <%--this field will show all the practice groups (name followed by code) and whether the user is a primary or secondary approver on the practice code. --%>
        </Columns>
        <InsertItemTemplate>
            <table border="0" cellpadding="2" cellspacing="3">
                <tr>
                    <td>
                        New User:
                    </td>
                    <td>
                        <uc:PersonAutoComplete ID="ucEmployee" runat="server" ServiceMethod="TimekeeperService" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Office:
                    </td>
                    <td>
                        <advcontrols:OfficeDropDown ID="advOfficeDropDown" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td valign="top">Choose Role:</td>
                    <td valign="top">
                        <table>
                            <tr>
                                <td>User is in Adversary Group?</td>
                                <td>
                                    <asp:CheckBox ID="cbIsAdversary" runat="server" AutoPostBack="false" />&nbsp;Yes
                                </td>
                                <td>
                                    Is Adversary Administrator?
                                    <asp:CheckBox ID="cbIsAdversaryAdmin" runat="server" AutoPostBack="false" />&nbsp;Yes
                                </td>
                            </tr>
                            <tr>
                                <td>User is AP?</td>
                                <td>
                                    <asp:CheckBox ID="cbIsAP" runat="server" AutoPostBack="false" />&nbsp;Yes
                                </td>
                                <td>
                                    AP Type:&nbsp;<asp:DropDownList ID="ddlAPType" runat="server">
                                    <asp:ListItem Text="Primary" /><asp:ListItem Text="Secondary" /></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>User is PGL?</td>
                                <td>
                                    <asp:CheckBox ID="cbIsPGL" runat="server" CssClass="pglCheckbox" />&nbsp;Yes
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="trPracticeGroups">
                    <td>
                        Choose Practice Codes:
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td valign="top">
                                    Primary Practice Codes:
                                </td>
                                <td>
                                    <asp:ListBox ID="lbPracticeGroupsPrimary" runat="server" Rows="15" SelectionMode="Multiple"></asp:ListBox>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    Secondary Practice Codes:
                                </td>
                                <td>
                                    <asp:ListBox ID="lbPracticeGroupsSecondary" runat="server" Rows="15" SelectionMode="Multiple"></asp:ListBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </InsertItemTemplate>    
    </uc:ListContainerControl>
        
</asp:Content>
