﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.Web.Pages;

namespace Adversary.Web.Admin
{
    public partial class ApprovalSearch : AdminBasePage
    {
        Adversary.DataLayer.ScreeningSearch _dataLayer = null;
        Adversary.DataLayer.ScreeningSearch DataLayer
        {
            get
            {
                if (_dataLayer == null)
                {
                    _dataLayer = new DataLayer.ScreeningSearch();
                }
                return _dataLayer;
            }
        }

        public override void Dispose()
        {
            if (this._dataLayer != null)
            {
                _dataLayer.Dispose();
            }
            base.Dispose();
            GC.SuppressFinalize(this);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.pnlSearch.Visible == true)
            {
                smlSearch.ObjectInstance = DataLayer;
                smlSearch.DataBind();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            this.pnlSearch.Visible = true;
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admin/ApprovalSearch.aspx");
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (this.Page.Master is AdminMaster)
            {
                ((AdminMaster)this.Page.Master).TabPageUrl = "/Admin/ApprovalDefault.aspx";
                ((AdminMaster)this.Page.Master).SubTabPageUrl = "/Admin/ApprovalDefault.aspx";
            }
        }
    }
}