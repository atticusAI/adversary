﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.Web.Pages;
using Adversary.Web.Controls;
using Adversary.DataLayer;
using Adversary.Screening;

namespace Adversary.Web.Admin
{
    public partial class AdminProcessing : AdminBasePage
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (ThisMemo != null)
            {
                if (this.HasRuleViolations)
                {
                    base.ErrorSummary.ErrorList.AddRange(this.ViolatedRules);
                    base.ErrorSummary.DisplayMessage = "This screening memo has errors that require attention.";
                    base.ErrorSummary.DisplayType = Web.Controls.MessageDisplayType.DisplayError;
                }
                else
                {
                    Tracking trk = (ThisMemo.Trackings == null || ThisMemo.Trackings.Count <= 0) ? null : ThisMemo.Trackings[0];
                    if (trk != null && trk.StatusTypeID == TrackingStatusType.Completed)
                    {
                        base.ErrorSummary.DisplayMessage = "This memo has been processed and completed!";
                        base.ErrorSummary.DisplayType = Web.Controls.MessageDisplayType.DisplayValid;
                    }
                }
            }
        }

        public int LockedScrMemID
        {
            get
            {
                int lockedScrMemID = -1;
                HiddenField hidLockedScrMemID = (HiddenField)this.frmAdminProc.FindControl("hidLockedScrMemID");
                if (hidLockedScrMemID != null)
                {
                    if (!Int32.TryParse(hidLockedScrMemID.Value, out lockedScrMemID))
                    {
                        lockedScrMemID = -1;
                    }
                }
                return lockedScrMemID;
            }
        }

        private List<string> _violatedRules = null;

        public List<string> ViolatedRules
        {
            get 
            {
                if (this._violatedRules == null)
                {
                    this._violatedRules = this.RunRules();
                }
                return this._violatedRules;
            }
        }

        public bool HasRuleViolations
        {
            get
            {
                return (this.ViolatedRules.Count > 0);
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (this.Page.Master is AdminMaster)
            {
                ((AdminMaster)this.Page.Master).TabPageUrl = "/Admin/AdminDefault.aspx";
                ((AdminMaster)this.Page.Master).SubTabPageUrl = "/Admin/AdminDefault.aspx";
            }
        }

        private List<string> RunRules()
        {
            List<string> errorMessages = new List<string>();
            MemoRuleEngine memoRule = new MemoRuleEngine(ThisMemo);
            memoRule.RunRules();
            if (memoRule.HasErrors)
            {
                errorMessages.AddRange(memoRule.RuleViolations.Select(r => r.RuleText));
            }
            return errorMessages;
        }

        protected void approverCombo_PreRender(object sender, EventArgs e)
        {
            ((Adversary.Web.Controls.ApproverComboBox)sender).ThisMemo = ThisMemo;
        }

        protected void apprTable_PreRender(object sender, EventArgs e)
        {
            ((Adversary.Web.Controls.ApprovalTable)sender).ThisMemo = ThisMemo;
        }

        protected void btnComplete_Click(object sender, EventArgs e)
        {
            if (ThisMemo.Trackings != null && ThisMemo.Trackings.Count > 0)
            {
                Tracking tracking = ThisMemo.Trackings[0];
                tracking.StatusTypeID = TrackingStatusType.Completed;
                this.UpdateTracking(tracking);
                Response.Redirect("~/Admin/AdminQueue.aspx");
            }
            this.scrLock.RenewExpriration();
        }

        private void UpdateTracking(Tracking tracking)
        {
            tracking.Notes = ((TextBox)frmAdminProc.FindControl("tbNotes")).Text;
            tracking.Opened = ((TextBox)frmAdminProc.FindControl("tbOpened")).Text;
            tracking.Conflicts = ((TextBox)frmAdminProc.FindControl("tbConflicts")).Text;
            tracking.FeeSplits = ((TextBox)frmAdminProc.FindControl("tbFeeSplits")).Text;
            tracking.FinalCheck = ((TextBox)frmAdminProc.FindControl("tbFinalCheck")).Text;
            this.ApprovalDataLayer.UpdateTracking(tracking);
            this.LoadMemo(ThisMemo.ScrMemID);
            this.scrLock.RenewExpriration();
        }

        protected void lbSend_Click(object sender, EventArgs e)
        {
            if (ThisMemo.Trackings != null && ThisMemo.Trackings.Count > 0)
            {
                Tracking tracking = ThisMemo.Trackings[0];
                tracking.CMNumbersSentDate = DateTime.Now;
                this.UpdateTracking(tracking);
                this.HandleStatusNotification(ThisMemo);
            }
            this.scrLock.RenewExpriration();
        }

        protected void approverCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ThisMemo.Trackings != null && ThisMemo.Trackings.Count > 0)
            {
                Tracking tracking = ThisMemo.Trackings[0];
                Adversary.Web.Controls.ApproverComboBox combo = (Adversary.Web.Controls.ApproverComboBox)sender;
                bool unassigned = combo.SelectedValue == "unassigned";
                int? adversaryUserID = (unassigned) ? null : (int?)Int32.Parse(combo.SelectedValue);
                tracking.AdversaryUserID = adversaryUserID;
                tracking.AdversaryAcknowledged = !unassigned;
                string msg = (unassigned) ? "Adversary Group Assignment Removed" : "Assigned to Adversary Group Member " + combo.SelectedItem.Text;
                this.InfoMessage(msg);
                this.UpdateTracking(tracking);
            }
            else
            {
                this.ErrorMessage("Tracking record not found. No change was made.");
            }
            this.scrLock.RenewExpriration();
        }

        protected void frmAdminProc_PreRender(object sender, EventArgs e)
        {
            List<Memo> memos = new List<Memo>();
            if (ThisMemo != null)
            {
                memos.Add(ThisMemo);
            }
            this.frmAdminProc.DataSource = memos;
            this.frmAdminProc.DataBind();
        }

        protected void cmList_AddClick(object sender, EventArgs e)
        {
            ListContainerControl cmList = (ListContainerControl)frmAdminProc.FindControl("cmList");
            TextBox txtClientMatterNumber = (TextBox)cmList.FindAddControl("txtClientMatterNumber");
            this.ApprovalDataLayer.AddClientMatterNumber(ThisMemo.ScrMemID, txtClientMatterNumber.Text);
            this.InfoMessage("Added a new client matter number to this memo.");
            this.UpdateTracking(ThisMemo.Trackings[0]);
            this.LoadCMList(cmList);
        }

        protected void cmList_ChangeClick(object sender, EventArgs e)
        {
            ListContainerControl cmList = (ListContainerControl)frmAdminProc.FindControl("cmList");
            HiddenField hidClientMatterID = (HiddenField)cmList.FindChangeControl("hidClientMatterID");
            TextBox txtClientMatterNumber = (TextBox)cmList.FindChangeControl("txtClientMatterNumber");
            int clientMatterID = Convert.ToInt32(hidClientMatterID.Value);
            this.ApprovalDataLayer.UpdateClientMatterNumber(clientMatterID, txtClientMatterNumber.Text);
            this.InfoMessage("Updated a client matter number on this memo.");
            this.UpdateTracking(ThisMemo.Trackings[0]);
            this.LoadCMList(cmList);
        }

        protected void cmList_RowCommand(object sender, EventArgs e)
        {
            ListContainerControl cmList = (ListContainerControl)frmAdminProc.FindControl("cmList");
            cmList.DataSource = ThisMemo.ClientMatterNumbers;
        }

        protected void cmList_PreRender(object sender, EventArgs e)
        {
            LoadCMList((ListContainerControl)sender);
        }

        private void LoadCMList(ListContainerControl cmList)
        {
            cmList.DataSource = ThisMemo.ClientMatterNumbers;
            cmList.DataBind();
        }

        protected void cmList_DeleteClick(object sender, EventArgs e)
        {
            ListContainerControl cmList = (ListContainerControl)frmAdminProc.FindControl("cmList");
            HiddenField hidClientMatterID = (HiddenField)cmList.FindDeleteControl("hidClientMatterID");
            int clientMatterID = Convert.ToInt32(hidClientMatterID.Value);
            this.ApprovalDataLayer.DeleteClientMatterNumber(clientMatterID);
            this.WarnMessage("Deleted a client matter number from this memo.");
            this.UpdateTracking(ThisMemo.Trackings[0]);
            this.LoadCMList(cmList);
        }

        protected string GetMatterName(object o)
        {
            Memo memo = (Memo)o;
            string matterName = (memo.ScrMemType == 6) ? "MATTER REQUIRES MODIFICATION" : memo.MatterName;
            if (memo.Trackings != null && memo.Trackings.Count > 0 && memo.Trackings[0].SubmittedOn.HasValue)
            {
                matterName += " <span style=\"color: #68849F;\">(Submitted: " + memo.Trackings[0].SubmittedOn.Value.ToString() + ")</span>";
            }
            return matterName;
        }

        protected int GetTrackingID(object o)
        {
            Memo memo = (Memo)o;
            int trackingID = -1;
            if (memo.Trackings != null && memo.Trackings.Count > 0)
            {
                trackingID = memo.Trackings[0].TrackingID;
            }
            return trackingID;
        }

        public int TrackingID
        {
            get
            {
                int trackingID = -1;
                HiddenField hidTrackingID = (HiddenField)this.frmAdminProc.FindControl("hidTrackingID");
                if (hidTrackingID != null)
                {
                    trackingID = Convert.ToInt32(hidTrackingID.Value);
                }
                return trackingID;
            }
        }

        public string Notes
        {
            get
            {
                string notes = "";
                TextBox tbNotes = (TextBox)this.frmAdminProc.FindControl("tbNotes");
                if (tbNotes != null)
                {
                    notes = tbNotes.Text;
                }
                return notes;
            }
        }

        protected string GetClientDisplayName(object o)
        {
            return ((AdminBasePage)Page).GetClientDisplayName((DataLayer.Memo)o);
        }

        protected string GetEnteringAttorneyName(object o)
        {
            return ((AdminBasePage)Page).GetEnteringAttorneyName((DataLayer.Memo)o);
        }

        protected string GetNotes(object o)
        {
            string notes = "";
            Memo memo = (Memo)o;
            if (memo.Trackings != null && memo.Trackings.Count > 0)
            {
                notes = memo.Trackings[0].Notes;
            }
            return notes;
        }

        protected string GetConfidential(object o)
        {
            string confidential = "No";
            Memo memo = (Memo)o;
            if (memo.Confidential ?? false)
            {
                confidential = "<span class=\"error\">THIS MEMO IS CONFIDENTIAL!</span>";
            }
            return confidential;
        }

        protected string GetApproval(object o)
        {
            string approval = "";
            Memo memo = (Memo)o;
            if (memo.Trackings != null && memo.Trackings.Count > 0)
            {
                Tracking tracking = memo.Trackings[0];
                if (!(tracking.AdversaryAdminApproved ?? false))
                {
                    approval = "<span class=\"error\">Not Approved</span>";
                }
                else
                {
                    approval = string.Format("<span class=\"good\">{0} ({1})</span>",
                        tracking.AdversarySignature,
                        tracking.AdversaryAdminDate);
                }
            }
            return approval;
        }

        protected string GetOpened(object o)
        {
            string opened = "";
            Memo memo = (Memo)o;
            if (memo.Trackings != null && memo.Trackings.Count > 0)
            {
                opened = memo.Trackings[0].Opened;
            }
            return opened;
        }

        protected string GetConflicts(object o)
        {
            string conflicts = "";
            Memo memo = (Memo)o;
            if (memo.Trackings != null && memo.Trackings.Count > 0)
            {
                conflicts = memo.Trackings[0].Conflicts;
            }
            return conflicts;
        }

        protected string GetFeeSplits(object o)
        {
            string feeSplits = "";
            Memo memo = (Memo)o;
            if (memo.Trackings != null && memo.Trackings.Count > 0)
            {
                feeSplits = memo.Trackings[0].FeeSplits;
            }
            return feeSplits;
        }

        protected string GetFinalCheck(object o)
        {
            string finalCheck = "";
            Memo memo = (Memo)o;
            if (memo.Trackings != null && memo.Trackings.Count > 0)
            {
                finalCheck = memo.Trackings[0].FinalCheck;
            }
            return finalCheck;
        }

        protected string GetClientMatterNumberStatus(object o)
        {
            string cmStatus = "";
            Memo memo = (Memo)o;
            if (memo.Trackings != null && memo.Trackings.Count > 0)
            {
                Tracking tracking = memo.Trackings[0];
                if (!tracking.CMNumbersSentDate.HasValue || tracking.CMNumbersSentDate.Value == DateTime.MinValue)
                {
                    cmStatus = "<span class=\"error\">Client Matter Numbers Have Not Been Sent</span>";
                }
                else
                {
                    cmStatus = string.Format("<span class=\"good\">Client Matter Numbers Sent: {0}</span>", tracking.CMNumbersSentDate);
                }
            }
            return cmStatus;
        }

        protected bool ShowClientMatterNumberStatus(object o)
        {
            bool show = false;
            Memo memo = (Memo)o;
            if (memo.Trackings != null && memo.Trackings.Count > 0)
            {
                Tracking tracking = memo.Trackings[0];
                show = (memo.ClientMatterNumbers.Count > 0 || tracking.CMNumbersSentDate.HasValue);
            }
            return show;
        }

        protected string GetSendButtonText(object o)
        {
            Memo memo = (Memo)o;
            return (memo.ClientMatterNumbers != null && memo.ClientMatterNumbers.Count > 0) 
                ? "Send Client Matter Numbers to Originating Party" 
                : "Send Status Update to Orignating Party";
        }

        protected string GetApprovalUrl(object o)
        {
            Memo memo = (Memo)o;
            return string.Format("~/Admin/Approval.aspx?ScrMemID={0}", memo.ScrMemID);
        }

        protected bool GetCompleteEnabled(object o)
        {
            Memo memo = (Memo)o;
            Tracking trk = (memo.Trackings == null || memo.Trackings.Count <= 0) ? null : memo.Trackings[0];
            bool enabled = true;
            
            if (HasRuleViolations)
            {
                enabled = false;
            }

            if (trk != null && trk.StatusTypeID == TrackingStatusType.Completed)
            {
                enabled = false;
            }

            if (trk != null && !(trk.AdversaryAdminApproved ?? false))
            {
                enabled = false;
            }
            
            return enabled;
        }

        protected void arBal_PreRender(object sender, EventArgs e)
        {
            Web.Controls.ARTable arTable = (Web.Controls.ARTable)sender;
            if (ThisMemo != null && !string.IsNullOrWhiteSpace(ThisMemo.ClientNumber))
            {
                arTable.ClientNumber = ThisMemo.ClientNumber;
                arTable.Visible = true;
            }
            else
            {
                arTable.ClientNumber = "-1";
                arTable.Visible = false;
            }
        }


    }
}