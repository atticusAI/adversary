﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true"
    CodeBehind="Impersonate.aspx.cs" Inherits="Adversary.Web.Admin.Impersonate" Theme="HH" %>

<%@ Register Src="../Controls/AutoCompleteTextbox.ascx" TagName="AutoCompleteTextbox"
    TagPrefix="uc1" %>
<asp:Content ID="cntHead" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            formValidation = hh.validateWithjQuery('#form1');
        });
    </script>
</asp:Content>
<asp:Content ID="cntMain" ContentPlaceHolderID="cphMainContent" runat="server">
    <asp:Panel ID="pnlApprover" runat="server" DefaultButton="btnView">
        <table>
            <tr>
                <td colspan="2">
                    Type the last name of the approver that you would like to view:
                </td>
            </tr>
            <tr>
                <td style="vertical-align: top">
                    <uc1:AutoCompleteTextbox ID="actApprover" runat="server" ServiceMethod="AdminLoginAutoCompleteService"
                        CssClass="search required" title="* You must select an approver name." />
                </td>
                <td style="vertical-align: top">
                    <asp:Button ID="btnView" runat="server" Text="Show Queue" OnClick="btnView_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Panel ID="pnlErrs" runat="server" CssClass="errdiv">
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:CustomValidator ID="valPage" runat="server" OnServerValidate="valPage_ServerValidate" />
</asp:Content>
