﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.Web.Pages;
using System.Reflection;
using System.Text;
using System.Web.Script.Serialization;

namespace Adversary.Web.Admin
{
    public partial class AdminQueue : AdminBasePage
    {

        public class IDMashup
        {
            public int ScrMemID
            {
                get;
                set;
            }

            public int TrackingID
            {
                get;
                set;
            }
        }

        private List<DataLayer.Memo> _adversaryQueue = null;

        public List<DataLayer.Memo> AdversaryQueue
        {
            get
            {
                if (this._adversaryQueue == null)
                {
                    this._adversaryQueue = this.QueueDataLayer.GetAdversaryQueue();
                }
                return this._adversaryQueue;
            }
        }

        private Adversary.DataLayer.Queue _queueDataLayer = null;

        public Adversary.DataLayer.Queue QueueDataLayer
        {
            get
            {
                if (this._queueDataLayer == null)
                {
                    this._queueDataLayer = new DataLayer.Queue();
                }
                return this._queueDataLayer;
            }
        }

        public override void Dispose()
        {
            if (this._queueDataLayer != null)
            {
                this._queueDataLayer.Dispose();
                this._queueDataLayer = null;
            }

            base.Dispose();
        }

        protected void lblCount_PreRender(object sender, EventArgs e)
        {
            if (this.AdversaryQueue == null || this.AdversaryQueue.Count == 0)
            {
                this.lblCount.Text = "Your queue is currently empty!";
            }
            else
            {
                this.lblCount.Text = string.Format("{0} Memo(s) Ready for Processing", this.AdversaryQueue.Count);
            }
        }

        protected void gvResults_PreRender(object sender, EventArgs e)
        {
            ((GridView)sender).DataSource = this.AdversaryQueue;
            ((GridView)sender).DataBind();
        }

        protected void gvResults_RowCommand(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "View")
            {
                JavaScriptSerializer ser = new JavaScriptSerializer();
                IDMashup ids = (IDMashup)ser.Deserialize(Convert.ToString(e.CommandArgument), typeof(IDMashup));

                this.ApprovalDataLayer.SaveADVAcknowledged(ids.TrackingID,
                    this.CurrentUser.AdminLogin.UserID,
                    true);

                Response.Redirect(string.Format("~/Admin/AdminProcessing.aspx?ScrMemID={0}", ids.ScrMemID));
            }
        }

        protected void refQueue_Refresh(object sender, EventArgs e)
        {
            this.lblRefresh.Visible = true;
            this.lblRefresh.Text = string.Format("Your queue was last refreshed {0}<br />", DateTime.Now.ToLongTimeString());
        }

        protected string GetFeeSplit(object o)
        {
            string splits = "N";
            DataLayer.Memo memo = (DataLayer.Memo)o;

            if (memo.FeeSplitStaffs != null && memo.FeeSplitStaffs.Count > 0)
            {
                splits = "Y";
            }

            return splits;
        }


        protected string GetTrackingID(object o)
        {
            IDMashup ids = new IDMashup()
            {
                TrackingID = -1,
                ScrMemID = -1
            };

            DataLayer.Memo memo = (DataLayer.Memo)o;
            if (memo.Trackings != null && memo.Trackings.Count > 0)
            {
                ids.TrackingID = memo.Trackings[0].TrackingID;
                ids.ScrMemID = memo.ScrMemID;
            }

            JavaScriptSerializer ser = new JavaScriptSerializer();
            return ser.Serialize(ids);
        }

        protected string GetEnteringAttorneyName(object o)
        {
            return ((AdminBasePage)Page).GetEnteringAttorneyName((DataLayer.Memo)o);
        }

        protected string GetPersonnelName(object o)
        {
            return ((AdminBasePage)Page).GetPersonnelName((DataLayer.Memo)o);
        }

        protected string GetClientDisplayName(object o)
        {
            return ((AdminBasePage)Page).GetClientDisplayName((DataLayer.Memo)o);
        }

        protected string GetCMNumbersSentDate(object o)
        {
            return ((DataLayer.Memo)o).Trackings[0].CMNumbersSentDate.ToString();
        }

        protected string GetAPApproval(object o)
        {
            return this.GetTrackingStatus((DataLayer.Memo)o, "AP");
        }

        protected string GetPGMApproval(object o)
        {
            return this.GetTrackingStatus((DataLayer.Memo)o, "PGM");
        }

        protected string GetAdversaryAdminApproval(object o)
        {
            return this.GetTrackingStatus((DataLayer.Memo)o, "AdversaryAdmin");
        }

        /// <summary>
        /// TODO: FS 3.5 crazy but it works, please refactor the database schema to make more sense
        /// </summary>
        private string GetTrackingStatus(DataLayer.Memo memo, string prefix)
        {
            DataLayer.Tracking tracking = memo.Trackings[0];
            Type t = typeof(DataLayer.Tracking);

            PropertyInfo info = t.GetProperty(prefix + "Acknowledged");
            bool? b = (bool?)info.GetValue(tracking, null);
            bool acknolwedged = b.HasValue && b.Value;

            info = t.GetProperty(prefix + "Approved");
            b = (bool?)info.GetValue(tracking, null);
            bool approved = b.HasValue && b.Value;

            if (!acknolwedged && !approved)
            {
                if (memo.ScrMemType == 6 && (prefix == "AP" || prefix == "PGM"))
                    return "N/A";
                if (memo.OrigOffice == "10" && prefix == "AP")
                    return "N/A";
                return string.Empty;
            }

            if (approved)
            {
                info = t.GetProperty(prefix.Replace("Admin", "") + "Signature");
                string sig = (string)info.GetValue(tracking, null);
                info = t.GetProperty(prefix + "Date");
                DateTime? stamp = (DateTime?)info.GetValue(tracking, null);

                StringBuilder sb = new StringBuilder();
                sb.Append("<span style='color: green;'>");
                if (!string.IsNullOrEmpty(sig))
                    sb.Append(sig);
                if (stamp.HasValue)
                    sb.Append(" (" + stamp.Value.ToShortDateString() + ")");
                sb.Append("</span>");
                return sb.ToString();
            }
            else
            {
                return "<span style='color: blue;'>Acknowledged</span>";
            }
        }


        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (this.Page.Master is AdminMaster)
            {
                ((AdminMaster)this.Page.Master).TabPageUrl = "/Admin/AdminDefault.aspx";
                ((AdminMaster)this.Page.Master).SubTabPageUrl = "/Admin/AdminDefault.aspx";
            }
        }
    }
}