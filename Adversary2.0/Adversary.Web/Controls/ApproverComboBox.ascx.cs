﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.Web.Extensions;
using Adversary.DataLayer;
using AjaxControlToolkit;
using Adversary.DataLayer.HRDataService;

namespace Adversary.Web.Controls
{
    public partial class ApproverComboBox : System.Web.UI.UserControl
    {
        private Adversary.DataLayer.ApproverComboBox _dataLayer = null;

        private Adversary.DataLayer.ApproverComboBox DataLayer
        {
            get
            {
                if (_dataLayer == null)
                {
                    _dataLayer = new Adversary.DataLayer.ApproverComboBox();
                }
                return _dataLayer;
            }
        }

        private Adversary.DataLayer.AdversaryData _adversaryDataLayer = null;

        private Adversary.DataLayer.AdversaryData AdversaryDataLayer
        {
            get
            {
                if (_adversaryDataLayer == null)
                {
                    _adversaryDataLayer = new Adversary.DataLayer.AdversaryData();
                }
                return _adversaryDataLayer;
            }
        }

        public ApproverComboBoxType ApproverComboBoxType
        {
            get;
            set;
        }

        public bool Enabled
        {
            get
            {
                return this.combo.Enabled;
            }

            set
            {
                this.combo.Enabled = value;
            }
        }

        private int GetApproverID()
        {
            int selected = -1;
            if (ThisMemo.Trackings != null && ThisMemo.Trackings.Count > 0)
            {
                if (this.ApproverComboBoxType == Web.Controls.ApproverComboBoxType.AdministrativePartner)
                {
                    selected = Convert.ToInt32(ThisMemo.Trackings[0].APUserID);
                }
                else if (this.ApproverComboBoxType == Web.Controls.ApproverComboBoxType.AdversaryGroup)
                {
                    selected = Convert.ToInt32(ThisMemo.Trackings[0].AdversaryUserID);
                }
                else if (this.ApproverComboBoxType == Web.Controls.ApproverComboBoxType.PracticeGroupLeader)
                {
                    selected = Convert.ToInt32(ThisMemo.Trackings[0].PGMUserID);
                }
            }
            return selected;
        }

        private bool IsRejectedMatter()
        {
            return false;
        }


        /// <summary>
        /// 
        /// </summary>
        private int GetOfficeCode()
        {
            string officeCode = "";
            FirmOffice office = AdversaryDataLayer.GetAttorneyOffice(ThisMemo.RespAttID);
            if (!IsRejectedMatter() && office == null)
            {
                officeCode = ThisMemo.OrigOffice;
            }
            else
            {
                officeCode = office.LocationCode;
            }

            //TODO: temp fix for Carson City, remove this when multiple APs allowed
            if (!IsRejectedMatter() && !string.IsNullOrEmpty(officeCode) && officeCode == "68")
            {
                officeCode = "67";
            }

            return Convert.ToInt32(officeCode);
        }

        public Memo ThisMemo
        {
            get;
            set;
        }

        public event EventHandler SelectedIndexChanged;
        
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            this.ThisMemo.HH_GuardAgainstNull("ThisMemo");
        }

        public override void Dispose()
        {
            if (this._dataLayer != null)
            {
                this._dataLayer.Dispose();
                this._dataLayer = null;
            }

            if (this._adversaryDataLayer != null)
            {
                this._adversaryDataLayer.Dispose();
                this._adversaryDataLayer = null;
            }

            base.Dispose();
        }

        public bool AutoPostBack
        {
            get
            {
                return this.combo.AutoPostBack;
            }

            set
            {
                this.combo.AutoPostBack = value;
            }
        }

        public string CSSClass
        {
            get
            {
                return this.combo.CssClass;
            }

            set
            {
                this.combo.CssClass = value;
            }
        }

        public string SelectedValue
        {
            get
            {
                return this.combo.SelectedValue;
            }

            set
            {
                this.combo.SelectedValue = value;
            }
        }

        public ListItem SelectedItem
        {
            get
            {
                return this.combo.SelectedItem;
            }
        }

        public ListItemCollection Items
        {
            get
            {
                return combo.Items;
            }
        }

        protected void combo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.SelectedIndexChanged != null)
            {
                this.SelectedIndexChanged(this, e);
            }
        }

        protected void combo_PreRender(object sender, EventArgs e)
        {
            ComboBox combo = (ComboBox)sender;

            int approverID = this.GetApproverID();
            List<AdminLogin> adminLogins = null;
            if (this.ApproverComboBoxType == Web.Controls.ApproverComboBoxType.PracticeGroupLeader)
            {
                adminLogins = DataLayer.GetAdminLoginsByPracCode(ThisMemo.PracCode);
            }
            else if (this.ApproverComboBoxType == Web.Controls.ApproverComboBoxType.AdversaryGroup)
            {
                adminLogins = DataLayer.GetAdminLoginsByAdversaryGroup();
            }
            else if (this.ApproverComboBoxType == Web.Controls.ApproverComboBoxType.AdministrativePartner)
            {
                adminLogins = DataLayer.GetAdminLoginsByOfficeCode(GetOfficeCode());
            }

            combo.Items.Add(new ListItem("(Unassigned)", "unassigned"));
            if (adminLogins != null)
            {
                foreach (AdminLogin log in adminLogins)
                {
                    combo.Items.Add(new ListItem(log.Login + NameSuffix(log), log.UserID.ToString()));
                    if (log.UserID == approverID)
                    {
                        combo.SelectedValue = log.UserID.ToString();
                    }
                }
            }
        }

        private const string PrimarySuffix = " (P)";
        private const string SecondarySuffix = " (S)";

        private string NameSuffix(AdminLogin login)
        {
            string suffix = "";
            switch (this.ApproverComboBoxType)
            {
                case Web.Controls.ApproverComboBoxType.AdministrativePartner:
                    suffix = (login.IsPrimaryAP ?? false) ? PrimarySuffix : SecondarySuffix;
                    break;
                case Web.Controls.ApproverComboBoxType.PracticeGroupLeader:
                    // TODO: ugly hack to show both primary and secondary 
                    //       practice group leader
                    AdminPracCode pcode = (from c in DataLayer.AdminPracCodes
                                           where c.PracCode == ThisMemo.PracCode
                                           && (c.IsPrimary ?? false) == true
                                           && c.AdminLogin.UserID == login.UserID 
                                           select c).FirstOrDefault();

                    AdminPracCode scode = (from c in DataLayer.AdminPracCodes
                                           where c.PracCode == ThisMemo.PracCode
                                           && (c.IsPrimary ?? false) == false
                                           && c.AdminLogin.UserID == login.UserID 
                                           select c).FirstOrDefault();

                    if (pcode != null && scode == null)
                    {
                        suffix = PrimarySuffix;
                    }
                    else if (pcode == null && scode != null)
                    {
                        suffix = SecondarySuffix;
                    }
                    else if (pcode != null && scode != null)
                    {
                        string f = (from ListItem l in this.combo.Items
                                    where l.Text == login.Login + PrimarySuffix
                                    select l.Text).FirstOrDefault();
                        if (f == null)
                        {
                            suffix = PrimarySuffix;
                        }
                        else
                        {
                            suffix = SecondarySuffix;
                        }
                    }
                    break;
                case Web.Controls.ApproverComboBoxType.AdversaryGroup:
                    suffix = (login.IsAdversaryAdmin ?? false) ? PrimarySuffix : SecondarySuffix;
                    break;
            }
            return suffix;
        }
    }
    
    public enum ApproverComboBoxType
    {
        AdministrativePartner,
        PracticeGroupLeader,
        AdversaryGroup
    }
}