﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using System.ComponentModel;
using Adversary.Web.Extensions;

namespace Adversary.Web.Controls
{
    [ParseChildren(true)]
    [PersistChildren(false)]
    public partial class ListContainerControl : System.Web.UI.UserControl
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            _insertItemTemplate.HH_GuardAgainstNull("InsertItemTemplate");

            grv.EmptyDataTemplate = _emptyDataTemplate;
            grv.PagerTemplate = _pagerTemplate;

            frmAdd.InsertItemTemplate = _insertItemTemplate;
            
            mpxChange.BehaviorID = string.Format("{0}_behChange", this.ClientID);
            mpxDelete.BehaviorID = string.Format("{0}_behDelete", this.ClientID);

            if (_editItemTemplate != null)
            {
                frmChange.EditItemTemplate = _editItemTemplate;
            }

            if (_deleteItemTemplate != null)
            {
                frmDelete.ItemTemplate = _deleteItemTemplate;
            }
        }

        private int DelIdx
        {
            get
            {
                return Convert.ToInt32(this.Session[this.ClientID + "_DelIdx"]);
            }
            set
            {
                this.Session[this.ClientID + "_DelIdx"] = value;
            }
        }

        private int EdtIdx
        {
            get
            {
                return Convert.ToInt32(this.Session[this.ClientID + "_EdtIdx"]);
            }
            set
            {
                this.Session[this.ClientID + "_EdtIdx"] = value;
            }
        }

        #region button properties
        
        public string Text
        {
            get
            {
                return btnPopAdd.Text;
            }

            set
            {
                btnPopAdd.Text = value;
            }
        }

        #endregion

        #region gridview properties

        public string AccessKey
        {
            get
            {
                return grv.AccessKey;
            }

            set
            {
                grv.AccessKey = value;
            }
        }

        public bool AllowPaging
        {
            get
            {
                return grv.AllowPaging;
            }
            set
            {
                grv.AllowPaging = value;
            }
        }

        public bool AllowSorting
        {
            get
            {
                return grv.AllowSorting;
            }
            set
            {
                grv.AllowSorting = value;
            }
        }

        [Browsable(false)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [MergableProperty(false)]
        public TableItemStyle AlternatingRowStyle
        {
            get
            {
                return grv.AlternatingRowStyle;
            }
        }

        public bool AutoGenerateColumns
        {
            get
            {
                return grv.AutoGenerateColumns;
            }
            set
            {
                grv.AutoGenerateColumns = value;
            }
        }

        public System.Drawing.Color BackColor
        {
            get
            {
                return grv.BackColor;
            }
            set
            {
                grv.BackColor = value;
            }
        }

        public string BackImageUrl
        {
            get
            {
                return grv.BackImageUrl;
            }
            set
            {
                grv.BackImageUrl = value;
            }
        }

        public System.Drawing.Color BorderColor
        {
            get
            {
                return grv.BorderColor;
            }
            set
            {
                grv.BorderColor = value;
            }
        }

        [Browsable(false)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [MergableProperty(false)]
        public BorderStyle BorderStyle
        {
            get
            {
                return grv.BorderStyle;
            }
            set
            {
                grv.BorderStyle = value;
            }
        }

        public Unit BorderWidth
        {
            get
            {
                return grv.BorderWidth;
            }
            set
            {
                grv.BorderWidth = value;
            }
        }

        public GridViewRow BottomPagerRow
        {
            get
            {
                return grv.BottomPagerRow;
            }
        }

        public string Caption
        {
            get
            {
                return grv.Caption;
            }
            set
            {
                grv.Caption = value;
            }
        }

        public TableCaptionAlign CaptionAlign
        {
            get
            {
                return grv.CaptionAlign;
            }
            set
            {
                grv.CaptionAlign = value;
            }
        }

        public int CellPadding
        {
            get
            {
                return grv.CellPadding;
            }
            set
            {
                grv.CellPadding = value;
            }
        }

        public int CellSpacing
        {
            get
            {
                return grv.CellSpacing;
            }
            set
            {
                grv.CellSpacing = value;
            }
        }

        [Browsable(false)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [MergableProperty(true)]
        public DataControlFieldCollection Columns
        {
            get
            {
                return grv.Columns;
            }
        }

        public bool ControlStyleCreated
        {
            get
            {
                return grv.ControlStyleCreated;
            }
        }

        public string CssClass
        {
            get
            {
                return grv.CssClass;
            }
            set
            {
                grv.CssClass = value;
            }
        }

        public string[] DataKeyNames
        {
            get
            {
                return grv.DataKeyNames;
            }
            set
            {
                grv.DataKeyNames = value;
            }
        }

        public DataKeyArray DataKeys
        {
            get
            {
                return grv.DataKeys;
            }
        }

        public string DataMember
        {
            get
            {
                return grv.DataMember;
            }
            set
            {
                grv.DataMember = value;
            }
        }

        public object DataSource
        {
            get
            {
                return grv.DataSource;
            }
            set
            {
                grv.DataSource = value;
                frmAdd.DataSource = value;
                frmChange.DataSource = value;
                frmDelete.DataSource = value;
            }
        }

        public string DataSourceID
        {
            get
            {
                return grv.DataSourceID;
            }
            set
            {
                grv.DataSourceID = value;
            }
        }

        public IDataSource DataSourceObject
        {
            get
            {
                return grv.DataSourceObject;
            }
        }

        public int EditIndex
        {
            get
            {
                return grv.EditIndex;
            }
            set
            {
                grv.EditIndex = value;
            }
        }

        [Browsable(false)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [MergableProperty(false)]
        public TableItemStyle EditRowStyle
        {
            get
            {
                return grv.EditRowStyle;
            }
        }

        [Browsable(false)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [MergableProperty(false)]
        public TableItemStyle EmptyDataRowStyle
        {
            get
            {
                return grv.EmptyDataRowStyle;
            }
        }

        private ITemplate _emptyDataTemplate;

        [Browsable(false)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [MergableProperty(false)]
        public ITemplate EmptyDataTemplate
        {
            get
            {
                return _emptyDataTemplate;
            }
            set
            {
                _emptyDataTemplate = value;
            }
        }

        public string EmptyDataText
        {
            get
            {
                return grv.EmptyDataText;
            }
            set
            {
                grv.EmptyDataText = value;
            }
        }

        public bool Enabled
        {
            get
            {
                return grv.Enabled;
            }
            set
            {
                grv.Enabled = value;
            }
        }

        public bool EnableModelValidation
        {
            get
            {
                return grv.EnableModelValidation;
            }
            set
            {
                grv.EnableModelValidation = value;
            }
        }

        public bool EnablePersistedSelection
        {
            get
            {
                return grv.EnablePersistedSelection;
            }
            set
            {
                grv.EnablePersistedSelection = value;
            }
        }

        public bool EnableSortingAndPagingCallbacks
        {
            get
            {
                return grv.EnableSortingAndPagingCallbacks;
            }
            set
            {
                grv.EnableSortingAndPagingCallbacks = value;
            }
        }

        public FontInfo Font
        {
            get
            {
                return grv.Font;
            }
        }

        public GridViewRow FooterRow
        {
            get
            {
                return grv.FooterRow;
            }
        }

        [Browsable(false)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [MergableProperty(false)]
        public TableItemStyle FooterStyle
        {
            get
            {
                return grv.FooterStyle;
            }
        }

        public System.Drawing.Color ForeColor
        {
            get
            {
                return grv.ForeColor;
            }
            set
            {
                grv.ForeColor = value;
            }
        }

        public GridLines GridLines
        {
            get
            {
                return grv.GridLines;
            }
            set
            {
                grv.GridLines = value;
            }
        }

        public bool HasAttributes
        {
            get
            {
                return grv.HasAttributes;
            }
        }

        public GridViewRow HeaderRow
        {
            get
            {
                return grv.HeaderRow;
            }
        }

        [Browsable(false)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [MergableProperty(false)]
        public TableItemStyle HeaderStyle
        {
            get
            {
                return grv.HeaderStyle;
            }
        }

        public Unit Height
        {
            get
            {
                return grv.Height;
            }
            set
            {
                grv.Height = value;
            }
        }

        public HorizontalAlign HorizontalAlign
        {
            get
            {
                return grv.HorizontalAlign;
            }
            set
            {
                grv.HorizontalAlign = value;
            }
        }

        public int PageCount
        {
            get
            {
                return grv.PageCount;
            }
        }

        public int PageIndex
        {
            get
            {
                return grv.PageIndex;
            }
            set
            {
                grv.PageIndex = value;
            }
        }

        [Browsable(false)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [MergableProperty(false)]
        public PagerSettings PagerSettings
        {
            get
            {
                return grv.PagerSettings;
            }
        }

        [Browsable(false)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [MergableProperty(false)]
        public TableItemStyle PagerStyle
        {
            get
            {
                return grv.PagerStyle;
            }
        }

        private ITemplate _pagerTemplate;

        [Browsable(false)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [MergableProperty(false)]
        public ITemplate PagerTemplate
        {
            get
            {
                return _pagerTemplate;
            }
            set
            {
                _pagerTemplate = value;
            }
        }

        public int PageSize
        {
            get
            {
                return grv.PageSize;
            }
            set
            {
                grv.PageSize = value;
            }
        }

        public string RowHeaderColumn
        {
            get
            {
                return grv.RowHeaderColumn;
            }
            set
            {
                grv.RowHeaderColumn = value;
            }
        }

        public GridViewRowCollection Rows
        {
            get
            {
                return grv.Rows;
            }
        }

        [Browsable(false)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [MergableProperty(false)]
        public TableItemStyle RowStyle
        {
            get
            {
                return grv.RowStyle;
            }
        }

        public int SelectedIndex
        {
            get
            {
                return grv.SelectedIndex;
            }
            set
            {
                grv.SelectedIndex = value;
            }
        }

        public DataKey SelectedPersistedDataKey
        {
            get
            {
                return grv.SelectedPersistedDataKey;
            }
            set
            {
                grv.SelectedPersistedDataKey = value;
            }
        }

        public GridViewRow SelectedRow
        {
            get
            {
                return grv.SelectedRow;
            }
        }

        [Browsable(false)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [MergableProperty(false)]
        public TableItemStyle SelectedRowStyle
        {
            get
            {
                return grv.SelectedRowStyle;
            }
        }

        public object SelectedValue
        {
            get
            {
                return grv.SelectedValue;
            }
        }

        public bool ShowFooter
        {
            get
            {
                return grv.ShowFooter;
            }
            set
            {
                grv.ShowFooter = value;
            }
        }

        public bool ShowHeader
        {
            get
            {
                return grv.ShowHeader;
            }
            set
            {
                grv.ShowHeader = value;
            }
        }

        public bool ShowHeaderWhenEmpty
        {
            get
            {
                return grv.ShowHeaderWhenEmpty;
            }
            set
            {
                grv.ShowHeaderWhenEmpty = value;
            }
        }

        public SortDirection SortDirection
        {
            get
            {
                return grv.SortDirection;
            }
        }

        [Browsable(false)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [MergableProperty(false)]
        public TableItemStyle SortedAscendingCellStyle
        {
            get
            {
                return grv.SortedAscendingCellStyle;
            }
        }

        [Browsable(false)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [MergableProperty(false)]
        public TableItemStyle SortedAscendingHeaderStyle
        {
            get
            {
                return grv.SortedAscendingHeaderStyle;
            }
        }

        [Browsable(false)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [MergableProperty(false)]
        public TableItemStyle SortedDescendingCellStyle
        {
            get
            {
                return grv.SortedDescendingCellStyle;
            }
        }

        [Browsable(false)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [MergableProperty(false)]
        public TableItemStyle SortedDescendingHeaderStyle
        {
            get
            {
                return grv.SortedDescendingHeaderStyle;
            }
        }

        public string SortExpression
        {
            get
            {
                return grv.SortExpression;
            }
        }

        public CssStyleCollection Style
        {
            get
            {
                return grv.Style;
            }
        }

        public bool SupportsDisabledAttribute
        {
            get
            {
                return grv.SupportsDisabledAttribute;
            }
        }

        public short TabIndex
        {
            get
            {
                return grv.TabIndex;
            }
            set
            {
                grv.TabIndex = value;
            }
        }

        public string ToolTip
        {
            get
            {
                return grv.ToolTip;
            }
            set
            {
                grv.ToolTip = value;
            }
        }

        public GridViewRow TopPagerRow
        {
            get
            {
                return grv.TopPagerRow;
            }
        }

        public bool UseAccessibleHeader
        {
            get
            {
                return grv.UseAccessibleHeader;
            }
            set
            {
                grv.UseAccessibleHeader = value;
            }
        }

        public Unit Width
        {
            get
            {
                return grv.Width;
            }
            set
            {
                grv.Width = value;
            }
        }

        #endregion
        
        #region gridview events

        public event EventHandler GridDataBinding = null;

        protected void grv_DataBinding(object sender, EventArgs e)
        {
            if (this.GridDataBinding != null)
            {
                this.GridDataBinding(sender, e);
            }
        }

        public event EventHandler GridDataBound = null;

        protected void grv_DataBound(object sender, EventArgs e)
        {
            if (this.GridDataBound != null)
            {
                this.GridDataBound(sender, e);
            }
        }

        public event EventHandler GridDisposed = null;

        protected void grv_Disposed(object sender, EventArgs e)
        {
            if (this.GridDisposed != null)
            {
                this.GridDisposed(sender, e);
            }
        }

        public event EventHandler GridInit = null;

        protected void grv_Init(object sender, EventArgs e)
        {
            if (this.GridInit != null)
            {
                this.GridInit(sender, e);
            }
        }

        public event EventHandler GridLoad = null;

        protected void grv_Load(object sender, EventArgs e)
        {
            if (this.GridLoad != null)
            {
                this.GridLoad(sender, e);
            }
        }

        public event EventHandler PageIndexChanged = null;

        protected void grv_PageIndexChanged(object sender, EventArgs e)
        {
            if (this.PageIndexChanged != null)
            {
                this.PageIndexChanged(sender, e);
            }
        }

        public event EventHandler PageIndexChanging = null;

        protected void grv_PageIndexChanging(object sender, EventArgs e)
        {
            if (this.PageIndexChanging != null)
            {
                this.PageIndexChanging(sender, e);
            }
        }

        public event EventHandler GridPreRender = null;

        protected void grv_PreRender(object sender, EventArgs e)
        {
            if (this.GridPreRender != null)
            {
                this.GridPreRender(sender, e);
            }
        }
         
        public event EventHandler RowCancelingEdit = null;

        protected void grv_RowCancelingEdit(object sender, EventArgs e)
        {
            if (this.RowCancelingEdit != null)
            {
                this.RowCancelingEdit(sender, e);
            }
        }

        public event GridViewCommandEventHandler RowCommand = null;

        protected void grv_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                DelIdx = Convert.ToInt32(e.CommandArgument);
            }
            else if (e.CommandName == "Edit")
            {
                EdtIdx = Convert.ToInt32(e.CommandArgument);
            }

            if (this.RowCommand != null)
            {
                this.RowCommand(sender, e);
            }
        }

        public event GridViewRowEventHandler RowCreated = null;

        protected void grv_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (this.RowCreated != null)
            {
                this.RowCreated(sender, e);
            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (_editItemTemplate != null)
                {
                    LinkButton btnChange = (LinkButton)e.Row.FindControl("btnChange");
                    btnChange.Visible = true;
                    btnChange.CommandArgument = Convert.ToString(e.Row.RowIndex);
                    btnChange.OnClientClick = string.Format("{0}_showMPX(\"{1}\")", this.ClientID, mpxChange.BehaviorID);
                }

                if (_deleteItemTemplate != null)
                {
                    LinkButton btnDelete = (LinkButton)e.Row.FindControl("btnDelete");
                    btnDelete.Visible = true;
                    btnDelete.CommandArgument = Convert.ToString(e.Row.RowIndex);
                    btnDelete.OnClientClick = string.Format("{0}_showMPX(\"{1}\")", this.ClientID, mpxDelete.BehaviorID);
                }
            }
        }

        public event GridViewRowEventHandler RowDataBound = null;

        protected void grv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (this.RowDataBound != null)
            {
                this.RowDataBound(sender, e);
            }
        }

        public event GridViewDeletedEventHandler RowDeleted = null;

        protected void grv_RowDeleted(object sender, GridViewDeletedEventArgs e)
        {
            if (this.RowDeleted != null)
            {
                this.RowDeleted(sender, e);
            }
        }

        public event GridViewDeleteEventHandler RowDeleting = null;

        protected void grv_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            if (this.RowDeleting != null)
            {
                this.RowDeleting(sender, e);
            }
        }

        public event GridViewEditEventHandler RowEditing = null;

        protected void grv_RowEditing(object sender, GridViewEditEventArgs e)
        {
            if (this.RowEditing != null)
            {
                this.RowEditing(sender, e);
            }
        }

        public event GridViewUpdatedEventHandler RowUpdated = null;

        protected void grv_RowUpdated(object sender, GridViewUpdatedEventArgs e)
        {
            if (this.RowUpdated != null)
            {
                this.RowUpdated(sender, e);
            }
        }

        public event GridViewUpdateEventHandler RowUpdating = null;

        protected void grv_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            if (this.RowUpdating != null)
            {
                this.RowUpdating(sender, e);
            }
        }

        public event EventHandler SelectedIndexChanged = null;

        protected void grv_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.SelectedIndexChanged != null)
            {
                this.SelectedIndexChanged(sender, e);
            }
        }

        public event GridViewSelectEventHandler SelectedIndexChanging = null;

        protected void grv_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            if (this.SelectedIndexChanging != null)
            {
                this.SelectedIndexChanging(sender, e);
            }
        }

        public event EventHandler Sorted = null;

        protected void grv_Sorted(object sender, EventArgs e)
        {
            if (this.Sorted != null)
            {
                this.Sorted(sender, e);
            }
        }

        public event GridViewSortEventHandler Sorting = null;

        protected void grv_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (this.Sorting != null)
            {
                this.Sorting(sender, e);
            }
        }

        public event EventHandler GridUnload = null;

        protected void grv_Unload(object sender, EventArgs e)
        {
            if (this.GridUnload != null)
            {
                this.GridUnload(sender, e);
            }
        }

        #endregion 
        
        #region gridview methods

        public override void DataBind()
        {
            grv.DataBind();
            frmAdd.DataBind();
            frmChange.DataBind();
            frmDelete.DataBind();
        }

        public void DeleteRow(int rowIndex)
        {
            grv.DeleteRow(rowIndex);
        }

        public bool IsBindableType(Type type)
        {
            return grv.IsBindableType(type);
        }

        public void SetEditRow(int rowIndex)
        {
            grv.SetEditRow(rowIndex);
        }

        public void SetPageIndex(int rowIndex)
        {
            grv.SetPageIndex(rowIndex);
        }

        public void Sort(string sortExpression, SortDirection sortDirection)
        {
            grv.Sort(sortExpression, sortDirection);
        }

        public void UpdateRow(int rowIndex, bool causesValidation)
        {
            grv.UpdateRow(rowIndex, causesValidation);
        }

        #endregion 

        #region form view properties

        private ITemplate _insertItemTemplate;

        [Browsable(false)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [MergableProperty(false)]
        public ITemplate InsertItemTemplate
        {
            get
            {
                return _insertItemTemplate;
            }
            set
            {
                _insertItemTemplate = value;
            }
        }

        private ITemplate _editItemTemplate;

        [Browsable(false)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [MergableProperty(false)]
        public ITemplate EditItemTemplate
        {
            get
            {
                return _editItemTemplate;
            }
            set
            {
                _editItemTemplate = value;
            }
        }

        private ITemplate _deleteItemTemplate;

        [Browsable(false)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [MergableProperty(false)]
        public ITemplate DeleteItemTemplate
        {
            get
            {
                return _deleteItemTemplate;
            }
            set
            {
                _deleteItemTemplate = value;
            }
        }
        
        #endregion 

        #region modal popup properties

        public string PopupCssClass
        {
            get
            {
                return this.pnlAdd.CssClass;
            }
            set
            {
                this.pnlAdd.CssClass = value;
                this.pnlChange.CssClass = value;
                this.pnlDelete.CssClass = value;
            }
        }

        public string BackgroundCssClass
        {
            get
            {
                return this.mpxAdd.BackgroundCssClass;
            }
            set
            {
                this.mpxAdd.BackgroundCssClass = value;
                this.mpxChange.BackgroundCssClass = value;
                this.mpxDelete.BackgroundCssClass = value;
            }
        }

        public int AddX
        {
            get { return this.mpxAdd.X; }
            set { this.mpxAdd.X = value; }
        }

        public int AddY
        {
            get { return this.mpxAdd.Y; }
            set { this.mpxAdd.Y = value; }
        }

        public int ChangeX
        {
            get { return this.mpxChange.X; }
            set { this.mpxChange.X = value; }
        }

        public int ChangeY
        {
            get { return this.mpxChange.Y; }
            set { this.mpxChange.Y = value; }
        }

        public int DeleteX
        {
            get { return this.mpxDelete.X; }
            set { this.mpxDelete.X = value; }
        }

        public int DeleteY
        {
            get { return this.mpxDelete.Y; }
            set { this.mpxDelete.Y = value; }
        }

        #endregion 

        #region form view events

        public event FormViewDeletedEventHandler ItemDeleted = null;

        protected void frmDelete_ItemDeleted(object sender, FormViewDeletedEventArgs e)
        {
            if (this.ItemDeleted != null)
            {
                this.ItemDeleted(sender, e);
            }
        }

        public event FormViewDeleteEventHandler ItemDeleting = null;

        protected void frmDelete_ItemDeleting(object sender, FormViewDeleteEventArgs e)
        {
            if (this.ItemDeleting != null)
            {
                this.ItemDeleting(sender, e);
            }
        }

        public event FormViewUpdatedEventHandler ItemUpdated = null;

        protected void frmChange_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {
            if (this.ItemUpdated != null)
            {
                this.ItemUpdated(sender, e);
            }
        }

        public event FormViewUpdateEventHandler ItemUpdating = null;

        protected void frmChange_ItemUpdating(object sender, FormViewUpdateEventArgs e)
        {
            if (this.ItemUpdating != null)
            {
                this.ItemUpdating(sender, e);
            }
        }

        public event FormViewInsertedEventHandler ItemInserted = null;

        protected void frmAdd_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {
            if (this.ItemInserted != null)
            {
                this.ItemInserted(sender, e);
            }
        }

        public event FormViewInsertEventHandler ItemInserting = null;

        protected void frmAdd_ItemInserting(object sender, FormViewInsertEventArgs e)
        {
            if (this.ItemInserting != null)
            {
                this.ItemInserting(sender, e);
            }
        }

        public event EventHandler AddClick = null;

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            if (this.AddClick != null)
            {
                this.AddClick(sender, e);
            }
        }

        public event EventHandler AddCancelClick = null;

        protected void btnAddCancel_Click(object sender, EventArgs e)
        {
            if (this.AddCancelClick != null)
            {
                this.AddCancelClick(sender, e);
            }
        }

        public event EventHandler ChangeClick = null;

        protected void btnChange_Click(object sender, EventArgs e)
        {
            if (this.ChangeClick != null)
            {
                this.ChangeClick(sender, e);
            }
        }

        public event EventHandler ChangeCancelClick = null;

        protected void btnChangeCancel_Click(object sender, EventArgs e)
        {
            if (this.ChangeCancelClick != null)
            {
                this.ChangeCancelClick(sender, e);
            }
        }

        public event EventHandler DeleteClick = null;

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (this.DeleteClick != null)
            {
                this.DeleteClick(sender, e);
            }
        }

        public event EventHandler DeleteCancelClick = null;

        protected void btnDeleteCancel_Click(object sender, EventArgs e)
        {
            if (this.DeleteCancelClick != null)
            {
                this.DeleteCancelClick(sender, e);
            }
        }
        
        protected void frmChange_PreRender(object sender, EventArgs e)
        {
            this.frmChange.PageIndex = EdtIdx;
            this.frmChange.DataBind();
            this.upChange.Update();
        }

        protected void frmDelete_PreRender(object sender, EventArgs e)
        {
            this.frmDelete.PageIndex = DelIdx;
            this.frmDelete.DataBind();
            this.upDelete.Update();
        }

        #endregion 

        #region form view methods

        public Control FindAddControl(string id)
        {
            return frmAdd.FindControl(id);
        }

        public Control FindChangeControl(string id)
        {
            return frmChange.FindControl(id);
        }

        public Control FindDeleteControl(string id)
        {
            return frmDelete.FindControl(id);
        }

        public UpdatePanel DeleteUpdatePanel
        {
            get {
                return upDelete;
            }
        }

        #endregion 
    }
}