﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SubTabNavigation.ascx.cs"
    Inherits="Adversary.Web.Controls.Navigation" %>
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        $("a.subtab.dropdown").click(function () {
            $("div.dropdown").hide("fast");
            var d = document.getElementById(this.rel);
            var pos = $(this).offset();
            d.style.left = pos.left + "px";
            var w = $(d).outerWidth();
            var w2 = $(this).outerWidth();
            d.style.width = ((w > w2) ? w : w2) + "px";
            $(d).show("fast");
            return false;
        });

        $(document).click(function () {
            $("div.dropdown").hide("fast");
        });
    });
</script>
<asp:Repeater runat="server" ID="rptNav" OnPreRender="rptNav_PreRender" OnItemDataBound="rptNav_ItemDataBound">
    <ItemTemplate>
        <asp:LinkButton ID="lbNavigate" runat="server" CausesValidation="true" CommandName="Navigate"
            CssClass="subtab" Text='<%#Eval("Name")%>' ToolTip='<%#Eval("Description")%>'
            PostBackUrl='<%#Eval("SubTabURL")%>' />
        <div id="divSubNav" runat="server" class="dropdown" visible="false">
            <asp:Repeater ID="rptSubNav" runat="server" Visible="false" OnItemDataBound="rptSubNav_ItemDataBound">
                <ItemTemplate>
                    <asp:LinkButton ID="lbSubNavigate" runat="server" CausesValidation="true" CommandName="SubNavigate"
                        CssClass="subtabitem" Text='<%#Eval("Name")%>' ToolTip='<%#Eval("Description")%>'
                        PostBackUrl='<%#Eval("SubTabURL")%>'/><br />
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </ItemTemplate>
</asp:Repeater>
