﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.Web.Extensions;
using System.ComponentModel;

namespace Adversary.Web.Controls
{
    [ParseChildren(true)]
    [PersistChildren(false)]
    public partial class AutoCompleteTextbox : System.Web.UI.UserControl
    {
        #region TextBox Events

        public event EventHandler OnTextDataBinding;

        protected void txt_DataBinding(object sender, EventArgs e)
        {
            if (this.OnTextDataBinding != null)
            {
                this.OnTextDataBinding(sender, e);
            }
        }

        public event EventHandler OnTextDisposed;

        protected void txt_Disposed(object sender, EventArgs e)
        {
            if (this.OnTextDisposed != null)
            {
                this.OnTextDisposed(sender, e);
            }
        }

        public event EventHandler OnTextInit;

        protected void txt_Init(object sender, EventArgs e)
        {
            if (this.OnTextInit != null)
            {
                this.OnTextInit(sender, e);
            }
        }

        public event EventHandler OnTextLoad;

        protected void txt_Load(object sender, EventArgs e)
        {
            if (this.OnTextLoad != null)
            {
                this.OnTextLoad(sender, e);
            }
        }

        public event EventHandler OnTextPreRender;

        protected void txt_PreRender(object sender, EventArgs e)
        {
            if (this.OnTextPreRender != null)
            {
                this.OnTextPreRender(sender, e);
            }
        }

        public event EventHandler OnTextChanged;
        
        protected void txt_TextChanged(object sender, EventArgs e)
        {
            if (OnTextChanged != null)
            {
                OnTextChanged(sender, e);
            }
        }

        public event EventHandler OnTextUnload;

        protected void txt_Unload(object sender, EventArgs e)
        {
            if (this.OnTextUnload != null)
            {
                this.OnTextUnload(sender, e);
            }
        }

        #endregion 

        #region TextBox Properties
        public TextBox TextBox
        {
            get
            {
                return this.txt;
            }
        }

        public string AccessKey
        {
            get
            {
                return this.txt.AccessKey;
            }

            set
            {
                this.txt.AccessKey = value;
            }
        }

        public AutoCompleteType AutoCompleteType
        {
            get
            {
                return this.txt.AutoCompleteType;
            }

            set
            {
                this.txt.AutoCompleteType = value;
            }
        }

        public bool AutoPostBack
        {
            get
            {
                return this.txt.AutoPostBack;
            }

            set
            {
                this.txt.AutoPostBack = value;
            }
        }

        public System.Drawing.Color BackColor
        {
            get
            {
                return this.txt.BackColor;
            }

            set
            {
                this.txt.BackColor = value;
            }
        }

        public System.Drawing.Color BorderColor
        {
            get
            {
                return this.txt.BorderColor;
            }

            set
            {
                this.txt.BorderColor = value;
            }
        }

        public System.Web.UI.WebControls.BorderStyle BorderStyle
        {
            get
            {
                return this.txt.BorderStyle;
            }

            set
            {
                this.txt.BorderStyle = value;
            }
        }

        public System.Web.UI.WebControls.Unit BorderWidth
        {
            get
            {
                return this.txt.BorderWidth;
            }

            set
            {
                this.txt.BorderWidth = value;
            }
        }

        public bool CausesValidation
        {
            get
            {
                return this.txt.CausesValidation;
            }

            set
            {
                this.txt.CausesValidation = true;
            }
        }

        public string CssClass
        {
            get
            {
                return this.txt.CssClass;
            }

            set
            {
                this.txt.CssClass = value;
            }
        }

        public bool Enabled
        {
            get
            {
                return this.txt.Enabled;
            }

            set
            {
                this.txt.Enabled = value;
            }
        }

        public System.Web.UI.WebControls.FontInfo Font
        {
            get
            {
                return this.txt.Font;
            }
        }

        public System.Drawing.Color ForeColor
        {
            get
            {
                return this.txt.ForeColor;
            }

            set
            {
                this.txt.ForeColor = value;
            }
        }

        public System.Web.UI.WebControls.Unit Height
        {
            get
            {
                return this.txt.Height;
            }

            set
            {
                this.txt.Height = value;
            }
        }

        public int MaxLength
        {
            get
            {
                return txt.MaxLength;
            }

            set
            {
                txt.MaxLength = value;
            }
        }

        public bool ReadOnly
        {
            get
            {
                return this.txt.ReadOnly;
            }

            set
            {
                this.txt.ReadOnly = value;
            }
        }

        public int Rows
        {
            get
            {
                return this.txt.Rows;
            }

            set
            {
                this.txt.Rows = value;
            }
        }

        public string ToolTip
        {
            get
            {
                return this.txt.ToolTip;
            }

            set
            {
                this.txt.ToolTip = value;
            }
        }

        public string Text
        {
            get
            {
                return this.txt.Text;
            }

            set
            {
                this.txt.Text = value;
            }
        }

        public string title
        {
            get
            {
                return this.txt.Attributes["title"];
            }
            set
            {
                this.txt.Attributes["title"] = value;
            }
        }

        #endregion 

        #region AutoCompleteExtender Events

        public event EventHandler OnCompleteDataBinding;

        protected void acx_DataBinding(object sender, EventArgs e)
        {
            if (this.OnCompleteDataBinding != null)
            {
                this.OnCompleteDataBinding(sender, e);
            }
        }

        public event EventHandler OnCompleteDisposed;
        
        protected void acx_Disposed(object sender, EventArgs e)
        {
            if (this.OnCompleteDisposed != null)
            {
                this.OnCompleteDisposed(sender, e);
            }
        }

        public event EventHandler OnCompleteInit;

        protected void acx_Init(object sender, EventArgs e)
        {
            if (this.OnCompleteInit != null)
            {
                this.OnCompleteInit(sender, e);
            }
        }

        public event EventHandler OnCompleteLoad;

        protected void acx_Load(object sender, EventArgs e)
        {
            if (this.OnCompleteLoad != null)
            {
                this.OnCompleteLoad(sender, e);
            }
        }

        public event EventHandler OnCompletePreRender;

        protected void acx_PreRender(object sender, EventArgs e)
        {
            if (this.OnCompletePreRender != null)
            {
                this.OnCompletePreRender(sender, e);
            }
        }

        public event EventHandler OnCompleteResolveControlID;

        protected void acx_ResolveControlID(object sender, EventArgs e)
        {
            if (this.OnCompleteResolveControlID != null)
            {
                this.OnCompleteResolveControlID(sender, e);
            }
        }

        public event EventHandler OnCompleteUnload;

        protected void acx_Unload(object sender, EventArgs e)
        {
            if (this.OnCompleteUnload != null)
            {
                this.OnCompleteUnload(sender, e);
            }
        }

        #endregion 

        #region AutoCompleteExtender Properties
        [Browsable(false)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [MergableProperty(false)]
        public string Animations
        {
            get
            {
                return this.acx.Animations;
            }

            set
            {
                this.acx.Animations = value;
            }
        }       

        public string BehaviorID
        {
            get
            {
                return this.acx.BehaviorID;
            }

            set
            {
                this.acx.BehaviorID = value;
            }
        }

        public int CompletionInterval
        {
            get
            {
                return this.acx.CompletionInterval;
            }

            set
            {
                this.acx.CompletionInterval = value;
            }
        }

        public string CompletionListCssClass
        {
            get
            {
                return this.acx.CompletionListCssClass;
            }

            set
            {
                this.acx.CompletionListCssClass = value;
            }
        }

        public string CompletionListHighlightedItemCssClass
        {
            get
            {
                return this.acx.CompletionListHighlightedItemCssClass;
            }

            set
            {
                this.acx.CompletionListHighlightedItemCssClass = value;
            }
        }

        public string CompletionListItemCssClass
        {
            get
            {
                return this.acx.CompletionListItemCssClass;
            }

            set
            {
                this.acx.CompletionListItemCssClass = value;
            }
        }

        public int CompletionSetCount
        {
            get
            {
                return this.acx.CompletionSetCount;
            }

            set
            {
                this.acx.CompletionSetCount = value;
            }
        }

        public string ContextKey
        {
            get
            {
                return this.acx.ContextKey;
            }

            set
            {
                this.acx.ContextKey = value;
            }
        }

        public string DelimiterCharacters
        {
            get
            {
                return this.acx.DelimiterCharacters;
            }

            set
            {
                this.acx.DelimiterCharacters = value;
            }
        }

        public int MinimumPrefixLength
        {
            get
            {
                return this.acx.MinimumPrefixLength;
            }

            set
            {
                this.acx.MinimumPrefixLength = value;
            }
        }

        public string OnClientHidden
        {
            get
            {
                return this.acx.OnClientHidden;
            }

            set
            {
                this.acx.OnClientHidden = value;
            }
        }

        public string OnClientHiding
        {
            get
            {
                return this.acx.OnClientHiding;
            }

            set
            {
                this.acx.OnClientHiding = value;
            }
        }

        public string OnClientItemOut
        {
            get
            {
                return this.acx.OnClientItemOut;
            }

            set
            {
                this.acx.OnClientItemOut = value;
            }
        }

        public string OnClientItemOver
        {
            get
            {
                return this.acx.OnClientItemOver;
            }

            set
            {
                this.acx.OnClientItemOver = value;
            }
        }

        public string OnClientItemSelected
        {
            get
            {
                return this.acx.OnClientItemSelected;
            }

            set
            {
                this.acx.OnClientItemSelected = value;
            }
        }

        public string OnClientPopulated
        {
            get
            {
                return this.acx.OnClientPopulated;
            }

            set
            {
                this.acx.OnClientPopulated = value;
            }
        }

        public string OnClientPopulating
        {
            get
            {
                return this.acx.OnClientPopulating;
            }

            set
            {
                this.acx.OnClientPopulating = value;
            }
        }

        public string OnClientShowing
        {
            get
            {
                return this.acx.OnClientShowing;
            }

            set
            {
                this.acx.OnClientShowing = value;
            }
        }

        public string OnClientShown
        {
            get
            {
                return this.acx.OnClientShown;
            }

            set
            {
                this.acx.OnClientShown = value;
            }
        }

        public AjaxControlToolkit.Animation OnHide
        {
            get
            {
                return this.acx.OnHide;
            }

            set
            {
                this.acx.OnHide = value;
            }
        }

        public AjaxControlToolkit.Animation OnShow
        {
            get
            {
                return this.acx.OnShow;
            }

            set
            {
                this.acx.OnShow = value;
            }
        }

        public string ScriptPath
        {
            get
            {
                return this.acx.ScriptPath;
            }

            set
            {
                this.acx.ScriptPath = value;
            }
        }

        public string ServiceMethod
        {
            get
            {
                return this.acx.ServiceMethod;
            }

            set
            {
                this.acx.ServiceMethod = value;
            }
        }

        public string ServicePath
        {
            get
            {
                return this.acx.ServicePath;
            }

            set
            {
                this.acx.ServicePath = value;
            }
        }

        public bool ShowOnlyCurrentWordInCompletionListItem
        {
            get
            {
                return this.acx.ShowOnlyCurrentWordInCompletionListItem;
            }

            set
            {
                this.acx.ShowOnlyCurrentWordInCompletionListItem = value;
            }
        }

        public bool UseContextKey
        {
            get
            {
                return this.acx.UseContextKey;
            }

            set
            {
                this.acx.UseContextKey = value;
            }
        }

        #endregion 

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            ServiceMethod.HH_GuardAgainstNullOrWhiteSpace("ServiceMethod");
        }
    }
}