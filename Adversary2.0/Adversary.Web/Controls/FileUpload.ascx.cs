﻿using System;
using System.Web;
using System.ComponentModel;
using Adversary.Utils;
using Adversary.DataLayer;
using Adversary.Controls;

namespace Adversary.Web.Controls
{
    public partial class FileUpload : System.Web.UI.UserControl
    {
        public delegate void OnFileUpload(Adversary.Controls.UploadEventArgs e);
        public event OnFileUpload FileUploadHandler;

        public delegate void OnFileUploadError(Adversary.Controls.UploadEventArgs e);
        public event OnFileUploadError FileUploadErrorHandler;

        public delegate void OnFileUploadSuccess(Adversary.Controls.UploadEventArgs e);
        public event OnFileUploadSuccess FileUploadSuccessHandler;

        private int _ScreeningMemoID;

        public int ScreeningMemoID
        {
            get { return _ScreeningMemoID; }
            set { _ScreeningMemoID = value; }
        }

        private string _FileUploadDirectory;

        public string FileUploadDirectory
        {
            get { return _FileUploadDirectory; }
            set { _FileUploadDirectory = value; }
        }

        private string _UploadPath;

        private void Page_Load(object sender, EventArgs e)
        {
            lblFileDescriptionCaption.Text = "File Description:";
            lblFileCaption.Text = "File to Upload:";
            //this.FileUploadHandler += new OnFileUpload(FileUpload_FileUploadHandler);
            //this.FileUploadErrorHandler += new OnFileUploadError(FileUpload_FileUploadErrorHandler);
            //this.FileUploadSuccessHandler += new OnFileUploadSuccess(FileUpload_FileUploadSuccessHandler);
            _UploadPath = _FileUploadDirectory + "\\" + _ScreeningMemoID.ToString() + "\\";
            
            if (_FileUploadMode != null)
            {
                if (tbFileDescription.Text.Length == 0 && !_FileUploadMode.Equals(FileUploadClass.FileUploadMode.None))
                {
                    tbFileDescription.Text = _FileUploadMode.ToDescription();
                }
                tbFileDescription.Enabled = (_FileUploadMode.Equals(FileUploadClass.FileUploadMode.None) ? true: false);
            }

        }

        protected override void OnPreRender(EventArgs e)
        {
            //base.OnPreRender(e);
            if (_FileUploadMode != null)
            {
                if (tbFileDescription.Text.Length == 0 && !_FileUploadMode.Equals(FileUploadClass.FileUploadMode.None))
                {
                    tbFileDescription.Text = _FileUploadMode.ToDescription();
                }
                tbFileDescription.Enabled = (_FileUploadMode.Equals(FileUploadClass.FileUploadMode.None) ? true : false);
            }
        }

        void FileUpload_FileUploadSuccessHandler(UploadEventArgs e)
        {
            statusSpan.InnerHtml = e.StatusMessage;
            FileUploadSuccessHandler(e);
        }

        private Adversary.DataLayer.FileUploadClass.FileUploadMode _FileUploadMode;
        public Adversary.DataLayer.FileUploadClass.FileUploadMode FileUploadMode
        {
            get { return _FileUploadMode; }
            set { _FileUploadMode = value; }
        }




        void FileUpload_FileUploadErrorHandler(UploadEventArgs e)
        {
            statusSpan.InnerHtml = e.StatusMessage + " :: " + e.FileUploadError;
            //if (FileUploadErrorHandler != null) FileUploadErrorHandler(e);
        }

        void FileUpload_FileUploadHandler(UploadEventArgs e)
        {
            statusSpan.InnerHtml = e.StatusMessage;
            //if (FileUploadHandler != null) FileUploadHandler(e);
        }

        

        public void uploadBtn_Click(object sender, EventArgs e)
        {
            //FileUpload_FileUploadHandler(new UploadEventArgs() { StatusMessage = "File uploading" });
            if (filename.PostedFile != null)
            {
                HttpPostedFile uploadedFile = filename.PostedFile;

                if (!Adversary.Utils.FileUtils.DirectoryExists(_UploadPath))
                {
                    Adversary.Utils.FileUtils.CreateDirectory(_UploadPath);
                }

                _UploadPath += System.IO.Path.GetFileName(uploadedFile.FileName);

                if (Adversary.Utils.FileUtils.FileExists(_UploadPath))
                {
                    FileUpload_FileUploadErrorHandler(new UploadEventArgs()
                    {
                        FileUploadError = "File exists",
                        StatusMessage = "Your file was not uploaded because that file already exists: '" + System.IO.Path.GetFileName(uploadedFile.FileName) + "'"
                    });
                }
                else
                {
                    try 
	                {	        
		                filename.PostedFile.SaveAs(_UploadPath);
                        Adversary.DataLayer.FileUploadClass _fileUpload = new FileUploadClass(_ScreeningMemoID);
                        _fileUpload.FileDescription = tbFileDescription.Text;
                        _fileUpload.FileName = System.IO.Path.GetFileName(uploadedFile.FileName);
                        _fileUpload.FileUploaded = true;
                        _fileUpload.Save();
                        FileUpload_FileUploadSuccessHandler(new UploadEventArgs()
                        {
                            StatusMessage = "File upload completed successfully",
                            HasUploadError = false
                        });
                        
	                }
	                catch (Exception ex)
	                {
                        FileUpload_FileUploadErrorHandler(new UploadEventArgs()
                        {
                            FileUploadError = "File upload error",
                            StatusMessage = "Exception message: " + ex.Message
                        });
	                }
                    
                }

                
            }
        }

    }

    
}