<%@ Control AutoEventWireup="true" Language="C#" CodeBehind="FileUpload.ascx.cs" Inherits="Adversary.Web.Controls.FileUpload" %>



   
<asp:UpdatePanel ID="upnlFileUpload" runat="server" ChildrenAsTriggers="true" UpdateMode="Always">
    <Triggers>
        <asp:PostBackTrigger ControlID="uploadBtn"/>
    </Triggers>
    <ContentTemplate>
     <table width="400" cellpadding="4" class="app">
        <tr>
            <td valign="top">
                <asp:Label ID="lblFileDescriptionCaption" runat="server" />
            </td>
            <td>
                <asp:TextBox ID="tbFileDescription" runat="server" />
            </td>
        </tr>
        <tr>
            <td valign="top" width="100">
                <asp:Label ID="lblFileCaption" runat="server" />
            </td>
            <td valign="top" >
                <input type="file" id="filename" runat="server" />
            </td>
        </tr>
        <tr>
            <td valign="top" >
                <asp:UpdateProgress AssociatedUpdatePanelID="upnlFileUpload" DisplayAfter="20" ID="uprgFileUpload" runat="server">
                    <ProgressTemplate>
                        <img src="../Images/ajax-loader.gif" alt="File Uploading, please wait" />
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <span id="statusSpan" runat="server"/>
            </td>
            <td valign="top" >
                <span id="status" runat="server" />
            </td>
        </tr>
        <tr>
            <td valign="top" >
                &nbsp;
            </td>
            <td valign="top" >
                <input type="button" id="uploadBtn" value="Upload File"
                    OnServerClick="uploadBtn_Click" causesvalidation="false"
                    runat="server" />    
            </td>
        </tr>
    </table> 
    </ContentTemplate>
</asp:UpdatePanel>


