﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AutoCompleteTextbox.ascx.cs"
    Inherits="Adversary.Web.Controls.AutoCompleteTextbox" %>
<%@ Register TagPrefix="ajax" Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" %>
<asp:TextBox ID="txt" runat="server" TextMode="SingleLine" OnDataBinding="txt_DataBinding"
    OnDisposed="txt_Disposed" OnInit="txt_Init" OnLoad="txt_Load" OnPreRender="txt_PreRender"
    OnTextChanged="txt_TextChanged" OnUnload="txt_Unload" />
<ajax:AutoCompleteExtender ID="acx" runat="server" TargetControlID="txt" OnDataBinding="acx_DataBinding"
    OnDisposed="acx_Disposed" OnInit="acx_Init" OnLoad="acx_Load" OnPreRender="acx_PreRender"
    OnResolveControlID="acx_ResolveControlID" OnUnload="acx_Unload" />
