﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ApprovalTable.ascx.cs"
    Inherits="Adversary.Web.Controls.ApprovalTable" %>
<asp:FormView ID="frmApprovalTable" runat="server" DefaultMode="ReadOnly" OnPreRender="frmApprovalTable_PreRender">
    <ItemTemplate>
        <table width="100%" border="1" cellspacing="0">
            <tr>
                <td width="15%">
                    Routing
                </td>
                <td width="25%">
                    Approval
                </td>
                <td width="10%">
                    Date
                </td>
                <td rowspan="5" style="vertical-align: top; background-color: Silver;">
                    <table width="100%" border="1" cellspacing="0">
                        <tr>
                            <td colspan="2">
                                Adversary Use Only:
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 35%;">
                                Approved:
                            </td>
                            <td>
                                <%#Eval("AdversarySignature")%>
                                <%#Eval("AdversaryAdminDate", "{0:d}")%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Client/Matter:
                            </td>
                            <td>
                                <%#ClientMatterNumbers()%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Opened:
                            </td>
                            <td>
                                <%#Eval("Opened")%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Conflicts:
                            </td>
                            <td>
                                <%#Eval("Conflicts")%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Fee Splits:
                            </td>
                            <td>
                                <%#Eval("FeeSplits")%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Final Check:
                            </td>
                            <td>
                                <%#Eval("FinalCheck")%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="white-space: nowrap;">
                    Administrative Partner
                </td>
                <td>
                    <%#Eval("APSignature")%>
                </td>
                <td>
                    <%#Eval("APDAte", "{0:d}")%>
                </td>
            </tr>
            <tr>
                <td style="white-space: nowrap;">
                    Practice Group Leader
                </td>
                <td>
                    <%#Eval("PGMSignature")%>
                </td>
                <td>
                    <%#Eval("PGMDate", "{0:d}")%>
                </td>
            </tr>
            <tr>
                <td>
                    Approval for A/R over 90 days
                </td>
                <td>
                    <asp:Label ID="lblARName" runat="server"></asp:Label>
                    <%#Eval("APARSignature")%><br />
                    <%#Eval("PGMARSignature")%>
                </td>
                <td>
                    &nbsp;
                    <%--<%#Eval("APARSignatureDate")%><br />
                    <%#Eval("PGMARSignatureDate")%>--%>
                </td>
            </tr>
            <tr>
                <td>
                    Exception approval for signed engagement letter
                </td>
                <td>
                    <asp:Label ID="lblExceptionName" runat="server"></asp:Label>
                    <%#Eval("APExceptionSignature")%><br />
                    <%#Eval("PGMExceptionSignature")%>
                </td>
                <td>
                    &nbsp;
                    <%--<%#Eval("APExceptionSignatureDate")%><br />
                    <%#Eval("PGMExceptionSignatureDate")%>--%>
                </td>
            </tr>
        </table>
    </ItemTemplate>
</asp:FormView>
