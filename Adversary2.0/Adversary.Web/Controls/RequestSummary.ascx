﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RequestSummary.ascx.cs" Inherits="Adversary.Web.Controls.RequestSummary" %>
<asp:Panel ID="pnlError" runat="server" Visible="false">
    <asp:Label ID="lblError" runat="server" CssClass="error">No Request is selected.</asp:Label>
</asp:Panel>
<asp:Panel ID="pnlRequest" runat="server">
    <asp:Table ID="tblRequestSummary" runat="server" BorderStyle="Solid" BorderWidth="1px" BorderColor="Navy" CellPadding="1" CellSpacing="1">
        <asp:TableRow ID="trStatus">
            <asp:TableCell>Status</asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="lblStatus" runat="server" /></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow BackColor="#EEEEEE">
            <asp:TableCell>
            Request ID#
            </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="lblRequestID" runat="server" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow BackColor="#EEEEEE">
            <asp:TableCell>
            Submitted On
            </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="lblSubmittedOn" runat="server" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow BackColor="#EEEEEE">
            <asp:TableCell>
            Deadline Date
            </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="lblDeadlineDate" runat="server" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow BackColor="#EEEEEE">
            <asp:TableCell>
            Attorney
            </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="lblAttorneyPayrollID" runat="server" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow BackColor="#EEEEEE">
            <asp:TableCell>
            Submitted By
            </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="lblTypistLogin" runat="server" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow BackColor="#EEEEEE" ID="trClientName">
            <asp:TableCell>
            Client Name
            </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="lblClientName" runat="server" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow BackColor="#EEEEEE">
            <asp:TableCell>
            Client Status Category
            </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="lblClientStatusCategory" runat="server" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow BackColor="#EEEEEE" ID="trClientMatterNumber">
            <asp:TableCell>
            Client Matter Number (if existing client)
            </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="lblClientMatterNumber" runat="server" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow BackColor="#EEEEEE">
            <asp:TableCell>
            Practice Type/Specialty Code
            </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="lblPracticeCode" runat="server" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow BackColor="#EEEEEE">
            <asp:TableCell>
            Keep this adversary request confidential?
            </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="lblConfidential" runat="server" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow BackColor="#EEEEEE">
            <asp:TableCell VerticalAlign="Top">
            Adverse Parties
            </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="lblAdverseParties" runat="server" />
            </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow BackColor="#EEEEEE">
            <asp:TableCell VerticalAlign="Top">Affiliates of Adverse Parties</asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="lblAffiliatesofAdverseParties" runat="server" /></asp:TableCell>
        </asp:TableRow>

        <asp:TableRow BackColor="#EEEEEE">
            <asp:TableCell VerticalAlign="Top">
            Related Co-Defendants
            </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="lblRelatedCoDefendants" runat="server" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow BackColor="#EEEEEE">
            <asp:TableCell VerticalAlign="Top">
            Related Parties (not including co-defendants)
            </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="lblRelatedPartiesNotIncludingCoDefendants" runat="server" />
            </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow BackColor="#EEEEEE">
            <asp:TableCell VerticalAlign="Top">
            Other Client Affiliates
            </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="lblOtherClientAffiliates" runat="server" />
            </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow BackColor="#EEEEEE">
            <asp:TableCell VerticalAlign="Top">
            Parties to Mediation and Arbitration
            </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="lblPartiesToMediationAndArbitration" runat="server" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow BackColor="#EEEEEE">
            <asp:TableCell VerticalAlign="Top">
            Opposing Counsel
            </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="lblOpposingCounsel" runat="server" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="trNoneOfTheAbove" runat="server" Visible="false">
            <asp:TableCell>
            None of the Above
            </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="lblNoneOfTheAbove" runat="server" ForeColor="Red" Text="Yes" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="trBankruptcyMatter" runat="server" Visible="false">
            <asp:TableCell>
            This is a bankruptcy matter
            </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="lblBankruptcyMatter" runat="server" ForeColor="Red" Text="Yes" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="trTelecommunications" runat="server" Visible="false">
            <asp:TableCell>
            This is a telecommunications client or matter
            </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="lblTelecommunications" runat="server" ForeColor="Red" Text="Yes" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="trInsurance" runat="server" Visible="false">
            <asp:TableCell>
            This client is an insurance company
            </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="lblInsuranceCompany" runat="server" ForeColor="Red" Text="Yes" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="trGovernmentalEntity" runat="server" Visible="false">
            <asp:TableCell>
            This client is a governmental entity
            </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="lblGovernmentalEntity" runat="server" ForeColor="Red" Text="Yes" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="trElectricUtility" runat="server" Visible="false">
            <asp:TableCell>
            This client is an electric utility
            </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="lblElectricUtility" runat="server" ForeColor="Red" Text="Yes" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="trLegalFeesPaid" runat="server" Visible="false">
            <asp:TableCell>
            This client's legal fees will be paid by an insurance company <em>or</em> other third party
            </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="lblLegalFeesPaid" runat="server" ForeColor="Red" Text="Yes" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="trLawFirm" runat="server" Visible="false">
            <asp:TableCell>
            This client <em>or</em> adversary is a lawyer or law firm
            </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="lblLawFirm" runat="server" ForeColor="Red" Text="Yes" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="trSkiResort" runat="server" Visible="false">
            <asp:TableCell>
            This client <em>or</em> adversary is a ski resort
            </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="lblSkiResort" runat="server" ForeColor="Red" Text="Yes" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="trIndianTribe" runat="server" Visible="false">
            <asp:TableCell>
            This client <em>or</em> adversary is an Indian tribe
            </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="lblIndianTribe" runat="server" ForeColor="Red" Text="Yes" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="trOilGas" runat="server" Visible="false">
            <asp:TableCell>
            This client <em>or</em> adversary is in any segment of the oil and gas industry
            </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="lblOilGas" runat="server" ForeColor="Red" Text="Yes" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="trAccounting" runat="server" Visible="false">
            <asp:TableCell>
            This adversary is an accounting firm
            </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="lblAccounting" runat="server" ForeColor="Red" Text="Yes" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow BackColor="#EEEEEE">
            <asp:TableCell>
            Assigned To
            </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="lblAssignedTo" runat="server" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow BackColor="#EEEEEE">
            <asp:TableCell>
            Work Begun Date
            </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="lblWorkBegunDate" runat="server" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow BackColor="#EEEEEE">
            <asp:TableCell>
            Work Completed By
            </asp:TableCell>
            <asp:TableCell>
                <asp:Label ID="lblWorkCompletedBy" runat="server" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Panel>
