﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.Utils;

namespace Adversary.Web.Controls
{
    [Themeable(true)]
    public partial class ExistingFiles : System.Web.UI.UserControl
    {
        private DataLayer.FileUploadClass.FileUploadMode _FileUploadMode = DataLayer.FileUploadClass.FileUploadMode.None;

        public DataLayer.FileUploadClass.FileUploadMode FileUploadMode
        {
            get
            {
                return _FileUploadMode;
            }
            set
            {
                _FileUploadMode = value;
            }
        }

        private int _ScreeningMemoID;

        public int ScreeningMemoID
        {
            get { return _ScreeningMemoID; }
            set { _ScreeningMemoID = value; }
        }

        private string _FileUploadDirectory;

        public string FileUploadDirectory
        {
            get { return _FileUploadDirectory; }
            set { _FileUploadDirectory = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            gvExistingFiles.RowCommand += new GridViewCommandEventHandler(gvExistingFiles_RowCommand);
            gvExistingFiles.RowDataBound += new GridViewRowEventHandler(gvExistingFiles_RowDataBound);
        }

        protected void gvExistingFiles_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Adversary.DataLayer.Attachment __att = (Adversary.DataLayer.Attachment)e.Row.DataItem;
                string __fileLocation = FileUploadDirectory + ScreeningMemoID.ToString() + "\\" + __att.FileName;
                HyperLink __h = new HyperLink();
                __h.NavigateUrl = String.Format("~/download.ashx?attachmentID={0}", __att.AttachmentID.ToString());
                __h.Text = __att.FileName;
                PlaceHolder ph = (PlaceHolder)e.Row.Cells[3].FindControl("plcFileDownloadLink");
                ph.Controls.Add(__h);

                if (!System.IO.File.Exists(__fileLocation))
                {
                    __h.Enabled = false;
                    __h.ForeColor = System.Drawing.Color.DarkRed;
                    System.Web.UI.HtmlControls.HtmlGenericControl __divFileMissing = new System.Web.UI.HtmlControls.HtmlGenericControl();
                    __divFileMissing.InnerText = "* File Not Found on Directory";
                    __divFileMissing.Style.Add(HtmlTextWriterStyle.Color, "DarkRed");
                    ph.Controls.Add(__divFileMissing);
                }
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            BindFileData(); 
        }

        protected void gvExistingFiles_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            hidAttachmentID.Value = e.CommandArgument.ToString();
            BindFileData();
            mdlConfirmDelete.Show();
        }

        protected void btnConfirmDelete_Click(object sender, EventArgs e)
        {
            int _attachmentID = Convert.ToInt32(hidAttachmentID.Value);
            Adversary.DataLayer.FileUploadClass fileUploadClass = new DataLayer.FileUploadClass(ScreeningMemoID);
            fileUploadClass.Delete(_attachmentID);

            //delete the file
            FileUtils.DeleteFile(FileUploadDirectory + "\\" + ScreeningMemoID.ToString() + "\\" + fileUploadClass.FileName);
            BindFileData();
        }

        protected void btnCancelDelete_Click(object sender, EventArgs e)
        {
            mdlConfirmDelete.Hide();
            BindFileData();
        }

        private void BindFileData()
        {
            Adversary.DataLayer.FileUploadClass fuClass = new DataLayer.FileUploadClass(ScreeningMemoID);
            fuClass.FileDescription = Adversary.DataLayer.FileUploadClass.FileUploadMode.ConflictReport.ToDescription();
            List<Adversary.DataLayer.Attachment> _attachments = fuClass.GetAttachments(FileUploadMode);
            gvExistingFiles.DataSource = _attachments;
            gvExistingFiles.DataBind();
        }
    }
}