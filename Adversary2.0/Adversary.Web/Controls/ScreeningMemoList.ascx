﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ScreeningMemoList.ascx.cs"
    Inherits="Adversary.Web.Controls.ScreeningMemoList" %>
<asp:GridView ID="grvResults" runat="server" AutoGenerateColumns="false" GridLines="None"
    DataKeyNames="ScrMemID, ScrMemType" DataSourceID="odsMyMemos">
    <EmptyDataTemplate>
        <asp:Label ID="lblMyMemosEmpty" runat="server" CssClass="error" Text="No Screening Memos found." />
    </EmptyDataTemplate>
    <Columns>
        <asp:TemplateField HeaderText="">
            <ItemTemplate>
                <asp:HyperLink ID="lnkView" runat="server" NavigateUrl='<%#BuildUrl(Eval("ScrMemID"), Eval("ScrMemType"))%>'
                    Text="View" CssClass="view" />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Memo #" SortExpression="ScrMemID">
            <ItemTemplate>
                <asp:Label ID="lblMedID" runat="server" Text='<%#Eval("ScrMemID")%>' />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Matter Name" SortExpression="MatterName">
            <ItemTemplate>
                <asp:Label ID="lblMatterName" runat="server" Text='<%#Eval("MatterName")%>' />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Client">
            <ItemTemplate>
                <asp:Label ID="lblClientNo" runat="server" Text='<%#GetClientName(Container.DataItem)%>' />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Attorney">
            <ItemTemplate>
                <asp:Label ID="lblAttEntering" runat="server" Text='<%#GetPersonName(Eval("AttEntering"))%>' />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Entering Employee">
            <ItemTemplate>
                <asp:Label ID="lblPersonnelID" runat="server" Text='<%#GetPersonName(Eval("PersonnelID"))%>' />
            </ItemTemplate>
        </asp:TemplateField>
        <%--        <asp:TemplateField HeaderText="Client Matter #">
            <ItemTemplate>
                <asp:Label ID="lblClientMatterNo" runat="server" Text='<%#Eval("ClientMatterNumber")%>' />
            </ItemTemplate>
        </asp:TemplateField>--%>
    </Columns>
</asp:GridView>
<asp:ObjectDataSource ID="odsMyMemos" runat="server" OnObjectCreating="odsMyMemos_ObjectCreated"
    OnObjectDisposing="odsMyMemos_ObjectDisposing">
</asp:ObjectDataSource>
<asp:Label ID="lblCount" runat="server" />
