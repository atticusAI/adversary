﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientAutoComplete.ascx.cs"
    Inherits="Adversary.Web.Controls.ClientAutoComplete" %>
<%@ Register TagPrefix="ajax" Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" %>
<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <asp:TextBox runat="server" ID="tbClient" />
            <asp:Label ID="lblClient" runat="server" />
            <ajax:AutoCompleteExtender ID="aceClient" runat="server" MinimumPrefixLength="2"
                CompletionSetCount="20" />
        </td>
        <td>
            <asp:HiddenField ID="hidClientCode" runat="server" />
            <a href="#" onclick="javascript:<%=this.ClientID%>_clearClient()" class="clickableLink">
                clear</a>
        </td>
    </tr>
</table>
<asp:Button ID="btnRaiseEvent" runat="server" CssClass="dummyControl" OnClick="raiseEvent" />