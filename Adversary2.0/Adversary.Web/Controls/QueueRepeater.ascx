﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QueueRepeater.ascx.cs"
    Inherits="Adversary.Web.Controls.QueueRepeater" %>
<asp:Repeater ID="repQueue" runat="server" OnItemCommand="repQueue_ItemCommand" OnItemDataBound="repQueue_ItemDataBound">
    <ItemTemplate>
        <asp:Panel ID="pnlQueue" runat="server" CssClass="queue">
            <asp:Label ID="lblNotice" runat="server" CssClass="notice" Visible="false" />
            <table class="queuehdr">
                <tr>
                    <td>
                        <asp:Label ID="lblClientName" runat="server" Text='<%#GetClientDisplayName(Container.DataItem)%>' />
                    </td>
                    <td>
                        <asp:Label ID="lblMatterName" runat="server" Text='<%#Eval("MatterName")%>' />
                    </td>
                    <td>
                        <asp:Label ID="lblMemoType" runat="server" Text='<%#GetMemoTypeDescription(Container.DataItem)%>' />
                    </td>
                    <td>
                        <asp:Label ID="lblPracCode" runat="server" Text='<%#GetPracticeCodeName(Container.DataItem)%>' />
                    </td>
                    <td>
                        <asp:Label ID="lblOffice" runat="server" Text='<%#GetOfficeName(Container.DataItem)%>' />
                    </td>
                </tr>
            </table>
            <table class="queuecontent">
                <tr>
                    <td>
                        <asp:Button ID="btnView" runat="server" Text="View" CommandName="View" CommandArgument='<%#Eval("ScrMemID")%>' />
                    </td>
                    <td>
                        Memo#
                        <asp:Label ID="lblScrMemID" runat="server" Text='<%#Eval("ScrMemID")%>' />
                    </td>
                    <td>
                        Date
                        <asp:Label ID="lblDate" runat="server" Text='<%#GetSubmittedDate(Container, Container.DataItem)%>' />
                    </td>
                    <td>
                        Attorney
                        <asp:Label ID="lblAttEntering" runat="server" Text='<%#GetEnteringAttorneyName(Container.DataItem)%>' /><br />
                        Input By
                        <asp:Label ID="lblPersonnelID" runat="server" Text='<%#GetPersonnelName(Container.DataItem)%>' />
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        Description<br />
                        <asp:Label ID="lblWorkDesc" runat="server" Text='<%#Eval("WorkDesc")%>' />
                    </td>
                    <td>
                        <%#GetAPStatus(Container.DataItem)%>
                        <%#GetPGLStatus(Container.DataItem)%>
                        <%#GetAdvStatus(Container.DataItem)%>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </ItemTemplate>
</asp:Repeater>
