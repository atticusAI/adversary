﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Adversary.Web.Controls.SummarySections
{
    public partial class AdverseParties : SummarySectionBase 
    {
        protected void rptAdverseParties_PreRender(object sender, EventArgs e)
        {
            if (Memos != null && Memos.Count > 0)
            {
                ((Repeater)sender).DataSource = Memos[0].Parties.Where(p => Adversary.DataLayer.AdversaryData.AdversePartyCodes.Contains(p.PartyRelationshipCode));
                ((Repeater)sender).DataBind();
            }
        }
    }
}