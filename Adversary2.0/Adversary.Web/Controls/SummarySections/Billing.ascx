﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Billing.ascx.cs" Inherits="Adversary.Web.Controls.SummarySections.Billing" %>
<asp:Panel ID="pnlBilling" runat="server">
    <asp:FormView ID="frmBilling" runat="server" DefaultMode="ReadOnly" OnPreRender="frmBilling_PreRender">
        <EmptyDataTemplate>
            <a class="sum">Billing</a>
            <table class="sum">
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
            </table>
        </EmptyDataTemplate>
        <ItemTemplate>
            <a class="sum">Billing</a>
            <table class="sum">
                <tbody>
                    <tr>
                        <td>
                            Please describe any special fee arrangements
                        </td>
                        <td>
                            <%#Eval("SpecialFee") %>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Special billing instructions
                        </td>
                        <td>
                            <%#Eval("BillInstructions")%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Name of special bill format
                        </td>
                        <td>
                            <%#Eval("BillFormat")%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Client level billing?
                        </td>
                        <td>
                            <%#YesNo(Eval("ClientLevelBill"))%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Interest on late payments?
                        </td>
                        <td>
                            <%#YesNo(Eval("LateInterest"))%>
                        </td>
                    </tr>
            </table>
            <br />
        </ItemTemplate>
    </asp:FormView>
</asp:Panel>
