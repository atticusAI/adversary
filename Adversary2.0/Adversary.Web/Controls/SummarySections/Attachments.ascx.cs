﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Adversary.Web.Controls.SummarySections
{
    public partial class Attachments : SummarySectionBase 
    {
        protected void rptAttach_PreRender(object sender, EventArgs e)
        {
            if (Memos != null && Memos.Count > 0)
            {
                if (this.PrintMode())
                {
                    ((Repeater)sender).DataSource = Memos[0].Attachments;
                }
                else
                {
                    ((Repeater)sender).DataSource = Memos[0].Attachments.Where(a => a.Description != "Written Policy"
                        && a.Description != "Engagement Letter"
                        && a.Description != "Conflict Report"
                        && a.Description != "Rejection Letter");
                }
                ((Repeater)sender).DataBind();
            }
        }

        protected void pnlAttach_PreRender(object sender, EventArgs e)
        {
            ((Panel)sender).Visible = this.HasAdditionalAttachments();
        }


        protected string GetSectionName()
        {
            string sectionName = "Additional Attached Files";
            if (this.PrintMode())
            {
                sectionName = "Attachments";
            }
            return sectionName;
        }

    }
}