﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConflictReport.ascx.cs"
    Inherits="Adversary.Web.Controls.SummarySections.ConflictReport" %>
<asp:Panel ID="pnlConflictReport" runat="server">
    <table class="frv" cellspacing="0" style="border-collapse: collapse;">
        <tbody>
            <tr class="frvrow">
                <td colspan="2">
                    <a class="sum">Conflict Report</a>
                    <table class="sum" id="tblConflict" runat="server">
                        <tr>
                            <td>
                                List of attached Conflict Report files
                            </td>
                            <td>
                                <asp:Repeater ID="rptConflictReport" runat="server" OnPreRender="rptConflictReport_PreRender"
                                    OnItemDataBound="rpt_ItemDataBound">
                                    <ItemTemplate>
                                        <%--<asp:HyperLink CssClass="attach" ID="hypDownload" runat="server" NavigateUrl='<%#DownloadUrl(Eval("FileName"))%>'
                                            Text='<%#Eval("Description")%>' Target="_blank" />--%>
                                        <asp:HyperLink CssClass="attach" ID="hypDownload" runat="server" NavigateUrl='<%#DownloadUrl(Eval("AttachmentID"))%>'
                                            Text='<%#Eval("Description")%>' Target="_blank" />
                                        (<%#Eval("FileName")%>)
                                        <br />
                                    </ItemTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                    <table class="sum" id="tblEmpty" runat="server" onprerender="tblEmpty_PreRender">
                        <tr>
                            <td>
                                Reason why a Conflict Report will not be sent
                            </td>
                            <td>
                                <%=GetNoConflictReason()%>
                            </td>
                        </tr>
                    </table>
                    <br />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Panel>
