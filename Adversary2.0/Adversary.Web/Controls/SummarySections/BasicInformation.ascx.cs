﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.DataLayer;
using Adversary.Web.Extensions;

namespace Adversary.Web.Controls.SummarySections
{
    public partial class BasicInformation : SummarySectionBase
    {
        protected void frmBasic_PreRender(object sender, EventArgs e)
        {
            ((FormView)sender).DataSource = Memos;
            ((FormView)sender).DataBind();
        }
    }
}