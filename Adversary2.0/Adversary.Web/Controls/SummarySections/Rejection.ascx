﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Rejection.ascx.cs" Inherits="Adversary.Web.Controls.SummarySections.Rejection" %>
<asp:Panel ID="pnlEngagementLetter" runat="server">
    <table class="frv" cellspacing="0" style="border-collapse: collapse;">
        <tbody>
            <tr class="frvrow">
                <td colspan="2">
                    <a class="sum">Rejection Letter</a>
                    <table class="sum" id="tblRejection" runat="server">
                        <tr>
                            <td>
                                List of attached Rejection Letter files
                            </td>
                            <td>
                                <asp:Repeater ID="rptRejectionLetter" runat="server" OnPreRender="rptRejectionLetter_PreRender">
                                    <ItemTemplate>
                                        <asp:HyperLink CssClass="attach" ID="hypDownload" runat="server" NavigateUrl='<%#DownloadUrl(Eval("AttachmentID"))%>'
                                            Text='<%#Eval("Description")%>' Target="_blank" />
                                        (<%#Eval("FileName")%>)
                                        <br />
                                    </ItemTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                    <table class="sum" id="tblEmpty" runat="server" onprerender="tblEmpty_PreRender">
                        <tr>
                            <td>
                                Reason a Rejection Letter will not be attached
                            </td>
                            <td>
                                <%=GetNoRejectReason()%>
                            </td>
                        </tr>
                    </table>
                    <br />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Panel>
