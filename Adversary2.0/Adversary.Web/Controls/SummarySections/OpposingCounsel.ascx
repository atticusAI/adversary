﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OpposingCounsel.ascx.cs"
    Inherits="Adversary.Web.Controls.SummarySections.OpposingCounsel" %>
<asp:Panel ID="pnlOpposingCounsel" runat="server">
    <asp:FormView ID="frmOpposingCounsel" runat="server" DefaultMode="ReadOnly" OnPreRender="frmOpposingCounsel_PreRender">
        <ItemTemplate>
            <a class="sum">Opposing Counsel</a>
            <table class="sum">
                <tr>
                    <td>
                        Is there opposing counsel on this matter?
                    </td>
                    <td>
                        <%#YesNo(Eval("OppQuestion"))%>
                    </td>
                </tr>
                <asp:Repeater ID="rptOpposingCounsel" runat="server" OnPreRender="rptOpposingCounsel_PreRender"
                    Visible='<%#Convert.ToBoolean(Eval("OppQuestion"))%>'>
                    <ItemTemplate>
                        <tr>
                            <td>
                                Law Firm Name
                            </td>
                            <td>
                                <%#Eval("LawFirmName")%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Lawyer Name
                            </td>
                            <td>
                                <%#Eval("LawyerName")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
            <br />
        </ItemTemplate>
    </asp:FormView>
</asp:Panel>
