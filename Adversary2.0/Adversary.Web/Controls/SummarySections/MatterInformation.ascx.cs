﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Adversary.Web.Controls.SummarySections
{
    public partial class MatterInformation : SummarySectionBase 
    {
        protected void frmMatter_PreRender(object sender, EventArgs e)
        {
            ((FormView)sender).DataSource = Memos;
            ((FormView)sender).DataBind();
        }
    }
}