﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Reason.ascx.cs" Inherits="Adversary.Web.Controls.SummarySections.Reason" %>
<asp:Panel ID="pnlReason" runat="server">
    <asp:FormView ID="frmReason" runat="server" DefaultMode="ReadOnly" OnPreRender="frmReason_PreRender">
        <EmptyDataTemplate>
            <a class="sum">Reason</a>
            <table class="sum">
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
            </table>
        </EmptyDataTemplate>
        <ItemTemplate>
            <a class="sum">Reason</a>
            <table class="sum">
                <tr>
                    <td>
                        Reason for the Rejection
                    </td>
                    <td>
                        <%#Eval("RejectDesc")%>
                    </td>
                </tr>
            </table>
            <br />
        </ItemTemplate>
    </asp:FormView>
</asp:Panel>
