﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Adversary.Web.Controls.SummarySections
{
    public partial class StaffingInformation : SummarySectionBase 
    {
        protected void rptStaffInformation_PreRender(object sender, EventArgs e)
        {
            if (Memos != null && Memos.Count > 0)
            {
                ((Repeater)sender).DataSource = Memos[0].Staffings;
                ((Repeater)sender).DataBind();
            }
        }

        protected void lblBiller_PreRender(object sender, EventArgs e)
        {
            if (this.Memos != null && this.Memos.Count > 0)
            {
                ((Label)sender).Text = this.GetPersonName(this.Memos[0].RespAttID);
            }
        }
    }
}