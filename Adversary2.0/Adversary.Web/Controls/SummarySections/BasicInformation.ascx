﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BasicInformation.ascx.cs"
    Inherits="Adversary.Web.Controls.SummarySections.BasicInformation" %>
<asp:Panel runat="server" ID="pnlBasic">
    <asp:FormView ID="frmBasic" runat="server" OnPreRender="frmBasic_PreRender" DefaultMode="ReadOnly">
        <EmptyDataTemplate>
            <a class="sum">Basic Information</a>
            <table class="sum">
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
            </table>
        </EmptyDataTemplate>
        <ItemTemplate>
            <a class="sum">Basic Information</a>
            <table class="sum">
                <tr>
                    <td>
                        Screening Memo Number
                    </td>
                    <td>
                        <%#Eval("ScrMemID")%>
                    </td>
                </tr>
                <tr id="trProBono" runat="server" visible='<%#IsProBono()%>'>
                    <td>
                        Pro Bono Type
                    </td>
                    <td>
                        <%#GetProBonoType()%>
                    </td>
                </tr>
                <tr>
                    <td>
                        Date
                    </td>
                    <td>
                        <%#Eval("RefDate", "{0:d}")%>
                    </td>
                </tr>
                <tr>
                    <td>
                        Confidential Memo?
                    </td>
                    <td>
                        <%#YesNo(Eval("Confidential"))%>
                    </td>
                </tr>
                <tr>
                    <td>
                        Attorney Preparing Memo
                    </td>
                    <td>
                        <%#GetPersonName(Eval("AttEntering"))%>
                    </td>
                </tr>
                <tr>
                    <td>
                        Input By
                    </td>
                    <td>
                        <%#GetPersonName(Eval("PersonnelID"))%>
                    </td>
                </tr>
            </table>
            <br />
        </ItemTemplate>
    </asp:FormView>
</asp:Panel>
