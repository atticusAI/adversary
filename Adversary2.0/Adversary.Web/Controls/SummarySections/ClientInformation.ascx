﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientInformation.ascx.cs"
    Inherits="Adversary.Web.Controls.SummarySections.ClientInformation" %>
<asp:Panel runat="server" ID="pnlClient">
    <asp:FormView ID="frmClient" runat="server" OnPreRender="frmClient_PreRender" DefaultMode="ReadOnly">
        <EmptyDataTemplate>
            <a class="sum"><%=GetSectionName()%></a>
            <table class="sum">
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
            </table>
        </EmptyDataTemplate>
        <ItemTemplate>
                <a class="sum"><%=GetSectionName()%></a>
                <table class="sum">
                    <tr>
                        <td>
                            Client
                        </td>
                        <td>
                            <%#GetClientDisplayName()%>
                        </td>
                    </tr>
                    <tr runat="server" visible='<%#ShowBankruptcyRow()%>'>
                        <td>
                            Is this client a debtor in a pending bankruptcy?
                        </td>
                        <td>
                            <%#YesNo(Eval("PendingBankruptcy"))%>
                        </td>
                    </tr>
                </table>
                <br />
        </ItemTemplate>
    </asp:FormView>
</asp:Panel>
