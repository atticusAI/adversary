﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LegalWorkInformation.ascx.cs"
    Inherits="Adversary.Web.Controls.SummarySections.LegalWorkInformation" %>
<asp:Panel ID="pnlLegalWork" runat="server">
    <asp:FormView ID="frmLegalWork" runat="server" DefaultMode="ReadOnly" OnPreRender="frmLegalWork_PreRender">
        <ItemTemplate>
            <a class="sum">Legal Work Information</a>
            <table class="sum">
                <tbody>
                    <tr id="trWrkFor" runat="server" visible="<%#PrintMode()%>">
                        <td>
                            Legal Work For
                        </td>
                        <td>
                            <%#GetLegalWorkFor()%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Client
                        </td>
                        <td>
                            <%#GetClientDisplayName()%>
                        </td>
                    </tr>
                </tbody>
            </table>
            <br />
        </ItemTemplate>
    </asp:FormView>
</asp:Panel>
