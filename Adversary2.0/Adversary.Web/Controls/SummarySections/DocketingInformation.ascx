﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DocketingInformation.ascx.cs"
    Inherits="Adversary.Web.Controls.SummarySections.DocketingInformation" %>
<asp:Panel ID="pnlDocketing" runat="server">
    <asp:FormView ID="frmDocketing" runat="server" DefaultMode="ReadOnly" OnPreRender="frmDocketing_PreRender">
        <EmptyDataTemplate>
            <a class="sum">Docketing Information</a>
            <table class="sum">
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
            </table>
        </EmptyDataTemplate>
        <ItemTemplate>
            <a class="sum">Docketing Information</a>
            <table class="sum">
                <tr>
                    <td>
                        Docketing Attorney
                    </td>
                    <td>
                        <%#GetDocketingPerson(Eval("DocketingAtty"))%>
                    </td>
                </tr>
                <tr>
                    <td>
                        Docketing Staff
                    </td>
                    <td>
                        <%#GetDocketingPerson(Eval("DocketingStaff"))%>
                    </td>
                </tr>
                <tr>
                    <td>
                        Docketing Team
                    </td>
                    <td>
                        <asp:Repeater ID="rptDocketing" runat="server" OnPreRender="rptDocketing_PreRender" OnItemDataBound="rpt_ItemDataBound">
                            <ItemTemplate>
                                <%#GetPersonName(Eval("PersonnelID"))%><br />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblEmpty" runat="server" Visible="false"><strong>- No Data Available -</strong></asp:Label>
                            </FooterTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
            </table>
            <br />
        </ItemTemplate>
    </asp:FormView>
</asp:Panel>
