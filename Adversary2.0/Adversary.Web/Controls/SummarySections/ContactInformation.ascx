﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactInformation.ascx.cs"
    Inherits="Adversary.Web.Controls.SummarySections.ContactInformation" %>
<asp:Panel ID="pnlContact" runat="server">
    <table class="frv" cellspacing="0" style="border-collapse: collapse;">
        <tbody>
            <tr class="frvrow">
                <td colspan="2">
                    <a class="sum">Contact Information</a>
                    <table class="sum">
                        <asp:Repeater ID="rptContacts" runat="server" OnPreRender="rptContacts_PreRender" OnItemDataBound="rpt_ItemDataBound">
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        Contact
                                    </td>
                                    <td>
                                        <%#Eval("ContactFName")%>
                                        <%#Eval("ContactMName")%>
                                        <%#Eval("ContactLName")%>
                                        (<%#Eval("ContactTitle")%>)
                                    </td>
                                </tr>
                            </ItemTemplate>        
                            <FooterTemplate>
                                <tr id="trEmpty" runat="server" visible="false">
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                            </FooterTemplate>
                        </asp:Repeater>
                    </table>
                    <br />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Panel>
