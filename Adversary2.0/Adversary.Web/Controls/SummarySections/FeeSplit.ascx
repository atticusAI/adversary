﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FeeSplit.ascx.cs" Inherits="Adversary.Web.Controls.SummarySections.FeeSplit" %>
<asp:Panel ID="pnlFeeSplit" runat="server">
    <asp:FormView ID="frmFeeSplit" runat="server" DefaultMode="ReadOnly" OnPreRender="frmFeeSplit_PreRender">
        <ItemTemplate>
            <a class="sum">Fee Split</a>
            <table class="sum">
                <tr>
                    <td>
                        * If claiming an origination, provide justification including the percentage of
                        origination claimed by the associate(s)
                    </td>
                    <td>
                        <%#NewMatterOnly(Eval("NewMatterOnly"))%>
                    </td>
                </tr>
                <tr>
                    <td>
                        Explanation
                    </td>
                    <td>
                        <%#Eval("FeeSplitDesc")%>
                    </td>
                </tr>
                <asp:Repeater ID="rptFeeSplit" runat="server" OnPreRender="rptFeeSplit_PreRender">
                    <ItemTemplate>
                        <tr>
                            <td>
                                Personnel
                            </td>
                            <td>
                                <%#GetPersonName(Eval("PersonnelID"))%>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
            <br />
        </ItemTemplate>
    </asp:FormView>
</asp:Panel>
