﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.DataLayer;
using Adversary.Requests;
using Adversary.DataLayer.FinancialDataService;

namespace Adversary.Web.Controls
{
    public partial class RequestSummary : System.Web.UI.UserControl
    {
        private Adversary.DataLayer.Request _Request;
        public Adversary.DataLayer.Request Request
        {
            get { return _Request; }
            set { _Request = value; }
        }

        public Adversary.DataLayer.AdversaryData AdversaryDataLayer
        {
            get;
            set;
        }

        protected string GetEmployeeName(string payrollID)
        {
            string employeeName = "";
            if (AdversaryDataLayer != null)
            {
                FinancialDataEmployee emp = AdversaryDataLayer.GetEmployee(payrollID);
                if (emp != null)
                {
                    employeeName = emp.EmployeeName;
                }
            }
            return employeeName + " (" + payrollID + ")";
        }

        private string _TableSkinID;
        public string TableSkinID
        {
            get { return _TableSkinID; }
            set { _TableSkinID = value; }
        }

        private bool _AdversaryGroupView = false;
        public bool AdversaryGroupView
        {
            get { return _AdversaryGroupView; }
            set { _AdversaryGroupView = value; }
        }
        

        protected void Page_PreInit(EventArgs e)
        {
            if (!String.IsNullOrEmpty(TableSkinID)) tblRequestSummary.SkinID = TableSkinID;
        }
        
        protected override void OnLoad(EventArgs e)
        {
            if (Request == null)
            {
                this.pnlError.Visible = true;
                this.pnlRequest.Visible = false;
                return;
            }

            base.OnLoad(e);
            //set labels
            if (AdversaryGroupView)
            {
                lblStatus.Text = (Request.HH_IsRequestComplete() ? "Complete" : "Pending");
            }
            else
            {
                trStatus.Visible = false;
            }
            lblRequestID.Text = Request.ReqID.ToString();
            lblSubmittedOn.Text = (Request.SubmittalDate.HasValue ? String.Format("{0:G}", Request.SubmittalDate.Value) : string.Empty);
            lblDeadlineDate.Text = (Request.DeadlineDate.HasValue ? String.Format("{0:G}", Request.DeadlineDate.Value) : string.Empty);

            lblAttorneyPayrollID.Text = string.IsNullOrWhiteSpace(Request.AttyLogin) ? "" : this.GetEmployeeName(Request.AttyLogin);
            lblTypistLogin.Text = this.GetEmployeeName(Request.TypistLogin);

            lblClientName.Text = Request.ClientName;
            lblClientStatusCategory.Text = Request.ClientCategory;
            lblClientMatterNumber.Text = Request.ClientMatter;
            lblPracticeCode.Text = Request.PracticeCode;
            lblAdverseParties.Text = ReplaceNewlineWithHTML(Request.AdverseParties);
            lblRelatedCoDefendants.Text = ReplaceNewlineWithHTML(Request.RelatedCoDefendants);
            lblRelatedPartiesNotIncludingCoDefendants.Text = ReplaceNewlineWithHTML(Request.RelatedParties);
            lblPartiesToMediationAndArbitration.Text = ReplaceNewlineWithHTML(Request.PartiesToMediationArbitration);
            lblOpposingCounsel.Text = ReplaceNewlineWithHTML(Request.OpposingCounsel);
            lblOtherClientAffiliates.Text = ReplaceNewlineWithHTML(Request.OtherClientAffiliates);
            lblAffiliatesofAdverseParties.Text = ReplaceNewlineWithHTML(Request.AffiliatesOfAdverseParties);

            if (Request.OKforNewMatter.HasValue && Request.OKforNewMatter.Value)
            {
                lblConfidential.Text = "Yes";
                lblConfidential.ForeColor = System.Drawing.Color.Red;
                trNoneOfTheAbove.Visible = true;
            }

            if (Request.ClientCategory.ToLowerInvariant().Equals("potential"))
            {
                trClientMatterNumber.Visible = false;
            }
            ///TODO: add check for "none of the above" here and hide the row if it is false.
            if (Request.NoneOfTheAbove.HasValue && Request.NoneOfTheAbove.Value)
            {
                trNoneOfTheAbove.Visible = true;
            }
            else
            {
                trNoneOfTheAbove.Visible = false;
            }

            //if statements to display each table row if the checkbox was selected.  Also, if the checkbox was checked
            //the label text will be red.
            if (Request.AccountingFirm.HasValue && Request.AccountingFirm.Value)
            {
                trAccounting.Visible = true;
            }
            if (Request.Bankruptcy.HasValue && Request.Bankruptcy.Value)
            {
                trBankruptcyMatter.Visible = true;
            }
            if (Request.Communications.HasValue && Request.Communications.Value)
            {
                trTelecommunications.Visible = true;
            }
            if (Request.ElectricUtility.HasValue && Request.ElectricUtility.Value)
            {
                trElectricUtility.Visible = true;
            }
            if (Request.Government.HasValue && Request.Government.Value)
            {
                trGovernmentalEntity.Visible = true;
            }
            if (Request.Insurance.HasValue && Request.Insurance.Value)
            {
                trInsurance.Visible = true;
            }
            if (Request.IndianTribe.HasValue && Request.IndianTribe.Value)
            {
                trIndianTribe.Visible = true;
            }
            if (Request.LawFirm.HasValue && Request.LawFirm.Value)
            {
                trLawFirm.Visible = true;
            }
            if (Request.LegalFees.HasValue && Request.LegalFees.Value)
            {
                trLegalFeesPaid.Visible = true;
            }
            if (Request.OilGas.HasValue && Request.OilGas.Value)
            {
                trOilGas.Visible = true;
            }
            if (Request.SkiResort.HasValue && Request.SkiResort.Value)
            {
                trSkiResort.Visible = true;
            }   
        }



        private string ReplaceNewlineWithHTML(string stringToReplace)
        {
            if (String.IsNullOrEmpty(stringToReplace)) return string.Empty;
            else return stringToReplace.Replace("\r\n", "<br/>").Replace("\n", "<br/>").Replace("\r", "<br/>");
        }
    }
}