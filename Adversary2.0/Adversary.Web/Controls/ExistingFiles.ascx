﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExistingFiles.ascx.cs" Inherits="Adversary.Web.Controls.ExistingFiles" %>
<%@ Register TagPrefix="ajax" Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" %>
<div style="margin:4px 4px 4px 4px">
<asp:GridView ID="gvExistingFiles" runat="server" AutoGenerateColumns="false" ShowHeader="false"
     CellPadding="3" CellSpacing="2" BorderStyle="None">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:LinkButton ID="btnDeleteFile" runat="server"
                    CommandName="DeleteFile" CommandArgument='<%#Eval("AttachmentID")%>'
                    Text="Delete" />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="Description" />
        <asp:BoundField DataField="FileName" />
        <asp:TemplateField>
            <ItemTemplate>
                <asp:PlaceHolder runat="server" ID="plcFileDownloadLink" />
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</div>
<asp:Button ID="btnDummy" runat="server" CssClass="dummyControl" />
<ajax:ModalPopupExtender ID="mdlConfirmDelete" runat="server" TargetControlID="btnDummy" PopupControlID="pnlConfirmDelete" />
<asp:Panel ID="pnlConfirmDelete" runat="server" SkinID="HH_PanelPopup_ConfirmBox">
    <asp:HiddenField ID="hidAttachmentID" runat="server" />
    <div align="center">
        <table border="0" cellpadding="3" cellspacing="2">
            <tr>
                <td>
                    Are you sure you wish to delete this file?
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnConfirmDelete" runat="server" Text="Yes" OnClick="btnConfirmDelete_Click" />
                    &nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnCancelDelete" runat="server" Text="No" OnClick="btnCancelDelete_Click" />
                </td>
            </tr>
        </table>
    </div>
</asp:Panel>
