﻿using Adversary.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Adversary.Web.Controls
{
    public partial class ProBonoTypes : System.Web.UI.UserControl
    {
        public int SelectedProBonoType
        {
            get
            {
                return Convert.ToInt32(this.ddlProBonoTypes.SelectedValue);
            }
            set
            {
                this.ddlProBonoTypes.SelectedValue = value.ToString();
            }
        }

        protected void ddlProBonoTypes_Init(object sender, EventArgs e)
        {
            using (AdversaryData data = new AdversaryData())
            {
                ddlProBonoTypes.DataTextField = "Name";
                ddlProBonoTypes.DataValueField = "ProBonoTypeID";
                ddlProBonoTypes.DataSource = data.GetProBonoTypes();
                ddlProBonoTypes.DataBind();
            }
        }
    }
}