﻿using System;
using Adversary.Controls;
using Adversary.DataLayer;
using System.Web.UI;

namespace Adversary.Web.Controls
{
    public partial class PersonAutoComplete : System.Web.UI.UserControl
    {
        public delegate void PersonAutoCompleteHandler(object sender, PersonAutoCompletePopulatedEventArgs e);

        public event PersonAutoCompleteHandler AutoCompletePopulated;

        #region properties
        private string _TextBoxCssClass;
        [CssClassProperty]
        public string TextBoxCssClass
        {
            get 
            { 
                return _TextBoxCssClass; 
            }
            set 
            { 
                _TextBoxCssClass = value; 
            }
        }

        private string _JqueryRequiredMessage;
        public string JqueryRequiredMessage
        {
            get 
            { 
                return _JqueryRequiredMessage; 
            }
            set 
            { 
                _JqueryRequiredMessage = value; 
            }
        }

        // private string _SelectedEmployeeCode;
        public string SelectedEmployeeCode
        {
            get
            {
                // return String.IsNullOrEmpty(_SelectedEmployeeCode) ? "" : _SelectedEmployeeCode;
                return this.hidEmployeeCode.Value;
            }
            set 
            { 
                // _SelectedEmployeeCode = value; 
                this.hidEmployeeCode.Value = value;
            }
        }

        private string _SelectedEmployeeName;
        public string SelectedEmployeeName
        {
            get
            {
                return String.IsNullOrEmpty(_SelectedEmployeeName) ? "" : _SelectedEmployeeName;
            }
            set 
            { 
                _SelectedEmployeeName = value; 
            }
        }

        private string _ServiceMethod = "";
        public string ServiceMethod
        {
            get 
            { 
                return _ServiceMethod; 
            }
            set 
            { 
                _ServiceMethod = value; 
            }
        }

        private bool _PopulatedEventCausesValidation = false;
        public bool PopulatedEventCausesValidation
        {
            get 
            { 
                return _PopulatedEventCausesValidation; 
            }
            set 
            { 
                _PopulatedEventCausesValidation = value; 
            }
        }

        private bool _RaiseEventOnItemSelected = true;
        public bool RaiseEventOnItemSelected
        {
            get 
            { 
                return _RaiseEventOnItemSelected; 
            }
            set 
            { 
                _RaiseEventOnItemSelected = value; 
            }
        }

        private const string C_DISPLAYBLOCK = "display:block;";
        private const string C_DISPLAYNONE = "display:none;";

        private AdversaryData dataLayer;
        #endregion

        protected void Page_Load(object sender, EventArgs e) { }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            btnRaiseEvent.Click += new EventHandler(Local_PersonAutoCompleteHandler);
            btnRaiseEvent.CausesValidation = _PopulatedEventCausesValidation;
            this.AutoCompletePopulated += new PersonAutoCompleteHandler(PersonAutoComplete_AutoCompletePopulated);
            aceEmployee.MinimumPrefixLength = 3;
            aceEmployee.ServiceMethod = _ServiceMethod;
            aceEmployee.CompletionInterval = 100;
            aceEmployee.CompletionSetCount = 20;
            aceEmployee.TargetControlID = this.tbEmployee.ID;
            aceEmployee.OnClientItemSelected = this.ID + "_OnItemSelected";
            if (!Page.IsPostBack)
            {
                if (String.IsNullOrEmpty(_ServiceMethod)) throw new Exception("You must specify the 'ServiceMethod' property");
            }
        }

        void PersonAutoComplete_AutoCompletePopulated(object sender, PersonAutoCompletePopulatedEventArgs e)
        {
            SelectedEmployeeCode = hidEmployeeCode.Value;
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (!String.IsNullOrEmpty(TextBoxCssClass)) tbEmployee.CssClass = TextBoxCssClass;
            if (!String.IsNullOrEmpty(JqueryRequiredMessage)) tbEmployee.ToolTip = JqueryRequiredMessage;
            if (!String.IsNullOrEmpty(SelectedEmployeeCode)) hidEmployeeCode.Value = SelectedEmployeeCode;
            if (String.IsNullOrEmpty(_SelectedEmployeeName))
            {
                dataLayer = new AdversaryData();
                DataLayer.FinancialDataService.FinancialDataEmployee fEmployee = dataLayer.GetEmployee(SelectedEmployeeCode);
                if (fEmployee != null)
                {
                    _SelectedEmployeeName = fEmployee.EmployeeName;
                }
                else
                {
                    _SelectedEmployeeName = "No employee matching Employee code '" + SelectedEmployeeCode + "' could be found.";
                }
            }

            lblEmployee.Text = _SelectedEmployeeName + " (" + SelectedEmployeeCode + ")";
                
            if (String.IsNullOrEmpty(SelectedEmployeeCode))
            {
                tbEmployee.Attributes.Add("style", C_DISPLAYBLOCK);
                this.lblEmployee.Attributes.Add("style", C_DISPLAYNONE);
            }
            else
            {
                tbEmployee.Attributes.Add("style", C_DISPLAYNONE);
                this.lblEmployee.Attributes.Add("style", C_DISPLAYBLOCK);
            }

            string __itemselectedKey = "itemselected" + this.ID;
            string __itemclearedKey = "itemcleared" + this.ID;

            if(!Page.ClientScript.IsClientScriptBlockRegistered(__itemselectedKey))
                Page.ClientScript.RegisterClientScriptBlock(typeof(string), __itemselectedKey, ItemSelectedJavascript().ToString());
            if(!Page.ClientScript.IsClientScriptBlockRegistered(__itemclearedKey))
                Page.ClientScript.RegisterClientScriptBlock(typeof(string), __itemclearedKey, ItemClearedJavascript().ToString());

            aClear.Attributes.Add("onclick", "javascript:" + this.ID + "_clearEmployee();");
        }

        private System.Text.StringBuilder ItemSelectedJavascript()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder("");
            sb.Append("<script language='javascript' type='text/javascript'>");
            sb.Append("function " + this.ID + "_OnItemSelected(source, eventArgs) {");

            //#if DEBUG
            //sb.Append("alert('adding an item');");
            //#endif

            sb.Append("var t = $get('" + tbEmployee.ClientID + "');" + System.Environment.NewLine);
            sb.Append("var h = $get('" + hidEmployeeCode.ClientID + "');" + System.Environment.NewLine);
            sb.Append("var l = $get('" + lblEmployee.ClientID + "');" + System.Environment.NewLine);
            sb.Append("l.innerHTML = eventArgs.get_text();" + System.Environment.NewLine);
            sb.Append("h.value = eventArgs.get_value();" + System.Environment.NewLine);
            sb.Append("l.style.display = 'block';" + System.Environment.NewLine);
            sb.Append("t.style.display = 'none';" + System.Environment.NewLine);

            // #if DEBUG
            // sb.Append("alert(h.value);");
            // #endif

            sb.Append("var b = $get('" + btnRaiseEvent.ClientID + "');" + System.Environment.NewLine);
            //if (_RaiseEventOnItemSelected)
            //{
                sb.Append("b.click();" + System.Environment.NewLine);
            //}
            sb.Append("}</script>");
            return sb;
        }

        private System.Text.StringBuilder ItemClearedJavascript()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder("");
            sb.Append("<script language='javascript' type='text/javascript'>");
            sb.Append("function " + this.ID + "_clearEmployee() {");
            
            sb.Append("var t = $get('" + tbEmployee.ClientID + "');");
            sb.Append("var h = $get('" + hidEmployeeCode.ClientID + "');");
            sb.Append("var l = $get('" + lblEmployee.ClientID + "');");

            // #if DEBUG
            // sb.Append("alert(h.value);");
            // #endif
            
            sb.Append("t.value = ''; h.value = ''; l.innerHTML = '';");
            sb.Append("l.style.display = 'none'; t.style.display = 'block';");
            sb.Append("}</script>");
            return sb;
        }

        protected virtual void Local_PersonAutoCompleteHandler(object sender, EventArgs e)
        {
            string[] names = Adversary.Utils.StringUtils.ParseNameFromAutoCompleteText(tbEmployee.Text);

            PersonAutoCompletePopulatedEventArgs acArgs = new PersonAutoCompletePopulatedEventArgs()
            {
                PersonnelID = names[0],
                EmployeeFirstName = names[1],
                EmployeeMiddleName = names[2],
                EmployeeLastName = names[3]
            };
            this.SelectedEmployeeCode = acArgs.PersonnelID;
            OnAutoCompletePopulated(sender, acArgs);
        }

        protected void OnAutoCompletePopulated(object sender, PersonAutoCompletePopulatedEventArgs e)
        {
            if (RaiseEventOnItemSelected)
            {
                if (AutoCompletePopulated != null)
                    AutoCompletePopulated(this, e);
            }
        }
    }
}
