﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using System.Web.UI.HtmlControls;
using System.Text;
using Adversary.Web.Extensions;
using System.ComponentModel;

namespace Adversary.Web.Controls
{

    public enum ScreeningMemoListMode
    {
        UserList,
        Search,
        ApprovalSearch,
        AdminSearch
    }

    [ParseChildren(true)]
    [PersistChildren(false)]
    public partial class ScreeningMemoList : System.Web.UI.UserControl
    {
        [Browsable(false)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [MergableProperty(false)]
        public ParameterCollection SelectParameters
        {
            get
            {
                return this.odsMyMemos.SelectParameters;
            }
        }
        
        [Browsable(false)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [MergableProperty(false)]
        public System.Web.UI.WebControls.PagerSettings PagerSettings
        {
            get
            {
                return this.grvResults.PagerSettings;
            }
        }

        private Adversary.DataLayer.ScreeningMemoList __dataLayer = null;

        private Adversary.DataLayer.ScreeningMemoList DataLayer
        {
            get
            {
                if (__dataLayer == null)
                {
                    __dataLayer = new Adversary.DataLayer.ScreeningMemoList();
                }
                return __dataLayer;
            }
        }

        private object __objectInstance = null;

        public object ObjectInstance 
        {
            get
            {
                return __objectInstance;
            }

            set
            {
                __objectInstance = value;
            }
        }

        public int PageSize
        {
            get
            {
                return this.grvResults.PageSize;
            }

            set
            {
                this.grvResults.PageSize = value;
            }
        }

        public bool AllowPaging
        {
            get
            {
                return this.grvResults.AllowPaging;
            }

            set
            {
                this.grvResults.AllowPaging = value;
            }
        }

        public bool AllowSorting
        {
            get
            {
                return this.grvResults.AllowSorting;
            }

            set
            {
                this.grvResults.AllowSorting = value;
            }
        }

        public string SelectMethod
        {
            get
            {
                return this.odsMyMemos.SelectMethod;
            }

            set
            {
                this.odsMyMemos.SelectMethod = value;
            }
        }

        public string SelectCountMethod
        {
            get
            {
                return this.odsMyMemos.SelectCountMethod;
            }

            set
            {
                this.odsMyMemos.SelectCountMethod = value;
            }
        }

        public string TypeName
        {
            get
            {
                return this.odsMyMemos.TypeName;
            }

            set
            {
                this.odsMyMemos.TypeName = value;
            }
        }

        public string SortParameterName
        {
            get
            {
                return this.odsMyMemos.SortParameterName;
            }

            set
            {
                this.odsMyMemos.SortParameterName = value;
            }
        }

        public string MaximumRowsParameterName
        {
            get
            {
                return this.odsMyMemos.MaximumRowsParameterName;
            }

            set
            {
                this.odsMyMemos.MaximumRowsParameterName = value;
            }
        }

        public string StartRowIndexParameterName
        {
            get
            {
                return this.odsMyMemos.StartRowIndexParameterName;
            }

            set
            {
                this.odsMyMemos.StartRowIndexParameterName = value;
            }
        }

        //private string _requestType = null;

        //public string RequestType
        //{
        //    get
        //    {
        //        if (this._requestType == null)
        //        {
        //            this._requestType = this.Request.QueryString["type"] ?? "";
        //        }
        //        return this._requestType;
        //    }
        //}

        private ScreeningMemoListMode _mode = ScreeningMemoListMode.Search;

        public ScreeningMemoListMode Mode 
        {
            get
            {
                //switch (this.RequestType.ToLower())
                //{
                //    case "approval":
                //        this._mode = ScreeningMemoListMode.ApprovalSearch;
                //        break;
                //    case "admin":
                //        this._mode = ScreeningMemoListMode.AdminSearch;
                //        break;
                //    default:
                //        break;
                //}
                return this._mode;
            }
            set
            {
                this._mode = value;
            }
        }

        public override void Dispose()
        {
            if (__dataLayer != null)
            {
                __dataLayer.Dispose();
                __dataLayer = null;
            }
            base.Dispose();
        }

        public override void DataBind()
        {
            grvResults.DataBind();
        }

        public string SortExpression
        {
            get
            {
                return Convert.ToString(this.ViewState["SortExpression"]);
            }

            set
            {
                this.ViewState["SortExpression"] = value;
            }
        }

        public System.Web.UI.WebControls.SortDirection SortDirection
        {
            get
            {
                string d = Convert.ToString(this.ViewState["SortDirection"]);
                return d == System.Web.UI.WebControls.SortDirection.Ascending.ToString()
                    ? System.Web.UI.WebControls.SortDirection.Ascending
                    : System.Web.UI.WebControls.SortDirection.Descending;
            }

            set
            {
                this.ViewState["SortDirection"] = value.ToString();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void odsMyMemos_ObjectCreated(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = this.ObjectInstance;
        }

        protected void odsMyMemos_ObjectDisposing(object sender, ObjectDataSourceDisposingEventArgs e)
        {
            e.Cancel = true;
        }

        protected string BuildUrl(object scrMemID, object scrMemType)
        {
            string url = "";
            if (scrMemID.HH_IsValidInt() && scrMemType.HH_IsValidInt())
            {
                string baseUrl = "";
                switch (Mode)
                {
                    case ScreeningMemoListMode.AdminSearch:
                        // baseUrl = "~/Admin/AdminProcessing.aspx";
                        baseUrl = "~/Admin/Approval.aspx";
                        break;
                    case ScreeningMemoListMode.ApprovalSearch:
                        baseUrl = "~/Admin/Approval.aspx";
                        break;
                    default:
                        baseUrl = "~/Screening/Summary.aspx";
                        break;
                }
                url = string.Format("{0}?ScrMemID={1}&ScrMemType={2}", baseUrl, Convert.ToInt32(scrMemID), Convert.ToInt32(scrMemType));
            }
            return url;
        }

        protected string GetPersonName(object personID)
        {
            string name = "";

            if (personID.HH_IsValidInt())
            {
                try
                {
                    name = DataLayer.GetPersonName((string)personID);
                }
                catch (Exception ex)
                {
                    name = "<span class=\"error\">Name lookup failed.</span>";
                }
            }
            
            return name;
        }

        protected string GetClientName(object memo)
        {
            StringBuilder sb = new StringBuilder();
            if (memo is Adversary.DataLayer.Memo)
            {
                Adversary.DataLayer.Memo scrMem = (Adversary.DataLayer.Memo)memo;
                if (Convert.ToInt32(scrMem.ScrMemType).Equals(1))
                {
                    bool blnIndivCompanyChosen = false;
                    bool blnIsCompany = false;
                    bool blnIsIndividual = false;
                    if (scrMem.Company.HasValue || scrMem.Individual.HasValue)
                    {
                        blnIndivCompanyChosen = true;
                        if (scrMem.Company.HasValue)
                        {
                            if (Convert.ToBoolean(scrMem.Company))
                                blnIsCompany = true;
                        }
                        if (scrMem.Individual.HasValue)
                        {
                            if (Convert.ToBoolean(scrMem.Individual))
                                blnIsIndividual = true;
                        }
                    }

                    if ((blnIndivCompanyChosen && (blnIsIndividual || blnIsCompany)) && scrMem.NewClients != null && scrMem.NewClients.Count > 0)
                    {
                        if (Convert.ToBoolean(scrMem.Individual))
                        {
                            sb.Append(scrMem.NewClients[0].FName);
                            sb.Append(string.IsNullOrEmpty(scrMem.NewClients[0].FName) ? string.Empty : " ");
                            sb.Append(scrMem.NewClients[0].MName);
                            sb.Append(string.IsNullOrEmpty(scrMem.NewClients[0].MName) ? string.Empty : " ");
                            sb.Append(scrMem.NewClients[0].LName);
                        }
                        else if (Convert.ToBoolean(scrMem.Company))
                        {
                            sb.Append(scrMem.NewClients[0].CName);
                        }
                    }
                }
            }
            return sb.ToString();
        }

    }
}
