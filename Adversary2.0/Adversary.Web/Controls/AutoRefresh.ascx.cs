﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Adversary.Web.Controls
{
    public partial class AutoRefresh : System.Web.UI.UserControl
    {
        public int Interval
        {
            get
            {
                return this.timRefresh.Interval;
            }
            set
            {
                this.timRefresh.Interval = value;
            }
        }

        public int IntervalSeconds
        {
            get
            {
                return this.timRefresh.Interval / 1000;
            }
            set
            {
                this.timRefresh.Interval = value * 1000;
            }
        }

        public string PostbackURL
        {
            get;
            set;
        }

        public event EventHandler Refresh = null;

        protected void timRefresh_Tick(object sender, EventArgs e)
        {
            if (this.Refresh != null)
            {
                this.Refresh(this, e);
            }
            
            if (!string.IsNullOrWhiteSpace(this.PostbackURL))
            {
                this.Response.Redirect(this.PostbackURL);
            }
        }

        protected void timRefresh_Init(object sender, EventArgs e)
        {
            if (this.Refresh != null || !(string.IsNullOrWhiteSpace(this.PostbackURL)))
            {
                this.timRefresh.Tick += new EventHandler<EventArgs>(timRefresh_Tick);
            }
        }
    }
}