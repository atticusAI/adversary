﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.Screening;
using Adversary.DataLayer;
using System.Drawing;

namespace Adversary.Web.Controls
{
    public enum MessageDisplayType
    {
        DisplayError,
        DisplayValid,
        DisplayWarning,
        DisplayDefault,
        DisplayInfo,
        DisplayMessage,
        DisplayNone
    }

    public partial class ErrorSummary : System.Web.UI.UserControl
    {
        private System.Drawing.Color _MessageColor = Color.Black;

        
        //set a default error icon
        private string _ErrorIconPath = "~/Images/red_x_lrg.png";

        public string ErrorIconPath
        {
            get { return _ErrorIconPath; }
            set { _ErrorIconPath = value; }
        }
        
        //set a default valid icon
        private string _ValidIconPath = "~/images/green_check.jpg";

        public string ValidIconPath
        {
            get { return _ValidIconPath; }
            set { _ValidIconPath = value; }
        }

        //set a default info icon
        private string _InfoIconPath = "~/images/Info_box_blue.png";

        public string InfoIconPath
        {
            get { return _InfoIconPath; }
            set { _InfoIconPath = value; }
        }

        private string _WarningIconPath = "~/images/warning.png";

        public string WarningIconPath
        {
            get { return _WarningIconPath; }
            set { _WarningIconPath = value; }
        }

        private string _MessageIconPath = "~/images/e_mail.png";

        public string MessageIconPath
        {
            get { return _MessageIconPath; }
            set { _MessageIconPath = value; }
        }
        

        private List<string> _ErrorList;

        public List<string> ErrorList
        {
            get
            { 
                if(_ErrorList == null) _ErrorList = new List<string>();
                return _ErrorList; 
            }
            set { _ErrorList = value; }
        }

        private string _DocumentToValidate;

        public string DocumentToValidate
        {
            get { return _DocumentToValidate; }
            set { _DocumentToValidate = value; }
        }

        private string _DisplayMessage;

        public string DisplayMessage
        {
            get
            {
                return _DisplayMessage;
            
            
            }
            set { _DisplayMessage = value; }
        }

        private string _StatusDescription;

        public string StatusDescription
        {
            get { return _StatusDescription; }
            set { _StatusDescription = value; }
        }

        private MessageDisplayType _DisplayType = MessageDisplayType.DisplayDefault;
        public MessageDisplayType DisplayType
        {
            get { return _DisplayType; }
            set
            {
                switch (value)
                {
                    case MessageDisplayType.DisplayError:
                        _MessageColor = Color.Red;
                        break;
                    case MessageDisplayType.DisplayValid:
                        _MessageColor = Color.Green;
                        break;
                    case MessageDisplayType.DisplayWarning:
                        _MessageColor = Color.DarkOrange;
                        break;
                    case MessageDisplayType.DisplayInfo:
                        _MessageColor = Color.Navy;
                        break;
                    case MessageDisplayType.DisplayDefault:
                    default:
                        break;
                }
                _DisplayType = value; 
            
            }
        }

        private bool _HideIcon = false;

        public bool HideIcon
        {
            get { return _HideIcon; }
            set { _HideIcon = value; }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
           
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            //if (String.IsNullOrEmpty(ErrorIconPath) && String.IsNullOrEmpty(ValidIconPath))
            //    imgIcon.Visible = false;

            lblErrorSummaryMessage.Text = DisplayMessage;
            lblErrorSummaryMessage.ForeColor = _MessageColor;

            if (ErrorList != null)
            {
                if (ErrorList.Count > 0)
                {
                    rptErrors.DataSource = ErrorList;
                    rptErrors.DataBind();
                }
            }

            
            
            string _iconPath = string.Empty;
            switch (DisplayType)
            {
                case MessageDisplayType.DisplayError:
                    _iconPath = ErrorIconPath;
                    break;
                case MessageDisplayType.DisplayValid:
                    _iconPath = ValidIconPath;
                    break;
                case MessageDisplayType.DisplayWarning:
                    _iconPath = WarningIconPath;
                    break;
                case MessageDisplayType.DisplayInfo:
                    _iconPath = InfoIconPath;
                    break;
                case MessageDisplayType.DisplayMessage:
                    _iconPath = MessageIconPath;
                    break;
                case MessageDisplayType.DisplayDefault:
                case MessageDisplayType.DisplayNone:
                    _iconPath = String.Empty;
                    HideIcon = true;
                    break;
                default:
                    break;
            }
            //if (DisplayType.Equals(MessageDisplayType.DisplayError))
            //{
            //    imgIcon.Visible = true;
            //    imgIcon.ImageUrl = ErrorIconPath;
            //}
            //else if (DisplayType.Equals(MessageDisplayType.DisplayValid))
            //{
            //    imgIcon.Visible = true;
            //    imgIcon.ImageUrl = ValidIconPath;
            //}
            //else
            //{
            //    imgIcon.Visible = false;
            //}
            if (!String.IsNullOrEmpty(_iconPath))
                imgIcon.ImageUrl = _iconPath;
            
            if (!HideIcon || String.IsNullOrEmpty(_iconPath))
            {
                imgIcon.Visible = !HideIcon;
            }

            #region old code
            //base.OnPreRender(e);
            //if (String.IsNullOrEmpty(_ErrorIconPath) && String.IsNullOrEmpty(_ValidIconPath))
            //    imgIcon.Visible = false;

            //lblErrorSummaryMessage.Text = DisplayMessage;

            //if (_ErrorList != null)
            //{
            //    if (_ErrorList.Count > 0)
            //    {
            //        lblErrorSummaryMessage.ForeColor = System.Drawing.Color.Red;
            //        imgIcon.ImageUrl = ErrorIconPath;
            //        rptErrors.DataSource = ErrorList;
            //        rptErrors.DataBind();
            //    }
            //    else
            //    {
            //        lblErrorSummaryMessage.ForeColor = System.Drawing.Color.Green;
            //        imgIcon.ImageUrl = ValidIconPath;
            //    }
            //}



            //if (DisplayType == null)
            //{
            //    if (_ErrorList != null)
            //    {
            //        if (_ErrorList.Count > 0)
            //        {
            //            lblErrorSummaryMessage.ForeColor = System.Drawing.Color.Red;
            //            imgIcon.ImageUrl = ErrorIconPath;
            //            rptErrors.DataSource = ErrorList;
            //            rptErrors.DataBind();
            //        }
            //        else
            //        {
            //            lblErrorSummaryMessage.ForeColor = System.Drawing.Color.Green;
            //            imgIcon.ImageUrl = ValidIconPath;
            //        }
            //    }
            //}
            //else
            //{
            //    lblErrorSummaryMessage.ForeColor = _MessageColor;
            //    if (_ErrorList != null)
            //    {
            //        if (_ErrorList.Count > 0)
            //        {
            //            rptErrors.DataSource = ErrorList;
            //            rptErrors.DataBind();
            //        }
            //    }
            //}
            #endregion
        }
    }
}