﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Adversary.Web.Controls
{
    public partial class PrintViewLink : System.Web.UI.UserControl
    {

        public int ScrMemID
        {
            get;
            set;
        }

        protected void lnkPrintView_PreRender(object sender, EventArgs e)
        {
            lnkPrintView.HRef = ResolveUrl("~/Screening/PrintView.aspx") + "?ScrMemID=" + this.ScrMemID.ToString();
        }
    }
}