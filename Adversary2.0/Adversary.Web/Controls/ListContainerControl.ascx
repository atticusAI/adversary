﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ListContainerControl.ascx.cs"
    Inherits="Adversary.Web.Controls.ListContainerControl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<script type="text/javascript" language="javascript">
    function <%=this.ClientID%>_showMPX(behaviorID) {
        $find(behaviorID).show();
    }
</script>
<asp:Button ID="btnPopAdd" runat="server" />
<asp:GridView ID="grv" runat="server" OnDataBinding="grv_DataBinding" OnDataBound="grv_DataBound"
    OnDisposed="grv_Disposed" OnInit="grv_Init" OnLoad="grv_Load" OnPageIndexChanged="grv_PageIndexChanged"
    OnPageIndexChanging="grv_PageIndexChanging" OnPreRender="grv_PreRender" OnRowCancelingEdit="grv_RowCancelingEdit"
    OnRowCommand="grv_RowCommand" OnRowCreated="grv_RowCreated" OnRowDataBound="grv_RowDataBound"
    OnRowDeleted="grv_RowDeleted" OnRowDeleting="grv_RowDeleting" OnRowEditing="grv_RowEditing"
    OnRowUpdated="grv_RowUpdated" OnRowUpdating="grv_RowUpdating" OnSelectedIndexChanged="grv_SelectedIndexChanged"
    OnSelectedIndexChanging="grv_SelectedIndexChanging" OnSorted="grv_Sorted" OnSorting="grv_Sorting"
    OnUnload="grv_Unload">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:LinkButton Text="Edit" ID="btnChange" CommandName="Edit" runat="server" Visible="false" />
                <asp:LinkButton Text="Delete" ID="btnDelete" CommandName="Delete" runat="server"
                    Visible="false" />
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<asp:ModalPopupExtender ID="mpxAdd" runat="server" TargetControlID="btnPopAdd" PopupControlID="pnlAdd"
    CancelControlID="btnAddCancel" />
<asp:Panel ID="pnlAdd" runat="server" Style="display: none;">
    <asp:FormView ID="frmAdd" runat="server" DefaultMode="Insert" OnItemInserted="frmAdd_ItemInserted"
        OnItemInserting="frmAdd_ItemInserting" />
    <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click" />
    <asp:Button ID="btnAddCancel" runat="server" Text="Cancel" />
</asp:Panel>
<asp:Button ID="btnPopHid" runat="server" Style="display: none;" />
<asp:ModalPopupExtender ID="mpxChange" runat="server" TargetControlID="btnPopHid"
    PopupControlID="pnlChange" CancelControlID="btnChangeCancel" />
<asp:Panel ID="pnlChange" runat="server" Style="display: none">
    <asp:UpdatePanel ID="upChange" runat="server" RenderMode="Inline" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:FormView ID="frmChange" runat="server" DefaultMode="Edit" OnPreRender="frmChange_PreRender"
                OnItemUpdated="frmChange_ItemUpdated" OnItemUpdating="frmChange_ItemUpdating" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="grv" EventName="RowCommand" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:Button ID="btnChange" runat="server" Text="Update" OnClick="btnChange_Click" />
    <asp:Button ID="btnChangeCancel" runat="server" Text="Cancel" />
</asp:Panel>
<asp:ModalPopupExtender ID="mpxDelete" runat="server" TargetControlID="btnPopHid"
    PopupControlID="pnlDelete" CancelControlID="btnDeleteCancel" />
<asp:Panel ID="pnlDelete" runat="server" Style="display: none;">
    <asp:UpdatePanel ID="upDelete" runat="server" RenderMode="Inline" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:FormView ID="frmDelete" runat="server" DefaultMode="ReadOnly" OnPreRender="frmDelete_PreRender"
                OnItemDeleted="frmDelete_ItemDeleted" OnItemDeleting="frmDelete_ItemDeleting" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="grv" EventName="RowCommand" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:Button ID="btnDelete" runat="server" Text="Delete" OnClick="btnDelete_Click" />
    <asp:Button ID="btnDeleteCancel" runat="server" Text="Cancel" />
</asp:Panel>
