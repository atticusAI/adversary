﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PersonAutoComplete.ascx.cs"
    Inherits="Adversary.Web.Controls.PersonAutoComplete" %>
<%@ Register TagPrefix="ajax" Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" %>
<asp:UpdatePanel runat="server" ID="upnl_PersonAutoComplete" ChildrenAsTriggers="false"
    UpdateMode="Conditional">
    <ContentTemplate>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <asp:TextBox runat="server" ID="tbEmployee" />
                    <asp:Label ID="lblEmployee" runat="server" />
                    <ajax:AutoCompleteExtender ID="aceEmployee" runat="server" MinimumPrefixLength="2"
                        CompletionSetCount="20" />
                </td>
                <td>
                    <asp:HiddenField ID="hidEmployeeCode" runat="server" />
                    <a runat="server" id="aClear" class="clickableLink">clear</a>
                </td>
            </tr>
        </table>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnRaiseEvent" EventName="Click" />
    </Triggers>
</asp:UpdatePanel>
<asp:Button ID="btnRaiseEvent" runat="server" CssClass="dummyControl" />
