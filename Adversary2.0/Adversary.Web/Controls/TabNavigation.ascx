﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TabNavigation.ascx.cs"
    Inherits="Adversary.Web.Controls.TabNavigation" %>
<asp:Repeater runat="server" ID="rptNav" OnPreRender="rptNav_PreRender" OnItemDataBound="rptNav_ItemDataBound">
    <ItemTemplate>
        <asp:LinkButton ID="lbNavigate" runat="server" CausesValidation="true" CommandName="Navigate"
            CssClass="tab" Text='<%#Eval("Name")%>' ToolTip='<%#Eval("Description")%>'
            PostBackUrl='<%#Eval("PageURL")%>' />
    </ItemTemplate>
</asp:Repeater>
