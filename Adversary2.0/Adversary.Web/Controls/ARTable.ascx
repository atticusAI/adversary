﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ARTable.ascx.cs" Inherits="Adversary.Web.Controls.ARTable" %>
<asp:FormView ID="frmARBalance" runat="server" DefaultMode="ReadOnly" OnPreRender="frmARBalance_PreRender">
    <EmptyDataTemplate>
        <span class="error">Could not retrieve A/R Balance for this client</span>
    </EmptyDataTemplate>
    <ItemTemplate>
        <table runat="server" visible="<%#HasBalance(Container.DataItem)%>" class="ar">
            <tr>
                <td colspan="5">
                    <span>This client has a current A/R balance</span>
                </td>
            </tr>
            <tr>
                <td>
                    Current
                </td>
                <td>
                    31 to 60 Day
                </td>
                <td>
                    61 to 90 Day
                </td>
                <td>
                    91 to 120 Day
                </td>
                <td>
                    121+ Days
                </td>
            </tr>
            <tr>
                <td>
                    <%#Eval("Current", "{0:c}")%>
                </td>
                <td>
                    <%#Eval("Days_31_to_60", "{0:c}")%>
                </td>
                <td>
                    <%#Eval("Days_61_to_90", "{0:c}")%>
                </td>
                <td>
                    <%#Eval("Days_91_to_120", "{0:c}")%>
                </td>
                <td>
                    <%#Eval("Days_121_Plus", "{0:c}")%>
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    Total amount owed by this client is <span style='color: red;'>
                        <%#Eval("TotalAR", "{0:c}")%></span>
                </td>
            </tr>
        </table>
        <br />
        <span runat="server" visible="<%#!HasBalance(Container.DataItem)%>" style="color: blue;">
            This client does not have a current A/R balance</span><br />
    </ItemTemplate>
</asp:FormView>
