﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Adversary.Web.Controls
{
    public partial class PageMessage : System.Web.UI.UserControl
    {
        public void SetMessage(string message, string cssClass)
        {
            this.litMessage.Text = string.Format("<span class=\"msg {0}\">{1}</span>", cssClass, message);
        }

        public void ClearMessage()
        {
            this.litMessage.Text = "";
        }

        public void ErrorMessage(string message)
        {
            this.SetMessage(message, "error");
        }

        public void InfoMessage(string message)
        {
            this.SetMessage(message, "info");
        }

        public void WarnMessage(string message)
        {
            this.SetMessage(message, "warn");
        }
    }
}