﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ScreeningMemoSearch.ascx.cs"
    Inherits="Adversary.Web.Controls.ScreeningMemoSearch" %>
<%@ Register Src="../Controls/ClientAutoComplete.ascx" TagName="ClientAutoComplete"
    TagPrefix="adv" %>
<%@ Register Src="../Controls/PersonAutoComplete.ascx" TagName="PersonAutoComplete"
    TagPrefix="adv" %>
<%@ Register Src="../Controls/ScreeningMemoList.ascx" TagName="ScreeningMemoList"
    TagPrefix="adv" %>
<%@ Register Src="../Controls/AutoCompleteTextbox.ascx" TagName="AutoCompleteTextbox"
    TagPrefix="adv" %>
<div class="notice">
    Looking for a memo that was created prior to December 1st, 2010? If you used the
    old screening memo site to create your memo <a href="http://vhhcf01.hollandhart.com/samedi/website/htdocs/acct/adversary/scrmem/dev/"
        target="_blank">you can click here to search for it</a>.
</div>
<div class="clear">
</div>
<asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
    <table cellpadding="2px;">
        <tr>
            <td>
                Client
            </td>
            <td>
                <adv:ClientAutoComplete ID="cacClient" runat="server" ServiceMethod="ClientService"
                    cssclass="search" />
            </td>
        </tr>
        <tr>
            <td>
                Matter Name
            </td>
            <td>
                <adv:AutoCompleteTextbox ID="actMatter" runat="server" ServiceMethod="MatterNameAutoCompleteService"
                    CssClass="search" />
            </td>
        </tr>
        <tr>
            <td>
                Client Matter Number
            </td>
            <td>
                <adv:AutoCompleteTextbox ID="actClientMatter" runat="server" ServiceMethod="ClientMatterNoAutoCompleteService"
                    CssClass="search" />
            </td>
        </tr>
        <tr>
            <td>
                Attorney Preparing Memo
            </td>
            <td>
                <adv:PersonAutoComplete ID="pacAttorney" runat="server" ServiceMethod="TimekeeperService"
                    cssclass="search" />
            </td>
        </tr>
        <tr>
            <td>
                Input By
            </td>
            <td>
                <adv:PersonAutoComplete ID="pacInput" runat="server" ServiceMethod="TimekeeperService"
                    cssclass="search" />
            </td>
        </tr>
        <tr>
            <td>
                Screening Memo Number
            </td>
            <td>
                <adv:AutoCompleteTextbox ID="actMemoNo" runat="server" ServiceMethod="ScreeningMemoNoAutocompleteService"
                    CssClass="search" />
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;">
                <asp:Button ID="btnSearch" runat="server" Text="Search" Width="100px" />
                <asp:Button ID="btnClear" runat="server" Text="Clear" Width="100px" />
            </td>
        </tr>
    </table>
</asp:Panel>
