﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.DataLayer;
using Adversary.Web.Extensions;
using System.Configuration;

namespace Adversary.Web.Controls
{
    public partial class ScreeningMemoLock : System.Web.UI.UserControl
    {
        public int ScrMemID
        {
            get
            {
                int scrMemID = -1;
                int.TryParse(HttpContext.Current.Request.QueryString["ScrMemID"], out scrMemID);
                return scrMemID;
            }
        }

        public string LoginName
        {
            get
            {
                return HttpContext.Current.User.Identity.Name;
            }
        }

        public void RenewExpriration()
        {
            AdversaryLock al = AdversaryLocking.Instance.GetLock(this.ScrMemID);
            if (al != null)
            {
                al.RenewExpiration();
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
            this.RunLockChecks();
        }

        private void UnlockMyExpired()
        {
            AdversaryLock al = AdversaryLocking.Instance.GetLock(this.ScrMemID);
            if (al != null)
            {
                if (!al.Locked && al.Expired && al.Login == "" && al.UnlockedBy == this.LoginName)
                {
                    AdversaryLocking.Instance.RemoveLock(this.ScrMemID);
                }
            }
        }

        private void UnlockOtherExpired()
        {
            AdversaryLock al = AdversaryLocking.Instance.GetLock(this.ScrMemID);
            if (al != null)
            {
                if (!al.Locked && al.Expired && al.Login == "" && al.UnlockedBy != this.LoginName)
                {
                    AdversaryLocking.Instance.RemoveLock(this.ScrMemID);
                }
            }
        }

        private void RunLockChecks()
        {
            if (this.ScrMemID > 0)
            {
                if (!this.Page.IsPostBack)
                {
                    this.UnlockMyExpired();
                }
                this.UnlockOtherExpired();

                
                AdversaryLock al = AdversaryLocking.Instance.GetLock(this.ScrMemID);

                if (al == null)
                {
                    al = new AdversaryLock(this.ScrMemID, this.LoginName);
                    AdversaryLocking.Instance.AddLock(this.ScrMemID, al);
                }
                else
                {
                    if (!al.Locked && al.UnlockedBy != this.LoginName && al.Expired)
                    {
                        al.Lock(this.LoginName);
                    }
                }                
                
                if (al.Locked && al.Login != this.LoginName && !al.Expired)
                {
                    this.lblMsg.Text = string.Format("Memo {0} is currently locked by {1}", this.ScrMemID, al.Login);
                    this.lblLockMsg.Text = lblMsg.Text;
                    this.lnkUnlock.Visible = true;
                    this.lnkAcknowledge.Visible = false;
                    this.lnkBack.Visible = false;
                    this.mpxLock.Show();
                }
                else if (al.Locked && al.Login == this.LoginName && !al.Expired)
                {
                    this.lblMsg.Text = string.Format("You currently have a lock on memo {0}", this.ScrMemID);
                    this.lblLockMsg.Text = lblMsg.Text;
                    this.lnkUnlock.Visible = false;
                    this.lnkAcknowledge.Visible = false;
                    this.lnkBack.Visible = false;
                    this.mpxLock.Hide();
                }
                else if (!al.Locked && al.UnlockedBy == this.LoginName && al.Expired)
                {
                    this.lblMsg.Text = string.Format("Your lock on memo {0} has expired", this.ScrMemID);
                    this.lblLockMsg.Text = lblMsg.Text;
                    this.lnkAcknowledge.Visible = false;
                    this.lnkUnlock.Visible = false;
                    this.lnkBack.Visible = true;
                    this.mpxLock.Show();
                    this.timLock.Enabled = false;
                }
                else if (!al.Locked && al.UnlockedBy == this.LoginName && !al.Expired)
                {
                    this.lblMsg.Text = string.Format("You removed the lock on memo {0}.  Please wait for the user to acknowledge the unlock request.", this.ScrMemID);
                    this.lblLockMsg.Text = lblMsg.Text;
                    this.lnkUnlock.Visible = false;
                    this.lnkAcknowledge.Visible = false;
                    this.lnkBack.Visible = false;
                    this.mpxLock.Show();
                }
                else if (!al.Locked && al.UnlockedBy != this.LoginName)
                {
                    this.lblMsg.Text = string.Format("{0} has removed your lock on memo {1}", al.UnlockedBy, this.ScrMemID);
                    this.lblLockMsg.Text = lblMsg.Text;
                    this.lnkUnlock.Visible = false;
                    this.lnkAcknowledge.Visible = true;
                    this.lnkBack.Visible = false;
                    this.mpxLock.Show();
                    this.timLock.Enabled = false;
                }
                this.upLock.Update();
            }
        }

        protected void timLock_Tick(object sender, EventArgs e)
        {
        }

        protected void lnkUnlock_Click(object sender, EventArgs e)
        {
            if (this.ScrMemID > 0)
            {
                AdversaryLock al = AdversaryLocking.Instance.GetLock(this.ScrMemID);
                if (al != null && al.Locked)
                {
                    al.Unlock(this.LoginName);
                    this.lblMsg.Text = string.Format("You removed the lock on memo {0}.  Please wait for the user to acknowledge the unlock request.", this.ScrMemID);
                    this.lblLockMsg.Text = lblMsg.Text;
                    this.lnkUnlock.Visible = false;
                    this.lnkAcknowledge.Visible = false;
                    this.lnkBack.Visible = false;
                    this.mpxLock.Show();
                }
                this.upLock.Update();
            }
        }

        protected void lnkAcknowledge_Click(object sender, EventArgs e)
        {
            if (this.ScrMemID > 0)
            {
                AdversaryLock al = AdversaryLocking.Instance.GetLock(this.ScrMemID);
                if (al != null)
                {
                    al.Lock(al.UnlockedBy);
                    Response.Redirect("~/Admin/AdminQueue.aspx");
                }
            }
        }
    }

    public class AdversaryLock
    {
        public int LockExpirationTime
        {
            get
            {
                int expirationInterval = 0;
                try
                {
                    if (!Int32.TryParse(ConfigurationManager.AppSettings["LockExpirationTime"], out expirationInterval))
                    {
                        expirationInterval = 0;
                    }
                }
                catch { }
                return expirationInterval;
            }
        }

        private int _scrMemID;
        
        public int ScrMemID
        {
            get
            {
                return this._scrMemID;
            }
        }

        private string _login;
        
        public string Login
        {
            get
            {
                return this._login;
            }
        }

        private DateTime _expiration = DateTime.Now;

        public DateTime Expiration 
        {
            get
            {
                return this._expiration;
            }
        }

        private bool _locked = false;
        
        public bool Locked
        {
            get
            {
                return this._locked;
            }
        }

        private string _unlockedBy;
        
        public string UnlockedBy
        {
            get
            {
                return this._unlockedBy;
            }
        }

        public bool Expired
        {
            get
            {
                return this.LockExpirationTime > 0
                    ? (this._expiration <= DateTime.Now)
                    : false;
            }
        }

        public AdversaryLock(int scrMemId, string login)
        {
            this._scrMemID = scrMemId;
            this._login = login;
            this._locked = true;
            this.RenewExpiration();
        }

        public void Unlock(string login)
        {
            if (this._locked)
            {
                this._locked = false;
                this._login = "";
                this._unlockedBy = login;
            }
        }

        public void Lock(string login)
        {
            if (!this._locked)
            {
                this._locked = true;
                this._login = login;
                this._unlockedBy = "";
            }
            this.RenewExpiration();
        }

        public void RenewExpiration()
        {
            this._expiration = DateTime.Now.AddMinutes(LockExpirationTime);
        }
    }

    /// <summary>
    /// Adversary requested a method for "locking" screening memos from being edited by multiple people when 
    /// they were being processed, this class handles the locking
    /// </summary>
    public class AdversaryLocking
    {
        private static AdversaryLocking _instance = null;
        private static readonly object _syncRoot = new object();

        private Dictionary<int?, AdversaryLock> _locks = new Dictionary<int?, AdversaryLock>();

        public static AdversaryLocking Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_syncRoot)
                    {
                        if (_instance == null)
                        {
                            _instance = new AdversaryLocking();
                        }
                    }
                }
                else
                {
                    CheckExpiration();
                }
                return _instance;
            }
        }

        public static int StaleLockDays
        {
            get
            {
                int staleLockDays = 0;
                try
                {
                    if (!Int32.TryParse(ConfigurationManager.AppSettings["StaleLockDays"], out staleLockDays))
                    {
                        staleLockDays = 0;
                    }
                }
                catch { }
                return staleLockDays;
            }
        }

        private static void CheckExpiration()
        {
            lock (_syncRoot)
            {
                List<int?> remove = new List<int?>();
                foreach (AdversaryLock al in _instance._locks.Values)
                {
                    if (al.Expired)
                    {
                        al.Unlock(al.Login); // unlock all that have expired
                    }
                    
                    // cleanup stale locks
                    if (StaleLockDays > 0)
                    {
                        TimeSpan diff = DateTime.Now.Subtract(al.Expiration);
                        if (diff.Days >= StaleLockDays)
                        {
                            remove.Add(al.ScrMemID);
                        }
                    }
                }
                foreach (int? scrMemId in remove)
                {
                    _instance._locks.Remove(scrMemId);
                }
            }
        }

        private AdversaryLocking() { }

        public void AddLock(int scrMemId, AdversaryLock al)
        {
            lock (_syncRoot)
            {
                if (!HasLock(scrMemId))
                {
                    _locks.Add(scrMemId, al);
                }
            }
        }

        private bool HasLock(int scrMemId)
        {
            bool has = true;
            lock (_syncRoot)
            {
                if (!_locks.ContainsKey(scrMemId))
                {
                    has = false;
                }
            }
            return has;
        }

        public bool RemoveLock(int scrMemId)
        {
            bool removed = false;
            lock (_syncRoot)
            {
                removed = _locks.Remove(scrMemId);
            }
            return removed;
        }

        public AdversaryLock GetLock(int scrMemId)
        {
            AdversaryLock lck = null;
            lock (_syncRoot)
            {
                lck = (!HasLock(scrMemId)) ? null : _locks[scrMemId];
            }
            return lck;
        }
    }
}
