﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

namespace Adversary.Web
{
    /// <summary>
    /// TODO: look at federated login with IE and sign in for approvals
    /// </summary>
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RedirectAuthenticatedUsers();
        }

        /// <summary>
        /// redirect away from the login page if the users is already logged in
        /// </summary>
        private void RedirectAuthenticatedUsers()
        {
            if (HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated == true)
            {
                this.Response.Redirect("~/Default.aspx");
            }
        }

        protected void login_LoggingIn(object sender, LoginCancelEventArgs e)
        {
            string userName = login.UserName.ToLower();
            if (userName.Contains("hhdev"))
            {
                login.UserName = userName.Replace("hhdev\\", "");
                login.MembershipProvider = "HHDEVMembershipProvider";
            }
        }
    }
}