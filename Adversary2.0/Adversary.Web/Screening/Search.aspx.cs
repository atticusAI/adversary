﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using Adversary.DataLayer.FinancialDataService;
using Adversary.DataLayer;
using AjaxControlToolkit;
using Adversary.Web.Controls;

namespace Adversary.Web.Screening
{
    public partial class Search : ScreeningBasePage
    {
        Adversary.DataLayer.ScreeningSearch __dataLayer = null;
        Adversary.DataLayer.ScreeningSearch DataLayer
        {
            get
            {
                if (__dataLayer == null)
                {
                    __dataLayer = new DataLayer.ScreeningSearch();
                }
                return __dataLayer;
            }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.pnlSearch.Visible == true)
            {
                smlSearch.ObjectInstance = DataLayer;
                smlSearch.DataBind();
            }

            ((ScreeningMasterPage)this.Master).ShowNavigationButtons = false;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            this.pnlSearch.Visible = true;
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Screening/Search.aspx");
        }
    }
}