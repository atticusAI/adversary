﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.DataLayer;
using Adversary.Utils;
using System.Web.UI.HtmlControls;
using Adversary.Web.Extensions;

namespace Adversary.Web.Screening
{
    public partial class ClientAddress2 : ScreeningBasePage
    {
        Adversary.DataLayer.AdversaryData advData;

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            advData = new AdversaryData();

            List<Adversary.DataLayer.ClientMatterLookupService.ClientAddress> __Addresses =
                        new List<DataLayer.ClientMatterLookupService.ClientAddress>();

            rptClientAddresses.ItemDataBound += new RepeaterItemEventHandler(rptClientAddresses_ItemDataBound);

            Adversary.DataLayer.Address _address = ThisMemo.Addresses.FirstOrDefault();

            lbAddressList.Text = "View addresses for ";

            //if this is a new client, then the link for addresses should not display
            if (ThisMemo.NewClients.Count <= 0 || String.IsNullOrEmpty(ThisMemo.ClientNumber))
            {
                lbAddressList.Visible = false;
                pnlClientAddressForm.Visible = false;
            }
            else
            {
                lbAddressList.Text += advData.SearchClients(ThisMemo.ClientNumber).FirstOrDefault().ClientName;
            }

            if (!IsPostBack)
            {
                if (_address != null)
                {
                    if (!String.IsNullOrEmpty(_address.AddressID.ToString()))
                    {
                        hidAddressID.Value = _address.AddressID.ToString();
                        tbAddress1.Text = _address.Address1;
                        tbAddress2.Text = _address.Address2;
                        tbAddress3.Text = _address.Address3;
                        tbAltPhone.Text = _address.AltPhone;
                        tbCity.Text = _address.City;
                        // tbCountry.Text = _address.Country;
                        tbEmail.Text = _address.email;
                        tbFax.Text = _address.Fax;
                        tbPhone.Text = _address.Phone;
                        // tbState.Text = _address.State;
                        tbZIP.Text = _address.Zip;
                    }
                    else
                    {
                        hidAddressID.Value = "0";
                    }
                }
                else
                {
                    hidAddressID.Value = "0";
                }
            }

            if (!String.IsNullOrEmpty(ThisMemo.ClientNumber))
            {
                __Addresses = advData.GetClientAddresses(ThisMemo.ClientNumber);
                rptClientAddresses.DataSource = __Addresses;
                rptClientAddresses.DataBind();
            }
            else
            {

            }
        }

                
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        void rptClientAddresses_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Adversary.DataLayer.ClientMatterLookupService.ClientAddress __Address =
                    (Adversary.DataLayer.ClientMatterLookupService.ClientAddress)e.Item.DataItem;

                Panel __pnlClientAddressDisplay = (Panel)e.Item.FindControl("pnlClientAddressDisplay");
                Panel __pnlClientAddressPhone = (Panel)e.Item.FindControl("pnlClientAddressPhone");
                Panel __pnlClientAddressSelectButton = (Panel)e.Item.FindControl("pnlClientAddressSelectButton");
                HtmlGenericControl hrClientAddressPhoneSeparator = (HtmlGenericControl)e.Item.FindControl("hrClientAddressPhoneSeparator");

                if (!String.IsNullOrEmpty(__Address.ADDRESS1))
                    __pnlClientAddressDisplay.Controls.Add(new LiteralControl(__Address.ADDRESS1 + "<br/>"));
                if (!String.IsNullOrEmpty(__Address.ADDRESS2))
                    __pnlClientAddressDisplay.Controls.Add(new LiteralControl(__Address.ADDRESS2 + "<br/>"));
                if (!String.IsNullOrEmpty(__Address.ADDRESS3))
                    __pnlClientAddressDisplay.Controls.Add(new LiteralControl(__Address.ADDRESS3 + "<br/>"));
                bool hasCity = false;
                if (!String.IsNullOrEmpty(__Address.CITY))
                {
                    hasCity = true;
                    __pnlClientAddressDisplay.Controls.Add(new LiteralControl(__Address.CITY + " "));
                }
                bool hasState = false;
                if (!String.IsNullOrEmpty(__Address.STATE_CODE))
                {
                    hasState = true;
                    if (hasCity)
                    {
                        __pnlClientAddressDisplay.Controls.Add(new LiteralControl(", "));
                    }
                    __pnlClientAddressDisplay.Controls.Add(new LiteralControl(__Address.STATE_CODE + " "));
                }
                if (!String.IsNullOrEmpty(__Address.POST_CODE))
                {
                    //if (hasCity || hasState)
                    //    __pnlClientAddressDisplay.Controls.Add(new LiteralControl(__Address.POST_CODE));
                    __pnlClientAddressDisplay.Controls.Add(new LiteralControl(__Address.POST_CODE));
                }
                if (!String.IsNullOrEmpty(__Address.COUNTRY_CODE))
                {
                    __pnlClientAddressDisplay.Controls.Add(new LiteralControl("<br/>" + __Address.COUNTRY_CODE));
                }

                if (!String.IsNullOrEmpty(__Address.PHONE_NUMBER))
                    __pnlClientAddressPhone.Controls.Add(new LiteralControl("Phone: " + __Address.PHONE_NUMBER));
                if (!String.IsNullOrEmpty(__Address.FAX_NUMBER))
                    __pnlClientAddressPhone.Controls.Add(new LiteralControl("<br/>Fax: " + __Address.FAX_NUMBER));

                //hrClientAddressPhoneSeparator.Visible = (__pnlClientAddressPhone.Controls.Count > 0);

                //quit the databinding routine if nothing was found in any field and no literal controls were added to
                //the panels
                if (__pnlClientAddressDisplay.Controls.Count <= 0 && __pnlClientAddressPhone.Controls.Count <= 0)
                {
                    __pnlClientAddressDisplay.Controls.Add(new LiteralControl("No addresses were found."));
                }

                LinkButton lb = new LinkButton();
                lb.ID = "lbClientAddressChoose___" + e.Item.ItemIndex.ToString();
                lb.SetDataItem(__Address);
                lb.Text = "Choose this Address";
                lb.Click += new EventHandler(lb_Click);
                lb.CausesValidation = false;
                __pnlClientAddressSelectButton.Controls.Add(lb);
            }
        }

        void lb_Click(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            Adversary.DataLayer.ClientMatterLookupService.ClientAddress _address =
                (Adversary.DataLayer.ClientMatterLookupService.ClientAddress)lb.GetDataItem();

            tbAddress1.Text = _address.ADDRESS1;
            tbAddress2.Text = _address.ADDRESS2;
            tbAddress3.Text = _address.ADDRESS3;
            tbAltPhone.Text = string.Empty;
            tbCity.Text = _address.CITY;
            // tbCountry.Text = _address.COUNTRY_CODE;
            tbEmail.Text = string.Empty;
            tbFax.Text = _address.FAX_NUMBER;
            tbPhone.Text = _address.PHONE_NUMBER;
            // tbState.Text = _address.STATE_CODE;
            tbZIP.Text = _address.POST_CODE;
        }
              
        public override void Validate()
        {
            base.Validate();
            if (this.IsInputValid())
            {
                Adversary.DataLayer.ClientAddress clientAddress = new DataLayer.ClientAddress(ThisMemo.ScrMemID);
                clientAddress.AddressID = Convert.ToInt32(hidAddressID.Value);
                clientAddress.Address1 = tbAddress1.Text;
                clientAddress.Address2 = tbAddress2.Text;
                clientAddress.Address3 = tbAddress3.Text;
                clientAddress.AltPhone = tbAltPhone.Text;
                clientAddress.City = tbCity.Text;
                // clientAddress.Country = tbCountry.Text;
                clientAddress.Email = tbEmail.Text;
                clientAddress.Fax = tbFax.Text;
                clientAddress.Phone = tbPhone.Text;
                // clientAddress.StateProvince = tbState.Text;
                clientAddress.ZIPCode = tbZIP.Text;
                clientAddress.Save();
            }
        }

        public override void Dispose()
        {
            if (advData != null)
            {
                advData.Dispose();
                advData = null;
            }
            base.Dispose();
        }

        protected void valPage_ServerValidate(object sender, ServerValidateEventArgs e)
        {
            e.IsValid = this.IsInputValid();
        }

        private bool IsInputValid()
        {
            bool isValid = true;

            this.ClearErrors();

            if (string.IsNullOrWhiteSpace(this.tbAddress1.Text))
            {
                this.tbAddress1.HH_SetError(this.pnlErrs);
                isValid = false;
            }

            //if (string.IsNullOrWhiteSpace(this.tbCountry.Text))
            //{
            //    this.tbCountry.HH_SetError(this.pnlErrs);
            //    isValid = false;
            //}
            //else
            //{
            //    string[] us = {"u.s.",
            //        "u.s.a.",
            //        "us", 
            //        "usa", 
            //        "united states", 
            //        "united states of america"};

            //    if (us.Contains(this.tbCountry.Text.ToLower()))
            //    {
            //        if (string.IsNullOrWhiteSpace(this.tbCity.Text))
            //        {
            //            this.tbCity.HH_SetError(this.pnlErrs);
            //            isValid = false;
            //        }

            //        if (string.IsNullOrWhiteSpace(this.tbState.Text))
            //        {
            //            this.tbState.HH_SetError(this.pnlErrs);
            //            isValid = false;
            //        }
            //    }
            //}

            if (string.IsNullOrWhiteSpace(this.tbPhone.Text))
            {
                this.tbPhone.HH_SetError(this.pnlErrs);
                isValid = false;
            }

            return isValid;
        }

        private void ClearErrors()
        {
            this.tbAddress1.HH_ClearError();
            this.tbCity.HH_ClearError();
            // this.tbState.HH_ClearError();
            // this.tbCountry.HH_ClearError();
            this.tbPhone.HH_ClearError();
            this.pnlErrs.HH_ClearError();
        }

        protected void ddlCountry_Init(object sender, EventArgs e)
        {
            // load up the countries
        }

        protected void ddlState_Init(object sender, EventArgs e)
        {
            // load up the states
        }


    }
}