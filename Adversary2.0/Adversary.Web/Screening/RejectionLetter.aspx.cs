﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.Utils;
using Adversary.DataLayer;


namespace Adversary.Web.Screening
{
    public partial class RejectionLetter : ScreeningBasePage
    {
        private string _ErrorMessage = string.Empty;
        private bool _ShowError = false;
        private Adversary.DataLayer.FileUploadClass fupRejectionLetter;
        private string _RejectionLetterUploaded = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            fileUpload.FileUploadErrorHandler += new Web.Controls.FileUpload.OnFileUploadError(fileUpload_FileUploadErrorHandler);
            fileUpload.FileUploadHandler += new Web.Controls.FileUpload.OnFileUpload(fileUpload_FileUploadHandler);
            fileUpload.FileUploadSuccessHandler += new Web.Controls.FileUpload.OnFileUploadSuccess(fileUpload_FileUploadSuccessHandler);
            fileUpload.FileUploadDirectory = base.FileUploadDirectory;

            gvRejectionLetterFiles.RowCommand += new GridViewCommandEventHandler(gvRejectionLetterFiles_RowCommand);
            fupRejectionLetter = new DataLayer.FileUploadClass(ThisMemo.ScrMemID);
            fupRejectionLetter.FileUploadType = DataLayer.FileUploadClass.FileUploadMode.RejectionLetter;
        }

        void gvRejectionLetterFiles_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int _attachmentID = Convert.ToInt32(e.CommandArgument);
            Adversary.DataLayer.FileUploadClass fileUploadClass = new DataLayer.FileUploadClass(ThisMemo.ScrMemID);
            fileUploadClass.Delete(_attachmentID);

            //delete the file
            FileUtils.DeleteFile(base.FileUploadDirectory + "\\" + ThisMemo.ScrMemID.ToString() + "\\" + fileUploadClass.FileName);
            BindFileData();
        }

        void fileUpload_FileUploadSuccessHandler(Adversary.Controls.UploadEventArgs e)
        {
            fupRejectionLetter.FileUploaded = true;
            fupRejectionLetter.SaveMemoInformationOnly();
            if (fupRejectionLetter.HasUploadedFiles) lblRejectionLetterMessage.Text = string.Empty;
            BindFileData();
        }

        void fileUpload_FileUploadHandler(Adversary.Controls.UploadEventArgs e)
        {

        }

        void fileUpload_FileUploadErrorHandler(Adversary.Controls.UploadEventArgs e)
        {

        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            fileUpload.FileUploadMode = Adversary.DataLayer.FileUploadClass.FileUploadMode.RejectionLetter;
            fileUpload.ScreeningMemoID = ThisMemo.ScrMemID;
            rblRejectionLetter.HH_AddAccordionPaneJavascript(this.accRejectionLetter.ClientID);
            accRejectionLetter.SelectedIndex = -1;

            if (!Page.IsPostBack) BindFileData();
        }

        private void BindFileData()
        {
            Adversary.DataLayer.FileUploadClass fuClass = new DataLayer.FileUploadClass(ThisMemo.ScrMemID);
            fuClass.FileDescription = Adversary.DataLayer.FileUploadClass.FileUploadMode.RejectionLetter.ToDescription();
            List<Adversary.DataLayer.Attachment> _attachments = fuClass.GetAttachments(FileUploadClass.FileUploadMode.None);
            gvRejectionLetterFiles.DataSource = _attachments;
            gvRejectionLetterFiles.DataBind();
        }

        void advFileUpload_UploadError(object sender, Adversary.Controls.UploadEventArgs e)
        {
            _ShowError = true;
            _ErrorMessage = e.FileUploadError;
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (_ShowError)
            {
                lblMessage.Text = _ErrorMessage;
                lblMessage.Visible = true;
            }

            if (fupRejectionLetter != null)
            {
                fupRejectionLetter.Get();
                if (fupRejectionLetter.FileUploaded.HasValue)
                {
                    rblRejectionLetter.Items.FindByValue(Convert.ToBoolean(fupRejectionLetter.FileUploaded) ? "yes" : "no").Selected = true;
                    accRejectionLetter.SelectedIndex = (Convert.ToBoolean(fupRejectionLetter.FileUploaded) ? 0 : 1);
                }
                tbNoRejectionLetter.Text = fupRejectionLetter.NoFileUploadReason;
            }
            else
            {
                accRejectionLetter.SelectedIndex = 2;
            }
            if (!fupRejectionLetter.HasUploadedFiles) lblRejectionLetterMessage.Text = "No Rejection Letter(s) uploaded.";
        }

        public override void Validate()
        {
            base.Validate();
            if (rblRejectionLetter.SelectedValue.Contains("no"))
            {
                fupRejectionLetter.FileUploaded = false;
                fupRejectionLetter.NoFileUploadReason = tbNoRejectionLetter.Text;
            }
            else if (rblRejectionLetter.SelectedValue.ToLower().Contains("yes"))
            {
                fupRejectionLetter.FileUploaded = true;

            }
            fupRejectionLetter.SaveMemoInformationOnly();

        }
    }
}