﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Screening/Screening.Master" AutoEventWireup="true"
    CodeBehind="BasicInformation.aspx.cs" Inherits="Adversary.Web.Screening.BasicInformation"
    Theme="HH" %>

<%@ Register TagPrefix="ajax" Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" %>
<%@ Register TagPrefix="uc" TagName="EmployeeControl" Src="~/Controls/PersonAutoComplete.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ProBonoTypes" src="~/Controls/ProBonoTypes.ascx" %>
<%@ MasterType VirtualPath="~/Screening/Screening.Master" %>
<%@ Register TagPrefix="advcontrols" Assembly="Adversary.Controls" Namespace="Adversary.Controls" %>

<asp:Content ID="cntHead" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            formValidation = hh.validateWithjQuery("#form1");
        });
    </script>
</asp:Content>
<asp:Content ID="cntHeader" ContentPlaceHolderID="cphHeader" runat="server">
</asp:Content>
<asp:Content ID="cntMain" ContentPlaceHolderID="cphMainContent" runat="server">
    <table border="0" cellpadding="4" cellspacing="2">
        <tr>
            <td class="lbl">
                Screening Memo Number:
            </td>
            <td>
                <asp:Label ID="lblScreeningMemoMember" runat="server" />
            </td>
        </tr>
        <tr runat="server" id="trProBonoType">
            <td class="lbl">
                Pro Bono Type:
            </td>
            <td>
                <%--<advcontrols:ProBonoTypes ID="advProBonoTypes" runat="server" />--%>
                <uc1:ProBonoTypes ID="advProBonoTypes" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="lbl required">
                Date:
            </td>
            <td>
                <asp:TextBox ID="tbMemoDate" runat="server" CssClass="input required date" title="* You must enter a valid memo date." />
                <ajax:CalendarExtender runat="server" TargetControlID="tbMemoDate" Animated="false"
                    FirstDayOfWeek="Sunday" Format="MM/dd/yyyy" PopupPosition="BottomLeft">
                </ajax:CalendarExtender>
            </td>
        </tr>
        <tr>
            <td class="lbl">
                Confidential Memo?
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblConfidential" RepeatDirection="Horizontal"
                    CellPadding="3" CellSpacing="2" Width="110px">
                    <asp:ListItem Text="Yes" />
                    <asp:ListItem Text="No" />
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td class="lbl">
                Attorney Preparing Memo:
            </td>
            <td>
                <uc:EmployeeControl ID="ucAttorneyPreparingMemo" runat="server" ServiceMethod="TimekeeperService" />
            </td>
        </tr>
        <tr>
            <td class="lbl">
                Input By:
            </td>
            <td>
                <uc:EmployeeControl ID="ucInputBy" runat="server" ServiceMethod="TimekeeperService" />
            </td>
        </tr>
    </table>
    <asp:Panel ID="pnlErrs" runat="server" CssClass="errdiv">
    </asp:Panel>
    <asp:CustomValidator ID="valPage" runat="server" OnServerValidate="valPage_ServerValidate" />
</asp:Content>
