﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.DataLayer;
using System.Data.Linq;
using System.Text;

namespace Adversary.Web.Screening
{
    public partial class Summary : ScreeningBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ((ScreeningMasterPage)this.Master).ShowNavigationButtons = false;
            this.smsView.ThisMemo = ThisMemo;
            this.aptSummary.Visible = this.TrackingVisible();
            this.aptSummary.ThisMemo = ThisMemo;
        }

        protected bool TrackingVisible()
        {
            bool show = false;
            if (ThisMemo.Trackings != null && ThisMemo.Trackings.Count > 0)
            {
                show = ThisMemo.Trackings[0].StatusTypeID != 0;
            }
            return show;
        }
    }
}