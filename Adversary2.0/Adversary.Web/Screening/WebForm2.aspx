﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Screening/Screening.Master" AutoEventWireup="true" CodeBehind="WebForm2.aspx.cs" Inherits="Adversary.Web.Screening.WebForm2" %>
<%@ Register TagPrefix="advControls" Assembly="Adversary.Controls" Namespace="Adversary.Controls" %>
<%@ Register Src="~/Controls/ErrorSummary.ascx" TagPrefix="advWebControls" TagName="ErrorSummary" %>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMainContent" runat="server">
    <asp:UpdatePanel runat="server" ID="up1" UpdateMode="Always">
        <ContentTemplate>
        <asp:Label ID="lbl1" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <advWebControls:ErrorSummary ID="advErrorSummary" runat="server" />
</asp:Content>
