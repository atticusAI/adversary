﻿<%@ Page Title="" Language="C#" Theme="HH" MasterPageFile="~/Screening/Screening.Master"
    AutoEventWireup="true" CodeBehind="ClientAddress.aspx.cs" Inherits="Adversary.Web.Screening.ClientAddress" %>

<%@ Register TagPrefix="ajax" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="cntHead" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" language="javascript">
        function setUSALabels() {
            if (isUSA()) {
                $("td.usa_required").addClass("required");
            }
            else {
                $("td.usa_required").removeClass("required");
            }
        }

        function isUSA() {
            var us = ['u.s.', 'u.s.a.', 'us', 'usa',
                'united states', 'united states of america'];
            var country = $get("<%=tbCountry.ClientID%>").value.toLowerCase();
            country = $.trim(country);
            return (us.indexOf(country) >= 0);
        }

        $(document).ready(function () {
            formValidation = hh.validateWithjQuery("#form1");

            $("#<%=tbCountry.ClientID%>").change(function () {
                setUSALabels();
            });

            jQuery.validator.addMethod("usa_required", function (value, element) {
                return isUSA() ? value != "" : true;
            }, "This field is required.");

            setUSALabels();
        });
    </script>
</asp:Content>
<asp:Content ID="cntMain" ContentPlaceHolderID="cphMainContent" runat="server">
    <asp:HiddenField ID="hidAddressID" runat="server" />
    <table border="0" cellpadding="4" cellspacing="2">
        <tr>
            <td rowspan="3" valign="top" class="lbl required">
                Address:
            </td>
            <td>
                <asp:TextBox ID="tbAddress1" runat="server" SkinID="HH_Textbox" CssClass="required"
                    title="* You must enter at least one address line" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="tbAddress2" runat="server" SkinID="HH_Textbox" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="tbAddress3" runat="server" SkinID="HH_Textbox" />
            </td>
        </tr>
        <tr>
            <td class="lbl required">
                City:
            </td>
            <td>
                <asp:TextBox ID="tbCity" runat="server" CssClass="required" title="* You must enter a city." />
            </td>
        </tr>
        <tr>
            <td class="lbl required">
                State:
            </td>
            <td>
                <asp:TextBox ID="tbState" runat="server" MaxLength="2" CssClass="required"
                    title="* You must enter a State/Province" />
            </td>
        </tr>
        <tr>
            <td class="lbl">
                ZIP:
            </td>
            <td>
                <asp:TextBox ID="tbZIP" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="lbl required">
                Country:
            </td>
            <td>
                <asp:TextBox ID="tbCountry" runat="server" CssClass="required" title="* You must enter a Country" />
            </td>
        </tr>
        <tr>
            <td class="lbl required">
                Phone:
            </td>
            <td>
                <asp:TextBox ID="tbPhone" runat="server" CssClass="required" title="* You must enter a telephone number" />
            </td>
        </tr>
        <tr>
            <td class="lbl">
                Alt Phone:
            </td>
            <td>
                <asp:TextBox ID="tbAltPhone" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="lbl">
                Email:
            </td>
            <td>
                <asp:TextBox ID="tbEmail" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="lbl">
                Fax:
            </td>
            <td>
                <asp:TextBox ID="tbFax" runat="server" />
            </td>
        </tr>
    </table>
    <asp:Panel ID="pnlErrs" runat="server" CssClass="errdiv">
    </asp:Panel>
    <asp:CustomValidator ID="valPage" runat="server" OnServerValidate="valPage_ServerValidate" />
</asp:Content>
<asp:Content ContentPlaceHolderID="cphLowerContent" ID="LowerContentPlaceHolder"
    runat="server">
    <ajax:ModalPopupExtender ID="mdlClientAddressList" runat="server" TargetControlID="lbAddressList"
        PopupControlID="pnlClientAddressForm" BackgroundCssClass="pop-back" CancelControlID="btnCancel" />
    <asp:Panel ID="pnlClientAddressForm" Width="35%" runat="server" CssClass="modalPopup">
        <div style="height:500px; overflow:auto;">
            <asp:Repeater runat="server" ID="rptClientAddresses">
                <ItemTemplate>
                    <asp:Panel ID="pnlClientAddressDisplay" runat="server" />
                    <asp:Panel ID="pnlClientAddressPhone" runat="server" />
                    <asp:Panel ID="pnlClientAddressSelectButton" runat="server" />
                    <hr />
                </ItemTemplate>
            </asp:Repeater>
        </div><br />
        <div style="text-align:right; padding:5px;">
            <asp:LinkButton ID="btnCancel" runat="server" Text="Cancel" />
        </div>
    </asp:Panel>
    <asp:LinkButton ID="lbAddressList" runat="server" Font-Bold="true" CausesValidation="false"></asp:LinkButton>
</asp:Content>
