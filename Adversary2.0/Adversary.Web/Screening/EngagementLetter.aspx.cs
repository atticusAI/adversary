﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.DataLayer;
using Adversary.Utils;

namespace Adversary.Web.Screening
{
    public partial class EngagementLetter : ScreeningBasePage
    {
        private Adversary.DataLayer.FileUploadClass fupEngagementLetter;
        private Adversary.DataLayer.FileUploadClass fupWrittenPolicies;
        private Adversary.DataLayer.EngagementLetterScreen engScreen;

        protected void Page_Load(object sender, EventArgs e)
        {
            engScreen = new EngagementLetterScreen(ThisMemo.ScrMemID);

            rblEngagementLetter.HH_AddAccordionPaneJavascript(accEngagementLetter.ClientID);
            rblWrittenPolicies.HH_AddAccordionPaneJavascript(accWrittenPolicies.ClientID);
            fupEngagementLetter = new FileUploadClass(ThisMemo.ScrMemID);
            fupEngagementLetter.FileUploadType = DataLayer.FileUploadClass.FileUploadMode.EngagementLetter;
            
            fupWrittenPolicies = new FileUploadClass(ThisMemo.ScrMemID);
            fupWrittenPolicies.FileUploadType = DataLayer.FileUploadClass.FileUploadMode.WrittenPolicies;

            fileUpload.FileUploadErrorHandler += new Web.Controls.FileUpload.OnFileUploadError(fileUpload_FileUploadErrorHandler);
            fileUpload.FileUploadHandler += new Web.Controls.FileUpload.OnFileUpload(fileUpload_FileUploadHandler);
            fileUpload.FileUploadSuccessHandler += new Web.Controls.FileUpload.OnFileUploadSuccess(fileUpload_FileUploadSuccessHandler);
            fileUpload.FileUploadDirectory = base.FileUploadDirectory;
            fileUpload.ScreeningMemoID = ThisMemo.ScrMemID;

            fileUploadWrittenPolicies.FileUploadErrorHandler += new Web.Controls.FileUpload.OnFileUploadError(fileUploadWrittenPolicies_FileUploadErrorHandler);
            fileUploadWrittenPolicies.FileUploadHandler += new Web.Controls.FileUpload.OnFileUpload(fileUploadWrittenPolicies_FileUploadHandler);
            fileUploadWrittenPolicies.FileUploadSuccessHandler += new Web.Controls.FileUpload.OnFileUploadSuccess(fileUploadWrittenPolicies_FileUploadSuccessHandler);
            fileUploadWrittenPolicies.FileUploadDirectory = base.FileUploadDirectory;
            fileUploadWrittenPolicies.ScreeningMemoID = ThisMemo.ScrMemID;

            advFilesEngagementLetter.ScreeningMemoID = base.ThisMemo.ScrMemID;
            advFilesEngagementLetter.FileUploadDirectory = base.FileUploadDirectory;
            advFilesEngagementLetter.FileUploadMode = DataLayer.FileUploadClass.FileUploadMode.EngagementLetter;

            advFilesWrittenPolicies.ScreeningMemoID = base.ThisMemo.ScrMemID;
            advFilesWrittenPolicies.FileUploadDirectory = base.FileUploadDirectory;
            advFilesWrittenPolicies.FileUploadMode = DataLayer.FileUploadClass.FileUploadMode.WrittenPolicies;

        }

        void fileUploadWrittenPolicies_FileUploadSuccessHandler(Adversary.Controls.UploadEventArgs e)
        {
            fupWrittenPolicies.FileUploaded = true;
            fupWrittenPolicies.SaveMemoInformationOnly();
            BindGrids();
        }

        void fileUploadWrittenPolicies_FileUploadHandler(Adversary.Controls.UploadEventArgs e)
        {
            
        }

        void fileUploadWrittenPolicies_FileUploadErrorHandler(Adversary.Controls.UploadEventArgs e)
        {
           
        }

        void fileUpload_FileUploadSuccessHandler(Adversary.Controls.UploadEventArgs e)
        {
            fupEngagementLetter.FileUploaded = true;
            fupEngagementLetter.SaveMemoInformationOnly();
            BindGrids();
        }

        void fileUpload_FileUploadHandler(Adversary.Controls.UploadEventArgs e)
        {
            
        }

        void fileUpload_FileUploadErrorHandler(Adversary.Controls.UploadEventArgs e)
        {
            
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            fupEngagementLetter.Get();
            fupWrittenPolicies.Get();
            engScreen.Get();

            if (engScreen.EngagementLetterUploaded.HasValue)
            {
                rblEngagementLetter.Items.FindByValue(Convert.ToBoolean(engScreen.EngagementLetterUploaded) ? "yes" : "no").Selected = true;
                accEngagementLetter.SelectedIndex = Convert.ToBoolean(engScreen.EngagementLetterUploaded) ? 0 : 1;
            }

            //change to select *only* the Upload option if the memo
            //is new client/new matter
            if (ThisMemo.ScrMemType == 1)
            {
                rblEngagementLetter.SelectedIndex = -1;
                rblEngagementLetter.Items.FindByValue("yes").Selected = true;
                rblEngagementLetter.Items[1].Enabled = false;
                accEngagementLetter.SelectedIndex = 0;
            }


            if (engScreen.WrittenPolicies.HasValue)
            {
                rblWrittenPolicies.Items.FindByValue(Convert.ToBoolean(engScreen.WrittenPolicies) ? "yes" : "no").Selected = true;
                accWrittenPolicies.SelectedIndex = Convert.ToBoolean(engScreen.WrittenPolicies) ? 0 : 1;
            }

            tbNoEngagementLetter.Text = engScreen.EngagementLetterReason;
            
            if (rblEngagementLetter.SelectedIndex == -1) accEngagementLetter.SelectedIndex = 2;
            if (rblWrittenPolicies.SelectedIndex == -1) accWrittenPolicies.SelectedIndex = 2;
        }
        public override void Validate()
        {
            base.Validate();

            if (rblEngagementLetter.SelectedValue.ToLower().Contains("no"))
            {

                fupEngagementLetter.NoFileUploadReason = tbNoEngagementLetter.Text;
                fupEngagementLetter.FileUploaded = false;
            }
            else if(rblEngagementLetter.SelectedValue.ToLower().Contains("yes"))
            {
                fupEngagementLetter.FileUploaded = true;
            }

            if (rblWrittenPolicies.SelectedValue.ToLower().Contains("no"))
            {
                fupWrittenPolicies.FileUploaded = false;
            }
            else if (rblWrittenPolicies.SelectedValue.ToLower().Contains("yes"))
            {
                fupWrittenPolicies.FileUploaded = true;
            }
            fupEngagementLetter.SaveMemoInformationOnly();
            fupWrittenPolicies.SaveMemoInformationOnly();

        }
        private void BindGrids()
        {
            FileUploadClass fupEngageLetter = new FileUploadClass(ThisMemo.ScrMemID);
            FileUploadClass fupWrittenPolicy = new FileUploadClass(ThisMemo.ScrMemID);

            fupEngageLetter.FileUploadType = FileUploadClass.FileUploadMode.EngagementLetter;
            fupWrittenPolicy.FileUploadType = FileUploadClass.FileUploadMode.WrittenPolicies;

            //gvEngagementLetterFiles.DataSource = fupEngageLetter.GetAttachments();
            //gvWrittenPolicies.DataSource = fupWrittenPolicy.GetAttachments();

            //gvEngagementLetterFiles.DataBind();
            //gvWrittenPolicies.DataBind();

        }
    }
}