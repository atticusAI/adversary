﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Screening/Screening.Master" AutoEventWireup="true"
    CodeBehind="ProBonoReason.aspx.cs" Inherits="Adversary.Web.Screening.ProBonoReason"
    Theme="HH" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMainContent" runat="server">
    <table width="900" cellpadding="2" cellspacing="1" border="0">
        <tr>
            <td valign="top">
                Please state why you feel that this memo is Pro Bono?
            </td>
            <td>
                <asp:TextBox ID="tbProBonoReason" runat="server" TextMode="MultiLine" Columns="50"
                    Rows="16" />
            </td>
        </tr>
    </table>
</asp:Content>
