﻿<%@ Page Title="" Theme="HH" Language="C#" MasterPageFile="~/Screening/Screening.Master" AutoEventWireup="true" CodeBehind="Submit.aspx.cs" Inherits="Adversary.Web.Screening.Submit" %>
<%@ Register TagPrefix="ajax" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="adv" TagName="errorsummary" Src="~/Controls/ErrorSummary.ascx" %>
<%@ Register TagPrefix="adv" TagName="fileupload" Src="~/Controls/FileUpload.ascx" %>
<%@ Register TagPrefix="adv" TagName="existingfiles" Src="~/Controls/ExistingFiles.ascx" %>
<%@ Register TagPrefix="adv" TagName="PrintViewLink" src="~/Controls/PrintViewLink.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../App_Themes/HH/HH.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphMainContent" runat="server">
   
    <table border="0" cellpadding="3" cellspacing="4" width="700">
        <tr>
            <td>
                
            </td>
        </tr>
        <tr>
            <td class="grvselrow">
                <asp:Panel ID="pnlAdditionalFilesHeader" runat="server">Attach Additional Files to this Memo <asp:Image ID="imgCollapseExpand" runat="server" ImageUrl="~/Images/expand.jpg" /></asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="pnlAdditionalFilesContent" runat="server">
                    <adv:existingfiles ID="advExistingFiles" runat="server" />
                    
                    <adv:fileUpload ID="fileUploadAdditionalFiles" runat="server" FileUploadMode="None" />
                </asp:Panel>
                <ajax:CollapsiblePanelExtender TargetControlID="pnlAdditionalFilesContent" ExpandControlID="pnlAdditionalFilesHeader" CollapseControlID="pnlAdditionalFilesHeader"
                    id="cpxAdditionalFiles" runat="server" Collapsed="true" SuppressPostBack="true" CollapsedImage="~/Images/collapse.jpg" ExpandedImage="~/Images/expand.jpg" 
                    ImageControlID="imgCollapseExpand" />
            </td>
        </tr>
        <tr>
            <td>
                <adv:PrintViewLink ID="lnkPrintView" runat="server" OnPreRender="lnkPrintView_PreRender" />
            </td>
        </tr>
        <tr runat="server" id="trSubmitRow">
            <td>
                <%--<asp:LinkButton ID="lbSubmitMemo" runat="server" Text="Submit Screening Memo For Approval" />--%>
                <asp:Button ID="btnSubmitMemo" runat="server" Text="Submit Screening Memo for Approval" SkinID="HH_Button" />
            </td>
        </tr>
    </table>
                
</asp:Content>
