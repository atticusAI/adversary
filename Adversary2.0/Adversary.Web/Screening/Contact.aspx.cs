﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.DataLayer;
using AjaxControlToolkit;

namespace Adversary.Web.Screening
{
    public partial class Contact : ScreeningBasePage
    {
        ContactScreen _ContactScreen;

        protected void Page_Load(object sender, EventArgs e)
        {
            _ContactScreen = new ContactScreen(ThisMemo.ScrMemID);
        }

        protected void gvContacts_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string contactID = e.CommandArgument.ToString();
            _ContactScreen.ContactID = Convert.ToInt32(contactID);

            if (e.CommandName == "Delete")
            {
                _ContactScreen.Delete();
            }
            else
            {
                _ContactScreen.Get();

                //populate
                tbFirstName.Text = _ContactScreen.ContactFName;
                tbMiddleName.Text = _ContactScreen.ContactMName;
                tbLastName.Text = _ContactScreen.ContactLName;
                tbTitle.Text = _ContactScreen.ContactTitle;
                hidContactID.Value = _ContactScreen.ContactID.ToString();

                this.mdlModifyContact.Show();
            }
        }

        protected void gvContacts_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            // no-op
        }

        protected void btnSubmitContact_Click(object sender, EventArgs e)
        {
            Adversary.DataLayer.ContactScreen _Contact = new ContactScreen(ThisMemo.ScrMemID);
           
            _Contact.ContactFName = tbFirstName.Text;
            _Contact.ContactMName = tbMiddleName.Text;
            _Contact.ContactLName = tbLastName.Text;
            _Contact.ContactTitle = tbTitle.Text;
            _Contact.ContactID = (String.IsNullOrEmpty(hidContactID.Value) ? 0 : Convert.ToInt32(hidContactID.Value));
            _Contact.Save();
            this.mdlContactEntry.Hide();
            
            this.tbFirstName.Text = "";
            this.tbMiddleName.Text = "";
            this.tbLastName.Text = "";
            this.tbTitle.Text = "";
            this.hidContactID.Value = "";

            upnlContacts.Update();
        }

        private void BindData()
        {
            ReloadMemo();
            gvContacts.DataSource = _ContactScreen.GetContactList();
            gvContacts.DataBind();
        }

        public override void Dispose()
        {
            if (_ContactScreen != null)
            {
                _ContactScreen.Dispose();
                _ContactScreen = null;
            }
            base.Dispose();
        }

        protected void gvContacts_PreRender(object sender, EventArgs e)
        {
            this.BindData();
        }
    }
}