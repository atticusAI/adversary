﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Adversary.Utils;
using Adversary.DataLayer;

namespace Adversary.Web.Screening
{
    public partial class ConflictReport : ScreeningBasePage
    {
        private string _ErrorMessage = string.Empty;
        private bool _ShowError = false;
        private Adversary.DataLayer.FileUploadClass fupConflictReport;
        private string _ConflictReportUploaded = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            fileUpload.FileUploadErrorHandler += new Web.Controls.FileUpload.OnFileUploadError(fileUpload_FileUploadErrorHandler);
            fileUpload.FileUploadHandler += new Web.Controls.FileUpload.OnFileUpload(fileUpload_FileUploadHandler);
            fileUpload.FileUploadSuccessHandler += new Web.Controls.FileUpload.OnFileUploadSuccess(fileUpload_FileUploadSuccessHandler);

            //gvConflictReportFiles.RowCommand += new GridViewCommandEventHandler(gvConflictReportFiles_RowCommand);
            fupConflictReport = new DataLayer.FileUploadClass(ThisMemo.ScrMemID);
            fupConflictReport.FileUploadType = DataLayer.FileUploadClass.FileUploadMode.ConflictReport;
            fileUpload.FileUploadDirectory = base.FileUploadDirectory;

            advFiles.ScreeningMemoID = base.ThisMemo.ScrMemID;
            advFiles.FileUploadDirectory = base.FileUploadDirectory;
            advFiles.FileUploadMode = DataLayer.FileUploadClass.FileUploadMode.ConflictReport;
        }

        void fileUpload_FileUploadSuccessHandler(Adversary.Controls.UploadEventArgs e)
        {
            fupConflictReport.FileUploaded = true;
            fupConflictReport.SaveMemoInformationOnly();
            if (fupConflictReport.HasUploadedFiles) lblConflictReportMessage.Text = string.Empty;
            //BindFileData();
        }

        void fileUpload_FileUploadHandler(Adversary.Controls.UploadEventArgs e)
        {
            
        }

        void fileUpload_FileUploadErrorHandler(Adversary.Controls.UploadEventArgs e)
        {
            
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            fileUpload.FileUploadMode = Adversary.DataLayer.FileUploadClass.FileUploadMode.ConflictReport;
            fileUpload.ScreeningMemoID = ThisMemo.ScrMemID;

            //adding functionality for new memo/new client: these memos (track #1) will
            //only allow users to select "upload" and not allow the user to defer uploading
            
            rblConflictReport.HH_AddAccordionPaneJavascript(this.accConflictReport.ClientID);
            accConflictReport.SelectedIndex = -1;
            
           // if (!Page.IsPostBack) BindFileData();
        }

        void advFileUpload_UploadError(object sender, Adversary.Controls.UploadEventArgs e)
        {
            _ShowError = true;
            _ErrorMessage = e.FileUploadError;
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (_ShowError)
            {
                lblMessage.Text = _ErrorMessage;
                lblMessage.Visible = true;
            }
            
            if (fupConflictReport != null)
            {
                fupConflictReport.Get();
                if (fupConflictReport.FileUploaded.HasValue)
                {
                    rblConflictReport.Items.FindByValue(Convert.ToBoolean(fupConflictReport.FileUploaded) ? "yes" : "no").Selected = true;
                    accConflictReport.SelectedIndex = (Convert.ToBoolean(fupConflictReport.FileUploaded) ? 0 : 1);
                }
                tbNoConflictReport.Text = fupConflictReport.NoFileUploadReason;

                if (ThisMemo.ScrMemType == 1)
                {
                    rblConflictReport.SelectedIndex = -1;
                    rblConflictReport.Items.FindByValue("yes").Selected = true;
                    rblConflictReport.Items[1].Enabled = false;
                    accConflictReport.SelectedIndex = 0;
                }
                
            }
            else
            {
                accConflictReport.SelectedIndex = 2;
            }
            if (!fupConflictReport.HasUploadedFiles) lblConflictReportMessage.Text = "No Conflict Report(s) uploaded.";
        }

        public override void Validate()
        {
            base.Validate();
            if (rblConflictReport.SelectedValue.Contains("no"))
            {
                fupConflictReport.FileUploaded = false;
                fupConflictReport.NoFileUploadReason = tbNoConflictReport.Text;
            }
            else if (rblConflictReport.SelectedValue.ToLower().Contains("yes"))
            {
                fupConflictReport.FileUploaded = true;
                
            }
            fupConflictReport.SaveMemoInformationOnly();

        }
    }
}