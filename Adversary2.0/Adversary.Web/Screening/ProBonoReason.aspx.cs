﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.Utils;
using Adversary.DataLayer;

namespace Adversary.Web.Screening
{
    public partial class ProBonoReason : ScreeningBasePage
    {
        ProBonoReasonScreen proBonoScreen;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            proBonoScreen = new ProBonoReasonScreen(ThisMemo.ScrMemID);
            proBonoScreen.Get();
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            tbProBonoReason.Text = proBonoScreen.ProBonoReason;
        }

        public override void Validate()
        {
            base.Validate();
            proBonoScreen.ProBonoReason = tbProBonoReason.Text;
            proBonoScreen.Save();
        }

    }
}