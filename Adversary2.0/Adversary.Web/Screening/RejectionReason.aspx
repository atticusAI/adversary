﻿<%@ Page Theme="HH" Title="" Language="C#" MasterPageFile="~/Screening/Screening.Master" AutoEventWireup="true" CodeBehind="RejectionReason.aspx.cs" Inherits="Adversary.Web.Screening.RejectionReason" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphMainContent" runat="server">
    <table border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td valign="top">
                Reason for the Rejection
            </td>
            <td>
                <asp:TextBox runat="server" ID="tbRejectionReason" TextMode="MultiLine" Rows="10" Columns="60" />
            </td>
        </tr>
    </table>
</asp:Content>
