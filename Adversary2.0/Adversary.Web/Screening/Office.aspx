﻿<%@ Page Title="" Theme="HH" Language="C#" MasterPageFile="~/Screening/Screening.Master"
    AutoEventWireup="true" CodeBehind="Office.aspx.cs" Inherits="Adversary.Web.Screening.Office" %>

<%@ Register TagPrefix="advControls" Assembly="Adversary.Controls" Namespace="Adversary.Controls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../App_Themes/HH/HH.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMainContent" runat="server">
    <table border="0" cellpadding="3" cellspacing="2">
        <tr>
            <td class="lbl">
                Originating Office:
            </td>
            <td>
                <advControls:OfficeDropDown ID="advOfficeDropDown" runat="server" CssClass="dropDownList" />
            </td>
        </tr>
    </table>
</asp:Content>
