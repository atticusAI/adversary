﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Screening/Screening.Master" AutoEventWireup="true"
    CodeBehind="CM.aspx.cs" Inherits="Adversary.Web.Screening.CM" Theme="HH" %>

<%@ Register Assembly="Adversary.Controls" Namespace="Adversary.Controls" TagPrefix="advControls" %>
<%@ Register Src="~/Controls/ClientAutoComplete.ascx" TagPrefix="advWebControls"
    TagName="ClientDropDown" %>
<%@ Register Src="~/Controls/PersonAutoComplete.ascx" TagPrefix="advWebControls"
    TagName="PersonDropDown" %>
<%@ Register Src="~/Controls/ListContainerControl.ascx" TagPrefix="advWebControls"
    TagName="ListContainerControl" %>
<%@ Register Src="~/Controls/ARTable.ascx" TagPrefix="advWebControls" TagName="ARTable" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            formValidation = hh.validateWithjQuery("#form1");
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMainContent" runat="server">
    <asp:UpdatePanel ID="upnlARInfo" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
        <ContentTemplate>
            <asp:Panel ID="pnlARInfo" runat="server">
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <table border="0" cellpadding="3" cellspacing="2">
        <tr>
            <td class="lbl required">
                Matter Name:
            </td>
            <td>
                <asp:TextBox ID="tbMatterName" runat="server" CssClass="required" Columns="40" Rows="3"
                    TextMode="MultiLine" title="* You must enter a matter name" />
            </td>
        </tr>
        <asp:Panel runat="server" ID="pnlClientDropDown">
            <tr>
                <td class="lbl">
                    Client:
                </td>
                <td>
                    <advWebControls:ClientDropDown ID="advClientDropDown" runat="server" ServiceMethod="ClientService" />
                </td>
            </tr>
        </asp:Panel>
        <tr>
            <td class="lbl required">
                Work Description:
            </td>
            <td>
                <asp:TextBox runat="server" ID="tbWorkDescription" CssClass="required" Columns="40"
                    Rows="3" TextMode="MultiLine" title="* You must enter a description of work" />
            </td>
        </tr>
        <tr>
            <td class="lbl required">
                Practice Type Code:
            </td>
            <td>
                <advControls:PracticeCodeDropdown ID="pcDropDown" Title="* You must select a practice code."
                    CssClass="required" runat="server" OnClientChange="return pracCodeChanged(this);" />
            </td>
        </tr>
        <tr id="trEstimateOfFees" runat="server">
            <td class="lbl">
                Estimate of Fees
            </td>
            <td>
                <advControls:EstimateOfFeesDropDown ID="estFeesDropdown" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="lbl">
                <asp:Label ID="lblContingencyFee" runat="server" AssociatedControlID="rblContingencyFee"
                    Text="Contingency Fee?" />
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblContingencyFee">
                    <asp:ListItem Text="Yes" />
                    <asp:ListItem Text="No" />
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td class="lbl">
                <asp:Label ID="lblCreateNDriveFolder" runat="server" AssociatedControlID="rblCreateNDriveFolder"
                    Text="Create an N Drive folder for this client?" />
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblCreateNDriveFolder" AutoPostBack="false"
                    CausesValidation="false">
                    <asp:ListItem Text="Yes" Value="true" />
                    <asp:ListItem Text="No" Value="false" />
                </asp:RadioButtonList>
            </td>
        </tr>
        <asp:UpdatePanel ID="upnlNDriveDropDown" runat="server" ChildrenAsTriggers="false"
            UpdateMode="Conditional">
            <ContentTemplate>
                <tr>
                    <td class="lbl">
                        N Drive Folder Type
                    </td>
                    <td>
                        <advControls:NDriveFolderTypeDropDown ID="ndriveDropDown" CausesPostBackOnSelected="false"
                            runat="server" />
                        <asp:HiddenField ID="hidNDriveSelectedValue" runat="server" />
                        <asp:UpdateProgress ID="uprgNDriveDropDown" runat="server" AssociatedUpdatePanelID="upnlNDriveDropDown"
                            DisplayAfter="0">
                            <ProgressTemplate>
                                <img src="../Images/ajax-loader.gif" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div id="divNDriveStaff" runat="server">
                            <advWebControls:ListContainerControl AutoGenerateColumns="false" ID="NDriveStaff"
                                runat="server" Text="Add Staff" OnPreRender="NDriveStaff_OnPreRender" OnRowDataBound="NDriveStaff_OnRowDataBound"
                                OnDeleteClick="NDriveStaff_OnDeleteClick" OnAddClick="NDriveStaff_OnAddClick"
                                OnRowCommand="NDriveStaff_OnRowCommand">
                                <Columns>
                                    <asp:BoundField HeaderText="Staff" />
                                </Columns>
                                <DeleteItemTemplate>
                                    <asp:Panel ID="pnldelete" runat="server" SkinID="HH_PanelPopup" Height="200" Width="275"
                                        BorderColor="Navy" BorderStyle="Solid" BorderWidth="1px">
                                        <div align="center">
                                            Are you sure you wish to delete this record?
                                            <asp:HiddenField ID="hidDeleteNDriveStaff" runat="server" Value='<%#Eval("NDriveAccessID")%>' />
                                        </div>
                                    </asp:Panel>
                                </DeleteItemTemplate>
                                <InsertItemTemplate>
                                    <asp:Panel runat="server" ID="pnlinsert" SkinID="HH_PanelPopup" Height="200" Width="275"
                                        BorderColor="Navy" BorderStyle="Solid" BorderWidth="1px">
                                        <div align="center">
                                            Grant Access To:
                                            <advWebControls:PersonDropDown ID="advPersonDropDown" runat="server" RaiseEventOnItemSelected="false"
                                                PopulatedEventCausesValidation="false" ServiceMethod="TimekeeperService" />
                                        </div>
                                        <br />
                                        <span class="info">Enter Employee Name or Payroll ID Number</span>
                                    </asp:Panel>
                                </InsertItemTemplate>
                            </advWebControls:ListContainerControl>
                        </div>
                    </td>
                </tr>
            </ContentTemplate>
        </asp:UpdatePanel>
    </table>
    <asp:Panel ID="pnlErrs" runat="server" CssClass="errdiv">
    </asp:Panel>
    <asp:CustomValidator ID="valPage" runat="server" OnServerValidate="valPage_ServerValidate" />
</asp:Content>
