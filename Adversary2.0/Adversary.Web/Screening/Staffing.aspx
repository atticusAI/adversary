﻿<%@ Page Theme="HH" Title="" Language="C#" MasterPageFile="~/Screening/Screening.Master"
    AutoEventWireup="true" CodeBehind="Staffing.aspx.cs" Inherits="Adversary.Web.Screening.Staffing" %>

<%@ Register TagPrefix="uc" TagName="Staffing" Src="~/Controls/ListContainerControl.ascx" %>
<%@ Register TagPrefix="uc" TagName="BillingAttorney" Src="~/Controls/PersonAutoComplete.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../App_Themes/HH/HH.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMainContent" runat="server">
    <table border="0" cellpadding="0" cellspacing="2" width="800">
        <tr>
            <td colspan="2">
                Proposed Initial Staffing
            </td>
        </tr>
    </table>
    <uc:Staffing ID="StaffingControl" runat="server" AutoGenerateColumns="false" AddY="45"
        DeleteY="45" PopupCssClass="pop" BackgroundCssClass="pop-back" OnRowDataBound="StaffingControl_RowDataBound"
        OnPreRender="StaffingControl_PreRender" OnAddClick="StaffingControl_AddClick"
        OnDeleteClick="StaffingControl_DeleteClick" EmptyDataText="No staffing specified"
        Text="Add Staffing">
        <Columns>
            <asp:BoundField ShowHeader="true" HeaderText="Staff" />
        </Columns>
        <InsertItemTemplate>
            Employee Last Name or Personnel ID:
            <uc:BillingAttorney ID="ucStaffingID" runat="server" ServiceMethod="TimekeeperService" />
        </InsertItemTemplate>
        <DeleteItemTemplate>
            Are you sure you wish to delete this record?
            <asp:HiddenField ID="hidDeleteStaffingID" runat="server" Value='<%#Eval("StaffingID")%>' />
        </DeleteItemTemplate>
    </uc:Staffing>
    <table border="0" cellpadding="3" cellspacing="2">
        <tr>
            <td class="lbl">
                Billing Attorney:
            </td>
            <td>
                <uc:BillingAttorney ID="ucBillingAttorney" runat="server" ServiceMethod="TimekeeperService" />
            </td>
        </tr>
    </table>
</asp:Content>
