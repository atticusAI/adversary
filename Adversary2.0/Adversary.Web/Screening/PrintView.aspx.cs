﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Adversary.Web.Screening
{
    public partial class PrintView : ScreeningBasePage
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.DisplayMode = DisplayMode.PRINT;
            this.smsView.ThisMemo = ThisMemo;
            this.aptPrintView.ThisMemo = ThisMemo;
        }

        protected void lblHdeader_PreRender(object sender, EventArgs e)
        {
            if (ThisMemo == null || ThisMemo.ScrMemID <= 0)
            {
                this.lblHeader.Text = "Print view is unavailable, no memo selected";
            }
            else
            {
                this.lblHeader.Text = string.Format("Screening Memo - {0} - {1}", 
                    ThisMemo.ScrMemID, 
                    this.AdversaryDataLayer.GetMemoTypeDescription(ThisMemo));
            }
        }
    }
}