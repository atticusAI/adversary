﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.DataLayer;

namespace Adversary.Web.Screening
{
    public partial class RejectionReason : ScreeningBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
                tbRejectionReason.Text = ThisMemo.RejectDesc;
        }

        public override void Validate()
        {
            base.Validate();
            RejectionReasonScreen rej = new RejectionReasonScreen(ThisMemo.ScrMemID);
            rej.RejectionReason = tbRejectionReason.Text;
            rej.Save();
        }
    }
}