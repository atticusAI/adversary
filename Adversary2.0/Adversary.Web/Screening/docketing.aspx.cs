﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.Web.Controls;

namespace Adversary.Web.Screening
{
    public partial class docketing : ScreeningBasePage
    {
        private bool _IsAjaxPostback = false;
        private Adversary.DataLayer.Docketing _Docketing;

        protected override void OnLoad(EventArgs e)
        {
            _Docketing = new DataLayer.Docketing(ThisMemo.ScrMemID);
            ucDocketingAtty.AutoCompletePopulated += new PersonAutoComplete.PersonAutoCompleteHandler(ucDocketingAtty_AutoCompletePopulated);
            ucDocketingStaff.AutoCompletePopulated += new PersonAutoComplete.PersonAutoCompleteHandler(ucDocketingStaff_AutoCompletePopulated);
        }

        void ucDocketingStaff_AutoCompletePopulated(object sender, Adversary.Controls.PersonAutoCompletePopulatedEventArgs e)
        {
            _IsAjaxPostback = true;
            SaveMemoInformationOnly();
        }

        void ucDocketingAtty_AutoCompletePopulated(object sender, Adversary.Controls.PersonAutoCompletePopulatedEventArgs e)
        {
            _IsAjaxPostback = true;
            SaveMemoInformationOnly();
        }

        private void SaveMemoInformationOnly()
        {
            HiddenField hidStaff = (HiddenField)ucDocketingStaff.FindControl("hidEmployeeCode");
            HiddenField hidAtty = (HiddenField)ucDocketingAtty.FindControl("hidEmployeeCode");
            if (hidStaff.Value.Length > 0)
            {
                _Docketing.DocketingStaff = hidStaff.Value;
            }
            if (hidAtty.Value.Length > 0)
            {
                _Docketing.DocketingAttorney = hidAtty.Value;
            }
            _Docketing.Save();
        }


        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            ReloadMemo();
            ucDocketingAtty.SelectedEmployeeCode = ThisMemo.DocketingAtty;
            ucDocketingStaff.SelectedEmployeeCode = ThisMemo.DocketingStaff;

        }

        private void BindGrid()
        {
            _Docketing.RefreshDocketingTeam();
            this.DocketingStaff.DataSource = _Docketing.DocketingTeam.OrderBy(I => I.PersonnelID);
            this.DocketingStaff.DataBind();
        }

        public void DocketingStaff_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Adversary.DataLayer.DocketingTeam D = (Adversary.DataLayer.DocketingTeam)e.Row.DataItem;
                e.Row.Cells[1].Text = AdversaryDataLayer.GetEmployee(D.PersonnelID).EmployeeName +
                        " (" + D.PersonnelID.ToString() + ")";
            }
        }

        public void DocketingStaff_OnPreRender(object sender, EventArgs e)
        {
            BindGrid();
        }

        public void DocketingStaff_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            DocketingStaff.DataSource = _Docketing.GetDocketingTeam();
            _IsAjaxPostback = true;
        }

        public void DocketingStaff_OnAddClick(object sender, EventArgs e)
        {
            PersonAutoComplete pautoComplete = (PersonAutoComplete)DocketingStaff.FindAddControl("advPersonDropDown");
            HiddenField hidEmployeeCode = (HiddenField)pautoComplete.FindControl("hidEmployeeCode");
            _Docketing.AddDocketingTeamMember(hidEmployeeCode.Value);
            _IsAjaxPostback = true;
            BindGrid();
        }

        public void DocketingStaff_OnDeleteClick(object sender, EventArgs e)
        {
            HiddenField hidDeletedStaff = (HiddenField)DocketingStaff.FindDeleteControl("hidDeleteDocketingStaff");
            _Docketing.DeleteDocketingTeamMember(Convert.ToInt32(hidDeletedStaff.Value.ToString()));
            _IsAjaxPostback = true;
            BindGrid();
        }
        


        public override void Validate()
        {
            base.Validate();
            HiddenField hidAtty = (HiddenField)ucDocketingAtty.FindControl("hidEmployeeCode");
            HiddenField hidStaff = (HiddenField)ucDocketingStaff.FindControl("hidEmployeeCode");

            _Docketing.DocketingAttorney = hidAtty.Value;
            _Docketing.DocketingStaff = hidStaff.Value;
            _Docketing.Save();
        }
    }
    
}