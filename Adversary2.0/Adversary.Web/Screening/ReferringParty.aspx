﻿<%@ Page Title="" Theme="HH" Language="C#" MasterPageFile="~/Screening/Screening.Master"
    AutoEventWireup="true" CodeBehind="ReferringParty.aspx.cs" Inherits="Adversary.Web.Screening.ReferringParty" %>

<%@ Register TagPrefix="ajax" Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" %>
<%@ MasterType VirtualPath="~/Screening/Screening.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMainContent" runat="server">
    <script src="../Scripts/AccordionPane.js" type="text/javascript"></script>
    <table border="0" cellpadding="3" cellspacing="2">
        <tr>
            <td class="lbl">
                Please Enter How This Client Came
                <br />
                to Select H&H for this work
            </td>
            <td>
                <asp:TextBox ID="tbOriginationNotes" runat="server" SkinID="HH_Textbox" Rows="5"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="lbl" rowspan="3">
                Address:
            </td>
            <td>
                <asp:TextBox ID="tbAddress1" runat="server" SkinID="HH_Textbox" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="tbAddress2" runat="server" SkinID="HH_Textbox" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="tbAddress3" runat="server" SkinID="HH_Textbox" />
            </td>
        </tr>
        <tr>
            <td class="lbl">
                City:
            </td>
            <td>
                <asp:TextBox ID="tbCity" runat="server" SkinID="HH_Textbox" />
            </td>
        </tr>
        <tr>
            <td class="lbl">
                State:
            </td>
            <td>
                <asp:TextBox ID="tbState" runat="server" SkinID="HH_Textbox" />
            </td>
        </tr>
        <tr>
            <td class="lbl">
                ZIP:
            </td>
            <td>
                <asp:TextBox ID="tbZIP" runat="server" SkinID="HH_Textbox" />
            </td>
        </tr>
        <tr>
            <td class="lbl">
                Country:
            </td>
            <td>
                <asp:TextBox ID="tbCountry" runat="server" SkinID="HH_Textbox" />
            </td>
        </tr>
        <tr>
            <td class="lbl">
                Phone:
            </td>
            <td>
                <asp:TextBox ID="tbPhone" runat="server" SkinID="HH_Textbox" />
            </td>
        </tr>
        <tr>
            <td class="lbl">
                Fax:
            </td>
            <td>
                <asp:TextBox ID="tbFax" runat="server" SkinID="HH_Textbox" />
            </td>
        </tr>
        <tr>
            <td class="lbl">
                Email:
            </td>
            <td>
                <asp:TextBox ID="tbEmail" runat="server" SkinID="HH_Textbox" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Party Type:
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:RadioButtonList runat="server" ID="rblPartyType" RepeatLayout="Table" RepeatDirection="Horizontal"
                    CausesValidation="false" AutoPostBack="false">
                    <asp:ListItem Text="Individual" />
                    <asp:ListItem Text="Organization" />
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <ajax:Accordion ID="accParty" Width="100%" FadeTransitions="true" FramesPerSecond="60"
                    Height="199" RequireOpenedPane="true" runat="server" TransitionDuration="200"
                    SelectedIndex="-1" AutoSize="None">
                    <Panes>
                        <ajax:AccordionPane ID="apIndividual" runat="server">
                            <Content>
                                <table border="0" cellpadding="3" cellspacing="2">
                                    <tr>
                                        <td class="lbl">
                                            First Name:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbFirstName" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lbl">
                                            Middle Name:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbMiddleName" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lbl">
                                            Last Name:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbLastName" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </Content>
                        </ajax:AccordionPane>
                        <ajax:AccordionPane ID="apOrganization" runat="server">
                            <Content>
                                <table border="0" cellpadding="3" cellspacing="2">
                                    <tr>
                                        <td class="lbl">
                                            Organization Name:
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" TextMode="MultiLine" AutoPostBack="false" ID="tbOrganization"
                                                Rows="3" Columns="60" />
                                        </td>
                                    </tr>
                                </table>
                            </Content>
                        </ajax:AccordionPane>
                    </Panes>
                </ajax:Accordion>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hidPartyID" runat="server" />
</asp:Content>
