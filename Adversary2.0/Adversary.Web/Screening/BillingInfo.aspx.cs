﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.DataLayer;

namespace Adversary.Web.Screening
{
    public partial class BillingInfo : ScreeningBasePage
    {
        MemoBillingInfo m;
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            m = new MemoBillingInfo(ThisMemo.ScrMemID);
            m.Get();
            
        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (m.Billings.Count > 0 && !IsPostBack)
            {
                Billing bill = m.Billings.FirstOrDefault();
                hidBillingID.Value = bill.BillingID.ToString();
                tbSpecialBillFormat.Text = bill.BillFormat;
                tbSpecialBillingInstructions.Text = bill.BillInstructions;
                tbSpecialFeeArrangements.Text = bill.SpecialFee;
                if (bill.LateInterest.HasValue)
                {
                    rblInterestLatePayments.Items.FindByValue((Convert.ToBoolean(bill.LateInterest) ? "yes" : "no")).Selected = true;
                }
                if (bill.ClientLevelBill.HasValue)
                {
                    rblClientLevelBilling.Items.FindByValue((Convert.ToBoolean(bill.ClientLevelBill) ? "yes" : "no")).Selected = true;
                }

            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public override void Validate()
        {
            base.Validate();
            int __BillingID = 0;
            bool convertsuccess = Int32.TryParse(hidBillingID.Value, out __BillingID);
            
            if(convertsuccess) m.BillingID = Convert.ToInt32(hidBillingID.Value);

            m.BillFormat = tbSpecialBillFormat.Text;
            m.BillInstructions = tbSpecialBillingInstructions.Text;
            m.SpecialFee = tbSpecialFeeArrangements.Text;
            if (rblClientLevelBilling.SelectedIndex > -1)
            {
                m.ClientLevelBill = (rblClientLevelBilling.SelectedItem.Value.Equals("yes") ? true : false);
            }
            if (rblInterestLatePayments.SelectedIndex > -1)
            {
                m.LateInterest = (rblInterestLatePayments.SelectedItem.Value.Equals("yes") ? true : false);
            }
            m.Save();
        }
    }
}