﻿<%@ Page Title="" Theme="HH" Language="C#" MasterPageFile="~/Screening/Screening.Master" AutoEventWireup="true" CodeBehind="AdverseParties.aspx.cs" Inherits="Adversary.Web.Screening.AdverseParties" %>
<%@ Register TagPrefix="ajax" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="Adversary.Controls" Namespace="Adversary.Controls" TagPrefix="advControls" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/AccordionPane.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphMainContent" runat="server">
        <asp:UpdatePanel ID="upnlAdversePartyGrid" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                    <asp:GridView ID="gvAdversePartys" runat="server" AutoGenerateColumns="false" OnRowCommand="gvAdversePartys_RowCommand"
            SkinID="HH_Gridview">
            <Columns>
                <asp:TemplateField ShowHeader="false">
                    <ItemTemplate>
                        <asp:LinkButton ID="lbModifyAdverseParty" runat="server" Text="Modify" 
                            CausesValidation="false" CommandArgument='<%#Eval("PartyID")%>' CommandName="Modify" />
                        <asp:LinkButton ID="lbDeleteAdverseParty" runat="server" Text="Delete" CommandName="Delete" CommandArgument='<%#Eval("PartyID")%>'
                        OnClientClick="javascript:return confirm('Are you sure you want to delete this record?');" />                                        
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="PartyRelationshipName" ShowHeader="true" HeaderText="Relationship" />
                <asp:BoundField DataField="PartyType" ShowHeader="true" HeaderText="Type" />
                <asp:BoundField HeaderText="Party Name" />
            </Columns>
        </asp:GridView>     

            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:LinkButton ID="lbAddAdversePartys" runat="server" Text="Add Adverse Parties" CausesValidation="false" />
        <asp:Button ID="btnModifyAdverseParty_Hidden" runat="server" style="display:none;"/>

        <ajax:ModalPopupExtender ID="mdlAdversePartys" runat="server" TargetControlID="lbAddAdversePartys"
            PopupControlID="pnlAdversePartyEntry" PopupDragHandleControlID="pnlAdversePartyEntry" />
        <ajax:ModalPopupExtender ID="mdlModifyAdverseParty" runat="server" 
            TargetControlID="btnModifyAdverseParty_Hidden"
            PopupControlID="pnlAdversePartyModify" />
            <asp:Panel ID="pnlAdversePartyEntry" runat="server">
            <asp:UpdatePanel ID="upnlAdversePartys" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
                <ContentTemplate>
                    <asp:Wizard runat="server" ID="adverseWizard" OnNextButtonClick="adverseWizard_NextButtonClick" CancelButtonText="Cancel"
                        OnCancelButtonClick="adverseWizard_CancelButtonClick" CancelButtonType="Link" FinishCompleteButtonType="Link"
                        DisplayCancelButton="true" BackColor="White" StartNextButtonType="Link"
                        OnFinishButtonClick="adverseWizard_FinishButtonClick" OnSideBarButtonClick="adverseWizard_SideBarButtonClick"
                        Width="450px" Height="200px" BorderColor="Navy" BorderStyle="Solid" BorderWidth="1px">
                        <SideBarStyle VerticalAlign="Top" />
                        <WizardSteps>
                            <asp:WizardStep ID="partyNameStep" runat="server" Title="Enter Party Names">
                                <asp:TextBox runat="server" ID="tbPartyNames" TextMode="MultiLine" Columns="40" Rows="4" />
                            </asp:WizardStep>
                            <asp:WizardStep ID="definePartyRelationshipsStep" runat="server" 
                                Title="Define Party Relationships">
                                <asp:GridView runat="server" ID="gvPartyRelationships" AutoGenerateColumns="false" 
                                    OnRowDataBound="gvPartyRelationships_RowDataBound"
                                    ShowHeader="false"
                                    ShowFooter="false">
                                    <Columns>
                                        <asp:TemplateField ShowHeader="false">
                                            <ItemTemplate>
                                                <asp:TextBox ID="tbName" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ShowHeader="false">
                                            <ItemTemplate>
                                                <asp:DropDownList runat="server" ID="ddlPartyType">
                                                    <asp:ListItem Text="Individual" />
                                                    <asp:ListItem Text="Organization" />
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ShowHeader="false">
                                            <ItemTemplate>
                                                <advControls:PartyRelationshipDropDown ID="partyrelPartyRelationshipDropDown" runat="server" RelationshipDisplayMode="AdverseParties" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </asp:WizardStep>
                        </WizardSteps>
                    </asp:Wizard>
                </ContentTemplate>
            </asp:UpdatePanel>
            </asp:Panel>
            <asp:Panel ID="pnlAdversePartyModify" runat="server">
            <asp:UpdatePanel ID="upnlAdversePartyModify" runat="server" UpdateMode="Always" ChildrenAsTriggers="true">
                <ContentTemplate>
                    <asp:HiddenField ID="hidAdversePartyModifyPartyID" runat="server" />
                    <table border="0" cellpadding="0" cellspacing="0" style="background-color:wheat;">
                        <tr>
                            <td colspan="2">
                                Party Type:
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:RadioButtonList runat="server" ID="rblAdversePartyModifyPartyType">
                                    <asp:ListItem Text="Individual" />
                                    <asp:ListItem Text="Organization" />
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <ajax:Accordion ID="accAdversePartyModify" Width="100%" FadeTransitions="true" FramesPerSecond="60" Height="199"
                                        RequireOpenedPane="true" runat="server" TransitionDuration="200" SelectedIndex="-1" AutoSize="None">
                                        <Panes>
                                        <ajax:AccordionPane ID="apAdversePartyModifyIndividual" runat="server">
                                            <Content>
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>First Name</td>
                                                        <td><asp:TextBox ID="tbAdversePartyModifyFirstName" runat="server" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Middle Name</td>
                                                        <td><asp:TextBox ID="tbAdversePartyModifyMiddleName" runat="server" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Last Name</td>
                                                        <td><asp:TextBox ID="tbAdversePartyModifyLastName" runat="server" /></td>
                                                    </tr>
                                                </table>
                                            </Content>
                                        </ajax:AccordionPane>
                                        <ajax:AccordionPane ID="apAdversePartyModifyOrganization" runat="server">
                                            <Content>
                                                <table border="0" cellpadding="1" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td valign="top">
                                                            Organization Name:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox runat="server" TextMode="MultiLine"
                                                                AutoPostBack="false" ID="tbAdversePartyModifyOrganization"
                                                                Rows="3" Columns="60" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </Content>
                                        </ajax:AccordionPane>
                                        </Panes>
                                </ajax:Accordion>
                            </td>
                        </tr>
                            
                        <tr>
                            <td>
                                Party Relationship:
                            </td>
                            <td>
                                <advControls:PartyRelationshipDropDown ID="partyrelAdversePartyModify" runat="server" RelationshipDisplayMode="AdverseParties" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <asp:LinkButton ID="btnAdversePartyModifySubmit" runat="server" Text="Submit" OnClick="btnAdversePartyModifySubmit_Click" />
                                    &nbsp;&nbsp;&nbsp;
                                <asp:LinkButton ID="btnAdversePartyModifyCancel" runat="server" Text="Cancel" OnClick="btnAdversePartyModifyCancel_Click" />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
           
        </asp:Panel> 
</asp:Content>

