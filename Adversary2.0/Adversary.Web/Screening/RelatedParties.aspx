﻿<%@ Page Title="" Theme="HH" Language="C#" MasterPageFile="~/Screening/Screening.Master"
    AutoEventWireup="true" CodeBehind="RelatedParties.aspx.cs" Inherits="Adversary.Web.Screening.RelatedParties" %>

<%@ Register TagPrefix="ajax" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="Adversary.Controls" Namespace="Adversary.Controls" TagPrefix="advControls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/AccordionPane.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMainContent" runat="server">
    <strong>Please be careful to enter the correct Individual(s)/Organization(s) and their
        Relationship(s). Referring Organization may also be entered.</strong><br />
    <br />
    <strong>Please use code 102 &quot;Co-Client Affiliate&quot; if your client is an organization
        and you also represent one of its related organizations, such as a subsidiary, or
        sister company. Use code 101 &quot;Co-Client (Non Affiliate)&quot; for all other
        multiple representations. </strong><br />
    <br />
    <asp:UpdatePanel ID="upnlRelatedPartyGrid" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="gvRelatedPartys" runat="server" AutoGenerateColumns="false" OnRowCommand="gvRelatedPartys_RowCommand"
                SkinID="HH_Gridview">
                <Columns>
                    <asp:TemplateField ShowHeader="false">
                        <ItemTemplate>
                            <asp:LinkButton ID="lbModifyRelatedParty" runat="server" Text="Modify" CausesValidation="false"
                                CommandArgument='<%#Eval("PartyID")%>' CommandName="Modify" />
                            <asp:LinkButton ID="lbDeleteRelatedParty" runat="server" Text="Delete" CommandName="Delete"
                                CommandArgument='<%#Eval("PartyID")%>' OnClientClick="javascript:return confirm('Are you sure you want to delete this record?');" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="PartyRelationshipName" ShowHeader="true" HeaderText="Relationship" />
                    <asp:BoundField DataField="PartyType" ShowHeader="true" HeaderText="Type" />
                    <asp:BoundField HeaderText="Party Name" />
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:LinkButton ID="lbAddRelatedPartys" runat="server" Text="Add Related Parties"
        CausesValidation="false" />
    <asp:Button ID="btnModifyRelatedParty_Hidden" runat="server" Style="display: none;" />
    <ajax:ModalPopupExtender ID="mdlRelatedPartys" runat="server" TargetControlID="lbAddRelatedPartys"
        PopupControlID="pnlRelatedPartyEntry" PopupDragHandleControlID="pnlRelatedPartyEntry" />
    <ajax:ModalPopupExtender ID="mdlModifyRelatedParty" runat="server" TargetControlID="btnModifyRelatedParty_Hidden"
        PopupControlID="pnlRelatedPartyModify" />
    <asp:Panel ID="pnlRelatedPartyEntry" runat="server">
        <asp:UpdatePanel ID="upnlRelatedPartys" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
            <ContentTemplate>
                <asp:Wizard runat="server" ID="RelatedWizard" OnNextButtonClick="RelatedWizard_NextButtonClick"
                    CancelButtonText="Cancel" OnCancelButtonClick="RelatedWizard_CancelButtonClick"
                    CancelButtonType="Link" FinishCompleteButtonType="Link" DisplayCancelButton="true"
                    BackColor="White" StartNextButtonType="Link" OnFinishButtonClick="RelatedWizard_FinishButtonClick"
                    OnSideBarButtonClick="RelatedWizard_SideBarButtonClick" Width="450px" Height="200px"
                    BorderColor="Navy" BorderStyle="Solid" BorderWidth="1px">
                    <SideBarStyle VerticalAlign="Top" />
                    <WizardSteps>
                        <asp:WizardStep ID="partyNameStep" runat="server" Title="Enter Party Names">
                            <asp:TextBox runat="server" ID="tbPartyNames" TextMode="MultiLine" Columns="40" Rows="4" />
                        </asp:WizardStep>
                        <asp:WizardStep ID="definePartyRelationshipsStep" runat="server" Title="Define Party Relationships">
                            <asp:GridView runat="server" ID="gvPartyRelationships" AutoGenerateColumns="false"
                                OnRowDataBound="gvPartyRelationships_RowDataBound" ShowHeader="false" ShowFooter="false">
                                <Columns>
                                    <asp:TemplateField ShowHeader="false">
                                        <ItemTemplate>
                                            <asp:TextBox ID="tbName" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ShowHeader="false">
                                        <ItemTemplate>
                                            <asp:DropDownList runat="server" ID="ddlPartyType">
                                                <asp:ListItem Text="Individual" />
                                                <asp:ListItem Text="Organization" />
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ShowHeader="false">
                                        <ItemTemplate>
                                            <advControls:PartyRelationshipDropDown ID="partyrelPartyRelationshipDropDown" runat="server"
                                                RelationshipDisplayMode="RelatedParties" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:WizardStep>
                    </WizardSteps>
                </asp:Wizard>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <asp:Panel ID="pnlRelatedPartyModify" runat="server">
        <asp:UpdatePanel ID="upnlRelatedPartyModify" runat="server" UpdateMode="Always" ChildrenAsTriggers="true">
            <ContentTemplate>
                <asp:HiddenField ID="hidRelatedPartyModifyPartyID" runat="server" />
                <table border="0" cellpadding="0" cellspacing="0" style="background-color: wheat;">
                    <tr>
                        <td colspan="2">
                            Party Type:
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:RadioButtonList runat="server" ID="rblRelatedPartyModifyPartyType">
                                <asp:ListItem Text="Individual" />
                                <asp:ListItem Text="Organization" />
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <ajax:Accordion ID="accRelatedPartyModify" Width="100%" FadeTransitions="true" FramesPerSecond="60"
                                Height="199" RequireOpenedPane="true" runat="server" TransitionDuration="200"
                                SelectedIndex="-1" AutoSize="None">
                                <Panes>
                                    <ajax:AccordionPane ID="apRelatedPartyModifyIndividual" runat="server">
                                        <Content>
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        First Name
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="tbRelatedPartyModifyFirstName" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Middle Name
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="tbRelatedPartyModifyMiddleName" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Last Name
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="tbRelatedPartyModifyLastName" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </Content>
                                    </ajax:AccordionPane>
                                    <ajax:AccordionPane ID="apRelatedPartyModifyOrganization" runat="server">
                                        <Content>
                                            <table border="0" cellpadding="1" cellspacing="0" width="100%">
                                                <tr>
                                                    <td valign="top">
                                                        Organization Name:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" TextMode="MultiLine" AutoPostBack="false" ID="tbRelatedPartyModifyOrganization"
                                                            Rows="3" Columns="60" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </Content>
                                    </ajax:AccordionPane>
                                </Panes>
                            </ajax:Accordion>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Party Relationship:
                        </td>
                        <td>
                            <advControls:PartyRelationshipDropDown ID="partyrelRelatedPartyModify" runat="server"
                                RelationshipDisplayMode="RelatedParties" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:LinkButton ID="btnRelatedPartyModifySubmit" runat="server" Text="Submit" OnClick="btnRelatedPartyModifySubmit_Click" />
                            &nbsp;&nbsp;&nbsp;
                            <asp:LinkButton ID="btnRelatedPartyModifyCancel" runat="server" Text="Cancel" OnClick="btnRelatedPartyModifyCancel_Click" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
