﻿<%@ Page Title="" Language="C#" MasterPageFile="Print.Master" AutoEventWireup="true"
    CodeBehind="PrintView.aspx.cs" Inherits="Adversary.Web.Screening.PrintView" Theme="HHPrint" %>

<%@ Register Src="~/Controls/ScreeningMemoSummary.ascx" TagName="ScreeningMemoSummary"
    TagPrefix="uc1" %>
<%@ Register Src="~/Controls/ApprovalTable.ascx" TagName="ApprovalTable" TagPrefix="uc2" %>
<asp:Content ID="cntHead" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            window.focus();
        });
    </script>
</asp:Content>
<asp:Content ID="cntMain" ContentPlaceHolderID="cphMainContent" runat="server">
    <div class="hdr">
        <asp:Label ID="lblHeader" runat="server" OnPreRender="lblHdeader_PreRender" />
        <span class="error">(Please electronically submit your memo using the submit button)</span><br />
        <span style="display:inline-block; width:100%; text-align:right"><a href="javascript:this.close();">Close</a></span><br />
    </div>
    <uc2:ApprovalTable ID="aptPrintView" runat="server" />
    <uc1:ScreeningMemoSummary ID="smsView" runat="server" DisplayMode="PRINT" />
</asp:Content>
