﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using Adversary.Web.Pages;

namespace Adversary.Web
{
    public partial class Default : TrackBasePage
    {
        bool RequestLogout
        {
            get
            {
                bool logout = false;
                if (!string.IsNullOrWhiteSpace(this.Request.QueryString["logout"]))
                {
                    bool.TryParse(this.Request.QueryString["logout"], out logout);
                }
                return logout;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.RequestLogout)
            {
                this.Logout();
            }
        }

        private void Logout()
        {
            this.Session.Clear();
            this.Session.Abandon();
            FormsAuthentication.SignOut();
            this.Response.Cache.SetExpires(DateTime.Now);
            this.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            this.Response.Cache.SetNoStore();
            this.Response.Redirect("~/Login.aspx");
        }

        protected void dtApproval_PreRender(object sender, EventArgs e)
        {
            ((HtmlGenericControl)sender).Visible = this.CurrentUser.IsAdversary;
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (this.Page.Master is BaseMaster)
            {
                ((BaseMaster)this.Page.Master).TabPageUrl = "/Default.aspx";
                ((BaseMaster)this.Page.Master).SubTabPageUrl = "/Default.aspx";
            }
        }
    }
}