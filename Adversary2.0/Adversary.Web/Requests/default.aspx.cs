﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.Web.Requests;
using Adversary.Web.Pages;

namespace Adversary.Web.Requests
{
    public partial class _default : Adversary.Web.Pages.RequestBasePage
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            base.thisMasterPage.ShowNavigationButtons = false;

            if (this.Page.Master is RequestsMasterPage)
            {
                ((RequestsMasterPage)this.Page.Master).TabPageUrl = "/Requests/Default.aspx";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void lbCreateAdversaryRequest_OnClick(object sender, EventArgs e)
        {
            Adversary.DataLayer.Request req = null;
            req = AdversaryDataLayer.CreateRequest(CurrentUser.CurrentUserPayrollID);

            if (req != null)
            {
                Response.Redirect(String.Format("basicinfo.aspx?requestid={0}", req.ReqID.ToString()));
            }
            else
            {
                ((RequestsMasterPage)this.Page.Master).SetErrorMessage("Failed to create a new request, please contact support.", Web.Controls.MessageDisplayType.DisplayError);
            }
        }

        protected void lbSelectExistingRequest_Click(object sender, EventArgs e)
        {
            Response.Redirect("viewrequests.aspx?type=submitted");
        }
    }
}