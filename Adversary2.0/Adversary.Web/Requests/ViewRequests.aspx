﻿<%@ Page Title="" Theme="HH" Language="C#" MasterPageFile="~/Requests/Requests.Master"
    AutoEventWireup="true" CodeBehind="ViewRequests.aspx.cs" Inherits="Adversary.Web.Requests.ViewRequests" %>

<%@ Register TagPrefix="advcontrols" Assembly="Adversary.Controls" Namespace="Adversary.Controls" %>
<%@ Register TagPrefix="ajax" Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" %>
<asp:Content ID="cntHead" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            validatePlugin = hh.validateWithjQuery("#form1");
        });
    </script>
</asp:Content>
<asp:Content ID="cntMain" ContentPlaceHolderID="cphMainContent" runat="server">
    <asp:Panel ID="pnlRequest" runat="server" DefaultButton="btnFilterRequests">
        <table width="800">
            <tr>
                <td align="right">
                    <table>
                        <tr>
                            <td>Show All Requests Submitted After:
                            </td>
                            <td>
                                <asp:TextBox ID="tbSubmittedAfter" runat="server" SkinID="HH_Textbox" CssClass="required date"
                                    title="Date is required and must be a valid date." />
                                <ajax:CalendarExtender ID="calSubmittedAfter" runat="server" TargetControlID="tbSubmittedAfter"
                                    Animated="false" PopupPosition="BottomLeft" TodaysDateFormat="MM/dd/yyyy" />
                            </td>
                            <td>
                                <asp:Button ID="btnFilterRequests" runat="server" Text="Go" OnClick="btnFilterRequests_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>Show Requests That Are:
                            </td>
                            <td colspan="2">
                                <asp:DropDownList ID="ddlShowRequests" runat="server" SkinID="HH_DropDownList">
                                    <asp:ListItem Text="Pending" Value="pending" />
                                    <asp:ListItem Text="Pending &amp; Completed" Value="pendingcompleted" />
                                    <asp:ListItem Text="UnSubmitted" Value="unsubmitted" />
                                </asp:DropDownList>
                                <br />
                                <asp:CheckBox ID="cbMyRequests" runat="server" Text="Only My Requests" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <%--<asp:UpdateProgress ID="uprgRequests" runat="server" AssociatedUpdatePanelID="upnlRequests"
                        DisplayAfter="0" DynamicLayout="true">
                        <ProgressTemplate>
                            <img src="../Images/ajax-loader.gif" alt="" />
                            Sending...
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel runat="server" ID="upnlRequests" UpdateMode="Conditional" ChildrenAsTriggers="true">
                        <ContentTemplate>--%>
                    <asp:GridView runat="server" ID="gvRequests" SkinID="HH_Gridview" AllowPaging="true"
                        AutoGenerateColumns="false" OnPageIndexChanging="gvRequests_PageIndexChanging" OnRowDataBound="gvRequests_RowDataBound" >
                        <Columns>
                            <asp:TemplateField HeaderText="&nbsp;" ItemStyle-Wrap="false">
                                <ItemTemplate>
                                    <asp:LinkButton OnClick="lb_Click" ID="lbSummary" runat="server" CommandName="Summary" CommandArgument='<%#Eval("ReqID")%>'>Summary</asp:LinkButton><br />
                                    <asp:LinkButton OnClick="lb_Click" ID="lbEdit" runat="server" CommandName="EditReq" CommandArgument='<%#Eval("ReqID")%>'>Edit</asp:LinkButton><br />
                                    <asp:LinkButton OnClick="lb_Click" ID="lbAssign" runat="server" CommandName="AssignReq" CommandArgument='<%#Eval("ReqID")%>'>Assign to Me</asp:LinkButton><br />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="ReqID" HeaderText="Request #" />
                            <asp:TemplateField HeaderText="Assigned To" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <advcontrols:AdversaryGroupDropdown ID="ddlAssign" runat="server" SelectedMember='<%#Eval("WorkBegunBy")%>'
                                        OnAdversaryMemberSelected="advDropDown_AdversaryMemberSelected" CommandArgument='<%#Eval("ReqID")%>' />
                                    <asp:Label ID="lblAssigned" runat="server" Text='<%#Eval("WorkBegunBy")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="ClientName" HeaderText="Client" SortExpression="ClientName" />
                            <asp:BoundField DataField="ClientMatter" HeaderText="Client Matter" SortExpression="ClientMatter" />
                            <asp:BoundField DataField="PracticeCode" HeaderText="Code" SortExpression="PracticeCode" />
                            <asp:BoundField DataField="AttyLogin" HeaderText="From" SortExpression="AttyLogin" />
                            <asp:BoundField DataField="TypistLogin" HeaderText="Typist" SortExpression="Typist" />
                            <asp:BoundField DataField="SubmittalDate" HeaderText="Submitted On" SortExpression="SubmittalDate" />
                            <asp:BoundField DataField="DeadlineDate" HeaderText="Deadline" SortExpression="DeadlineDate"
                                Visible="false" />
                            <asp:BoundField DataField="CompletionDate" HeaderText="Completed" SortExpression="CompletionDate" />
                        </Columns>
                    </asp:GridView>
                    <%--</ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="gvRequests" EventName="RowCommand" />
                        </Triggers>
                    </asp:UpdatePanel>--%>
                </td>
            </tr>
        </table>
        <asp:Panel ID="divErrs" runat="server" CssClass="errdiv">
        </asp:Panel>
        <asp:CustomValidator ID="valPage" runat="server" OnServerValidate="valPage_ServerValidate" />
    </asp:Panel>
</asp:Content>

