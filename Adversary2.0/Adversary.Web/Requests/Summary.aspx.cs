﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.DataLayer;
using Adversary.Requests;

namespace Adversary.Web.Requests
{
    public partial class Summary : Pages.RequestBasePage
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (this.Page.Master is RequestsMasterPage)
            {
                ((RequestsMasterPage)this.Page.Master).TabPageUrl = "/Requests/Default.aspx";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ucRequestSummary.AdversaryDataLayer = base.AdversaryDataLayer;
            ucRequestSummary.Request = ThisRequest;
            if (ThisRequest.HH_IsRequestSubmitted())
            {
                thisMasterPage.SetErrorMessage(RequestMessages.REQ_SUBMITTED_MSG, Web.Controls.MessageDisplayType.DisplayValid);
            }
        }
    }
}