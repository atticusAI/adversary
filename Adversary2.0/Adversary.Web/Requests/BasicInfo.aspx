﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Requests/Requests.Master" AutoEventWireup="true" CodeBehind="BasicInfo.aspx.cs" Inherits="Adversary.Web.Requests.BasicInfo" Theme="HH" %>
<%@ Register TagPrefix="uc" TagName="PersonAutoComplete" Src="~/Controls/PersonAutoComplete.ascx" %>
<%@ Register TagPrefix="uc" Assembly="Adversary.Controls" Namespace="Adversary.Controls" %>
<%@ Register TagPrefix="ajax" Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/AccordionPane.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="server">
    <strong>This is an adversary index search request ONLY. Submission of either a regular or rejected screening memo is required to complete this process.</strong>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMainContent" runat="server">
    <table border="0" cellpadding="2" cellspacing="3">
        <tr>
            <td>Request ID</td>
            <td><asp:Label ID="lblRequestID" runat="server" /><asp:HiddenField ID="hidRequestID" runat="server" /></td>
        </tr>
        <tr>
            <td>Submitted On:</td>
            <td><asp:Label ID="lblSubmittedOn" runat="server" /></td>
        </tr>
        <tr>
            <td>Deadline Date</td>
            <td><asp:Label ID="lblDeadlineDate" runat="server" /></td>
        </tr>
        <tr>
            <td>Client Name</td>
            <td>
                <asp:TextBox ID="tbClientName" runat="server" SkinID="HH_Textbox" Rows="3" Columns="50" TextMode="MultiLine" />
                <ajax:AutoCompleteExtender ID="acxClientName" runat="server" ServiceMethod="ClientService" TargetControlID="tbClientName" />
            </td>
        </tr>
        <tr>
            <td>Submitted By Payroll ID</td>
            <td><uc:PersonAutoComplete ID="ucSubmittedPayrollID" runat="server" RaiseEventOnItemSelected="false" PopulatedEventCausesValidation="false"
                ServiceMethod="TimekeeperService" /></td>
        </tr>
        <tr>
            <td>Attorney Payroll ID</td>
            <td><uc:PersonAutoComplete ID="ucAttorneyPayrollID" runat="server" RaiseEventOnItemSelected="false" PopulatedEventCausesValidation="false"
                ServiceMethod="TimekeeperService" /></td>
        </tr>
        <tr>
            <td>Client Status Category</td>
            <td>
                <asp:RadioButtonList runat="server" ID="rblClientStatusCategory">
                    <asp:ListItem Text="Potential" />
                    <asp:ListItem Text="Existing" />
                </asp:RadioButtonList>
                <%--<asp:UpdatePanel ID="upnlChangeCategory" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
                    <ContentTemplate>
                        <asp:DropDownList ID="ddlClientStatusCategory" runat="server" SkinID="HH_DropDownList" 
                            OnSelectedIndexChanged="ddlClientStatusCategory_OnSelectedIndexChanged" AutoPostBack="true">
                            <asp:ListItem Text="Potential" />
                            <asp:ListItem Text="Existing" />
                        </asp:DropDownList>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddlClientStatusCategory" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>--%>
            </td>
        </tr>
        <tr>
            <td>
                Client Matter Number (if existing client)
            </td>
            <td>
                <ajax:Accordion ID="accClientMatterNumber" runat="server" Height="60" RequireOpenedPane="false">
                    <Panes>
                        <ajax:AccordionPane ID="apClientMatterNumberDisabled" runat="server">
                            <Content>
                                <asp:TextBox ID="tbClientMatterNumberDisabled" runat="server" SkinID="HH_Textbox_Disabled" Enabled="false" />
                            </Content>
                        </ajax:AccordionPane>
                        <ajax:AccordionPane ID="apClientMatterNumberEnabled" runat="server">
                            <Content>
                                <asp:TextBox ID="tbClientMatterNumber" runat="server" SkinID="HH_Textbox" />
                            </Content>             
                        </ajax:AccordionPane>
                        
                    </Panes>
                </ajax:Accordion>
                    
            </td>

            <%--<td>Client Matter Number (if existing client)</td>
            <td>
                <asp:Panel ID="pnlClientMatterNumber" runat="server"><asp:TextBox ID="tbClientMatterNumber" runat="server" CssClass="input" /></asp:Panel>
                <asp:Panel ID="pnlClientMatterNumberDisabled" runat="server"><asp:TextBox ID="tbClientMatterNumber_Disabled" runat="server" CssClass="inputDisabled" Enabled="false" /></asp:Panel>
                
            </td>--%>
        </tr>
        <tr>
            <td>Practice Type/Specialty Code</td>
            <td><uc:PracticeCodeDropdown runat="server" ID="ucPracticeCodeDropdown" IsRequired="true" /></td>
        </tr>
        <tr>
            <td>Keep this adversary request confidential?</td>
            <td><asp:CheckBox ID="cbConfidential" runat="server" Text="Yes" /></td>
        </tr>
    </table>
</asp:Content>
