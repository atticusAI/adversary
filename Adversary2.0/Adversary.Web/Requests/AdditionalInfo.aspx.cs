﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Adversary.Web.Requests
{
    public partial class AdditionalInfo : Pages.RequestBasePage
    {
        private Adversary.DataLayer.Requests.AdditionalInformation __addlInfo;

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (this.Page.Master is RequestsMasterPage)
            {
                ((RequestsMasterPage)this.Page.Master).TabPageUrl = "/Requests/Default.aspx";
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (ThisRequest == null)
            {
                this.pnlError.Visible = true;
                this.pnlRequest.Visible = false;
                return;
            }
            __addlInfo = new DataLayer.Requests.AdditionalInformation(ThisRequest.ReqID);

            if (!Page.IsPostBack)
            {
                cbAccounting.Checked = __addlInfo.IsAccounting;
                cbBankruptcy.Checked = __addlInfo.IsBankruptcy;
                cbCommunications.Checked = __addlInfo.IsCommunications;
                cbInsurance.Checked = __addlInfo.IsInsurance;
                cbGovernment.Checked = __addlInfo.IsGovernment;
                cbElectric.Checked = __addlInfo.IsElectricUtility;
                cbIndianTribe.Checked = __addlInfo.IsIndianTribe;
                cbLawFirm.Checked = __addlInfo.IsLawFirm;
                cbLegalFees.Checked = __addlInfo.IsLegalFees;
                cbNoneOfTheAbove.Checked = __addlInfo.NoneOfTheAbove;
                cbOilGas.Checked = __addlInfo.IsOilGas;
                cbSkiResort.Checked = __addlInfo.IsSkiResort;

                ToggleCheckBoxes(!__addlInfo.NoneOfTheAbove, false, __addlInfo.NoneOfTheAbove);
            }
        }
        public void cbNoneOfTheAbove_OnCheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            if (cb.Checked) ShowPopup();
            else
            {
                ToggleCheckBoxes(true, true, true);
                //cvalNoCheckboxes.IsValid = false;
                upnlMessage.Update();
            }
        }

        //protected override void OnPreRender(EventArgs e)
        //{
        //    base.OnPreRender(e);
        //    if (__modalClickMade || __noneClickMade) ToggleCheckBoxes(__enableCheckboxes, false);
        //}

        private void ToggleCheckBoxes(bool isEnabled, bool setNoneOfTheAbove, bool clearCheckboxes)
        {
            foreach (Control ctrl in pnlCheckboxes.Controls)
            {
                if (ctrl is CheckBox)
                {
                    if ((setNoneOfTheAbove && ctrl.ID.Equals("cbNoneOfTheAbove")) || !ctrl.ID.Equals("cbNoneOfTheAbove"))
                    {
                        EnableCheckbox((CheckBox)ctrl, isEnabled);
                        if (clearCheckboxes) ((CheckBox)ctrl).Checked = false;
                    }
                }
            }
        }

        private void EnableCheckbox(CheckBox cb, bool isEnabled)
        {
            cb.Enabled = isEnabled;
        }

        public void btnConfirm_Click(object sender, EventArgs e)
        {
            ToggleCheckBoxes(false, false, true);
            lblNoCheckboxes.Text = string.Empty;
            upnlMessage.Update();
        }

        public void btnCancel_Click(object sender, EventArgs e)
        {
            ToggleCheckBoxes(true, true, true);
            
        }

        public void ShowPopup()
        {
            mdlCheckboxConfirm.Show();
        }

        public override void Validate()
        {
            //validate the checkboxes
            if(!cbNoneOfTheAbove.Checked &&
                !cbAccounting.Checked &&
                !cbBankruptcy.Checked &&
                !cbCommunications.Checked &&
                !cbElectric.Checked &&
                !cbGovernment.Checked &&
                !cbIndianTribe.Checked &&
                !cbInsurance.Checked &&
                !cbLawFirm.Checked &&
                !cbLegalFees.Checked &&
                !cbOilGas.Checked &&
                !cbSkiResort.Checked)
            {
                thisMasterPage.AllowNavigation = false;
                lblNoCheckboxes.Text = "You have not checked any boxes. If the client or adverse party does not fit any of the criteria listed please select 'None of the above'.";
                upnlMessage.Update();
                return;
            }
            


            base.Validate();
            if (cbNoneOfTheAbove.Checked)
            {
                __addlInfo.IsAccounting = false;
                __addlInfo.IsBankruptcy = false;
                __addlInfo.IsCommunications = false;
                __addlInfo.IsElectricUtility = false;
                __addlInfo.IsGovernment = false;
                __addlInfo.IsIndianTribe = false;
                __addlInfo.IsInsurance = false;
                __addlInfo.IsLawFirm = false;
                __addlInfo.IsLegalFees = false;
                __addlInfo.IsOilGas = false;
                __addlInfo.IsSkiResort = false;
                __addlInfo.NoneOfTheAbove = true;

            }
            else
            {
                __addlInfo.IsAccounting = cbAccounting.Checked;
                __addlInfo.IsBankruptcy = cbBankruptcy.Checked;
                __addlInfo.IsCommunications = cbCommunications.Checked;
                __addlInfo.IsElectricUtility = cbElectric.Checked;
                __addlInfo.IsGovernment = cbGovernment.Checked;
                __addlInfo.IsIndianTribe = cbIndianTribe.Checked;
                __addlInfo.IsInsurance = cbInsurance.Checked;
                __addlInfo.IsLawFirm = cbLawFirm.Checked;
                __addlInfo.IsLegalFees = cbLegalFees.Checked;
                __addlInfo.IsOilGas = cbOilGas.Checked;
                __addlInfo.IsSkiResort = cbSkiResort.Checked;
                __addlInfo.NoneOfTheAbove = false;
            }
            __addlInfo.Save();
        }
    }
}