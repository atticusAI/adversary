﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Requests/Requests.Master" AutoEventWireup="true" CodeBehind="AdditionalInfo.aspx.cs" Inherits="Adversary.Web.Requests.AdditionalInfo" Theme="HH" %>

<%@ Register TagPrefix="ajax" Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/jquery-1.7.1.min.js" type="text/javascript"></script>
    <%--<script type="text/javascript">
    function CheckForSelection(sender, args) {
        var selected = $('#CheckBoxList1 input:checked').length;
        if (selected < 1) {
            args.IsValid = false;
        }
        else {
            args.IsValid = true;
        }

    } 

</script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="server">
    <asp:UpdatePanel ID="upnlMessage" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Label ID="lblNoCheckboxes" runat="server" Font-Bold="true" ForeColor="Red" />
            <%--<asp:CustomValidator ID="cvalNoCheckboxes" runat="server" ClientValidationFunction="CheckForSelection"
                ForeColor="Red" Font-Bold="true"
                ErrorMessage="You have not checked any boxes. If the client or adverse party does not fit any of the criteria listed please select 'None of the above'." />--%>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMainContent" runat="server">
    <asp:Panel ID="pnlError" runat="server" Visible="false">
        <asp:Label ID="lblError" runat="server" CssClass="error">No Request is selected.</asp:Label>
    </asp:Panel>
    <asp:Panel ID="pnlRequest" runat="server">
        <asp:UpdatePanel ID="upnlAddlInfoCheckboxes" runat="server" ChildrenAsTriggers="true">
            <ContentTemplate>
                <asp:Panel ID="pnlCheckboxes" runat="server">
                    <table border="0" cellpadding="3" cellspacing="5">
                        <tr>
                            <td>This is a bankruptcy matter.</td>
                            <td>
                                <asp:CheckBox ID="cbBankruptcy" runat="server" /></td>
                        </tr>
                        <tr>
                            <td>This is a telecommunications client or matter.</td>
                            <td>
                                <asp:CheckBox ID="cbCommunications" runat="server" /></td>
                        </tr>
                        <tr>
                            <td>This client is an insurance company.</td>
                            <td>
                                <asp:CheckBox ID="cbInsurance" runat="server" /></td>
                        </tr>
                        <tr>
                            <td>This client is a governmental entity.</td>
                            <td>
                                <asp:CheckBox ID="cbGovernment" runat="server" /></td>
                        </tr>
                        <tr>
                            <td>This client is an electric utility.</td>
                            <td>
                                <asp:CheckBox ID="cbElectric" runat="server" /></td>
                        </tr>
                        <tr>
                            <td>This client's legal fees will be paid by an insurance company <em>or</em> other third party.</td>
                            <td>
                                <asp:CheckBox ID="cbLegalFees" runat="server" /></td>
                        </tr>
                        <tr>
                            <td>This client <em>or</em> adversary is a lawyer or a law firm.</td>
                            <td>
                                <asp:CheckBox ID="cbLawFirm" runat="server" /></td>
                        </tr>
                        <tr>
                            <td>This client <em>or</em> adversary is a ski resort.</td>
                            <td>
                                <asp:CheckBox ID="cbSkiResort" runat="server" /></td>
                        </tr>
                        <tr>
                            <td>This client <em>or</em> adversary is an Indian tribe.</td>
                            <td>
                                <asp:CheckBox ID="cbIndianTribe" runat="server" /></td>
                        </tr>
                        <tr>
                            <td>This client <em>or</em> adversary is an any segment of the oil and gas industry.</td>
                            <td>
                                <asp:CheckBox ID="cbOilGas" runat="server" /></td>
                        </tr>
                        <tr>
                            <td>This adversary is an accounting firm.</td>
                            <td>
                                <asp:CheckBox ID="cbAccounting" runat="server" /></td>
                        </tr>
                        <tr>
                            <td>None of the above</td>
                            <td>
                                <asp:CheckBox ID="cbNoneOfTheAbove" runat="server" OnCheckedChanged="cbNoneOfTheAbove_OnCheckedChanged" AutoPostBack="true" CausesValidation="false" /></td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button ID="btnDummy" runat="server" CssClass="dummyControl" />
                <ajax:ModalPopupExtender ID="mdlCheckboxConfirm" runat="server" X="-20" Y="-50"
                    PopupControlID="pnlCheckboxConfirm" TargetControlID="btnDummy">
                </ajax:ModalPopupExtender>
                <asp:Panel ID="pnlCheckboxConfirm" runat="server" SkinID="HH_PanelPopup">
                    <table cellpadding="3" cellspacing="5" border="0">
                        <tr>
                            <td colspan="2">Are you sure that the client or adverse party does not fit any of the criteria listed above?
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnConfirm" runat="server" Text="Yes" OnClick="btnConfirm_Click" CausesValidation="false" /></td>
                            <td align="left">
                                <asp:Button ID="btnCancel" runat="server" Text="No" OnClick="btnCancel_Click" CausesValidation="false" /></td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="cbNoneOfTheAbove" EventName="CheckedChanged" />
                <asp:AsyncPostBackTrigger ControlID="btnConfirm" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLowerContent" runat="server">
</asp:Content>
