﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Requests/Requests.Master" AutoEventWireup="true" CodeBehind="Parties.aspx.cs" Inherits="Adversary.Web.Requests.Parties" Theme="HH" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMainContent" runat="server">
    <asp:Panel ID="pnlError" runat="server" Visible="false">
        <asp:Label ID="lblError" runat="server" CssClass="error">No Request is selected.</asp:Label>
    </asp:Panel>
    <asp:Panel ID="pnlRequest" runat="server">
        <table border="0" cellpadding="3" cellspacing="6">
            <tr>
                <td>Adverse Parties:
                </td>
                <td>
                    <asp:TextBox ID="tbAdverseParties" TextMode="MultiLine" runat="server" SkinID="HH_Textbox"
                        Rows="3" Columns="60" />
                </td>
            </tr>
            <tr>
                <td>Affiliates of Adverse Parties:
                </td>
                <td>
                    <asp:TextBox ID="tbAffiliatesOfAdverseParties" TextMode="MultiLine" runat="server" SkinID="HH_Textbox"
                        Rows="3" Columns="60" />
                </td>
            </tr>

            <tr>
                <td>Related Co-Defendants:
                </td>
                <td>
                    <asp:TextBox ID="tbRelatedCoDefendants" TextMode="MultiLine" runat="server" SkinID="HH_Textbox"
                        Rows="3" Columns="60" />
                </td>
            </tr>

            <tr>
                <td>Related Parties (not including co-defendants):
                </td>
                <td>
                    <asp:TextBox ID="tbRelatedPartiesNotCodefendants" TextMode="MultiLine" runat="server" SkinID="HH_Textbox"
                        Rows="3" Columns="60" />
                </td>
            </tr>

            <tr>
                <td>Other Client Affiliates:
                </td>
                <td>
                    <asp:TextBox ID="tbOtherClientAffiliates" TextMode="MultiLine" runat="server" SkinID="HH_Textbox"
                        Rows="3" Columns="60" />
                </td>
            </tr>

            <tr>
                <td>Parties to Mediation and Arbitration:
                </td>
                <td>
                    <asp:TextBox ID="tbPartiesToMediationArbitration" TextMode="MultiLine" runat="server" SkinID="HH_Textbox"
                        Rows="3" Columns="60" />
                </td>
            </tr>
            <tr>
                <td>Opposing Counsel:
                </td>
                <td>
                    <asp:TextBox ID="tbOpposingCounsel" TextMode="MultiLine" runat="server" SkinID="HH_Textbox"
                        Rows="3" Columns="60" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLowerContent" runat="server">
</asp:Content>
