﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Requests/Requests.Master" AutoEventWireup="true" CodeBehind="Submit.aspx.cs" Inherits="Adversary.Web.Requests.Submit" Theme="HH" %>

<%@ Register TagPrefix="uc" TagName="RequestSummary" Src="~/Controls/RequestSummary.ascx" %>
<%@ Register TagPrefix="uc" TagName="ErrorSummary" Src="~/Controls/ErrorSummary.ascx" %>
<%@ Register TagPrefix="ajax" Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" %>
<%@ Register TagPrefix="advcontrols" Assembly="Adversary.Controls" Namespace="Adversary.Controls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="server">
    <asp:UpdatePanel ID="upnlMessage" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <%--<asp:Label ID="lblMessage" runat="server" />--%>
            <uc:ErrorSummary ID="ucErrorSummary" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMainContent" runat="server">
    <asp:UpdatePanel ID="upnlSubmitRequest" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
        <ContentTemplate>
            <uc:RequestSummary ID="ucRequestSummary" runat="server" />
            <asp:Button ID="btnSubmitRequest" runat="server" Text="Submit Adversary Request"
                CausesValidation="false" OnClick="btnSubmitRequest_Click" />
            <asp:Panel ID="pnlAdminTools" runat="server">
                <table>
                    <tr>
                        <td>Assigned To:</td>
                        <td>
                            <advcontrols:AdversaryGroupDropdown ID="advAssignedTo" runat="server" SuppressAutoPostBack="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>Work Begun Date:</td>
                        <td>
                            <asp:Label ID="lblWorkBegunDate" runat="server" /></td>
                    </tr>
                    <tr>
                        <td>Work Completed By:</td>
                        <td>
                            <advcontrols:AdversaryGroupDropdown ID="advWorkCompletedBy" runat="server" SuppressAutoPostBack="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnSaveAssignments" runat="server" Text="Save Assignments" OnClick="btnAdmin_Click" CommandName="saveassignments" />
                            &nbsp;
                                <asp:Button ID="btnMarkComplete" runat="server" Text="Mark Complete" OnClick="btnClick_ShowPopup" CommandArgument="markcomplete" />
                            &nbsp;
                                <asp:Button ID="btnRemoveComplete" runat="server" Text="Remove Complete" OnClick="btnClick_ShowPopup" CommandArgument="removecomplete" />
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="pnlMarkCompletePopup" runat="server" SkinID="HH_PanelPopup_ConfirmBox">
                    <table width="100%">
                        <tr>
                            <td>
                                <strong>
                                    <asp:Label ID="lblMarkComplete" runat="server" />
                                </strong>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button ID="btnMarkCompletePopup" runat="server" Text="OK" OnClick="btnAdmin_Click" CommandName="markcomplete" CausesValidation="false" />
                                &nbsp;&nbsp;<asp:Button ID="btnCancelMarkCompletePopup" runat="server" Text="Cancel" CausesValidation="false"
                                    OnClick="btnCancelPopup_Click" CommandArgument="markcomplete" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajax:ModalPopupExtender ID="mdlMarkCompletePopup" runat="server" TargetControlID="btnDummy1" PopupControlID="pnlMarkCompletePopup" />
                <asp:Panel ID="pnlRemoveCompletePopup" runat="server" SkinID="HH_PanelPopup_ConfirmBox">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td><strong>This request has already been marked as complete. Are you sure that you want to mark as pending and re-open it?</strong>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="btnRemoveCompletePopup" runat="server" Text="OK" OnClick="btnAdmin_Click" CommandName="removecomplete" />
                                &nbsp;&nbsp;<asp:Button ID="btnCancelRemoveCompletePopup" runat="server" Text="Cancel" OnClick="btnCancelPopup_Click"
                                    CommandArgument="removecomplete" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <ajax:ModalPopupExtender ID="mdlRemoveCompletePopup" runat="server" TargetControlID="btnDummy2" PopupControlID="pnlRemoveCompletePopup" />
                <asp:Button ID="btnDummy1" runat="server" CssClass="dummyControl" />
                <asp:Button ID="btnDummy2" runat="server" CssClass="dummyControl" />
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSubmitRequest" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdateProgress runat="server" ID="uprgSubmitRequest" DisplayAfter="0" AssociatedUpdatePanelID="upnlSubmitRequest">
        <ProgressTemplate>
            <img src="../Images/ajax-loader.gif" />
            Sending...
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphLowerContent" runat="server">
</asp:Content>
