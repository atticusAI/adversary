﻿/* 
* utility functions 
* in the hh namespace
* requires 
*   jQuery 1.7
*   asp.net $find() 
*/
var hh = {

    toBoolean: function (b) {
        var r = false;
        if (typeof b == "boolean") {
            r = b;
        }
        else if (typeof b == "string" && b.toLowerCase() == "true") {
            r = true;
        }
        return r;
    },

    showMPX: function (behaviorID) {
        try {
            $find(behaviorID).show();
        }
        catch (error) {
        }
    },

    validateWithjQuery: function (form) {
        return $(form).validate({
            onfocusout: false,
            onkeyup: false,
            onclick: false,
            focusInvalid: false,
            errorContainer: '.errdiv',
            errorLabelContainer: '.errdiv',
            invalidHandler: function (form, validator) {
                setTimeout(function () { window.scrollTo(0, document.documentElement.scrollHeight); }, 100);
            }
        });
    },

    popWindow: function(url){
        var win = window.open(url, "pop");
    }
}