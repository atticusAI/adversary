﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.DataLayer
{
    public class Rules
    {
        public List<AllRule> GetAllRules(int memoTrackID)
        {
            List<AllRule> _AllRules = new List<AllRule>();
            using (AdversaryDataContext dc = new AdversaryDataContext(AdversaryConnection.AdversaryConnectionString()))
            {
                _AllRules = (from A in dc.AllRules
                             where A.TrackNumber == memoTrackID
                             select A).ToList();
            }
            return _AllRules;
        }
        public List<AllRule> GetAllRules(int memoTrackID, bool activeOnly)
        {
            List<AllRule> _allRules = GetAllRules(memoTrackID);
            if (activeOnly) _allRules = _allRules.Where(R => R.RuleActive).ToList();
            return _allRules;
        }
    }
}
