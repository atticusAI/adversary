﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Adversary.DataLayer.HRDataService;
using Adversary.DataLayer.FinancialDataService;
using Adversary.DataLayer.AdversaryDataService;

namespace Adversary.DataLayer
{
    public class ScreeningMemoList : IDisposable 
    {
        private PeopleDataContractClient _peopleDataContract = null;
        private PeopleDataContractClient PeopleDataContract
        {
            get
            {
                if (_peopleDataContract == null)
                {
                    _peopleDataContract = new PeopleDataContractClient();
                }
                return _peopleDataContract;
            }
        }

        private FinancialDataServiceContractClient _financialDataContract = null;
        private FinancialDataServiceContractClient FinancialDataContract
        {
            get
            {
                if (_financialDataContract == null)
                {
                    _financialDataContract = new FinancialDataServiceContractClient();
                }
                return _financialDataContract;
            }
        }

        private FirmDataContractClient _firmDataContract = null;
        private FirmDataContractClient FirmDataContract
        {
            get
            {
                if (_firmDataContract == null)
                {
                    _firmDataContract = new FirmDataContractClient();
                }
                return _firmDataContract;
            }
        }

        private AdversaryDataServiceContractClient _adversaryDataContract = null;
        private AdversaryDataServiceContractClient AdversaryDataContract
        {
            get
            {
                if (_adversaryDataContract == null)
                {
                    _adversaryDataContract = new AdversaryDataServiceContractClient();
                }
                return _adversaryDataContract;
            }
        }

        private TimekeeperLookupServiceContractClient _timeKeeperContract = null;
        private TimekeeperLookupServiceContractClient TimeKeeperContract
        {
            get
            {
                if (this._timeKeeperContract == null)
                {
                    this._timeKeeperContract = new TimekeeperLookupServiceContractClient();
                }
                return this._timeKeeperContract;
            }
        }

        private FirmOffice[] _firmOffices = null;
        private FirmOffice[] FirmOffices
        {
            get
            {
                if (_firmOffices == null)
                {
                    _firmOffices = FirmDataContract.GetFirmOffices();
                }
                return _firmOffices;
            }
        }

        private FirmPracticeGroup[] _practiceGroups = null;
        private FirmPracticeGroup[] PracticeGroups
        {
            get
            {
                if (_practiceGroups == null)
                {
                    _practiceGroups = FirmDataContract.GetFirmPracticeGroups();
                }
                return _practiceGroups;
            }
        }

        private PracticeCode[] _practiceCodes;
        private PracticeCode[] PracticeCodes
        {
            get
            {
                if (_practiceCodes == null)
                {
                    _practiceCodes = AdversaryDataContract.GetPracticeCodesList();
                }
                return _practiceCodes;
            }
        }

        /// <summary>
        /// Get the cooked name from the HHESB
        /// *will throw* if the name is not found of if something else bad happens 
        /// you must catch!
        /// </summary>
        /// <param name="personID">the person ID</param>
        /// <returns>FinancialDataEmployee.EmployeeName</returns>
        public string GetPersonName(string personID)
        {
            FinancialDataEmployee emp = TimeKeeperContract.TimekeeperLookup_GetEmployeeByLoginOrCode(personID);
            return emp.EmployeeName;
        }

        /// <summary>
        /// Get the name of a client from the HHESB
        /// *will throw* if the name is not found of if something else bad happens 
        /// you must catch!
        /// </summary>
        /// <param name="clientCode">the client code</param>
        /// <returns>the clientname field</returns>
        public string GetClientName(string clientCode)
        {
            return FinancialDataContract.GetClient(clientCode).ClientName;
        }

        /// <summary>
        /// get the city name given a location code
        /// *will throw* if sumthing bad happens, or if nothing is found
        /// you must catch!
        /// </summary>
        /// <param name="locationCode">the location xode</param>
        /// <returns>the city field</returns>
        public string GetOfficeName(string locationCode)
        {
            return FirmOffices.Where(o => o.LocationCode == locationCode).FirstOrDefault().City;
        }

        /// <summary>
        /// get the practice code name
        /// *will throw* if something goes worng or if the name is not found
        /// you must catch!
        /// </summary>
        /// <param name="sectionCode">the code</param>
        /// <returns>the practice code name (matt_cat_desc field)</returns>
        public string GetPracticeCodeName(string mattCatCode)
        {
            return PracticeCodes.Where(p => p.matt_cat_code == mattCatCode).FirstOrDefault().matt_cat_desc;
        }

        public void Dispose()
        {
            if (_peopleDataContract != null)
            {
                _peopleDataContract.Close();
                _peopleDataContract = null;
            }

            if (_financialDataContract != null)
            {
                _financialDataContract.Close();
                _financialDataContract = null;
            }

            if (_firmDataContract != null)
            {
                _firmDataContract.Close();
                _firmDataContract = null;
            }

            if (_adversaryDataContract != null)
            {
                _adversaryDataContract.Close();
                _adversaryDataContract = null;
            }

            if (this._timeKeeperContract != null)
            {
                this._timeKeeperContract.Close();
                this._timeKeeperContract = null;
            }
        }
    }
}
