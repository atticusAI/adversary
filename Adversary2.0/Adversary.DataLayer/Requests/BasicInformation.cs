﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Adversary.DataLayer;

namespace Adversary.DataLayer.Requests
{
    public class BasicInformation : RequestBaseClass, IRequestGet, IRequestSave
    {
        public BasicInformation(int requestID)
        {
            _RequestID = requestID;
        }

        private string _ClientMatterNumber;
        public string ClientMatterNumber
        {
            get { return _ClientMatterNumber; }
            set { _ClientMatterNumber = value; }
        }

        private string _ClientCategory;
        public string ClientCategory
        {
            get { return _ClientCategory; }
            set { _ClientCategory = value; }
        }

        private string _ClientName;
        public string ClientName
        {
            get { return _ClientName; }
            set { _ClientName = value; }
        }

        private string _SubmittedByPayrollID;
        public string SubmittedByPayrollID
        {
            get { return _SubmittedByPayrollID; }
            set { _SubmittedByPayrollID = value; }
        }

        private string _AttorneyID;
        public string AttorneyID
        {
            get { return _AttorneyID; }
            set { _AttorneyID = value; }
        }

        private string _PracticeCode;
        public string PracticeCode
        {
            get { return _PracticeCode; }
            set { _PracticeCode = value; }
        }

        private bool? _OKForNewMatter;
        public bool? OKForNewMatter
        {
            get { return _OKForNewMatter; }
            set { _OKForNewMatter = value; }
        }

        private int _RequestID;
        public int RequestID
        {
            get { return _RequestID; }
        }

        private DateTime? _SubmittedOn;
        public DateTime? SubmittedOn
        {
            get { return _SubmittedOn; }
            set { _SubmittedOn = value; }
        }

        private DateTime? _DeadlineDate;
        public DateTime? DeadlineDate
        {
            get { return _DeadlineDate; }
            set { _DeadlineDate = value; }
        }

        public bool Get()
        {
            if (RequestID == null)
                return false;
            else
            {
                Request R = (from r in AdversaryContext.Requests
                             where r.ReqID == RequestID
                             select r).FirstOrDefault();
                if (R != null)
                {
                    ClientCategory = R.ClientCategory;
                    ClientName = R.ClientName;
                    SubmittedByPayrollID = R.TypistLogin;
                    AttorneyID = R.AttyLogin;
                    PracticeCode = R.PracticeCode;
                    OKForNewMatter = R.OKforNewMatter.HasValue;
                    SubmittedOn = R.SubmittalDate.Value;
                    DeadlineDate = R.DeadlineDate.Value;
                    ClientName = R.ClientName;
                }
            }
            return true;

        }

        public bool Save()
        {
            Adversary.DataLayer.Request request;
            bool newRequest = false;
            if (RequestID == 0)
            {
                //new request
                request = new Request();
                newRequest = true;
            }
            else
            {
                request = (from R in base.AdversaryContext.Requests
                           where R.ReqID == RequestID
                           select R).FirstOrDefault();
            }

            if (request != null)
            {
                request.ClientMatter = ClientMatterNumber;
                request.ClientName = ClientName;
                request.ClientCategory = ClientCategory;
                request.AttyLogin = AttorneyID;
                request.TypistLogin = SubmittedByPayrollID;
                request.PracticeCode = PracticeCode;
                request.OKforNewMatter = OKForNewMatter;
                request.SubmittalDate = SubmittedOn;
                request.DeadlineDate = DeadlineDate;
                if (newRequest)
                    AdversaryContext.Requests.InsertOnSubmit(request);
                AdversaryContext.SubmitChanges();
            }
            else
            {
                return false;
            }
            return true;
        }

        public bool SubmitRequest()
        {
            if(this.SubmittedOn.HasValue)
                SubmitRequest(this.SubmittedOn.Value);
            else
            {
                throw new ArgumentOutOfRangeException("No submission date could be determined");
            }
            return true;
        }

        public bool SubmitRequest(DateTime submittedDate)
        {
            Adversary.DataLayer.Request request = (from R in AdversaryContext.Requests
                                                   where R.ReqID == RequestID
                                                   select R).FirstOrDefault();
            if (request != null)
            {
                request.SubmittalDate = submittedDate;
                AdversaryContext.SubmitChanges();
            }
            return true;
        }

        public bool BeginWorkOnRequest(string workBegunBy, DateTime workBegunOnDate)
        {
            Request R = (from r in AdversaryContext.Requests
                         where r.ReqID == RequestID
                         select r).FirstOrDefault();
            if (R != null)
            {
                R.WorkBegunBy = workBegunBy;
                R.WorkBegunDate = workBegunOnDate;
                AdversaryContext.SubmitChanges();
            }
            return true;
        }


    }
}
