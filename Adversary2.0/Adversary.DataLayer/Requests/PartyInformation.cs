﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.DataLayer.Requests
{
    public class PartyInformation : RequestBaseClass, IRequestGet, IRequestSave
    {

        public PartyInformation(int requestID)
        {
            _RequestID = requestID;
        }

        #region screen properties
        private string _AdverseParties;
        public string AdverseParties
        {
            get { return _AdverseParties; }
            set { _AdverseParties = value; }
        }

        private string _AffiliatesOfAdverseParties;
        public string AffiliatesOfAdverseParties
        {
            get { return _AffiliatesOfAdverseParties; }
            set { _AffiliatesOfAdverseParties = value; }
        }

        private string _RelatedCoDefendants;
        public string RelatedCoDefendants
        {
            get { return _RelatedCoDefendants; }
            set { _RelatedCoDefendants = value; }
        }

        private string _RelatedPartiesNotCoDefendants;
        public string RelatedPartiesNotCoDefendants
        {
            get { return _RelatedPartiesNotCoDefendants; }
            set { _RelatedPartiesNotCoDefendants = value; }
        }

        private string _OtherClientAffiliates;
        public string OtherClientAffiliates
        {
            get { return _OtherClientAffiliates; }
            set { _OtherClientAffiliates = value; }
        }

        private string _PartiesToMediationAndArbitration;
        public string PartiesToMediationAndArbitration
        {
            get { return _PartiesToMediationAndArbitration; }
            set { _PartiesToMediationAndArbitration = value; }
        }

        private string _OpposingCounsel;
        public string OpposingCounsel
        {
            get { return _OpposingCounsel; }
            set { _OpposingCounsel = value; }
        }
        #endregion

        public bool Get()
        {
            Request r = (from R in base.AdversaryContext.Requests
                         where R.ReqID == RequestID
                         select R).FirstOrDefault();

            AdverseParties = r.AdverseParties;
            AffiliatesOfAdverseParties = r.AffiliatesOfAdverseParties;
            RelatedCoDefendants = r.RelatedCoDefendants;
            RelatedPartiesNotCoDefendants = r.RelatedParties;
            OtherClientAffiliates = r.OtherClientAffiliates;
            PartiesToMediationAndArbitration = r.PartiesToMediationArbitration;
            OpposingCounsel = r.OpposingCounsel;
            return true;
        }

        private int _RequestID;
        public int RequestID
        {
            get { return _RequestID; }
        }

        public bool Save()
        {
            Request r = (from R in base.AdversaryContext.Requests
                         where R.ReqID == RequestID
                         select R).FirstOrDefault();
            if (r != null)
            {
                r.AdverseParties = AdverseParties;
                r.AffiliatesOfAdverseParties = AffiliatesOfAdverseParties;
                r.RelatedCoDefendants = RelatedCoDefendants;
                r.RelatedParties = RelatedPartiesNotCoDefendants;
                r.OtherClientAffiliates = OtherClientAffiliates;
                r.PartiesToMediationArbitration = PartiesToMediationAndArbitration;
                r.OpposingCounsel = OpposingCounsel;
                base.AdversaryContext.SubmitChanges();
            }
            else
            {
                return false;
            }

            return true;
        }
    }
}
