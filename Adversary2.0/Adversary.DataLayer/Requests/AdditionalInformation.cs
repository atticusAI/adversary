﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.DataLayer.Requests
{
    public class AdditionalInformation : RequestBaseClass, IRequestGet, IRequestSave
    {
        private bool _NoneOfTheAbove;
        public bool NoneOfTheAbove
        {
            get { return _NoneOfTheAbove; }
            set { _NoneOfTheAbove = value; }
        }

        private bool _IsInsurance;
        public bool IsInsurance
        {
            get { return _IsInsurance; }
            set { _IsInsurance = value; }
        }

        private bool _IsSkiResort;
        public bool IsSkiResort
        {
            get { return _IsSkiResort; }
            set { _IsSkiResort = value; }
        }

        private bool _IsLawFirm;
        public bool IsLawFirm
        {
            get { return _IsLawFirm; }
            set { _IsLawFirm = value; }
        }

        private bool _IsCommunications;
        public bool IsCommunications
        {
            get { return _IsCommunications; }
            set { _IsCommunications = value; }
        }

        private bool _IsGovernment;
        public bool IsGovernment
        {
            get { return _IsGovernment; }
            set { _IsGovernment = value; }
        }

        private bool _IsIndianTribe;
        public bool IsIndianTribe
        {
            get { return _IsIndianTribe; }
            set { _IsIndianTribe = value; }
        }

        private bool _IsPharm;
        public bool IsPharm
        {
            get { return _IsPharm; }
            set { _IsPharm = value; }
        }

        private bool _IsOilGas;
        public bool IsOilGas
        {
            get { return _IsOilGas; }
            set { _IsOilGas = value; }
        }

        private bool _IsElectricUtility;
        public bool IsElectricUtility
        {
            get { return _IsElectricUtility; }
            set { _IsElectricUtility = value; }
        }

        private bool _IsBankruptcy;
        public bool IsBankruptcy
        {
            get { return _IsBankruptcy; }
            set { _IsBankruptcy = value; }
        }

        private bool _IsAccounting;
        public bool IsAccounting
        {
            get { return _IsAccounting; }
            set { _IsAccounting = value; }
        }

        private bool _IsLegalFees;
        public bool IsLegalFees
        {
            get { return _IsLegalFees; }
            set { _IsLegalFees = value; }
        }



        public AdditionalInformation(int requestID)
        {
            _RequestID = requestID;
            Get();
        }

        public bool Save()
        {
            Request r = (from R in base.AdversaryContext.Requests
                         where R.ReqID == RequestID
                         select R).FirstOrDefault();
            if (r != null)
            {
                r.AccountingFirm = _IsAccounting;
                r.Bankruptcy = _IsBankruptcy;
                r.Communications = _IsCommunications;
                r.ElectricUtility = _IsElectricUtility;
                r.Government = _IsGovernment;
                r.IndianTribe = _IsIndianTribe;
                r.Insurance = _IsInsurance;
                r.LawFirm = _IsLawFirm;
                r.OilGas = _IsOilGas;
                r.Pharm = _IsPharm;
                r.SkiResort = _IsSkiResort;
                r.LegalFees = _IsLegalFees;
                r.NoneOfTheAbove = _NoneOfTheAbove;
                base.AdversaryContext.SubmitChanges();
            }
            else
            {
                return false;
            }
            return true;
        }

        private int _RequestID;
        public int RequestID
        {
            get { return _RequestID; }
        }

        public bool Get()
        {
            Request r = (from R in base.AdversaryContext.Requests
                         where R.ReqID == RequestID
                         select R).FirstOrDefault();
            _IsAccounting = (r.AccountingFirm.HasValue ? r.AccountingFirm.Value : false);
            _IsBankruptcy = (r.Bankruptcy.HasValue ? r.Bankruptcy.Value : false);
            _IsCommunications = (r.Communications.HasValue ? r.Communications.Value : false);
            _IsElectricUtility = (r.ElectricUtility.HasValue ? r.ElectricUtility.Value : false);
            _IsGovernment = (r.Government.HasValue ? r.Government.Value : false);
            _IsIndianTribe = (r.IndianTribe.HasValue ? r.IndianTribe.Value : false);
            _IsInsurance = (r.Insurance.HasValue ? r.Insurance.Value : false);
            _IsLawFirm = (r.LawFirm.HasValue ? r.LawFirm.Value : false);
            _IsOilGas = (r.OilGas.HasValue ? r.OilGas.Value : false);
            _IsPharm = (r.Pharm.HasValue ? r.Pharm.Value : false);
            _IsSkiResort = (r.SkiResort.HasValue ? r.SkiResort.Value : false);
            _IsLegalFees = (r.LegalFees.HasValue ? r.LegalFees.Value : false);
            _NoneOfTheAbove = (r.NoneOfTheAbove.HasValue ? r.NoneOfTheAbove.Value : false);
            return true;
        }
    }
}
