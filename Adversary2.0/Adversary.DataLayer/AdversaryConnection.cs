﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Adversary.DataLayer.ConnectionManagerService;

namespace Adversary.DataLayer
{
    internal static class AdversaryConnection
    {
        internal static string AdversaryConnectionString()
        {
            string connectionString = string.Empty;
            using (ConnectionManagerServiceClient cm = new ConnectionManagerServiceClient())
            {
                connectionString = cm.GetConnectionString("ADVERSARY", false);
                // connectionString = "Data Source=densql06.hollandhart.com;Initial Catalog=Adversary;User=Adversary;Password=AdvN3Bu5s;";
            }
            return connectionString;
        }

    }
}
