﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq.SqlClient;

namespace Adversary.DataLayer
{
    public class ScreeningSearch : IDisposable
    {
        private AdversaryDataContext _dataContext = null;

        private AdversaryDataContext DataContext
        {
            get
            {
                if (_dataContext == null)
                {
                    _dataContext = new AdversaryDataContext(AdversaryConnection.AdversaryConnectionString());
                }
                return _dataContext;
            }
        }

        public void Dispose()
        {
            if (_dataContext != null)
            {
                _dataContext.Dispose();
                _dataContext = null;
            }
        }

        public List<Memo> SearchScreeningMemos(string client, string matter, string clientMatterNumber, string attorney, string employee, string screeningMemoNumber, string sortExpression)
        {
            List<Memo> memos = null;

            if (string.IsNullOrWhiteSpace(sortExpression))
            {
                sortExpression = "ScrMemID DESC";
            }

            if (!string.IsNullOrWhiteSpace(client))
            {
                memos = this.SearchScreeningMemosByClient(client);
            }
            else if (!string.IsNullOrWhiteSpace(matter))
            {
                memos = this.SearchScreeningMemosByMatter(matter);
            }
            else if (!string.IsNullOrWhiteSpace(clientMatterNumber))
            {
                memos = this.SearchScreeningMemosByClientMatterNumber(clientMatterNumber);
            }
            else if (!string.IsNullOrWhiteSpace(attorney))
            {
                memos = this.SearchScreeningMemosByAttorney(attorney);
            }
            else if (!string.IsNullOrWhiteSpace(employee))
            {
                memos = this.SearchScreeningMemosByEmployee(employee);
            }
            else if (!string.IsNullOrWhiteSpace(screeningMemoNumber))
            {
                memos = this.SearchScreeningMemosByScreeningMemoNumber(screeningMemoNumber);
            }

            return memos == null ? null : memos.HH_Sort(sortExpression).ToList();
        }

        public List<Memo> SearchScreeningMemosByScreeningMemoNumber(string screeningMemoNumber)
        {
            return (from m in DataContext.Memos
                    where SqlMethods.Like(Convert.ToString(m.ScrMemID), screeningMemoNumber.HH_AsSqlSearchParm())
                    select m).ToList();
        }

        public List<Memo> SearchScreeningMemosByScreeningMemoNumberStartsWith(string screeningMemoNumber)
        {
            return (from m in DataContext.Memos
                    where SqlMethods.Like(Convert.ToString(m.ScrMemID), screeningMemoNumber.HH_AsSqlSearchParamStartsWith())
                    select m).ToList();
        }

        public List<Memo> SearchScreeningMemosByEmployee(string employee)
        {
            return (from m in DataContext.Memos
                    where SqlMethods.Like(m.PersonnelID, employee.HH_AsSqlSearchParm())
                        || SqlMethods.Like(m.AttEntering, employee.HH_AsSqlSearchParm())
                    select m).ToList();
        }

        public List<Memo> SearchScreeningMemosByAttorney(string attorney)
        {
            return (from m in DataContext.Memos
                    where SqlMethods.Like(m.AttEntering, attorney.HH_AsSqlSearchParm())
                        || SqlMethods.Like(m.PersonnelID, attorney.HH_AsSqlSearchParm())
                    select m).ToList();
        }

        public List<Memo> SearchScreeningMemosByClientMatterNumber(string clientMatterNumber)
        {
            return (from m in DataContext.Memos
                    join cm in DataContext.ClientMatterNumbers on m.ScrMemID equals cm.ScrMemID
                    where SqlMethods.Like(cm.ClientMatterNumber1, clientMatterNumber.HH_AsSqlSearchParm())
                    select m).ToList();
        }

        public List<Memo> SearchScreeningMemosByClientMatterNumberStartsWith(string clientMatterNumber)
        {
            return (from m in DataContext.Memos
                    join cm in DataContext.ClientMatterNumbers on m.ScrMemID equals cm.ScrMemID
                    where SqlMethods.Like(cm.ClientMatterNumber1, clientMatterNumber.HH_AsSqlSearchParamStartsWith())
                    select m).ToList();
        }

        public List<Memo> SearchScreeningMemosByMatter(string matter)
        {
            return (from m in DataContext.Memos
                    where SqlMethods.Like(m.MatterName, matter.HH_AsSqlSearchParm())
                    select m).ToList();
        }

        public List<Memo> SearchScreeningMemosByMatterStartsWith(string matter)
        {
            return (from m in DataContext.Memos
                    where SqlMethods.Like(m.MatterName, matter.HH_AsSqlSearchParamStartsWith())
                    select m).ToList();
        }

        public List<Memo> SearchScreeningMemosByClient(string client)
        {
            return (from m in DataContext.Memos
                    where SqlMethods.Like(m.ClientNumber, client.HH_AsSqlSearchParm())
                    select m).ToList();
        }
    }
}
