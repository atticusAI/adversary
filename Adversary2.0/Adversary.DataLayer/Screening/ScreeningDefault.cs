﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace Adversary.DataLayer
{
    public class ScreeningDefault : IDisposable
    {
        private AdversaryDataContext __dataContext = null;

        private AdversaryDataContext DataContext
        {
            get
            {
                if (__dataContext == null)
                {
                    __dataContext = new AdversaryDataContext(AdversaryConnection.AdversaryConnectionString());
                }
                return __dataContext;
            }
        }

        public List<Memo> GetMyScreeningMemos(string personnelID)
        {
            List<Memo> myMemos = null;
            
            myMemos = (from m in DataContext.Memos
                        where m.PersonnelID == personnelID || m.RespAttID == personnelID
                        select m).ToList();

            return myMemos;
        }

        public List<Memo> GetMyScreeningMemos(string personnelID, string sortExpression)
        {
            List<Memo> myMemos = this.GetMyScreeningMemos(personnelID);

            if (string.IsNullOrWhiteSpace(sortExpression))
            {
                sortExpression = "ScrMemID DESC";
            }

            return myMemos.HH_Sort(sortExpression).ToList();
        }

        public void Dispose()
        {
            if (__dataContext != null)
            {
                __dataContext.Dispose();
                __dataContext = null;
            }
        }
    }
}
