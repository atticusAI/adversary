﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.DataLayer
{
    public class ProBonoReasonScreen : IScreeningMemoGet, IScreeningMemoSave, IDisposable
    {
        private string _ProBonoReason;

        public string ProBonoReason
        {
            get { return _ProBonoReason; }
            set { _ProBonoReason = value; }
        }


        private AdversaryDataContext __adversaryContext = null;
        private AdversaryDataContext AdversaryContext
        {
            get
            {
                if (__adversaryContext == null)
                    __adversaryContext = new AdversaryDataContext(AdversaryConnection.AdversaryConnectionString());
                return __adversaryContext;

            }
        }

        public ProBonoReasonScreen(int screeningMemoID)
        {
            this._ScreeningMemoID = screeningMemoID;
        }
        public void Dispose()
        {
            if (__adversaryContext != null)
                __adversaryContext.Dispose();
            __adversaryContext = null;
        }

        public bool Save()
        {
            Memo m = (from M in AdversaryContext.Memos
                      where M.ScrMemID == _ScreeningMemoID
                      select M).FirstOrDefault();
            if (m != null)
            {
                m.ProBonoReason = _ProBonoReason;
                AdversaryContext.SubmitChanges();
            }
            else
            {
                return false;
            }
            return true;
        }

        private int _ScreeningMemoID;
        public int ScreeningMemoID
        {
            get { throw new NotImplementedException(); }
        }

        public bool Get()
        {
            _ProBonoReason = (from M in AdversaryContext.Memos
                              where M.ScrMemID == _ScreeningMemoID
                              select M).FirstOrDefault().ProBonoReason;
            return true;
        }
    }
}
