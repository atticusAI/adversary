﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Adversary.Utils;
using Adversary.Utils.Interfaces;

namespace Adversary.DataLayer
{
    public class AdverseParty : PartyBase,
        IScreeningMemoGet,
        IScreeningMemoSave,
        IScreeningMemoDelete,
        IDisposable
    {
        private AdversaryDataContext __adversaryContext = null;
        private AdversaryDataContext AdversaryContext
        {
            get
            {
                if (__adversaryContext == null)
                    __adversaryContext = new AdversaryDataContext(AdversaryConnection.AdversaryConnectionString());
                return __adversaryContext;

            }
        }

        public AdverseParty(int screeningMemoID)
        {
            base.ScrMemID = screeningMemoID;
            _ScreeningMemoID = screeningMemoID;
        }
        public bool Get()
        {
            int _partyID = this.PartyID;
            return false;
        }

        public List<Party> GetPartyList()
        {
            List<Party> _ReturnList = new List<Party>();
            string[] _partyCodes = new string[] { };
            _ReturnList = (from C in AdversaryContext.Parties
                               //where Adversary.Utils.StringUtils.AdversePartyCodes.Contains(C.PartyRelationshipCode)
                               where Adversary.DataLayer.AdversaryData.AdversePartyCodes.Contains(C.PartyRelationshipCode)
                               && C.ScrMemID == _ScreeningMemoID
                               select C).ToList();
            
            return _ReturnList;
        }

        public Party GetParty(int partyID)
        {
            Party thisParty = new Party();
            
            thisParty = (from pty in AdversaryContext.Parties
                        where pty.PartyID == partyID
                        select pty).FirstOrDefault();
            
            return thisParty;
        }


        private int _ScreeningMemoID;
        public int ScreeningMemoID
        {
            get { return _ScreeningMemoID; }
        }

        public bool Delete()
        {   
            Party _relatedParty = (from P in AdversaryContext.Parties
                                    where P.PartyID == this.PartyID
                                    select P).First();
            AdversaryContext.Parties.DeleteOnSubmit(_relatedParty);
            AdversaryContext.SubmitChanges();
            
            return true;
        }

        public void Dispose()
        {
            if (__adversaryContext != null)
                __adversaryContext.Dispose();
            __adversaryContext = null;
        }
    }
}
