﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.DataLayer
{
    public class ScreeningMemoSummary: IDisposable
    {
        private AdversaryDataContext __dataContext = null;

        private AdversaryDataContext DataContext
        {
            get
            {
                if (__dataContext == null)
                {
                    __dataContext = new AdversaryDataContext(AdversaryConnection.AdversaryConnectionString());
                }
                return __dataContext;
            }
        }

        public List<ScreeningMemoSummarySection> GetScrMemSummarySection(int scrMemType)
        {
            return (from smss in DataContext.ScreeningMemoSummarySections
                    join smtss in DataContext.ScreeningMemoTypeSummarySections
                        on smss.ScrMemSummarySectionID equals smtss.ScrMemSummarySectionID
                    where smtss.TrackNumber == scrMemType
                    orderby smtss.SectionOrder
                    select smss).ToList();
        }

        public void Dispose()
        {
            if (__dataContext != null)
            {
                __dataContext.Dispose();
                __dataContext = null;
            }
        }
    }
}
