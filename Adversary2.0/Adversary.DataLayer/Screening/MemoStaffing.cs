﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.DataLayer
{
    public class MemoStaffing : IScreeningMemoGet, IScreeningMemoSave
    {
        private string _BillingAttorney;
        //memo.RespAttID
        public string BillingAttorney
        {
            get { return _BillingAttorney; }
            set { _BillingAttorney = value; }
        }

        private List<Staffing> _StaffingList;

        public List<Staffing> StaffingList
        {
            get
            {
                if (_StaffingList == null)
                {
                    GetStaffingList();
                }
                return _StaffingList;
            }
            set { _StaffingList = value; }
        }

        public MemoStaffing(int screeningMemoID)
        {
            _ScreeningMemoID = screeningMemoID;
        }

        public bool Get()
        {
            //GetStaffingList();
            using (AdversaryDataContext dc = new AdversaryDataContext(AdversaryConnection.AdversaryConnectionString()))
            {
                Memo memo = (from M in dc.Memos
                          where M.ScrMemID == ScreeningMemoID
                          select M).FirstOrDefault();
                _BillingAttorney = memo.RespAttID;
            }
            return true;
        }

        public List<Staffing> GetStaffingList()
        {
            using (AdversaryDataContext dc = new AdversaryDataContext(AdversaryConnection.AdversaryConnectionString()))
            {
                _StaffingList = (from S in dc.Staffings
                                 where S.ScrMemID == ScreeningMemoID
                                 select S).ToList();
                //if (_StaffingList.Count > 0)
                //{
                //    foreach (Staffing _staff in _StaffingList)
                //    {

                //    }
                //}
            }
            return _StaffingList;
        }

        public bool AddStaffMember(string personnelID)
        {
            using (AdversaryDataContext dc = new AdversaryDataContext(AdversaryConnection.AdversaryConnectionString()))
            {
                Staffing newStaff = new Staffing() { ScrMemID = _ScreeningMemoID, PersonnelID = personnelID };
                dc.Staffings.InsertOnSubmit(newStaff);
                dc.SubmitChanges();
            }
            return true;
        }

        public bool DeleteStaffMember(int staffingID)
        {
            using (AdversaryDataContext dc = new AdversaryDataContext(AdversaryConnection.AdversaryConnectionString()))
            {
                Staffing _deletedMember = (from S in dc.Staffings
                                           where S.StaffingID == staffingID
                                           select S).FirstOrDefault();
                if (_deletedMember != null)
                {
                    dc.Staffings.DeleteOnSubmit(_deletedMember);
                    dc.SubmitChanges();
                }
            }
            return true;
        }

        private int _ScreeningMemoID;
        public int ScreeningMemoID
        {
            get { return _ScreeningMemoID; }
        }

        public bool Save()
        {
            using (AdversaryDataContext dc = new AdversaryDataContext(AdversaryConnection.AdversaryConnectionString()))
            {
                Memo m = (from M in dc.Memos
                          where M.ScrMemID == _ScreeningMemoID
                          select M).FirstOrDefault();
                if (m != null)
                {
                    m.RespAttID = _BillingAttorney;
                    dc.SubmitChanges();
                }
                else
                {
                    return false;
                }
            }
            return true;
        }
    }
}
