﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;

namespace Adversary.DataLayer
{

    public class Docketing : IScreeningMemoSave, IScreeningMemoDelete, IDisposable
    {
        private AdversaryDataContext __adversaryContext = null;
        private AdversaryDataContext AdversaryContext
        {
            get
            {
                if (__adversaryContext == null)
                    __adversaryContext = new AdversaryDataContext(AdversaryConnection.AdversaryConnectionString());
                return __adversaryContext;

            }
        }

        public Docketing(int screeningMemoID)
        {
            this._ScreeningMemoID = screeningMemoID;
        }

        private string _DocketingAttorney;

        public string DocketingAttorney
        {
            get { return _DocketingAttorney; }
            set { _DocketingAttorney = value; }
        }

        private string _DocketingStaff;

        public string DocketingStaff
        {
            get { return _DocketingStaff; }
            set { _DocketingStaff = value; }
        }

        public List<DocketingTeam> GetDocketingTeam()
        {
            List<DocketingTeam> _Team = new List<DocketingTeam>();
            try
            {
                _Team = (from T in AdversaryContext.DocketingTeams
                            where T.ScrMemID == _ScreeningMemoID
                            select T).ToList<DocketingTeam>();   
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _Team;
        }

        private List<DocketingTeam> _DocketingTeam;
        public List<DocketingTeam> DocketingTeam
        {
            get
            {
                if (_DocketingTeam == null)
                {
                    _DocketingTeam = GetDocketingTeam();
                }
                return _DocketingTeam;

            }
            set { _DocketingTeam = value; }
        }

        public void RefreshDocketingTeam()
        {
            this._DocketingTeam = GetDocketingTeam();
        }

        public bool Save()
        {
            ////find the deleted entry
            //List<Adversary.DataLayer.DocketingTeam> _newTeam = this.DocketingTeam.ToList();
            //RefreshDocketingTeam();
            //Adversary.DataLayer.DocketingTeam _deletedMember;
            //Adversary.DataLayer.DocketingTeam _addedMember;
            //_deletedMember = (from D in _DocketingTeam where !(from N in _newTeam select N.PersonnelID).Contains(D.PersonnelID) select D).FirstOrDefault();
            //_addedMember = (from D in _newTeam where !(from N in _DocketingTeam select N.PersonnelID).Contains(D.PersonnelID) select D).FirstOrDefault();
            ////delete the current docketing team and replace them
            using (AdversaryDataContext data = new AdversaryDataContext(AdversaryConnection.AdversaryConnectionString()))
            {
                //if (_deletedMember != null)
                //{
                //    _deletedMember = (from D in data.DocketingTeams
                //                      where (D.PersonnelID == _deletedMember.PersonnelID &&
                //                          D.ScrMemID == _ScreeningMemoID)
                //                      select D).FirstOrDefault();
                //    data.DocketingTeams.DeleteOnSubmit(_deletedMember);
                //}
                //else if (_addedMember != null)
                //{
                //    data.DocketingTeams.InsertOnSubmit(_addedMember);
                //}

                Adversary.DataLayer.Memo _Memo =
                    (from M in data.Memos
                     where M.ScrMemID == ScreeningMemoID
                     select M).FirstOrDefault();

                if (_Memo != null)
                {
                    _Memo.DocketingAtty = _DocketingAttorney;
                    _Memo.DocketingStaff = _DocketingStaff;
                    data.SubmitChanges();
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        private int _ScreeningMemoID;
        public int ScreeningMemoID
        {
            get { return _ScreeningMemoID; }
        }

        public void Dispose()
        {
            if (__adversaryContext != null)
                __adversaryContext.Dispose();
            __adversaryContext = null;
        }

        public bool Delete()
        {
            throw new NotImplementedException();
        }

        public bool AddDocketingTeamMember(string personnelID)
        {
            DocketingTeam _Exist = 
                (from DT in AdversaryContext.DocketingTeams
                 where DT.ScrMemID == _ScreeningMemoID &&
                 DT.PersonnelID == personnelID select DT).FirstOrDefault();

            if (_Exist == null)
            {
                DocketingTeam D = new DocketingTeam();
                D.PersonnelID = personnelID;
                D.ScrMemID = _ScreeningMemoID;
                AdversaryContext.DocketingTeams.InsertOnSubmit(D);
                AdversaryContext.SubmitChanges();
                return true;
            }
            else return false;
        }

        public bool DeleteDocketingTeamMember(int docketingTeamID)
        {
            DocketingTeam _Member =
                (from DT in AdversaryContext.DocketingTeams
                 where DT.DocketingTeamID == docketingTeamID
                 select DT).FirstOrDefault();
            if (_Member != null)
            {
                AdversaryContext.DocketingTeams.DeleteOnSubmit(_Member);
                AdversaryContext.SubmitChanges();
                return true;
            }
            else return false;
        }

    }
}
