﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.DataLayer
{
    public class ContactScreen : Adversary.DataLayer.ClientContact, IScreeningMemoGet, IScreeningMemoSave, IScreeningMemoDelete, IDisposable
    {
        private AdversaryDataContext __adversaryContext = null;
        private AdversaryDataContext AdversaryContext
        {
            get
            {
                if (__adversaryContext == null)
                    __adversaryContext = new AdversaryDataContext(AdversaryConnection.AdversaryConnectionString());
                return __adversaryContext;

            }
        }


        public ContactScreen(int screeningMemoID)
        {
            this.ScreeningMemoID = screeningMemoID;
        }
        public int ScreeningMemoID
        {
            get;
            private set;
        }

        public bool Save()
        {
            ClientContact _clientContact;
            bool isNewClientContact = true;
            if (this.ContactID > 0) isNewClientContact = false;

            using (AdversaryDataContext data = new AdversaryDataContext(AdversaryConnection.AdversaryConnectionString()))
            {
                if (!isNewClientContact)
                {
                    _clientContact = (from C in data.ClientContacts
                                      where (C.ContactID == this.ContactID)
                                      select C).FirstOrDefault();
                }
                else
                {
                    _clientContact = new ClientContact();
                }

                if (_clientContact != null)
                {
                    _clientContact.ContactFName = this.ContactFName;
                    _clientContact.ContactMName = this.ContactMName;
                    _clientContact.ContactLName = this.ContactLName;
                    _clientContact.ContactTitle = this.ContactTitle;
                    _clientContact.ScrMemID = this.ScreeningMemoID;

                    if (isNewClientContact)
                        data.ClientContacts.InsertOnSubmit(_clientContact);

                    data.SubmitChanges();
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        public bool Get()
        {
            ClientContact clientContact = new ClientContact();
            
            clientContact = (from C in AdversaryContext.ClientContacts
                                where C.ContactID == this.ContactID
                                select C).FirstOrDefault();
            this.ContactID = clientContact.ContactID;
            this.ContactFName = clientContact.ContactFName;
            this.ContactMName = clientContact.ContactMName;
            this.ContactLName = clientContact.ContactLName;
            this.ContactTitle = clientContact.ContactTitle;
            this.ScrMemID = clientContact.ScrMemID;
            this.ScreeningMemoID = Convert.ToInt32(clientContact.ScrMemID);
            
            return true;
        }

        public List<ClientContact> GetContactList()
        {
            List<ClientContact> _Contacts = new List<ClientContact>();
            try
            {
                _Contacts = (from C in AdversaryContext.ClientContacts
                             where C.ScrMemID == ScreeningMemoID
                             select C).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _Contacts;
        }

        public bool Delete()
        {
            ClientContact _deletedContact;
            
            _deletedContact = (from C in AdversaryContext.ClientContacts
                                where C.ContactID == this.ContactID
                                select C).FirstOrDefault();
            AdversaryContext.ClientContacts.DeleteOnSubmit(_deletedContact);
            AdversaryContext.SubmitChanges();
            
            return true;
        }

        public void Dispose()
        {
            if (__adversaryContext != null)
                __adversaryContext.Dispose();
            __adversaryContext = null;
        }
    }
}
