﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.DataLayer
{
    interface IScreeningMemo
    {
        int ScreeningMemoID { get; }
    }
    interface IScreeningMemoGet : IScreeningMemo
    {
        bool Get();
    }
    interface IScreeningMemoSave : IScreeningMemo
    {
        bool Save();
    }
    interface IScreeningMemoDelete : IScreeningMemo
    {
        bool Delete();
    }
}
