﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.DataLayer.Screening
{
    public class OpposingCounselScreen : DataLayer.OpposingCounsel, IScreeningMemoGet, IScreeningMemoSave, IScreeningMemoDelete, IDisposable
    {
        private AdversaryDataContext __adversaryContext = null;
        private AdversaryDataContext AdversaryContext
        {
            get
            {
                if (__adversaryContext == null)
                    __adversaryContext = new AdversaryDataContext(AdversaryConnection.AdversaryConnectionString());
                return __adversaryContext;

            }
        }

        public OpposingCounselScreen(int screeningMemoID)
        {
            _ScreeningMemoID = screeningMemoID;
        }

        public bool Delete()
        {
            OpposingCounsel _opposingCounsel;
            _opposingCounsel = (from O in AdversaryContext.OpposingCounsels
                                where O.opposingID == this.opposingID
                                select O).FirstOrDefault();
            AdversaryContext.OpposingCounsels.DeleteOnSubmit(_opposingCounsel);
            AdversaryContext.SubmitChanges();
            return true;

        }

        private int _ScreeningMemoID;
        public int ScreeningMemoID
        {
            get { return _ScreeningMemoID; }
        }

        public bool Get()
        {
            OpposingCounsel o = AdversaryContext.OpposingCounsels.Where(oc => oc.opposingID.Equals(this.opposingID)).FirstOrDefault();
            Memo m = AdversaryContext.Memos.Where(memo => memo.ScrMemID.Equals(this.ScreeningMemoID)).FirstOrDefault();

            if (o != null)
            {
                this.LawfirmName = o.LawfirmName;
                this.LawyerName = o.LawyerName;
                this.opposingID = o.opposingID;
                this.ScrMemID = o.ScrMemID;
                if (m != null)
                {
                    if (m.OppQuestion.HasValue) m.OppQuestion = m.OppQuestion.Value;
                }

                return true;
            }
            else return false;
        }

        public bool Save()
        {
            OpposingCounsel opposingCounsel;
            bool _isNewCounsel = true;
            if (this.opposingID > 0) _isNewCounsel = false;

            using (AdversaryDataContext data = new AdversaryDataContext(AdversaryConnection.AdversaryConnectionString()))
            {
                if (!_isNewCounsel)
                {
                    opposingCounsel = (from C in data.OpposingCounsels
                                       where (C.opposingID == this.opposingID)
                                       select C).FirstOrDefault();
                }
                else
                {
                    opposingCounsel = new OpposingCounsel();
                }

                if (opposingCounsel != null)
                {
                    opposingCounsel.LawyerName = this.LawyerName;
                    opposingCounsel.LawfirmName = this.LawfirmName;
                    opposingCounsel.ScrMemID = this.ScreeningMemoID;

                    if (_isNewCounsel)
                        data.OpposingCounsels.InsertOnSubmit(opposingCounsel);

                    data.SubmitChanges();
                }
                else
                {
                    return false;
                }
            }
            return true;
        }


        private bool _HasOpposingCounsel;

        public bool HasOpposingCounsel
        {
            get { return _HasOpposingCounsel; }
            set { _HasOpposingCounsel = value; }
        }

        public bool SaveMemoInformationOnly()
        {
            using (AdversaryDataContext adv = new AdversaryDataContext(AdversaryConnection.AdversaryConnectionString()))
            {
                Memo m = (from M in adv.Memos where M.ScrMemID == this.ScreeningMemoID select M).FirstOrDefault();
                m.OppQuestion = this.HasOpposingCounsel;
                adv.SubmitChanges();
            }
            return true;
        }


        public List<DataLayer.OpposingCounsel> GetOpposingCounsel()
        {
            List<DataLayer.OpposingCounsel> _list = new List<DataLayer.OpposingCounsel>();
            _list = AdversaryContext.OpposingCounsels.Where(o => o.ScrMemID.Equals(_ScreeningMemoID)).ToList();
            return _list;
        }

        public void Dispose()
        {
            if (__adversaryContext != null)
                __adversaryContext.Dispose();
            __adversaryContext = null;
        }
    }
}
