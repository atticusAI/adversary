using System.Collections.Generic;
using System.Linq;

namespace Adversary.DataLayer
{
    public partial class TrackingStatusType
    {
        private static List<TrackingStatusType> _trackingStatusTypes = null;

        public static List<TrackingStatusType> TrackingStatusTypes
        {
            get
            {
                if (_trackingStatusTypes == null)
                {
                    _trackingStatusTypes = GetTrackingStatusTypes();
                }
                return _trackingStatusTypes;
            }
        }

        private static List<TrackingStatusType> GetTrackingStatusTypes()
        {
            List<TrackingStatusType> lst = null;

            using (AdversaryDataContext dc = new AdversaryDataContext(AdversaryConnection.AdversaryConnectionString()))
            {
                var qry = from tst in dc.TrackingStatusTypes 
                          select tst;

                lst = qry.ToList();
            }

            return lst;
        }

        private static Dictionary<string, int> _trackingStatusTypesDictionary = null;

        public static Dictionary<string, int> TrackingStatusTypesDictionary
        {
            get
            {
                if (_trackingStatusTypesDictionary == null)
                {
                    _trackingStatusTypesDictionary = new Dictionary<string, int>();
                    foreach (TrackingStatusType tst in TrackingStatusTypes)
                    {
                        _trackingStatusTypesDictionary.Add(tst.Status, tst.StatusTypeID);
                    }
                }
                return _trackingStatusTypesDictionary;
            }
        }

        public static int? NA
        {
            get
            {
                return TrackingStatusTypesDictionary["N/A"];
            }
        }

        public static int? AdversaryGroupProcessing
        {
            get
            {
                return TrackingStatusTypesDictionary["Adversary Group Processing"];
            }
        }

        public static int? SubmittedForApproval
        {
            get
            {
                return TrackingStatusTypesDictionary["Submitted for Approval"];
            }
        }

        public static int? ModificationRequested
        {
            get
            {
                return TrackingStatusTypesDictionary["Modification Requested"];
            }
        }

        public static int? Completed
        {
            get
            {
                return TrackingStatusTypesDictionary["Completed"];
            }
        }

        public static int? Rejected
        {
            get
            {
                return TrackingStatusTypesDictionary["Rejected"];
            }
        }
    }
}
