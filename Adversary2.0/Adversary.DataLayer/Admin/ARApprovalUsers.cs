﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.DataLayer
{
    public class ARApprovalUsers
    {
        public ARApprovalUser Add(ARApprovalUser user)
        {
            using (AdversaryDataContext adc = new AdversaryDataContext(AdversaryConnection.AdversaryConnectionString()))
            {
                adc.ARApprovalUsers.InsertOnSubmit(user);
                adc.SubmitChanges();
                return user;
            }
        }

        public void Delete(int approverID)
        {
            using (AdversaryDataContext adc = new AdversaryDataContext(AdversaryConnection.AdversaryConnectionString()))
            {
                ARApprovalUser deletedAR = (from A in adc.ARApprovalUsers
                                            where A.ApprovalUserID == approverID
                                            select A).First();
                adc.ARApprovalUsers.DeleteOnSubmit(deletedAR);
                adc.SubmitChanges();
                return;
            }
        }
    }
}
