﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.DataLayer
{
    public class TrackingData : IScreeningMemoSave, IScreeningMemoGet, IDisposable
    {
        public TrackingData(Memo memo)
        {
            _ScreeningMemoID = memo.ScrMemID;

        }
        public TrackingData(int scrMemID)
        {
            _ScreeningMemoID = scrMemID;
        }

        private AdversaryDataContext __adversaryContext = null;
        private AdversaryDataContext AdversaryContext
        {
            get
            {
                if (__adversaryContext == null)
                    __adversaryContext = new AdversaryDataContext(AdversaryConnection.AdversaryConnectionString());
                return __adversaryContext;

            }
        }

        private Adversary.DataLayer.AdversaryData advData;

        private DataLayer.Tracking _Tracking;
        public Tracking Tracking
        {
            get { return _Tracking; }
        }

        private int _TrackingID;

        public int TrackingID
        {
            get { return _TrackingID; }
            set { _TrackingID = value; }
        }

        private DateTime _SubmittedOn;

        public DateTime SubmittedOn
        {
            get { return _SubmittedOn; }
            set { _SubmittedOn = value; }
        }

        private int _StatusTypeID;

        public int StatusTypeID
        {
            get { return _StatusTypeID; }
            set { _StatusTypeID = value; }
        }

        private List<string> _RejectionReasons;

        public List<string> RejectionReasons
        {
            get
            {
                _RejectionReasons = new List<string>();
                if (this.Tracking != null)
                {
                    if(this.Tracking.APRejectionReason != null)
                        _RejectionReasons.Add(this.Tracking.APRejectionReason);
                    if(this.Tracking.PGMRejectionReason != null)
                        _RejectionReasons.Add(this.Tracking.PGMRejectionReason);
                    if(this.Tracking.AdversaryRejectionReason != null)
                        _RejectionReasons.Add(this.Tracking.AdversaryRejectionReason);
                }
                return _RejectionReasons;
            }
            
        }


        public bool Save()
        {
            ///TODO: write save logic here to generate the Tracking object
            ///and then put it in the _Tracking field
            
            try
            {
                bool isNew = false;
                Tracking __localTracking = (from T in AdversaryContext.Trackings
                                            where T.TrackingID == _TrackingID
                                            select T).FirstOrDefault();
                if (__localTracking == null)
                {
                    isNew = true;
                    __localTracking = new Tracking();
                }

                __localTracking.ScrMemID = _ScreeningMemoID;
                __localTracking.SubmittedOn = _SubmittedOn;
                __localTracking.StatusTypeID = _StatusTypeID;

                if (isNew)
                {
                    AdversaryContext.Trackings.InsertOnSubmit(__localTracking);
                }
                AdversaryContext.SubmitChanges();
                this._TrackingID = __localTracking.TrackingID;
                this._Tracking = __localTracking;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            return true;
        }

        private int _ScreeningMemoID;
        public int ScreeningMemoID
        {
            get { return _ScreeningMemoID; }
        }

        public bool Get()
        {
            try
            {
                _Tracking = (from T in AdversaryContext.Trackings
                             where (T.TrackingID == _TrackingID) ||
                             (T.ScrMemID == _ScreeningMemoID)
                             select T).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            if (_Tracking == null)
            {
                _Tracking = new DataLayer.Tracking();
                _Tracking.TrackingID = 0; //set up
                _Tracking.ScrMemID = _ScreeningMemoID;
                _Tracking.StatusTypeID = 0; //status of "n/a"
            }
            return true;
        }

        public void Dispose()
        {
            if (__adversaryContext != null)
                __adversaryContext.Dispose();
            __adversaryContext = null;
        }
    }
}
