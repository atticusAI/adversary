﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Adversary.Utils;

namespace Adversary.DataLayer
{
    public class FileUploadClass : IScreeningMemoGet, IScreeningMemoSave, IScreeningMemoDelete
    {
        public bool HasUploadedFiles
        {
            get
            {
                if (GetAttachments().Count > 0) return true;
                else return false;
            }
        }


        public enum FileUploadMode
        {
            [Description("")]
            None,
            [Description("Engagement Letter")]
            EngagementLetter,
            [Description("Conflict Report")]
            ConflictReport,
            [Description("Written Policy")]
            WrittenPolicies,
            [Description("Rejection Letter")]
            RejectionLetter
        }


        public FileUploadClass(int screeningMemoID)
        {
            _ScreeningMemoID = screeningMemoID;
        }
        

        public bool Get()
        {
            using (AdversaryDataContext dc = new AdversaryDataContext(AdversaryConnection.AdversaryConnectionString()))
            {
                Memo m = (from M in dc.Memos
                          where M.ScrMemID == _ScreeningMemoID
                          select M).First();
                if (m != null)
                {
                    switch (_FileUploadType)
                    {
                        case FileUploadMode.None:
                            break;
                        case FileUploadMode.EngagementLetter:
                            if (m.EngageLetter.HasValue)
                            {
                                _FileUploaded = Convert.ToBoolean(m.EngageLetter);
                                _NoFileUploadReason = m.EngageDesc;
                            }
                            break;
                        case FileUploadMode.ConflictReport:
                            if (m.ConflictReport.HasValue)
                            {
                                _FileUploaded = Convert.ToBoolean(m.ConflictReport);
                                _NoFileUploadReason = m.ConflictDesc;
                            }
                            break;
                        case FileUploadMode.WrittenPolicies:
                            if (m.WrittenPolicies.HasValue)
                            {
                                _FileUploaded = Convert.ToBoolean(m.WrittenPolicies);
                            }
                            break;
                        case FileUploadMode.RejectionLetter:
                            if (m.NoRejectionLetter.HasValue)
                            {
                                _FileUploaded = !Convert.ToBoolean(m.NoRejectionLetter);
                                _NoFileUploadReason = m.NoRejectionLetterReason;
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            return true; 
        }

        public List<Attachment> GetAttachments()
        {
            List<Attachment> _attachments = new List<Attachment>();
            using (AdversaryDataContext dc = new AdversaryDataContext(AdversaryConnection.AdversaryConnectionString()))
            {
                if (FileUploadType == FileUploadMode.None)
                {
                    _attachments = (from A in dc.Attachments
                                    where A.ScrMemID == _ScreeningMemoID
                                    select A).ToList(); 
                }
                else
                {
                    _attachments = (from A in dc.Attachments
                                    where A.ScrMemID == _ScreeningMemoID
                                    && A.Description == _FileDescription
                                    select A).ToList();
                }
            }
            return _attachments;
        }

        public List<Attachment> GetAttachments(FileUploadMode fileUploadMode)
        {
            this.FileUploadType = fileUploadMode;
            return GetAttachments();
        }

        private int _ScreeningMemoID;
        public int ScreeningMemoID
        {
            get { return _ScreeningMemoID; }
        }

        private FileUploadMode _FileUploadType;

        public FileUploadMode FileUploadType
        {
            get
            {
                _FileDescription = _FileUploadType.ToDescription();
                return _FileUploadType;
            }
            set
            {
                _FileUploadType = value;
                _FileDescription = _FileUploadType.ToDescription();
            }
        }     
        
        private string _FileDescription;
        public string FileDescription
        {
            get { return _FileDescription; }
            set { _FileDescription = value; }
        }

        private string _FileName;
        public string FileName
        {
            get { return _FileName; }
            set { _FileName = value; }
        }
        
        private int? _AttachmentID;
        public int? AttachmentID
        {
            get { return _AttachmentID; }
            set { _AttachmentID = value; }
        }

        private string _NoFileUploadReason;
        public string NoFileUploadReason
        {
            get { return _NoFileUploadReason; }
            set { _NoFileUploadReason = value; }
        }

        private bool? _FileUploaded;
        public bool? FileUploaded
        {
            get { return _FileUploaded; }
            set { _FileUploaded = value; }
        }

        public bool Save()
        {
            Attachment att = new Attachment();
            using (AdversaryDataContext dc = new AdversaryDataContext(AdversaryConnection.AdversaryConnectionString()))
            {
                if (_FileUploaded.HasValue)
                {
                    if (Convert.ToBoolean(_FileUploaded))
                    {
                        if (_AttachmentID.HasValue)
                        {
                            if (_AttachmentID > 0)
                            {
                                att = (from A in dc.Attachments
                                       where A.AttachmentID == _AttachmentID
                                       select A).FirstOrDefault();
                            }
                        }
                        att.Description = _FileDescription;
                        att.FileName = _FileName;
                        att.ScrMemID = _ScreeningMemoID;

                        if (!_AttachmentID.HasValue)
                            dc.Attachments.InsertOnSubmit(att);
                    }
                    else
                    {
                        SaveMemoInformationOnly();
                    }
                }
               
                dc.SubmitChanges();
            }
                        
            return true;
            
        }

        public bool SaveMemoInformationOnly()
        {
            using (AdversaryDataContext dc = new AdversaryDataContext(AdversaryConnection.AdversaryConnectionString()))
            {
                Memo m = (from M in dc.Memos
                          where M.ScrMemID == _ScreeningMemoID
                          select M).First();
                if (m != null)
                {
                    switch (FileUploadType)
                    {
                        case FileUploadMode.None:
                            break;
                        case FileUploadMode.EngagementLetter:
                            m.EngageLetter = _FileUploaded;
                            m.EngageDesc = _NoFileUploadReason;
                            break;
                        case FileUploadMode.ConflictReport:
                            m.ConflictReport = _FileUploaded;
                            m.ConflictDesc = _NoFileUploadReason;
                            break;
                        case FileUploadMode.WrittenPolicies:
                            m.WrittenPolicies = _FileUploaded;
                            break;
                        case FileUploadMode.RejectionLetter:
                            m.NoRejectionLetterReason = _NoFileUploadReason;
                            m.NoRejectionLetter = !_FileUploaded;
                            break;
                        default:
                            break;
                    }
                }
                dc.SubmitChanges();
            }
            return false;
        }

        public bool Delete(int attachmentID)
        {
            this._AttachmentID = attachmentID;
            Delete();
            return true;
        }

        public bool Delete()
        {
            using (AdversaryDataContext dc = new AdversaryDataContext(AdversaryConnection.AdversaryConnectionString()))
            {
                Attachment att = new Attachment();
                att = (from A in dc.Attachments
                       where A.AttachmentID == _AttachmentID
                       select A).First();
                if (att != null)
                {
                    this.FileName = att.FileName;
                    dc.Attachments.DeleteOnSubmit(att);
                    dc.SubmitChanges();
                }
            }
            return true;
        }
    }
}
