﻿using System;
using System.Linq;
using System.Data.Linq;
using Adversary.DataLayer;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Adversary.Test.Notifications
{
    [TestClass]
    public class Notifications
    {
        private int[] _scrMemIds = { 116111, 116108 };

        // don't run this on live database records
        // you will loose your notification history
        private string _connectionString = "Data Source=10.10.162.39;Initial Catalog=Adversary07152012;user id=sa;password=d0ntuseSA";

        #region Test Methods
        
        /// <summary>
        /// test every notification condition starting with first ending with all sent
        /// </summary>
        [TestMethod]
        public void AllNotifications()
        {
            Assert.IsNotNull(this.NotificationRecords, "Notifications null where non null expected.");
            Assert.IsTrue(this.NotificationRecords.Count == this._scrMemIds.Count(), "Notifications count not equal to ScrMemIDs count.");

            if (this.NotificationRecords != null && this.NotificationRecords.Count > 0)
            {
                // save dates and set to now minus two days
                Dictionary<int, DateTime?> savedDates = new Dictionary<int, DateTime?>();
                foreach (Notification n in this.NotificationRecords)
                {
                    savedDates[n.NotificationID] = n.SubmittedOn;
                    this.UpdateSubmittedOn(n.NotificationID, DateTime.Now.AddDays(-1));
                }

                // run the tests
                try
                {
                    List<NotificationSelect> seconds = this.GetSecondNotifications();
                    Assert.IsTrue(this.HasNotifications(seconds, savedDates.Keys.ToList()), "Second Notifications not found where some were expected.");
                    this.SetSecondNotificationSent(seconds);
                    this.FakeValidNotification1Date(savedDates.Keys.ToList());

                    List<NotificationSelect> thirds = this.GetThirdNotifications();
                    Assert.IsTrue(this.HasNotifications(thirds, savedDates.Keys.ToList()), "Third Notifications not found where some were expected.");
                    this.SetThirdNotificationSent(thirds);
                    this.FakeValidNotification2Date(savedDates.Keys.ToList());

                    List<NotificationSelect> fourths = GetFourthNotifications();
                    Assert.IsTrue(this.HasNotifications(fourths, savedDates.Keys.ToList()), "Fourth Notifications not found where some were expected.");
                    this.SetFourthNotificationSent(fourths);
                    this.FakeValidNotification3Date(savedDates.Keys.ToList());

                    List<NotificationSelect> finals = this.GetFinalNotifications();
                    Assert.IsTrue(this.HasNotifications(finals, savedDates.Keys.ToList()), "Final Notifications not found where some were expected.");
                    this.SetFinalNotificationSent(finals);
                    this.FakeValidNotificationFinalDate(savedDates.Keys.ToList());

                    Assert.IsTrue(this.HasNotifications(finals, savedDates.Keys.ToList()), "Final Notifications not found where some were expected. (2)");
                    this.SetFinalNotificationSent(finals);
                    
                    seconds = this.GetSecondNotifications();
                    Assert.IsFalse(this.HasNotifications(seconds, savedDates.Keys.ToList()), "Found Second Notifications where none expected.");

                    thirds = this.GetThirdNotifications();
                    Assert.IsFalse(this.HasNotifications(thirds, savedDates.Keys.ToList()), "Found Third Notifications where none expected.");

                    fourths = GetFourthNotifications();
                    Assert.IsFalse(this.HasNotifications(fourths, savedDates.Keys.ToList()), "Found Fourth Notifications where none expected.");

                    finals = this.GetFinalNotifications();
                    Assert.IsFalse(this.HasNotifications(finals, savedDates.Keys.ToList()), "Found Final Notifications where none expected.");
                }
                catch (Exception x)
                {
                    Assert.Fail(x.Message);
                }
                finally
                {
                    // roll the dates back
                    foreach (int notificationID in savedDates.Keys)
                    {
                        this.UpdateSubmittedOn(notificationID, savedDates[notificationID]);
                    }
                }
            }
        }

        /// <summary>
        /// set the submitted date to NOW and verify that no notifications get selected
        /// </summary>
        [TestMethod]
        public void NoNotification()
        {
            Assert.IsNotNull(this.NotificationRecords, "Notifications null where non null expected.");
            Assert.IsTrue(this.NotificationRecords.Count == this._scrMemIds.Count(), "Notifications count not equal to ScrMemIDs count.");

            if (this.NotificationRecords != null && this.NotificationRecords.Count > 0)
            {
                // save dates and set to now
                Dictionary<int, DateTime?> savedDates = new Dictionary<int, DateTime?>();
                foreach (Notification n in this.NotificationRecords)
                {
                    savedDates[n.NotificationID] = n.SubmittedOn;
                    this.UpdateSubmittedOn(n.NotificationID, DateTime.Now);
                }

                // run the tests
                try
                {
                    List<NotificationSelect> seconds = this.GetSecondNotifications();                    
                    Assert.IsFalse(this.HasNotifications(seconds, savedDates.Keys.ToList()), "Found Second Notifications where none expected.");

                    List<NotificationSelect> thirds = this.GetThirdNotifications();
                    Assert.IsFalse(this.HasNotifications(thirds, savedDates.Keys.ToList()), "Found Third Notifications where none expected.");

                    List<NotificationSelect> fourths = GetFourthNotifications();
                    Assert.IsFalse(this.HasNotifications(fourths, savedDates.Keys.ToList()), "Found Fourth Notifications where none expected.");

                    List<NotificationSelect> finals = this.GetFinalNotifications();
                    Assert.IsFalse(this.HasNotifications(finals, savedDates.Keys.ToList()), "Found Final Notifications where none expected.");
                }
                catch (Exception x)
                {
                    Assert.Fail(x.Message);
                }
                finally
                {
                    // roll the dates back
                    foreach (int notificationID in savedDates.Keys)
                    {
                        this.UpdateSubmittedOn(notificationID, savedDates[notificationID]);
                    }
                }
            }
        }

        #endregion

        #region Test Utility Methods

        private void SetSecondNotificationSent(List<NotificationSelect> seconds)
        {
            foreach (NotificationSelect n in seconds){
                using (AdversaryDataContext dataContext = new AdversaryDataContext(this._connectionString))
                {
                    dataContext.ExecuteCommand("[dbo].[PR_SET_SECOND_NOTIFICATION_SENT] @NOTIFICATION_ID = {0}", n.Notification_ID);
                }
            }
        }

        private void SetThirdNotificationSent(List<NotificationSelect> thirds)
        {
            foreach (NotificationSelect n in thirds)
            {
                using (AdversaryDataContext dataContext = new AdversaryDataContext(this._connectionString))
                {
                    dataContext.ExecuteCommand("[dbo].[PR_SET_THIRD_NOTIFICATION_SENT] @NOTIFICATION_ID = {0}", n.Notification_ID);
                }
            }
        }

        private void SetFourthNotificationSent(List<NotificationSelect> fourths)
        {
            foreach (NotificationSelect n in fourths)
            {
                using (AdversaryDataContext dataContext = new AdversaryDataContext(this._connectionString))
                {
                    dataContext.ExecuteCommand("[dbo].[PR_SET_FOURTH_NOTIFICATION_SENT] @NOTIFICATION_ID = {0}", n.Notification_ID);
                }
            }
        }

        private void SetFinalNotificationSent(List<NotificationSelect> finals)
        {
            foreach (NotificationSelect n in finals)
            {
                using (AdversaryDataContext dataContext = new AdversaryDataContext(this._connectionString))
                {
                    dataContext.ExecuteCommand("[dbo].[PR_SET_FINAL_NOTIFICATION] @NOTIFICATIONID = {0}", n.Notification_ID);
                }
            }
        }

        private bool HasNotifications(List<NotificationSelect> notificationSelects, List<int> notificationIDs)
        {
            bool hasNotification = true;
            foreach (int notificationID in notificationIDs)
            {
                if (notificationSelects != null && notificationSelects.Count > 0)
                {
                    int count = (from t in notificationSelects
                                 where t.Notification_ID == notificationID
                                 select t).Count();
                    if (count == 0)
                    {
                        hasNotification = false;
                        break;
                    }
                }
                else
                {
                    hasNotification = false;
                }
            }
            return hasNotification;
        }

        private List<NotificationSelect> GetSecondNotifications()
        {
            using (AdversaryDataContext dataContext = new AdversaryDataContext(this._connectionString))
            {
                return (dataContext.ExecuteQuery<NotificationSelect>(
                    "exec [dbo].[PR_SELECT_SECOND_NOTIFICATIONS] @MINUTESELAPSED = 1440")
                    ).ToList();
            }
        }

        private List<NotificationSelect> GetThirdNotifications()
        {
            using (AdversaryDataContext dataContext = new AdversaryDataContext(this._connectionString))
            {
                return (dataContext.ExecuteQuery<NotificationSelect>(
                    "exec [dbo].[PR_SELECT_THIRD_NOTIFICATIONS] @MINUTESELAPSED = 1440")
                    ).ToList();
            }
        }

        private List<NotificationSelect> GetFourthNotifications()
        {
            using (AdversaryDataContext dataContext = new AdversaryDataContext(this._connectionString))
            {
                return (dataContext.ExecuteQuery<NotificationSelect>(
                    "exec [dbo].[PR_SELECT_FOURTH_NOTIFICATIONS] @MINUTESELAPSED = 1440")
                    ).ToList();
            }
        }

        private List<NotificationSelect> GetFinalNotifications()
        {
            using (AdversaryDataContext dataContext = new AdversaryDataContext(this._connectionString))
            {
                var finals = dataContext.ExecuteQuery<NotificationSelect>("exec [dbo].[PR_SELECT_FINAL_NOTIFICATIONS] @MINUTESELAPSED = 1440");
                return finals == null ? new List<NotificationSelect>() : finals.ToList();
            }
        }

        private void UpdateSubmittedOn(int notificationID, DateTime? submittedOn)
        {
            using (AdversaryDataContext dataContext = new AdversaryDataContext(this._connectionString))
            {
                var qry = from n in dataContext.Notifications
                          where n.NotificationID == notificationID
                          select n;

                Notification notification = qry.FirstOrDefault();

                if (notification != null)
                {
                    notification.SubmittedOn = submittedOn;
                    notification.Notification1Date = null;
                    notification.Notification1Sent = null;
                    notification.Notification2Date = null;
                    notification.Notification2Sent = null;
                    notification.Notification3Date = null;
                    notification.Notification3Sent = null;
                    notification.NotificationFinalDate = null;
                    notification.NotificationFinalSent = null;
                    dataContext.SubmitChanges();
                }
            }
        }

        private void FakeValidNotification1Date(List<int> notificationIDs)
        {
            using (AdversaryDataContext dataContext = new AdversaryDataContext(this._connectionString))
            {
                var qry = from n in dataContext.Notifications
                          where notificationIDs.Contains(n.NotificationID)
                          select n;

                foreach (Notification notification in qry.ToList())
                {
                    notification.Notification1Date = DateTime.Now.AddDays(-1);
                    dataContext.SubmitChanges();
                }
            }
        }

        private void FakeValidNotificationFinalDate(List<int> notificationIDs)
        {
            using (AdversaryDataContext dataContext = new AdversaryDataContext(this._connectionString))
            {
                var qry = from n in dataContext.Notifications
                          where notificationIDs.Contains(n.NotificationID)
                          select n;

                foreach (Notification notification in qry.ToList())
                {
                    notification.NotificationFinalDate = DateTime.Now.AddDays(-1);
                    dataContext.SubmitChanges();
                }
            }
        }

        private void FakeValidNotification2Date(List<int> notificationIDs)
        {
            using (AdversaryDataContext dataContext = new AdversaryDataContext(this._connectionString))
            {
                var qry = from n in dataContext.Notifications
                          where notificationIDs.Contains(n.NotificationID)
                          select n;

                foreach (Notification notification in qry.ToList())
                {
                    notification.Notification2Date = DateTime.Now.AddDays(-1);
                    dataContext.SubmitChanges();
                }
            }
        }

        private void FakeValidNotification3Date(List<int> notificationIDs)
        {
            using (AdversaryDataContext dataContext = new AdversaryDataContext(this._connectionString))
            {
                var qry = from n in dataContext.Notifications
                          where notificationIDs.Contains(n.NotificationID)
                          select n;

                foreach (Notification notification in qry.ToList())
                {
                    notification.Notification3Date = DateTime.Now.AddDays(-1);
                    dataContext.SubmitChanges();
                }
            }
        }

        private List<Notification> _notificationRecords = null;

        private List<Notification> NotificationRecords
        {
            get
            {
                if (this._notificationRecords == null)
                {
                    using (AdversaryDataContext dataContext = new AdversaryDataContext(this._connectionString))
                    {
                        this._notificationRecords = (from n in dataContext.Notifications
                                                     where this._scrMemIds.Contains(n.ScrMemID)
                                                     select n).ToList();
                    }
                }
                return this._notificationRecords;
            }
        }

        #endregion
    }

    public class NotificationSelect
    {
        public int Notification_ID{ get; set; }
        public int? Tracking_ID { get; set; }
        public int? ScrMem_ID { get; set; }
        public DateTime? SubmittedOn { get; set; }
        public int? ElapsedTimeHours { get; set; }
        public int? Office { get; set; }
        public string PracCode { get; set; }
        public string Email { get; set; }
        public int? StatusTypeID { get; set; }
        public int? SqlPart { get; set; }
        public string UserID { get; set; }
    }
}
