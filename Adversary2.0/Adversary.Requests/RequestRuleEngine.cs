﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Adversary.DataLayer;

namespace Adversary.Requests
{
    public class RequestRuleEngine : IDisposable
    {
        public RequestRuleEngine()  {     }
        public RequestRuleEngine(Request request)
        {
            Request = request;
        }

        private Request _Request;
        public Request Request
        {
            private get { return _Request; }
            set { _Request = value; }
        }

        private List<AllRule> _RuleViolations;
        public List<AllRule> RuleViolations
        {
            get { if (_RuleViolations == null) _RuleViolations = new List<AllRule>(); return _RuleViolations; }
            private set { _RuleViolations = value; }
        }

        private bool _HasErrors;
        public bool HasErrors
        {
            get { return _RuleViolations.Count > 0; }
            private set { _HasErrors = value; }
        }

        public bool RunRules()
        {
            if (this.Request == null) return false;

            List<AllRule> _RulesToRun = new List<AllRule>();
            Adversary.DataLayer.Rules _Rules = new Rules();
            _RulesToRun = _Rules.GetAllRules(8, true);

            foreach (AllRule rule in _RulesToRun)
            {
                switch (rule.RuleID)
                {
                    case 41:
                        if (Request.DeadlineDate == null || Request.DeadlineDate <= DateTime.Now)
                            RuleViolations.Add(rule);
                        break;
                    case 42:
                        if (string.IsNullOrEmpty(Request.ClientName))
                            RuleViolations.Add(rule);
                        break;
                    case 43:
                        if (string.IsNullOrEmpty(Request.TypistLogin))
                            RuleViolations.Add(rule);
                        break;
                    case 44:
                        if (string.IsNullOrEmpty(Request.AttyLogin))
                            RuleViolations.Add(rule);
                        break;
                    case 45:
                        bool criteriaChecked = (from bools in Request.GetType().GetProperties()
                                where bools.PropertyType == typeof(bool?) 
                                && bools.Name != "DailyMemo" 
                                && bools.Name != "Pharm" 
                                && bools.Name != "OKForNewMatter" 
                                && (bool?)bools.GetValue(Request, null) == true
                                select bools).Count() > 0;
                        criteriaChecked = (!criteriaChecked ? (Request.NoneOfTheAbove.HasValue && Request.NoneOfTheAbove.Value) : criteriaChecked);
                        if (!criteriaChecked)
                            RuleViolations.Add(rule);
                        break;
                    default:
                        break;
                }
            }
            return (RuleViolations.Count == 0);
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public bool CanSubmit()
        {
            return (RunRules() &&
                String.IsNullOrEmpty(Request.WorkCompletedBy) &&
                ((!Request.SubmittalDate.HasValue) ||
                (Request.SubmittalDate.HasValue && !String.IsNullOrEmpty(Request.WorkBegunBy))));
        }

        private IEnumerable<string> GetErrorMessages(IEnumerable<AllRule> rules)
        {
            foreach (AllRule rule in rules)
            {
                yield return rule.RuleText;
            }
        }

        private List<string> _ErrorMessages;
        public List<string> ErrorMessages
        {
            get
            {
                if (this.RuleViolations.Count > 0)
                {
                    return GetErrorMessages((IEnumerable<AllRule>)RuleViolations).ToList();
                }
                else
                    return new List<string>() { };
            }
        }

    }
}
/*
 
 ValidationException ex = new ValidationException("This adversary request is not valid");

        if (this.DeadlineDate == null ||
            this.DeadlineDate <= DateTime.Now)
            ex.AddInvalidReason("The deadline date for this request is either invalid or has not been entered");

        if (string.IsNullOrEmpty(this.ClientName))
            ex.AddInvalidReason("A client name was not entered");

        if (string.IsNullOrEmpty(this.TypistLogin))
            ex.AddInvalidReason("A name or payroll ID was not specified for this request");

        if (string.IsNullOrEmpty(this.AttyLogin))
            ex.AddInvalidReason("An attorney name or payroll ID was not specified for this request");

        //bool criteriaChecked = ((this.AccountingFirm.HasValue && (bool)this.AccountingFirm) ||
        //                        (this.Communications.HasValue && (bool)this.Communications) ||
        //                        (this.ElectricUtility.HasValue && (bool)this.ElectricUtility) ||
        //                        (this.Government.HasValue && (bool)this.Government) ||
        //                        (this.IndianTribe.HasValue && (bool)this.IndianTribe) ||
        //                        (this.Insurance.HasValue && (bool)this.Insurance) ||
        //                        (this.LawFirm.HasValue && (bool)this.LawFirm) ||
        //                        (this.LegalFees.HasValue && (bool)this.LegalFees) ||
        //                        (this.OilGas.HasValue && (bool)this.OilGas) ||
        //                        (this.Pharm.HasValue && (bool)this.Pharm) ||
        //                        (this.SkiResort.HasValue && (bool)this.SkiResort) ||
        //                        (this.NoneOfTheAbove.HasValue && (bool)this.NoneOfTheAbove));

        bool criteriaChecked = (from bools in this.GetType().GetProperties()
                                where bools.PropertyType == typeof(bool?) && 
                                bools.Name != "DailyMemo" && 
                                (bool?)bools.GetValue(this, null) == true
                                select bools).Count() > 0;
        
        if (!criteriaChecked)
            ex.AddInvalidReason("Please choose a valid criteria for the client or adverse party on the Additional Information page.");

        return ex;*/
