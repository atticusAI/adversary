﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Adversary.DataLayer;
using System.Net.Mail;
using Adversary.Utils;
using System.Configuration;

namespace Adversary.Requests
{
    public class RequestEmail
    {
        private AdversaryData advData;
        private EmailService.EmailClient emailServiceClient;

        public RequestEmail()
        {
            advData = new AdversaryData();
            emailServiceClient = new EmailService.EmailClient();
        }
        public void SendAdversaryRequestEmail(Adversary.DataLayer.Request request)
        {
            List<string> to = new List<string>();
            bool isRequestComplete = request.HH_IsRequestComplete();
            if (!isRequestComplete)
            {
                foreach (AdminLogin adminLogin in advData.GetAdversaryGroupAdmins())
                {
                    to.Add(adminLogin.Email);
                }
            }

            if (!String.IsNullOrEmpty(request.TypistLogin))
                to.Add(advData.GetHREmployee(request.TypistLogin).CompanyEmailAddress);
            if (!String.IsNullOrEmpty(request.AttyLogin))
                to.Add(advData.GetHREmployee(request.AttyLogin).CompanyEmailAddress);

            StringBuilder msg = new StringBuilder();
            bool existingMemo = !(String.IsNullOrEmpty(request.WorkBegunBy));
            MailAddress from = new MailAddress("AdversaryRequest@hollandhart.com");
            string subject;
            string header;
            if (isRequestComplete)
            {
                subject = "Adversary Request Processed and Completed";
                header = "Adversary Request Completed By " + request.WorkCompletedBy;
            }
            else
            {
                subject = (!existingMemo) ? "New Adversary Request" : "Adversary Request Updated";
                header = "Adversary Request Submitted";
            }

            if (existingMemo && !isRequestComplete)
                msg.AppendLine("EDIT NOTICE: A REQUEST ASSIGNED TO " + request.WorkBegunBy + " HAS BEEN UPDATED BY THE USER.\r\n\r\n");

            msg.AppendLine(header);
            msg.AppendLine();
            msg.AppendLine();
            msg.AppendLine("Request # " + request.ReqID.ToString());
            msg.AppendLine();
            msg.AppendLine("Submitted On: " + String.Format("{0:G}", request.SubmittalDate));
            msg.AppendLine();
            msg.AppendLine("Client Name: " + request.ClientName);
            msg.AppendLine();
            msg.AppendLine("Client Matter Number: " + request.ClientMatter);
            msg.AppendLine();
            msg.AppendLine("Attorney: " + advData.GetEmployee(request.AttyLogin).EmployeeName);
            msg.AppendLine();
            msg.AppendLine("Submitted By: " + advData.GetEmployee(request.TypistLogin).EmployeeName);
            msg.AppendLine();

            if (WebUtils.TestMode)
            {
                if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["EmailTestAddresses"]))
                {
                    string[] testAddresses = ConfigurationManager.AppSettings["EmailTestAddresses"].Split(',');
                    if (testAddresses != null && testAddresses.Count() > 0)
                    {
                        msg.Insert(0, "***TEST MODE***\r\n" +
                            string.Join("\r\n", to) +
                            "\r\n" +
                            "***************\r\n");
                        emailServiceClient.SendMail(from.Address, "Adversary Request", testAddresses, null, null, subject, msg.ToString(), false, false);
                    }
                }
            }
            else
            {
                emailServiceClient.SendMail(from.Address, "Adversary Request", to.ToArray(), null, null, subject, msg.ToString(), false, false);
            }
        }
    }
}
