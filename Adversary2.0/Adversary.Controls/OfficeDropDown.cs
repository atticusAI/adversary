﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using Adversary.DataLayer;
using Adversary.Utils;

namespace Adversary.Controls
{
    [ToolboxData("<{0}:OfficeDropDown runat=server></{0}:OfficeDropDown>")]
    public class OfficeDropDown : UserControl, INamingContainer
    {
        public DropDownList ddlOffice;

        public OfficeDropDown()
        {
            ddlOffice = new DropDownList();
        }

        private string _SelectedOffice;

        public string SelectedOffice
        {
            get { return _SelectedOffice; }
            set
            { 
                _SelectedOffice = value;
                if (ddlOffice.SelectedValue != _SelectedOffice || ddlOffice.SelectedIndex == -1)
                {
                    try
                    {
                        ddlOffice.SelectedIndex = -1;
                        ddlOffice.Items.FindByValue(_SelectedOffice).Selected = true;
                    }
                    catch { /*do nothing*/ }
                }
            
            }
        }

        private string _CssClass;
        [CssClassProperty]
        public string CssClass
        {
            get
            {
                return _CssClass;
            }
            set
            {
                _CssClass = value;
            }
        }



        protected override void CreateChildControls()
        {
            EnsureChildControls();
            ddlOffice = new DropDownList();
            if (CssClass != null)
            {
                this.ddlOffice.CssClass = CssClass;
            }
            BindOfficeData();
            Controls.Add(ddlOffice);
        }

        private void BindOfficeData()
        {
            List<Adversary.DataLayer.HRDataService.FirmOffice> _offices = new List<DataLayer.HRDataService.FirmOffice>();

            using (Adversary.DataLayer.AdversaryData advData = new AdversaryData())
            {
                _offices = advData.GetFirmOffices();
            }
            this.ddlOffice.DataSource = _offices;
            this.ddlOffice.DataTextField = "LocationDescription";
            this.ddlOffice.DataValueField = "LocationCode";
            
            this.ddlOffice.DataBind();

            if (!String.IsNullOrEmpty(_SelectedOffice))
            {
                try
                {
                    ddlOffice.SelectedIndex = -1;
                    ddlOffice.Items.FindByValue(_SelectedOffice).Selected = true;
                }
                catch { }
            }

        }
    }
}
