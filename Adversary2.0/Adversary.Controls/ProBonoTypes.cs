﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.DataLayer;
using System.Linq;
using System.Data.Linq;
using System;

namespace Adversary.Controls
{
    [ToolboxData("<{0}:ProBonoTypesDropdown runat=server></{0}:ProBonoTypesDropdown>")]
    public class ProBonoTypes : WebControl, INamingContainer
    {
        private List<ProBonoType> _ProBonoTypes = new List<ProBonoType>();

        protected DropDownList ddlProBonoTypes
        {
            get;
            private set;
        }

        [Bindable(true)]
        public int SelectedProBonoType
        {
            get;
            set;
        }

        protected override void EnsureChildControls()
        {
            if (ddlProBonoTypes == null) ddlProBonoTypes = new DropDownList();
            base.EnsureChildControls();
        }

        protected override void CreateChildControls()
        {
            base.CreateChildControls();
            if (ddlProBonoTypes != null)
            {
                ddlProBonoTypes.ID = "ddlProBonoTypes";
                using (AdversaryData data = new AdversaryData())
                {
                    _ProBonoTypes = data.GetProBonoTypes();
                }
                ddlProBonoTypes.DataTextField = "Name";
                ddlProBonoTypes.DataValueField = "ProBonoTypeID";
                ddlProBonoTypes.DataSource = _ProBonoTypes;
                ddlProBonoTypes.DataBind();
            }

            if (SelectedProBonoType > 0)
            {
                ddlProBonoTypes.SelectedIndex = -1;
                ddlProBonoTypes.Items.FindByValue(SelectedProBonoType.ToString()).Selected = true;
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            ddlProBonoTypes.RenderControl(writer);
        }
    }
}
