﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.DataLayer;

namespace Adversary.Controls
{
    [ToolboxData("<{0}:EstimateOfFeesDropDown runat=server></{0}:EstimateOfFeesDropDown>")]
    public class EstimateOfFeesDropDown : UserControl, INamingContainer
    {
        private DropDownList _ddlEstimateOfFees = null;
        public DropDownList ddlEstimateOfFees
        {
            get { return _ddlEstimateOfFees; }
        }

        private string _SelectedEstimateOfFees;

        public string SelectedEstimateOfFees
        {
            get { return _SelectedEstimateOfFees; }
            set { _SelectedEstimateOfFees = value; }
        }


        protected override void CreateChildControls()
        {
            EnsureChildControls();
            base.CreateChildControls();
            _ddlEstimateOfFees = new DropDownList();
            _ddlEstimateOfFees.Items.Add(new ListItem("Over $10,000", "Over_$10000"));
            _ddlEstimateOfFees.Items.Insert(0, new ListItem("$5,000 - $10,000", "$5000_$10000"));
            _ddlEstimateOfFees.Items.Insert(0, new ListItem("Under $5,000", "Under_$5000"));
            Controls.Add(_ddlEstimateOfFees);
            if (!string.IsNullOrEmpty(_SelectedEstimateOfFees))
            {
                try
                {
                    _ddlEstimateOfFees.Items.FindByValue(_SelectedEstimateOfFees).Selected = true;
                }
                catch (System.Exception)
                {
                    _ddlEstimateOfFees.SelectedIndex = -1;
                }
                
            }
            
        }

    }
}
