﻿using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;

namespace Adversary.Controls
{
    [ToolboxData("<{0}:AutoCompleteField runat=server></{0}:AutoCompleteField>")]
    public class AutoCompleteField : WebControl, INamingContainer
    {
        public enum ServiceBindingSource
        {
            Timekeepers,
            PracticeCodes,
            ClientMatterNumbers
        }

        private AutoCompleteExtender acExtender = null;
        private TextBox tbEntry = null;
        private ServiceBindingSource _BindingSource;
        private int _CompletionSetCount = 1;
        private int _MinimumPrefixLength = 2;
        private ScriptManager sm;
        
        [Bindable(true)]
        [RequiredProperty()]
        public ServiceBindingSource BindingSource
        {
            get { return _BindingSource; }
            set { _BindingSource = value; }
        }
        [Bindable(true)]
        public int CompletionSetCount
        {
            get { return _CompletionSetCount; }
            set { _CompletionSetCount = value; }
        }
        [Bindable(true)]
        public int MinimumPrefixLength
        {
            get { return _MinimumPrefixLength; }
            set { _MinimumPrefixLength = value; }
        }

        public AutoCompleteField()
        {
            acExtender = new AjaxControlToolkit.AutoCompleteExtender();
            tbEntry = new TextBox();
        }
        protected override void OnInit(EventArgs e)
        {
            sm = ScriptManager.GetCurrent(this.Page);
            acExtender = new AutoCompleteExtender();
            tbEntry = new TextBox();
            base.OnInit(e);
        }
        
        protected override void CreateChildControls()
        {
            base.CreateChildControls();
            //set up the textbox
            if (tbEntry != null)
            {
                tbEntry.ID = "tb__" + this.ID;
                if (!String.IsNullOrEmpty(this.CssClass))
                    tbEntry.CssClass = this.CssClass;
            }


            //set up the ac extender
            if (acExtender != null)
            {
                acExtender.ID = "ac__" + tbEntry.ID;
                acExtender.CompletionSetCount = _CompletionSetCount;
                acExtender.FirstRowSelected = true;
                acExtender.MinimumPrefixLength = _MinimumPrefixLength;
                acExtender.TargetControlID = tbEntry.ID;
                acExtender.Page = this.Page;
                switch (_BindingSource)
                {
                    case ServiceBindingSource.Timekeepers:
                        acExtender.ServiceMethod = "TimekeeperService";
                        break;
                    case ServiceBindingSource.PracticeCodes:
                        break;
                    case ServiceBindingSource.ClientMatterNumbers:
                        break;
                    default:
                        break;
                }
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            sm.RegisterExtenderControl<AutoCompleteExtender>(acExtender, tbEntry);
        }

        protected override void Render(HtmlTextWriter writer)
        {
            base.Render(writer);
            tbEntry.RenderControl(writer);
            acExtender.RenderControl(writer);
        }
    }
}
