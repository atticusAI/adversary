﻿using Adversary.DataLayer;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Collections.Generic;

namespace Adversary.Controls
{
    [ToolboxData("<{0}:ClientTypeRadioButtons runat=server></{0}:ClientTypeRadioButtons>")]
    public class ClientTypeRadioButtons : WebControl, INamingContainer
    {
        [Bindable(true)]
        public string SetValue;
    }
}
