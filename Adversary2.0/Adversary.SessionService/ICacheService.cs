﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Adversary.SessionService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICacheService" in both code and config file together.
    [ServiceContract]
    public interface ICacheService
    {
        [OperationContract(Name="GetObjectByKey")]
        object Get(string key);
        [OperationContract(Name = "GetObjectByGuid")]
        object Get(Guid guid);
        [OperationContract]
        void PlaceObject(object objectToPlace, string key);
        [OperationContract]
        string TestMe();
    }
}
