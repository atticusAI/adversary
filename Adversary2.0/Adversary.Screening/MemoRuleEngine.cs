﻿//changes 06/01/2012 by A Reimer for New Client/New Matter spec change
//the group has said that a new client/new matter screening memo (track 1)
//MUST ALWAYS have a conflict report and engagement letter.

//rules 38, 39, 40 do not apply any longer to track 1 and they were removed from the MemoRules table

//rule 46 now applies, and the question for whether or not the entering attorney is uploading a report
//will be changed.  It will always now default to "yes", but also only require an upload.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Adversary.DataLayer;
using Adversary.Utils;
using System.Data;

namespace Adversary.Screening
{
    public class MemoRuleEngine : IDisposable
    {
        private Adversary.DataLayer.ClientAddress __ClientAddress;
        private Adversary.DataLayer.NewClientInformation __NewClient;
        private Adversary.DataLayer.FileUploadClass __FileUpload;
        private Adversary.DataLayer.AdversaryData __AdversaryData;

        /// <summary>
        /// The screening memo on which to attach rules
        /// </summary>
        private Memo _ScreeningMemo;
        public Memo ScreeningMemo
        {
            //get { return _ScreeningMemo; }
            set { _ScreeningMemo = value; }
        }

        public MemoRuleEngine(Memo ScreeningMemoToEvaluate)
        {
            this._ScreeningMemo = ScreeningMemoToEvaluate;
            _RuleViolations = new List<AllRule>();
            __NewClient = new NewClientInformation(_ScreeningMemo.ScrMemID);
            __NewClient.Get();

            __ClientAddress = new ClientAddress(_ScreeningMemo.ScrMemID);
            __ClientAddress.Get();

            __FileUpload = new FileUploadClass(_ScreeningMemo.ScrMemID);

            __AdversaryData = new AdversaryData();

        }

        private bool _HasErrors;
        public bool HasErrors
        {
            get { return _RuleViolations.Count > 0; }
            private set { _HasErrors = value; }
        }

        private List<AllRule> _RuleViolations;
        public List<AllRule> RuleViolations
        {
            get { return _RuleViolations; }
            private set { _RuleViolations = value; }
        }

        public void RunRules(Memo _memo)
        {
            _ScreeningMemo = _memo;
            RunRules();
        }

        public void RunRules()
        {
            List<AllRule> _RulesToRun = new List<AllRule>();
            Adversary.DataLayer.Rules _Rules = new Rules();
            Adversary.DataLayer.HRDataService.PeopleDataContractClient peopleData = new DataLayer.HRDataService.PeopleDataContractClient();
            Adversary.DataLayer.NDriveStaff nDrive = new NDriveStaff(_ScreeningMemo.ScrMemID);
            
            int __conflictReportFileCount = 0;
            int __engagementLetterFileCount = 0;
            int __rejectionLetterFileCount = 0;
            int __nDriveAccessCount = 0;
            int __writtenPoliciesFileCount = 0;

            List<Attachment> __attachments = __FileUpload.GetAttachments(FileUploadClass.FileUploadMode.None);
            __conflictReportFileCount = __attachments.Where(A => A.Description.Equals("Conflict Report")).Count();
            __engagementLetterFileCount = __attachments.Where(A => A.Description.Equals("Engagement Letter")).Count();
            __rejectionLetterFileCount = __attachments.Where(A => A.Description.Equals("Rejection Letter")).Count();
            __writtenPoliciesFileCount = __attachments.Where(A => A.Description.Equals("Written Policy")).Count();
            __nDriveAccessCount = nDrive.GetNDriveAccess().Count;
           
            //get the rules
            _RulesToRun = _Rules.GetAllRules(Convert.ToInt32(_ScreeningMemo.ScrMemType), true);

            foreach (AllRule rule in _RulesToRun)
            {
                switch (rule.RuleID)
                {
                    case 1:
                        if (String.IsNullOrEmpty(_ScreeningMemo.AttEntering))
                        {
                            _RuleViolations.Add(rule);
                        }
                        break;
                    case 2:
                        if (peopleData.GetEmployeeByPersonId(_ScreeningMemo.AttEntering) == null)
                        {
                            rule.FormatRuleMessage(rule.RuleText,
                                _ScreeningMemo.AttEntering);
                            _RuleViolations.Add(rule);
                        }
                        break;
                    case 3:
                        if (String.IsNullOrEmpty(_ScreeningMemo.PersonnelID))
                        {
                            _RuleViolations.Add(rule);
                        }
                        break;
                    case 4:
                        if (_ScreeningMemo.PersonnelID.StartsWith("8")) continue;
                        if (peopleData.GetEmployeeByPersonId(_ScreeningMemo.PersonnelID) == null)
                        {
                            rule.FormatRuleMessage(rule.RuleText, _ScreeningMemo.PersonnelID);
                            _RuleViolations.Add(rule);
                        }
                        break;
                    case 5:
                        if (_ScreeningMemo.ScrMemType.Value == 2 && String.IsNullOrEmpty(_ScreeningMemo.ClientNumber))
                        {
                            _RuleViolations.Add(rule);
                        }
                        break;
                    case 6:
                        if (string.IsNullOrEmpty(_ScreeningMemo.ClientNumber) && !_ScreeningMemo.Company.HasValue)
                            _RuleViolations.Add(rule);
                        break;
                    case 7:
                        if (_ScreeningMemo.Company ?? false)
                        {
                            if (_ScreeningMemo.ScrMemType == 6)
                            {
                                if (_ScreeningMemo.NewClients != null && _ScreeningMemo.NewClients.Count > 0)
                                {
                                    NewClient clnt = _ScreeningMemo.NewClients[0];
                                    if (String.IsNullOrEmpty(clnt.CName))
                                    {
                                        _RuleViolations.Add(rule);
                                    }
                                }
                                else
                                {
                                    _RuleViolations.Add(rule);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(__NewClient.OrganizationName))
                                {
                                    _RuleViolations.Add(rule);
                                }
                            }
                        }
                        break;
                    case 8:
                        if (
                            !(_ScreeningMemo.Company ?? false) && 
                            (_ScreeningMemo.Individual ?? false) &&
                            String.IsNullOrEmpty(__NewClient.ClientFirstName) &&
                            String.IsNullOrEmpty(__NewClient.ClientMiddleName) &&
                            String.IsNullOrEmpty(__NewClient.ClientLastName))
                        {
                            _RuleViolations.Add(rule);
                        }
                        break;
                    case 9:
                        if (__ClientAddress.Address == null || string.IsNullOrEmpty(__ClientAddress.Address.Address1))
                            _RuleViolations.Add(rule);
                        break;
                    case 10:
                        if (__ClientAddress.Address == null || string.IsNullOrEmpty(__ClientAddress.Address.City))
                            _RuleViolations.Add(rule);
                        break;
                    case 11:
                        //there is no 11?
                        break;
                    case 12:
                        if (__ClientAddress.Address == null || string.IsNullOrEmpty(__ClientAddress.Address.State))
                            _RuleViolations.Add(rule);
                        break;
                    case 13:
                        if (__ClientAddress.Address == null || string.IsNullOrEmpty(__ClientAddress.Address.Country))
                            _RuleViolations.Add(rule);
                        break;
                    case 14:
                        if (__ClientAddress.Address == null || string.IsNullOrEmpty(__ClientAddress.Address.Phone))
                                _RuleViolations.Add(rule);
                        break;
                    case 15:
                        if (String.IsNullOrEmpty(_ScreeningMemo.RejectDesc))
                        {
                            _RuleViolations.Add(rule);
                        }
                        break;                
                    case 16: 
                        if (!_ScreeningMemo.NoRejectionLetter.HasValue)
                        {
                            _RuleViolations.Add(rule);
                            break;
                        }
                        break;
                    case 17: 
                        if((_ScreeningMemo.NoRejectionLetter.HasValue && _ScreeningMemo.NoRejectionLetter.Value) && 
                            String.IsNullOrEmpty(_ScreeningMemo.NoRejectionLetterReason))
                        {
                            _RuleViolations.Add(rule);
                        }
                        break;
                    case 18: 
                        if (_ScreeningMemo.NoRejectionLetter.HasValue && !_ScreeningMemo.NoRejectionLetter.Value && __rejectionLetterFileCount <= 0)
                        {
                            _RuleViolations.Add(rule);
                        }
                        break;
                    //change to ensure that for New Client/New Matter memos (track 1) that an engagement letter
                    //is ALWAYS uploaded. New rule added. 06/01/2012
                    case 19:
                        if (!_ScreeningMemo.EngageLetter.HasValue)
                        {
                            _RuleViolations.Add(rule);
                        }
                        break;
                    case 20:
                        if (_ScreeningMemo.EngageLetter.HasValue)
                        {
                            if (_ScreeningMemo.EngageLetter.Value && __engagementLetterFileCount <= 0)
                            {
                                _RuleViolations.Add(rule);
                            }
                        }
                        break;
                    case 21:
                        if (_ScreeningMemo.EngageLetter.HasValue)
                        {
                            if (_ScreeningMemo.EngageLetter.Value == false && String.IsNullOrEmpty(_ScreeningMemo.EngageDesc))
                                _RuleViolations.Add(rule);
                        }
                        break;
                    case 22:
                        if (String.IsNullOrEmpty(_ScreeningMemo.OrigOffice))
                        {
                            _RuleViolations.Add(rule);
                        }
                        break;
                    case 23:
                        if (String.IsNullOrEmpty(_ScreeningMemo.PracCode))
                        {
                            _RuleViolations.Add(rule);
                        }
                        break;
                    case 24:
                        //if (StringUtils.UnusedPracticeCodes.Contains(_ScreeningMemo.PracCode))
                        if(Adversary.DataLayer.AdversaryData.UnusedPracticeCodes.Contains(_ScreeningMemo.PracCode))
                        {
                            //rule.RuleText = String.Format(rule.RuleText, String.Join(",", StringUtils.UnusedPracticeCodes));
                            rule.RuleText = String.Format(rule.RuleText, String.Join(",", Adversary.DataLayer.AdversaryData.UnusedPracticeCodes));
                            _RuleViolations.Add(rule);
                        }
                        break;
                    case 25: //matter name
                        if (String.IsNullOrEmpty(_ScreeningMemo.MatterName))
                        {
                            _RuleViolations.Add(rule);
                        }
                        break;
                    case 26: //description of work
                        if (String.IsNullOrEmpty(_ScreeningMemo.WorkDesc) && rule.RuleID.Equals(26))
                        {
                            _RuleViolations.Add(rule);
                        }
                        break;
                    case 27: //ndrive type
                    case 28: //ndrive users
                        if (_ScreeningMemo.NDrive.HasValue && _ScreeningMemo.NDrive.Value)
                        {
                            if ((!_ScreeningMemo.NDriveType.HasValue || _ScreeningMemo.NDriveType < 1) && rule.RuleID.Equals(27))
                            {
                                _RuleViolations.Add(rule);
                            }
                            else if ((_ScreeningMemo.NDriveType.Value == 4 || _ScreeningMemo.NDriveType.Value == 5) &&
                                __nDriveAccessCount == 0 && rule.RuleID.Equals(28))
                                _RuleViolations.Add(rule);
                        }
                        break;
                    case 29: //docketing attorney
                    case 30: //docketing staff
                        if (__AdversaryData.IsMemoDocketingRequired(_ScreeningMemo))
                        {
                            if (rule.RuleID.Equals(29) && string.IsNullOrEmpty(_ScreeningMemo.DocketingAtty))
                                _RuleViolations.Add(rule);
                            if (rule.RuleID.Equals(30) && string.IsNullOrEmpty(_ScreeningMemo.DocketingStaff))
                                _RuleViolations.Add(rule);
                        }
                        break;
                    case 31: //billing attorney missing
                    case 32: //billing attorney didn't resolve
                        if (string.IsNullOrEmpty(_ScreeningMemo.RespAttID) && rule.RuleID.Equals(31))
                        {
                            _RuleViolations.Add(rule);
                        }
                        else
                        {
                            if (__AdversaryData.GetEmployee(_ScreeningMemo.RespAttID) == null && rule.RuleID.Equals(32))
                            {
                                rule.RuleText = String.Format(rule.RuleText, _ScreeningMemo.RespAttID);
                                _RuleViolations.Add(rule);
                            }
                        }
                        break;
                    case 33: //retainer
                        if (!_ScreeningMemo.Retainer.HasValue)
                            _RuleViolations.Add(rule);
                        break;
                    case 34: //previous origination credit
                    case 35: //justification for origination credit
                        if (_ScreeningMemo.FeeSplitStaffs.Count > 0)
                        {
                            if (string.IsNullOrEmpty(_ScreeningMemo.NewMatterOnly) && rule.RuleID.Equals(34))
                                _RuleViolations.Add(rule);
                            else if (_ScreeningMemo.NewMatterOnly == "no" && String.IsNullOrEmpty(_ScreeningMemo.FeeSplitDesc) && rule.RuleID.Equals(35))
                                _RuleViolations.Add(rule);
                        }
                        break;
                    case 36: //written policies
                        //ensure selection
                        if (!_ScreeningMemo.WrittenPolicies.HasValue)
                            _RuleViolations.Add(rule);
                        break;
                    case 37:
                        if (String.IsNullOrEmpty(_ScreeningMemo.ProBonoReason))
                            _RuleViolations.Add(rule);
                        break;

                        

                    case 38: //conflict report
                        if (!_ScreeningMemo.ConflictReport.HasValue)
                            _RuleViolations.Add(rule);
                        break;
                    case 39: //user is uploading a conflict report, but has not specified any files.
                        if (((_ScreeningMemo.ConflictReport ?? false) && __conflictReportFileCount == 0) && !String.IsNullOrEmpty(_ScreeningMemo.ConflictDesc))
                        {
                            _RuleViolations.Add(rule);
                        }
                        break;
                    case 40: //user has specified that the conflict report won't be uploaded but did not specify a reason.
                        if (_ScreeningMemo.ConflictReport.HasValue && !_ScreeningMemo.ConflictReport.Value)
                        {
                            if (String.IsNullOrEmpty(_ScreeningMemo.ConflictDesc))
                                _RuleViolations.Add(rule);
                        }
                        break;
                    case 46: //new matters new clients - MUST have a conflict report -- older memos before this rule
                        //was implemented can have a description - they will pass validation.  The new method does not
                        //allow typing a description.
                        if (__conflictReportFileCount <= 0 && String.IsNullOrEmpty(_ScreeningMemo.ConflictDesc))
                        {
                            _RuleViolations.Add(rule);
                        }
                        break;
                    case 47:  //new matters new clients - MUST have an engagement letter -- older memos before this rule
                        //was implemented can have a description - they will pass validation.  The new method does not allow
                        //typing a description
                        if (__engagementLetterFileCount <= 0 && String.IsNullOrEmpty(_ScreeningMemo.EngageDesc))
                        {
                            _RuleViolations.Add(rule);
                        }
                        break;
                    case 48: //written policies were declared to be attached but no attachments were found.
                        if (__writtenPoliciesFileCount <= 0 && (_ScreeningMemo.WrittenPolicies.HasValue && _ScreeningMemo.WrittenPolicies.Value))
                        {
                            _RuleViolations.Add(rule);
                        }
                        break;
                    case 49:
                        if((!_ScreeningMemo.Individual.HasValue || !_ScreeningMemo.Individual.Value) &&
                            (!_ScreeningMemo.Company.HasValue || !_ScreeningMemo.Company.Value))
                        {
                            _RuleViolations.Add(rule);
                        }
                        break;
                    default:
                        break;
                }
            }
            

        }


        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
