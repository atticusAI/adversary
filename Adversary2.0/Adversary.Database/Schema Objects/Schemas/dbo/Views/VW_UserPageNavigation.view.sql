﻿


/***********************************************************************************************************/
/* Create Views*/
CREATE VIEW [dbo].[VW_UserPageNavigation] AS 

SELECT al.[UserID],
	r.[Role], 
	r.[RoleID], 
	pn.[PageNavigationID],
	pn.[PageUrl]
FROM [dbo].[AdminLogins] al
LEFT OUTER JOIN [dbo].[Role] r on r.[Role] = 'Adversary'
	AND ISNULL(al.[IsAdversary], 0) = 1
LEFT OUTER JOIN [dbo].[PageNavigationRole] pnr on pnr.[RoleID] = r.[RoleID]
LEFT OUTER JOIN [dbo].[PageNavigation] pn on pn.[PageNavigationID] = pnr.[PageNavigationID]
WHERE pn.[PageNavigationID] IS NOT NULL

UNION

SELECT al.[UserID],
	r.[Role], 
	r.[RoleID], 
	pn.[PageNavigationID],
	pn.[PageUrl]
FROM [dbo].[AdminLogins] al
LEFT OUTER JOIN [dbo].[Role] r on r.[Role] = 'AdversaryAdmin'
	AND ISNULL(al.[IsAdversaryAdmin], 0) = 1
LEFT OUTER JOIN [dbo].[PageNavigationRole] pnr on pnr.[RoleID] = r.[RoleID]
LEFT OUTER JOIN [dbo].[PageNavigation] pn on pn.[PageNavigationID] = pnr.[PageNavigationID]
WHERE pn.[PageNavigationID] IS NOT NULL

UNION

SELECT al.[UserID],
	r.[Role], 
	r.[RoleID], 
	pn.[PageNavigationID],
	pn.[PageUrl]
FROM [dbo].[AdminLogins] al
LEFT OUTER JOIN [dbo].[Role] r on r.[Role] = 'AP'
	AND ISNULL(al.[IsAP], 0) = 1
LEFT OUTER JOIN [dbo].[PageNavigationRole] pnr on pnr.[RoleID] = r.[RoleID]
LEFT OUTER JOIN [dbo].[PageNavigation] pn on pn.[PageNavigationID] = pnr.[PageNavigationID]
WHERE pn.[PageNavigationID] IS NOT NULL

UNION

SELECT al.[UserID],
	r.[Role], 
	r.[RoleID], 
	pn.[PageNavigationID],
	pn.[PageUrl]
FROM [dbo].[AdminLogins] al
LEFT OUTER JOIN [dbo].[Role] r on r.[Role] = 'PrimaryAP'
	AND ISNULL(al.[IsPrimaryAP], 0) = 1
LEFT OUTER JOIN [dbo].[PageNavigationRole] pnr on pnr.[RoleID] = r.[RoleID]
LEFT OUTER JOIN [dbo].[PageNavigation] pn on pn.[PageNavigationID] = pnr.[PageNavigationID]
WHERE pn.[PageNavigationID] IS NOT NULL

UNION

SELECT al.[UserID],
	r.[Role], 
	r.[RoleID], 
	pn.[PageNavigationID],
	pn.[PageUrl]
FROM [dbo].[AdminLogins] al
LEFT OUTER JOIN [dbo].[Role] r on r.[Role] = 'PGM'
	AND ISNULL(al.[IsPGM], 0) = 1
LEFT OUTER JOIN [dbo].[PageNavigationRole] pnr on pnr.[RoleID] = r.[RoleID]
LEFT OUTER JOIN [dbo].[PageNavigation] pn on pn.[PageNavigationID] = pnr.[PageNavigationID]
WHERE pn.[PageNavigationID] IS NOT NULL


UNION

SELECT NULL AS [UserID],
	r.[Role], 
	r.[RoleID], 
	pn.[PageNavigationID],
	pn.[PageUrl]
FROM [dbo].[Role] r 
LEFT OUTER JOIN [dbo].[PageNavigationRole] pnr on pnr.[RoleID] = r.[RoleID]
LEFT OUTER JOIN [dbo].[PageNavigation] pn on pn.[PageNavigationID] = pnr.[PageNavigationID]
WHERE pn.[PageNavigationID] IS NOT NULL
	AND r.[Role] = 'Default'
