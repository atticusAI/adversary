﻿
CREATE VIEW [dbo].[AllRules]
AS
SELECT     dbo.MemoRules.MemoRuleID, dbo.MemoRules.TrackNumber, dbo.Rules.RuleText, dbo.Rules.RuleActive, dbo.Rules.RuleID
FROM         dbo.MemoRules INNER JOIN
                      dbo.Rules ON dbo.MemoRules.RuleID = dbo.Rules.RuleID
