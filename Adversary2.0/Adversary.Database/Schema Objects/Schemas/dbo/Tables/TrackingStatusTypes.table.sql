﻿CREATE TABLE [dbo].[TrackingStatusTypes] (
    [StatusTypeID]  INT            NOT NULL,
    [Status]        VARCHAR (100)  NULL,
    [StatusMessage] VARCHAR (2000) NULL
);

