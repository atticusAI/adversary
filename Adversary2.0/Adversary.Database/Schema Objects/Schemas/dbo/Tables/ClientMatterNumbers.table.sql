﻿CREATE TABLE [dbo].[ClientMatterNumbers] (
    [ClientMatterID]     INT            IDENTITY (1, 1) NOT NULL,
    [ScrMemID]           INT            NULL,
    [ClientMatterNumber] NVARCHAR (150) NULL
);

