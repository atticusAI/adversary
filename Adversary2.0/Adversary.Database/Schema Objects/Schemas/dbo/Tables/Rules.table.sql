﻿CREATE TABLE [dbo].[Rules] (
    [RuleID]     INT            IDENTITY (1, 1) NOT NULL,
    [RuleText]   NVARCHAR (500) NOT NULL,
    [RuleActive] BIT            NOT NULL
);

