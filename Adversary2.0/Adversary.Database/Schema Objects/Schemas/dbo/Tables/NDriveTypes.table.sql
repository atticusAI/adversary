﻿CREATE TABLE [dbo].[NDriveTypes] (
    [NDriveTypeID]          INT            IDENTITY (1, 1) NOT NULL,
    [NDriveTypeDescription] NVARCHAR (500) NULL
);

