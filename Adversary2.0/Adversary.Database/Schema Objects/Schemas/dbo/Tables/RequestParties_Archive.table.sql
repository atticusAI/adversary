﻿CREATE TABLE [dbo].[RequestParties_Archive] (
    [ArchiveID]         INT            IDENTITY (1, 1) NOT NULL,
    [PartyID]           INT            NOT NULL,
    [ReqID]             INT            NULL,
    [PartyName]         NVARCHAR (255) NULL,
    [PartyRelationship] NVARCHAR (255) NULL
);

