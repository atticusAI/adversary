﻿CREATE TABLE [dbo].[AdminPracCodes] (
    [UserID]          INT          NULL,
    [PracCode]        NVARCHAR (5) NULL,
    [IsPrimary]       BIT          NULL,
    [AdminPracCodeID] INT          IDENTITY (1, 1) NOT NULL
);

