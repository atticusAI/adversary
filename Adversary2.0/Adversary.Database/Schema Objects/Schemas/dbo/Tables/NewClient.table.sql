﻿CREATE TABLE [dbo].[NewClient] (
    [ScrMemID]            INT            NOT NULL,
    [NwClientID]          INT            IDENTITY (1, 1) NOT NULL,
    [CName]               NVARCHAR (255) NULL,
    [LName]               NVARCHAR (30)  NULL,
    [FName]               NVARCHAR (30)  NULL,
    [MName]               NVARCHAR (50)  NULL,
    [ContactName]         NVARCHAR (50)  NULL,
    [ContactTitle]        NVARCHAR (50)  NULL,
    [OriginationComments] NVARCHAR (MAX) NULL,
    [ClientHasAffiliates] BIT            NULL
);

