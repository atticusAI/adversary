﻿CREATE TABLE [dbo].[OpposingCounsel] (
    [opposingID]  INT            IDENTITY (1, 1) NOT NULL,
    [LawfirmName] NVARCHAR (150) NULL,
    [LawyerName]  NVARCHAR (50)  NULL,
    [ScrMemID]    INT            NULL
);

