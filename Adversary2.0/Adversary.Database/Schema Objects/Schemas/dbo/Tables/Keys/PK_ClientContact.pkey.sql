﻿ALTER TABLE [dbo].[ClientContact]
    ADD CONSTRAINT [PK_ClientContact] PRIMARY KEY CLUSTERED ([ContactID] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);

