﻿ALTER TABLE [dbo].[ClientAffiliates]
    ADD CONSTRAINT [PK_ClientAffiliates] PRIMARY KEY CLUSTERED ([ClientAffiliateID] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);

