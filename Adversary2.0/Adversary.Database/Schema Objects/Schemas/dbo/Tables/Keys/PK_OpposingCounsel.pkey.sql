﻿ALTER TABLE [dbo].[OpposingCounsel]
    ADD CONSTRAINT [PK_OpposingCounsel] PRIMARY KEY CLUSTERED ([opposingID] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);

