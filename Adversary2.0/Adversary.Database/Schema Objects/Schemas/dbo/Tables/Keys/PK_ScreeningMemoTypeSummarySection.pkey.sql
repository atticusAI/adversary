﻿ALTER TABLE [dbo].[ScreeningMemoTypeSummarySection]
    ADD CONSTRAINT [PK_ScreeningMemoTypeSummarySection] PRIMARY KEY CLUSTERED ([ScrMemTypeSummarySectionID] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);

