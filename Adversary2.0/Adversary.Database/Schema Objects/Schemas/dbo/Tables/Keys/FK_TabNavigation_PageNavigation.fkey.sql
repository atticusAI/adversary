﻿ALTER TABLE [dbo].[TabNavigation]
    ADD CONSTRAINT [FK_TabNavigation_PageNavigation] FOREIGN KEY ([ParentPageNavigationID]) REFERENCES [dbo].[PageNavigation] ([PageNavigationID]) ON DELETE NO ACTION ON UPDATE NO ACTION;

