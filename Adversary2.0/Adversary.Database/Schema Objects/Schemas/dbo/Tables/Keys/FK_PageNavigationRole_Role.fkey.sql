﻿ALTER TABLE [dbo].[PageNavigationRole]
    ADD CONSTRAINT [FK_PageNavigationRole_Role] FOREIGN KEY ([RoleID]) REFERENCES [dbo].[Role] ([RoleID]) ON DELETE NO ACTION ON UPDATE NO ACTION;

