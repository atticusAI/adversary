﻿ALTER TABLE [dbo].[ClientMatterNumbers]
    ADD CONSTRAINT [PK_ClientMatterNumbers] PRIMARY KEY CLUSTERED ([ClientMatterID] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);

