﻿CREATE TABLE [dbo].[MemoApplicationTrack] (
    [MemoApplicationTrackID] INT            IDENTITY (1, 1) NOT NULL,
    [MemoTypeTrackNumber]    INT            NULL,
    [PageOrder]              INT            NULL,
    [PageURL]                VARCHAR (2000) NULL,
    [Name]                   VARCHAR (50)   NULL,
    [Description]            VARCHAR (2000) NULL
);

