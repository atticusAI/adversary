﻿CREATE TABLE [dbo].[Attachments] (
    [AttachmentID] INT           IDENTITY (1, 1) NOT NULL,
    [ScrMemID]     INT           NULL,
    [Description]  VARCHAR (400) NULL,
    [FileName]     VARCHAR (200) NULL
);

