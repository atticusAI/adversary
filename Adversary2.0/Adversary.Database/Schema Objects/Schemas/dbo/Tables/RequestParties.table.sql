﻿CREATE TABLE [dbo].[RequestParties] (
    [PartyID]           INT            IDENTITY (400000, 1) NOT NULL,
    [ReqID]             INT            NULL,
    [PartyName]         NVARCHAR (255) NULL,
    [PartyRelationship] NVARCHAR (255) NULL
);

