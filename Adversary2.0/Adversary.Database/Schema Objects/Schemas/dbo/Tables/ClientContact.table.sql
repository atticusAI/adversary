﻿CREATE TABLE [dbo].[ClientContact] (
    [ContactID]    INT           IDENTITY (1, 1) NOT NULL,
    [ContactFName] NVARCHAR (50) NULL,
    [ContactMName] NVARCHAR (50) NULL,
    [ContactLName] NVARCHAR (50) NULL,
    [ContactTitle] NVARCHAR (50) NULL,
    [ScrMemID]     INT           NULL
);

