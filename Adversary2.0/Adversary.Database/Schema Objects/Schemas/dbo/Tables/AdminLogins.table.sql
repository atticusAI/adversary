﻿CREATE TABLE [dbo].[AdminLogins] (
    [UserID]           INT          IDENTITY (1, 1) NOT NULL,
    [Login]            VARCHAR (50) NULL,
    [Email]            VARCHAR (50) NULL,
    [Office]           INT          NULL,
    [IsAdversary]      BIT          NULL,
    [IsAdversaryAdmin] BIT          NULL,
    [IsAP]             BIT          NULL,
    [IsPrimaryAP]      BIT          NULL,
    [IsPGM]            BIT          NULL,
    [SortOrder]        INT          NULL,
    [IsFinancialAdmin] BIT          NULL
);

