﻿CREATE TABLE [dbo].[Notifications] (
    [NotificationID]        INT          IDENTITY (1, 1) NOT NULL,
    [TrackingID]            INT          NOT NULL,
    [ScrMemID]              INT          NOT NULL,
    [Acknowledged]          BIT          NULL,
    [SubmittedOn]           DATETIME     NULL,
    [Office]                INT          NULL,
    [PracCode]              NVARCHAR (5) NULL,
    [Notification1Sent]     BIT          NULL,
    [Notification1Date]     DATETIME     NULL,
    [Notification2Sent]     BIT          NULL,
    [Notification2Date]     DATETIME     NULL,
    [NotificationFinalSent] BIT          NULL,
    [NotificationFinalDate] DATETIME     NULL,
    [Notification3Sent]     BIT          NULL,
    [Notification3Date]     DATETIME     NULL
);

