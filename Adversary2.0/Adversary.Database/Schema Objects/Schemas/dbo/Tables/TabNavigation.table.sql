﻿CREATE TABLE [dbo].[TabNavigation] (
    [TabNavigationID]        INT IDENTITY (1, 1) NOT NULL,
    [ParentPageNavigationID] INT NOT NULL,
    [PageNavigationID]       INT NULL,
    [TabOrder]               INT NULL
);

