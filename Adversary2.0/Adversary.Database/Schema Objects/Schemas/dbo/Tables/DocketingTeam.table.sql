﻿CREATE TABLE [dbo].[DocketingTeam] (
    [ScrMemID]        INT           NULL,
    [PersonnelID]     NVARCHAR (50) NOT NULL,
    [DocketingTeamID] INT           IDENTITY (1, 1) NOT NULL
);

