﻿CREATE TABLE [dbo].[NDriveAccess] (
    [ScrMemID]       INT           NULL,
    [PersonnelID]    NVARCHAR (50) NOT NULL,
    [NDriveAccessID] INT           IDENTITY (1, 1) NOT NULL
);

