﻿CREATE TABLE [dbo].[Address] (
    [ScrMemID]  INT           NOT NULL,
    [AddressID] INT           IDENTITY (1, 1) NOT NULL,
    [Address1]  NVARCHAR (50) NULL,
    [Address2]  NVARCHAR (50) NULL,
    [Address3]  NVARCHAR (50) NULL,
    [City]      NVARCHAR (50) NULL,
    [State]     NVARCHAR (2)  NULL,
    [Zip]       NVARCHAR (12) NULL,
    [Country]   NVARCHAR (50) NULL,
    [Phone]     NVARCHAR (50) NULL,
    [AltPhone]  NVARCHAR (50) NULL,
    [email]     NVARCHAR (50) NULL,
    [Fax]       NVARCHAR (50) NULL
);

