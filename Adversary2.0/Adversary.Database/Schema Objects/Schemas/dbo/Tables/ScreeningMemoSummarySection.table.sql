﻿CREATE TABLE [dbo].[ScreeningMemoSummarySection] (
    [ScrMemSummarySectionID] INT            IDENTITY (1, 1) NOT NULL,
    [Name]                   VARCHAR (50)   NOT NULL,
    [Description]            VARCHAR (256)  NULL,
    [VirtualPath]            VARCHAR (2000) NOT NULL
);

