﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[TRG_OFFICE_PGROUP_CHANGE]
   ON  [dbo].[Memo]
   AFTER UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @SCRMEMID INT
	SELECT @SCRMEMID = ScrMemID FROM Inserted

    -- Insert statements for trigger here
    IF (UPDATE (PracCode)) OR (UPDATE (RespAttOfficeCode))
    BEGIN
		DECLARE @PRACCODE NVARCHAR(5)
		DECLARE @RESPATTOFFICECODE NVARCHAR(50)
		
		SELECT
			@PRACCODE = PracCode,
			@RESPATTOFFICECODE = RespAttOfficeCode
		FROM
			Inserted
			
		UPDATE Notifications SET
			Office = CAST(@RESPATTOFFICECODE as INT),
			PracCode = @PRACCODE
		WHERE
			ScrMemID = @SCRMEMID
	END
END

