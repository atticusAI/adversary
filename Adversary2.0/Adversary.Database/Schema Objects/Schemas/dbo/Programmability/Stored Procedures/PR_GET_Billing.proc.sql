﻿CREATE PROCEDURE [dbo].[PR_GET_Billing]
	@BillingID [int] = null,
	@ScrMemID [int] = null
AS
SELECT * FROM [dbo].[Billing] WHERE
 ((@BillingID is null) OR (BillingID = @BillingID)) AND 
 ((@ScrMemID is null) OR (ScrMemID = @ScrMemID))
