﻿CREATE PROCEDURE [dbo].[PR_SEARCH_ARApprovalUser]
	@ApprovalUserID [int] = null,
	@UserName [nvarchar](150) = null,
	@EmailAddress [nvarchar](100) = null
AS

SELECT * FROM [dbo].[ARApprovalUser] WHERE
 ((@ApprovalUserID is null) OR (ApprovalUserID = @ApprovalUserID)) AND 
 ((@UserName is null) OR (UserName = @UserName)) AND 
 ((@EmailAddress is null) OR (EmailAddress = @EmailAddress))
