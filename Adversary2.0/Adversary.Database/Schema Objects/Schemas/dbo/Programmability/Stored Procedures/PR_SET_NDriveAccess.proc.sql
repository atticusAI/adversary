﻿
/* SET PROC */

CREATE PROCEDURE [dbo].[PR_SET_NDriveAccess]
	@ScrMemID [int],
	@PersonnelID nvarchar(50)
AS
IF NOT EXISTS(SELECT * FROM NDriveAccess WHERE ScrMemID=@ScrMemID AND @PersonnelID=PersonnelID)
BEGIN
 INSERT INTO [dbo].[NDriveAccess] (
 [ScrMemID], [PersonnelID]
  )
  VALUES (
 @ScrMemID, @PersonnelID
  );

END

SELECT * FROM NDriveAccess WHERE ScrMemID=@ScrMemID AND @PersonnelID=PersonnelID
