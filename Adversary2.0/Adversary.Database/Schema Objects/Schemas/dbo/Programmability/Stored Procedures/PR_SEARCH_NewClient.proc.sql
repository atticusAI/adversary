﻿CREATE PROCEDURE [dbo].[PR_SEARCH_NewClient]
	@ScrMemID [int] = null,
	@NwClientID [int] = null,
	@CName [nvarchar](255) = null,
	@LName [nvarchar](30) = null,
	@FName [nvarchar](30) = null,
	@MName [nvarchar](50) = null,
	@ContactName [nvarchar](50) = null,
	@ContactTitle [nvarchar](50) = null,
	@OriginationComments [nvarchar](max) = null
AS
SELECT * FROM [dbo].[NewClient] WHERE
 ((@ScrMemID is null) OR (ScrMemID = @ScrMemID)) AND 
 ((@NwClientID is null) OR (NwClientID = @NwClientID)) AND 
 ((@CName is null) OR (CName = @CName)) AND 
 ((@LName is null) OR (LName = @LName)) AND 
 ((@FName is null) OR (FName = @FName)) AND 
 ((@MName is null) OR (MName = @MName)) AND 
 ((@ContactName is null) OR (ContactName = @ContactName)) AND 
 ((@ContactTitle is null) OR (ContactTitle = @ContactTitle)) AND 
 ((@OriginationComments is null) OR (OriginationComments = @OriginationComments))
