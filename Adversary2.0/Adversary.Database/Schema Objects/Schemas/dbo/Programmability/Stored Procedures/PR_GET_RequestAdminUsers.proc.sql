﻿CREATE PROCEDURE [dbo].[PR_GET_RequestAdminUsers]
	@AdminUserID [int] = null
AS
SELECT * FROM [dbo].[RequestAdminUsers] WHERE
 ((@AdminUserID is null) OR (AdminUserID = @AdminUserID))
