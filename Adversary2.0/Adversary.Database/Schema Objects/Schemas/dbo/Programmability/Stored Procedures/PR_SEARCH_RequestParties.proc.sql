﻿CREATE PROCEDURE [dbo].[PR_SEARCH_RequestParties]
	@PartyID [int] = null,
	@ReqID [int] = null,
	@PartyName [nvarchar](255) = null,
	@PartyRelationship [nvarchar](255) = null
AS
SELECT * FROM [dbo].[RequestParties] WHERE
 ((@PartyID is null) OR (PartyID = @PartyID)) AND 
 ((@ReqID is null) OR (ReqID = @ReqID)) AND 
 ((@PartyName is null) OR (PartyName = @PartyName)) AND 
 ((@PartyRelationship is null) OR (PartyRelationship = @PartyRelationship))
