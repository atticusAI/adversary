﻿CREATE PROCEDURE [dbo].[PR_SET_RequestAdminUsers]
	@AdminUserID [int],
	@AdminLoginName [nvarchar](50),
	@AdminEmail [nvarchar](50),
	@SortOrder [int]
AS
IF @AdminUserID < 1
BEGIN
 INSERT INTO [dbo].[RequestAdminUsers] (
 [AdminLoginName], [AdminEmail], [SortOrder]
  )
  VALUES (
 @AdminLoginName, @AdminEmail, @SortOrder
  );
SELECT @AdminUserID=@@IDENTITY;
END
ELSE
 UPDATE [dbo].[RequestAdminUsers]
 SET 
  [AdminLoginName]=@AdminLoginName, [AdminEmail]=@AdminEmail, [SortOrder]=@SortOrder
 WHERE AdminUserID=@AdminUserID;

SELECT * FROM [dbo].[RequestAdminUsers] WHERE AdminUserID=@AdminUserID;
