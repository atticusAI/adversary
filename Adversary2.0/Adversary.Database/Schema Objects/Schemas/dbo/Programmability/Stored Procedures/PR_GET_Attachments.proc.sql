﻿CREATE PROCEDURE [dbo].[PR_GET_Attachments]
	@AttachmentID [int] = null,
	@ScrMemID [int] = null
AS
SELECT * FROM [dbo].[Attachments] WHERE
 ((@AttachmentID is null) OR (AttachmentID = @AttachmentID)) AND 
 ((@ScrMemID is null) OR (ScrMemID = @ScrMemID))
