﻿CREATE PROCEDURE [dbo].[PR_SEARCH_ClientContact]
	@ContactID [int] = null,
	@ContactFName [nvarchar](50) = null,
	@ContactMName [nvarchar](50) = null,
	@ContactLName [nvarchar](50) = null,
	@ContactTitle [nvarchar](50) = null,
	@ScrMemID [int] = null
AS
SELECT * FROM [dbo].[ClientContact] WHERE
 ((@ContactID is null) OR (ContactID = @ContactID)) AND 
 ((@ContactFName is null) OR (ContactFName = @ContactFName)) AND 
 ((@ContactMName is null) OR (ContactMName = @ContactMName)) AND 
 ((@ContactLName is null) OR (ContactLName = @ContactLName)) AND 
 ((@ContactTitle is null) OR (ContactTitle = @ContactTitle)) AND 
 ((@ScrMemID is null) OR (ScrMemID = @ScrMemID))
