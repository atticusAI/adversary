﻿CREATE PROCEDURE [dbo].[PR_SEARCH_Billing]
	@ScrMemID [int] = null,
	@BillingID [int] = null,
	@SpecialFee [nvarchar](max) = null,
	@BillInstructions [nvarchar](max) = null,
	@BillFormat [nvarchar](50) = null,
	@ClientLevelBill [bit] = null,
	@LateInterest [bit] = null
AS
SELECT * FROM [dbo].[Billing] WHERE
 ((@ScrMemID is null) OR (ScrMemID = @ScrMemID)) AND 
 ((@BillingID is null) OR (BillingID = @BillingID)) AND 
 ((@SpecialFee is null) OR (SpecialFee = @SpecialFee)) AND 
 ((@BillInstructions is null) OR (BillInstructions = @BillInstructions)) AND 
 ((@BillFormat is null) OR (BillFormat = @BillFormat)) AND 
 ((@ClientLevelBill is null) OR (ClientLevelBill = @ClientLevelBill)) AND 
 ((@LateInterest is null) OR (LateInterest = @LateInterest))
