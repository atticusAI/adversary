﻿CREATE PROCEDURE [dbo].[PR_GET_ClientContact]
	@ContactID [int] = null,
	@ScrMemID [int] = null
AS
SELECT * FROM [dbo].[ClientContact] WHERE
 ((@ContactID is null) OR (ContactID = @ContactID)) AND 
 ((@ScrMemID is null) OR (ScrMemID = @ScrMemID))
