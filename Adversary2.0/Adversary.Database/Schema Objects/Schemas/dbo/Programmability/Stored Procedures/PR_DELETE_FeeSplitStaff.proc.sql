﻿CREATE PROCEDURE [dbo].[PR_DELETE_FeeSplitStaff]
	@ScrMemID [int],
	@PersonnelID [nvarchar](50)
AS
DELETE FROM [dbo].[FeeSplitStaff] 
WHERE ScrMemID=@ScrMemID AND PersonnelID=@PersonnelID
