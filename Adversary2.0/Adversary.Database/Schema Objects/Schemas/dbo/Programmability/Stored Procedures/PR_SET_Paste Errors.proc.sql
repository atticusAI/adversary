﻿CREATE PROCEDURE [dbo].[PR_SET_Paste Errors]
	@TrackNum [int],
	@PageID [int],
	@PageOrder [int],
	@MenuTitle [nvarchar](255),
	@TrackStart [bit]
AS
IF @PageID < 1
BEGIN
 INSERT INTO [dbo].[Paste Errors] (
 [TrackNum], [PageOrder], [MenuTitle], [TrackStart]
  )
  VALUES (
 @TrackNum, @PageOrder, @MenuTitle, @TrackStart
  );
SELECT @PageID=@@IDENTITY;
END
ELSE
 UPDATE [dbo].[Paste Errors]
 SET 
  [TrackNum]=@TrackNum, [PageOrder]=@PageOrder, [MenuTitle]=@MenuTitle, [TrackStart]=@TrackStart
 WHERE PageID=@PageID;

SELECT * FROM [dbo].[Paste Errors] WHERE PageID=@PageID;
