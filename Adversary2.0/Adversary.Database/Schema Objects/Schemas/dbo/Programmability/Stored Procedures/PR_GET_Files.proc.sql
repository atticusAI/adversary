﻿CREATE PROCEDURE [dbo].[PR_GET_Files]
	@FileID [int] = null,
	@ScrMemID [int] = null
AS
SELECT * FROM [dbo].[Files] WHERE
 ((@FileID is null) OR (FileID = @FileID)) AND 
 ((@ScrMemID is null) OR (ScrMemID = @ScrMemID))
