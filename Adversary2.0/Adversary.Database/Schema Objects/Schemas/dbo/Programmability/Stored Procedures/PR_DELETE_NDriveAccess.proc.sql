﻿

/* DELETE PROC */

CREATE PROCEDURE [dbo].[PR_DELETE_NDriveAccess]
	@ScrMemID [int],
	@PersonnelID nvarchar(50)
AS
DELETE FROM [dbo].[NDriveAccess] 
WHERE ScrMemID=@ScrMemID AND PersonnelID=@PersonnelID
