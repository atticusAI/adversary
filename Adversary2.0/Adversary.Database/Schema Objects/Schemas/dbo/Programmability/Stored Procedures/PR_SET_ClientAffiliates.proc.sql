﻿CREATE PROC PR_SET_ClientAffiliates
(
	@ClientAffiliateID INT,
	@CName varchar(255) = null,
	@LName varchar(50) = null,
	@FName varchar(50) = null,
	@MName varchar(50) = null,
	@ContactName nvarchar(50) = null,
	@ContactTitle nvarchar(50) = null,
	@RelationshipCode nvarchar(50) = null,
	@NwClientID INT
)
AS

IF (@ClientAffiliateID < 1)
BEGIN
	INSERT INTO [Adversary].[dbo].[ClientAffiliates]
           ([CName]
           ,[LName]
           ,[FName]
           ,[MName]
           ,[ContactName]
           ,[ContactTitle]
           ,[RelationshipCode]
           ,[NwClientID])
     VALUES
           (@CName
           ,@LName
           ,@FName
           ,@MName
           ,@ContactName
           ,@ContactTitle
           ,@RelationshipCode
           ,@NwClientID)
           SELECT @ClientAffiliateID = @@IDENTITY
END
ELSE
BEGIN
	UPDATE [Adversary].[dbo].[ClientAffiliates]
   SET [CName] = @CName
      ,[LName] = @LName
      ,[FName] = @FName
      ,[MName] = @MName
      ,[ContactName] = @ContactName
      ,[ContactTitle] = @ContactTitle
      ,[RelationshipCode] = @RelationshipCode
 WHERE ClientAffiliateID = @ClientAffiliateID
 
 SELECT * FROM [dbo].[ClientAffiliates] WHERE ClientAffiliateID = @ClientAffiliateID
END
