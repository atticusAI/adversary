﻿


CREATE PROCEDURE [dbo].[PR_SEARCH_Party]
	@ScrMemID [int] = null,
	@PartyType [nvarchar](50) = null,
	@PartyFName [nvarchar](MAX) = null,
	@PartyMName [nvarchar](MAX) = null,
	@PartyLName [nvarchar](MAX) = null,
	@PartyOrganization [nvarchar](MAX) = null,
	@PartyRelationshipName [nvarchar](MAX) = null,
	@PartyRelationshipCode [nvarchar](50) = null,
	@PartyID [int] = null,
	@PC [int] = null
AS
SELECT * FROM [dbo].[Party] WHERE
 ((@ScrMemID is null) OR (ScrMemID = @ScrMemID)) AND 
 ((@PartyType is null) OR (PartyType = @PartyType)) AND 
 ((@PartyFName is null) OR (PartyFName = @PartyFName)) AND 
 ((@PartyMName is null) OR (PartyMName = @PartyMName)) AND 
 ((@PartyLName is null) OR (PartyLName = @PartyLName)) AND 
 ((@PartyOrganization is null) OR (PartyOrganization = @PartyOrganization)) AND 
 ((@PartyRelationshipName is null) OR (PartyRelationshipName = @PartyRelationshipName)) AND 
 ((@PartyRelationshipCode is null) OR (PartyRelationshipCode = @PartyRelationshipCode)) AND 
 ((@PartyID is null) OR (PartyID = @PartyID)) AND 
 ((@PC is null) OR (PC = @PC))

