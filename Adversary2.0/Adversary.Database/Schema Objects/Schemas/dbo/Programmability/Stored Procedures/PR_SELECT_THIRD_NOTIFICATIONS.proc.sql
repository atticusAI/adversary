﻿


CREATE proc [dbo].[PR_SELECT_THIRD_NOTIFICATIONS] (@MINUTESELAPSED INT)
AS

SET NOCOUNT ON
SET FMTONLY OFF

--get notifications for NOT denver.
DECLARE @NOTIFICATIONS TABLE
(
	NOTIFICATION_ID INT,
	TRACKING_ID INT,
	SCRMEM_ID INT,
	SUBMITTEDON DATETIME,
	ELAPSEDTIMEHOURS INT,
	OFFICE INT,
	PRACCODE NVARCHAR(5),
	EMAIL VARCHAR(50),
	STATUSTYPEID INT,
	SCRMEMTYPE INT,
	SQLPART INT,
	USERID VARCHAR(100)
)

DECLARE @NOW DATETIME
SET @NOW = GETDATE()

INSERT @NOTIFICATIONS
SELECT
	N.NotificationID,
	N.TrackingID,
	N.ScrMemID,
	N.SubmittedOn,
	DATEDIFF(n, N.Notification1Date, @NOW),
	N.Office,
	N.PracCode,
	AL.Email,
	T.StatusTypeID,
	M.ScrMemType,
	1,
	AL.[Login]
FROM
	Notifications N
	LEFT JOIN
	Tracking T
	ON N.TrackingID = T.TrackingID
	LEFT JOIN
	Memo M
	ON N.ScrMemID = M.ScrMemID
	LEFT JOIN
	AdminLogins AL
	ON M.OrigOffice = AL.Office
WHERE
	T.StatusTypeID = 1
	AND
	M.ScrMemType <> 6
	AND
	AL.IsPrimaryAP = 1
	AND
	DATEDIFF(n, N.Notification1Date, @NOW) >= @MINUTESELAPSED
	AND
	ISNULL(N.Notification2Sent, 0) = 0
	AND
	ISNULL(N.Notification1Sent, 0) = 1
	AND
	ISNULL(N.Acknowledged, 0) = 0
	AND
	ISNULL(T.APApproved, 0) = 0
	AND
	ISNULL(T.APAcknowledged, 0) = 0
	AND
	M.OrigOffice <> '10'
	

--insert the Denver notifications which are sent to a PGM
INSERT @NOTIFICATIONS
SELECT
	N.NotificationID,
	N.TrackingID,
	N.ScrMemID,
	N.SubmittedOn,
	DATEDIFF(n, N.Notification1Date, @NOW),
	M.OrigOffice,
	N.PracCode,
	AL.Email,
	T.StatusTypeID,
	M.ScrMemType,
	2,
	AL.[Login]
FROM
	Notifications N
	LEFT JOIN
	Tracking T
	ON N.TrackingID = T.TrackingID
	LEFT JOIN
	Memo M
	ON T.ScrMemID = M.ScrMemID
	LEFT JOIN
	AdminPracCodes APC
	ON M.PracCode = APC.PracCode
	LEFT JOIN
	AdminLogins AL 
	ON APC.UserID = AL.UserID
WHERE
	T.StatusTypeID = 1
	AND
	M.ScrMemType <> 6
	AND
	ISNULL(N.Acknowledged,0) = 0
	AND
	DATEDIFF(n, N.Notification1Date, @NOW) >= @MINUTESELAPSED
	AND
	ISNULL(N.Notification1Sent, 0) = 1
	AND
	ISNULL(N.Notification2Sent, 0) = 0
	AND
	ISNULL(T.PGMApproved, 0) = 0
	AND
	ISNULL(T.PGMAcknowledged, 0) = 0
	AND
	APC.IsPrimary = 1






	
SELECT
	NOTIFICATION_ID,
	TRACKING_ID,
	SCRMEM_ID,
	SUBMITTEDON,
	ELAPSEDTIMEHOURS,
	OFFICE,
	PRACCODE,
	EMAIL,
	STATUSTYPEID,
	SQLPART,
	USERID
FROM
	@NOTIFICATIONS
ORDER BY
	NOTIFICATION_ID,
	TRACKING_ID,
	SQLPART



