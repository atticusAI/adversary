﻿CREATE PROCEDURE [dbo].[PR_SEARCH_ProBonoType]
	@ProBonoTypeID [int] = null,
	@Name [nvarchar](50) = null
AS
SELECT * FROM [dbo].[ProBonoType] WHERE
 ((@ProBonoTypeID is null) OR (ProBonoTypeID = @ProBonoTypeID)) AND 
 ((@Name is null) OR (Name = @Name))
