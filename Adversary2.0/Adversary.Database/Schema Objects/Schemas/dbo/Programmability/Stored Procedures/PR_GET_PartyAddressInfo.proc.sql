﻿CREATE PROCEDURE [dbo].[PR_GET_PartyAddressInfo]
	@PartyAddressID [int] = null,
	@PartyID [int] = null
AS
SELECT * FROM [dbo].[PartyAddressInfo] WHERE
 ((@PartyAddressID is null) OR (PartyAddressID = @PartyAddressID)) AND 
 ((@PartyID is null) OR (PartyID = @PartyID))
