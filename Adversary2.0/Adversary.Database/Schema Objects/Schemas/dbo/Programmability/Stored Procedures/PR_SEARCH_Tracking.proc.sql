﻿

CREATE PROCEDURE [dbo].[PR_SEARCH_Tracking]
	@TrackingID [int] = null,
	@ScrMemID [int] = null,
	@StatusTypeID [int] = null,
	@Locked [bit] = null,
	@SubmittedOn [smalldatetime] = null,
	@Notes [varchar](MAX)  = null,
	@RetainerAcknowledged bit  = null,
	@APSignature [varchar](50) = null,
	@APDate [smalldatetime] = null,
	@APNotes [varchar](5000) = null,
	@APARSignature [varchar](50) = null,
	@APExceptionSignature [varchar](50) = null,
	@PGMSignature [varchar](50) = null,
	@PGMDate [smalldatetime] = null,
	@PGMNotes [varchar](5000) = null,
	@PGMARSignature [varchar](50) = null,
	@PGMExceptionSignature [varchar](50) = null,
	--@ClientMatterNumber [varchar](50) = null,
	@RejectionHold [bit] = null,
	@Opened [varchar](MAX) = null,
	@Conflicts [varchar](5000) = null,
	@FeeSplits [varchar](5000) = null,
	@FinalCheck [varchar](5000) = null,
	@AdversarySignature [varchar](50) = null,
	@AdversaryAdminDate [smalldatetime] = null,
    @APUserID [int] = null,
	@PGMUserID [int] = null,
	@AdversaryUserID [int] = null,
	@CMNumbersSentDate [smalldatetime] = null
AS
SELECT * FROM [dbo].[Tracking] WHERE
 ((@TrackingID is null) OR (TrackingID = @TrackingID)) AND 
 ((@ScrMemID is null) OR (ScrMemID = @ScrMemID)) AND 
 ((@StatusTypeID is null) OR (StatusTypeID = @StatusTypeID)) AND 
 ((@Locked is null) OR (Locked = @Locked)) AND 
 ((@SubmittedOn is null) OR (SubmittedOn = @SubmittedOn)) AND
 ((@Notes is null) OR (Notes = @Notes)) AND
 ((@RetainerAcknowledged is null) OR (RetainerAcknowledged = @RetainerAcknowledged)) AND
 ((@APSignature is null) OR (APSignature = @APSignature)) AND 
 ((@APDate is null) OR (APDate = @APDate)) AND 
 ((@APNotes is null) OR (APNotes = @APNotes)) AND 
 ((@APARSignature is null) OR (APARSignature = @APARSignature)) AND 
 ((@APExceptionSignature is null) OR (APExceptionSignature = @APExceptionSignature)) AND 
 ((@PGMSignature is null) OR (PGMSignature = @PGMSignature)) AND 
 ((@PGMDate is null) OR (PGMDate = @PGMDate)) AND 
 ((@PGMNotes is null) OR (PGMNotes = @PGMNotes)) AND 
 ((@PGMARSignature is null) OR (PGMARSignature = @PGMARSignature)) AND 
 ((@PGMExceptionSignature is null) OR (PGMExceptionSignature = @PGMExceptionSignature)) AND 
 ((@AdversarySignature is null) OR (AdversarySignature = @AdversarySignature)) AND 
 ((@AdversaryAdminDate is null) OR (AdversaryAdminDate = @AdversaryAdminDate)) AND 
 --((@ClientMatterNumber is null) OR (ClientMatterNumber like @ClientMatterNumber + '%')) AND 
 ((@RejectionHold is null) OR (RejectionHold = @RejectionHold)) AND 
 ((@Opened is null) OR (Opened = @Opened)) AND 
 ((@Conflicts is null) OR (Conflicts = @Conflicts)) AND 
 ((@FeeSplits is null) OR (FeeSplits = @FeeSplits)) AND 
 ((@FinalCheck is null) OR (FinalCheck = @FinalCheck)) AND 
 ((@APUserID is null) OR (APUserID = @APUserID)) AND 
 ((@PGMUserID is null) OR (PGMUserID = @PGMUserID)) AND 
 ((@AdversaryUserID is null) OR (AdversaryUserID = @AdversaryUserID)) AND
 ((@CMNumbersSentDate is null) OR (CMNumbersSentDate = @CMNumbersSentDate))


