﻿CREATE PROCEDURE [dbo].[PR_GET_Paste Errors]
	@PageID [int] = null
AS
SELECT * FROM [dbo].[Paste Errors] WHERE
 ((@PageID is null) OR (PageID = @PageID))
