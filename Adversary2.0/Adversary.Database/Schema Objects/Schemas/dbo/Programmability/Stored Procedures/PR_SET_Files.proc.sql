﻿CREATE PROCEDURE [dbo].[PR_SET_Files]
	@FileID [int],
	@ScrMemID [int],
	@FileType [nvarchar](50),
	@FileLabel [nvarchar](max)
AS
IF @FileID < 1
BEGIN
 INSERT INTO [dbo].[Files] (
 [ScrMemID], [FileType], [FileLabel]
  )
  VALUES (
 @ScrMemID, @FileType, @FileLabel
  );
SELECT @FileID=@@IDENTITY;
END
ELSE
 UPDATE [dbo].[Files]
 SET 
  [ScrMemID]=@ScrMemID, [FileType]=@FileType, [FileLabel]=@FileLabel
 WHERE FileID=@FileID;

SELECT * FROM [dbo].[Files] WHERE FileID=@FileID;
