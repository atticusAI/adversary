﻿CREATE PROCEDURE [dbo].[PR_SEARCH_Files]
	@FileID [int] = null,
	@ScrMemID [int] = null,
	@FileType [nvarchar](50) = null,
	@FileLabel [nvarchar](max) = null
AS
SELECT * FROM [dbo].[Files] WHERE
 ((@FileID is null) OR (FileID = @FileID)) AND 
 ((@ScrMemID is null) OR (ScrMemID = @ScrMemID)) AND 
 ((@FileType is null) OR (FileType = @FileType)) AND 
 ((@FileLabel is null) OR (FileLabel = @FileLabel))
