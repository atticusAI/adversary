﻿CREATE PROCEDURE [dbo].[PR_SET_AdminPracCodes]
	@UserID [int],
	@PracCode [nvarchar](5),
	@IsPrimary [bit]
AS

IF NOT EXISTS(SELECT * FROM [dbo].[AdminPracCodes] WHERE UserID=@UserID AND PracCode=@PracCode)
BEGIN
 INSERT INTO [dbo].[AdminPracCodes] (
 [UserID], [PracCode], [IsPrimary]
  )
  VALUES (
 @UserID, @PracCode, @IsPrimary
  );
END

--SELECT * FROM [dbo].[AdminPracCodes] WHERE UserID=@UserID;
