﻿CREATE PROCEDURE [dbo].[PR_SET_OpposingCounsel]
	@opposingID [int],
	@LawfirmName [nvarchar](150),
	@LawyerName [nvarchar](50),
	@ScrMemID [int]
AS
IF @opposingID < 1
BEGIN
 INSERT INTO [dbo].[OpposingCounsel] (
 [LawfirmName], [LawyerName], [ScrMemID]
  )
  VALUES (
 @LawfirmName, @LawyerName, @ScrMemID
  );
SELECT @opposingID=@@IDENTITY;
END
ELSE
 UPDATE [dbo].[OpposingCounsel]
 SET 
  [LawfirmName]=@LawfirmName, [LawyerName]=@LawyerName, [ScrMemID]=@ScrMemID
 WHERE opposingID=@opposingID;

SELECT * FROM [dbo].[OpposingCounsel] WHERE opposingID=@opposingID;
