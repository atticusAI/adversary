﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PR_SET_FINAL_NOTIFICATION]
	@NOTIFICATIONID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE NOTIFICATIONS SET
		NotificationFinalSent = 1,
		NotificationFinalDate = GETDATE()
	WHERE
		NotificationID = @NOTIFICATIONID
END

