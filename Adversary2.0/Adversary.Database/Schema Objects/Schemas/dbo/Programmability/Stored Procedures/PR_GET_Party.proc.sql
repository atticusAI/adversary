﻿CREATE PROCEDURE [dbo].[PR_GET_Party]
	@PartyID [int] = null,
	@ScrMemID [int] = null
AS
SELECT * FROM [dbo].[Party] WHERE
 ((@PartyID is null) OR (PartyID = @PartyID)) AND 
 ((@ScrMemID is null) OR (ScrMemID = @ScrMemID))
