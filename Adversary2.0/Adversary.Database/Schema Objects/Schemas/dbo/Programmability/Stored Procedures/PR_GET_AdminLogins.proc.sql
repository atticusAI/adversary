﻿CREATE PROCEDURE [dbo].[PR_GET_AdminLogins]
	@UserID [int] = null
AS
SELECT * FROM [dbo].[AdminLogins] WHERE
 ((@UserID is null) OR (UserID = @UserID))
  ORDER BY [Login]
