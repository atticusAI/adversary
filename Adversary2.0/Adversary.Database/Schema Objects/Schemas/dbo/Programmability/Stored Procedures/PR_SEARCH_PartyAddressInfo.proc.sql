﻿CREATE PROCEDURE [dbo].[PR_SEARCH_PartyAddressInfo]
	@PartyAddressID [int] = null,
	@PartyID [int] = null,
	@Address_Line_1 [nvarchar](50) = null,
	@Address_Line_2 [nvarchar](50) = null,
	@Address_Line_3 [nvarchar](50) = null,
	@City [nvarchar](50) = null,
	@State [nvarchar](50) = null,
	@Zip [nvarchar](50) = null,
	@Country [nvarchar](50) = null,
	@Phone [nvarchar](50) = null,
	@Fax [nvarchar](50) = null,
	@EMail [nvarchar](50) = null
AS
SELECT * FROM [dbo].[PartyAddressInfo] WHERE
 ((@PartyAddressID is null) OR (PartyAddressID = @PartyAddressID)) AND 
 ((@PartyID is null) OR (PartyID = @PartyID)) AND 
 ((@Address_Line_1 is null) OR (Address_Line_1 = @Address_Line_1)) AND 
 ((@Address_Line_2 is null) OR (Address_Line_2 = @Address_Line_2)) AND 
 ((@Address_Line_3 is null) OR (Address_Line_3 = @Address_Line_3)) AND 
 ((@City is null) OR (City = @City)) AND 
 ((@State is null) OR (State = @State)) AND 
 ((@Zip is null) OR (Zip = @Zip)) AND 
 ((@Country is null) OR (Country = @Country)) AND 
 ((@Phone is null) OR (Phone = @Phone)) AND 
 ((@Fax is null) OR (Fax = @Fax)) AND 
 ((@EMail is null) OR (EMail = @EMail))
