﻿

CREATE proc [dbo].[PR_SET_FOURTH_NOTIFICATION_SENT]
(@NOTIFICATION_ID INT)
AS

UPDATE dbo.Notifications SET
	Notification3Sent = 1,
	Notification3Date = GETDATE()
WHERE
	NotificationID = @NOTIFICATION_ID


