﻿CREATE PROCEDURE [dbo].[PR_SEARCH_Paste Errors]
	@TrackNum [int] = null,
	@PageID [int] = null,
	@PageOrder [int] = null,
	@MenuTitle [nvarchar](255) = null,
	@TrackStart [bit] = null
AS
SELECT * FROM [dbo].[Paste Errors] WHERE
 ((@TrackNum is null) OR (TrackNum = @TrackNum)) AND 
 ((@PageID is null) OR (PageID = @PageID)) AND 
 ((@PageOrder is null) OR (PageOrder = @PageOrder)) AND 
 ((@MenuTitle is null) OR (MenuTitle = @MenuTitle)) AND 
 ((@TrackStart is null) OR (TrackStart = @TrackStart))
