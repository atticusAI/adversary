﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel;

namespace Adversary.Utils
{
    public static class ExtensionMethods
    {
        

        public static bool IsNumeric(this string s)
        {
            float output;
            return float.TryParse(s, out output);
        }

        public static string ToDescription(this Enum value)
        {
            DescriptionAttribute[] da =
                (DescriptionAttribute[])
                (value.GetType().GetField(value.ToString())
                .GetCustomAttributes(typeof(DescriptionAttribute), false));
            return da.Length > 0 ? da[0].Description : value.ToString();
        }
    }

    public static class TemplateExtensionMethods
    {
        public class EmbedTemplate : TemplateControl
        {
            private object _data;

            public EmbedTemplate(object data)
            {
                _data = data;
            }

            public object GetEmbeddedData()
            {
                return _data;
            }
        }

        public static void SetDataItem(this WebControl c, object data)
        {
            c.TemplateControl = new EmbedTemplate(data);
        }

        public static object GetDataItem(this WebControl c)
        {
            if (c.TemplateControl == null)
                return null;
            if (!(c.TemplateControl is EmbedTemplate))
                return null;

            return ((EmbedTemplate)c.TemplateControl).GetEmbeddedData();
        }
    }
}