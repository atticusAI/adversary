﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.Utils
{
    public static class StringUtils
    {

        /// <summary>
        /// TODO: move this out of Adversary.Utils and into the datalayer, probably more appropriate there, or even Adversary.Screening
        /// Done 5/14/12 AR
        /// </summary>
        //public static string[] ProBonoPracticeCodes = { "101X", "105X" };
        //public static string[] AffiliatePartyCodes = { "102" };
        //public static string[] ReferringPartyCodes = { "902" };
        //public static string[] RelatedPartyCodes = { "101", "102", "104", "900", "211", "205", "206", "207", "204", "212", "219", "208", "222", "202", "210", "203", "201" };
        //public static string[] AdversePartyCodes = { "300", "311", "306", "307", "308", "303", "302", "301", "999" };
        //public static string[] UnusedPracticeCodes = { "180L", "181L", "182L" };
        public const string TWO_LINE_FEED_STRING = "\r\n\r\n";

        public const string JAVASCRIPT_TAG_STARTER = "<script type='text/javascript'>";

        public static string ParseNameFromAutoCompleteExtenderItem(string input)
        {
            string employeeName = "";
            string parsedServiceReturn = string.Empty;
            parsedServiceReturn = input
                                .Replace("}", string.Empty)
                                .Replace("{", string.Empty)
                                .Replace("\"", string.Empty)
                                .Split(':')[1];
            string[] nameParts = parsedServiceReturn.Split(',');
            employeeName = nameParts[0] + ", " + nameParts[1];
            return employeeName;
        }

        /// <summary>
        /// This accepts a string like Lname, Fname MInit. (NNNN) and returns
        /// a string array which is in this order: ID - First Name - Middle Name -
        /// Last Name
        /// </summary>
        /// <param name="input">a string put together by the autocomplete Lname, Fname Minit. (NNNN)</param>
        /// <returns>an array: ID - First Name - Middle Name/Init - Last Name</returns>
        public static string[] ParseNameFromAutoCompleteText(string input)
        {
            string[] returnString;
            input = input.Trim();
            int firstParen = input.IndexOf('(');
            string _personnelID = input.Substring(firstParen + 1, 4);
            string _fullname = input.Substring(0, firstParen);
            int comma = _fullname.IndexOf(',');
            string _lastname = _fullname.Substring(0, comma);
            string[] _firstMiddle = _fullname.Substring(comma, _fullname.Length - comma).Replace(",", "").Trim().Split(' ');
            string _firstname = _firstMiddle[0];
            string _middlename = string.Empty;
            if(_firstMiddle.Length > 1)
                 _middlename = _firstMiddle[1];
            returnString = new string[] { _personnelID, _firstname, _middlename, _lastname };
            return returnString;
        }

        public static Dictionary<string, string> MIMEExtensionsAndTypes()
        {
            return new Dictionary<string, string>() {
                {"doc", "application/msword"},
                {"docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"},
                {"xls", "application/vnd.ms-excel"},  
                {"xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
                {"pdf", "application/pdf"},
                {"msg", "application/vnd.ms-outlook"},
                {"nrl","application/octet-stream"},
                {"mp3","audio/mpeg3"},
                {"tif","image/tiff"},
                {"txt","text/plain"},
                {"log","text/plain"}
            };

        }
    }
}
