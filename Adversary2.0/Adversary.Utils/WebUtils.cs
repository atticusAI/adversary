﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Adversary.Utils
{
    public static class WebUtils
    {
        public static bool TestMode
        {
            get
            {
                bool testMode = false;
                if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["TestMode"]))
                {
                    Boolean.TryParse(ConfigurationManager.AppSettings["TestMode"], out testMode);
                }
                return testMode;
            }
        }
    }
}
