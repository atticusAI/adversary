﻿using Adversary.DataLayer;
using Adversary.Utils;
using AjaxControlToolkit;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Collections.Generic;

namespace Adversary.Controls
{
    public class UploadEventArgs : System.EventArgs
    {
        public string FileName { get; set; }
        public string StatusMessage { get; set; }
        public string FileSize { get; set; }
        public string FileDescription { get; set; }
        public string FileUploadError { get; set; }
        public bool HasUploadError { get; set; }
    }

   
    //[ToolboxData("<{0}:FileUploadControl runat=server></{0}:FileUploadControl>")]
    //public class FileUploadControl : UserControl, INamingContainer
    //{
    //    public delegate void UploadCompletedEventHandler(object sender, UploadEventArgs e);
    //    public event UploadCompletedEventHandler UploadCompleted;

    //    public Label lblFileDescriptionCaption;
    //    public Label lblFileToUploadCaption;
    //    public TextBox tbFileDescription;
    //    public AjaxControlToolkit.AsyncFileUpload asFileUpload;
    //    public Table tblFileUpload;
    //    public Panel pnlThrobber;
    //    public Image imgThrobber;
    //    public Label lblThrobber;

    //    private FileUploadMode _ControlFileUploadMode;

    //    public FileUploadMode ControlFileUploadMode
    //    {
    //        get { return _ControlFileUploadMode; }
    //        set { _ControlFileUploadMode = value; }
    //    }

    //    private string _ThrobberImageUrl;

    //    public string ThrobberImageUrl
    //    {
    //        get { return _ThrobberImageUrl; }
    //        set { _ThrobberImageUrl = value; }
    //    }



    //    public enum FileUploadMode
    //    {
    //        [Description("")]
    //        None,
    //        [Description("Engagement Letter")]
    //        EngagementLetter,
    //        [Description("Conflict Report")]
    //        ConflictReport
    //    }

    //    protected override void CreateChildControls()
    //    {
    //        EnsureChildControls();
    //        base.CreateChildControls();

    //        lblFileDescriptionCaption = new Label();
    //        lblFileToUploadCaption = new Label();
    //        tbFileDescription = new TextBox();
    //        tblFileUpload = new Table();
    //        asFileUpload = new AsyncFileUpload();

    //        lblFileDescriptionCaption.Text = "File Description";
    //        lblFileToUploadCaption.Text = "File to Upload";

    //        imgThrobber = new Image();
    //        imgThrobber.ImageUrl = _ThrobberImageUrl;

    //        lblThrobber = new Label();
    //        lblThrobber.Text = "Uploading file. Large files may require some extra time to complete.";

    //        pnlThrobber = new Panel();
    //        pnlThrobber.Controls.Add(imgThrobber);
    //        pnlThrobber.Controls.Add(lblThrobber);

    //        asFileUpload.UploadedComplete += new System.EventHandler<AsyncFileUploadEventArgs>(asFileUpload_UploadedComplete);
    //        asFileUpload.CompleteBackColor = System.Drawing.Color.LightGreen;

    //        tblFileUpload.Width = Unit.Percentage(50);
    //        tblFileUpload.CellPadding = 2;
    //        tblFileUpload.CellSpacing = 2;
    //        tblFileUpload.BorderColor = System.Drawing.Color.Black;
    //        tblFileUpload.BorderStyle = BorderStyle.Solid;
    //        tblFileUpload.BorderWidth = Unit.Pixel(2);

    //        TableRow tr;
    //        TableCell tc;

    //        tr = new TableRow();

    //        tc = new TableCell();
    //        tc.Controls.Add(lblFileDescriptionCaption);
    //        tr.Cells.Add(tc);

    //        tc = new TableCell();
    //        string fileDescription = _ControlFileUploadMode.ToDescription();
    //        if (fileDescription.Length > 0)
    //        {
    //            tbFileDescription.Enabled = false;
    //        }
    //        tbFileDescription.Text = fileDescription;
    //        tc.Controls.Add(tbFileDescription);
    //        tr.Cells.Add(tc);

    //        tblFileUpload.Rows.Add(tr);

    //        tr = new TableRow();

    //        tc = new TableCell();
    //        tc.Controls.Add(lblFileToUploadCaption);
    //        tr.Cells.Add(tc);

    //        tc = new TableCell();
    //        tc.Controls.Add(asFileUpload);
    //        tr.Cells.Add(tc);

    //        tblFileUpload.Rows.Add(tr);

    //        tr = new TableRow();
    //        tc = new TableCell();
    //        tc.ColumnSpan = 2;
    //        tc.Controls.Add(pnlThrobber);
    //        tr.Cells.Add(tc);
            
    //        tblFileUpload.Rows.Add(tr);

    //        //Controls.Add(lblFileDescriptionCaption);
    //        //Controls.Add(lblFileToUploadCaption);
    //        //Controls.Add(asFileUpload);
    //        //Controls.Add(tbFileDescription);
    //        Controls.Add(tblFileUpload);


    //    }

    //    protected override void Render(HtmlTextWriter writer)
    //    {
    //        asFileUpload.ThrobberID = pnlThrobber.ClientID;
    //        base.Render(writer);
    //    }

    //    void asFileUpload_UploadedComplete(object sender, AsyncFileUploadEventArgs e)
    //    {
    //        UploadEventArgs uArgs = new UploadEventArgs();
    //        uArgs.FileName = e.FileName;
    //        uArgs.FileSize = e.FileSize;
    //        uArgs.FileDescription = tbFileDescription.Text;
    //        asFileUpload.SaveAs("C:\\THISISJUSTATEST.txt");
    //        UploadCompleted(this, uArgs);
    //    }
    //}
}
