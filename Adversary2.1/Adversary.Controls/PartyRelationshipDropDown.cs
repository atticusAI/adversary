﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using Adversary.DataLayer;
using Adversary.Utils;

namespace Adversary.Controls
{
    [ToolboxData("<{0}:PartyRelationshipDropDown runat=server></{0}:PartyRelationshipDropDown>")]
    public class PartyRelationshipDropDown : UserControl, INamingContainer
    {
        public DropDownList ddlPartyRelationship;

        [Bindable(true)]
        public enum PartyRelationshipMode
        {
            AffiliateParties,
            RelatedParties,
            AdverseParties
        }

        private PartyRelationshipMode _RelationshipDisplayMode;
        public PartyRelationshipMode RelationshipDisplayMode
        {
            get { return _RelationshipDisplayMode; }
            set { _RelationshipDisplayMode = value; }
        }

        public PartyRelationshipDropDown()
        {
            ddlPartyRelationship = new DropDownList();
            ddlPartyRelationship.DataBound += new EventHandler(ddlPartyRelationship_DataBound);
        }

        void ddlPartyRelationship_DataBound(object sender, EventArgs e)
        {
            
        }

        protected override void CreateChildControls()
        {
            EnsureChildControls();
            ddlPartyRelationship = new DropDownList();
            BindPartyData();
            Controls.Add(ddlPartyRelationship);
        }

        private void BindPartyData()
        {
            List<DataLayer.AdversaryDataService.PartyStatus> partyList = new List<DataLayer.AdversaryDataService.PartyStatus>();
            using (AdversaryData d = new AdversaryData())
            {
                partyList = d.GetPartyStatusList();
            }

            
            switch (_RelationshipDisplayMode)
            {
                case PartyRelationshipMode.AffiliateParties:
                    //partyList = partyList.Where(P => Utils.StringUtils.AffiliatePartyCodes.Contains(P.STATUS_CODE)).ToList();
                    partyList = partyList.Where(P => Adversary.DataLayer.AdversaryData.AffiliatePartyCodes.Contains(P.STATUS_CODE)).ToList();
                    break;
                case PartyRelationshipMode.RelatedParties:
                    //partyList = partyList.Where(P => Utils.StringUtils.RelatedPartyCodes.Contains(P.STATUS_CODE)).ToList();
                    partyList = partyList.Where(P => Adversary.DataLayer.AdversaryData.RelatedPartyCodes.Contains(P.STATUS_CODE)).ToList();
                    break;
                case PartyRelationshipMode.AdverseParties:
                    //partyList = partyList.Where(P => Utils.StringUtils.AdversePartyCodes.Contains(P.STATUS_CODE)).ToList();
                    partyList = partyList.Where(P => Adversary.DataLayer.AdversaryData.AdversePartyCodes.Contains(P.STATUS_CODE)).ToList();
                    break;
                default:
                    break;
            }

            this.ddlPartyRelationship.DataTextField = "DESCRIPTION";
            this.ddlPartyRelationship.DataValueField = "STATUS_CODE";
            partyList = partyList.OrderBy(P => P.DESCRIPTION).ToList();
            partyList.Reverse();
            foreach (DataLayer.AdversaryDataService.PartyStatus partyStatus in partyList)
            {
                ddlPartyRelationship.Items.Insert(0, new ListItem(partyStatus.STATUS_CODE + " " + partyStatus.DESCRIPTION,
                    partyStatus.STATUS_CODE));
            }
            //this.ddlPartyRelationship.DataSource = partyList;
            //this.ddlPartyRelationship.DataBind();
        }
    }
}
