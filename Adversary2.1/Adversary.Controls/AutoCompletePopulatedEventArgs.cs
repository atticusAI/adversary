﻿using System;

namespace Adversary.Controls
{
    public class PersonAutoCompletePopulatedEventArgs : System.EventArgs
    {
        public string PersonnelID { get; set; }
        public string EmployeeFullName { get; set; }
        public string EmployeeFirstName { get; set; }
        public string EmployeeMiddleName { get; set; }
        public string EmployeeLastName { get; set; }
    }

    public class ClientAutoCompletePopulatedEventArgs : System.EventArgs
    {
        public string ClientNumber { get; set; }
        public string ClientName { get; set; }
    }
}
