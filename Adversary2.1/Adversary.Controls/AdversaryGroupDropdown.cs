﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Adversary.DataLayer;
using AjaxControlToolkit;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Adversary.Controls
{
    public class AdversaryMemberSelectedEventArgs : EventArgs
    {
        public string SelectedMember { get; set; }
        public DateTime WorkBegunOn { get; set; }
    }

    [ToolboxData("<{0}:AdversaryGroupDropdown runat=server></{0}:AdversaryGroupDropdown>")]
    public class AdversaryGroupDropdown : WebControl, INamingContainer
    {
        public delegate void OnAdversaryMemberSelectedHandler(object sender, AdversaryMemberSelectedEventArgs e);
        public event OnAdversaryMemberSelectedHandler AdversaryMemberSelected;

        [Bindable(true)]
        public string CommandArgument
        {
            get 
            {
                return this.ViewState[this.ID + "_commandArgument"] as string;
            }
            set 
            {
                this.ViewState[this.ID + "_commandArgument"] = value;
            }
        }

        private string _SelectedMember;
        [Bindable(true)]
        public string SelectedMember
        {
            get { return _SelectedMember; }
            set { _SelectedMember = value; }
        }

        private bool _SuppressAutoPostBack = false;

        public bool SuppressAutoPostBack
        {
            get { return _SuppressAutoPostBack; }
            set { _SuppressAutoPostBack = value; }
        }


        public DropDownList ddlAdversaryGroup;

        private AdversaryData _AdversaryData;

        public AdversaryGroupDropdown()
        {
            _AdversaryData = new AdversaryData();
        }

        public void AdversaryMemberSelectedEventArgs()
        {
            _AdversaryData = new AdversaryData();
        }

        protected override void CreateChildControls()
        {
            EnsureChildControls();
            ddlAdversaryGroup = new DropDownList();
            ddlAdversaryGroup.SelectedIndexChanged += new EventHandler(ddlAdversaryGroup_SelectedIndexChanged);
            ddlAdversaryGroup.DataTextField = "Login";
            ddlAdversaryGroup.DataValueField = "Login";
            ddlAdversaryGroup.AutoPostBack = !SuppressAutoPostBack;
            ddlAdversaryGroup.CausesValidation = false;
            if (_SkinID != null) ddlAdversaryGroup.SkinID = _SkinID;
            bool bindSuccess = BindAdversaryGroup();

            try
            {
                if (bindSuccess && !String.IsNullOrEmpty(SelectedMember))
                {
                    ddlAdversaryGroup.SelectedIndex = -1;
                    ddlAdversaryGroup.Items.FindByValue(SelectedMember).Selected = true;
                }

            }
            catch
            {
                ddlAdversaryGroup.SelectedIndex = -1;
            }
            Controls.Add(ddlAdversaryGroup);
            
        }

        private bool BindAdversaryGroup()
        {
            List<AdminLogin> _adminLogins = new List<AdminLogin>();
            _adminLogins = _AdversaryData.GetAdversaryMembers().Where(M=>M.IsAdversary.HasValue && M.IsAdversary.Value).ToList();
            ddlAdversaryGroup.DataSource = _adminLogins;
            ddlAdversaryGroup.DataBind();
            ddlAdversaryGroup.Items.Insert(0, new ListItem("(Unassigned)", ""));
            return (ddlAdversaryGroup.Items.Count > 1);
        }

        public void ddlAdversaryGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            OnAdversaryMemberSelected(sender, e);
        }

        public virtual void OnAdversaryMemberSelected(object sender, EventArgs e)
        {
            DropDownList d = (DropDownList)sender;
            AdversaryMemberSelectedEventArgs eventArgs = new AdversaryMemberSelectedEventArgs()
            {
                SelectedMember = d.SelectedValue,
                WorkBegunOn = DateTime.Now
            };
            if (AdversaryMemberSelected != null)
                AdversaryMemberSelected(this, eventArgs);
        }

        private string _SkinID;
        public override string SkinID
        {
            get
            {
                //apply the skin ID to the dropdown list
                return base.SkinID;
            }
            set
            {
                _SkinID = value;
                base.SkinID = value;
            }
        }
    }
}
