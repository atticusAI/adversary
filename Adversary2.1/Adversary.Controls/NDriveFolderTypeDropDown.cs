﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using Adversary.DataLayer;
using AjaxControlToolkit;

namespace Adversary.Controls
{
    [ToolboxData("<{0}:NDriveFolderTypeDropDown runat=server></{0}:NDriveFolderTypeDropDown>")]
    public class NDriveFolderTypeDropDown : UserControl, INamingContainer
    {
        public NDriveFolderTypeDropDown()
        {
            if (ddlNDriveTypes == null) ddlNDriveTypes = new DropDownList();
        }

        public delegate void NDriveSelectedEventHandler(object sender, EventArgs e);
        public event NDriveSelectedEventHandler NDriveSelected;

        public DropDownList ddlNDriveTypes;

        private bool _NDriveEnabled;

        public bool NDriveEnabled
        {
            get { return _NDriveEnabled; }
            set
            {
                _NDriveEnabled = value;

                if (_NDriveEnabled == true)
                    EnableNDrive();
            }
        }

        private bool _RequiresNDriveStaff;

        public bool RequiresNDriveStaff
        {
            get
            {
                string[] enabledNDriveTypes = new string[] { "4", "5" };
                if (enabledNDriveTypes.Contains(ddlNDriveTypes.SelectedValue) ||
                    enabledNDriveTypes.Contains(this.NDriveType))
                {
                    _RequiresNDriveStaff = true;
                }
                else
                {
                    _RequiresNDriveStaff = false;
                }
                return _RequiresNDriveStaff;
            }
            set { _RequiresNDriveStaff = value; }
        }

        public void EnableNDrive()
        {
            if (ddlNDriveTypes == null) ddlNDriveTypes = new DropDownList();

            ddlNDriveTypes.Enabled = _NDriveEnabled;
            if (!_NDriveEnabled) ddlNDriveTypes.SelectedIndex = -1;
        }

        //private string _AttachJavascriptFunctionName;

        //public string AttachJavascriptFunctionName
        //{
        //    get { return _AttachJavascriptFunctionName; }
        //    set { _AttachJavascriptFunctionName = value; }
        //}

        private bool _CausesPostBackOnSelected = true;
        public bool CausesPostBackOnSelected
        {
            get { return _CausesPostBackOnSelected; }
            set { _CausesPostBackOnSelected = value; }
        }
  
  


        private string _NDriveType = "";
        private string NDriveType
        {
            get { return _NDriveType; }
            set { _NDriveType = value; }
        }

        protected override void CreateChildControls()
        {
            EnsureChildControls();
            //UpdatePanel upnlNDrive = new UpdatePanel();
            //upnlNDrive.ChildrenAsTriggers = true;
            //upnlNDrive.UpdateMode = UpdatePanelUpdateMode.Conditional;
            
            ddlNDriveTypes.SelectedIndexChanged += new EventHandler(ddlNDriveTypes_SelectedIndexChanged);
            ddlNDriveTypes.AutoPostBack = CausesPostBackOnSelected;
            BindNDriveData();
            if (NDriveEnabled != null && CausesPostBackOnSelected) ddlNDriveTypes.Enabled = NDriveEnabled;
            //ddlNDriveTypes.Enabled = _NDriveEnabled;
            //if (AttachJavascriptFunctionName.Length > 0)
            //{
            //    ddlNDriveTypes.Attributes.Add("onselectedindexchanged",
            //}
            //upnlNDrive.ContentTemplateContainer.Controls.Add(ddlNDriveTypes);

            //this.Controls.Add(upnlNDrive);
            this.Controls.Add(ddlNDriveTypes);

        }

        public void ddlNDriveTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            OnNDriveSelected(sender, e);
        }

        private void BindNDriveData()
        {
            List<NDriveType> _list = new List<NDriveType>();
            using (AdversaryData advData = new AdversaryData())
            {
                _list = advData.GetNDriveTypes();
            }
            ddlNDriveTypes.ID = "ddlNDriveTypes";
            ddlNDriveTypes.DataValueField = "NDriveTypeID";
            ddlNDriveTypes.DataTextField = "NDriveTypeDescription";
            ddlNDriveTypes.DataSource = _list;
            ddlNDriveTypes.DataBind();

            //add the first item
            ddlNDriveTypes.Items.Insert(0, new ListItem("Please Choose...", ""));

            if (!String.IsNullOrEmpty(this.NDriveType))
            {
                try
                {
                    ddlNDriveTypes.Items.FindByValue(this.NDriveType).Selected = true;
                }
                catch
                {
                    ddlNDriveTypes.SelectedIndex = -1;
                }
            }
        }

        protected virtual void OnNDriveSelected(object sender, EventArgs e)
        {
            DropDownList d = (DropDownList)sender;
            _RequiresNDriveStaff = (d.SelectedItem.Value == "3" ? false : true);
            NDriveSelectedEventHandler handler = NDriveSelected;
            if (handler != null)
                handler(sender, e);
            
        }

        public void SetSelectedNDriveType(string nDriveType)
        {
            this.NDriveType = nDriveType;
        }
    }
}
