﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.DataLayer;
using System.Linq;
using System.Data.Linq;

namespace Adversary.Controls
{
    [ToolboxData("<{0}:PracticeCodeDropdown runat=server></{0}:PracticeCodeDropdown>")]
    [Themeable(true)]
    public class PracticeCodeDropdown : WebControl, INamingContainer
    {
        private List<Adversary.DataLayer.AdversaryDataService.PracticeCode> practiceCodes = new List<DataLayer.AdversaryDataService.PracticeCode>();

        #region properties
        [Bindable(true)]
        public string[] RemovedCodes
        {
            get
            {
                object objCodes = ViewState["RemovedCodes"];
                if (objCodes == null)
                    return new string[] { };
                else
                    return (string[])objCodes;
            }
            set
            {
                ViewState["RemovedCodes"] = value;
            }
        }

        private string _CssClass;
        [CssClassProperty]
        public string CssClass
        {
            get { return _CssClass; }
            set { _CssClass = value; }
        }

        private string _ValidationMessage;
        public string ValidationMessage { get { return _ValidationMessage; } set { _ValidationMessage = value; } }


        public RequiredFieldValidator rqfvPracticeCode
        {
            get;
            private set;
        }

        private int _DropDownListWidth = 335;

        public int DropDownListWidth
        {
            get { return _DropDownListWidth; }
            set { _DropDownListWidth = value; }
        }


        private bool _IsRequired;

        public bool IsRequired
        {
            get { return _IsRequired; }
            set { _IsRequired = value; }
        }

        private string _IsRequiredErrorMessage;

        public string IsRequiredErrorMessage
        {
            get { return _IsRequiredErrorMessage; }
            set { _IsRequiredErrorMessage = value; }
        }

        private string _SelectedPracticeCode;

        public string SelectedPracticeCode
        {
            get { return _SelectedPracticeCode; }
            set { _SelectedPracticeCode = value; }
        }

        private bool _IsProBonoOnly;

        public bool IsProBonoOnly
        {
            get { return _IsProBonoOnly; }
            set { _IsProBonoOnly = value; }
        }

        private bool _IsEmployeeLegalWork;

        public bool IsEmployeeLegalWork
        {
            get { return _IsEmployeeLegalWork; }
            set { _IsEmployeeLegalWork = value; }
        }

        private bool _RestrictToSelectedCodeOnly;
        public bool RestrictToSelectedCodeOnly
        {
            get { return _RestrictToSelectedCodeOnly; }
            set { _RestrictToSelectedCodeOnly = value; }
        }

        public string OnClientChange
        {
            get
            {
                return ddlCodes.Attributes["onchange"];
            }

            set
            {
                ddlCodes.Attributes["onchange"] = value;
            }
        }

        private DropDownList _ddlCodes = null;

        public DropDownList ddlCodes
        {
            get
            {
                if (_ddlCodes == null)
                {
                    _ddlCodes = new DropDownList();
                }
                return _ddlCodes;
            }

            private set
            {
                _ddlCodes = value;
            }
        }

        public string Title
        {
            get
            {
                return this.ddlCodes.Attributes["title"];
            }
            set
            {
                this.ddlCodes.Attributes["title"] = value;
            }
        }

#endregion

        private void AddSelectPracticeCodeOption()
        {
            ddlCodes.Items.Insert(0, new ListItem("(select practice code)", ""));
        }
               
        protected override void CreateChildControls()
        {
            base.CreateChildControls();
            if (ddlCodes != null)
            {
                ddlCodes.ID = "ddlCodes";
                if (!string.IsNullOrWhiteSpace(CssClass)) ddlCodes.CssClass = CssClass;
                if (!string.IsNullOrWhiteSpace(ValidationMessage)) ddlCodes.ToolTip = ValidationMessage;

                using (AdversaryData d = new AdversaryData())
                {
                    practiceCodes = d.GetPracticeGroupsList();
                }

                //set up a list of removed codes.
                List<string> __RemovedCodesList = RemovedCodes.ToList();
                __RemovedCodesList.AddRange(Adversary.DataLayer.AdversaryData.UnusedPracticeCodes.ToList<string>());
                if (IsProBonoOnly)
                {
                    practiceCodes = practiceCodes.Where(P => P.matt_cat_code == "105X" || P.matt_cat_code == "105Z").ToList();
                    AddSelectPracticeCodeOption();
                }
                else if (IsEmployeeLegalWork)
                {
                    practiceCodes = practiceCodes.Where(P => P.matt_cat_code == "107X").ToList();
                    this.SelectedPracticeCode = "107X";
                    this.RestrictToSelectedCodeOnly = true;
                }
                else
                {
                    practiceCodes = practiceCodes.Where(P => !__RemovedCodesList.Contains(P.matt_cat_code)).ToList();
                    AddSelectPracticeCodeOption();
                }



                BindData();



                if (!string.IsNullOrEmpty(SelectedPracticeCode))
                {
                    ddlCodes.SelectedIndex = -1;

                    ListItem itm = ddlCodes.Items.FindByValue(SelectedPracticeCode);
                    if (itm != null)
                    {
                        itm.Selected = true;
                    }
                }
            }
            ddlCodes.Width = DropDownListWidth;
            Controls.Add(ddlCodes);

            if (_IsRequired)
            {
                rqfvPracticeCode = new RequiredFieldValidator();
                rqfvPracticeCode.ID = "rqfvPracticeCode";
                rqfvPracticeCode.Display = ValidatorDisplay.None;
                rqfvPracticeCode.ValidationGroup = "valGroup_Screen";
                rqfvPracticeCode.InitialValue = "";
                rqfvPracticeCode.ControlToValidate = ddlCodes.ID;
                rqfvPracticeCode.ErrorMessage = this.IsRequiredErrorMessage;
                Controls.Add(rqfvPracticeCode);

            }

            if (_RestrictToSelectedCodeOnly)
            {
                practiceCodes = practiceCodes.Where(P => P.matt_cat_code == this.SelectedPracticeCode).ToList();
            }
            

        }

        

        protected override void Render(HtmlTextWriter writer)
        {
            ddlCodes.RenderControl(writer);
            if (_IsRequired) rqfvPracticeCode.RenderControl(writer);
            
        }

        private void BindData()
        {
            ddlCodes.DataTextField = "cat_plus_desc";
            ddlCodes.DataValueField = "matt_cat_code";
            ddlCodes.DataSource = practiceCodes;
            ddlCodes.DataBind();
        }
    }
}
