﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.DataLayer
{
    public class MemoNavigation
    {
        private List<MemoApplicationTrack> _AllTracks;

        //ctor
        public MemoNavigation() 
        {
        }

        public MemoNavigation(int currentTrack, string pageURL)
        {
            _AllTracks = GetAllApplicationTracks(currentTrack);
            int currentLocation = -1;

            MemoApplicationTrack trk = _AllTracks.Where(T => (string.Compare(T.PageURL, pageURL, true) == 0) ).FirstOrDefault();
            if (trk != null)
            {
                currentLocation = Convert.ToInt32(trk.PageOrder);
            }

            GetMemoNavigation(currentTrack, currentLocation);
        }

        public MemoNavigation(int currentTrack, int currentLocation)
        {
            _AllTracks = GetAllApplicationTracks(currentTrack);
            GetMemoNavigation(currentTrack, currentLocation);
        }

        //method to fetch navigations
        public void GetMemoNavigation(int currentTrack, int currentLocation)
        {
            Dictionary<MemoNavigation.LocationType, MemoApplicationTrack> locations =
                new Dictionary<MemoNavigation.LocationType, MemoApplicationTrack>();
            locations = GetApplicationTracks(currentTrack, currentLocation);

            if (locations[MemoNavigation.LocationType.Previous] != null)
            {
                _PreviousSectionUrl = locations[MemoNavigation.LocationType.Previous].PageURL;
                _PreviousSectionID = Convert.ToInt32(locations[MemoNavigation.LocationType.Previous].PageOrder);
            }

            if (locations[MemoNavigation.LocationType.Current] != null)
            {
                _CurrentSectionUrl = locations[MemoNavigation.LocationType.Current].PageURL;
                _CurrentSectionID = Convert.ToInt32(locations[MemoNavigation.LocationType.Current].PageOrder);
            }

            if (locations[MemoNavigation.LocationType.Next] != null)
            {
                _NextSectionUrl = locations[MemoNavigation.LocationType.Next].PageURL;
                _NextSectionID = Convert.ToInt32(locations[MemoNavigation.LocationType.Next].PageOrder);
            }
        }
        
        public enum LocationType
        {
            Previous,
            Current,
            Next
        }

        public Dictionary<LocationType, MemoApplicationTrack> GetApplicationTracks(int currentTrack, int currentLocation)
        {
            MemoApplicationTrack previousLocation;
            MemoApplicationTrack nextLocation;
            MemoApplicationTrack thisLocation;
            List<MemoApplicationTrack> copiedTracks = _AllTracks;
            Dictionary<LocationType, MemoApplicationTrack> getTracks = new Dictionary<LocationType, MemoApplicationTrack>();

            thisLocation = _AllTracks.Find(T => T.PageOrder.Equals(currentLocation));
            nextLocation = _AllTracks.SkipWhile(T => Convert.ToInt32(T.PageOrder) != currentLocation).Skip(1).FirstOrDefault();
            copiedTracks.Reverse();
            previousLocation = copiedTracks.SkipWhile(T => Convert.ToInt32(T.PageOrder) != currentLocation).Skip(1).FirstOrDefault();

            //List<MemoApplicationTrack> thisSpot;
            //using (AdversaryDataContext d = new AdversaryDataContext(AdversaryConnection.AdversaryConnectionString()))
            //{
            //    thisSpot = (from T in d.MemoApplicationTracks
            //                where T.MemoTypeTrackNumber == currentTrack
            //                orderby T.PageOrder
            //                select T).ToList<MemoApplicationTrack>();
            //}
            //thisLocation = thisSpot.Find(i => i.PageOrder == currentLocation);
            //previousLocation = thisSpot.TakeWhile(i => i.PageOrder < currentLocation).LastOrDefault();
            //nextLocation = thisSpot.SkipWhile(i => i.PageOrder <= currentLocation).FirstOrDefault();
            getTracks.Add(LocationType.Current, thisLocation);
            getTracks.Add(LocationType.Next, nextLocation);
            getTracks.Add(LocationType.Previous, previousLocation);

            return getTracks;
        }

        public List<MemoApplicationTrack> GetAllApplicationTracks(int currentTrack)
        {
            List<MemoApplicationTrack> memotracks = new List<MemoApplicationTrack>();
            using (AdversaryDataContext d = new AdversaryDataContext())
            {
                memotracks = (from tracks in d.MemoApplicationTracks
                              where tracks.MemoTypeTrackNumber == currentTrack
                              select tracks).ToList();
            }
            memotracks = memotracks.OrderBy(M => M.PageOrder).ToList();
            return memotracks;
        }      

        private int _PreviousSectionID;
        public int PreviousSectionID
        {
            get { return _PreviousSectionID; }
            set { _PreviousSectionID = value; }
        }

        private int _CurrentSectionID;
        public int CurrentSectionID
        {
            get { return _CurrentSectionID; }
            set { _CurrentSectionID = value; }
        }

        private int _NextSectionID;
        public int NextSectionID
        {
            get { return _NextSectionID; }
            set { _NextSectionID = value; }
        }

        //navigation section properties
        private string _PreviousSectionUrl = null;
        public string PreviousSectionUrl
        {
            get { return _PreviousSectionUrl; }
            set { _PreviousSectionUrl = value; }
        }

        private string _CurrentSectionUrl = null;
        public string CurrentSectionUrl
        {
            get { return _CurrentSectionUrl; }
            set { _CurrentSectionUrl = value; }
        }

        private string _NextSectionUrl = null;
        public string NextSectionUrl
        {
            get { return _NextSectionUrl; }
            set { _NextSectionUrl = value; }
        }
    }
}
