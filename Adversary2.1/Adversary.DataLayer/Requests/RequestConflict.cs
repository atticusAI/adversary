﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.DataLayer.Requests
{
    public class RequestConflict : IDisposable
    {
        #region Data Context

        private AdversaryDataContext _dataContext = null;

        private AdversaryDataContext DataContext
        {
            get
            {
                if (this._dataContext == null)
                {
                    this._dataContext = new AdversaryDataContext();
                }
                return this._dataContext;
            }
        }

        #endregion

        #region Construction

        public RequestConflict(int reqID, int conflictID, bool answer, string description, string name)
        {
            this._reqID = reqID;
            this._conflictID = conflictID;
            this._answer = answer;
            this._description = description;
            this._name = name;
        }

        #endregion

        #region Properties

        private int _reqID;

        public int ReqID
        {
            get { return this._reqID; }
        }

        private int _conflictID;

        public int ConflictID
        {
            get { return this._conflictID; }
        }

        private bool _answer;

        public bool Answer
        {
            get { return this._answer; }
            set { this._answer = value; }
        }

        private string _name;

        public string Name
        {
            get { return this._name; }
        }

        private string _description;

        public string Description
        {
            get { return this._description; }
        }

        #endregion

        #region Update

        /// <summary>
        /// call the database and update this record
        /// </summary>
        public void Update()
        {
            var qry = from rc in this.DataContext.RequestConflicts
                      where rc.ReqID == this.ReqID
                      && rc.ConflictID == this.ConflictID
                      select rc;

            DataLayer.RequestConflict drc = qry.FirstOrDefault();

            if (drc != null)
            {
                drc.Answer = this.Answer;
                this.DataContext.SubmitChanges();
            }
        }

        /// <summary>
        /// call the database if the answer property is changed
        /// if not then don't bother
        /// </summary>
        public void Update(bool answer)
        {
            bool changed = (this.Answer != answer);
            this.Answer = answer;
            if (changed) this.Update();
        }

        #endregion

        #region IDisposable

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this._dataContext != null)
                {
                    this._dataContext.Dispose();
                    this._dataContext = null;
                }
            }
        }

        #endregion
    }

    public class RequestConflictCollection : IDisposable, ICollection<RequestConflict>
    {
        #region Data Context

        private AdversaryDataContext _dataContext = null;

        private AdversaryDataContext DataContext
        {
            get
            {
                if (this._dataContext == null)
                {
                    this._dataContext = new AdversaryDataContext();
                }
                return this._dataContext;
            }
        }

        #endregion

        #region Construction

        private readonly int _reqID;

        public RequestConflictCollection(int reqID)
        {
            this._reqID = reqID;
        }

        #endregion 

        #region Storage

        private List<RequestConflict> _conflicts = null;

        private List<RequestConflict> Conflicts
        {
            get
            {
                if (this._conflicts == null || this._conflicts.Count == 0)
                {
                    this._conflicts = this.Load();
                }
                return this._conflicts;
            }
        }

        #endregion

        #region Load and Create

        /// <summary>
        /// if any conflicts records exist for this memo then load them up
        /// if not create them then load them up
        /// </summary>
        private List<RequestConflict> Load()
        {
            int count = (from rc in this.DataContext.RequestConflicts 
                         where rc.ReqID == this._reqID
                         select rc).Count();

            bool available = count > 0 || this.Create();

            List<RequestConflict> cons = new List<RequestConflict>();

            var qry = from rc in this.DataContext.RequestConflicts 
                      where rc.ReqID == this._reqID
                      orderby rc.Conflict.SortOrder 
                      select rc;

            foreach (DataLayer.RequestConflict c in qry)
            {
                cons.Add(new RequestConflict(c.ReqID, c.ConflictID, c.Answer, c.Conflict.Description, c.Conflict.Name));
            }

            return cons;
        }
        
        /// <summary>
        /// attempt to create conflicts records from the metadata
        /// </summary>
        /// <returns>true if it worked otherwise false</returns>
        private bool Create()
        {
            bool created = false;

            List<DataLayer.RequestConflict> drcs = new List<DataLayer.RequestConflict>();

            var qry = from cons in this.DataContext.Conflicts
                      where cons.TrackNumber == MemoType.AdversaryRequest
                      select cons;

            foreach (DataLayer.Conflict c in qry)
            {
                bool answer = this.GetRequestConflictAnswer(this._reqID, c.RequestField);
                drcs.Add(new DataLayer.RequestConflict()
                {
                    ReqID = this._reqID,
                    ConflictID = c.ConflictID,
                    Answer = answer
                });
            }

            if (drcs.Count > 0)
            {
                this.DataContext.RequestConflicts.InsertAllOnSubmit(drcs);
                this.DataContext.SubmitChanges();
                created = true;
            }

            return created;
        }

        /// <summary>
        /// get the answer that was previously stored in the requests table
        /// </summary>
        private bool GetRequestConflictAnswer(int reqID, string requestField)
        {
            bool answer = false;

            if (!string.IsNullOrWhiteSpace(requestField))
            {
                try
                {
                    string sql = string.Format("select isnull({0}, 0) from Requests where reqID = {{0}}", requestField);
                    answer = this.DataContext.ExecuteQuery<bool>(sql, reqID).FirstOrDefault();
                }
                catch(Exception x)
                {
                    answer = false;
                }
            }

            return answer;
        }

        #endregion

        #region ICollection

        /// <summary>
        /// readonly collection
        /// add metadata to the Conflict table if you want more
        /// </summary>
        public void Add(RequestConflict item)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// readonly collection
        /// delete metadata to the Conflict table if you want less
        /// </summary>
        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(RequestConflict item)
        {
            return this.Conflicts.Contains(item);
        }

        public void CopyTo(RequestConflict[] array, int arrayIndex)
        {
            this.Conflicts.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return this.Conflicts.Count; }
        }

        /// <summary>
        /// don't add to the collection, add to the Conflict table
        /// </summary>
        public bool IsReadOnly
        {
            get { return true; }
        }

        /// <summary>
        /// readonly collection
        /// delete metadata to the Conflict table if you want less
        /// </summary>
        public bool Remove(RequestConflict item)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<RequestConflict> GetEnumerator()
        {
            return this.Conflicts.GetEnumerator();
        }
        
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.Conflicts.GetEnumerator();
        }

        #endregion

        #region IDisposable

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this._dataContext != null)
                {
                    this._dataContext.Dispose();
                    this._dataContext = null;
                }
            }
        }

        #endregion
    }
}
