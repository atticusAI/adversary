﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.DataLayer.Requests
{
    public class RequestAssignments : RequestBaseClass, IRequestGet, IRequestSave
    {
        public RequestAssignments(int requestID)
        {
            _RequestID = requestID;
        }

        private DateTime _WorkBegunDate;
        public DateTime WorkBegunDate
        {
            get { return _WorkBegunDate; }
            set { _WorkBegunDate = value; }
        }
        
        private string _WorkBegunBy;
        public string WorkBegunBy
        {
            get { return _WorkBegunBy; }
            set { _WorkBegunBy = value; }
        }

        private string _WorkCompletedBy;
        public string WorkCompletedBy
        {
            get { return _WorkCompletedBy; }
            set { _WorkCompletedBy = value; }
        }

        private DateTime _CompletionDate;
        public DateTime CompletionDate
        {
            get { return _CompletionDate; }
            set { _CompletionDate = value; }
        }


        public bool Save()
        {
            using (AdversaryDataContext dc = new AdversaryDataContext())
            {
                Request request = (from R in dc.Requests 
                                   where R.ReqID == RequestID 
                                   select R).FirstOrDefault();
                if (request != null)
                {
                    request.WorkCompletedBy = WorkCompletedBy;
                    if (CompletionDate != null && !CompletionDate.Equals(DateTime.MinValue)) request.CompletionDate = CompletionDate;
                    if (WorkBegunDate != null) request.WorkBegunDate = WorkBegunDate;
                    request.WorkBegunBy = WorkBegunBy;
                    dc.SubmitChanges();
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        private int _RequestID;
        public int RequestID
        {
            get { return _RequestID; }
        }

        public bool Get()
        {
            using (AdversaryDataContext dc = new AdversaryDataContext())
            {
                Request request = (from R in dc.Requests where R.ReqID == RequestID select R).FirstOrDefault();
                WorkBegunBy = request.WorkBegunBy;
                WorkCompletedBy = request.WorkCompletedBy;
                if (request.CompletionDate.HasValue)
                    CompletionDate = request.CompletionDate.Value;
                if (request.WorkBegunDate.HasValue)
                    WorkBegunDate = request.WorkBegunDate.Value;
            }
            return true;
        }
    }
}
