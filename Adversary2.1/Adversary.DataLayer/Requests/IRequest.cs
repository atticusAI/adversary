﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.DataLayer.Requests
{
    public interface IRequest
    {
        int RequestID { get; }
        
    }

    public interface IRequestSave : IRequest
    {
        bool Save();
    }

    public interface IRequestGet : IRequest
    {
        bool Get();
    }
}
