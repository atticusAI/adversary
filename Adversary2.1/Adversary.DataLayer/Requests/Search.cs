﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.DataLayer.Requests
{
    public class Search : RequestBaseClass
    {
        public IEnumerable<Request> SearchRequests(List<int> conflicts)
        {
            var qry = from r in this.AdversaryContext.Requests
                      join rc in this.AdversaryContext.RequestConflicts on r.ReqID equals rc.ReqID
                      where conflicts.Contains(rc.ConflictID)
                      && rc.Answer == true 
                      select r;

            return qry.Distinct();
        }
    }
}
