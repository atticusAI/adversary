﻿USE [iVantage_05242011]
GO
/****** Object:  StoredProcedure [dbo].[usp_FirmDirectory_GetEmployees]    Script Date: 04/10/2012 09:21:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER      PROCEDURE [dbo].[usp_FirmDirectory_GetEmployees] 
(
       @personID int = null,
       @extension varchar(15) = null,
       @name varchar(125) = null,
       @lname varchar(125) = null 
)
AS

SELECT
       tPerson.PersonGUID,
       tPerson.PersonID,
       tPerson.LastName,
       tPerson.FirstName,
       tPerson.MiddleName,
       tPerson.Nickname,
       tPerson.PayrollID,
       tPerson.Salutation,
       tPerson.PreferredFirstName,
       tPerson.PreferredLastName,
       tPerson.PreferredMiddleName,
       tPerson.Suffix, --addition for new service architecture
       tPerson.CompanyEmailAddress,
       CASE
              WHEN ((tPerson.Salutation = 'Dr.') AND (tPerson.PreferredFirstName IS NOT NULL) AND (tPerson.PreferredMiddleName IS NOT NULL) AND (tPerson.PreferredLastName IS NOT NULL)) THEN
                           tPerson.Salutation + ' ' + tPerson.PreferredFirstName + ' ' + tPerson.PreferredMiddleName + ' ' + tPerson.PreferredLastName
              WHEN ((tPerson.Salutation = 'Dr.') AND (tPerson.PreferredFirstName IS NOT NULL) AND (tPerson.PreferredLastName IS NOT NULL)) THEN
                           tPerson.Salutation + ' ' + tPerson.PreferredFirstName + ' ' + tPerson.PreferredLastName
              WHEN ((tPerson.Salutation = 'Dr.') AND (tPerson.PreferredFirstName IS NOT NULL) AND (tPerson.PreferredLastName IS NULL)) THEN
                           tPerson.Salutation + ' ' + tPerson.PreferredFirstName + ' ' + tPerson.LastName
              WHEN ((tPerson.PreferredFirstName IS NOT NULL) AND (tPerson.PreferredMiddleName IS NOT NULL) AND (tPerson.PreferredLastName IS NOT NULL)) THEN
                           tPerson.PreferredFirstName + ' ' + tPerson.PreferredMiddleName + ' ' + tPerson.PreferredLastName
              WHEN ((tPerson.PreferredFirstName IS NOT NULL) AND (tPerson.PreferredLastName IS NOT NULL)) THEN
                           tPerson.PreferredFirstName + ' ' + tPerson.PreferredLastName
              WHEN ((tPerson.PreferredFirstName IS NOT NULL) AND (tPerson.PreferredLastName IS NULL)) THEN
                     tPerson.PreferredFirstName + ' ' + tPerson.LastName
              WHEN ((tPerson.PreferredLastName IS NOT NULL) AND (tPerson.PreferredFirstName IS NULL)) THEN
                     --tPerson.PreferredLastName + ' ' + tPerson.firstname
                     tPerson.firstName + ' ' + tPerson.PreferredLastName

              WHEN (tPerson.Salutation = 'Dr.') THEN
                     tPerson.Salutation + ' ' + tPerson.FirstName + ' ' + tPerson.LastName
              WHEN ((tPerson.FirstName IS NOT NULL) AND (tPerson.MiddleName IS NOT NULL) AND (tPerson.LastName IS NOT NULL)) THEN
                     
                     tPerson.FirstName + ' '+ tPerson.MiddleName + ' '   + tPerson.LastName
              
              ELSE
                     tPerson.FirstName + ' '   + tPerson.LastName
       END as FullName,
       CASE
              WHEN (tPerson.PreferredLastName IS NOT NULL) THEN tPerson.PreferredLastName
              ELSE tPerson.LastName
       END as SortLastName,
       CASE
              WHEN (tPerson.PreferredFirstName IS NOT NULL) THEN tPerson.PreferredFirstName
              ELSE tPerson.FirstName
       END as SortFirstName,
       PHONE_HOME.UnlistedPhone AS UnlistedPhoneFlag,
       PHONE_DAY.AreaCode + '-' + PHONE_DAY.Phone as DayPhone,
       PHONE_DAY.Extension  as DayExtension,
       PHONE_BB.AreaCode as BlackBerryAreaCode,
       PHONE_BB.Phone as BlackBerryPhoneNumber,
       PHONE_FAX.AreaCode as FaxAreaCode,
       PHONE_FAX.Phone as FaxPhone,
       CASE WHEN PHONE_PAGER.UnlistedPhone = 0 THEN
              PHONE_PAGER.AreaCode
       ELSE NULL END as PagerAreaCode,
       CASE WHEN PHONE_PAGER.UnlistedPhone = 0 THEN
              PHONE_PAGER.Phone
       ELSE NULL END as PagerPhone,
       CASE WHEN PHONE_HOME.UnlistedPhone = 0 THEN
              PHONE_HOME.AreaCode
       ELSE NULL END as HomeAreaCode,
       CASE WHEN PHONE_HOME.UnlistedPhone = 0 THEN
              PHONE_HOME.Phone
       ELSE NULL END as HomePhone,
       CASE WHEN PHONE_MOBILE.UnlistedPhone = 0 THEN
              PHONE_MOBILE.AreaCode
       ELSE NULL END as MobileAreaCode,
       CASE WHEN PHONE_MOBILE.UnlistedPhone = 0 THEN
              PHONE_MOBILE.Phone
       ELSE NULL END as MobilePhone,
       CASE WHEN PHONE_OTHER.UnlistedPhone = 0 THEN
              PHONE_OTHER.AreaCode
       ELSE NULL END AS OtherPhoneAreaCode,
       CASE WHEN PHONE_OTHER.UnlistedPhone = 0 THEN
              PHONE_OTHER.Phone
       ELSE NULL END AS OtherPhone,
       tPersonLocationHist.[Floor],
       tLocation.LocationDescription,
       CASE WHEN tLocation.LocationCode = '51' then tLocation.LocationDescription
       ELSE tLocation.City END as City,
       tJob.JobTitle,
       CASE WHEN tSection.SectionDescription = 'Business Development' THEN 'Marketing'
       ELSE tSection.SectionDescription END as SectionDescription,
       tRank.RankDescription,
       tDepartment.DepartmentDescription,
       tDepartment.DepartmentCode,
       tSection.SectionCode,
       tRank.RankCode,
       tJob.JobCode,
       tLocation.LocationCode,
       tJobFamily.JobFamilyCode,
       tJobFamily.JobFamilyDescription,
       tperson.NetLogin
FROM
       tPerson
       INNER JOIN tPersonLocationHist
       ON tPerson.PersonGUID = tPersonLocationHist.PersonGUID
       INNER JOIN tPersonJobHist
       ON tPerson.PersonGUID = tPersonJobHist.PersonGUID
       INNER JOIN tPersonStatusHist
       ON tPerson.PersonGUID = tPersonStatusHist.PersonGUID
       INNER JOIN tLocation
       ON tPersonLocationHist.LocationCode = tLocation.LocationCode
       INNER JOIN tJob
       ON tPersonJobHist.JobCode = tJob.JobCode
       INNER JOIN tSection
       ON tPersonLocationHist.SectionCode = tSection.SectionCode
       INNER JOIN tRank
       ON tPersonJobHist.[Rank] = tRank.RankCode
       INNER JOIN tDepartment
       ON tPersonLocationHist.DepartmentCode = tDepartment.DepartmentCode
       INNER JOIN tJobFamily
       ON tJob.JobFamilyCode = tJobFamily.JobFamilyCode
       LEFT JOIN tPersonPhone PHONE_DAY
       ON tPerson.PersonGUID = PHONE_DAY.PersonGUID
       AND PHONE_DAY.PhoneTypeCode = 'WORK'
       LEFT JOIN tPersonPhone PHONE_BB
       ON tPerson.PersonGUID = PHONE_BB.PersonGUID
       AND PHONE_BB.PhoneTypeCode = 'BBerry'
       LEFT JOIN tPersonPhone PHONE_FAX
       ON tPerson.PersonGUID = PHONE_FAX.PersonGUID
       AND PHONE_FAX.PhoneTypeCode = 'FAX'
       LEFT JOIN tPersonPhone PHONE_HOME
       ON tPerson.PersonGUID = PHONE_HOME.PersonGUID
       AND PHONE_HOME.PhoneTypeCode = 'HOME'
       LEFT JOIN tPersonPhone PHONE_PAGER
       ON tPerson.PersonGUID = PHONE_PAGER.PersonGUID
       AND PHONE_PAGER.PhoneTypeCode = 'PAGER'
       LEFT JOIN tPersonPhone PHONE_MOBILE
       ON tPerson.PersonGUID = PHONE_MOBILE.PersonGUID
       AND PHONE_MOBILE.PhoneTypeCode = 'MOBILE'
       LEFT JOIN tPersonPhone PHONE_OTHER
       ON tPerson.PersonGUID = PHONE_OTHER.PersonGUID
       AND PHONE_OTHER.PhoneTypeCode = 'OTHER'
WHERE
       (
              tPersonLocationHist.PersonLocationCurrentFlag = '1'
              AND
              tPersonJobHist.PersonJobCurrentFlag = '1'
              AND
              tPersonStatusHist.PersonStatusCurrentFlag = '1'
              AND
              (tPersonStatusHist.StatusCode NOT IN ('T','R','D') OR tPersonStatusHist.StatusCode = 'RA')
              AND
              tPerson.PersonID = COALESCE(@personID, tPerson.PersonID)
              AND
              (
                     PHONE_DAY.Extension = COALESCE(@extension, PHONE_DAY.Extension) 
                     OR (PHONE_DAY.Extension IS NULL AND @extension IS NULL)
              ) 
              AND
              (
                     (tPerson.FullName LIKE '%' + COALESCE(@name, tPerson.FullName) + '%') OR
                     (tPerson.PreferredFirstName LIKE '%' + COALESCE(@name, tPerson.PreferredFirstName) + '%') OR
                     (tPerson.PreferredLastName LIKE '%' + COALESCE(@name, tPerson.PreferredLastName) + '%') OR
                     (tPerson.FirstName + ' '  + tPerson.LastName = @name) OR
                     (tPerson.PreferredFirstName + ' ' + tPerson.LastName = @name) 
              )
              AND
              (
                     COALESCE(tPerson.PreferredLastName,tPerson.LastName) LIKE 
                     (COALESCE(ISNULL(@lname,''),COALESCE(tPerson.PreferredLastName,tPerson.LastName))+'%')
              )
       )
       OR --create an exception for a particular user this way
       (
              tPerson.PersonID = '0981'
              AND
              tPersonLocationHist.PersonLocationEndDate = '2010-12-31 23:59:59.000'
              AND
              tPersonJobHist.PersonJobEndDate = '12/31/2010'
              AND
              tPersonStatusHist.PersonStatusCurrentFlag = '1'
              AND
              (
                     PHONE_DAY.Extension = COALESCE(@extension, PHONE_DAY.Extension) 
                     OR (PHONE_DAY.Extension IS NULL AND @extension IS NULL)
              )
              AND
              tPerson.PersonID = COALESCE(@personID, tPerson.PersonID)
              AND
              (
                     (tPerson.FullName LIKE '%' + COALESCE(@name, tPerson.FullName) + '%') OR
                     (tPerson.PreferredFirstName LIKE '%' + COALESCE(@name, tPerson.PreferredFirstName) + '%') OR
                     (tPerson.PreferredLastName LIKE '%' + COALESCE(@name, tPerson.PreferredLastName) + '%') OR
                     (tPerson.FirstName + ' '  + tPerson.LastName = @name) OR
                     (tPerson.PreferredFirstName + ' ' + tPerson.LastName = @name) 
              )
              AND
              (
                     COALESCE(tPerson.PreferredLastName,tPerson.LastName) LIKE 
                     (COALESCE(ISNULL(@lname,''),COALESCE(tPerson.PreferredLastName,tPerson.LastName))+'%')
              )             
       )
ORDER BY
       SortLastName,
       SortFirstName

