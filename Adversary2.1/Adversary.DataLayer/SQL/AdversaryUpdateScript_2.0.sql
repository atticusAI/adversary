-- blmcgee@hollandhart.com 
-- 2012-05-31
-- refreshed meta data insert statements blmcgee@hollandhart.com 2012-05-31
-- added missing table NDriveTypes blmcgee@hollandhart.com 2012-05-31
-- refreshed meta data insert statements again blmcgee@hollandhart.com 2012-06-22
USE [Adversary]
GO


/***********************************************************************************************************/
/* Drop Constraints */
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PageNavigationRole_PageNavigation]') AND parent_object_id = OBJECT_ID(N'[dbo].[PageNavigationRole]'))
ALTER TABLE [dbo].[PageNavigationRole] DROP CONSTRAINT [FK_PageNavigationRole_PageNavigation]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PageNavigationRole_Role]') AND parent_object_id = OBJECT_ID(N'[dbo].[PageNavigationRole]'))
ALTER TABLE [dbo].[PageNavigationRole] DROP CONSTRAINT [FK_PageNavigationRole_Role]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Rules2_RuleActive]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Rules] DROP CONSTRAINT [DF_Rules2_RuleActive]
END

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ScreeningMemoTypeSummarySection_ScreeningMemoSummarySection]') AND parent_object_id = OBJECT_ID(N'[dbo].[ScreeningMemoTypeSummarySection]'))
ALTER TABLE [dbo].[ScreeningMemoTypeSummarySection] DROP CONSTRAINT [FK_ScreeningMemoTypeSummarySection_ScreeningMemoSummarySection]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SubTabNavigation_PageNavigation]') AND parent_object_id = OBJECT_ID(N'[dbo].[SubTabNavigation]'))
ALTER TABLE [dbo].[SubTabNavigation] DROP CONSTRAINT [FK_SubTabNavigation_PageNavigation]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SubTabNavigation_SubTabNavigation]') AND parent_object_id = OBJECT_ID(N'[dbo].[SubTabNavigation]'))
ALTER TABLE [dbo].[SubTabNavigation] DROP CONSTRAINT [FK_SubTabNavigation_SubTabNavigation]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TabNavigation_PageNavigation]') AND parent_object_id = OBJECT_ID(N'[dbo].[TabNavigation]'))
ALTER TABLE [dbo].[TabNavigation] DROP CONSTRAINT [FK_TabNavigation_PageNavigation]
GO



/***********************************************************************************************************/
/* Drop Tables */
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MemoApplicationTrack]') AND type in (N'U'))
DROP TABLE [dbo].[MemoApplicationTrack]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MemoRules]') AND type in (N'U'))
DROP TABLE [dbo].[MemoRules]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MemoTypes]') AND type in (N'U'))
DROP TABLE [dbo].[MemoTypes]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PageNavigation]') AND type in (N'U'))
DROP TABLE [dbo].[PageNavigation]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PageNavigationRole]') AND type in (N'U'))
DROP TABLE [dbo].[PageNavigationRole]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Role]') AND type in (N'U'))
DROP TABLE [dbo].[Role]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Rules]') AND type in (N'U'))
DROP TABLE [dbo].[Rules]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ScreeningMemoSummarySection]') AND type in (N'U'))
DROP TABLE [dbo].[ScreeningMemoSummarySection]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ScreeningMemoTypeSummarySection]') AND type in (N'U'))
DROP TABLE [dbo].[ScreeningMemoTypeSummarySection]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SubTabNavigation]') AND type in (N'U'))
DROP TABLE [dbo].[SubTabNavigation]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TabNavigation]') AND type in (N'U'))
DROP TABLE [dbo].[TabNavigation]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrackingStatusTypes]') AND type in (N'U'))
DROP TABLE [dbo].[TrackingStatusTypes]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VW_UserPageNavigation]'))
DROP VIEW [dbo].[VW_UserPageNavigation]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[AllRules]'))
DROP VIEW [dbo].[AllRules]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NDriveTypes]') AND type in (N'U'))
DROP TABLE [dbo].[NDriveTypes]
GO



/***********************************************************************************************************/
/* Create Tables */
CREATE TABLE [dbo].[MemoApplicationTrack](
	[MemoApplicationTrackID] [int] IDENTITY(1,1) NOT NULL,
	[MemoTypeTrackNumber] [int] NULL,
	[PageOrder] [int] NULL,
	[PageURL] [varchar](2000) NULL,
	[Name] [varchar](50) NULL,
	[Description] [varchar](2000) NULL,
 CONSTRAINT [PK_MemoApplicationTrack] PRIMARY KEY CLUSTERED 
(
	[MemoApplicationTrackID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[MemoRules](
	[MemoRuleID] [int] IDENTITY(1,1) NOT NULL,
	[RuleID] [int] NOT NULL,
	[TrackNumber] [int] NOT NULL,
 CONSTRAINT [PK_MemoRules] PRIMARY KEY CLUSTERED 
(
	[MemoRuleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[MemoTypes](
	[MemoType] [varchar](50) NULL,
	[TrackNumber] [int] NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[PageNavigation](
	[PageNavigationID] [int] IDENTITY(1,1) NOT NULL,
	[PageUrl] [varchar](500) NOT NULL,
	[Name] [varchar](50) NULL,
	[Description] [varchar](2000) NULL,
 CONSTRAINT [PK_PageNavigation] PRIMARY KEY CLUSTERED 
(
	[PageNavigationID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[PageNavigationRole](
	[PageNavigationRoleID] [int] IDENTITY(1,1) NOT NULL,
	[PageNavigationID] [int] NOT NULL,
	[RoleID] [int] NOT NULL,
 CONSTRAINT [PK_PageNavigationRole] PRIMARY KEY CLUSTERED 
(
	[PageNavigationRoleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Role](
	[RoleID] [int] IDENTITY(1,1) NOT NULL,
	[Role] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[Rules](
	[RuleID] [int] IDENTITY(1,1) NOT NULL,
	[RuleText] [nvarchar](500) NOT NULL,
	[RuleActive] [bit] NOT NULL,
 CONSTRAINT [PK_Rules] PRIMARY KEY CLUSTERED 
(
	[RuleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[ScreeningMemoSummarySection](
	[ScrMemSummarySectionID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Description] [varchar](256) NULL,
	[VirtualPath] [varchar](2000) NOT NULL,
 CONSTRAINT [PK_ScreeningMemoSummarySection] PRIMARY KEY CLUSTERED 
(
	[ScrMemSummarySectionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[ScreeningMemoTypeSummarySection](
	[ScrMemTypeSummarySectionID] [int] IDENTITY(1,1) NOT NULL,
	[TrackNumber] [int] NOT NULL,
	[ScrMemSummarySectionID] [int] NOT NULL,
	[SectionOrder] [int] NOT NULL,
 CONSTRAINT [PK_ScreeningMemoTypeSummarySection] PRIMARY KEY CLUSTERED 
(
	[ScrMemTypeSummarySectionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[SubTabNavigation](
	[SubTabNavigationID] [int] IDENTITY(1,1) NOT NULL,
	[PageNavigationID] [int] NOT NULL,
	[SubTabOrder] [int] NOT NULL,
	[SubTabURL] [varchar](2000) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Description] [varchar](2000) NULL,
	[ParentSubTabNavigationID] [int] NULL,
 CONSTRAINT [PK_SubTabNavigation] PRIMARY KEY CLUSTERED 
(
	[SubTabNavigationID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[TabNavigation](
	[TabNavigationID] [int] IDENTITY(1,1) NOT NULL,
	[ParentPageNavigationID] [int] NOT NULL,
	[PageNavigationID] [int] NULL,
	[TabOrder] [int] NULL,
 CONSTRAINT [PK_TabNavigation] PRIMARY KEY CLUSTERED 
(
	[TabNavigationID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[TrackingStatusTypes](
	[StatusTypeID] [int] NOT NULL,
	[Status] [varchar](100) NULL,
	[StatusMessage] [varchar](2000) NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[NDriveTypes](
	[NDriveTypeID] [int] IDENTITY(1,1) NOT NULL,
	[NDriveTypeDescription] [nvarchar](500) NULL
) ON [PRIMARY]

GO


/***********************************************************************************************************/
/* Insert Data */
USE [Adversary];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[MemoApplicationTrack] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[MemoApplicationTrack]([MemoApplicationTrackID], [MemoTypeTrackNumber], [PageOrder], [PageURL], [Name], [Description])
SELECT 1, 3, 100, N'summary.aspx', N'Memo Summary', N'Display screening memo details.' UNION ALL
SELECT 2, 1, 2, N'basicinformation.aspx', N'Basic Information', N'Edit basic screening memo information.' UNION ALL
SELECT 3, 1, 3, N'clientdetails.aspx', N'Client Information', N'Edit basic client details.' UNION ALL
SELECT 4, 1, 5, N'docketing.aspx', N'Docketing', N'Edit docketing details.' UNION ALL
SELECT 5, 1, 6, N'clientaddress.aspx', N'Client Address', N'Edit client address details.' UNION ALL
SELECT 6, 1, 8, N'ReferringParty.aspx', N'Referring Party', N'Edit referring party.' UNION ALL
SELECT 7, 1, 11, N'AdverseParties.aspx', N'Adverse Parties', N'Edit Adverse Parties' UNION ALL
SELECT 8, 1, 14, N'Fees.aspx', N'Fee Split', N'Edit fee details.' UNION ALL
SELECT 9, 1, 17, N'Retainer.aspx', N'Retainer', N'Edit the retainer details.' UNION ALL
SELECT 10, 1, 18, N'BillingInfo.aspx', N'Billing', N'Edit billing details.' UNION ALL
SELECT 11, 2, 1, N'summary.aspx', N'Memo Summary', N'Display screening memo details.' UNION ALL
SELECT 12, 2, 3, N'CM.aspx', N'Matter Information', N'Edit client matter details.' UNION ALL
SELECT 13, 2, 4, N'docketing.aspx', N'Docketing', N'Edit docketing details.' UNION ALL
SELECT 15, 2, 6, N'ClientAddress.aspx', N'Client Address', N'Edit Client Addresses' UNION ALL
SELECT 16, 1, 15, N'ConflictReport.aspx', N'Conflict Report', N'View the conflict report.' UNION ALL
SELECT 17, 1, 4, N'CM.aspx', N'Matter Information', N'Edit client matter details.' UNION ALL
SELECT 18, 2, 5, N'Contact.aspx', N'Contact Information', N'Edit contacts.' UNION ALL
SELECT 19, 1, 7, N'Contact.aspx', N'Contact Information', N'Edit contacts.' UNION ALL
SELECT 20, 1, 9, N'RelatedParties.aspx', N'Related Parties', N'Edit related parties.' UNION ALL
SELECT 21, 1, 12, N'Staffing.aspx', N'Staffing Information', N'Edit staffing details.' UNION ALL
SELECT 22, 1, 13, N'Office.aspx', N'Originating Office', N'Edit office details.' UNION ALL
SELECT 23, 1, 16, N'EngagementLetter.aspx', N'Engagement Letter', N'Edit the engagement letter.' UNION ALL
SELECT 25, 0, 101, N'~/Default.aspx', N'New Business Intake Home', N'Go back to the home page.' UNION ALL
SELECT 26, 0, 201, N'Default.aspx', N'Screening Memo Main Menu', N'Go to the main menu for the screening menus.' UNION ALL
SELECT 27, 0, 301, N'Search.aspx', N'Search', N'Search screening memos.' UNION ALL
SELECT 28, 0, 401, N'~/Default.aspx?logout=true', N'Logout', N'Log out of the New Business Intake website.' UNION ALL
SELECT 29, 1, 19, N'Submit.aspx', N'Final Review', N'Review any input errors and submit the memo for approval.' UNION ALL
SELECT 30, 3, 200, N'BasicInformation.aspx', N'Basic Information', N'Edit basic information' UNION ALL
SELECT 31, 3, 300, N'CM.aspx', N'Matter Information', N'Edit matter specific information' UNION ALL
SELECT 33, 3, 500, N'ClientDetails.aspx', N'Client Details', N'Edit client details' UNION ALL
SELECT 34, 3, 600, N'ClientAddress.aspx', N'Client Address', N'Edit client address data' UNION ALL
SELECT 35, 3, 700, N'Contact.aspx', N'Contact Information', N'Edit contact information' UNION ALL
SELECT 36, 3, 800, N'RelatedParties.aspx', N'Related Parties', N'Edit related parties data' UNION ALL
SELECT 37, 3, 900, N'AdverseParties.aspx', N'Adverse Parties', N'Edit adverse parties data' UNION ALL
SELECT 38, 3, 1000, N'Opposing.aspx', N'Opposing Counsel', N'Edit opposing counsel data' UNION ALL
SELECT 39, 3, 1100, N'Staffing.aspx', N'Staffing Information', N'Edit staffing information data' UNION ALL
SELECT 40, 3, 1200, N'Office.aspx', N'Originating Office', N'Edit originating office data' UNION ALL
SELECT 41, 3, 1300, N'ConflictReport.aspx', N'Conflict Report', N'Edit/upload conflict report' UNION ALL
SELECT 42, 3, 1400, N'EngagementLetter.aspx', N'Engagement Letter', N'Edit/upload engagement letter' UNION ALL
SELECT 43, 3, 1500, N'ProBonoReason.aspx', N'Reason', N'Edit pro bono reason' UNION ALL
SELECT 44, 3, 1600, N'Submit.aspx', N'Final Review', N'Review before submitting' UNION ALL
SELECT 45, 2, 2, N'BasicInformation.aspx', N'Basic Information', N'Edit basic information data' UNION ALL
SELECT 46, 5, 1, N'summary.aspx', N'Memo Summary', N'Summary' UNION ALL
SELECT 47, 5, 2, N'BasicInformation.aspx', N'Basic Information', N'Edit basic info' UNION ALL
SELECT 48, 5, 3, N'LegalWork.aspx', N'Legal Work Information', N'Edit legal work info (who work is for)' UNION ALL
SELECT 49, 5, 4, N'CM.aspx', N'Matter Information', N'Edit matter specific info' UNION ALL
SELECT 50, 5, 5, N'docketing.aspx', N'Docketing', N'Edit Docketing Information' UNION ALL
SELECT 51, 5, 6, N'RelatedParties.aspx', N'Related Parties', N'Edit related parties' UNION ALL
SELECT 52, 5, 7, N'AdverseParties.aspx', N'Adverse Parties', N'Edit adverse parties' UNION ALL
SELECT 53, 5, 8, N'Staffing.aspx', N'Staffing Information', N'Edit staffing information'
COMMIT;
RAISERROR (N'[dbo].[MemoApplicationTrack]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[MemoApplicationTrack]([MemoApplicationTrackID], [MemoTypeTrackNumber], [PageOrder], [PageURL], [Name], [Description])
SELECT 54, 5, 9, N'office.aspx', N'Originating Office', N'Edit originating office' UNION ALL
SELECT 55, 5, 11, N'ConflictReport.aspx', N'Conflict Report', N'Edit/upload conflict report' UNION ALL
SELECT 56, 5, 12, N'EngagementLetter.aspx', N'Engagement Letter', N'Edit/upload engagement letter' UNION ALL
SELECT 57, 5, 13, N'BillingInfo.aspx', N'Billing', N'Edit billing info' UNION ALL
SELECT 58, 5, 14, N'Submit.aspx', N'Final Review', N'Final review before submitting.' UNION ALL
SELECT 59, 6, 1, N'summary.aspx', N'Memo Summary', N'Memo summary' UNION ALL
SELECT 60, 6, 2, N'basicinformation.aspx', N'Basic Information', N'Basic Information' UNION ALL
SELECT 61, 6, 3, N'clientdetails.aspx', N'Client Type', N'client details' UNION ALL
SELECT 62, 6, 4, N'ClientAddress.aspx', N'Client Address', N'Client Address' UNION ALL
SELECT 63, 6, 5, N'Contact.aspx', N'Contact Information', N'Contact Information' UNION ALL
SELECT 64, 6, 6, N'RelatedParties.aspx', N'Related Parties', N'Related Parties' UNION ALL
SELECT 65, 6, 7, N'AdverseParties.aspx', N'Adverse Parties', N'Adverse Parties' UNION ALL
SELECT 66, 6, 8, N'ConflictReport.aspx', N'Conflict Report', N'Conflict Report' UNION ALL
SELECT 67, 6, 9, N'RejectionReason.aspx', N'Reason', N'Rejection Reason' UNION ALL
SELECT 68, 6, 11, N'RejectionLetter.aspx', N'Rejection Letter', N'Rejection Letter' UNION ALL
SELECT 69, 6, 12, N'Submit.aspx', N'Final Review', N'Final Review' UNION ALL
SELECT 70, 3, 400, N'docketing.aspx', N'Docketing', N'Edit docketing info' UNION ALL
SELECT 71, 2, 7, N'RelatedParties.aspx', N'Related Parties', N'Edit related parties' UNION ALL
SELECT 72, 2, 8, N'AdverseParties.aspx', N'Adverse Parties', N'Edit adverse parties' UNION ALL
SELECT 73, 2, 9, N'Staffing.aspx', N'Staffing Information', N'Edit staffing information' UNION ALL
SELECT 74, 2, 11, N'Fees.aspx', N'Fee Split', N'Edit fee split information' UNION ALL
SELECT 75, 2, 12, N'ConflictReport.aspx', N'Conflict Report', N'Edit conflict report files' UNION ALL
SELECT 76, 2, 13, N'EngagementLetter.aspx', N'Engagement Letter', N'Edit engagement letter files' UNION ALL
SELECT 77, 2, 14, N'Retainer.aspx', N'Retainer', N'Edit retainer information' UNION ALL
SELECT 78, 2, 15, N'BillingInfo.aspx', N'Billing', N'Edit billing information' UNION ALL
SELECT 79, 2, 16, N'Submit.aspx', N'Final Review', N'Final review' UNION ALL
SELECT 80, 8, 2, N'BasicInfo.aspx', N'Basic Information', N'Edit request basic information' UNION ALL
SELECT 81, 8, 3, N'Parties.aspx', N'Parties', N'Edit request party information' UNION ALL
SELECT 82, 8, 4, N'AdditionalInfo.aspx', N'Additional Information', N'Edit request additional information' UNION ALL
SELECT 83, 8, 5, N'Submit.aspx', N'Final Review', N'Review your adversary request' UNION ALL
SELECT 84, 8, 1, N'Summary.aspx', N'Summary', N'Summary of your adversary request' UNION ALL
SELECT 85, 8, 0, N'ViewRequests.aspx?type=my', N'My Submitted Adversary Requests', N'Start your adversary request here' UNION ALL
SELECT 86, 8, -1, N'default.aspx', N'Requests Home', N'Requests Home' UNION ALL
SELECT 87, 2, 10, N'office.aspx', N'Originating Office', N'Edit originating office' UNION ALL
SELECT 89, 1, 1, N'summary.aspx', N'Memo Summary', N'Display screening memo details.'
COMMIT;
RAISERROR (N'[dbo].[MemoApplicationTrack]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[MemoApplicationTrack] OFF;

USE [Adversary];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[MemoRules] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[MemoRules]([MemoRuleID], [RuleID], [TrackNumber])
SELECT 1, 1, 1 UNION ALL
SELECT 2, 1, 2 UNION ALL
SELECT 4, 2, 1 UNION ALL
SELECT 5, 3, 1 UNION ALL
SELECT 6, 2, 2 UNION ALL
SELECT 7, 3, 2 UNION ALL
SELECT 8, 4, 1 UNION ALL
SELECT 9, 4, 2 UNION ALL
SELECT 10, 5, 2 UNION ALL
SELECT 11, 9, 1 UNION ALL
SELECT 12, 9, 3 UNION ALL
SELECT 14, 9, 6 UNION ALL
SELECT 15, 9, 2 UNION ALL
SELECT 16, 6, 1 UNION ALL
SELECT 17, 6, 3 UNION ALL
SELECT 18, 6, 6 UNION ALL
SELECT 19, 7, 1 UNION ALL
SELECT 20, 7, 3 UNION ALL
SELECT 21, 7, 6 UNION ALL
SELECT 22, 8, 1 UNION ALL
SELECT 23, 8, 3 UNION ALL
SELECT 25, 8, 6 UNION ALL
SELECT 26, 10, 1 UNION ALL
SELECT 27, 10, 2 UNION ALL
SELECT 28, 10, 3 UNION ALL
SELECT 29, 10, 6 UNION ALL
SELECT 30, 12, 1 UNION ALL
SELECT 31, 12, 2 UNION ALL
SELECT 32, 12, 3 UNION ALL
SELECT 33, 12, 6 UNION ALL
SELECT 34, 13, 1 UNION ALL
SELECT 35, 13, 2 UNION ALL
SELECT 36, 13, 3 UNION ALL
SELECT 37, 13, 6 UNION ALL
SELECT 38, 14, 1 UNION ALL
SELECT 39, 14, 2 UNION ALL
SELECT 41, 14, 3 UNION ALL
SELECT 42, 14, 6 UNION ALL
SELECT 43, 15, 6 UNION ALL
SELECT 44, 16, 6 UNION ALL
SELECT 45, 17, 6 UNION ALL
SELECT 46, 18, 6 UNION ALL
SELECT 47, 19, 1 UNION ALL
SELECT 48, 19, 2 UNION ALL
SELECT 49, 19, 3 UNION ALL
SELECT 50, 19, 5 UNION ALL
SELECT 52, 20, 1 UNION ALL
SELECT 53, 20, 2 UNION ALL
SELECT 54, 20, 3 UNION ALL
SELECT 55, 20, 5
COMMIT;
RAISERROR (N'[dbo].[MemoRules]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[MemoRules]([MemoRuleID], [RuleID], [TrackNumber])
SELECT 56, 20, 6 UNION ALL
SELECT 57, 21, 1 UNION ALL
SELECT 58, 21, 2 UNION ALL
SELECT 59, 21, 3 UNION ALL
SELECT 60, 21, 5 UNION ALL
SELECT 61, 21, 6 UNION ALL
SELECT 62, 22, 1 UNION ALL
SELECT 63, 22, 2 UNION ALL
SELECT 64, 22, 3 UNION ALL
SELECT 65, 22, 5 UNION ALL
SELECT 67, 23, 1 UNION ALL
SELECT 68, 23, 2 UNION ALL
SELECT 69, 23, 3 UNION ALL
SELECT 70, 23, 5 UNION ALL
SELECT 72, 24, 1 UNION ALL
SELECT 73, 24, 2 UNION ALL
SELECT 74, 24, 3 UNION ALL
SELECT 75, 24, 5 UNION ALL
SELECT 76, 24, 6 UNION ALL
SELECT 77, 25, 1 UNION ALL
SELECT 78, 25, 2 UNION ALL
SELECT 79, 25, 3 UNION ALL
SELECT 80, 25, 5 UNION ALL
SELECT 81, 26, 1 UNION ALL
SELECT 82, 26, 2 UNION ALL
SELECT 83, 26, 3 UNION ALL
SELECT 84, 26, 5 UNION ALL
SELECT 85, 27, 1 UNION ALL
SELECT 86, 27, 2 UNION ALL
SELECT 87, 27, 3 UNION ALL
SELECT 88, 27, 5 UNION ALL
SELECT 89, 28, 1 UNION ALL
SELECT 90, 28, 2 UNION ALL
SELECT 91, 28, 3 UNION ALL
SELECT 92, 28, 5 UNION ALL
SELECT 93, 28, 6 UNION ALL
SELECT 94, 29, 1 UNION ALL
SELECT 95, 29, 6 UNION ALL
SELECT 96, 30, 1 UNION ALL
SELECT 97, 30, 6 UNION ALL
SELECT 98, 31, 1 UNION ALL
SELECT 99, 31, 2 UNION ALL
SELECT 100, 31, 3 UNION ALL
SELECT 101, 31, 5 UNION ALL
SELECT 103, 32, 1 UNION ALL
SELECT 104, 32, 2 UNION ALL
SELECT 105, 32, 3 UNION ALL
SELECT 106, 32, 5 UNION ALL
SELECT 107, 32, 6 UNION ALL
SELECT 108, 33, 1
COMMIT;
RAISERROR (N'[dbo].[MemoRules]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[MemoRules]([MemoRuleID], [RuleID], [TrackNumber])
SELECT 109, 33, 2 UNION ALL
SELECT 110, 34, 1 UNION ALL
SELECT 111, 35, 1 UNION ALL
SELECT 112, 36, 1 UNION ALL
SELECT 113, 36, 2 UNION ALL
SELECT 114, 36, 3 UNION ALL
SELECT 115, 36, 5 UNION ALL
SELECT 117, 37, 3 UNION ALL
SELECT 118, 38, 1 UNION ALL
SELECT 119, 38, 2 UNION ALL
SELECT 120, 38, 3 UNION ALL
SELECT 121, 38, 5 UNION ALL
SELECT 122, 38, 6 UNION ALL
SELECT 123, 39, 1 UNION ALL
SELECT 124, 39, 2 UNION ALL
SELECT 125, 39, 3 UNION ALL
SELECT 126, 39, 5 UNION ALL
SELECT 127, 39, 6 UNION ALL
SELECT 128, 46, 1 UNION ALL
SELECT 129, 40, 2 UNION ALL
SELECT 130, 40, 3 UNION ALL
SELECT 131, 40, 5 UNION ALL
SELECT 132, 40, 6 UNION ALL
SELECT 134, 41, 8 UNION ALL
SELECT 135, 42, 8 UNION ALL
SELECT 136, 43, 8 UNION ALL
SELECT 137, 44, 8 UNION ALL
SELECT 138, 45, 8 UNION ALL
SELECT 139, 47, 1 UNION ALL
SELECT 140, 48, 1 UNION ALL
SELECT 141, 48, 2 UNION ALL
SELECT 142, 48, 3 UNION ALL
SELECT 143, 48, 5
COMMIT;
RAISERROR (N'[dbo].[MemoRules]: Insert Batch: 3.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[MemoRules] OFF;

USE [Adversary];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[MemoTypes]([MemoType], [TrackNumber])
SELECT N'Adversary Request', 8 UNION ALL
SELECT N'New Client and New Matter', 1 UNION ALL
SELECT N'Existing Client and New Matter', 2 UNION ALL
SELECT N'Pro Bono', 3 UNION ALL
SELECT N'Pro Bono', 4 UNION ALL
SELECT N'H&H Legal Work', 5 UNION ALL
SELECT N'Rejected Matter', 6 UNION ALL
SELECT N'Test Pages', 7
COMMIT;
RAISERROR (N'[dbo].[MemoTypes]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

USE [Adversary];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[PageNavigation] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[PageNavigation]([PageNavigationID], [PageUrl], [Name], [Description])
SELECT 2, N'~/Admin/ApprovalDefault.aspx', N'Approval', N'Screening Memo Approval' UNION ALL
SELECT 3, N'~/Admin/AdminDefault.aspx', N'Admin', N'New Business Intake Administration' UNION ALL
SELECT 4, N'~/Default.aspx', N'Home', N'New Business Intake Home' UNION ALL
SELECT 5, N'~/Screening/Default.aspx', N'Screening Memo', N'Screening Memos' UNION ALL
SELECT 6, N'~/Requests/Default.aspx', N'Adversary Requests', N'Adversary Requests' UNION ALL
SELECT 7, N'~/Requests/ViewRequests.aspx', N'View Adversary Requests', N'View Adversary Requests' UNION ALL
SELECT 8, N'~/Requests/Summary.aspx', N'Adversary Request Summary', N'Adversary Request Summary' UNION ALL
SELECT 9, N'~/Requests/BasicInfo.aspx', N'Basic Request Info', N'Basic Request Information' UNION ALL
SELECT 10, N'~/Requests/Parties.aspx', N'Request Parties', N'Adversary Requst Parties' UNION ALL
SELECT 11, N'~/Requests/AdditionalInfo.aspx', N'Request Info', N'Additional Request Information' UNION ALL
SELECT 12, N'~/Requests/Submit.aspx', N'Final Review', N'Review and Submit Adversary Request' UNION ALL
SELECT 13, N'~/Admin/Queue.aspx', N'Approval Queue', N'Unified Approval Queue' UNION ALL
SELECT 14, N'~/Admin/Approval.aspx', N'Approval', N'Unified Approval and Processing Page' UNION ALL
SELECT 15, N'~/Admin/AdminSearch.aspx', N'Search', N'Search Screening Memos as Admin' UNION ALL
SELECT 16, N'~/Admin/ApprovalSearch.aspx', N'Search', N'Search screening memos as approval' UNION ALL
SELECT 17, N'~/Admin/Users.aspx', N'User Administration', N'User Administration'
COMMIT;
RAISERROR (N'[dbo].[PageNavigation]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[PageNavigation] OFF;

USE [Adversary];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[PageNavigationRole] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[PageNavigationRole]([PageNavigationRoleID], [PageNavigationID], [RoleID])
SELECT 1, 3, 1 UNION ALL
SELECT 2, 3, 2 UNION ALL
SELECT 3, 2, 1 UNION ALL
SELECT 5, 2, 2 UNION ALL
SELECT 6, 4, 1 UNION ALL
SELECT 7, 5, 1 UNION ALL
SELECT 8, 6, 1 UNION ALL
SELECT 9, 4, 2 UNION ALL
SELECT 10, 5, 2 UNION ALL
SELECT 11, 6, 2 UNION ALL
SELECT 12, 6, 3 UNION ALL
SELECT 13, 6, 4 UNION ALL
SELECT 14, 6, 5 UNION ALL
SELECT 15, 4, 3 UNION ALL
SELECT 16, 4, 4 UNION ALL
SELECT 17, 4, 5 UNION ALL
SELECT 18, 5, 3 UNION ALL
SELECT 19, 5, 4 UNION ALL
SELECT 20, 5, 5 UNION ALL
SELECT 21, 7, 1 UNION ALL
SELECT 22, 7, 2 UNION ALL
SELECT 23, 7, 3 UNION ALL
SELECT 24, 7, 4 UNION ALL
SELECT 25, 7, 5 UNION ALL
SELECT 26, 8, 1 UNION ALL
SELECT 27, 8, 2 UNION ALL
SELECT 28, 8, 3 UNION ALL
SELECT 29, 8, 4 UNION ALL
SELECT 31, 8, 5 UNION ALL
SELECT 32, 9, 1 UNION ALL
SELECT 33, 9, 2 UNION ALL
SELECT 34, 9, 3 UNION ALL
SELECT 35, 9, 4 UNION ALL
SELECT 36, 9, 5 UNION ALL
SELECT 37, 10, 1 UNION ALL
SELECT 38, 10, 2 UNION ALL
SELECT 39, 10, 3 UNION ALL
SELECT 40, 10, 4 UNION ALL
SELECT 41, 10, 5 UNION ALL
SELECT 43, 11, 1 UNION ALL
SELECT 44, 11, 2 UNION ALL
SELECT 45, 11, 3 UNION ALL
SELECT 46, 11, 4 UNION ALL
SELECT 47, 11, 5 UNION ALL
SELECT 48, 12, 1 UNION ALL
SELECT 49, 12, 2 UNION ALL
SELECT 50, 12, 3 UNION ALL
SELECT 51, 12, 4 UNION ALL
SELECT 52, 12, 5 UNION ALL
SELECT 53, 2, 3
COMMIT;
RAISERROR (N'[dbo].[PageNavigationRole]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[PageNavigationRole]([PageNavigationRoleID], [PageNavigationID], [RoleID])
SELECT 54, 2, 4 UNION ALL
SELECT 55, 13, 1 UNION ALL
SELECT 56, 13, 2 UNION ALL
SELECT 57, 13, 3 UNION ALL
SELECT 58, 13, 4 UNION ALL
SELECT 59, 13, 5 UNION ALL
SELECT 60, 14, 1 UNION ALL
SELECT 61, 14, 2 UNION ALL
SELECT 62, 14, 3 UNION ALL
SELECT 63, 14, 4 UNION ALL
SELECT 64, 14, 5 UNION ALL
SELECT 65, 4, 6 UNION ALL
SELECT 66, 5, 6 UNION ALL
SELECT 67, 6, 6 UNION ALL
SELECT 68, 17, 1 UNION ALL
SELECT 69, 17, 2
COMMIT;
RAISERROR (N'[dbo].[PageNavigationRole]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[PageNavigationRole] OFF;

USE [Adversary];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[Role] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[Role]([RoleID], [Role])
SELECT 1, N'Adversary' UNION ALL
SELECT 2, N'AdversaryAdmin' UNION ALL
SELECT 3, N'AP' UNION ALL
SELECT 4, N'PrimaryAP' UNION ALL
SELECT 5, N'PGM' UNION ALL
SELECT 6, N'Default'
COMMIT;
RAISERROR (N'[dbo].[Role]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[Role] OFF;

USE [Adversary];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[Rules] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[Rules]([RuleID], [RuleText], [RuleActive])
SELECT 1, N'Specify an attorney for this memo.', 1 UNION ALL
SELECT 2, N'The attorney listed for this memo with employee number {0} did not resolve to a valid H&H employee.', 1 UNION ALL
SELECT 3, N'Specify the person entering this memo.', 1 UNION ALL
SELECT 4, N'The person listed in the ''input by'' field for this memo with employee number {0} did not resolve to a valid H&H employee.', 1 UNION ALL
SELECT 5, N'Provide a valid client name or number.', 1 UNION ALL
SELECT 6, N'Specify whether this is a new or existing client.', 1 UNION ALL
SELECT 7, N'Provide the client organization''s name.', 1 UNION ALL
SELECT 8, N'Provide the client individual''s name.', 1 UNION ALL
SELECT 9, N'The client address is missing a street', 1 UNION ALL
SELECT 10, N'The client address is missing a city', 1 UNION ALL
SELECT 12, N'The client address is missing a state', 1 UNION ALL
SELECT 13, N'The client address is missing a country', 1 UNION ALL
SELECT 14, N'The client address is missing a phone number', 1 UNION ALL
SELECT 15, N'Provide a reason why this matter is being rejected', 1 UNION ALL
SELECT 16, N'Specify whether a rejection letter will be attached to this memo', 1 UNION ALL
SELECT 17, N'Specify a reason why a rejection letter will not be attached to this memo', 1 UNION ALL
SELECT 18, N'A rejection letter was not attached to this memo', 1 UNION ALL
SELECT 19, N'Specify whether an engagement letter will be sent.', 1 UNION ALL
SELECT 20, N'An engagement letter has not been attached to this screening memo.', 1 UNION ALL
SELECT 21, N'Specify a reason why an engagement letter will not be attached.', 1 UNION ALL
SELECT 22, N'No originating office was selected.', 1 UNION ALL
SELECT 23, N'No practice group was selected for this memo.', 1 UNION ALL
SELECT 24, N'Practice group codes 101X, 180L, 181L and 182L are no longer valid.', 1 UNION ALL
SELECT 25, N'Specify a matter name.', 1 UNION ALL
SELECT 26, N'Provide a description of the work.', 1 UNION ALL
SELECT 27, N'Select the type of N Drive folder required.', 1 UNION ALL
SELECT 28, N'Specify users requiring access to the N Drive folder.', 1 UNION ALL
SELECT 29, N'Specify the docketing attorney.', 1 UNION ALL
SELECT 30, N'Specify the docketing staff member.', 1 UNION ALL
SELECT 31, N'A billing attorney was not entered.', 1 UNION ALL
SELECT 32, N'Billing attorney ''{0}'' did not resolve to a valid H&H employee', 1 UNION ALL
SELECT 33, N'Specify whether a retainer will be required for this memo.', 1 UNION ALL
SELECT 34, N'Select whether you have previously received origination credit', 1 UNION ALL
SELECT 35, N'Please give justification for receiving the origination credit', 1 UNION ALL
SELECT 36, N'Specify whether this client requires outside counsel to follow written policies.', 1 UNION ALL
SELECT 37, N'You must specify a pro bono reason', 1 UNION ALL
SELECT 38, N'Specify whether a conflict report will be sent.', 1 UNION ALL
SELECT 39, N'A Conflict Report was not attached to this memo', 1 UNION ALL
SELECT 40, N'Specify a reason why a Conflict Report will not be attached.', 1 UNION ALL
SELECT 41, N'The deadline date for this request is either invalid or has not been entered', 1 UNION ALL
SELECT 42, N'A client name was not entered', 1 UNION ALL
SELECT 43, N'A name or payroll ID was not specified for this request.', 1 UNION ALL
SELECT 44, N'An attorney name or payroll ID was not specified for this request.', 1 UNION ALL
SELECT 45, N'Please choose a valid criteria for the client or adverse party on the Additional Information page.', 1 UNION ALL
SELECT 46, N'You must upload a conflict report for new matters for new clients', 1 UNION ALL
SELECT 47, N'You must upload an engagement letter for new matters for new clients', 1 UNION ALL
SELECT 48, N'Written policies were not attached to this memo', 1
COMMIT;
RAISERROR (N'[dbo].[Rules]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[Rules] OFF;

USE [Adversary];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[ScreeningMemoSummarySection] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[ScreeningMemoSummarySection]([ScrMemSummarySectionID], [Name], [Description], [VirtualPath])
SELECT 1, N'Basic Information', NULL, N'~/Controls/SummarySections/BasicInformation.ascx' UNION ALL
SELECT 2, N'Client Information', NULL, N'~/Controls/SummarySections/ClientInformation.ascx' UNION ALL
SELECT 3, N'Matter Information', NULL, N'~/Controls/SummarySections/MatterInformation.ascx' UNION ALL
SELECT 4, N'Docketing Information', NULL, N'~/Controls/SummarySections/DocketingInformation.ascx' UNION ALL
SELECT 5, N'Client Address', NULL, N'~/Controls/SummarySections/ClientAddress.ascx' UNION ALL
SELECT 6, N'Contact Information', NULL, N'~/Controls/SummarySections/ContactInformation.ascx' UNION ALL
SELECT 7, N'Referring Party', NULL, N'~/Controls/SummarySections/ReferringParty.ascx' UNION ALL
SELECT 8, N'Related Parties', NULL, N'~/Controls/SummarySections/RelatedParties.ascx' UNION ALL
SELECT 9, N'Adverse Parties', NULL, N'~/Controls/SummarySections/AdverseParties.ascx' UNION ALL
SELECT 10, N'Staffing Information', NULL, N'~/Controls/SummarySections/StaffingInformation.ascx' UNION ALL
SELECT 11, N'Originating Office', NULL, N'~/Controls/SummarySections/OriginatingOffice.ascx' UNION ALL
SELECT 12, N'Fee Split', NULL, N'~/Controls/SummarySections/FeeSplit.ascx' UNION ALL
SELECT 13, N'Conflict Report', NULL, N'~/Controls/SummarySections/ConflictReport.ascx' UNION ALL
SELECT 14, N'Engagement Letter', NULL, N'~/Controls/SummarySections/EngagementLetter.ascx' UNION ALL
SELECT 15, N'Retainer', NULL, N'~/Controls/SummarySections/Retainer.ascx' UNION ALL
SELECT 16, N'Billing', NULL, N'~/Controls/SummarySections/Billing.ascx' UNION ALL
SELECT 17, N'Additional Attached Files', NULL, N'~/Controls/SummarySections/Attachments.ascx' UNION ALL
SELECT 18, N'Reason', NULL, N'~/Controls/SummarySections/Reason.ascx' UNION ALL
SELECT 19, N'Opposing Counsel', NULL, N'~/Controls/SummarySections/OpposingCounsel.ascx' UNION ALL
SELECT 20, N'Legal Work Information', NULL, N'~/Controls/SummarySections/LegalWorkInformation.ascx' UNION ALL
SELECT 22, N'Rejection', NULL, N'~/Controls/SummarySections/Rejection.ascx'
COMMIT;
RAISERROR (N'[dbo].[ScreeningMemoSummarySection]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[ScreeningMemoSummarySection] OFF;

USE [Adversary];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[ScreeningMemoTypeSummarySection] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[ScreeningMemoTypeSummarySection]([ScrMemTypeSummarySectionID], [TrackNumber], [ScrMemSummarySectionID], [SectionOrder])
SELECT 1, 1, 1, 1 UNION ALL
SELECT 2, 1, 2, 2 UNION ALL
SELECT 3, 1, 3, 3 UNION ALL
SELECT 4, 1, 4, 4 UNION ALL
SELECT 5, 1, 5, 5 UNION ALL
SELECT 6, 1, 6, 6 UNION ALL
SELECT 7, 1, 7, 7 UNION ALL
SELECT 8, 1, 8, 8 UNION ALL
SELECT 9, 1, 9, 9 UNION ALL
SELECT 10, 1, 10, 10 UNION ALL
SELECT 11, 1, 11, 11 UNION ALL
SELECT 12, 1, 12, 12 UNION ALL
SELECT 13, 1, 13, 13 UNION ALL
SELECT 14, 1, 14, 14 UNION ALL
SELECT 15, 1, 15, 15 UNION ALL
SELECT 16, 1, 16, 16 UNION ALL
SELECT 17, 1, 17, 17 UNION ALL
SELECT 18, 2, 1, 1 UNION ALL
SELECT 19, 2, 3, 2 UNION ALL
SELECT 20, 2, 4, 3 UNION ALL
SELECT 21, 2, 6, 4 UNION ALL
SELECT 22, 2, 5, 5 UNION ALL
SELECT 23, 2, 8, 6 UNION ALL
SELECT 24, 2, 9, 7 UNION ALL
SELECT 25, 2, 10, 8 UNION ALL
SELECT 26, 2, 11, 9 UNION ALL
SELECT 27, 2, 12, 10 UNION ALL
SELECT 28, 2, 13, 11 UNION ALL
SELECT 29, 2, 14, 12 UNION ALL
SELECT 30, 2, 15, 13 UNION ALL
SELECT 31, 2, 16, 14 UNION ALL
SELECT 32, 2, 17, 15 UNION ALL
SELECT 33, 3, 1, 1 UNION ALL
SELECT 34, 3, 3, 2 UNION ALL
SELECT 35, 3, 4, 3 UNION ALL
SELECT 36, 3, 2, 4 UNION ALL
SELECT 37, 3, 5, 5 UNION ALL
SELECT 38, 3, 6, 6 UNION ALL
SELECT 39, 3, 8, 7 UNION ALL
SELECT 40, 3, 9, 8 UNION ALL
SELECT 41, 3, 18, 9 UNION ALL
SELECT 42, 3, 10, 10 UNION ALL
SELECT 43, 3, 11, 11 UNION ALL
SELECT 44, 3, 13, 12 UNION ALL
SELECT 45, 3, 14, 13 UNION ALL
SELECT 46, 3, 22, 14 UNION ALL
SELECT 47, 3, 17, 15 UNION ALL
SELECT 48, 4, 1, 1 UNION ALL
SELECT 49, 4, 20, 2 UNION ALL
SELECT 50, 4, 3, 3
COMMIT;
RAISERROR (N'[dbo].[ScreeningMemoTypeSummarySection]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[ScreeningMemoTypeSummarySection]([ScrMemTypeSummarySectionID], [TrackNumber], [ScrMemSummarySectionID], [SectionOrder])
SELECT 51, 4, 4, 4 UNION ALL
SELECT 52, 4, 8, 5 UNION ALL
SELECT 53, 4, 9, 6 UNION ALL
SELECT 54, 4, 10, 7 UNION ALL
SELECT 55, 4, 11, 8 UNION ALL
SELECT 56, 4, 13, 9 UNION ALL
SELECT 57, 4, 14, 10 UNION ALL
SELECT 58, 4, 16, 11 UNION ALL
SELECT 59, 4, 17, 12 UNION ALL
SELECT 60, 5, 1, 1 UNION ALL
SELECT 61, 5, 20, 2 UNION ALL
SELECT 62, 5, 3, 3 UNION ALL
SELECT 63, 5, 4, 4 UNION ALL
SELECT 64, 5, 8, 5 UNION ALL
SELECT 65, 5, 9, 6 UNION ALL
SELECT 66, 5, 10, 7 UNION ALL
SELECT 67, 5, 11, 8 UNION ALL
SELECT 68, 5, 13, 9 UNION ALL
SELECT 69, 5, 14, 10 UNION ALL
SELECT 70, 5, 16, 11 UNION ALL
SELECT 71, 5, 17, 12 UNION ALL
SELECT 72, 6, 1, 1 UNION ALL
SELECT 73, 6, 2, 2 UNION ALL
SELECT 74, 6, 5, 3 UNION ALL
SELECT 75, 6, 6, 4 UNION ALL
SELECT 76, 6, 8, 5 UNION ALL
SELECT 77, 6, 9, 6 UNION ALL
SELECT 78, 6, 13, 7 UNION ALL
SELECT 79, 6, 18, 8 UNION ALL
SELECT 80, 6, 22, 9 UNION ALL
SELECT 81, 6, 17, 10 UNION ALL
SELECT 82, 1, 18, 18 UNION ALL
SELECT 83, 1, 22, 18 UNION ALL
SELECT 84, 1, 19, 19 UNION ALL
SELECT 85, 1, 20, 20
COMMIT;
RAISERROR (N'[dbo].[ScreeningMemoTypeSummarySection]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[ScreeningMemoTypeSummarySection] OFF;

USE [Adversary];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[SubTabNavigation] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[SubTabNavigation]([SubTabNavigationID], [PageNavigationID], [SubTabOrder], [SubTabURL], [Name], [Description], [ParentSubTabNavigationID])
SELECT 1, 2, 100, N'~/Admin/ApprovalSearch.aspx', N'Search for Memo', N'Find a screening memo.', NULL UNION ALL
SELECT 2, 2, 200, N'~/Admin/Queue.aspx', N'Queue', N'Show the screening memo queue.', NULL UNION ALL
SELECT 3, 2, 300, N'~/Admin/Approval.aspx?scrmemid=<ScrMemID>', N'Approve Selected Memo', N'Approve the selected screening memo.', NULL UNION ALL
SELECT 4, 2, 400, N'~/Screening/Summary.aspx?scrmemid=<ScrMemID>&type=approval', N'Summary View', N'View / Edit the selected screening memo.', NULL UNION ALL
SELECT 5, 2, 500, N'javascript:hh.popWindow("~/Screening/PrintView.aspx?scrmemid=<ScrMemID>"); return false;', N'Print View', N'Show the most recently selected memo in the print view.', NULL UNION ALL
SELECT 6, 2, 600, N'~/Admin/FilesAdmin.aspx?scrmemid=<ScrMemID>&type=approval', N'Attachment View', N'View and upload additional documents.', NULL UNION ALL
SELECT 7, 2, 700, N'~/Default.aspx?logout=true', N'Logout', N'Log out of the new business intake website.', NULL UNION ALL
SELECT 8, 3, 1000, N'', N'Screening Memo Approval', N'Screening memo approval and processing', NULL UNION ALL
SELECT 9, 3, 2000, N'~/Admin/AddNamesToExistingMatters.aspx', N'Add Names to Existing Matters', N'Add names to existing clients/matters.', NULL UNION ALL
SELECT 11, 3, 3000, N'~/Admin/Impersonate.aspx', N'View an Approver''s Queue', N'View some other approver''s queue.', NULL UNION ALL
SELECT 12, 3, 4000, N'', N'Adversary Requests', N'Adversary Request approval and processing.', NULL UNION ALL
SELECT 13, 3, 5000, N'', N'User Administration', N'Administer user access right and A/R approval.', NULL UNION ALL
SELECT 14, 3, 6000, N'~/Default.aspx?logout=true', N'Logout', N'Log out of the new business intake website.', NULL UNION ALL
SELECT 15, 3, 1100, N'~/Admin/AdminSearch.aspx', N'Search for Memo', N'Find a screening memo.', 8 UNION ALL
SELECT 17, 3, 1200, N'~/Admin/AdminQueue.aspx', N'Memos Ready for Processing', N'View the admin queue.', 8 UNION ALL
SELECT 18, 3, 1300, N'~/Admin/AdminProcessing.aspx?scrmemid=<ScrMemID>', N'Process Selected Memo', N'Screening memo approval.', 8 UNION ALL
SELECT 19, 3, 1400, N'javascript:hh.popWindow("~/Screening/PrintView.aspx?scrmemid=<ScrMemID>"); return false;', N'Print View', N'Show the print version of the selected memo.', 8 UNION ALL
SELECT 20, 3, 1500, N'~/Admin/FilesAdmin.aspx?scrmemid=<ScrMemID>&view=admin', N'Attachment View', N'View and upload additional documents.', 8 UNION ALL
SELECT 22, 3, 1600, N'~/Screening/Summary.aspx?scrmemid=<ScrMemID>&view=admin', N'Summary View', N'View the contents of the selected memo.', 8 UNION ALL
SELECT 23, 3, 4100, N'~/Requests/ViewRequests.aspx?type=all', N'View All Requests', N'View and assign all adversary requests.', 12 UNION ALL
SELECT 24, 3, 4200, N'~/Requests/ViewRequests.aspx?type=my', N'View My Requests', N'View and assign my adversary request.', 12 UNION ALL
SELECT 25, 3, 4300, N'~/Requests/Summary.aspx?requestid=<RequestID>&type=admin', N'Summary', N'Summary of the data entered for this adversary.', 12 UNION ALL
SELECT 26, 3, 4400, N'~/Admin/EditAdversaryRequest.aspx?requestid=<RequestID>', N'Edit Request', N'Edit the adversary request.', 12 UNION ALL
SELECT 27, 3, 4500, N'~/Admin/DailyList.aspx', N'Daily List Report', N'Daily list report for all-firm emails, summaries, etc', 12 UNION ALL
SELECT 28, 3, 5100, N'~/Admin/Users.aspx', N'User Accounts', N'Assigna APs, PGM, and Adversary Roles to Accounts', 13 UNION ALL
SELECT 29, 3, 5200, N'~/Admin/ARApprovalUsers.aspx', N'A/R Approval Administration', N'Set default users that recieve A/R approval messages.', 13
COMMIT;
RAISERROR (N'[dbo].[SubTabNavigation]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[SubTabNavigation] OFF;

USE [Adversary];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[TabNavigation] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[TabNavigation]([TabNavigationID], [ParentPageNavigationID], [PageNavigationID], [TabOrder])
SELECT 30, 3, 3, 500 UNION ALL
SELECT 31, 3, 4, 100 UNION ALL
SELECT 32, 3, 5, 200 UNION ALL
SELECT 33, 3, 6, 300 UNION ALL
SELECT 35, 2, 2, 400 UNION ALL
SELECT 36, 2, 3, 500 UNION ALL
SELECT 37, 2, 4, 100 UNION ALL
SELECT 38, 3, 2, 400 UNION ALL
SELECT 39, 2, 5, 200 UNION ALL
SELECT 40, 2, 6, 300 UNION ALL
SELECT 41, 4, 4, 100 UNION ALL
SELECT 42, 4, 5, 200 UNION ALL
SELECT 43, 4, 6, 300 UNION ALL
SELECT 44, 4, 2, 400 UNION ALL
SELECT 45, 4, 3, 500 UNION ALL
SELECT 46, 6, 4, 100 UNION ALL
SELECT 47, 6, 5, 200 UNION ALL
SELECT 48, 6, 6, 300 UNION ALL
SELECT 49, 6, 2, 400 UNION ALL
SELECT 50, 6, 3, 500
COMMIT;
RAISERROR (N'[dbo].[TabNavigation]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[TabNavigation] OFF;

USE [Adversary];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[TrackingStatusTypes]([StatusTypeID], [Status], [StatusMessage])
SELECT 1, N'Submitted for Approval', N'This screening memo is awaiting approval' UNION ALL
SELECT 200, N'Modification Requested', N'This screening memo requires modification.' UNION ALL
SELECT 0, N'N/A', NULL UNION ALL
SELECT 3, N'Adversary Group Processing', N'This screening memo has been approved and is being processed.' UNION ALL
SELECT 100, N'Completed', N'This screening memo has been processed and completed.' UNION ALL
SELECT 600, N'Rejected', N'This screening memo has been rejected.'
COMMIT;
RAISERROR (N'[dbo].[TrackingStatusTypes]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

USE [Adversary];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[NDriveTypes] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[NDriveTypes]([NDriveTypeID], [NDriveTypeDescription])
SELECT 1, N'Patent Working Files' UNION ALL
SELECT 2, N'Patent Files' UNION ALL
SELECT 3, N'Trademark Working Files' UNION ALL
SELECT 4, N'Matter Folder' UNION ALL
SELECT 5, N'Patent Record'
COMMIT;
RAISERROR (N'[dbo].[NDriveTypes]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[NDriveTypes] OFF;



/***********************************************************************************************************/
/* Create Constraints */
ALTER TABLE [dbo].[PageNavigationRole]  WITH CHECK ADD  CONSTRAINT [FK_PageNavigationRole_PageNavigation] FOREIGN KEY([PageNavigationID])
REFERENCES [dbo].[PageNavigation] ([PageNavigationID])
GO

ALTER TABLE [dbo].[PageNavigationRole] CHECK CONSTRAINT [FK_PageNavigationRole_PageNavigation]
GO

ALTER TABLE [dbo].[PageNavigationRole]  WITH CHECK ADD  CONSTRAINT [FK_PageNavigationRole_Role] FOREIGN KEY([RoleID])
REFERENCES [dbo].[Role] ([RoleID])
GO

ALTER TABLE [dbo].[PageNavigationRole] CHECK CONSTRAINT [FK_PageNavigationRole_Role]
GO

ALTER TABLE [dbo].[Rules] ADD  CONSTRAINT [DF_Rules2_RuleActive]  DEFAULT ((0)) FOR [RuleActive]
GO

ALTER TABLE [dbo].[ScreeningMemoTypeSummarySection]  WITH CHECK ADD  CONSTRAINT [FK_ScreeningMemoTypeSummarySection_ScreeningMemoSummarySection] FOREIGN KEY([ScrMemSummarySectionID])
REFERENCES [dbo].[ScreeningMemoSummarySection] ([ScrMemSummarySectionID])
GO

ALTER TABLE [dbo].[ScreeningMemoTypeSummarySection] CHECK CONSTRAINT [FK_ScreeningMemoTypeSummarySection_ScreeningMemoSummarySection]
GO

ALTER TABLE [dbo].[SubTabNavigation]  WITH CHECK ADD  CONSTRAINT [FK_SubTabNavigation_PageNavigation] FOREIGN KEY([PageNavigationID])
REFERENCES [dbo].[PageNavigation] ([PageNavigationID])
GO

ALTER TABLE [dbo].[SubTabNavigation] CHECK CONSTRAINT [FK_SubTabNavigation_PageNavigation]
GO

ALTER TABLE [dbo].[SubTabNavigation]  WITH CHECK ADD  CONSTRAINT [FK_SubTabNavigation_SubTabNavigation] FOREIGN KEY([ParentSubTabNavigationID])
REFERENCES [dbo].[SubTabNavigation] ([SubTabNavigationID])
GO

ALTER TABLE [dbo].[SubTabNavigation] CHECK CONSTRAINT [FK_SubTabNavigation_SubTabNavigation]
GO

ALTER TABLE [dbo].[TabNavigation]  WITH CHECK ADD  CONSTRAINT [FK_TabNavigation_PageNavigation] FOREIGN KEY([ParentPageNavigationID])
REFERENCES [dbo].[PageNavigation] ([PageNavigationID])
GO

ALTER TABLE [dbo].[TabNavigation] CHECK CONSTRAINT [FK_TabNavigation_PageNavigation]
GO



/***********************************************************************************************************/
/* Create Views*/
CREATE VIEW [dbo].[VW_UserPageNavigation] AS 

SELECT al.[UserID],
	r.[Role], 
	r.[RoleID], 
	pn.[PageNavigationID],
	pn.[PageUrl]
FROM [dbo].[AdminLogins] al
LEFT OUTER JOIN [dbo].[Role] r on r.[Role] = 'Adversary'
	AND ISNULL(al.[IsAdversary], 0) = 1
LEFT OUTER JOIN [dbo].[PageNavigationRole] pnr on pnr.[RoleID] = r.[RoleID]
LEFT OUTER JOIN [dbo].[PageNavigation] pn on pn.[PageNavigationID] = pnr.[PageNavigationID]
WHERE pn.[PageNavigationID] IS NOT NULL

UNION

SELECT al.[UserID],
	r.[Role], 
	r.[RoleID], 
	pn.[PageNavigationID],
	pn.[PageUrl]
FROM [dbo].[AdminLogins] al
LEFT OUTER JOIN [dbo].[Role] r on r.[Role] = 'AdversaryAdmin'
	AND ISNULL(al.[IsAdversaryAdmin], 0) = 1
LEFT OUTER JOIN [dbo].[PageNavigationRole] pnr on pnr.[RoleID] = r.[RoleID]
LEFT OUTER JOIN [dbo].[PageNavigation] pn on pn.[PageNavigationID] = pnr.[PageNavigationID]
WHERE pn.[PageNavigationID] IS NOT NULL

UNION

SELECT al.[UserID],
	r.[Role], 
	r.[RoleID], 
	pn.[PageNavigationID],
	pn.[PageUrl]
FROM [dbo].[AdminLogins] al
LEFT OUTER JOIN [dbo].[Role] r on r.[Role] = 'AP'
	AND ISNULL(al.[IsAP], 0) = 1
LEFT OUTER JOIN [dbo].[PageNavigationRole] pnr on pnr.[RoleID] = r.[RoleID]
LEFT OUTER JOIN [dbo].[PageNavigation] pn on pn.[PageNavigationID] = pnr.[PageNavigationID]
WHERE pn.[PageNavigationID] IS NOT NULL

UNION

SELECT al.[UserID],
	r.[Role], 
	r.[RoleID], 
	pn.[PageNavigationID],
	pn.[PageUrl]
FROM [dbo].[AdminLogins] al
LEFT OUTER JOIN [dbo].[Role] r on r.[Role] = 'PrimaryAP'
	AND ISNULL(al.[IsPrimaryAP], 0) = 1
LEFT OUTER JOIN [dbo].[PageNavigationRole] pnr on pnr.[RoleID] = r.[RoleID]
LEFT OUTER JOIN [dbo].[PageNavigation] pn on pn.[PageNavigationID] = pnr.[PageNavigationID]
WHERE pn.[PageNavigationID] IS NOT NULL

UNION

SELECT al.[UserID],
	r.[Role], 
	r.[RoleID], 
	pn.[PageNavigationID],
	pn.[PageUrl]
FROM [dbo].[AdminLogins] al
LEFT OUTER JOIN [dbo].[Role] r on r.[Role] = 'PGM'
	AND ISNULL(al.[IsPGM], 0) = 1
LEFT OUTER JOIN [dbo].[PageNavigationRole] pnr on pnr.[RoleID] = r.[RoleID]
LEFT OUTER JOIN [dbo].[PageNavigation] pn on pn.[PageNavigationID] = pnr.[PageNavigationID]
WHERE pn.[PageNavigationID] IS NOT NULL


UNION

SELECT NULL AS [UserID],
	r.[Role], 
	r.[RoleID], 
	pn.[PageNavigationID],
	pn.[PageUrl]
FROM [dbo].[Role] r 
LEFT OUTER JOIN [dbo].[PageNavigationRole] pnr on pnr.[RoleID] = r.[RoleID]
LEFT OUTER JOIN [dbo].[PageNavigation] pn on pn.[PageNavigationID] = pnr.[PageNavigationID]
WHERE pn.[PageNavigationID] IS NOT NULL
	AND r.[Role] = 'Default'
GO

CREATE VIEW [dbo].[AllRules]
AS
SELECT     dbo.MemoRules.MemoRuleID, dbo.MemoRules.TrackNumber, dbo.Rules.RuleText, dbo.Rules.RuleActive, dbo.Rules.RuleID
FROM         dbo.MemoRules INNER JOIN
                      dbo.Rules ON dbo.MemoRules.RuleID = dbo.Rules.RuleID
GO
