﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.DataLayer
{
    public class ApproverComboBox: IDisposable 
    {
        private AdversaryDataContext _dataContext = null;

        private AdversaryDataContext DataContext
        {
            get
            {
                if (_dataContext == null)
                {
                    _dataContext = new AdversaryDataContext();
                }
                return _dataContext;
            }
        }

        private List<AdminPracCode> _adminPracCodes = null;

        public List<AdminPracCode> AdminPracCodes
        {
            get
            {
                if (_adminPracCodes == null)
                {
                    _adminPracCodes = (from apc in DataContext.AdminPracCodes
                                       select apc).ToList();
                }
                return _adminPracCodes;
            }
        }

        public List<AdminLogin> GetAdminLoginsByOfficeCode(int officeCode)
        {
            return (from al in DataContext.AdminLogins
                    where al.Office == officeCode
                    && Convert.ToBoolean(al.IsAP) == true
                    select al).ToList();
        }

        public List<AdminLogin> GetAdminLoginsByPracCode(string pracCode)
        {
            var qry = from a in DataContext.AdminLogins
                      join p in DataContext.AdminPracCodes on a.UserID equals p.UserID
                      where p.PracCode == pracCode
                      select a;

            return qry.ToList();
        }

        public List<AdminLogin> GetAdminLoginsByAdversaryGroup()
        {
            return (from a in DataContext.AdminLogins
                    where Convert.ToBoolean(a.IsAdversary) == true
                    select a).ToList();
        }

        public void Dispose()
        {
            if (_dataContext != null)
            {
                _dataContext.Dispose();
                _dataContext = null;
            }
        }
    }
}
