﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.DataLayer
{
    public class ARTable : IDisposable
    {
        private FinancialDataService.FinancialDataServiceContractClient _svcFinancialData = null;

        private FinancialDataService.FinancialDataServiceContractClient SvcFinancialData
        {
            get
            {
                if (_svcFinancialData == null)
                {
                    _svcFinancialData = new FinancialDataService.FinancialDataServiceContractClient();
                }
                return _svcFinancialData;
            }
        }

        public void Dispose()
        {
            if (_svcFinancialData == null)
            {
                _svcFinancialData.Close();
                _svcFinancialData = null;
            }
        }

        class BillComparer : IEqualityComparer<FinancialDataService.Bill>
        {
            public bool Equals(FinancialDataService.Bill x, FinancialDataService.Bill y)
            {
                return x.BillNumber == y.BillNumber;
            }

            public int GetHashCode(FinancialDataService.Bill bill)
            {
                return bill.BillNumber.GetHashCode();
            }
        }

        public ARBalance GetClientARBalance(string clientNumber)
        {
            ARBalance balance = new ARBalance();

            FinancialDataService.ClientAR _clientAR = SvcFinancialData.GetClientAR(clientNumber);

            if (_clientAR != null)
            {
                balance.Current = _clientAR.ARZero30;
                balance.Days_31_to_60 = _clientAR.AR3060;
                balance.Days_61_to_90 = _clientAR.AR6090;
                balance.Days_91_to_120 = _clientAR.AROver90;
                balance.Days_121_Plus = _clientAR.TrustTotal;
                balance.TotalAR = _clientAR.ARZero30 + _clientAR.AR3060 + _clientAR.AR6090 + _clientAR.AROver90 + _clientAR.TrustTotal;
            }

            return balance;



            ///TODO: change this to use the new GetClientARBills() method which will return far fewer
            ///rows and eliminate timeout and large dataset errors.
            //List<FinancialDataService.Bill> bills = SvcFinancialData.GetClientBills(clientNumber)
            //    .Distinct(new BillComparer())
            //    .Where(b => b.TotalAR > 0)
            //    .ToList();
            //foreach (FinancialDataService.Bill bill in bills)
            //{
            //    if (balance == null)
            //        balance = new ARBalance();

            //    TimeSpan span = DateTime.Now - (DateTime)bill.BillDate;
            //    if (span.Days < 30)
            //        balance.Current += (decimal)bill.TotalAR;
            //    else if (span.Days >= 31 && span.Days <= 60)
            //        balance.Days_31_to_60 += (decimal)bill.TotalAR;
            //    else if (span.Days >= 61 && span.Days <= 90)
            //        balance.Days_61_to_90 += (decimal)bill.TotalAR;
            //    else if (span.Days >= 91 && span.Days <= 120)
            //        balance.Days_91_to_120 += (decimal)bill.TotalAR;
            //    else if (span.Days >= 121)
            //        balance.Days_121_Plus += (decimal)bill.TotalAR;
            //}
            
            //if (balance != null)
            //    balance.TotalAR = balance.Current + 
            //        balance.Days_121_Plus + 
            //        balance.Days_31_to_60 + 
            //        balance.Days_61_to_90 + 
            //        balance.Days_91_to_120;

            //return balance;
        }
    }

    public class ARBalance
    {
        public decimal TotalAR { get; set; }
        public decimal Current { get; set; }
        public decimal Days_31_to_60 { get; set; }
        public decimal Days_61_to_90 { get; set; }
        public decimal Days_91_to_120 { get; set; }
        public decimal Days_121_Plus { get; set; }
    }
}
