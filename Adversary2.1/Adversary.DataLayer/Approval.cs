﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Adversary.DataLayer.FinancialDataService;
using Adversary.DataLayer.HRDataService;
using Adversary.DataLayer.AdversaryDataService;

namespace Adversary.DataLayer
{
    public class Approval : IDisposable
    {
        private AdversaryDataContext _dataContext = null;

        private FinancialDataServiceContractClient _financialDataContract = null;

        private FinancialDataServiceContractClient FinancialDataContract
        {
            get
            {
                if (this._financialDataContract == null)
                {
                    this._financialDataContract = new FinancialDataServiceContractClient();
                }
                return this._financialDataContract;
            }
        }

        private FirmDataContractClient _firmDataContract = null;

        private FirmDataContractClient FirmDataContract
        {
            get
            {
                if (this._firmDataContract == null)
                {
                    this._firmDataContract = new FirmDataContractClient();
                }
                return this._firmDataContract;
            }
        }

        private AdversaryDataServiceContractClient _adversaryDataContract = null;

        private AdversaryDataServiceContractClient AdversaryDataContract
        {
            get
            {
                if (_adversaryDataContract == null)
                {
                    _adversaryDataContract = new AdversaryDataServiceContractClient();
                }
                return _adversaryDataContract;
            }
        }

        private List<FirmOffice> _firmOffices = null;

        public List<FirmOffice> FirmOffices
        {
            get
            {
                if (this._firmOffices == null)
                {
                    this._firmOffices = FirmDataContract.GetFirmOffices().ToList();
                }
                return this._firmOffices;
            }
        }

        public FirmOffice GetFirmOffice(string locationCode)
        {
            var qry = from FirmOffice off in this.FirmOffices
                      where off.LocationCode == locationCode
                      select off;
            return qry.FirstOrDefault();
        }

        public void UpdateClientMatterNumber(int clientMatterID, string clientMatterNumber)
        {
            var qry = from c in this.DataContext.ClientMatterNumbers
                      where c.ClientMatterID == clientMatterID
                      select c;
            ClientMatterNumber cmn = qry.FirstOrDefault();

            if (cmn != null)
            {
                cmn.ClientMatterNumber1 = clientMatterNumber;
                this.DataContext.SubmitChanges();
            }
        }

        public void AddClientMatterNumber(int scrMemID, string clientMatterNumber)
        {
            ClientMatterNumber cmn = new ClientMatterNumber()
            {
                ScrMemID = scrMemID, 
                ClientMatterNumber1 = clientMatterNumber 
            };
            this.DataContext.ClientMatterNumbers.InsertOnSubmit(cmn);
            this.DataContext.SubmitChanges();
        }

        public void DeleteClientMatterNumber(int clientMatterID)
        {
            var qry = from c in this.DataContext.ClientMatterNumbers
                      where c.ClientMatterID == clientMatterID
                      select c;
            ClientMatterNumber cmn = qry.FirstOrDefault();

            if (cmn != null)
            {
                this.DataContext.ClientMatterNumbers.DeleteOnSubmit(cmn);
                this.DataContext.SubmitChanges();
            }
        }

        public AdminLogin GetAdminLogin(string login)
        {
            var qry = from al in this.DataContext.AdminLogins
                      where al.Login == login
                      select al;
            return qry.FirstOrDefault();
        }

        /// <summary>
        /// Get the name of a client from the HHESB
        /// *will throw* if the name is not found of if something else bad happens 
        /// you must catch!
        /// </summary>
        /// <param name="clientCode">the client code</param>
        /// <returns>the clientname field</returns>
        public string GetClientName(string clientCode)
        {
            return this.FinancialDataContract.GetClient(clientCode).ClientName;
        }

        public void UpdateTrackingStatus(Tracking tracking, int? statusTypeID)
        {
            tracking.StatusTypeID = statusTypeID;
            if (statusTypeID != TrackingStatusType.Rejected && statusTypeID != TrackingStatusType.ModificationRequested)
            {
                tracking.RejectionHold = false;
            }
            else
            {
                tracking.RejectionHold = true;
            }
        }

        public void UpdateAPAcknowledged(Tracking tracking, int? apUserID, bool apAcknowledged)
        {
            tracking.APUserID = apUserID;
            tracking.APAcknowledged = apAcknowledged;
            if (!apAcknowledged)
            {
                tracking.APApproved = false;
            }
        }

        public void UpdatePGMAcknowledged(Tracking tracking, int? pgmUserID, bool pgmAcknowledged)
        {
            tracking.PGMUserID = pgmUserID;
            tracking.PGMAcknowledged = pgmAcknowledged;
            if (!pgmAcknowledged)
            {
                tracking.PGMApproved = false;
            }
        }

        public void SaveADVAcknowledged(int trackingID, int? adversaryUserID, bool adversaryAcknowledged)
        {
            Tracking trk = this.GetTrackingRecord(trackingID);
            if (trk != null)
            {
                trk.AdversaryUserID = adversaryUserID;
                trk.AdversaryAcknowledged = adversaryAcknowledged;
                this.DataContext.SubmitChanges();
            }
        }

        public void UpdateADVAcknowledged(Tracking tracking, int? adversaryUserID, bool adversaryAcknowledged, bool adversaryAdminAcknowledged)
        {
            tracking.AdversaryUserID = adversaryUserID;
            tracking.AdversaryAcknowledged = adversaryAcknowledged;
            tracking.AdversaryAdminAcknowledged = adversaryAdminAcknowledged;
        }

        public void UpdateTracking(Tracking tracking)
        {
            Tracking trk = this.GetTrackingRecord(tracking.TrackingID);
            if (trk != null)
            {
                trk.AdversaryAcknowledged = tracking.AdversaryAcknowledged;
                trk.AdversaryAdminAcknowledged = tracking.AdversaryAdminAcknowledged;
                trk.AdversaryAdminApproved = tracking.AdversaryAdminApproved;
                trk.AdversaryAdminDate = tracking.AdversaryAdminDate;
                trk.AdversaryRejectionReason = tracking.AdversaryRejectionReason;
                trk.AdversarySignature = tracking.AdversarySignature;
                trk.AdversaryUserID = tracking.AdversaryUserID;
                trk.APAcknowledged = tracking.APAcknowledged;
                trk.APApproved = tracking.APApproved;
                trk.APARSignature = tracking.APARSignature;
                trk.APDate = tracking.APDate;
                trk.APExceptionSignature = tracking.APExceptionSignature;
                trk.APNotes = tracking.APNotes;
                trk.APRejectionReason = tracking.APRejectionReason;
                trk.APSignature = tracking.APSignature;
                trk.APUserID = tracking.APUserID;
                trk.ARApprovalReceived = tracking.ARApprovalReceived;
                trk.CMNumbersSentDate = tracking.CMNumbersSentDate;
                trk.Conflicts = tracking.Conflicts;
                trk.FeeSplits = tracking.FeeSplits;
                trk.FinalCheck = tracking.FinalCheck;
                trk.Locked = tracking.Locked;
                trk.Notes = tracking.Notes;
                trk.Opened = tracking.Opened;
                trk.PGMAcknowledged = tracking.PGMAcknowledged;
                trk.PGMApproved = tracking.PGMApproved;
                trk.PGMARSignature = tracking.PGMARSignature;
                trk.PGMDate = tracking.PGMDate;
                trk.PGMExceptionSignature = tracking.PGMExceptionSignature;
                trk.PGMNotes = tracking.PGMNotes;
                trk.PGMRejectionReason = tracking.PGMRejectionReason;
                trk.PGMSignature = tracking.PGMSignature;
                trk.PGMUserID = tracking.PGMUserID;
                trk.RejectionHold = tracking.RejectionHold;
                trk.RetainerAcknowledged = tracking.RetainerAcknowledged;
                trk.StatusTypeID = tracking.StatusTypeID;
                trk.PGM2Acknowledged = tracking.PGM2Acknowledged;
                trk.PGM2Approved = tracking.PGM2Approved;
                trk.PGM2ARSignature = tracking.PGM2ARSignature;
                trk.PGM2Date = tracking.PGM2Date;
                trk.PGM2ExceptionSignature = tracking.PGM2ExceptionSignature;
                trk.PGM2Notes = tracking.PGM2Notes;
                trk.PGM2RejectionReason = tracking.PGM2RejectionReason;
                trk.PGM2Signature = tracking.PGM2Signature;
                trk.PGM2UserID = tracking.PGM2UserID;
                trk.FinAcknowledged = tracking.FinAcknowledged;
                trk.FinApproved = tracking.FinApproved;
                trk.FinARSignature = tracking.FinARSignature;
                trk.FinDate = tracking.FinDate;
                trk.FinExceptionSignature = tracking.FinExceptionSignature;
                trk.FinNotes = tracking.FinNotes;
                trk.FinRejectionReason = tracking.FinRejectionReason;
                trk.FinSignature = tracking.FinSignature;
                trk.FinUserID = tracking.FinUserID;
                this.DataContext.SubmitChanges();
            }
        }

        public AdversaryBiller GetAdversaryBiller(string employeeCode)
        {
            return AdversaryDataContract.GetAdversaryBiller(employeeCode);
        }

        public Tracking GetTrackingRecord(int trackingID)
        {
            var qry = from tr in this.DataContext.Trackings
                      where tr.TrackingID == trackingID
                      select tr;
            return qry.FirstOrDefault();
        }

        #region Data Context

        private AdversaryDataContext DataContext
        {
            get
            {
                if (_dataContext == null)
                {
                    _dataContext = new AdversaryDataContext();
                }
                return _dataContext;
            }
        }

        #endregion

        #region IDisposable

        public void Dispose()
        {
            if (_dataContext != null)
            {
                _dataContext.Dispose();
                _dataContext = null;
            }

            if (_financialDataContract != null)
            {
                _financialDataContract.Close();
                _financialDataContract = null;
            }
        }

        #endregion
    }
}
