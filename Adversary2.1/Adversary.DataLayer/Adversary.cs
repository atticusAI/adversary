using System.Collections.Generic;
using System.Linq;

namespace Adversary.DataLayer
{
    #region Data Context Construction

    public partial class AdversaryDataContext
    {
        public AdversaryDataContext() : 
            base(AdversaryConnection.AdversaryConnectionString(Properties.Settings.Default.ApplicationName), mappingSource)
        {
            OnCreated();
        }
    }

    #endregion 

    #region Memo Type

    public partial class MemoType
    {
        private static List<MemoType> _memoTypes = null;

        public static List<MemoType> MemoTypes
        {
            get
            {
                if (_memoTypes == null)
                {
                    _memoTypes = GetMemoTypes();
                }
                return _memoTypes;
            }
        }

        private static List<MemoType> GetMemoTypes()
        {
            List<MemoType> lst = null;
            using (AdversaryDataContext dc = new AdversaryDataContext())
            {
                var qry = from mt in dc.MemoTypes
                          select mt;

                lst = qry.ToList();
            }

            return lst;
        }

        private static Dictionary<string, int> _memoTypesDictionary = null;

        public static Dictionary<string, int> MemoTypesDictionary
        {
            get
            {
                if (_memoTypesDictionary == null)
                {
                    _memoTypesDictionary = new Dictionary<string, int>();
                    foreach (MemoType mt in MemoTypes)
                    {
                        _memoTypesDictionary.Add(mt.MemoType1, mt.TrackNumber ?? -1);
                    }
                }
                return _memoTypesDictionary;
            }
        }

        public static int AdversaryRequest
        {
            get
            {
                int t = -1;
                MemoTypesDictionary.TryGetValue("Adversary Request", out t);
                return t;
            }
        }

        public static int? NewClientAndNewMatter
        {
            get
            {
                int t = -1;
                MemoTypesDictionary.TryGetValue("New Client and New Matter", out t);
                return t;
            }
        }

        public static int ExistingClientAndNewMatter
        {
            get
            {
                int t = -1;
                MemoTypesDictionary.TryGetValue("Existing Client and New Matter", out t);
                return t;
            }
        }

        public static int ProBono
        {
            get
            {
                int t = -1;
                MemoTypesDictionary.TryGetValue("Pro Bono", out t);
                return t;
            }
        }

        public static int ProBonoObsolete
        {
            get
            {
                int t = -1;
                MemoTypesDictionary.TryGetValue("Pro Bono (Obsolete)", out t);
                return t;
            }
        }

        public static int LegalWork
        {
            get
            {
                int t = -1;
                MemoTypesDictionary.TryGetValue("H&H Legal Work", out t);
                return t;
            }
        }

        public static int RejectedMatter
        {
            get
            {
                int t = -1;
                MemoTypesDictionary.TryGetValue("Rejected Matter", out t);
                return t;
            }
        }

        public static int TestPages
        {
            get
            {
                int t = -1;
                MemoTypesDictionary.TryGetValue("Test Pages", out t);
                return t;
            }
        } 
    }

    #endregion 

    #region Tracking Status Type

    public partial class TrackingStatusType
    {
        private static List<TrackingStatusType> _trackingStatusTypes = null;

        public static List<TrackingStatusType> TrackingStatusTypes
        {
            get
            {
                if (_trackingStatusTypes == null)
                {
                    _trackingStatusTypes = GetTrackingStatusTypes();
                }
                return _trackingStatusTypes;
            }
        }

        private static List<TrackingStatusType> GetTrackingStatusTypes()
        {
            List<TrackingStatusType> lst = null;

            using (AdversaryDataContext dc = new AdversaryDataContext())
            {
                var qry = from tst in dc.TrackingStatusTypes 
                          select tst;

                lst = qry.ToList();
            }

            return lst;
        }

        private static Dictionary<string, int> _trackingStatusTypesDictionary = null;

        public static Dictionary<string, int> TrackingStatusTypesDictionary
        {
            get
            {
                if (_trackingStatusTypesDictionary == null)
                {
                    _trackingStatusTypesDictionary = new Dictionary<string, int>();
                    foreach (TrackingStatusType tst in TrackingStatusTypes)
                    {
                        _trackingStatusTypesDictionary.Add(tst.Status, tst.StatusTypeID);
                    }
                }
                return _trackingStatusTypesDictionary;
            }
        }

        public static int NA
        {
            get
            {
                int t = -1;
                TrackingStatusTypesDictionary.TryGetValue("N/A", out t);
                return t;
            }
        }

        public static int AdversaryGroupProcessing
        {
            get
            {
                int t = -1;
                TrackingStatusTypesDictionary.TryGetValue("Adversary Group Processing", out t);
                return t;
            }
        }

        public static int SubmittedForApproval
        {
            get
            {
                int t = -1;
                TrackingStatusTypesDictionary.TryGetValue("Submitted for Approval", out t);
                return t;
            }
        }

        public static int ModificationRequested
        {
            get
            {
                int t = -1;
                TrackingStatusTypesDictionary.TryGetValue("Modification Requested", out t);
                return t;
            }
        }

        public static int Completed
        {
            get
            {
                int t = -1;
                TrackingStatusTypesDictionary.TryGetValue("Completed", out t);
                return t;
            }
        }

        public static int Rejected
        {
            get
            {
                int t = -1;
                TrackingStatusTypesDictionary.TryGetValue("Rejected", out t);
                return t;
            }
        }
    }

    #endregion
}
