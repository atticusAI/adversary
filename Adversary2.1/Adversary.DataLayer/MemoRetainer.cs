﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.DataLayer
{
    public class MemoRetainer : IScreeningMemoGet, IScreeningMemoSave
    {
        public MemoRetainer(int screeningMemoID)
        {
            _ScreeningMemoID = screeningMemoID;
        }
        private bool _Retainer;

        public bool Retainer
        {
            get { return _Retainer; }
            set { _Retainer = value; }
        }

        private string _RetainerDescNo;

        public string RetainerDescNo
        {
            get { return _RetainerDescNo; }
            set { _RetainerDescNo = value; }
        }

        private string _RetainerDescYes;

        public string RetainerDescYes
        {
            get { return _RetainerDescYes; }
            set { _RetainerDescYes = value; }
        }

        private string _RetainerDesc;

        public string RetainerDesc
        {
            get { return _RetainerDesc; }
            set { _RetainerDesc = value; }
        }

        public bool Get()
        {
            using (AdversaryDataContext dc = new AdversaryDataContext())
            {
                var m = (from M in dc.Memos
                          where M.ScrMemID == _ScreeningMemoID
                          select M).FirstOrDefault();
                _ScreeningMemoID = m.ScrMemID;
                // _Retainer = (_Retainer != null ? Convert.ToBoolean(m.Retainer) : false);
                _Retainer = (m.Retainer ?? false);
                _RetainerDesc = m.RetainerDesc;
                _RetainerDescNo = m.RetainerDescNo;
                _RetainerDescYes = m.RetainerDescYes;
            }
            return true;
        }

        private int _ScreeningMemoID;
        public int ScreeningMemoID
        {
            get { return _ScreeningMemoID; }
        }

        public bool Save()
        {
            using (AdversaryDataContext dc = new AdversaryDataContext())
            {
                Memo m = (from M in dc.Memos
                          where M.ScrMemID == _ScreeningMemoID
                          select M).FirstOrDefault();
                if (m != null)
                {
                    m.Retainer = _Retainer;
                    m.RetainerDesc = _RetainerDesc;
                    m.RetainerDescNo = _RetainerDescNo;
                    m.RetainerDescYes = _RetainerDescYes;
                    dc.SubmitChanges();
                }
                else
                {
                    return false;
                }
            }
            return true;
        }
    }
}
