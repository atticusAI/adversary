﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web.UI.WebControls;


namespace Adversary.DataLayer
{
    public static class ScreeningExtensionMethods
    {
        private class MemoExtensionProperties
        {
            public bool IsLocked { get; private set; }
            public bool IsResubmit { get; private set; }
            public bool IsApproved { get; private set; }
            public bool IsClientMatterNumberSent { get; private set; }
            public bool IsArchived { get; private set; }
            private readonly Memo _Memo;
            public Memo Memo { get { return _Memo; } }
            public MemoExtensionProperties(Memo memo)
            {
                _Memo = memo;
                if(memo != null)
                {
                    if (memo.Trackings != null && memo.Trackings.Count > 0)
                    {
                        Tracking T = memo.Trackings.FirstOrDefault();
                        this.IsLocked = (T.Locked ?? false);
                        this.IsApproved = (T.AdversaryAdminApproved ?? false);
                        this.IsClientMatterNumberSent = (T.CMNumbersSentDate.HasValue);
                        this.IsResubmit = (T.RejectionHold.HasValue);
                    }
                    //check for archived memos
                    if ((memo.ScrMemID < 100000) && (memo.ScrMemID != 0))
                    {
                        this.IsLocked = true;
                        this.IsArchived = true;
                    }
                }
            }
        }


        public static void FormatRuleMessage(this AllRule rule, string ruleMessage, string ruleMessageInput)
        {
            rule.RuleText = String.Format(ruleMessage, ruleMessageInput);
        }

        public static IEnumerable<T> HH_Sort<T>(this IEnumerable<T> source, string sortExpression)
        {
            string[] sortExprA = sortExpression.Split(' ');
            string paramName = sortExprA.Count() > 0 ? sortExprA[0] : "";
            string sortDirection = sortExprA.Count() > 1 ? sortExprA[1] : "";

            if (!string.IsNullOrWhiteSpace(paramName))
            {
                var param = Expression.Parameter(typeof(T), "item");

                var expr = Expression.Lambda<Func<T, object>>
                    (Expression.Convert(Expression.Property(param, paramName), typeof(object)), param);

                if (sortDirection.ToLower().StartsWith("desc"))
                {
                    return source.AsQueryable<T>().OrderByDescending<T, object>(expr);
                }
                else
                {
                    return source.AsQueryable<T>().OrderBy<T, object>(expr);
                }
            }
            else
            {
                return source;
            }
        }

        public static string HH_AsSqlSearchParm(this string s)
        {
            string parm = "";
            if (!string.IsNullOrWhiteSpace(s))
            {
                parm = s.StartsWith("%") ? s : string.Format("%{0}", s);
                parm = parm.EndsWith("%") ? parm : string.Format("{0}%", parm);
            }
            return parm;
        }

        public static string HH_AsSqlSearchParamStartsWith(this string s)
        {
            string parm = "";
            if (!string.IsNullOrWhiteSpace(s))
            {
                parm = s.EndsWith("%") ? s : string.Format("{0}%", s);
            }
            return parm;
        }

        public static string HH_AsSqlSearchParamEndsWith(this string s)
        {
            string parm = "";
            if (!string.IsNullOrWhiteSpace(s))
            {
                parm = s.StartsWith("%") ? s : string.Format("%{0}", s);
            }
            return parm;
        }

        public static bool HH_IsMemoLocked(this Memo memo)
        {
            MemoExtensionProperties m = new MemoExtensionProperties(memo);
            return m.IsLocked || m.IsArchived;
            //bool isLocked = false;
            //Tracking t = memo.Trackings.FirstOrDefault();
            //if (t != null)
            //{
            //    isLocked = (t.Locked ?? false);
            //}
            //isLocked = (isLocked || (memo.ScrMemID != 0 && memo.ScrMemID < 100000));
            //return isLocked;
        }

        public static bool HH_IsMemoArchived(this Memo memo)
        {
            MemoExtensionProperties m = new MemoExtensionProperties(memo);
            return m.IsArchived;
        }

        public static bool HH_IsDocketingInfoRequired(this Memo memo)
        {
            string pracCode = memo.PracCode;

            bool required = true;

            if (string.IsNullOrWhiteSpace(pracCode))
            {
                required = false;
            }
            else if (pracCode.Contains("E") || pracCode.Contains("T"))
            {
                required = false;
            }
            else if (pracCode == "501L" || pracCode == "502L")
            {
                required = false;
            }

            return required;
        }

        public static bool HH_IsMemoResubmit(this Memo memo)
        {
            bool isResubmit = false;
            if (memo.Trackings != null)
            {
                if (memo.Trackings.Count > 0)
                {
                    if (memo.Trackings[0].RejectionHold.HasValue && memo.Trackings[0].RejectionHold.Value)
                        isResubmit = true;
                }
            }
            return isResubmit;
        }

        public static bool HH_IsMemoApproved(this Memo memo)
        {
            MemoExtensionProperties m = new MemoExtensionProperties(memo);
            return m.IsLocked;

            //bool isApproved = false;
            //if (memo.Trackings != null)
            //{
            //    if (memo.Trackings.Count > 0)
            //    {
            //        isApproved = (memo.Trackings[0].AdversaryAdminApproved.HasValue && memo.Trackings[0].AdversaryAdminApproved.Value);
            //    }
            //}
            //return isApproved;
        }

        public static bool HH_IsClientMatterSent(this Memo memo)
        {
            MemoExtensionProperties m = new MemoExtensionProperties(memo);
            return m.IsClientMatterNumberSent;
            //bool isMatterSent = false;
            //if (memo.Trackings != null)
            //{
            //    if (memo.Trackings.Count > 0)
            //    {
            //        isMatterSent = (memo.Trackings[0].CMNumbersSentDate.HasValue);
            //    }
            //}
            //return isMatterSent;
        }

        //extension method
        public static bool HH_IsRequestComplete(this Adversary.DataLayer.Request request)
        {
            return (request.CompletionDate != null) && !string.IsNullOrEmpty(request.WorkCompletedBy);
        }
        public static bool HH_IsRequestSubmitted(this Adversary.DataLayer.Request request)
        {
            bool submitted = false;
            if (request != null)
            {
                submitted = (request.SubmittalDate.HasValue);
            }
            return submitted;
        }
        public static bool HH_IsRequestWorkBegun(this Adversary.DataLayer.Request request)
        {
            return (request.WorkBegunDate.HasValue && !String.IsNullOrEmpty(request.WorkBegunBy));
        }
        public static void HH_RemoveCompleted(this Adversary.DataLayer.Request request)
        {
            using (AdversaryDataContext adv = new AdversaryDataContext())
            {
                Request RemoveRequest = (from R in adv.Requests where R.ReqID == request.ReqID select R).FirstOrDefault();
                RemoveRequest.CompletionDate = null;
                RemoveRequest.WorkCompletedBy = null;
                adv.SubmitChanges();
            }
        }

    }
}
