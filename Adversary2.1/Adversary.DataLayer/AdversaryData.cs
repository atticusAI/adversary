﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Linq;

namespace Adversary.DataLayer
{
    public class AdversaryData : IDisposable 
    {
        private AdversarySessionService.CacheServiceClient cacheClient;
        public AdversaryData()
        {
            cacheClient = new AdversarySessionService.CacheServiceClient();
        }

        #region Navigation
        public List<VW_UserPageNavigation> GetUserPageNavigation(int userID)
        {
            List<VW_UserPageNavigation> userPageNavigation = null;
                
            using(AdversaryDataContext adv = new AdversaryDataContext())
            {
                userPageNavigation = (from upn in adv.VW_UserPageNavigations
                                      where upn.UserID == userID 
                                        || upn.Role == "Default"
                                      select upn).ToList();
            }

            return userPageNavigation;
        }

        #endregion

        #region Party Status
        public List<AdversaryDataService.PartyStatus> PartyStatusRelated
        {
            get
            {
                string relatedCodes = "101,104,900,211,205,206,207,204,212,219,208,222,202,210,203,201";
                return GetFilteredParties(relatedCodes.Split(','));
            }
        }
        public List<AdversaryDataService.PartyStatus> PartyStatusAdverse
        {
            get
            {
                string adverseCodes = "300,301,306,307,308,311,303,999";
                return GetFilteredParties(adverseCodes.Split(','));
            }
        }
        public List<AdversaryDataService.PartyStatus> PartyStatusAffiliate
        {
            get
            {
                string affiliateStatuses = "102";
                return GetFilteredParties(affiliateStatuses.Split(','));
            }
        }

        // private string _ConnectionString = AdversaryConnection.AdversaryConnectionString();
        private List<AdversaryDataService.PartyStatus> GetFilteredParties(string[] codes)
        {
            List<AdversaryDataService.PartyStatus> _statuses = new List<AdversaryDataService.PartyStatus>();
            List<AdversaryDataService.PartyStatus> _statusList = GetPartyStatusList();
            _statuses = _statusList.Where(p => codes.Contains(p.STATUS_CODE.Trim())).ToList<AdversaryDataService.PartyStatus>();
            return _statuses;
        }
        public List<AdversaryDataService.PartyStatus> GetPartyStatusList()
        {
            List<AdversaryDataService.PartyStatus> parties = new List<AdversaryDataService.PartyStatus>();
            using (AdversaryDataService.AdversaryDataServiceContractClient adv = new AdversaryDataService.AdversaryDataServiceContractClient())
            {
                parties = adv.GetPartyStatus().ToList<AdversaryDataService.PartyStatus>();
            }
            return parties;
        }
        #endregion

        #region Practice Codes

        public List<AdversaryDataService.PracticeCode> GetPracticeGroupsList()
        {
            List<AdversaryDataService.PracticeCode> p = new List<AdversaryDataService.PracticeCode>();
            using (AdversaryDataService.AdversaryDataServiceContractClient adv = new AdversaryDataService.AdversaryDataServiceContractClient())
            {
                p = adv.GetPracticeCodesList().ToList<AdversaryDataService.PracticeCode>();
            }
            return p;
        }

        private List<AdversaryDataService.PracticeCode> _practiceCodes = null;

        public List<AdversaryDataService.PracticeCode> PracticeCodes
        {
            get
            {
                if (this._practiceCodes == null)
                {
                    this._practiceCodes = this.GetPracticeGroupsList();
                }
                return this._practiceCodes;
            }
        }

        public AdversaryDataService.PracticeCode GetPracticeCode(string pracCode)
        {
            return (from pcs in this.PracticeCodes
                    where pcs.matt_cat_code == pracCode
                    select pcs).FirstOrDefault();
        }

        // TODO: 2.1 add a new table for all these code types and load these arrays from the database
        //public static string[] ProBonoPracticeCodes = { "101X", "105X", "105Z", "107X" };
        public static string[] ProBonoPracticeCodes = { "105X", "105Z", "107X" };
        public static string[] LegalWorkPracticeCodes = { "105Z", "107X" };
        public static string[] AffiliatePartyCodes = { "102" };
        public static string[] ReferringPartyCodes = { "902" };
        public static string[] RelatedPartyCodes = { "101", "102", "104", "900", "211", "205", "206", "207", "204", "212", "219", "208", "222", "202", "210", "203", "201" };
        public static string[] AdversePartyCodes = { "300", "311", "306", "307", "308", "303", "302", "301", "999" };
        public static string[] UnusedPracticeCodes = { "101X", "180L", "181L", "182L", "102X" }; // blmcgee@hollandhart.com 2012-10-31 added 102x at request of vabecker@hollandhart.com
        public static string[] PersuasionStrategiesCodes = { "501L", "502L" };

        #endregion

        #region timekeepers
        
        public List<FinancialDataService.FinancialDataEmployee> SearchEmployees(string searchCriteria)
        {
            List<FinancialDataService.FinancialDataEmployee> _employees = new List<FinancialDataService.FinancialDataEmployee>();
            try
            {
                using (FinancialDataService.TimekeeperLookupServiceContractClient tkData = new FinancialDataService.TimekeeperLookupServiceContractClient())
                {
                    _employees = tkData.TimekeeperLookup_SearchEmployees(searchCriteria, FinancialDataService.TimekeeperLookupSearchType.Loose).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
            return _employees;
        }

        public FinancialDataService.FinancialDataEmployee GetEmployee(string criteria)
        {
            FinancialDataService.FinancialDataEmployee emp = new FinancialDataService.FinancialDataEmployee();
            try
            {
                using (FinancialDataService.TimekeeperLookupServiceContractClient tkData = new FinancialDataService.TimekeeperLookupServiceContractClient())
                {
                    emp = tkData.TimekeeperLookup_GetEmployeeByLoginOrCode(criteria);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
            return emp;
        }

        public HRDataService.Employee GetHREmployee(string username, bool isUsername)
        {
            HRDataService.Employee _employee = null;
            try
            {
                using (HRDataService.PeopleDataContractClient peopleData = new HRDataService.PeopleDataContractClient())
                {
                    _employee = peopleData.GetEmployees("", "", "", "", "").ToList()
                        .Where(E => E.CompanyEmailAddress.StartsWith(username.Replace("_", ""))).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _employee ?? new HRDataService.Employee();
        }

        public HRDataService.Employee GetHREmployee(string personID)
        {
            HRDataService.Employee _employee = null;
            try
            {
                using (HRDataService.PeopleDataContractClient peopledata = new HRDataService.PeopleDataContractClient())
                {
                    _employee = peopledata.GetEmployeeByPersonId(personID);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _employee ?? new HRDataService.Employee();
        }
        #endregion

        #region memo
        public Memo CreateMemo(int screeningMemoType)
        {
            using (AdversaryDataContext adv = new AdversaryDataContext())
            {
                Memo m = new Memo();
                
                m.ScrMemType = screeningMemoType;
                adv.Memos.InsertOnSubmit(m);
                adv.SubmitChanges();

                //set up all additional tables
                switch (screeningMemoType)
                {
                    case 1:
                    case 2:
                    case 3:
                    case 5:
                    case 6:
                        NewClient n = new NewClient();
                        Address a = new Address();
                        //ClientContact c = new ClientContact();
                        n.ScrMemID = m.ScrMemID;
                        a.ScrMemID = m.ScrMemID;
                        //c.ScrMemID = m.ScrMemID;
                        adv.NewClients.InsertOnSubmit(n);
                        adv.Addresses.InsertOnSubmit(a);
                        //adv.ClientContacts.InsertOnSubmit(c);
                        break;
                    default:
                        break;
                }

                adv.SubmitChanges();
                return m;
            }
        }

        public Memo GetMemo(int screeningMemoID)
        {
            Memo m = new Memo();
            using (AdversaryDataContext dc = new AdversaryDataContext())
            {
                DataLoadOptions dl = new DataLoadOptions();
                dl.LoadWith<Memo>(M => M.ClientContacts);
                dl.LoadWith<Memo>(M => M.NewClients);
                dl.LoadWith<Memo>(M => M.Addresses);
                dl.LoadWith<Memo>(M => M.Parties);
                dl.LoadWith<Memo>(M => M.Staffings);
                dl.LoadWith<Memo>(M => M.Billings);
                dl.LoadWith<Memo>(M => M.Attachments);
                dl.LoadWith<Memo>(M => M.DocketingTeams);
                dl.LoadWith<Memo>(M => M.FeeSplitStaffs);
                dl.LoadWith<Party>(P => P.PartyAddressInfos);
                dl.LoadWith<Memo>(M => M.OpposingCounsels);
                dl.LoadWith<Memo>(M => M.Trackings);
                dl.LoadWith<Memo>(M => M.ClientMatterNumbers);
                dl.LoadWith<Memo>(M => M.NDriveAccesses);

                dc.LoadOptions = dl;
                m = (from e in dc.Memos
                     where e.ScrMemID == screeningMemoID
                     select e).FirstOrDefault();

                //m.NewClients[0].CName = "New The Beatles";
                //dc.SubmitChanges();
            }
            return m;

            //return (from e in DataContext.Memos
            //        where e.ScrMemID == screeningMemoID
            //        select e).FirstOrDefault();
        }

        public List<Memo> GetOpenMemos(int? statusTypeID, int? userID, bool sortNewestFirst)
        {
            List<Memo> memos = new List<Memo>();
            using (AdversaryDataContext dc = new AdversaryDataContext())
            {
                DataLoadOptions dl = new DataLoadOptions();
                dl.LoadWith<Memo>(M => M.ClientContacts);
                dl.LoadWith<Memo>(M => M.NewClients);
                dl.LoadWith<Memo>(M => M.Addresses);
                dl.LoadWith<Memo>(M => M.Parties);
                dl.LoadWith<Memo>(M => M.Staffings);
                dl.LoadWith<Memo>(M => M.Billings);
                dl.LoadWith<Memo>(M => M.Attachments);
                dl.LoadWith<Memo>(M => M.DocketingTeams);
                dl.LoadWith<Memo>(M => M.FeeSplitStaffs);
                dl.LoadWith<Party>(P => P.PartyAddressInfos);
                dl.LoadWith<Memo>(M => M.OpposingCounsels);
                dl.LoadWith<Memo>(M => M.Trackings);
                dl.LoadWith<Memo>(M => M.ClientMatterNumbers);

                dc.LoadOptions = dl;
                memos = (from e in dc.Memos
                         join t in dc.Trackings on e.ScrMemID equals t.ScrMemID
                         where ((userID == null) || (t.APUserID == userID) || (t.PGMUserID == userID) || (t.AdversaryUserID == userID))
                         && (((statusTypeID == null) && (t.StatusTypeID != null) && (t.StatusTypeID > 0) && (t.StatusTypeID < 100)) ||
                            (t.StatusTypeID == statusTypeID))
                         orderby t.SubmittedOn
                         select e).Take(500).ToList();
                
                if (!sortNewestFirst)
                {
                    memos.Reverse();
                }
            }
            return memos;
        }

        public string GetMemoTypeDescription(Memo memo)
        {
            string memoTypeDescription = "";
            switch (memo.ScrMemType)
            {
                case 1:
                    memoTypeDescription = "New Matter for New Client";
                    break;
                case 2:
                    memoTypeDescription = "New Matter for Existing Client";
                    break;
                case 3:
                    memoTypeDescription = "Pro Bono";
                    break;
                case 5:
                    memoTypeDescription = "Legal Work for H&H or an Employee";
                    break;
                case 6:
                    memoTypeDescription = "Rejected Matter";
                    break;
                default:
                    memoTypeDescription = "Screening memo type ID " + memo.ScrMemType.ToString() + " was not found";
                    break;
            }
            return memoTypeDescription;
        }
    

        public bool IsMemoDocketingRequired(Memo m)
        {
            bool returnval = true;
            if (
                string.IsNullOrEmpty(m.PracCode) ||
                m.PracCode.Contains("E") ||
                m.PracCode.Contains("T") ||
                m.PracCode.Equals("501L") ||
                m.PracCode.Equals("502L")) returnval = false;

            return returnval;
                    
        }

        public List<MemoType> GetMemoTypes()
        {
            using (AdversaryDataContext adv = new AdversaryDataContext())
            {
                return adv.MemoTypes.ToList();
            }
        }
        #endregion

        #region requests
        public Request CreateRequest()
        {
            using (AdversaryDataContext adv = new AdversaryDataContext())
            {
                Request r = new Request();
                adv.Requests.InsertOnSubmit(r);
                adv.SubmitChanges();
                return r;
            }
        }

        public Request CreateRequest(string personID)
        {
            Request r = new Request()
            {
                TypistLogin = personID
            };

            using (AdversaryDataContext adv = new AdversaryDataContext())
            {
                adv.Requests.InsertOnSubmit(r);
                adv.SubmitChanges();                
            }

            return r;
        }

        public Request GetRequest(int requestID)
        {
            Request r = new Request();
            using (AdversaryDataContext dc = new AdversaryDataContext())
            {
                DataLoadOptions dl = new DataLoadOptions();
                dl.LoadWith<Request>(R => R.RequestParties);
                dc.LoadOptions = dl;
                r = (from req in dc.Requests
                     where req.ReqID == requestID
                     select req).FirstOrDefault();
            }
            return r;
        }



        //public List<Request> GetRequests(DateTime minDate)
        //{
        //    List<Request> requests = new List<Request>();
        //    using (AdversaryDataContext dc = new AdversaryDataContext(_ConnectionString))
        //    {
        //        requests = (from R in dc.Requests
        //                    where (R.SubmittalDate.HasValue && R.SubmittalDate >= minDate)
        //                    select R).ToList();
        //    }
        //    return requests;
        //}

        //public List<Request> GetRequests(DateTime minDate, bool pendingAndCompleted)
        //{
        //    if (pendingAndCompleted)
        //        return GetRequests(minDate);
        //    else
        //        return GetRequests(minDate).Where(R => !R.HH_IsRequestComplete()).ToList();
        //}

        //public List<Request> GetRequests(DateTime minDate, bool pendingAndCompleted, string userID)
        //{
        //    if (userID == null)
        //    {
        //        return GetRequests(minDate, pendingAndCompleted);
        //    }
        //    else
        //    {
        //        return GetRequests(minDate, pendingAndCompleted).Where(R => R.TypistLogin == userID).ToList();
        //    }
        //}

        public IQueryable<Request> GetRequestQuery(AdversaryDataContext dataContext, DateTime minDate, bool includeCompleted, bool includeUnsubmitted, string userID)
        {
            var qry = from r in dataContext.Requests
                      select r;

            if (!includeUnsubmitted)
            {
                qry = from r in qry
                      where (r.SubmittalDate.HasValue && r.SubmittalDate >= minDate)
                      select r;
            }
            else
            {
                qry = from r in qry
                      where !(r.SubmittalDate.HasValue)
                      select r;
            }

            if (!includeCompleted)
            {
                qry = from r in qry
                      where !(r.CompletionDate.HasValue)
                      select r;
            }

            if (!string.IsNullOrWhiteSpace(userID))
            {
                qry = from r in qry
                      where r.TypistLogin == userID
                      select r;
            }

            return qry;
        }

        public List<Request> GetRequests(DateTime minDate, bool includePending, bool includeUnsubmitted, string userID)
        {
            List<Request> requests = null;

            using (AdversaryDataContext dc = new AdversaryDataContext())
            {
                var qry = this.GetRequestQuery(dc, minDate, includePending, includeUnsubmitted, userID);
                requests = qry.ToList();
            }

            return requests;
        }

        public List<Request> GetDailyList(DateTime startDate, DateTime endDate)
        { 
            return GetDailyList(startDate, endDate, false, string.Empty); 
        }

        public List<Request> GetDailyList(DateTime startDate, DateTime endDate, bool includeConfidential)
        { 
            return GetDailyList(startDate, endDate, includeConfidential, string.Empty); 
        }

        public List<Request> GetDailyList(DateTime startDate, DateTime endDate, bool includeConfidential, string workBegunBy)
        {
            IQueryable<Request> qry = from r in DataContext.Requests
                                      where (r.SubmittalDate.HasValue && (r.SubmittalDate >= startDate) && (r.SubmittalDate <= endDate))
                                        && r.WorkCompletedBy != null && r.WorkCompletedBy.Length > 0
                                        && r.CompletionDate != null
                                      orderby r.ClientCategory descending,
                                        r.SubmittalDate
                                      select r;

            if (!String.IsNullOrEmpty(workBegunBy))
            {
                qry = from r in qry
                      where (r.WorkBegunBy ?? "") == workBegunBy
                      select r;
            }

            if (!includeConfidential)
            {
                qry = from r in qry
                      where (r.OKforNewMatter ?? false) == true
                      select r;
            }

            return qry.ToList();
        }

        #endregion

        #region Docketing Team
        public List<DocketingTeam> GetDocketingTeam(int screeningMemoID)
        {
            List<DocketingTeam> _Team = new List<DocketingTeam>();
            try
            {
                using (AdversaryDataContext data = new AdversaryDataContext())
                {
                    _Team = (from T in data.DocketingTeams
                             where T.ScrMemID == screeningMemoID
                             select T).ToList<DocketingTeam>();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _Team;
            //return (from T in DataContext.DocketingTeams
            //        where T.ScrMemID == screeningMemoID
            //        select T).ToList<DocketingTeam>();
        }

        #endregion

        #region N Drive Types
        private List<NDriveType> _nDriveTypes = null;

        public List<NDriveType> NDriveTypes
        {
            get
            {
                if (this._nDriveTypes == null)
                {
                    this._nDriveTypes = this.GetNDriveTypes();
                }
                return this._nDriveTypes;
            }
        }

        public List<NDriveType> GetNDriveTypes()
        {
            List<NDriveType> _returnList = new List<NDriveType>();
            try
            {
                using (AdversaryDataContext dc = new AdversaryDataContext())
                {
                    _returnList = (from N in dc.NDriveTypes
                                   select N).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _returnList;
            //return DataContext.NDriveTypes.ToList();
        }

        public string GetNDriveTypeDescription(int nDriveTypeID)
        {
            return (from n in this.NDriveTypes
                    where n.NDriveTypeID == nDriveTypeID
                    select n.NDriveTypeDescription).FirstOrDefault();
        }

        #endregion

        #region Offices
        public List<HRDataService.FirmOffice> GetFirmOffices()
        {
            List<HRDataService.FirmOffice> _offices = new List<HRDataService.FirmOffice>();
            try
            {
                using (HRDataService.FirmDataContractClient firmData = new HRDataService.FirmDataContractClient())
                {
                    _offices = firmData.GetFirmOffices().ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _offices;
        }

        private List<HRDataService.FirmOffice> _firmOffices = null;

        public List<HRDataService.FirmOffice> FirmOffices
        {
            get
            {
                if (this._firmOffices == null)
                {
                    this._firmOffices = this.GetFirmOffices();
                }
                return this._firmOffices;
            }
        }

        public HRDataService.FirmOffice GetFirmOffice(string locationCode)
        {
            return (from off in this.FirmOffices
                    where off.LocationCode == locationCode
                    select off).FirstOrDefault();
        }

        public HRDataService.FirmOffice GetAttorneyOffice(string respAttID)
        {
            HRDataService.FirmOffice office = null;
            if (!string.IsNullOrWhiteSpace(respAttID))
            {
                Adversary.DataLayer.FinancialDataService.FinancialDataEmployee employee = this.GetEmployee(respAttID);
                if (employee != null)
                {
                    office = (from off in this.FirmOffices
                              where ((employee.OfficeCode == "00" && off.City == "Denver") ||
                              (off.LocationCode == employee.OfficeCode))
                              select off).FirstOrDefault();
                }
            }
            return office;
        }

        public HRDataService.FirmOffice GetMemoOffice(Memo memo)
        {
            Adversary.DataLayer.HRDataService.FirmOffice office = this.GetAttorneyOffice(memo.RespAttID);
            if (office == null)
            {
                office = this.GetFirmOffice(memo.OrigOffice);
            }
            return office;
        }



        #endregion

        #region Clients
        public List<FinancialDataService.Client> SearchClients(string searchCriteria)
        {
            List<FinancialDataService.Client> _clients = new List<FinancialDataService.Client>();
            try
            {
                using (FinancialDataService.FinancialDataServiceContractClient fData = new FinancialDataService.FinancialDataServiceContractClient())
                {
                    _clients = fData.SearchClients(searchCriteria).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _clients;
        }

        public List<ClientMatterLookupService.ClientAddress> GetClientAddresses(string clientCode)
        {
            List<ClientMatterLookupService.ClientAddress> _addresses = new List<ClientMatterLookupService.ClientAddress>();
            using (ClientMatterLookupService.ClientMatterLookupContractClient c = new ClientMatterLookupService.ClientMatterLookupContractClient())
            {
                _addresses = c.GetClientAddresses(clientCode).ToList();
            }
            return _addresses;
        }

        public List<ClientMatterLookupService.ClientAddress> GetAllClientAddresses(string clientCode)
        {
            List<ClientMatterLookupService.ClientAddress> _addresses = new List<ClientMatterLookupService.ClientAddress>();
            using (ClientMatterLookupService.ClientMatterLookupContractClient c = new ClientMatterLookupService.ClientMatterLookupContractClient())
            {
                _addresses = c.GetAllClientAddresses(clientCode).ToList();
            }
            return _addresses;
        }
        #endregion

        #region Pro Bono Types
        public List<ProBonoType> GetProBonoTypes()
        {
            List<ProBonoType> _types = new List<ProBonoType>();
            try
            {
                using (AdversaryDataContext dc = new AdversaryDataContext())
                {
                    _types = dc.ProBonoTypes.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _types;
            //return DataContext.ProBonoTypes.ToList();
        }
        #endregion

        #region Tracking Status Types
        public List<TrackingStatusType> GetTrackingStatusTypes()
        {
            List<TrackingStatusType> _TrackingTypes = new List<TrackingStatusType>();
            try
            {
                using (AdversaryDataContext dc = new AdversaryDataContext())
                {
                    _TrackingTypes = dc.TrackingStatusTypes.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _TrackingTypes;
        }
        #endregion

        #region Adversary Group/PGM/AP methods
        public List<AdminLogin> GetAllAPs()
        {
            var query = (from A in DataContext.AdminLogins
                         where A.IsAP.HasValue && A.IsAP.Value
                         select A).ToList();
            return query;
        }

        public List<AdminLogin> GetAllPGMs()
        {
            var query = (from P in DataContext.AdminLogins
                         where P.IsPGM.HasValue && P.IsPGM.Value
                         select P).ToList();
            return query;
        }

        public List<AdminLogin> GetAdversaryMembers()
        {
            //get from cache if it exists
            
            //if (cacheClient.GetObjectByKey("AdversaryMembers") == null)
            //{
                List<AdminLogin> adminLogins = (from A in DataContext.AdminLogins
                                                where (A.IsAdversary.HasValue && A.IsAdversary.Value) ||
                                                (A.IsAdversaryAdmin.HasValue && A.IsAdversaryAdmin.Value)
                                                select A).ToList();
                //cacheClient.PlaceObject(adminLogins, "AdversaryMembers");

            //}


            //return (List<AdminLogin>)cacheClient.GetObjectByKey("AdversaryMembers");
                return adminLogins;
        }

        public List<AdminLogin> GetAdversaryGroupAdmins()
        {
            var query = (from A in DataContext.AdminLogins
                         where (A.IsAdversary.HasValue && A.IsAdversary.Value) &&
                         (A.IsAdversaryAdmin.HasValue && A.IsAdversaryAdmin.Value)
                         select A).ToList();
            return query;
        }

        public List<AdminLogin> GetAdversaryAdmins()
        {
            return (from A in this.DataContext.AdminLogins
                    where (A.IsAdversary ?? false)
                    select A).ToList();
        }

        public AdminLogin GetAdminLogin(string login)
        {
            return (from al in this.DataContext.AdminLogins
                    where al.Login == login
                    select al).FirstOrDefault();
        }

        public AdminLogin GetAdminLogin(int userID)
        {
            return (from al in this.DataContext.AdminLogins
                    where al.UserID == userID
                    select al).FirstOrDefault();
        }

        public List<AdminLogin> SearchAdminLogins(string searchText)
        {
            return (from al in this.DataContext.AdminLogins
                    where al.Login.Contains(searchText)
                    orderby al.Login 
                    select al).ToList();
        }

        #endregion

        #region AR Approval Users
        public List<ARApprovalUser> GetARApprovalUsers()
        {
            return DataContext.ARApprovalUsers.ToList<ARApprovalUser>();
        }
        #endregion

        #region attachment metadata
        public Attachment GetAttachment(int attachmentID)
        {
            return DataContext.Attachments.Where(att => att.AttachmentID == attachmentID).FirstOrDefault();
        }
        #endregion

        #region Memo Billers
        public HRDataService.Employee GetBiller(string billingAttorneyEmployeeCode)
        {
            Adversary.DataLayer.AdversaryDataService.AdversaryBiller _biller = null;
            HRDataService.Employee _returnBiller = null;

            using (Adversary.DataLayer.AdversaryDataService.AdversaryDataServiceContractClient _advClient = 
                        new AdversaryDataService.AdversaryDataServiceContractClient())
            {
                _biller = _advClient.GetAdversaryBiller(billingAttorneyEmployeeCode);    
            }
            
            if (_biller != null)
            {
                using (HRDataService.PeopleDataContractClient _pplClient = new HRDataService.PeopleDataContractClient())
                {
                    _returnBiller = _pplClient.GetEmployeeByPersonId(_biller.EmployeeCode);
                }
            }

            return _returnBiller;
        }
        #endregion

        #region #IDisposable

        private AdversaryDataContext __dataContext = null;

        private AdversaryDataContext DataContext
        {
            get
            {
                if (this.__dataContext == null)
                {
                    this.__dataContext = new AdversaryDataContext();
                }
                return this.__dataContext;
            }
        }

       
        public void Dispose()
        {
            if (this.__dataContext != null)
            {
                this.__dataContext.Dispose();
                this.__dataContext = null;
            }
        }

        #endregion
    }
}
