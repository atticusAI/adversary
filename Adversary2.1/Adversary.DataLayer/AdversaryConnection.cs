﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Adversary.DataLayer.ConnectionManagerService;

namespace Adversary.DataLayer
{
    internal static class AdversaryConnection
    {
        internal static string AdversaryConnectionString(string applicationName)
        {
            string connectionString = string.Empty;
            using (ConnectionManagerServiceClient cm = new ConnectionManagerServiceClient())
            {
                connectionString = cm.GetConnectionString(applicationName, false);
            }
            return connectionString;
        }
    }
}
