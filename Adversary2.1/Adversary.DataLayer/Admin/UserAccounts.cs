﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Linq;

namespace Adversary.DataLayer.Admin
{
    public class UserAccounts
    {
        public List<AdminLogin> GetAdminLogins()
        {
            using (AdversaryDataContext dc = new AdversaryDataContext())
            {
                DataLoadOptions dl = new DataLoadOptions();
                dl.LoadWith<AdminLogin>(A => A.AdminPracCodes);

                dc.LoadOptions = dl;
                return dc.AdminLogins.ToList<AdminLogin>();
            }
            
        }

        public bool DeleteAdminLogin(int userID)
        {
            using (AdversaryDataContext dc = new AdversaryDataContext())
            {
                AdminLogin _deletedLogin = dc.AdminLogins.Where(AL => AL.UserID.Equals(userID)).Single();
                if (_deletedLogin == null) return false;
                if(_deletedLogin.AdminPracCodes.Count > 0)
                    dc.AdminPracCodes.DeleteAllOnSubmit(_deletedLogin.AdminPracCodes);
                dc.AdminLogins.DeleteOnSubmit(_deletedLogin);
                dc.SubmitChanges();
            }
            return true;
        }

        public AdminLogin CreateAdminLogin(AdminLogin _newLogin)
        {
            using (AdversaryDataContext dc = new AdversaryDataContext())
            {
                dc.AdminLogins.InsertOnSubmit(_newLogin);
                dc.SubmitChanges();
                return _newLogin;
            }
            
        }

        public AdminLogin ChangeAdminLogin(AdminLogin _changedLogin)
        {
            using (AdversaryDataContext dc = new AdversaryDataContext())
            {
                //remove all the associated practice codes, they will be re-added.
                AdminLogin __existingLogin = dc.AdminLogins.Where(AL => AL.UserID == _changedLogin.UserID).FirstOrDefault();
                __existingLogin.Office = _changedLogin.Office;
                __existingLogin.IsAP = _changedLogin.IsAP;
                __existingLogin.IsAdversary = _changedLogin.IsAdversary;
                __existingLogin.IsPGM = _changedLogin.IsPGM;
                __existingLogin.IsAdversaryAdmin = _changedLogin.IsAdversaryAdmin;
                dc.SubmitChanges();
                
                dc.AdminPracCodes.DeleteAllOnSubmit(__existingLogin.AdminPracCodes);
                dc.SubmitChanges();

                __existingLogin.AdminPracCodes.Clear();
                __existingLogin.AdminPracCodes.AddRange(_changedLogin.AdminPracCodes);
                
                dc.AdminPracCodes.InsertAllOnSubmit(__existingLogin.AdminPracCodes);
                dc.SubmitChanges();


                return _changedLogin;
            }
            
        }
        
    }

    

}
