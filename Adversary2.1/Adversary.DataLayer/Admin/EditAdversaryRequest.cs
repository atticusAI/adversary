﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.DataLayer.Admin
{
    public class EditAdversaryRequest : IDisposable
    {
        public void SetPending(int requestID)
        {
            DataLayer.Request request = this.GetRequest(requestID);
            if (request != null)
            {
                request.CompletionDate = null;
                request.WorkCompletedBy = null;
                this.DataContext.SubmitChanges();
            }
        }

        public void UpdateRequest(DataLayer.Request adversaryRequest)
        {
            DataLayer.Request request = this.GetRequest(adversaryRequest.ReqID);
            if (request != null)
            {
                // request.AccountingFirm = adversaryRequest.AccountingFirm;
                request.AdverseParties = adversaryRequest.AdverseParties;
                request.AffiliatesOfAdverseParties = adversaryRequest.AffiliatesOfAdverseParties;
                request.AttyLogin = adversaryRequest.AttyLogin;
                // request.Bankruptcy = adversaryRequest.Bankruptcy;
                request.ClientAddress = adversaryRequest.ClientAddress;
                request.ClientCategory = adversaryRequest.ClientCategory;
                request.ClientMatter = adversaryRequest.ClientMatter;
                request.ClientName = adversaryRequest.ClientName;
                // request.Communications = adversaryRequest.Communications;
                request.CompletionDate = adversaryRequest.CompletionDate;
                request.DeadlineDate = adversaryRequest.DeadlineDate;
                // request.ElectricUtility = adversaryRequest.ElectricUtility;
                // request.Government = adversaryRequest.Government;
                // request.IndianTribe = adversaryRequest.IndianTribe;
                // request.Insurance = adversaryRequest.Insurance;
                // request.LawFirm = adversaryRequest.LawFirm;
                // request.LegalFees = adversaryRequest.LegalFees;
                // request.NoneOfTheAbove = adversaryRequest.NoneOfTheAbove;
                // request.OilGas = adversaryRequest.OilGas;
                request.OKforNewMatter = adversaryRequest.OKforNewMatter;
                request.OpposingCounsel = adversaryRequest.OpposingCounsel;
                request.OtherClientAffiliates = adversaryRequest.OtherClientAffiliates;
                request.PartiesToMediationArbitration = adversaryRequest.PartiesToMediationArbitration;
                // request.Pharm = adversaryRequest.Pharm;
                request.PracticeCode = adversaryRequest.PracticeCode;
                request.Priority = adversaryRequest.Priority;
                request.RelatedCoDefendants = adversaryRequest.RelatedCoDefendants;
                request.RelatedParties = adversaryRequest.RelatedParties;
                request.RequestParties = adversaryRequest.RequestParties;
                // request.SkiResort = adversaryRequest.SkiResort;
                request.SubmittalDate = adversaryRequest.SubmittalDate;
                request.TypistLogin = adversaryRequest.TypistLogin;
                request.WorkBegunBy = adversaryRequest.WorkBegunBy;
                request.WorkBegunDate = adversaryRequest.WorkBegunDate;
                request.WorkCompletedBy = adversaryRequest.WorkCompletedBy;
                this.DataContext.SubmitChanges();
            }
        }

        public DataLayer.Request GetRequest(int requestID)
        {
            return (from r in this.DataContext.Requests
                    where r.ReqID == requestID 
                    select r).FirstOrDefault();
        }

        private AdversaryDataContext _dataContext = null;

        public AdversaryDataContext DataContext
        {
            get
            {
                if (this._dataContext == null)
                {
                    this._dataContext = new AdversaryDataContext();
                }
                return this._dataContext;
            }
        }

        public void Dispose()
        {
            if (this._dataContext != null)
            {
                this._dataContext.Dispose();
                this._dataContext = null;
            }
        }
    }
}
