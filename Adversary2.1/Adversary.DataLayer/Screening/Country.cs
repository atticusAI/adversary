﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.DataLayer.Screening
{
    public class Country
    {
        #region Properties

        private int _countryID;

        public int CountryID
        {
            get { return this._countryID; }
        }

        private string _code;

        public string Code
        {
            get { return this._code; }
        }

        private string _name;

        public string Name
        {
            get { return this._name; }
        }

        private bool _primary;

        public bool Primary
        {
            get { return this._primary; }
        }

        #endregion

        #region Construction

        public Country(int countryID, string code, string name, bool primary)
        {
            this._countryID = countryID;
            this._code = code;
            this._name = name;
            this._primary = primary;
        }

        #endregion
    }

    public class CountryCollection : IDisposable, ICollection<Country>
    {
        #region Data Context

        private AdversaryDataContext _dataContext = null;

        private AdversaryDataContext DataContext
        {
            get
            {
                if (this._dataContext == null)
                {
                    this._dataContext = new AdversaryDataContext();
                }
                return this._dataContext;
            }
        }

        #endregion

        #region IDisposable

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this._dataContext != null)
                {
                    this._dataContext.Dispose();
                    this._dataContext = null;
                }
            }
        }

        #endregion

        #region Storage

        private IEnumerable<Country> _countries = null;

        public IEnumerable<Country> Countries
        {
            get
            {
                if (this._countries == null)
                {
                    this._countries = this.LoadAll();
                }
                return this._countries;
            }
        }

        #endregion

        #region Lookup 

        public string LookupCode(string name)
        {
            return (from cn in this.Countries
                    where String.Equals(cn.Name, name, StringComparison.CurrentCultureIgnoreCase)
                    select cn.Code).FirstOrDefault();
        }

        #endregion

        #region Load

        private IEnumerable<Country> LoadAll()
        {
            var qry = from cn in this.DataContext.Countries
                      orderby cn.SortOrder
                      select cn;

            foreach (DataLayer.Country c in qry)
            {
                yield return new Country(c.CountryID, c.Code, c.Name, (c.IsPrimary ?? false));
            }
        }

        #endregion

        #region ICollection

        public void Add(Country item)
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(Country item)
        {
            return this.Countries.Contains(item);
        }

        public void CopyTo(Country[] array, int arrayIndex)
        {
            this.Countries.ToList().CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return this.Countries.Count(); }
        }

        public bool IsReadOnly
        {
            get { return true; }
        }

        public bool Remove(Country item)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<Country> GetEnumerator()
        {
            return this.Countries.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.Countries.GetEnumerator();
        }

        #endregion
    }
}
