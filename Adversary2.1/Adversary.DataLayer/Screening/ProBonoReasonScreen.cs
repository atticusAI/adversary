﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.DataLayer
{
    public class ProBonoReasonScreen : IScreeningMemoGet, IScreeningMemoSave, IDisposable
    {
        private string _proBonoReason;

        public string ProBonoReason
        {
            get
            {
                return this._proBonoReason;
            }
            set 
            { 
                this._proBonoReason = value; 
            }
        }

        private bool? _proBonoLimitedMeans;

        public bool? ProBonoLimitedMeans
        {
            get
            {
                return this._proBonoLimitedMeans;
            }
            set
            {
                this._proBonoLimitedMeans = value;
            }
        }

        private AdversaryDataContext _adversaryContext = null;
        private AdversaryDataContext AdversaryContext
        {
            get
            {
                if (_adversaryContext == null)
                    _adversaryContext = new AdversaryDataContext();
                return _adversaryContext;
            }
        }

        public ProBonoReasonScreen(int screeningMemoID)
        {
            this._screeningMemoID = screeningMemoID;
        }

        public void Dispose()
        {
            if (_adversaryContext != null)
                _adversaryContext.Dispose();
            _adversaryContext = null;
        }

        public bool Save()
        {
            var qry = from M in AdversaryContext.Memos
                      where M.ScrMemID == _screeningMemoID
                      select M;
            
            Memo m = qry.FirstOrDefault();

            if (m != null)
            {
                m.ProBonoReason = this._proBonoReason;
                m.ProBonoLimitedMeans = this._proBonoLimitedMeans;
                AdversaryContext.SubmitChanges();
            }
            else
            {
                return false;
            }
            return true;
        }

        private int _screeningMemoID;

        public int ScreeningMemoID
        {
            get 
            {
                return this._screeningMemoID;
            }
        }

        public bool Get()
        {
            var qry = from M in AdversaryContext.Memos
                      where M.ScrMemID == this._screeningMemoID
                      select M;

            Memo m = qry.FirstOrDefault();

            if (m != null)
            {
                this._proBonoReason = m.ProBonoReason;
                this._proBonoLimitedMeans = m.ProBonoLimitedMeans;
            }
            else
            {
                return false;
            }
            return true;
        }
    }
}
