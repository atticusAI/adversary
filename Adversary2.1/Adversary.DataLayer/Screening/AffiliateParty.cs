﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.DataLayer
{
    public class AffiliateParty : PartyBase, IScreeningMemoGet, IScreeningMemoSave
    {
        public AffiliateParty(int screeningMemoID)
        {
            this._ScreeningMemoID = screeningMemoID;
        }

        public bool Get()
        {
            return false;
        }
        public Party GetParty(int partyID)
        {
            Party thisParty = new Party();
            using (AdversaryDataContext dc = new AdversaryDataContext())
            {
                thisParty = (from pty in dc.Parties
                     where pty.PartyID == partyID
                     select pty).FirstOrDefault();
            }
            return thisParty;
        }

        private int _ScreeningMemoID;
        public int ScreeningMemoID
        {
            get { return _ScreeningMemoID; }
        }

        //private int _PartyID;
        //public int PartyID
        //{
        //    get { return _PartyID; }
        //    set { _PartyID = value; }
        //}

        //private string _PartyName;
        //public string PartyName
        //{
        //    get { return _PartyName; }
        //    set { _PartyName = value; }
        //}

        //private string _PartyType;
        //public string PartyType
        //{
        //    get { return _PartyType; }
        //    set { _PartyType = value; }
        //}
        
        //private string _PartyRelationshipCode;
        //public string PartyRelationshipCode
        //{
        //    get { return _PartyRelationshipCode; }
        //    set { _PartyRelationshipCode = value; }
        //}

        public override bool Save()
        {
            using (AdversaryData advData = new AdversaryData())
            {
                Party p;
                using (AdversaryDataContext dc = new AdversaryDataContext())
                {
                    p = (from pty in dc.Parties 
                         where pty.PartyID == this.PartyID 
                         select pty).FirstOrDefault();

                    if (p != null)
                    {
                        p.PartyType = this.PartyType;
                        p.PartyID = this.PartyID;
                        p.PartyFName = this.PartyFName;
                        p.PartyMName = this.PartyMName;
                        p.PartyLName = this.PartyLName;
                        p.PartyOrganization = this.PartyOrganization;
                        p.PartyRelationshipCode = this.PartyRelationshipCode;
                        p.PartyRelationshipName = advData.GetPartyStatusList().Where(I => I.STATUS_CODE.Equals(this.PartyRelationshipCode))
                            .FirstOrDefault()
                            .DESCRIPTION;
                        dc.SubmitChanges();
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        //public bool SaveGroup(List<Party> partyList)
        //{
        //    using (AdversaryDataContext dc = new AdversaryDataContext(AdversaryConnection.AdversaryConnectionString()))
        //    {
        //        dc.Parties.InsertAllOnSubmit(partyList);
        //        dc.SubmitChanges();
        //    }
        //    return true;
        //}
    }
}
