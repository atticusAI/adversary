﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.DataLayer
{
    public class MatterInformation : IScreeningMemoSave, IScreeningMemoGet
    {

        #region read write properties 

        private string _MatterName;

        public string MatterName
        {
            get { return _MatterName; }
            set { _MatterName = value; }
        }

        private string _WorkDescription;

        public string WorkDescription
        {
            get { return _WorkDescription; }
            set { _WorkDescription = value; }
        }

        private string _PracticeTypeCode;

        public string PracticeTypeCode
        {
            get { return _PracticeTypeCode; }
            set { _PracticeTypeCode = value; }
        }

        private string _SecondPracticeTypeCode;

        public string SecondPracticeTypeCode
        {
            get { return _SecondPracticeTypeCode; }
            set { _SecondPracticeTypeCode = value; }
        }

        private string _FeeEstimate;

        public string FeeEstimate
        {
            get { return _FeeEstimate; }
            set { _FeeEstimate = value; }
        }

        private bool _ContingencyFee;

        public bool ContingencyFee
        {
            get { return _ContingencyFee; }
            set { _ContingencyFee = value; }
        }

        private bool _NDrive;

        public bool NDrive
        {
            get { return _NDrive; }
            set { _NDrive = value; }
        }

        private int _NDriveType;

        public int NDriveType
        {
            get { return _NDriveType; }
            set { _NDriveType = value; }
        }

        //Memo.ClientNumber
        private string _ClientCode;

        public string ClientCode
        {
            get { return _ClientCode; }
            set { _ClientCode = value; }
        }

        #endregion

        #region read only properties

        private int _ScreeningMemoID;
        public int ScreeningMemoID
        {
            get { return _ScreeningMemoID; }
        }

        #endregion

        #region construction

        public MatterInformation(int screeningMemoID)
        {
            this._ScreeningMemoID = screeningMemoID;
        }

        #endregion

        #region IScreeningMemoSave

        public bool Save()
        {
            using (AdversaryDataContext dc = new AdversaryDataContext())
            {
                Memo m = (from M in dc.Memos
                          where M.ScrMemID == _ScreeningMemoID
                          select M).FirstOrDefault();
                if (m != null)
                {
                    m.MatterName = _MatterName;
                    m.WorkDesc = _WorkDescription;
                    m.ContingFee = _ContingencyFee;
                    m.NDrive = _NDrive;

                    if (_NDriveType >= -1)
                        m.NDriveType = _NDriveType;
                    else
                        m.NDriveType = null;

                    m.PracCode = _PracticeTypeCode;
                    m.SecondPracCode = _SecondPracticeTypeCode;

                    m.EstFees = _FeeEstimate;
                    m.ClientNumber = _ClientCode;

                    // 11/5/2012 blmcgee@hollandhart.com 
                    // existing client not a company or individual
                    m.Company = null;
                    m.Individual = null;

                    dc.SubmitChanges();
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        #endregion

        #region IScreeningMemoGet

        public bool Get()
        {
            throw new NotImplementedException();
        }

        #endregion

    }
}
