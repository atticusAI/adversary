﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.DataLayer.Screening
{
    public class MemoConflict : IDisposable
    {
        #region Data Context

        private AdversaryDataContext _dataContext = null;

        private AdversaryDataContext DataContext
        {
            get
            {
                if (this._dataContext == null)
                {
                    this._dataContext = new AdversaryDataContext();
                }
                return this._dataContext;
            }
        }

        #endregion

        #region Construction

        public MemoConflict(int scrMemID, int conflictID, bool answer, string description, string name)
        {
            this._scrMemID = scrMemID;
            this._conflictID = conflictID;
            this._answer = answer;
            this._description = description;
            this._name = name;
        }

        #endregion

        #region Properties

        private int _scrMemID;

        public int ScrMemID
        {
            get { return this._scrMemID; }
        }

        private int _conflictID;

        public int ConflictID
        {
            get { return this._conflictID; }
        }

        private bool _answer;

        public bool Answer
        {
            get { return this._answer; }
            set { this._answer = value; }
        }

        private string _name;

        public string Name
        {
            get { return this._name; }
        }

        private string _description;

        public string Description
        {
            get { return this._description; }
        }

        #endregion

        #region Update

        /// <summary>
        /// call the database and update this record
        /// </summary>
        public void Update()
        {
            var qry = from mc in this.DataContext.MemoConflicts
                      where mc.ScrMemID == this.ScrMemID
                      && mc.ConflictID == this.ConflictID
                      select mc;

            DataLayer.MemoConflict dmc = qry.FirstOrDefault();

            if (dmc != null)
            {
                dmc.Answer = this.Answer;
                this.DataContext.SubmitChanges();
            }
        }

        /// <summary>
        /// call the database if the answer property is changed
        /// if not then don't bother
        /// </summary>
        public void Update(bool answer)
        {
            bool changed = (this.Answer != answer);
            this.Answer = answer;
            if (changed) this.Update();
        }

        #endregion

        #region IDisposable

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this._dataContext != null)
                {
                    this._dataContext.Dispose();
                    this._dataContext = null;
                }
            }
        }

        #endregion
    }

    public class MemoConflictCollection : IDisposable, ICollection<MemoConflict>
    {
        #region Data Context

        private AdversaryDataContext _dataContext = null;

        private AdversaryDataContext DataContext
        {
            get
            {
                if (this._dataContext == null)
                {
                    this._dataContext = new AdversaryDataContext();
                }
                return this._dataContext;
            }
        }

        #endregion

        #region Construction

        private readonly int _scrMemID;
        private readonly int _trackNumber;

        public MemoConflictCollection(int scrMemID, int trackNumber)
        {
            this._scrMemID = scrMemID;
            this._trackNumber = trackNumber;
        }

        #endregion 

        #region Storage

        private List<MemoConflict> _conflicts = null;

        private List<MemoConflict> Conflicts
        {
            get
            {
                if (this._conflicts == null || this._conflicts.Count == 0)
                {
                    this._conflicts = this.Load();
                }
                return this._conflicts;
            }
        }

        #endregion

        #region Load and Create

        /// <summary>
        /// if any conflicts records exist for this memo then load them up
        /// if not create them then load them up
        /// </summary>
        private List<MemoConflict> Load()
        {
            int count = (from mc in this.DataContext.MemoConflicts
                         where mc.ScrMemID == this._scrMemID
                         select mc).Count();

            bool available = count > 0 || this.Create();

            List<MemoConflict> cons = new List<MemoConflict>();

            var qry = from mc in this.DataContext.MemoConflicts
                      where mc.ScrMemID == this._scrMemID
                      && mc.Conflict.Active == true
                      orderby mc.Conflict.SortOrder 
                      select mc;

            foreach (DataLayer.MemoConflict c in qry)
            {
                cons.Add(new MemoConflict(c.ScrMemID, c.ConflictID, c.Answer, c.Conflict.Description, c.Conflict.Name));
            }

            return cons;
        }
        
        /// <summary>
        /// attempt to create conflicts records from the metadata
        /// </summary>
        /// <returns>true if it worked otherwise false</returns>
        private bool Create()
        {
            bool created = false;

            List<DataLayer.MemoConflict> dmcs = new List<DataLayer.MemoConflict>();

            var qry = from cons in this.DataContext.Conflicts
                      where cons.TrackNumber == this._trackNumber
                      select cons;

            foreach (DataLayer.Conflict c in qry)
            {
                dmcs.Add(new DataLayer.MemoConflict()
                {
                    ScrMemID = this._scrMemID,
                    ConflictID = c.ConflictID,
                    Answer = false
                });
            }

            if (dmcs.Count > 0)
            {
                this.DataContext.MemoConflicts.InsertAllOnSubmit(dmcs);
                this.DataContext.SubmitChanges();
                created = true;
            }

            return created;
        }

        #endregion

        #region ICollection

        /// <summary>
        /// readonly collection
        /// add metadata to the Conflict table if you want more
        /// </summary>
        public void Add(MemoConflict item)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// readonly collection
        /// delete metadata to the Conflict table if you want less
        /// </summary>
        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(MemoConflict item)
        {
            return this.Conflicts.Contains(item);
        }

        public void CopyTo(MemoConflict[] array, int arrayIndex)
        {
            this.Conflicts.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return this.Conflicts.Count; }
        }

        /// <summary>
        /// don't add to the collection, add to the Conflict table
        /// </summary>
        public bool IsReadOnly
        {
            get { return true; }
        }

        /// <summary>
        /// readonly collection
        /// delete metadata to the Conflict table if you want less
        /// </summary>
        public bool Remove(MemoConflict item)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<MemoConflict> GetEnumerator()
        {
            return this.Conflicts.GetEnumerator();
        }
        
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.Conflicts.GetEnumerator();
        }

        #endregion

        #region IDisposable

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this._dataContext != null)
                {
                    this._dataContext.Dispose();
                    this._dataContext = null;
                }
            }
        }

        #endregion
    }
}
