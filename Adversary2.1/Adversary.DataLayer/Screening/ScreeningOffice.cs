﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.DataLayer
{
    public class ScreeningOffice : IScreeningMemoGet, IScreeningMemoSave
    {
        public ScreeningOffice(int screeningMemoID)
        {
            _ScreeningMemoID = screeningMemoID;
        }
        public bool Save()
        {
            //save to Memo.OrigOffice
            using (AdversaryDataContext dc = new AdversaryDataContext())
            {
                Memo m = (from M in dc.Memos
                          where M.ScrMemID == _ScreeningMemoID
                          select M).FirstOrDefault();
                if (m != null)
                {
                    m.OrigOffice = _OrigOffice;
                    dc.SubmitChanges();
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        private string _OrigOffice;

        public string OrigOffice
        {
            get { return _OrigOffice; }
            set { _OrigOffice = value; }
        }


        private int _ScreeningMemoID;
        public int ScreeningMemoID
        {
            get { return _ScreeningMemoID; }
        }

        public bool Get()
        {
            using (AdversaryDataContext dc = new AdversaryDataContext())
            {
                Memo m = (from M in dc.Memos
                          where M.ScrMemID == _ScreeningMemoID
                          select M).FirstOrDefault();
                if (m != null)
                {
                    _OrigOffice = m.OrigOffice;
                }
                else
                {
                    return false;
                }
            }
            return true;
        }
    }
}
