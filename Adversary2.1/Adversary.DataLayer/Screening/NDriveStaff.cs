﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.DataLayer
{
    public class NDriveStaff : IScreeningMemoDelete, IDisposable, IScreeningMemoSave
    {
        private AdversaryDataContext __adversaryContext = null;
        private AdversaryDataContext AdversaryContext
        {
            get
            {
                if (__adversaryContext == null)
                    __adversaryContext = new AdversaryDataContext();
                return __adversaryContext;

            }
        }
        
        public NDriveStaff(int screeningMemoID)
        {
            _ScreeningMemoID = screeningMemoID;
        }

        private string _NDriveStaffMember;
        public string NDriveStaffMember
        {
            get { return _NDriveStaffMember; }
            set { _NDriveStaffMember = value; }
        }

        private List<NDriveAccess> _NDriveTeamMembers;
        public List<NDriveAccess> NDriveTeamMembers
        {
            get
            {
                if (_NDriveTeamMembers == null)
                    _NDriveTeamMembers = GetNDriveAccess();
                return _NDriveTeamMembers;
            }
            set { _NDriveTeamMembers = value; }
        }
        
        public List<NDriveAccess> GetNDriveAccess()
        {
            List<NDriveAccess> _list = new List<NDriveAccess>();
            try
            {
                _list = (from ND in AdversaryContext.NDriveAccesses
                         where ND.ScrMemID == _ScreeningMemoID
                         select ND).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _list;
        }

        public void RefreshNDriveTeam()
        {
            this._NDriveTeamMembers = GetNDriveAccess();
        }

        private int _ScreeningMemoID;
        public int ScreeningMemoID
        {
            get { return _ScreeningMemoID; }
        }

        public bool Delete()
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            if (__adversaryContext != null)
                __adversaryContext.Dispose();
            __adversaryContext = null;
        }

        public bool Save()
        {
            List<NDriveAccess> _newTeam =
                this._NDriveTeamMembers.ToList();
            RefreshNDriveTeam();
            Adversary.DataLayer.NDriveAccess _deletedMember;
            Adversary.DataLayer.NDriveAccess _addedMember;

            _deletedMember = (from D in _NDriveTeamMembers
                              where !(from N in _newTeam select N.PersonnelID).Contains(D.PersonnelID)
                              select D).FirstOrDefault();

            _addedMember = (from D in _newTeam 
                            where !(from N in _NDriveTeamMembers select N.PersonnelID).Contains(D.PersonnelID) 
                            select D).FirstOrDefault();

            if (_deletedMember != null)
            {
                _deletedMember = (from D in AdversaryContext.NDriveAccesses
                                  where (D.PersonnelID == _deletedMember.PersonnelID && D.ScrMemID == _deletedMember.ScrMemID)
                                  select D).FirstOrDefault();
                AdversaryContext.NDriveAccesses.DeleteOnSubmit(_deletedMember);
            }
            else if (_addedMember != null)
            {
                AdversaryContext.NDriveAccesses.InsertOnSubmit(_addedMember);
            }
            AdversaryContext.SubmitChanges();

            return true;
        }
    }
}
