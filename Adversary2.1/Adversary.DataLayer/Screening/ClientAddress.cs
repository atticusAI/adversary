﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.DataLayer
{
    public class ClientAddress : IScreeningMemoSave, IScreeningMemoGet, IDisposable
    {
        private AdversaryDataContext __adversaryContext = null;
        private AdversaryDataContext AdversaryContext
        {
            get
            {
                if (__adversaryContext == null)
                    __adversaryContext = new AdversaryDataContext();
                return __adversaryContext;

            }
        }

        public ClientAddress(int screeningMemoID)
        {
            this._ScreeningMemoID = screeningMemoID;
        }

        public bool Save()
        {
            Address _ClientAddress;
            bool isNewClientAddress = true;
            if (_AddressID > 0) isNewClientAddress = false;

            if (!isNewClientAddress)
            {
                _ClientAddress = (from A in AdversaryContext.Addresses
                                  where (A.AddressID == _AddressID)
                                  select A).FirstOrDefault();
            }
            else
            {
                _ClientAddress = new Address();
            }

            if (_ClientAddress != null)
            {
                _ClientAddress.Address1 = _Address1;
                _ClientAddress.Address2 = _Address2;
                _ClientAddress.Address3 = _Address3;
                _ClientAddress.AltPhone = _AltPhone;
                _ClientAddress.City = _City;
                _ClientAddress.Country = _Country;
                _ClientAddress.email = _Email;
                _ClientAddress.NoEmailReason = _NoEmailReason;
                _ClientAddress.Fax = _Fax;
                _ClientAddress.Phone = _Phone;
                _ClientAddress.State = _StateProvince;
                _ClientAddress.Zip = _ZIPCode;
                _ClientAddress.ScrMemID = this.ScreeningMemoID > 0 ? this.ScreeningMemoID : _ClientAddress.ScrMemID;

                if (isNewClientAddress)
                {
                    AdversaryContext.Addresses.InsertOnSubmit(_ClientAddress);
                }
                AdversaryContext.SubmitChanges();
            }
            else
            {
                return false;
            }
            return true;
        }


        #region properties
        private int _AddressID;

        public int AddressID
        {
            get { return _AddressID; }
            set { _AddressID = value; }
        }
        
        private string _Address1;

        public string Address1
        {
            get { return _Address1; }
            set { _Address1 = value; }
        }

        private string _Address2;

        public string Address2
        {
            get { return _Address2; }
            set { _Address2 = value; }
        }

        private string _Address3;

        public string Address3
        {
            get { return _Address3; }
            set { _Address3 = value; }
        }

        private string _StateProvince;

        public string StateProvince
        {
            get { return _StateProvince; }
            set { _StateProvince = value; }
        }

        private string _City;

        public string City
        {
            get { return _City; }
            set { _City = value; }
        }
        
        private string _ZIPCode;

        public string ZIPCode
        {
            get { return _ZIPCode; }
            set { _ZIPCode = value; }
        }

        private string _Country;

        public string Country
        {
            get { return _Country; }
            set { _Country = value; }
        }

        private string _Phone;

        public string Phone
        {
            get { return _Phone; }
            set { _Phone = value; }
        }

        private string _AltPhone;

        public string AltPhone
        {
            get { return _AltPhone; }
            set { _AltPhone = value; }
        }

        private string _Email;

        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }

        private string _NoEmailReason;

        public string NoEmailReason
        {
            get { return this._NoEmailReason; }
            set { this._NoEmailReason = value; }
        }

        private string _Fax;

        public string Fax
        {
            get { return _Fax; }
            set { _Fax = value; }
        }
        #endregion

        private int _ScreeningMemoID;
        public int ScreeningMemoID
        {
            get { return _ScreeningMemoID; }
        }

        private Address _Address;

        public Address Address
        {
            get { return _Address; }
            set { _Address = value; }
        }


        public bool Get()
        {
            if(_Address == null)
                _Address = new Address();

            try
            {
                _Address = (from A in AdversaryContext.Addresses
                            where A.ScrMemID == _ScreeningMemoID
                            select A).FirstOrDefault();
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }

        public void Dispose()
        {
            if (__adversaryContext != null)
                __adversaryContext.Dispose();
            __adversaryContext = null;
        }
    }
}
