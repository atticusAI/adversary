﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Adversary.Utils;
using Adversary.DataLayer;

namespace Adversary.DataLayer
{
    /// <summary>
    /// uses the Billing table in the Adversary database
    /// </summary>
    public class MemoBillingInfo : Billing, IScreeningMemoSave, IScreeningMemoGet, IDisposable
    {
        private AdversaryDataContext __adversaryContext = null;
        private AdversaryDataContext AdversaryContext
        {
            get
            {
                if (__adversaryContext == null)
                    __adversaryContext = new AdversaryDataContext();
                return __adversaryContext;

            }
        }

        public MemoBillingInfo(int screeningMemoID)
        {
            _ScreeningMemoID = screeningMemoID;
        }

        public List<Billing> Billings { get; private set; }

        public bool Save()
        {
            //using (AdversaryDataContext dc = new AdversaryDataContext(AdversaryConnection.AdversaryConnectionString()))
            //{
            //    Billing billing = (from B in dc.Billings
            //                       where B.BillingID == BillingID
            //                       select B).FirstOrDefault();
            //    if (billing == null) billing = new Billing();
            //    billing.LateInterest = LateInterest;
            //    billing.ClientLevelBill = ClientLevelBill;
            //    billing.BillFormat = BillFormat;
            //    billing.BillInstructions = BillInstructions;
            //    billing.SpecialFee = SpecialFee;
            //    billing.ScrMemID = _ScreeningMemoID;
            //    if (BillingID <= 0)
            //    {
            //        dc.Billings.InsertOnSubmit(billing);
            //    }
            //    dc.SubmitChanges();
            //}

            Billing billing = (from B in AdversaryContext.Billings
                               where B.BillingID == BillingID
                               select B).FirstOrDefault();
            
            if (billing == null) 
                billing = new Billing();

            billing.LateInterest = LateInterest;
            billing.ClientLevelBill = ClientLevelBill;
            billing.BillFormat = BillFormat;
            billing.BillInstructions = BillInstructions;
            billing.SpecialFee = SpecialFee;
            billing.ScrMemID = _ScreeningMemoID;
            if (BillingID <= 0)
            {
                AdversaryContext.Billings.InsertOnSubmit(billing);
            }
            AdversaryContext.SubmitChanges();

            return true;
        }

        private int _ScreeningMemoID;
        public int ScreeningMemoID
        {
            get { return _ScreeningMemoID; }
        }
        public Billing GetBilling(int billingID)
        {
            Billing billing = new Billing();
            if (BillingID <= 0)
            {
                //using (AdversaryDataContext dc = new AdversaryDataContext(AdversaryConnection.AdversaryConnectionString()))
                //{
                //    billing = (from B in dc.Billings
                //               where B.BillingID == BillingID
                //               select B).FirstOrDefault();
                //}
                billing = (from B in AdversaryContext.Billings
                           where B.BillingID == BillingID
                           select B).FirstOrDefault();
            }
            return billing;
        }
        public bool Get()
        {
            List<Billing> billings = new List<Billing>();
            if (BillingID <= 0)
            {
                billings = (from B in AdversaryContext.Billings
                            where B.ScrMemID == _ScreeningMemoID
                            select B).ToList();
            }
            else
            {
                billings = (from B in AdversaryContext.Billings
                            where B.BillingID == this.BillingID
                            select B).ToList();
            }


            this.Billings = billings;
            return true;
        }

        public void Dispose()
        {
            if (__adversaryContext != null)
                __adversaryContext.Dispose();
            __adversaryContext = null;
        }
    }
}
