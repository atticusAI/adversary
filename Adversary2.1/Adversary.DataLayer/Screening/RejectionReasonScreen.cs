﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.DataLayer
{
    public class RejectionReasonScreen: IScreeningMemoGet, IScreeningMemoSave, IDisposable
    {
        private AdversaryDataContext __adversaryContext = null;
        private AdversaryDataContext AdversaryContext
        {
            get
            {
                if (__adversaryContext == null)
                    __adversaryContext = new AdversaryDataContext();
                return __adversaryContext;

            }
        }
        
        
        private string _RejectionReason;
        public string RejectionReason
        {
            get { return _RejectionReason; }
            set { _RejectionReason = value; }
        }

        public RejectionReasonScreen(int screeningMemoID)
        {
            _ScreeningMemoID = screeningMemoID;
        }

        public bool Save()
        {
            Memo m = (from M in AdversaryContext.Memos
                      where M.ScrMemID == _ScreeningMemoID
                      select M).FirstOrDefault();
            if (m != null)
            {
                m.RejectDesc = _RejectionReason;
                AdversaryContext.SubmitChanges();
            }
            else
            {
                return false;
            }
            return true;
        }

        private int _ScreeningMemoID;
        public int ScreeningMemoID
        {
            get { return _ScreeningMemoID; }
            private set { _ScreeningMemoID = value; }
        }

        public bool Get()
        {
            Memo m = (from M in AdversaryContext.Memos
                      where M.ScrMemID == _ScreeningMemoID
                      select M).FirstOrDefault();
            _RejectionReason = m.RejectDesc;
            return true;

        }

        public void Dispose()
        {
            if (__adversaryContext != null)
                __adversaryContext.Dispose();
            __adversaryContext = null;
        }
    }
}
