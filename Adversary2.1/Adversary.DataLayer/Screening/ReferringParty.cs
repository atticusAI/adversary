﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Adversary.DataLayer;
using Adversary.Utils;

namespace Adversary.DataLayer
{
    public class ReferringParty : IScreeningMemoGet, IScreeningMemoSave, IScreeningMemoDelete, IDisposable
    {
        private AdversaryDataContext __adversaryContext = null;
        private AdversaryDataContext AdversaryContext
        {
            get
            {
                if (__adversaryContext == null)
                    __adversaryContext = new AdversaryDataContext();
                return __adversaryContext;

            }
        }

        public ReferringParty(int screeningMemoID)
        {
            this._ScreeningMemoID = screeningMemoID;
        }

        public bool Save()
        {
            //need to save a party, a party address and the referral reason
            //in the new party tables
            NewClient _NewClient = new NewClient();
            Party _Party = new Party();
            PartyAddressInfo _PartyAddressInfo = new PartyAddressInfo();
            using (AdversaryDataContext dc = new AdversaryDataContext())
            {
                _NewClient = (from NC in dc.NewClients
                              where NC.ScrMemID == _ScreeningMemoID
                              select NC).FirstOrDefault();

                _Party = (from P in dc.Parties
                          where P.PartyID == _PartyID
                          select P).FirstOrDefault();
                
                if (_NewClient == null)
                {
                    _NewClient = new NewClient();
                }
                if (_Party == null)
                {
                    _Party = new Party();
                }

                _NewClient.ScrMemID = _ScreeningMemoID;
                _NewClient.OriginationComments = _HowClientCameToHH;

                _Party.PartyFName = _PartyFirstName;
                _Party.PartyMName = _PartyMiddleName;
                _Party.PartyLName = _PartyLastName;
                _Party.PartyOrganization = _PartyOrganizationName;
                _Party.PartyType = _PartyType;
                _Party.PartyRelationshipCode = "902";
                _Party.ScrMemID = ScreeningMemoID;

                if (_Party.PartyID == 0)
                {    
                    dc.Parties.InsertOnSubmit(_Party);
                }

                if (_NewClient.NwClientID == 0)
                {
                    dc.NewClients.InsertOnSubmit(_NewClient);
                }

                dc.SubmitChanges();

                _PartyAddressInfo = (from PA in dc.PartyAddressInfos
                                     where PA.PartyID == _Party.PartyID
                                     select PA).FirstOrDefault();

                if (_PartyAddressInfo == null)
                {
                    _PartyAddressInfo = new PartyAddressInfo();
                }
                _PartyAddressInfo.PartyID = _Party.PartyID;
                _PartyAddressInfo.Phone = _Phone;
                _PartyAddressInfo.Address_Line_1 = _Address1;
                _PartyAddressInfo.Address_Line_2 = _Address2;
                _PartyAddressInfo.Address_Line_3 = _Address3;
                _PartyAddressInfo.City = _City;
                _PartyAddressInfo.Country = _Country;
                _PartyAddressInfo.EMail = _Email;
                _PartyAddressInfo.Fax = _Fax;
                _PartyAddressInfo.Phone = _Phone;
                _PartyAddressInfo.State = _State;
                _PartyAddressInfo.Zip = _ZIP;
                if(_PartyAddressInfo.PartyAddressID == 0)
                {
                    dc.PartyAddressInfos.InsertOnSubmit(_PartyAddressInfo);
                }

                dc.SubmitChanges();

            }
            return true;
        }

        private int _ScreeningMemoID;
        public int ScreeningMemoID
        {
            get { return _ScreeningMemoID; }
        }

        public void GetParty()
        {
            //ReferringParty rp = new ReferringParty();
            Adversary.DataLayer.PartyAddressInfo _PartyAddress = new PartyAddressInfo();
            Adversary.DataLayer.Party _Party = new Party();

            using (AdversaryDataContext dc = new AdversaryDataContext())
            {
                //var query = (from P in dc.Parties
                //             join PA in dc.PartyAddressInfos
                //             on P.PartyID equals PA.PartyID
                //             into P_PA 
                //             join NP in dc.NewClients
                //             on P.ScrMemID equals NP.ScrMemID
                //             into P_NP
                //             where P.ScrMemID == _ScreeningMemoID &&
                             
                //             select new { P, PA, NP }).FirstOrDefault();

                //var query = (from P in dc.Parties
                //             from PA in dc.PartyAddressInfos.Where(pa => pa.PartyID == P.PartyID).DefaultIfEmpty()
                //             from NC in dc.NewClients.Where(nc => nc.ScrMemID == P.ScrMemID).DefaultIfEmpty()
                //             where (NC.ScrMemID == _ScreeningMemoID || P.ScrMemID == _ScreeningMemoID)
                //             && P.PartyRelationshipCode == "902"
                //             select new { P, PA, NC }).FirstOrDefault();
                var query = (
                                from NC in dc.NewClients
                                where NC.ScrMemID == _ScreeningMemoID
                                from P in dc.Parties
                                .Where(p => (p.ScrMemID == NC.ScrMemID && p.PartyRelationshipCode == "902"))
                                .DefaultIfEmpty()
                                from PA in dc.PartyAddressInfos
                                .Where(pa => pa.PartyID == P.PartyID)
                                .DefaultIfEmpty()
                                select new
                                {
                                    NC,
                                    P,
                                    PA
                                }).FirstOrDefault();
                                 
                             



                if (query.PA != null)
                {
                    _Address1 = query.PA.Address_Line_1;
                    _Address2 = query.PA.Address_Line_2;
                    _Address3 = query.PA.Address_Line_3;
                    _City = query.PA.City;
                    _State = query.PA.State;
                    _Country = query.PA.Country;
                    _ZIP = query.PA.Zip;
                    _Phone = query.PA.Phone;
                    _Fax = query.PA.Fax;
                    _Email = query.PA.EMail;
                }
                if (query.NC != null)
                {
                    _HowClientCameToHH = query.NC.OriginationComments;
                }

                if (query.P != null)
                {
                    _PartyFirstName = query.P.PartyFName;
                    _PartyMiddleName = query.P.PartyMName;
                    _PartyLastName = query.P.PartyLName;
                    _PartyOrganizationName = query.P.PartyOrganization;
                    _PartyType = query.P.PartyType;
                    _PartyID = query.P.PartyID;
                }
            }
            

            

        }

        private string _HowClientCameToHH;
        public string HowClientCameToHH
        {
            get { return _HowClientCameToHH; }
            set { _HowClientCameToHH = value; }
        }

        private string _Address1;
        public string Address1
        {
            get { return _Address1; }
            set { _Address1 = value; }
        }

        private string _Address2;
        public string Address2
        {
            get { return _Address2; }
            set { _Address2 = value; }
        }

        private string _Address3;
        public string Address3
        {
            get { return _Address3; }
            set { _Address3 = value; }
        }

        private string _City;

        public string City
        {
            get { return _City; }
            set { _City = value; }
        }

        private string _State;

        public string State
        {
            get { return _State; }
            set { _State = value; }
        }

        private string _ZIP;

        public string ZIP
        {
            get { return _ZIP; }
            set { _ZIP = value; }
        }

        private string _Country;

        public string Country
        {
            get { return _Country; }
            set { _Country = value; }
        }

        private string _Phone;

        public string Phone
        {
            get { return _Phone; }
            set { _Phone = value; }
        }

        private string _Fax;

        public string Fax
        {
            get { return _Fax; }
            set { _Fax = value; }
        }

        private string _Email;

        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }

        private string _PartyType;

        public string PartyType
        {
            get { return _PartyType; }
            set { _PartyType = value; }
        }

        private string _PartyFirstName;

        public string PartyFirstName
        {
            get { return _PartyFirstName; }
            set { _PartyFirstName = value; }
        }

        private string _PartyMiddleName;

        public string PartyMiddleName
        {
            get { return _PartyMiddleName; }
            set { _PartyMiddleName = value; }
        }

        private string _PartyLastName;

        public string PartyLastName
        {
            get { return _PartyLastName; }
            set { _PartyLastName = value; }
        }

        private string _PartyOrganizationName;

        public string PartyOrganizationName
        {
            get { return _PartyOrganizationName; }
            set { _PartyOrganizationName = value; }
        }

        private int _PartyID;

        public int PartyID
        {
            get { return _PartyID; }
            set { _PartyID = value; }
        }

        public List<Party> GetReferringParties()
        {
            List<Party> _referringParties = new List<Party>();
            try
            {
                _referringParties = (from R in AdversaryContext.Parties
                                     where AdversaryData.ReferringPartyCodes.Contains(R.PartyRelationshipCode)
                                     select R).ToList();

            }
            catch (Exception)
            {
                throw;
            }
            return _referringParties;
        }
        public bool Get()
        {
            return false;
        }

        public bool Delete()
        {
            Party _deletedParty;
            try
            {
                _deletedParty = (from P in AdversaryContext.Parties
                                 where P.PartyID == _PartyID
                                 select P).FirstOrDefault();
                AdversaryContext.Parties.DeleteOnSubmit(_deletedParty);
                AdversaryContext.SubmitChanges();
            }
            catch (Exception)
            {
                
                throw;
            }
            return true;
        }

        public void Dispose()
        {
            if (__adversaryContext != null)
                __adversaryContext.Dispose();
            __adversaryContext = null;
        }
    }
}
