﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.DataLayer
{
    public class BasicInformation : IScreeningMemoSave
    {
        public BasicInformation(int screeningMemoID)
        {
            this._ScreeningMemoID = screeningMemoID;
        }

        private DateTime _MemoDate;

        public DateTime MemoDate
        {
            get { return _MemoDate; }
            set { _MemoDate = value; }
        }

        private bool _Confidential;

        public bool Confidential
        {
            get { return _Confidential; }
            set { _Confidential = value; }
        }

        private int _ProBonoTypeID;

        public int ProBonoTypeID
        {
            get { return _ProBonoTypeID; }
            set { _ProBonoTypeID = value; }
        }


        private string _PreparingAttorney;

        public string PreparingAttorney
        {
            get { return _PreparingAttorney; }
            set { _PreparingAttorney = value; }
        }

        private string _InputAttorney;

        public string InputAttorney
        {
            get { return _InputAttorney; }
            set { _InputAttorney = value; }
        }

        public bool Save()
        {
            using (AdversaryDataContext dc = new AdversaryDataContext())
            {
                var qry = from M in dc.Memos
                          where M.ScrMemID == _ScreeningMemoID
                          select M;
                Memo m = qry.FirstOrDefault();
                if (m != null)
                {
                    m.Confidential = _Confidential;
                    m.RefDate = _MemoDate;
                    m.AttEntering = _PreparingAttorney;
                    m.PersonnelID = _InputAttorney;
                    if (m.ScrMemType == 3) m.ProBonoTypeID = _ProBonoTypeID;
                    dc.SubmitChanges();
                }
            }
            return true;
        }

        private int _ScreeningMemoID;

        public int ScreeningMemoID
        {
            get { return _ScreeningMemoID; }
        }

    }
}
