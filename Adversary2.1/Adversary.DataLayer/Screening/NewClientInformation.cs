﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.DataLayer
{
    public class NewClientInformation : IScreeningMemoGet, IScreeningMemoSave,
        IDisposable
    {
        public NewClientInformation(int screeningMemoID)
        {
            this._ScreeningMemoID = screeningMemoID;
        }

        #region Data Context

        private AdversaryDataContext _adversaryContext = null;
        private AdversaryDataContext AdversaryContext
        {
            get
            {
                if (_adversaryContext == null)
                    _adversaryContext = new AdversaryDataContext();
                return _adversaryContext;
            }
        }

        #endregion

        /// <summary>
        /// the personnel ID of a person for whom the firm is doing legal work
        /// </summary>
        public int HHLegalWorkID { get; set; }
        /// <summary>
        /// whether the client is an organization or an individual
        /// </summary>
        public string ClientType { get; set; }
        /// <summary>
        /// the client organization name
        /// </summary>
        public string OrganizationName { get; set; }
        /// <summary>
        /// whether the client is in the middle of a pending bankruptcy
        /// </summary>
        public bool PendingBankruptcy { get; set; }
        /// <summary>
        /// if the client is an individual, this is the client first name
        /// </summary>
        public string ClientFirstName { get; set; }
        /// <summary>
        /// if the client is an individual, this is the client middle name
        /// </summary>
        public string ClientMiddleName { get; set; }
        /// <summary>
        /// if the client is an individual, this is the client last name
        /// </summary>
        public string ClientLastName { get; set; }

        private int _ScreeningMemoID;
        public int ScreeningMemoID
        {
            get
            {
                return _ScreeningMemoID;
            }
        }

        public bool Save()
        {
            bool isNew = false;

            NewClient n = (from N in AdversaryContext.NewClients
                           where N.ScrMemID == _ScreeningMemoID
                           select N).FirstOrDefault();

            Memo m = (from M in AdversaryContext.Memos
                      where M.ScrMemID == _ScreeningMemoID
                      select M).FirstOrDefault();

            if (n == null)
            {
                n = new NewClient();
                n.ScrMemID = _ScreeningMemoID;
                isNew = true;
            }

            if (this.ClientType != null)
            {
                if (this.ClientType.Equals("Organization"))
                {
                    m.Company = true;
                    m.Individual = false;
                }
                else if (this.ClientType.Equals("Individual"))
                {
                    m.Individual = true;
                    m.Company = false;
                }
            }

            n.FName = this.ClientFirstName;
            n.LName = this.ClientLastName;
            n.MName = this.ClientMiddleName;
            n.CName = this.OrganizationName;
            m.PendingBankruptcy = PendingBankruptcy;
            m.HHLegalWorkID = HHLegalWorkID;
            m.ClientNumber = null;

            if (isNew) AdversaryContext.NewClients.InsertOnSubmit(n);

            AdversaryContext.SubmitChanges();

            return true;
        }

        public bool Get()
        {
            NewClient n = (from NC in AdversaryContext.NewClients
                           where NC.ScrMemID == _ScreeningMemoID
                           select NC).FirstOrDefault();
            
            Memo m = (from M in AdversaryContext.Memos
                      where M.ScrMemID == _ScreeningMemoID
                      select M).FirstOrDefault();
            
            if (n != null)
            {
                this.ClientFirstName = n.FName;
                this.ClientLastName = n.LName;
                this.ClientMiddleName = n.MName;
                this.OrganizationName = n.CName;
            }
            
            if (m != null)
            {
                if (m.Company.HasValue && m.Company.Value)
                    this.ClientType = "Organization";
                else if (m.Individual.HasValue && m.Individual.Value)
                    this.ClientType = "Individual";
                if (m.PendingBankruptcy.HasValue)
                    this.PendingBankruptcy = m.PendingBankruptcy.Value;
                if (m.HHLegalWorkID.HasValue)
                    this.HHLegalWorkID = m.HHLegalWorkID.Value;
            }
            
            return true;
        }

        #region IDisposable 

        public void Dispose() 
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_adversaryContext != null)
                {
                    _adversaryContext.Dispose();
                    _adversaryContext = null;
                }
            }
        }

        #endregion
    }
}
