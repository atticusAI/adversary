﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Adversary.Utils;
using Adversary.DataLayer;

namespace Adversary.DataLayer
{
    public class EngagementLetterScreen : IScreeningMemoGet, IScreeningMemoSave, IDisposable
    {
        private AdversaryDataContext __adversaryContext = null;
        private AdversaryDataContext AdversaryContext
        {
            get
            {
                if (__adversaryContext == null)
                    __adversaryContext = new AdversaryDataContext();
                return __adversaryContext;

            }
        }

        public EngagementLetterScreen(int screeningMemoID)
        {
            _ScreeningMemoID = screeningMemoID;
        }

        public bool Get()
        {
            Memo m = (from M in AdversaryContext.Memos
                      where M.ScrMemID == _ScreeningMemoID
                      select M).FirstOrDefault();
            _EngagementLetterReason = m.EngageDesc;
            _EngagementLetterUploaded = m.EngageLetter;
            _WrittenPolicies = m.WrittenPolicies;
            return true;
        }

        private int _ScreeningMemoID;
        public int ScreeningMemoID
        {
            get { return _ScreeningMemoID; }
            private set { _ScreeningMemoID = value; }
        }

        private string _EngagementLetterReason;
        public string EngagementLetterReason
        {
            get { return _EngagementLetterReason; }
            set { _EngagementLetterReason = value; }
        }

        private bool? _EngagementLetterUploaded;
        public bool? EngagementLetterUploaded
        {
            get { return _EngagementLetterUploaded; }
            set { _EngagementLetterUploaded = value; }
        }

        private bool? _WrittenPolicies;
        public bool? WrittenPolicies
        {
            get { return _WrittenPolicies; }
            set { _WrittenPolicies = value; }
        }


        public bool Save()
        {
            Memo m = (from M in AdversaryContext.Memos
                      where M.ScrMemID == _ScreeningMemoID
                      select M).FirstOrDefault();
            if (m != null)
            {
                m.EngageLetter = _EngagementLetterUploaded;
                m.EngageDesc = _EngagementLetterReason;
                m.WrittenPolicies = _WrittenPolicies;
                AdversaryContext.SubmitChanges();
            }
            else
            {
                return false;
            }
            return true;
        }

        public void Dispose()
        {
            if (__adversaryContext != null)
                __adversaryContext.Dispose();
            __adversaryContext = null;
        }
    }
}
