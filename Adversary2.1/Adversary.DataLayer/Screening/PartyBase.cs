﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.DataLayer
{
    public abstract class PartyBase : Party
    {
        public bool SaveGroup(List<Party> partyList)
        {
            using (AdversaryDataContext dc = new AdversaryDataContext())
            {
                dc.Parties.InsertAllOnSubmit(partyList);
                dc.SubmitChanges();
            }
            return true;
        }

        public virtual bool Save()
        {
            using (AdversaryDataContext dc = new AdversaryDataContext())
            {
                Party _Party = (from P in dc.Parties
                                where P.PartyID == this.PartyID
                                select P).FirstOrDefault();
                if (_Party != null)
                {
                    _Party.PartyFName = this.PartyFName;
                    _Party.PartyMName = this.PartyMName;
                    _Party.PartyLName = this.PartyLName;
                    _Party.PartyOrganization = this.PartyOrganization;
                    _Party.PartyRelationshipCode = this.PartyRelationshipCode;
                    _Party.PartyRelationshipName = this.PartyRelationshipName;
                    _Party.PartyType = this.PartyType;
                    _Party.ScrMemID = base.ScrMemID;
                    _Party.PC = 0;
                    dc.SubmitChanges();
                }
                else
                {
                    return false;
                }
            }
            return true;
        }



        public bool Delete()
        {
            using (AdversaryDataContext dc = new AdversaryDataContext())
            {
                Party _Party = (from P in dc.Parties
                                       where P.PartyID == this.PartyID
                                       select P).First();
                dc.Parties.DeleteOnSubmit(_Party);
                dc.SubmitChanges();
            }
            return true;
        }

    }
}
