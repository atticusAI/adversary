﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.DataLayer.Screening
{
    public class State
    {
        #region Properties

        private int _stateID;

        public int StateID
        {
            get { return this._stateID; }
        }

        private string _code;

        public string Code
        {
            get { return this._code; }
        }

        private string _name;

        public string Name
        {
            get { return this._name; }
        }

        #endregion

        #region Construction

        public State(int countryID, string code, string name)
        {
            this._stateID = countryID;
            this._code = code;
            this._name = name;
        }

        #endregion
    }

    public class StateCollection : IDisposable, ICollection<State>
    {
        #region Data Context

        private AdversaryDataContext _dataContext = null;

        private AdversaryDataContext DataContext
        {
            get
            {
                if (this._dataContext == null)
                {
                    this._dataContext = new AdversaryDataContext();
                }
                return this._dataContext;
            }
        }

        #endregion

        #region IDisposable

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this._dataContext != null)
                {
                    this._dataContext.Dispose();
                    this._dataContext = null;
                }
            }
        }

        #endregion

        #region Storage

        private IEnumerable<State> _states = null;

        private IEnumerable<State> States
        {
            get
            {
                if (this._states == null)
                {
                    this._states = this.Load();
                }
                return this._states;
            }
        }

        #endregion

        #region Load

        private IEnumerable<State> Load()
        {
            var qry = from st in this.DataContext.States
                      orderby st.SortOrder 
                      select st;

            foreach (DataLayer.State s in qry)
            {
                yield return new State(s.StateID, s.Code, s.Name);
            }
        }

        #endregion

        #region ICollection

        public void Add(State item)
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(State item)
        {
            return this.States.Contains(item);
        }

        public void CopyTo(State[] array, int arrayIndex)
        {
            this.States.ToList().CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return this.States.Count(); }
        }

        public bool IsReadOnly
        {
            get { return true; }
        }

        public bool Remove(State item)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<State> GetEnumerator()
        {
            return this.States.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.States.GetEnumerator();
        }

        #endregion
    }
}
