﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.DataLayer
{
    public class FeeSplit : IScreeningMemoSave, IScreeningMemoGet, IScreeningMemoDelete
    {
        private Memo _Memo;

        private AdversaryDataContext __adversaryContext = null;
        private AdversaryDataContext AdversaryContext
        {
            get
            {
                if (__adversaryContext == null)
                    __adversaryContext = new AdversaryDataContext();
                return __adversaryContext;

            }
        }

        public FeeSplit(int screeningMemoID)
        {
            _ScreeningMemoID = screeningMemoID;
            using (AdversaryDataContext dc = new AdversaryDataContext())
            {
                dc.DeferredLoadingEnabled = true;
                System.Data.Linq.DataLoadOptions dlo = new System.Data.Linq.DataLoadOptions();
                dlo.LoadWith<Memo>(M => M.FeeSplitStaffs);
                dc.LoadOptions = dlo;
                _Memo = (from M in dc.Memos
                         where M.ScrMemID == _ScreeningMemoID
                         select M).FirstOrDefault();
            }
        }
        public bool Delete() { return false; }
        public bool Delete(int feeSplitStaffID)
        {
            using (AdversaryDataContext dc = new AdversaryDataContext())
            {
                FeeSplitStaff fs = (from F in dc.FeeSplitStaffs where F.FeeSplitStaffID == feeSplitStaffID select F).FirstOrDefault();
                if (fs != null)
                {
                    dc.FeeSplitStaffs.DeleteOnSubmit(fs);
                    dc.SubmitChanges();
                }
            }
            return false;
        }

        private int _ScreeningMemoID;
        public int ScreeningMemoID
        {
            get { return _ScreeningMemoID; }
        }

        private string _FeeSplitDescription;
        public string FeeSplitDescription
        {
            get { return _FeeSplitDescription; }
            set { _FeeSplitDescription = value; }
        }

        //this is a "yes" or "no"
        private string _NewMatterOnly;
        public string NewMatterOnly
        {
            get { return _NewMatterOnly; }
            set { _NewMatterOnly = value; }
        }
        

        public bool Get()
        {
            if (_Memo != null)
            {
                _FeeSplitDescription = _Memo.FeeSplitDesc;
                _NewMatterOnly = _Memo.NewMatterOnly;
                return true;
            }
            return false;
        }

        public List<FeeSplitStaff> GetFeeSplitStaff()
        {
            return AdversaryContext.FeeSplitStaffs.Where(FS => FS.ScrMemID == ScreeningMemoID).ToList<FeeSplitStaff>();
            //List<FeeSplitStaff> _FeeSplitStaff = new List<FeeSplitStaff>();
            //try
            //{
            //    using (AdversaryDataContext dc = new AdversaryDataContext(AdversaryConnection.AdversaryConnectionString()))
            //    {
            //        _FeeSplitStaff = (from F in dc.FeeSplitStaffs
            //                          where F.ScrMemID == _ScreeningMemoID
            //                          select F).ToList();
            //    }
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
            //return _FeeSplitStaff;
            //return _Memo.FeeSplitStaffs.ToList();
        }

        public bool Save()
        {
            using (AdversaryDataContext dc = new AdversaryDataContext())
            {
                Memo m = (from M in dc.Memos
                          where M.ScrMemID == _ScreeningMemoID
                          select M).FirstOrDefault();
                if (m != null)
                {
                    m.FeeSplitDesc = _FeeSplitDescription;
                    m.NewMatterOnly = _NewMatterOnly;
                    dc.SubmitChanges();
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        public bool SaveFeeSplitStaff(FeeSplitStaff fstaff)
        {
            using (AdversaryDataContext dc = new AdversaryDataContext())
            {
                
                dc.FeeSplitStaffs.InsertOnSubmit(fstaff);
                dc.SubmitChanges();
            }
            return true;
        }

        public bool AddFeeSplitStaff(string personnelID)
        {
            FeeSplitStaff _Exist =
                (from FS in AdversaryContext.FeeSplitStaffs
                 where FS.ScrMemID == _ScreeningMemoID &&
                 FS.PersonnelID == personnelID
                 select FS).FirstOrDefault();

            if (_Exist == null)
            {
                FeeSplitStaff F = new FeeSplitStaff();
                F.PersonnelID = personnelID;
                F.ScrMemID = _ScreeningMemoID;
                AdversaryContext.FeeSplitStaffs.InsertOnSubmit(F);
                AdversaryContext.SubmitChanges();
                return true;
            }
            else return false;
        }

        public bool DeleteFeeSplitStaff(int FeeSplitStaffID)
        {
            FeeSplitStaff _Staff = (from FS in AdversaryContext.FeeSplitStaffs
                                    where FS.FeeSplitStaffID == FeeSplitStaffID
                                    select FS).FirstOrDefault();
            if (_Staff != null)
            {
                AdversaryContext.FeeSplitStaffs.DeleteOnSubmit(_Staff);
                AdversaryContext.SubmitChanges();
                return true;
            }
            else return false;
        }

    }
}
