﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.DataLayer
{
    /// <summary>
    /// this class inherits from AdminLogin but associates all their practice codes
    /// </summary>
    public class AdminLoginWithCodes : AdminLogin
    {
        private string _PracCode;
        public string PracCode
        {
            get { return _PracCode; }
            set { _PracCode = value; }
        }

        private bool? _IsAdversary;
        public bool? IsAdversary
        {
            get { return _IsAdversary; }
            set { _IsAdversary = value; }
        }

        private bool? _IsAdversaryAdmin;
        public bool? IsAdversaryAdmin
        {
            get { return _IsAdversaryAdmin; }
            set { _IsAdversaryAdmin = value; }
        }

        private bool? _IsAP;
        public bool? IsAP
        {
            get { return _IsAP; }
            set { _IsAP = value; }
        }

        private bool? _IsPrimary;
        public bool? IsPrimary
        {
            get { return _IsPrimary; }
            set { _IsPrimary = value; }
        }

        private bool? _IsPrimaryAP;
        public bool? IsPrimaryAP
        {
            get { return _IsPrimaryAP; }
            set { _IsPrimaryAP = value; }
        }

        private bool? _IsPGM;
        public bool? IsPGM
        {
            get { return _IsPGM; }
            set { _IsPGM = value; }
        }

    }






    public class AdminLoginsClass
    {
        private class APComparer : IEqualityComparer<AdminLoginWithCodes>
        {
            public bool Equals(AdminLoginWithCodes a, AdminLoginWithCodes b)
            {
                return a.UserID == b.UserID;
            }
            public int GetHashCode(AdminLoginWithCodes obj)
            {
                return obj.UserID.GetHashCode();
            }
        }

        public AdminLoginsClass()
        {
            RefreshAdminList();
        }

        private AdminLoginWithCodes[] _AllAdmins = null;

        public AdminLoginWithCodes[] AllAdmins
        {
            get
            {
                if (_AllAdmins == null)
                {
                    RefreshAdminList();
                }
                return _AllAdmins;
            }
        }
        
        // private AdminLogin[] _admins = null;

        #region admin logins
        public AdminLogin GetAdminLoginFromLoginName(string login)
        {
            var query = from a in AllAdmins
                        where a.Login.ToLower() == login.ToLower()
                        select a;

            if (query.Count() == 0)
                throw new ArgumentException("Could not lookup data for login " + login);

            return query.FirstOrDefault();
        }

        public AdminLogin[] GetAPsByOffice(string officeCode, bool primaryOnly)
        {
            var query = (from a in AllAdmins
                         where (a.Office == Int32.Parse(officeCode)) &&
                               (a.IsAP.HasValue && a.IsAP.Value) &&
                               (!primaryOnly || (a.IsPrimaryAP.HasValue && a.IsPrimaryAP.Value))
                         select a).Distinct(new APComparer());

            return query.ToArray();
        }

        public AdminLogin[] GetAPsByOfficeName(string officeName, bool primaryOnly)
        {
            string officeCode = "";

            if (string.IsNullOrEmpty(officeName))
                throw new ArgumentException("Could not get APs by office name. Office name was not specified");

            if (officeName.Contains("DTC"))
                officeName = "Tech Center";

            AdversaryData adv = new AdversaryData();
            Adversary.DataLayer.HRDataService.FirmOffice ofc =
                adv.GetFirmOffices().Where(F => F.LocationDescription.Contains(officeName)).FirstOrDefault();

            //string officeCode = cms.GetOfficeCode(officeName);
            var query = from a in AllAdmins
                        where (a.Office == Int32.Parse(officeCode)) &&
                              (a.IsAP.HasValue && a.IsAP.Value) &&
                              (!primaryOnly || (a.IsPrimaryAP.HasValue && a.IsPrimaryAP.Value))
                        select a;

            return query.ToArray();
        }

        public AdminLogin[] GetAPsByOfficeCode(string officeCode, bool primaryOnly)
        {
            if (String.IsNullOrEmpty(officeCode))
                throw new ArgumentException("Could not get APs by office code. Office code was not specified");
            
            var _Logins = (from A in AllAdmins
                            where (A.Office == Int32.Parse(officeCode)) &&
                            (A.IsAP.HasValue && A.IsAP.Value) &&
                            (!primaryOnly || (A.IsPrimaryAP.HasValue && A.IsPrimaryAP.Value))
                            select A);
            
            return _Logins.ToArray();
        }

        public AdminLogin[] GetPGMsByPracticeCode(string pracCode, bool primaryOnly)
        {
            var _AdminLogins = (from A in AllAdmins
                                where (A.PracCode == pracCode) &&
                                (A.IsPGM.HasValue && A.IsPGM.Value) &&
                                (!primaryOnly || (A.IsPrimary.HasValue && A.IsPrimary.Value))
                                select A);
            return _AdminLogins.ToArray();
        }

        //public  AdminLogins[] GetAPsAndPGMs(Memo memo, bool primaryOnly) { return GetAPsAndPGMs(Int32.Parse(memo.OrigOffice), memo.PracCode, primaryOnly); }
        //public  AdminLogins[] GetAPsAndPGMs(int origOffice, string pracCode, bool primaryOnly)
        //{

        //    var query = from a in AllAdmins
        //                from p in a.AdminPracCodes
        //                where ( 
        //                        (a.Office == origOffice) && 
        //                        (a.IsAP.HasValue && a.IsAP.Value) && 
        //                        (!primaryOnly || (primaryOnly && a.IsPrimaryAP.HasValue && a.IsPrimaryAP.Value))
        //                      ) 
        //                      ||
        //                      ( 
        //                        (p.PracCode == pracCode) &&
        //                        (a.IsPGM.HasValue && a.IsPGM.Value) &&
        //                        (!primaryOnly || (primaryOnly && p.IsPrimary.HasValue && p.IsPrimary.Value))
        //                      )

        //                select a;

        //    return query.ToArray();
        //}

        public AdminLogin[] GetAllAPs()
        {
            var query = (from a in AllAdmins
                         where a.IsAP.HasValue && a.IsAP.Value
                         select a).Distinct();

            return query.ToArray();
        }

        public AdminLogin[] GetAllPGMs()
        {
            var query = from a in AllAdmins
                        where a.IsPGM.HasValue && a.IsPGM.Value
                        select a;

            return query.ToArray();
        }

        public AdminLogin[] GetAdversaryGroup()
        {
            var query = from a in AllAdmins
                        where a.IsAdversary.HasValue && a.IsAdversary.Value
                        select a;

            return query.ToArray();
        }

        public AdminLogin[] GetAdversaryGroupAdmins()
        {
            var query = from a in AllAdmins
                        where (a.IsAdversary ?? false) &&
                              (a.IsAdversaryAdmin ?? false)
                        select a;

            return query.ToArray();
        }

        public AdminLogin GetAdminLoginFromID(int? userId)
        {
            var query = from a in AllAdmins
                        where a.UserID == userId
                        select a;

            if (query.Count() == 0)
                throw new ArgumentException("Could not lookup data for User ID " + userId);

            return query.FirstOrDefault();
        }

        public string GetAdminLoginNameFromID(int? userId)
        {
            return GetAdminLoginFromID(userId).Login;
        }

        public void RefreshAdminList()
        {
            //this._admins = advData.AdminLogins.ToArray();

            List<AdminLoginWithCodes> _AdminLoginWithCodes = new List<AdminLoginWithCodes>();
            using (AdversaryDataContext advData = new AdversaryDataContext())
            {
                _AdminLoginWithCodes = (from AL in advData.AdminLogins
                                        join APC in advData.AdminPracCodes on AL.UserID equals APC.UserID into APCs
                                        from ALPC in APCs.DefaultIfEmpty()
                                        select new AdminLoginWithCodes
                                        {
                                            Email = AL.Email,
                                            IsAdversary = AL.IsAdversary,
                                            IsAdversaryAdmin = AL.IsAdversaryAdmin,
                                            IsAP = AL.IsAP,
                                            IsPrimaryAP = AL.IsPrimaryAP,
                                            IsFinancialAdmin = AL.IsFinancialAdmin,
                                            IsPGM = AL.IsPGM,
                                            UserID = AL.UserID,
                                            Office = AL.Office,
                                            Login = AL.Login,
                                            PracCode = ALPC.PracCode,
                                            IsPrimary = ALPC.IsPrimary
                                        }).ToList<AdminLoginWithCodes>();
            }
            _AllAdmins = _AdminLoginWithCodes.ToArray();
        }
        #endregion

    }
}
