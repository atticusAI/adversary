﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;
using Adversary.DataLayer.HRDataService;

namespace Adversary.DataLayer
{
    public class Queue : IDisposable
    {

        #region V1 Logic

        private bool _queuesRun = false;

        public bool QueuesRun
        {
            get
            {
                return this._queuesRun;
            }
        }

        private List<Memo> _backupList = null;

        public List<Memo> BackupList
        {
            get
            {
                return this._backupList;
            }
            set
            {
                this._backupList = value;
            }
        }

        private List<Memo> _processingList = null;

        public List<Memo> ProcessingList
        {
            get
            {
                return this._processingList;
            }
            set
            {
                this._processingList = value;
            }
        }

        private List<Memo> _primaryList = null;

        public List<Memo> PrimaryList
        {
            get
            {
                return this._primaryList;
            }
            set
            {
                this._primaryList = value;
            }
        }

        public void ProduceQueues(AdminLogin login, int? office, int? statusTypeID, int? userID, string pracCode, bool sortOldestFirst, bool setForDualRole, bool setForPGM)
        {
            if ((login != null) && ((login.IsAdversary ?? false) || (login.IsAdversaryAdmin ?? false) || (login.IsAP ?? false) || 
                (login.IsFinancialAdmin ?? false) || (login.IsPGM ?? false) || (login.IsPrimaryAP ?? false)) )
            {
                bool isAdversary = (login.IsAdversary.HasValue && login.IsAdversary.Value);
                bool bindDualRole = (setForDualRole && office == login.Office && pracCode == "my");

                List<Memo> memoList;
                memoList = this.GetQueue(statusTypeID, userID, sortOldestFirst);

                //filter out rejected memo for non-adversary
                if (!isAdversary)
                    memoList = memoList.Where(memo => memo.ScrMemType != 6).ToList();

                //filter by office
                if (office != null && !bindDualRole)
                {
                    //TODO: temp fix for Carson City, remove this when multiple APs allowed
                    if (office == 67)
                        memoList = memoList.Where(memo => this.GetOfficeNumber(memo.RespAttID) == 67 || this.GetOfficeNumber(memo.RespAttID) == 68).ToList();
                    else
                        memoList = memoList.Where(memo => (this.GetOfficeNumber(memo.RespAttID) == -1) || (this.GetOfficeNumber(memo.RespAttID) == office)).ToList();
                }

                if (bindDualRole)
                {
                    var query = (from m in memoList
                                 from p in login.AdminPracCodes
                                 where (m.PracCode == p.PracCode) ||
                                       (login.Office == 67 
                                       ? (GetOfficeNumber(m.RespAttID) == 67 || GetOfficeNumber(m.RespAttID) == 68) 
                                       : (GetOfficeNumber(m.RespAttID) == -1 || GetOfficeNumber(m.RespAttID) == office))
                                 select m).Distinct();

                    memoList = query.ToList();
                }
                else if (setForPGM && pracCode == "my")
                {
                    var query = from m in memoList
                                from p in login.AdminPracCodes
                                where m.PracCode == p.PracCode
                                select m;
                    memoList = query.ToList();
                }
                else if (pracCode != string.Empty)
                    memoList = memoList.Where(memo => memo.PracCode == pracCode).ToList();


                if (bindDualRole)
                    memoList = memoList.OrderBy(m => m, new SubmittedOnComparer(sortOldestFirst)).ToList();


                if (isAdversary)
                {
                    ProcessingList = (from m in memoList
                                      join t in this.DataContext.Trackings on m.ScrMemID equals t.ScrMemID
                                      where t.StatusTypeID == 3
                                      select m).Distinct().ToList();

                    memoList = memoList.Except(ProcessingList).ToList();
                }
                else
                {
                    bool isAP = login.IsAP.HasValue && login.IsAP.Value; //&& !_setForDualRole;
                    bool isPGM = login.IsPGM.HasValue && login.IsPGM.Value;// && !_setForDualRole;

                    if (setForDualRole)
                    {
                        ProcessingList = (from m in memoList
                                          join t in this.DataContext.Trackings on m.ScrMemID equals t.ScrMemID
                                          where (t.StatusTypeID == 3) ||
                                                ((login.Office == 67
                                                ? (GetOfficeNumber(m.RespAttID) != 67 && GetOfficeNumber(m.RespAttID) != 68)
                                                : (GetOfficeNumber(m.RespAttID) != login.Office)) ||
                                                 ((login.Office == 67
                                                ? (GetOfficeNumber(m.RespAttID) == 67 || GetOfficeNumber(m.RespAttID) == 68)
                                                : (GetOfficeNumber(m.RespAttID) == login.Office)) && t.APApproved.HasValue && t.APApproved.Value)) &&

                                                ((!this.IsMemberOfPracticeGroup(login, m.PracCode)) ||
                                                 (this.IsMemberOfPracticeGroup(login, m.PracCode) && t.PGMApproved.HasValue && t.PGMApproved.Value))
                                          select m).Distinct().ToList();
                    }
                    else
                    {
                        ProcessingList = (from m in memoList
                                          join t in this.DataContext.Trackings on m.ScrMemID equals t.ScrMemID
                                          where (t.StatusTypeID == 3) ||
                                                ((isAP && 
                                                (login.Office == 67
                                                ? GetOfficeNumber(m.RespAttID) == 67 || GetOfficeNumber(m.RespAttID) == 68
                                                : GetOfficeNumber(m.RespAttID) == login.Office) && 
                                                m.Trackings[0].APApproved.HasValue && 
                                                m.Trackings[0].APApproved.Value) ||
                                                 (isPGM && 
                                                 this.IsMemberOfPracticeGroup(login, m.PracCode) && 
                                                 m.Trackings[0].PGMApproved.HasValue && 
                                                 m.Trackings[0].PGMApproved.Value))
                                          select m).Distinct().ToList();
                    }

                    memoList = memoList.Except(ProcessingList).ToList();

                    //string officeCity = _cms.AllOffices.AsEnumerable().Where(row => row.Field<string>(CMSData.OFFICE_CODE) == _login.Office.ToString()).Single()[CMSData.OFFICE_DESCRIPTION].ToString();
                    //if (officeCity == "Tech Center")
                    //    officeCity = "DTC";

                    PrimaryList = (from m in memoList.Except(ProcessingList).ToList()
                                   join t in this.DataContext.Trackings on m.ScrMemID equals t.ScrMemID
                                   from p in login.AdminPracCodes.DefaultIfEmpty()
                                   where ((login.Office == 67 
                                   ? (GetOfficeNumber(m.RespAttID) == 67 || GetOfficeNumber(m.RespAttID) == 68) 
                                   : GetOfficeNumber(m.RespAttID) == login.Office)
                                   && (isAP /*|| _setForDualRole*/) 
                                   && login.IsPrimaryAP.HasValue 
                                   && login.IsPrimaryAP.Value) 
                                   || (t.SubmittedOn.HasValue && t.SubmittedOn.Value <= DateTime.Now.AddDays(-4)) 
                                   || (p != null && (isPGM /*|| _setForDualRole*/) && m.PracCode == p.PracCode && p.IsPrimary.HasValue && p.IsPrimary.Value)
                                   select m).Distinct().ToList();

                    BackupList = memoList.Except(PrimaryList).ToList();
                }

                if (PrimaryList == null)
                {
                    PrimaryList = memoList;
                }
            }
            this._queuesRun = true;
        }

        private bool IsMemberOfPracticeGroup(AdminLogin login, string practiceCode)
        {
            return this.GetAllPracticeCodes(login.UserID).Select(p => p.PracCode).Contains(practiceCode);
        }

        private List<Memo> GetQueue(int? statusTypeID, int? userID, bool sortOldestFirst)
        {
            return DataContext.PR_GET_OpenMemos(statusTypeID, userID, sortOldestFirst).ToList();
        }

        private class SubmittedOnComparer : IComparer<Memo>
        {
            bool _desc = false;
            protected internal SubmittedOnComparer(bool sortOldestFirst)
            {
                _desc = sortOldestFirst;
            }
            int IComparer<Memo>.Compare(Memo x, Memo y)
            {
                try
                {
                    if (_desc)
                        return DateTime.Compare(x.Trackings[0].SubmittedOn.Value, y.Trackings[0].SubmittedOn.Value);
                    else
                        return DateTime.Compare(y.Trackings[0].SubmittedOn.Value, x.Trackings[0].SubmittedOn.Value);
                }
                catch
                {
                    return 0;
                }
            }
        }
        
        private string GetOfficeCode(string respAttID)
        {
            string officeCode = "";
            using (AdversaryData adversaryDataLayer = new AdversaryData())
            {
                FirmOffice office = adversaryDataLayer.GetAttorneyOffice(respAttID);
                if (office != null)
                {
                    officeCode = office.LocationCode;
                }
            }
            return officeCode;
        }

        private AdversaryData _adversaryDataLayer = null;

        private AdversaryData AdversaryDataLayer
        {
            get
            {
                if (this._adversaryDataLayer == null)
                {
                    this._adversaryDataLayer = new AdversaryData();
                }
                return this._adversaryDataLayer;
            }
        }

        private int GetOfficeNumber(string respAttID)
        {
            int officeNumber = -1;

            FirmOffice office = AdversaryDataLayer.GetAttorneyOffice(respAttID);
            if (office != null)
            {
                officeNumber = Convert.ToInt32(office.LocationCode);
            }

            return officeNumber;
        }

        #endregion 

        //#region Base Query

        //public IQueryable<Memo> GetOpenMemos(int? userID)
        //{
        //    return this.GetOpenMemos(userID, null);
        //}

        //public IQueryable<Memo> GetOpenMemos(int? userID, int? statusTypeID)
        //{
        //    var qry = from m in this.DataContext.Memos
        //              join t in this.DataContext.Trackings on m.ScrMemID equals t.ScrMemID
        //              where ((userID == null) || (t.APUserID == userID || t.PGMUserID == userID || t.AdversaryUserID == userID))
        //              && ((statusTypeID == null && t.StatusTypeID != null && t.StatusTypeID > 0 && t.StatusTypeID < 100)
        //                || t.StatusTypeID == statusTypeID)
        //              select m;
        //    return qry;
        //}

        //#endregion

        //#region Dual Role Queue

        //public List<Memo> GetDualRolePrimaryQueue(int? userID, int? statusTypeID, string officeCode, List<string> includePracCodes, bool sortNewestFirst)
        //{
        //    List<Memo> memos = null;
        //    var qry = from m in this.GetOpenMemos(userID, statusTypeID)
        //              join t in this.DataContext.Trackings on m.ScrMemID equals t.ScrMemID
        //              where ((includePracCodes.Contains(m.PracCode) && (!(t.PGMApproved ?? false)))
        //                || (!(t.APApproved ?? false)))
        //              && (t.StatusTypeID != 3)
        //              && (m.ScrMemType != 6)
        //              && (t.SubmittedOn != null && t.SubmittedOn >= DateTime.Now.AddDays(-4))
        //              select m;
        //    memos = this.SortQueue(this.DataContext, qry, sortNewestFirst);
        //    return memos;
        //}

        //public List<Memo> GetDualRoleProcessingQueue(int? userID, int? statusTypeID, string officeCode, List<string> includePracCodes, bool sortNewestFirst)
        //{
        //    List<Memo> memos = null;
        //    var qry = from m in this.GetOpenMemos(userID, statusTypeID)
        //              join t in this.DataContext.Trackings on m.ScrMemID equals t.ScrMemID
        //              where ((t.StatusTypeID == 3)
        //                    && ((t.APApproved ?? false))
        //                    && ((!includePracCodes.Contains(m.PracCode)) || (t.PGMApproved ?? false))
        //                    )
        //                    && (m.ScrMemType != 6)
        //              select m;
        //    memos = this.SortQueue(this.DataContext, qry, sortNewestFirst, officeCode);
        //    return memos;
        //}

        //public List<Memo> GetDualRoleBackupQueue(int? userID, int? statusTypeID, string officeCode, List<string> includePracCodes, bool sortNewestFirst)
        //{
        //    List<Memo> memos = null;
        //    var qry = from m in this.GetOpenMemos(userID)
        //              join t in this.DataContext.Trackings on m.ScrMemID equals t.ScrMemID
        //              where (userID == null || t.APUserID == userID || t.PGMUserID == userID || t.AdversaryUserID == userID)
        //              && ((includePracCodes.Contains(m.PracCode) && (!(t.PGMApproved ?? false)))
        //                || ((!(t.APApproved ?? false))
        //                    ))
        //              && (t.StatusTypeID != 3 && t.StatusTypeID != null && t.StatusTypeID > 0 && t.StatusTypeID < 100)
        //              && (m.ScrMemType != 6)
        //              && (t.SubmittedOn != null && t.SubmittedOn <= DateTime.Now.AddDays(-4))
        //              select m;
        //    memos = this.SortQueue(this.DataContext, qry, sortNewestFirst, officeCode);
        //    return memos;
        //}

        //#endregion

        //#region Default Queue

        //public List<Memo> GetDefaultProcessingQueue(int? userID, string officeCode, List<string> includePracCodes, bool sortNewestFirst)
        //{
        //    List<Memo> memos = null;

        //    var qry = from m in this.DataContext.Memos
        //              join t in this.DataContext.Trackings on m.ScrMemID equals t.ScrMemID
        //              where (userID == null || t.APUserID == userID || t.PGMUserID == userID || t.AdversaryUserID == userID)
        //              && (t.StatusTypeID == 3)
        //              && (includePracCodes == null || includePracCodes.Count == 0 || includePracCodes.Contains(m.PracCode))
        //              select m;
        //    memos = this.SortQueue(this.DataContext, qry, sortNewestFirst, officeCode);
        //    return memos;
        //}

        //public List<Memo> GetDefaultPrimaryQueue(int? userID, int? statusTypeID, string officeCode, List<string> includePracCodes, bool sortNewestFirst)
        //{
        //    List<Memo> memos = null;
        //    var qry = from m in this.DataContext.Memos
        //              join t in this.DataContext.Trackings on m.ScrMemID equals t.ScrMemID
        //              where (userID == null || t.APUserID == userID || t.PGMUserID == userID || t.AdversaryUserID == userID)
        //              && ((statusTypeID == null && t.StatusTypeID != null && t.StatusTypeID > 0 && t.StatusTypeID < 100)
        //              || (t.StatusTypeID == statusTypeID))
        //              && (includePracCodes == null || includePracCodes.Count == 0 || includePracCodes.Contains(m.PracCode))
        //              && ((string.IsNullOrWhiteSpace(officeCode))
        //              || (officeCode == "67" && (m.RespAttOfficeCode == "67" || m.RespAttOfficeCode == "68"))
        //              || (m.RespAttOfficeCode == officeCode)
        //              || (m.RespAttOfficeCode == null))
        //              && (t.StatusTypeID != 3)
        //              && (m.ScrMemType != 6)
        //              select m;
        //    memos = this.SortQueue(this.DataContext, qry, sortNewestFirst);
        //    return memos;
        //}

        //public List<Memo> GetDefaultBackupQueue(int? userID, string officeCode, List<string> includePracCodes, bool sortNewestFirst)
        //{
        //    List<Memo> memos = null;
        //    var qry = from m in this.DataContext.Memos
        //              join t in this.DataContext.Trackings on m.ScrMemID equals t.ScrMemID
        //              where (userID == null || t.APUserID == userID || t.PGMUserID == userID || t.AdversaryUserID == userID)
        //              && ((includePracCodes.Contains(m.PracCode) && (!(t.PGMApproved ?? false)))
        //                || ((string.IsNullOrWhiteSpace(officeCode) && (!(t.APApproved ?? false)))
        //                    || ((m.RespAttOfficeCode == officeCode) && (!(t.APApproved ?? false)))))
        //              && (t.StatusTypeID != 3 && t.StatusTypeID != null && t.StatusTypeID > 0 && t.StatusTypeID < 100)
        //              && (m.ScrMemType != 6)
        //              && (t.SubmittedOn != null && t.SubmittedOn >= DateTime.Now.AddDays(-4))
        //              select m;
        //    memos = this.SortQueue(this.DataContext, qry, sortNewestFirst);
        //    return memos;
        //}

        //#endregion

        //#region PGM Queue

        //public List<Memo> GetPGMProcessingQueue(int? userID, string officeCode, int? statusTypeID, List<string> includePracCodes, bool sortNewestFirst)
        //{
        //    List<Memo> memos = null;
        //    var qry = from m in this.DataContext.Memos
        //              join t in this.DataContext.Trackings on m.ScrMemID equals t.ScrMemID
        //                where (userID == null || t.APUserID == userID || t.PGMUserID == userID || t.AdversaryUserID == userID)
        //                // && ((statusTypeID == null  && t.StatusTypeID != null && t.StatusTypeID > 0 && t.StatusTypeID < 100) || t.StatusTypeID == statusTypeID)
        //                && (includePracCodes.Contains(m.PracCode))
        //                && ((string.IsNullOrWhiteSpace(officeCode))
        //                || (officeCode == "67" && (m.RespAttOfficeCode == "67" || m.RespAttOfficeCode == "68"))
        //                || (m.RespAttOfficeCode == officeCode))
        //                && ((t.StatusTypeID == 3)) // || (m.ScrMemType != 6))
        //                && (t.PGMApproved ?? false)
        //                select m;
        //    memos = this.SortQueue(this.DataContext, qry, sortNewestFirst);
        //    return memos;
        //}

        //public List<Memo> GetPGMPrimaryQueue(int? userID, string officeCode, int? statusTypeID, List<string> includePracCodes, bool sortNewestFirst)
        //{
        //    List<Memo> memos = null;
        //    var qry = from m in this.DataContext.Memos
        //              join t in this.DataContext.Trackings on m.ScrMemID equals t.ScrMemID
        //              where (userID == null || t.APUserID == userID || t.PGMUserID == userID || t.AdversaryUserID == userID)
        //              && ((statusTypeID == null && t.StatusTypeID != null && t.StatusTypeID > 0 && t.StatusTypeID < 100) || t.StatusTypeID == statusTypeID)
        //              && (includePracCodes.Contains(m.PracCode))
        //              && ((string.IsNullOrWhiteSpace(officeCode))
        //                || (officeCode == "67" && (m.RespAttOfficeCode == "67" || m.RespAttOfficeCode == "68"))
        //                || (m.RespAttOfficeCode == officeCode))
        //              && (m.ScrMemType != 6)
        //              && (!(t.PGMApproved ?? false))
        //              && (t.SubmittedOn != null && t.SubmittedOn <= DateTime.Now.AddDays(-4))
        //              select m;
        //    memos = this.SortQueue(this.DataContext, qry, sortNewestFirst);
        //    return memos;
        //}

        //public List<Memo> GetPGMBackupQueue(int? userID, string officeCode, int? statusTypeID, List<string> includePracCodes, bool sortNewestFirst)
        //{
        //    List<Memo> memos = null;
        //    var qry = from m in this.DataContext.Memos
        //              join t in this.DataContext.Trackings on m.ScrMemID equals t.ScrMemID
        //              where (userID == null || t.APUserID == userID || t.PGMUserID == userID || t.AdversaryUserID == userID)
        //              && ((statusTypeID == null && t.StatusTypeID != null && t.StatusTypeID > 0 && t.StatusTypeID < 100) || t.StatusTypeID == statusTypeID)
        //              && (includePracCodes.Contains(m.PracCode))
        //              && ((string.IsNullOrWhiteSpace(officeCode))
        //                || (officeCode == "67" && (m.RespAttOfficeCode == "67" || m.RespAttOfficeCode == "68"))
        //                || (m.RespAttOfficeCode == officeCode))
        //              && (m.ScrMemType != 6)
        //              && (!(t.PGMApproved ?? false))
        //              && (t.SubmittedOn != null && t.SubmittedOn >= DateTime.Now.AddDays(-4))
        //              select m;
        //    memos = this.SortQueue(this.DataContext, qry, sortNewestFirst);
        //    return memos;
        //}

        //#endregion

        //#region AP Queue

        //public List<Memo> GetAPProcessingQueue(int? userID, string officeCode, int? statusTypeID, List<string> includePracCodes, bool sortNewestFirst)
        //{
        //    List<Memo> memos = null;
        //    var qry = from m in this.DataContext.Memos
        //              join t in this.DataContext.Trackings on m.ScrMemID equals t.ScrMemID
        //              where (userID == null || t.APUserID == userID || t.PGMUserID == userID || t.AdversaryUserID == userID)
        //              && ((statusTypeID == null && t.StatusTypeID != null && t.StatusTypeID > 0 && t.StatusTypeID < 100) || t.StatusTypeID == statusTypeID)
        //              && (includePracCodes == null || includePracCodes.Count == 0 || includePracCodes.Contains(m.PracCode))
        //              && (m.ScrMemType != 6)
        //              && ((officeCode == "67" && (m.RespAttOfficeCode == "67" || m.RespAttOfficeCode == "68"))
        //                || (m.RespAttOfficeCode == officeCode))
        //              && ((t.StatusTypeID == 3) || (t.APApproved ?? false))
        //              select m;
        //    memos = this.SortQueue(this.DataContext, qry, sortNewestFirst);
        //    return memos;
        //}

        //public List<Memo> GetAPPrimaryQueue(int? userID, string officeCode, int? statusTypeID, List<string> includePracCodes, bool sortNewestFirst)
        //{
        //    List<Memo> memos = null;
        //    var qry = from m in this.DataContext.Memos
        //              join t in this.DataContext.Trackings on m.ScrMemID equals t.ScrMemID
        //                where (userID == null || t.APUserID == userID || t.PGMUserID == userID || t.AdversaryUserID == userID)
        //                && ((statusTypeID == null && t.StatusTypeID != null && t.StatusTypeID > 0 && t.StatusTypeID < 100) 
        //                || (t.StatusTypeID == statusTypeID))
        //                && (includePracCodes == null || includePracCodes.Count == 0 || includePracCodes.Contains(m.PracCode))
        //                && (t.StatusTypeID != 3)
        //                && (m.ScrMemType != 6)
        //                && ((officeCode == "67" && (m.RespAttOfficeCode == "67" || m.RespAttOfficeCode == "68"))
        //                || (m.RespAttOfficeCode == officeCode))
        //                && ((t.APApproved ?? false) == false)
        //                && (t.SubmittedOn != null && t.SubmittedOn <= DateTime.Now.AddDays(-4))
        //                select m;
        //    memos = this.SortQueue(this.DataContext, qry, sortNewestFirst);
        //    return memos;
        //}

        //public List<Memo> GetAPBackupQueue(int? userID, string officeCode, int? statusTypeID, List<string> includePracCodes, bool sortNewestFirst)
        //{
        //    List<Memo> memos = null;
        //    var qry = from m in this.DataContext.Memos
        //              join t in this.DataContext.Trackings on m.ScrMemID equals t.ScrMemID
        //              where (userID == null || t.APUserID == userID || t.PGMUserID == userID || t.AdversaryUserID == userID)
        //              && ((statusTypeID == null && t.StatusTypeID != null && t.StatusTypeID > 0 && t.StatusTypeID < 100)
        //              || (t.StatusTypeID == statusTypeID))
        //              && (includePracCodes == null || includePracCodes.Count == 0 || includePracCodes.Contains(m.PracCode))
        //              && (t.StatusTypeID != 3)
        //              && (m.ScrMemType != 6)
        //              && ((officeCode == "67" && (m.RespAttOfficeCode == "67" || m.RespAttOfficeCode == "68"))
        //              || (m.RespAttOfficeCode == officeCode))
        //              && ((t.APApproved ?? false) == false)
        //              && (t.SubmittedOn != null && t.SubmittedOn >= DateTime.Now.AddDays(-4))
        //              select m;
        //    memos = this.SortQueue(this.DataContext, qry, sortNewestFirst);
        //    return memos;
        //}

        //#endregion

        //#region Adversary Queue

        //public List<Memo> GetAdversaryProcessingQueue(int? userID, string officeCode, List<string> includePracCodes, bool sortNewestFirst)
        //{
        //    List<Memo> memos = null;
        //    var qry = from m in this.DataContext.Memos
        //                join t in this.DataContext.Trackings on m.ScrMemID equals t.ScrMemID
        //                where (userID == null || t.APUserID == userID || t.PGMUserID == userID || t.AdversaryUserID == userID)
        //                && (t.StatusTypeID == 3)
        //                && (includePracCodes == null || includePracCodes.Count == 0 || includePracCodes.Contains(m.PracCode))
        //                && ((string.IsNullOrWhiteSpace(officeCode))
        //                || (officeCode == "67" && (m.RespAttOfficeCode == "67" || m.RespAttOfficeCode == "68"))
        //                || (m.RespAttOfficeCode == officeCode))
        //                select m;
        //    memos = this.SortQueue(this.DataContext, qry, sortNewestFirst);
        //    return memos;
        //}

        //public List<Memo> GetAdversaryPrimaryQueue(int? userID, int? statusTypeID, string officeCode, List<string> includePracCodes, bool sortNewestFirst)
        //{
        //    List<Memo> memos = null;
        //    var qry = from m in this.DataContext.Memos
        //              join t in this.DataContext.Trackings on m.ScrMemID equals t.ScrMemID
        //                where (userID == null || t.APUserID == userID || t.PGMUserID == userID || t.AdversaryUserID == userID)
        //                && ((statusTypeID == null && t.StatusTypeID != null && t.StatusTypeID > 0 && t.StatusTypeID < 100) 
        //                || (t.StatusTypeID == statusTypeID))
        //                && (includePracCodes == null || includePracCodes.Count == 0 || includePracCodes.Contains(m.PracCode))
        //                && ((string.IsNullOrWhiteSpace(officeCode))
        //                || (officeCode == "67" && (m.RespAttOfficeCode == "67" || m.RespAttOfficeCode == "68"))
        //                || (m.RespAttOfficeCode == officeCode)
        //                || (m.RespAttOfficeCode == null))
        //                && (t.StatusTypeID != 3)
        //                select m;
        //    memos = this.SortQueue(this.DataContext, qry, sortNewestFirst);
        //    return memos;
        //}
        //#endregion

        public List<Memo> GetAdversaryQueue()
        {
            List<Memo> memos = null;
            var qry = from m in this.DataContext.Memos
                      join t in this.DataContext.Trackings on m.ScrMemID equals t.ScrMemID
                      where t.StatusTypeID == 3
                      select m;
            memos = this.SortQueue(this.DataContext, qry, false);
            return memos;
        }
        
        # region Queue Sorting

        public List<Memo> SortQueue(AdversaryDataContext dc, IQueryable<Memo> qry, bool sortNewestFirst)
        {
            return SortQueue(dc, qry, sortNewestFirst, null);
        }

        public List<Memo> SortQueue(AdversaryDataContext dc, IQueryable<Memo> qry, bool sortNewestFirst, string officeCode)
        {
            if (sortNewestFirst)
            {
                qry = from m in qry
                      join t in dc.Trackings on m.ScrMemID equals t.ScrMemID
                      orderby t.SubmittedOn
                      select m;
            }
            else
            {
                qry = from m in qry
                      join t in dc.Trackings on m.ScrMemID equals t.ScrMemID
                      orderby t.SubmittedOn descending
                      select m;
            }

            //if (!string.IsNullOrWhiteSpace(officeCode))
            //{
            //    return this.FilterOffices(qry, officeCode);
            //}
            //else
            //{
                return qry.ToList();
            //}
        }

        #endregion

        //#region Office Filtering

        //private List<Memo> FilterOffices(IQueryable<Memo> qry, string officeCode)
        //{
        //    List<Memo> memos = qry.ToList();
        //    return memos.Where(m => IsRespAttOfficeCode(m, officeCode)).ToList();
        //}

        //private bool IsRespAttOfficeCode(Memo memo, string officeCode)
        //{
        //    string respAttOfficeCode =  this.GetOfficeCode(memo.RespAttID);
        //    return (officeCode == "67" && (respAttOfficeCode == "67" || respAttOfficeCode == "68"))
        //              || (respAttOfficeCode == officeCode);
        //}

        //#endregion

        #region Practice Code Getters

        public List<AdminLogin> GetAdminLoginsByPrimaryPracticeCode(string pracCode)
        {
            var qry = from a in this.DataContext.AdminLogins
                      join p in this.DataContext.AdminPracCodes on a.UserID equals p.UserID
                      where p.PracCode == pracCode && (a.IsPGM ?? false) && (p.IsPrimary ?? false)
                      select a;
            return qry.Distinct().ToList();
        }

        public List<AdminPracCode> GetAllPracticeCodes(int userID)
        {
            var qry = from apc in this.DataContext.AdminPracCodes
                      where apc.UserID == userID
                      select apc;
            return qry.ToList();
        }

        //public List<AdminPracCode> GetPracticeCodes(int userID)
        //{
        //    var qry = from apc in this.DataContext.AdminPracCodes
        //              where apc.UserID == userID
        //                  && ((apc.IsPrimary ?? false) == true)
        //              select apc;
        //    return qry.ToList();
        //}

        //public List<AdminPracCode> GetBackupPracticeCodes(int userID)
        //{
        //    var qry = from apc in this.DataContext.AdminPracCodes
        //            where apc.UserID == userID
        //                && ((apc.IsPrimary ?? false) == false)
        //            select apc;
        //    return qry.ToList();
        //}

        #endregion

        #region Data Context
        
        private AdversaryDataContext _dataContext = null;

        public AdversaryDataContext DataContext
        {
            get
            {
                if (this._dataContext == null)
                {
                    this._dataContext = new AdversaryDataContext();
                }
                return this._dataContext;
            }
        }

        #endregion

        #region IDisposable

        public void Dispose()
        {
            if (this._dataContext != null)
            {
                this._dataContext.Dispose();
                this._dataContext = null;
            }
            if (this._adversaryDataLayer != null)
            {
                this._adversaryDataLayer.Dispose();
                this._adversaryDataLayer = null;
            }
        }

        #endregion
    }
}
