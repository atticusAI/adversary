﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.DataLayer
{
    public class Navigation : IDisposable
    {
        private AdversaryDataContext _dataContext = null;

        public AdversaryDataContext DataContext
        {
            get
            {
                if (this._dataContext == null)
                {
                    this._dataContext = new AdversaryDataContext();
                }
                return this._dataContext;
            }
        }

        public List<SubTabNavigation> GetSubTabNavigationByPageUrl(string pageUrl)
        {
            return (from tab in this.DataContext.SubTabNavigations
                    join page in this.DataContext.PageNavigations on tab.PageNavigationID equals page.PageNavigationID
                    where page.PageUrl.Replace("~", "") == pageUrl.Replace("~", "")
                    && tab.ParentSubTabNavigationID == null
                    orderby tab.SubTabOrder 
                    select tab).ToList();
        }

        public List<SubTabNavigation> GetSubTabNavigationByParentSubTabNavigationID(int parentSubTabNavigationID)
        {
            return (from tab in this.DataContext.SubTabNavigations
                    where tab.ParentSubTabNavigationID == parentSubTabNavigationID
                    orderby tab.SubTabOrder
                    select tab).ToList();
        }

        public List<PageNavigation> GetPageNavigationByPageUrl(string pageUrl)
        {
            return (from tab in this.DataContext.TabNavigations
                    join page in this.DataContext.PageNavigations on tab.ParentPageNavigationID equals page.PageNavigationID
                    join page2 in this.DataContext.PageNavigations on tab.PageNavigationID equals page2.PageNavigationID
                    where page.PageUrl.Replace("~", "") == pageUrl.Replace("~", "")
                    orderby tab.TabOrder
                    select page2).ToList();
        }

        public void Dispose()
        {
            if (this._dataContext != null)
            {
                this._dataContext.Dispose();
                this._dataContext = null;
            }
        }
    }
}
