﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.DataLayer
{
    public class ConflictCollection : IDisposable, ICollection<Conflict>
    {
        #region Data Context

        private AdversaryDataContext _dataContext = null;

        private AdversaryDataContext DataContext
        {
            get
            {
                if (this._dataContext == null)
                {
                    this._dataContext = new AdversaryDataContext();
                }
                return this._dataContext;
            }
        }

        #endregion

        #region Construction

        private readonly int _trackNumber;

        public ConflictCollection(int trackNumber)
        {
            this._trackNumber = trackNumber;
        }

        #endregion

        #region Load 

        private List<Conflict> Load()
        {
            var qry = from c in this.DataContext.Conflicts
                      where c.TrackNumber == this._trackNumber
                      && c.Active == true 
                      orderby c.SortOrder
                      select c;
            
            return qry.ToList();
        }

        #endregion

        #region Storage

        private List<Conflict> _conflicts = null;

        private List<Conflict> Conflicts
        {
            get
            {
                if (this._conflicts == null || this._conflicts.Count == 0)
                {
                    this._conflicts = this.Load();
                }
                return this._conflicts;
            }
        }

        #endregion

        #region IDisposable

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this._dataContext != null)
                {
                    this._dataContext.Dispose();
                    this._dataContext = null;
                }
            }
        }

        #endregion 

        #region ICollection

        public void Add(Conflict item)
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(Conflict item)
        {
            return this.Conflicts.Contains(item);
        }

        public void CopyTo(Conflict[] array, int arrayIndex)
        {
            this.Conflicts.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return this.Conflicts.Count(); }
        }

        public bool IsReadOnly
        {
            get { return true; }
        }

        public bool Remove(Conflict item)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<Conflict> GetEnumerator()
        {
            return this.Conflicts.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.Conflicts.GetEnumerator();
        }

        #endregion
    }
}
