﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Adversary.DataLayer;

namespace Adversary.Screening
{
    public class MemoMetadata
    {
        private Adversary.DataLayer.Memo _Memo;
        public Adversary.DataLayer.Memo Memo
        {
            set { _Memo = value; }
        }

        private Adversary.DataLayer.Tracking _tracking;

        public MemoMetadata(Adversary.DataLayer.Memo memo)
        {
            _Memo = memo;
            _tracking = _Memo.Trackings.FirstOrDefault();
        }

        public bool IsValid()
        {
            MemoRuleEngine memoRule = new MemoRuleEngine(_Memo);
            memoRule.RunRules();
            return !memoRule.HasErrors;
        }

        public bool IsLocked()
        {
            if (_tracking == null)
                return false;
            else
                return (_tracking.Locked.HasValue && _tracking.Locked.Value) || ((_Memo.ScrMemID != 0 && (_Memo.ScrMemID < 100000)));
        }

        public bool IsMemoDocketingRequired()
        {
            bool returnval = true;
            if (
                string.IsNullOrEmpty(_Memo.PracCode) ||
                _Memo.PracCode.Contains("E") ||
                _Memo.PracCode.Contains("T") ||
                _Memo.PracCode.Equals("501L") ||
                _Memo.PracCode.Equals("502L")) returnval = false;

            return returnval;
                    
        }

        public bool CanSubmit()
        {
            bool _canSubmit = true;

            if (!IsValid())
                _canSubmit = false;
            if (IsLocked())
                _canSubmit = false;
            if (_tracking == null)
            {
                _canSubmit = true;
            }
            else if (_canSubmit == true)
            {
                if (_tracking.Locked.HasValue && _tracking.Locked.Value)
                    _canSubmit = false;
                
                
                if (!(_tracking.RejectionHold.HasValue && _tracking.RejectionHold.Value && ((int)_tracking.StatusTypeID >= 200)))
                    _canSubmit = false;
            }

            return _canSubmit;
        }

    }
}
