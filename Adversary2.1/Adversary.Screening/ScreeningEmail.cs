﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Adversary.DataLayer;
using System.Net.Mail;
using System.Net;
using System.Configuration;
using Adversary.Utils;
using Adversary.DataLayer.HRDataService;

namespace Adversary.Screening
{
    public class ScreeningEmail
    {
        //private string Adversary.Utils.StringUtils.TwoLineFeedString = "\r\n\r\n";

        private MailAddress fromAddress;

        private List<AdminLogin> _AdminLogins;
        public List<AdminLogin> AdminLogins
        {
            get
            {
                if (_AdminLogins == null)
                    _AdminLogins = new List<AdminLogin>();
                return _AdminLogins;
            }
            set { _AdminLogins = value; }
        }

        private AdversaryData advData;

        private bool _HasErrors;
        public bool HasErrors
        {
            get { return _HasErrors; }
            set { _HasErrors = value; }
        }

        private string _ErrorMessage;
        public string ErrorMessage
        {
            get { return _ErrorMessage; }
            set { _ErrorMessage = value; }
        }

        public ScreeningEmail()
        {
            advData = new AdversaryData();
            fromAddress = new MailAddress(ConfigurationManager.AppSettings["EmailSendAddress"],
                ConfigurationManager.AppSettings["EmailSendAddress"]);
        }

        private int GetOfficeNumber(string respAttID)
        {
            int officeNumber = -1;

            FirmOffice office = advData.GetAttorneyOffice(respAttID);
            if (office != null)
            {
                officeNumber = Convert.ToInt32(office.LocationCode);
            }

            return officeNumber;
        }

        public void SendErrorEmail(string errorMsg)
        {
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["EmailErrorAddress"]))
            {
                try
                {
                    MailAddressCollection to = new MailAddressCollection(); 
                    foreach (string recipient in ConfigurationManager.AppSettings["EmailErrorAddress"].Split(';'))
                    {
                        if (!string.IsNullOrWhiteSpace(recipient))
                        {
                            to.Add(new MailAddress(recipient.Trim()));
                        }
                    }
                    if (to.Count > 0)
                    {
                        SendEmail(to, "Adversary Site Error", errorMsg);
                    }
                }
                catch { }
            }
        }

        public void SendApproversEmail(Memo memo, string baseUrl) 
        { 
            SendApproversEmail(memo, false, baseUrl); 
        }

        public void SendApproversEmail(Memo memo, bool isResubmit, string baseUrl)
        {
            Adversary.DataLayer.AdminLoginsClass adminLoginsClass = new AdminLoginsClass();
            AdversaryData advData = new AdversaryData();
            Tracking memoTracking = memo.Trackings.First();
            StringBuilder msg = new StringBuilder("");
            if (memo.Trackings.First().StatusTypeID == 2)
            {
                msg.Append("Screening Memo #" + memo.ScrMemID + " has been approved for processing. Please click the link below" 
                + " to access your queue of new matters that require your review.");
                msg.AppendLine();
                msg.AppendLine(baseUrl + "Admin/AdminQueue.aspx");
            }
            else
            {
                msg.AppendLine("A new matter screening issue requires your attention. Please click the link below" 
                + " to access your queue of new matters that require your review.");
                msg.AppendLine();
                msg.AppendLine(baseUrl + "Admin/Queue.aspx");
            }
            msg.AppendLine();
            msg.AppendLine();

            MailAddressCollection adminRecipients = new MailAddressCollection();
            
            string adminMsg = string.Empty;

            #region switch on status type
            switch (memo.Trackings.First().StatusTypeID)
            {
                #region case 1 (submitted)
                case 1: //submitted

                    //set AP recipients for NON-Denver office memos (and NOT rejected memos)
                    if (GetOfficeNumber(memo.RespAttID) != 10 && memo.ScrMemType != 6)
                    {
                        try
                        {
                            //AdminLogin[] aps = adminLoginsClass.GetAPsByOffice(memo.RespAttOfficeCode, true);
                            AdminLogin[] aps = adminLoginsClass.GetAPsByOffice(advData.GetEmployee(memo.RespAttID).OfficeCode, true);
                            if (aps.Count() == 0)
                            {
                                AdminLogins.AddRange(adminLoginsClass.GetAdversaryGroupAdmins());
                                adminMsg += "* A corresponding AP was not found for the " + GetOfficeNumber(memo.RespAttID) + " office" + Adversary.Utils.StringUtils.TWO_LINE_FEED_STRING;
                            }
                            else
                            {
                                AdminLogins.AddRange(aps);
                            }
                        }
                        catch (Exception ex)
                        {
                            ///TODO : put error email logic here
                            throw ex;
                        }
                    }

                    
                    AdminLogin[] pgms = adminLoginsClass.GetPGMsByPracticeCode(memo.PracCode, false);
                    if (pgms.Count() == 0 && memo.ScrMemType != 6)
                    {
                        AdminLogins.AddRange(adminLoginsClass.GetAdversaryGroupAdmins());
                        adminMsg += "* A corresponding PGL was not found for practice code "
                            + advData.GetPracticeGroupsList().Where(P => P.matt_cat_code.Equals(memo.PracCode)).First().matt_cat_desc
                            + "." + Adversary.Utils.StringUtils.TWO_LINE_FEED_STRING;
                    }
                    else
                    {
                        AdminLogins.AddRange(pgms);
                    }

                    AdminLogins.AddRange(adminLoginsClass.GetAdversaryGroupAdmins());

                    break;
            #endregion

                #region case 3 (adversary group processing)
                case 3: //adversary group processing
                    AdminLogins.AddRange(adminLoginsClass.GetAdversaryGroup());
                    break;
                default:
                    AdminLogins.AddRange(adminLoginsClass.GetAdversaryGroupAdmins());
                    break;
                #endregion

            }
            if (AdminLogins.Count == 0)
            {
                throw new Exception("Could not send tracking email, no valid admin recipients found.");
            }
            #endregion

            foreach (AdminLogin admin in AdminLogins)
            {
                adminRecipients.Add(admin.Email);
            }
            if (!String.IsNullOrEmpty(adminMsg))
            {
                msg.AppendLine(adminMsg + Adversary.Utils.StringUtils.TWO_LINE_FEED_STRING);
            }

            if (!String.IsNullOrEmpty(adminMsg))
            {
                msg.AppendLine(adminMsg + Adversary.Utils.StringUtils.TWO_LINE_FEED_STRING);
            }

            msg.AppendLine(BuildMemoStatusEmail(memo));
            msg.AppendLine();
            msg.AppendLine(BuildMemoApproverEmail(memo));

            string subject = "Screening Memo #" + memo.ScrMemID.ToString();
            if (memoTracking != null)
            {
                if (memoTracking.StatusTypeID == 3) //adversary group processing
                {
                    subject += " - APPROVED";
                }
                else if (isResubmit)
                {
                    subject += " RESUBMITTED";
                }
                else if (memoTracking.StatusTypeID == 1)
                {
                    subject += " - SUBMITTED";
                }
            }
            SendEmail(adminRecipients, fromAddress, subject, msg.ToString(), false);
        }

        public void SendStatusEmail(Memo memo, string baseUrl, string userName)
        {
            SendStatusEmail(memo, false, baseUrl, userName); 
        }

        public void SendStatusEmail(Memo memo, string baseUrl) 
        { 
            SendStatusEmail(memo, false, baseUrl, null); 
        }

        private void SendStatusEmail(Memo memo, bool isResubmit, string baseUrl, string userName)
        {
            baseUrl += "Screening/Summary.aspx?ScrMemID=" + memo.ScrMemID;
            Adversary.DataLayer.Tracking _Tracking = memo.Trackings.First();
            Adversary.DataLayer.AdversaryData _AdvData = new AdversaryData();
            string msg = string.Empty;
            string subject = "Screening Memo #" + memo.ScrMemID;
            bool cmNumSent = false;
            if (_Tracking.StatusTypeID == 200 || _Tracking.StatusTypeID == 600) // mod request or rejected
            {
                StringBuilder rejectionMsg = new StringBuilder();
                rejectionMsg.Append("This memo requires modification. ");

                if (_Tracking.RejectionHold.HasValue && _Tracking.RejectionHold.Value)
                    rejectionMsg.Append("Please make amendments to this memo and resubmit. ");

                rejectionMsg.AppendLine();
                rejectionMsg.AppendLine();
                rejectionMsg.AppendLine("You can login to the New Business Intake Website and amend this memo at:" + Adversary.Utils.StringUtils.TWO_LINE_FEED_STRING + baseUrl);
                rejectionMsg.AppendLine();
                rejectionMsg.AppendLine("Reason(s) for Modification Request:");
                rejectionMsg.AppendLine();
                if (!string.IsNullOrEmpty(_Tracking.APRejectionReason))
                {
                    if (!string.IsNullOrWhiteSpace(userName))
                    {
                        rejectionMsg.AppendLine("From " + userName);
                    }
                    rejectionMsg.AppendLine("+ " + _Tracking.APRejectionReason);
                    rejectionMsg.AppendLine();
                }
                if (!string.IsNullOrEmpty(_Tracking.PGMRejectionReason))
                {
                    {
                        rejectionMsg.AppendLine("From " + userName);
                    }
                    rejectionMsg.AppendLine("+ " + _Tracking.PGMRejectionReason);
                    rejectionMsg.AppendLine();
                }
                if (!string.IsNullOrEmpty(_Tracking.AdversaryRejectionReason))
                {
                    {
                        rejectionMsg.AppendLine("From " + userName);
                    }
                    rejectionMsg.AppendLine("+ " + _Tracking.AdversaryRejectionReason);
                    rejectionMsg.AppendLine();
                }
                rejectionMsg.AppendLine();

                msg = BuildMemoStatusEmail(memo);
                msg += rejectionMsg.ToString();
                subject += " - MODIFICATION REQUESTED";
            }
            else
            {
                msg = BuildMemoStatusEmail(memo);
                msg += Adversary.Utils.StringUtils.TWO_LINE_FEED_STRING + "You can login to the New Business Intake Website and view this memo at:" + Adversary.Utils.StringUtils.TWO_LINE_FEED_STRING + baseUrl + Adversary.Utils.StringUtils.TWO_LINE_FEED_STRING;
                if (isResubmit)
                {
                    subject += " - RESUBMITTED";
                }
                else if ((_Tracking.StatusTypeID == 3 ||
                          _Tracking.StatusTypeID == 100) &&
                         memo.ClientMatterNumbers.Count > 0)
                {
                    subject += " - CLIENT MATTER NUMBERS";
                    cmNumSent = true;
                }
                else if (_Tracking.StatusTypeID == 1)
                    msg = "Your screening memo has been submitted for approval." + Adversary.Utils.StringUtils.TWO_LINE_FEED_STRING + msg;
            }

            const string ERROR = "Could not send status email because one or more recipients were not valid.";
            MailAddressCollection acknowledgeRecipients = new MailAddressCollection();

            if (string.IsNullOrEmpty(memo.AttEntering) || string.IsNullOrEmpty(memo.PersonnelID))
                throw new Exception(ERROR);
            try
            {
                if (WebUtils.TestMode)
                {
                    acknowledgeRecipients.Add(new MailAddress(_AdvData.GetHREmployee(memo.AttEntering).CompanyEmailAddress, "AttEntering"));
                    acknowledgeRecipients.Add(new MailAddress(_AdvData.GetHREmployee(memo.PersonnelID).CompanyEmailAddress, "PersonnelID"));
                }
                else
                {
                    acknowledgeRecipients.Add(_AdvData.GetHREmployee(memo.AttEntering).CompanyEmailAddress);
                    acknowledgeRecipients.Add(_AdvData.GetHREmployee(memo.PersonnelID).CompanyEmailAddress);
                }
                if (!string.IsNullOrEmpty(memo.RespAttID))
                {
                    if (WebUtils.TestMode)
                    {
                        acknowledgeRecipients.Add(new MailAddress(_AdvData.GetHREmployee(memo.RespAttID).CompanyEmailAddress, "RespAttID"));
                    }
                    else
                    {
                        acknowledgeRecipients.Add(_AdvData.GetHREmployee(memo.RespAttID).CompanyEmailAddress);
                    }
                }

                if (cmNumSent && !string.IsNullOrEmpty(ConfigurationManager.AppSettings["EmailOnCMNumIssued"]))
                {
                    try
                    {
                        string[] recipients = ConfigurationManager.AppSettings["EmailOnCMNumIssued"].Trim().Split(new Char[] { ';' }, StringSplitOptions.None);
                        foreach (string recipient in recipients)
                            if (recipient != null && recipient.Trim() != string.Empty)
                            {
                                if (WebUtils.TestMode)
                                {
                                    acknowledgeRecipients.Add(new MailAddress(recipient, "EMailOnCMNumIssued"));
                                }
                                else
                                {
                                    acknowledgeRecipients.Add(recipient);
                                }
                            }
                    }
                    catch { }
                }
            }
            catch
            {
                throw new Exception(ERROR);
            }

            SendEmail(acknowledgeRecipients, subject, msg);
        }

        public string BuildMemoStatusEmail(Memo memo)
        {
            
            StringBuilder memoSummary = new StringBuilder();
            memoSummary.AppendLine();
            memoSummary.AppendLine("Screening Memo #:\t\t\t" + memo.ScrMemID);
            memoSummary.AppendLine();
            memoSummary.AppendLine("Memo Type:\t\t\t\t" + advData.GetMemoTypes().Where(M => M.TrackNumber.Equals(memo.ScrMemType)).First().MemoType1);
            memoSummary.AppendLine();
            if (memo.ScrMemType != 6)
            {
                memoSummary.AppendLine("Practice Code:\t\t\t" 
                    + advData.GetPracticeGroupsList().Where(P=>P.matt_cat_code.Equals(memo.PracCode)).First().matt_cat_desc
                    + " (" + memo.PracCode + ")");
                memoSummary.AppendLine();
            }
            memoSummary.AppendLine("Status:\t\t\t\t" + advData.GetTrackingStatusTypes().Where(
                T => T.StatusTypeID.Equals(memo.Trackings.First().StatusTypeID)).First().StatusMessage);
            memoSummary.AppendLine();
            if (!string.IsNullOrEmpty(memo.AttEntering))
            {
                memoSummary.AppendLine("Attorney Preparing Memo:\t" + advData.GetEmployee(memo.AttEntering).EmployeeName + " (" + memo.AttEntering + ")");
                memoSummary.AppendLine();
            }
            if (!string.IsNullOrEmpty(memo.PersonnelID))
            {
                memoSummary.AppendLine("Memo Input By:\t\t\t" + advData.GetEmployee(memo.PersonnelID).EmployeeName + " (" + memo.PersonnelID + ")");
                memoSummary.AppendLine();
            }
            if (!string.IsNullOrEmpty(memo.RespAttID))
            {
                memoSummary.AppendLine("Billing Attorney:\t\t" + advData.GetEmployee(memo.RespAttID).EmployeeName + " (" + memo.RespAttID + ")");
                memoSummary.AppendLine();
            }
            memoSummary.AppendLine();
            string clientName = GetClientDisplayName(memo);
            if (!string.IsNullOrEmpty(clientName))
            {
                memoSummary.AppendLine("Client:");
                memoSummary.AppendLine();
                memoSummary.AppendLine(clientName);
                memoSummary.AppendLine();
            }
            if (memo.ClientMatterNumbers.Count > 0)
            {
                memoSummary.AppendLine("Client Matter Numbers:");
                memoSummary.AppendLine();
                foreach (ClientMatterNumber cmNum in memo.ClientMatterNumbers)
                    memoSummary.AppendLine(cmNum.ClientMatterNumber1);
                memoSummary.AppendLine();
            }
            if (!string.IsNullOrEmpty(memo.MatterName))
            {
                memoSummary.AppendLine("Matter Name:");
                memoSummary.AppendLine();
                memoSummary.AppendLine(memo.MatterName);
                memoSummary.AppendLine();
            }
            if (!string.IsNullOrEmpty(memo.WorkDesc))
            {
                memoSummary.AppendLine("Work Description:");
                memoSummary.AppendLine();
                memoSummary.AppendLine(memo.WorkDesc);
                memoSummary.AppendLine();
            }


            return memoSummary.ToString();
        }

        public string BuildMemoApproverEmail(Memo memo)
        {
            StringBuilder msg = new StringBuilder();

            Tracking T = memo.Trackings.First();
            if (T == null) return "";

            if (!string.IsNullOrEmpty(T.APSignature))
            {
                msg.Append("AP Signature: " + T.APSignature);
                msg.AppendLine((T.APDate.HasValue) ? " (" + T.APDate.Value + ")" : string.Empty);
                msg.AppendLine();
            }
            if (!string.IsNullOrEmpty(T.PGMSignature))
            {
                msg.Append("PGL Signature: " + T.PGMSignature);
                msg.AppendLine((T.PGMDate.HasValue) ? " (" + T.PGMDate.Value + ")" : string.Empty);
                msg.AppendLine();
            }

            if (!string.IsNullOrEmpty(T.APARSignature))
                msg.AppendLine("AP Approval for A/R over 90 days: " + T.APARSignature + "\r\n");
            if (!string.IsNullOrEmpty(T.PGMARSignature))
                msg.AppendLine("PGM Approval for A/R over 90 days: " + T.PGMARSignature + "\r\n");
            if (!string.IsNullOrEmpty(T.APExceptionSignature))
                msg.AppendLine("AP Exception approval for signed engagement letter: " + T.APExceptionSignature + "\r\n");
            if (!string.IsNullOrEmpty(T.PGMExceptionSignature))
                msg.AppendLine("PGL Exception approval for signed engagement letter: " + T.PGMExceptionSignature + "\r\n");

            if (!string.IsNullOrEmpty(T.Opened))
                msg.AppendLine("Opened: " + T.Opened + "\r\n");
            if (!string.IsNullOrEmpty(T.Conflicts))
                msg.AppendLine("Conflicts: " + T.Conflicts + "\r\n");
            if (!string.IsNullOrEmpty(T.FeeSplits))
                msg.AppendLine("Fee Splits: " + T.FeeSplits + "\r\n");
            if (!string.IsNullOrEmpty(T.FinalCheck))
                msg.AppendLine("Final Check: " + T.FinalCheck + "\r\n");

            if (!string.IsNullOrEmpty(T.Notes))
            {
                msg.AppendLine("Notes:");
                msg.AppendLine();
                msg.AppendLine(T.Notes);
                msg.AppendLine();
                msg.AppendLine();
            }
            if (!string.IsNullOrEmpty(T.APNotes))
            {
                msg.AppendLine("Additional AP Notes:");
                msg.AppendLine();
                msg.AppendLine(T.APNotes);
                msg.AppendLine();
                msg.AppendLine();
            }
            if (!string.IsNullOrEmpty(T.PGMNotes))
            {
                msg.AppendLine("Additional PGL Notes:");
                msg.AppendLine();
                msg.AppendLine(T.PGMNotes);
                msg.AppendLine();
                msg.AppendLine();
            }

            return msg.ToString();
        }

        public string GetClientDisplayName(Memo memo)
        {
            try
            {
                string name = string.Empty;
                if (memo.ScrMemType == 2 ||
                    ((memo.ScrMemType == 3 || memo.ScrMemType == 6) && !string.IsNullOrEmpty(memo.ClientNumber)))
                    name = string.IsNullOrEmpty(memo.ClientNumber) ? 
                        string.Empty : 
                        advData.SearchClients(memo.ClientNumber).First().ClientName + " (" + memo.ClientNumber + ")";
                else
                {
                    if (memo.Company.HasValue && memo.Company.Value)
                        name = memo.NewClients.First().CName;
                    else
                        name = memo.NewClients.First().FName + " " + memo.NewClients.First().MName + " " + memo.NewClients.First().LName;
                }

                name = name.Trim();
                name = name.Replace("  ", " ");
                return (string.IsNullOrEmpty(name)) ? "<em>unavailable</em>" : name;
            }
            catch
            {
                return "<span style='color: red;'>Client Name Not Found</span>";
            }
        }

        //send email section
        public void SendEmail(MailAddressCollection mailTo, string subject, string msg)
        {
            SendEmail(mailTo, null, subject, msg, false, null);
        }
        public void SendEmail(MailAddressCollection mailTo, MailAddress from, string subject, string msg, bool isBodyHTML)
        {
            SendEmail(mailTo, from, subject, msg, false, null);
        }
        public void SendEmail(MailAddressCollection mailTo, MailAddress from, string subject, string msg, bool isBodyHTML, NetworkCredential credential)
        {
            EmailService.EmailClient email = new EmailService.EmailClient();
            List<string> recipients = new List<string>();
            foreach (MailAddress to in mailTo)
            {
                recipients.Add(to.Address);
            }

            if (from == null)
            {
                from = new MailAddress(ConfigurationManager.AppSettings["EmailSendAddress"], 
                    ConfigurationManager.AppSettings["EmailSendDisplayName"]);
            }

            if (WebUtils.TestMode)
            {
                if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["EmailTestAddresses"]))
                {
                    string[] testAddresses = ConfigurationManager.AppSettings["EmailTestAddresses"].Split(',');
                    if (testAddresses != null && testAddresses.Count() > 0)
                    {
                        msg = "***TEST MODE***\r\n" +
                            string.Join("\r\n", (mailTo.Select(m => m.DisplayName + " (" + m.Address + ")").ToArray())) +
                            "\r\n" +
                            "***************\r\n" +
                            msg;

                        email.SendMail(from.Address, from.DisplayName, testAddresses, null, null, subject, msg, isBodyHTML, false);
                    }
                }
            }
            else
            {
                // email.SendMail(from.Address, from.DisplayName, recipients.ToArray(), null, null, subject, msg, isBodyHTML, false);
            }
        }
    }
}
