﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BaseMaster.Master" AutoEventWireup="true"
    CodeBehind="Login.aspx.cs" Inherits="Adversary.Web.Login" Theme="HH" %>

<asp:Content ID="cntMain" ContentPlaceHolderID="cphMainContent" runat="server">
    <asp:Panel ID="pnlLogin" runat="server" class="sidebar" DefaultButton="login$LoginButton">
        <div class="menu">
            <h4>
                Holland &amp; Hart New Business Intake Website</h4>
            <asp:Login ID="login" runat="server" OnLoggingIn="login_LoggingIn" />
        </div>
    </asp:Panel>
</asp:Content>
