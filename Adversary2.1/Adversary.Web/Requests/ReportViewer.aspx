﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Requests/Requests.Master" AutoEventWireup="true" CodeBehind="ReportViewer.aspx.cs" Inherits="Adversary.Web.Requests.ReportViewer" Theme="HH" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="cntHead" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="cntHeader" ContentPlaceHolderID="cphHeader" runat="server">
</asp:Content>
<asp:Content ID="cntMain" ContentPlaceHolderID="cphMainContent" runat="server">
    <asp:DropDownList ID="ddlReports" runat="server" OnInit="ddlReports_Init" AutoPostBack="true" OnSelectedIndexChanged="ddlReports_SelectedIndexChanged" />
    <hr />
    <asp:PlaceHolder ID="plcReportControls" runat="server"></asp:PlaceHolder>
    <rsweb:ReportViewer ID="rvwMain" runat="server" OnInit="rvwMain_Init" Width="100%"></rsweb:ReportViewer>
</asp:Content>
<asp:Content ID="cntLower" ContentPlaceHolderID="cphLowerContent" runat="server">
</asp:Content>
