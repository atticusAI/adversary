﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Adversary.Web.Requests
{
    public partial class Parties : Pages.RequestBasePage
    {
        private Adversary.DataLayer.Requests.PartyInformation _PartyInfo;

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (this.Page.Master is RequestsMasterPage)
            {
                ((RequestsMasterPage)this.Page.Master).TabPageUrl = "/Requests/Default.aspx";
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (ThisRequest == null)
            {
                this.pnlError.Visible = true;
                this.pnlRequest.Visible = false;
                return;
            }
            _PartyInfo = new DataLayer.Requests.PartyInformation(ThisRequest.ReqID);
            _PartyInfo.Get();
        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            tbAdverseParties.Text = _PartyInfo.AdverseParties;
            tbAffiliatesOfAdverseParties.Text = _PartyInfo.AffiliatesOfAdverseParties;
            tbOpposingCounsel.Text = _PartyInfo.OpposingCounsel;
            tbOtherClientAffiliates.Text = _PartyInfo.OtherClientAffiliates;
            tbPartiesToMediationArbitration.Text = _PartyInfo.PartiesToMediationAndArbitration;
            tbRelatedCoDefendants.Text = _PartyInfo.RelatedCoDefendants;
            tbRelatedPartiesNotCodefendants.Text = _PartyInfo.RelatedPartiesNotCoDefendants;
        }
        public override void Validate()
        {
            base.Validate();
            _PartyInfo.AdverseParties = tbAdverseParties.Text;
            _PartyInfo.AffiliatesOfAdverseParties = tbAffiliatesOfAdverseParties.Text;
            _PartyInfo.OtherClientAffiliates = tbOtherClientAffiliates.Text;
            _PartyInfo.OpposingCounsel = tbOpposingCounsel.Text;
            _PartyInfo.PartiesToMediationAndArbitration = tbPartiesToMediationArbitration.Text;
            _PartyInfo.RelatedCoDefendants = tbRelatedCoDefendants.Text;
            _PartyInfo.RelatedPartiesNotCoDefendants = tbRelatedPartiesNotCodefendants.Text;
            _PartyInfo.Save();

        }
    }
}