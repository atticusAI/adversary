﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using Adversary.Utils;

namespace Adversary.Web.Requests
{
    public partial class BasicInfo : Pages.RequestBasePage
    {
        Adversary.DataLayer.AdversaryData advData;

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (this.Page.Master is RequestsMasterPage)
            {
                ((RequestsMasterPage)this.Page.Master).TabPageUrl = "/Requests/Default.aspx";
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            rblClientStatusCategory.HH_AddAccordionPaneJavascript(accClientMatterNumber.ClientID);
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (HasRequestError)
            { thisMasterPage.HideScreen(); return; }
           

            if (!Page.IsPostBack)
            {
                lblRequestID.Text = ThisRequest.ReqID.ToString();
                hidRequestID.Value = ThisRequest.ReqID.ToString();

                if (ThisRequest.SubmittalDate.HasValue)
                {
                    lblSubmittedOn.Text = String.Format("{0:d}",
                        ((DateTime)ThisRequest.SubmittalDate.Value));
                }
                else
                {
                    lblSubmittedOn.Text = "pending";
                }

                if (ThisRequest.DeadlineDate.HasValue)
                {
                    lblDeadlineDate.Text = String.Format("{0:G}",
                            ((DateTime)ThisRequest.DeadlineDate.Value));
                }
                else
                {
                    lblDeadlineDate.Text = "pending";
                }

                if (!String.IsNullOrEmpty(ThisRequest.ClientCategory))
                {
                    //ddlClientStatusCategory.Items.FindByText(ThisRequest.ClientCategory).Selected = true;
                    ListItem itm = rblClientStatusCategory.Items.FindByText(ThisRequest.ClientCategory);
                    if (itm != null)
                    {
                        itm.Selected = true;
                    }
                }

                tbClientMatterNumber.Text = ThisRequest.ClientMatter;
                tbClientName.Text = ThisRequest.ClientName;

                //get the login from the currently logged on user
                if (String.IsNullOrEmpty(ThisRequest.TypistLogin))
                {
                    ucSubmittedPayrollID.SelectedEmployeeCode = base.CurrentUser.CurrentUserPayrollID;
                }
                else
                {
                    ucSubmittedPayrollID.SelectedEmployeeCode = ThisRequest.TypistLogin;
                }

                if (!String.IsNullOrEmpty(ThisRequest.AttyLogin))
                {
                    ucAttorneyPayrollID.SelectedEmployeeCode = ThisRequest.AttyLogin;
                }

                if (ThisRequest.ClientCategory != null)
                {
                    if (ThisRequest.ClientCategory.Contains("Potential"))
                        accClientMatterNumber.SelectedIndex = 0;
                    else if (ThisRequest.ClientCategory.Contains("Existing"))
                        accClientMatterNumber.SelectedIndex = 1;
                    else
                        accClientMatterNumber.SelectedIndex = 0;
                }

                ucPracticeCodeDropdown.SelectedPracticeCode = ThisRequest.PracticeCode;
                cbConfidential.Checked = (ThisRequest.OKforNewMatter ?? false);
            }
            
        }

        

        public override void Validate()
        {
            base.Validate();
            DateTime nowDate = DateTime.Now;
            if (ThisRequest != null)
            {
                if (ThisRequest.ReqID > 0)
                {
                    Adversary.DataLayer.Requests.BasicInformation basicInfo =
                        new DataLayer.Requests.BasicInformation(Convert.ToInt32(Request.QueryString["requestid"].ToString()));
                    basicInfo.PracticeCode = ucPracticeCodeDropdown.ddlCodes.SelectedValue;
                    basicInfo.OKForNewMatter = cbConfidential.Checked;
                    basicInfo.SubmittedByPayrollID = ((HiddenField)ucSubmittedPayrollID.FindControl("hidEmployeeCode")).Value;
                    
                    //if the request already has a submitted date, then send it
                    if (ThisRequest.SubmittalDate.HasValue)
                        basicInfo.SubmittedOn = ThisRequest.SubmittalDate;

                    basicInfo.DeadlineDate = Adversary.Requests.RequestUtils.ComputeDeadlineDate(nowDate);
                    basicInfo.ClientName = tbClientName.Text;
                    basicInfo.ClientMatterNumber = tbClientMatterNumber.Text;
                    basicInfo.ClientCategory = rblClientStatusCategory.SelectedValue;
                    basicInfo.AttorneyID = ((HiddenField)ucAttorneyPayrollID.FindControl("hidEmployeeCode")).Value;
                    basicInfo.Save();
                }
            }
        }

        
    }
}