﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using Adversary.Utils;
using Microsoft.Reporting.WebForms;
using Adversary.Web.Controls;

namespace Adversary.Web.Requests
{
    public partial class ReportViewer : Pages.RequestBasePage
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (this.Page.Master is RequestsMasterPage)
            {
                ((RequestsMasterPage)this.Page.Master).TabPageUrl = "/Requests/Default.aspx";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        private ReportControl ReportControl
        {
            get
            {
                ReportControl ctrl = LoadControl(this.ControlPath) as ReportControl;
                if (ctrl != null)
                {
                    ctrl.ID = "ctl" + this.ddlReports.SelectedItem.Text.Replace(" ", "");
                    ctrl.ReportViewer = this.rvwMain;
                    ctrl.ReportPath = this.ReportPath;
                }
                else
                {
                    throw new NullReferenceException();
                }
                return ctrl;
            }
        }

        private string ControlPath
        {
            get
            {
                string s = ReportUtils.GetReportControlsFile(this.ddlReports.SelectedValue);
                return Server.UnMapPath(s);
            }
        }

        private string ReportPath
        {
            get
            {
                return this.ddlReports.SelectedValue;
            }
        }
        
        protected void ddlReports_Init(object sender, EventArgs e)
        {
            string appPath = Server.MapPath("~/") + @"Requests\Reports";
            this.ddlReports.Items.AddRange(ReportUtils.GetReportsList(appPath));
        }

        protected void ddlReports_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.InitReport();
        }

        protected void rvwMain_Init(object sender, EventArgs e)
        {
            this.InitReport();
        }

        private void InitReport()
        {
            this.plcReportControls.Controls.Clear();
            this.rvwMain.Reset();
            this.plcReportControls.Controls.Add(this.ReportControl);
        }
    }
}