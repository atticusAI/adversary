﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Runtime.Serialization;
using System.Web.Script.Serialization;
using AjaxControlToolkit;
using System.Web.Services;
using System.Web.Script.Services;
using Adversary.Web.Pages;
using Adversary.DataLayer;
using System.Reflection;


namespace Adversary.Web.Requests
{
    public partial class RequestsMasterPage : MasterBase, IMemoTrack
    {

        private int _CurrentLocation;
        public int CurrentLocation
        {
            set { _CurrentLocation = value; }
        }

        private int _CurrentTrack;
        public int CurrentTrack
        {
            set { _CurrentTrack = value; }
        }

        private string _CurrentPageFileName;
        public string CurrentPageFileName
        {
            set { _CurrentPageFileName = value; }
        }

        private bool _ShowNavigationButtons = true;
        public bool ShowNavigationButtons
        {
            get { return _ShowNavigationButtons; }
            set { _ShowNavigationButtons = value; }
        }

        private int _RequestID;
        public int RequestID
        {
            get { return _RequestID; }
            set { _RequestID = value; }
        }

        private bool _AllowNavigation = true;

        public bool AllowNavigation
        {
            get { return _AllowNavigation; }
            set { _AllowNavigation = value; }
        }

        public string TabPageUrl
        {
            get
            {
                return this.tabNav.PageUrl;
            }
            set
            {
                this.tabNav.PageUrl = value;
            }
        }

        public void SetErrorSummaryVisibility(bool isVisible)
        {
            master_ErrorSummary.Visible = isVisible;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Adversary.DataLayer.MemoNavigation memoNav = new MemoNavigation(_CurrentTrack, _CurrentPageFileName);
            List<MemoApplicationTrack> allTracks = memoNav.GetAllApplicationTracks(this._CurrentTrack);
            rptToolbar.DataSource = allTracks;
            rptToolbar.DataBind();

            string nextURL = memoNav.NextSectionUrl;
            string previousURL = memoNav.PreviousSectionUrl;

            btnNext.Command += new CommandEventHandler(btnNext_Command);
            btnPrevious.Command += new CommandEventHandler(btnPrevious_Command);

            if (string.IsNullOrWhiteSpace(nextURL))
            {
                btnNext.Visible = false;
            }
            else
            {
                btnNext.CommandName = "Next";
                btnNext.CommandArgument = nextURL;
                // btnNext.Text = nextURL;
                btnNext.Text = "Next";
                btnNext.CausesValidation = true;
                btnNext.ValidationGroup = "valGroup_Screen";
            }

            if (string.IsNullOrWhiteSpace(previousURL))
            {
                btnPrevious.Visible = false;
            }
            else
            {
                btnPrevious.CommandName = "Previous";
                btnPrevious.CommandArgument = previousURL;
                // btnPrevious.Text = previousURL;
                btnPrevious.Text = "Previous";
                btnPrevious.CausesValidation = true;
                btnPrevious.ValidationGroup = "valGroup_Screen";
            }

            pnlPreviousNext.Visible = _ShowNavigationButtons;
            this.AddIncludes(this.head1);
            // this.HighlightCurrentTab(this.tdTabs);
        }

        protected void btnPrevious_Command(object sender, CommandEventArgs e)
        {
            (cphMainContent.Page as RequestBasePage).Validate();
            if(AllowNavigation) GotoPreviousPage(e.CommandArgument.ToString());
        }

        private void GotoPreviousPage(string prevPage)
        {
            //(cphMainContent.Page as RequestBasePage).Validate();
            if (this.Page.IsValid && this.AllowNavigation)
            {
                Response.Redirect(String.Format("{0}?requestid={1}", prevPage, RequestID));
            }
        }

        protected void btnNext_Command(object sender, CommandEventArgs e)
        {
            (cphMainContent.Page as RequestBasePage).Validate();
            if(AllowNavigation) GotoNextPage(e.CommandArgument.ToString());
        }

        private void GotoNextPage(string nextPage)
        {
            if (this.Page.IsValid && AllowNavigation)
            {
                Response.Redirect(String.Format("{0}?requestid={1}", nextPage, RequestID));
            }
        }

        protected void rptToolbar_ItemCreated(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton l = (LinkButton)e.Item.FindControl("lbNavigate");
                l.Command += new CommandEventHandler(l_Command);
            }
        }
        protected void l_Command(object sender, CommandEventArgs e)
        {
            cphMainContent.Page.Validate();
            if(AllowNavigation) Response.Redirect(e.CommandArgument.ToString());
        }

        protected void rptToolbar_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            MemoApplicationTrack a = (MemoApplicationTrack)e.Item.DataItem;
            LinkButton l = (LinkButton)e.Item.FindControl("lbNavigate");
            if (l != null && a != null)
            {
                l.CommandArgument = a.PageURL + "?requestid=" + _RequestID.ToString();
                // l.PostBackUrl = a.PageURL;
                l.Text = a.Name;
                l.ToolTip = a.Description;
                l.CausesValidation = true;

                if (a.PageURL.ToLowerInvariant().Contains(_CurrentPageFileName.ToLowerInvariant()))
                {
                    l.CssClass += " activesubtab";
                }
                if ((DisableTrackButtons || this._RequestID <= 0 ) &&
                    !a.PageURL.ToLower().Contains("default") &&
                    !a.PageURL.ToLower().Contains("viewrequests")) l.Enabled = false;
            }
        }

        protected void lnkNew_Click(object sender, EventArgs e)
        {
        }

        public void SetErrorMessage(string errorMessage)
        {
            SetErrorMessage(errorMessage, Web.Controls.MessageDisplayType.DisplayDefault, null);
        }

        public void SetErrorMessage(string errorMessage, Controls.MessageDisplayType messageDisplayType)
        {
            SetErrorMessage(errorMessage, messageDisplayType, null);
        }

        public void SetErrorMessage(string errorMessage, Controls.MessageDisplayType messageDisplayType, List<string> errors)
        {
            master_ErrorSummary.DocumentToValidate = "Adversary Request";
            master_ErrorSummary.DisplayType = messageDisplayType;
            master_ErrorSummary.DisplayMessage = errorMessage;
            if (errors != null) master_ErrorSummary.ErrorList = errors;
        }

        public bool DisableTrackButtons { get; set; }

        public void HideScreen()
        {
            pnlMainContent.Visible = false;
        }

        protected void tabNav_PreRender(object sender, EventArgs e)
        {
            if (this.Page is TrackBasePage)
            {
                this.tabNav.CurrentUser = ((TrackBasePage)this.Page).CurrentUser;
            }
        }
    }
}