﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.Requests;
using Adversary.DataLayer;

namespace Adversary.Web.Requests
{
    public partial class Submit : Pages.RequestBasePage
    {
        Adversary.Requests.RequestRuleEngine ruleEngine;

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            ruleEngine = new RequestRuleEngine();
            if (this.Page.Master is RequestsMasterPage)
            {
                ((RequestsMasterPage)this.Page.Master).TabPageUrl = "/Requests/Default.aspx";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ucRequestSummary.Request = ThisRequest;
            ucRequestSummary.TableSkinID = "RequestSummary";
            ucRequestSummary.AdversaryGroupView = (CurrentUser.IsAdversaryAdmin || CurrentUser.IsAdversary);

            //set the admin tools to invisible at first, and reset to visible if the right conditions are met
            pnlAdminTools.Visible = false;

            //if this request has not been submitted, run rules
            if (!ThisRequest.HH_IsRequestSubmitted())
            {
                ruleEngine.Request = ThisRequest;
                if (ruleEngine.CanSubmit())
                {
                    thisMasterPage.SetErrorMessage(RequestMessages.REQ_CAN_BE_SUBMITTED_MSG, Web.Controls.MessageDisplayType.DisplayValid);
                }
                else
                {
                    thisMasterPage.SetErrorMessage(RequestMessages.REQ_VALIDATION_FAILED_MSG, Web.Controls.MessageDisplayType.DisplayError,
                        ruleEngine.ErrorMessages);
                }
            }
            else //this request was submitted and is now being edited
            {
                thisMasterPage.SetErrorMessage(RequestMessages.REQ_SUBMITTED_MSG, Web.Controls.MessageDisplayType.DisplayValid);
                btnSubmitRequest.Enabled = false;
                if (CurrentUser.IsAdversary || CurrentUser.IsAdversaryAdmin)
                {
                    pnlAdminTools.Visible = true;
                }
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            lblWorkBegunDate.Text = (ThisRequest.WorkBegunDate.HasValue ? ThisRequest.WorkBegunDate.Value.ToShortDateString() : "");
            if (!ThisRequest.HH_IsRequestComplete()) btnRemoveComplete.Enabled = false;
            advAssignedTo.SelectedMember = ThisRequest.WorkBegunBy;
            advWorkCompletedBy.SelectedMember = ThisRequest.WorkCompletedBy;
        }

        protected void btnSubmitRequest_Click(object sender, EventArgs e)
        {
            ruleEngine.Request = ThisRequest;
            if (ruleEngine.CanSubmit())
            {
                Adversary.Requests.RequestEmail reqEmail = new RequestEmail();
                Adversary.DataLayer.Requests.BasicInformation basicInfo = new DataLayer.Requests.BasicInformation(ThisRequest.ReqID);
                basicInfo.SubmittedOn = DateTime.Now;
                basicInfo.SubmitRequest();
                LoadRequest();
                reqEmail.SendAdversaryRequestEmail(ThisRequest);
            }
            else
            {
                btnSubmitRequest.Enabled = false;
            }
            
            
            //if the work has begun show an updated message. If not, show a submitted message
            if(String.IsNullOrEmpty(ThisRequest.WorkBegunBy))
            {
                //thisMasterPage.SetErrorMessage("Your adversary request has been updated.",Web.Controls.MessageDisplayType.Info);
                //lblMessage.Text = "Your adversary request has been updated";
                //lblMessage.ForeColor = System.Drawing.Color.Navy;
                ucErrorSummary.DisplayMessage = "Your adversary request has been updated";
                ucErrorSummary.DisplayType = Adversary.Web.Controls.MessageDisplayType.DisplayInfo;
            }
            else
            {
                //thisMasterPage.SetErrorMessage("Your adversary request has been submitted.", Web.Controls.MessageDisplayType.DisplayValid);
                //lblMessage.Text = "Your adversary request has been submitted";
                //lblMessage.ForeColor = System.Drawing.Color.Green;
                ucErrorSummary.DisplayMessage = "Your adversary request has been submitted";
                ucErrorSummary.DisplayType = Adversary.Web.Controls.MessageDisplayType.DisplayValid;
            }
            
            btnSubmitRequest.Enabled = false;
            upnlMessage.Update();

        }

        protected void btnAdmin_Click(object sender, EventArgs e)
        {
            Adversary.DataLayer.Requests.RequestAssignments reqAssignments = 
                new DataLayer.Requests.RequestAssignments(ThisRequest.ReqID);

            //parse out the items from the dropdowns
            Button btn = (Button)sender;

            switch (btn.CommandName)
            {
                case "markcomplete":
                    if (advWorkCompletedBy.ddlAdversaryGroup.SelectedIndex <= 0) 
                    {
                        ucErrorSummary.DisplayMessage = "Please select a user to complete this request.";
                        ucErrorSummary.DisplayType = Adversary.Web.Controls.MessageDisplayType.DisplayError;
                        upnlMessage.Update();
                        return;
                    }
                    if (advAssignedTo.ddlAdversaryGroup.SelectedIndex <= 0)
                    {
                        ucErrorSummary.DisplayMessage = "Please select a user to assign this request.";
                        ucErrorSummary.DisplayType = Adversary.Web.Controls.MessageDisplayType.DisplayError;
                        upnlMessage.Update();
                        return;
                    }
                    reqAssignments.WorkCompletedBy = advWorkCompletedBy.ddlAdversaryGroup.SelectedValue;
                    reqAssignments.CompletionDate = DateTime.Now;
                    if (!btnRemoveComplete.Enabled) btnRemoveComplete.Enabled = true;
                    break;
                case "saveassignments":
                    if (advAssignedTo.ddlAdversaryGroup.SelectedIndex <= 0)
                    {
                        ucErrorSummary.DisplayMessage = "Please select a user to assign this request.";
                        ucErrorSummary.DisplayType = Adversary.Web.Controls.MessageDisplayType.DisplayError;
                        upnlMessage.Update();
                        return;
                    }
                    reqAssignments.WorkBegunBy = advAssignedTo.ddlAdversaryGroup.SelectedValue;
                    break;
                case "removecomplete":
                    ThisRequest.HH_RemoveCompleted();
                    break;
            }

            if(advAssignedTo.ddlAdversaryGroup.SelectedIndex > 0)
                reqAssignments.WorkBegunBy = advAssignedTo.ddlAdversaryGroup.SelectedValue;

            if (ThisRequest.WorkBegunDate.HasValue)
                reqAssignments.WorkBegunDate = ThisRequest.WorkBegunDate.Value;
            else
                reqAssignments.WorkBegunDate = DateTime.Now;

            reqAssignments.Save();
            //lblMessage.Text = "This adversary request has been assigned to " + reqAssignments.WorkBegunBy;
            ucErrorSummary.DisplayMessage = "This adversary request has been assigned to " + reqAssignments.WorkBegunBy;
            ucErrorSummary.DisplayType = Adversary.Web.Controls.MessageDisplayType.DisplayInfo;
            if (btn.CommandName == "markcomplete")
            {
                LoadRequest();
                Adversary.Requests.RequestEmail reqEmail = new RequestEmail();
                reqEmail.SendAdversaryRequestEmail(ThisRequest);
            }
            upnlMessage.Update();
        }

        protected void btnClick_ShowPopup(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            switch (btn.CommandArgument)
            {
                case "markcomplete":
                    if (advWorkCompletedBy.ddlAdversaryGroup.SelectedIndex <= 0) 
                    {
                        ucErrorSummary.DisplayMessage = "Please select a user to complete this request.";
                        ucErrorSummary.DisplayType = Adversary.Web.Controls.MessageDisplayType.DisplayError;
                        upnlMessage.Update();
                        return;
                    }
                    if (advAssignedTo.ddlAdversaryGroup.SelectedIndex <= 0)
                    {
                        ucErrorSummary.DisplayMessage = "Please select a user to assign this request.";
                        ucErrorSummary.DisplayType = Adversary.Web.Controls.MessageDisplayType.DisplayError;
                        upnlMessage.Update();
                        return;
                    }
                    //put logic here to make sure someone was selected from the completed by control
                    //passing the logic, it should display the text inside the label.
                    string markcompletemessage = String.Format("This request will be marked as completed by {0}. Please " +
                        "confirm to continue and remove this request from the queue.", advWorkCompletedBy.ddlAdversaryGroup.SelectedValue);
                    lblMarkComplete.Text = markcompletemessage;
                    mdlMarkCompletePopup.Show();
                    break;
                case "saveassignments":
                   
                    break;
                case "removecomplete":
                    mdlRemoveCompletePopup.Show();
                    break;
                default:
                    break;
            }
        }

        protected void btnCancelPopup_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            switch (btn.CommandArgument)
            {
                case "markcomplete":
                    mdlMarkCompletePopup.Hide();
                    break;
                case "removecomplete":
                    mdlRemoveCompletePopup.Hide();
                    break;
                default: break;
            }
        }
        
    }
}