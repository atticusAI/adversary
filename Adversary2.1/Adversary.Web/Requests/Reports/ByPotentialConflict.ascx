﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ByPotentialConflict.ascx.cs" Inherits="Adversary.Web.Requests.Reports.ByPotentialConflict" %>
<%@ Register Src="~/Controls/MultiSelectExtender.ascx" TagPrefix="uc1" TagName="MultiSelectExtender" %>

<script src="../Scripts/jquery.multiselect.min.js"></script>
<asp:ListBox ID="lb" runat="server" OnInit="lb_Init" OnSelectedIndexChanged="lb_SelectedIndexChanged" SelectionMode="Multiple" />
<uc1:MultiSelectExtender runat="server" ID="msx" AutoPostBack="true" TargetControlID="lb" OnClientClosed="alert(closed)" />