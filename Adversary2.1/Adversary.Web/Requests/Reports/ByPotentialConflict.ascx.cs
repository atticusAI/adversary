﻿using Adversary.DataLayer;
using Adversary.DataLayer.Requests;
using Adversary.Utils;
using Adversary.Web.Controls;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Adversary.Web.Requests.Reports
{
    public partial class ByPotentialConflict : ReportControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (!this.Page.IsPostBack)
            {
                this.InitReport();
            }
        }

        protected void lb_Init(object sender, EventArgs e)
        {
            this.lb.DataSource = new ConflictCollection(MemoType.AdversaryRequest);
            this.lb.DataTextField = "Description";
            this.lb.DataValueField = "ConflictID";
            this.lb.DataBind();
        }

        protected void lb_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.InitReport();
        }

        private void InitReport()
        {
            this.ReportViewer.Reset();
            this.ReportViewer.LocalReport.Dispose();
            this.ReportViewer.LocalReport.ReportPath = this.ReportPath;
            ReportDataSource rds = new ReportDataSource("Requests", this.DataLayer.SearchRequests(this.SelectedConflicts));
            this.ReportViewer.LocalReport.DataSources.Add(rds);
        }

        private List<int> SelectedConflicts
        {
            get
            {
                var cons = from ListItem i in this.lb.Items
                           where i.Selected == true
                           select Convert.ToInt32(i.Value);
                return cons.ToList();
            }
        }

        private DataLayer.Requests.Search _dataLayer = null;

        private DataLayer.Requests.Search DataLayer
        {
            get
            {
                if (this._dataLayer == null)
                {
                    this._dataLayer = new DataLayer.Requests.Search();
                }
                return this._dataLayer;
            }
        }
    }
}
