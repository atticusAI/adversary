﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.Web.Extensions;
using Adversary.Controls;

namespace Adversary.Web.Requests
{
    public partial class ViewRequests : Pages.RequestBasePage
    {
        private DateTime _minDate = DateTime.Now.AddDays(-7);
        private bool _includeCompleted = false;
        private bool _includeUnsubmitted = false;
        private bool _myRequests = false;

        /// <summary>
        /// unconditional page init
        /// </summary>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            thisMasterPage.ShowNavigationButtons = false;
            thisMasterPage.SetErrorSummaryVisibility(false);
            thisMasterPage.TabPageUrl = "/Requests/Default.aspx";
        }

        /// <summary>
        /// show/hide the "my requests" checkbox based on permissions
        /// setup first visit state
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                tbSubmittedAfter.Text = DateTime.Now.AddDays(-7).ToShortDateString();
                BindGrid();
            }

            if (!CurrentUser.IsAdversary || !CurrentUser.IsAdversaryAdmin)
            {
                this._myRequests = true;
                cbMyRequests.Visible = false;
            }
        }
        
        /// <summary>
        /// show/hide data row controls based on row data and permissions
        /// </summary>
        protected void gvRequests_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.DataRow) return;

            Adversary.DataLayer.Request request = (Adversary.DataLayer.Request)e.Row.DataItem;

            bool isAdversary = CurrentUser.IsAdversary;
            bool isAdversaryAdmin = CurrentUser.IsAdversaryAdmin;

            LinkButton lbEdit = (LinkButton)e.Row.FindControl("lbEdit");
            if (lbEdit != null)
            {
                if ((isAdversary || isAdversaryAdmin) && !request.CompletionDate.HasValue)
                {
                    lbEdit.Visible = true;
                }
                else
                {
                    lbEdit.Visible = false;
                }
            }

            LinkButton lbAssign = (LinkButton)e.Row.FindControl("lbAssign");
            if (lbAssign != null)
            {
                if ((isAdversary || isAdversaryAdmin) && !String.IsNullOrEmpty(request.WorkBegunBy))
                {
                    lbAssign.Visible = true;
                }
                else
                {
                    lbAssign.Visible = false;
                }
            }

            AdversaryGroupDropdown ddlAssign = (AdversaryGroupDropdown)e.Row.FindControl("ddlAssign");
            Label lblAssigned = (Label)e.Row.FindControl("lblAssigned");

            if (ddlAssign != null && lblAssigned != null)
            {
                if ((isAdversary || isAdversaryAdmin) && String.IsNullOrEmpty(request.WorkBegunBy))
                {
                    ddlAssign.Visible = true;
                    lblAssigned.Visible = false;
                }
                else
                {
                    ddlAssign.Visible = false;
                    lblAssigned.Visible = true;
                }
            }
        }

        /// <summary>
        /// save assignment 
        /// </summary>
        protected void advDropDown_AdversaryMemberSelected(object sender, Adversary.Controls.AdversaryMemberSelectedEventArgs e)
        {
            int requestID = Convert.ToInt32(((AdversaryGroupDropdown)sender).CommandArgument);
            string selectedAdversaryMember = e.SelectedMember;
            DateTime workBegunOn = e.WorkBegunOn;
            Adversary.DataLayer.Requests.BasicInformation basicInfo = new DataLayer.Requests.BasicInformation(requestID);
            basicInfo.BeginWorkOnRequest(selectedAdversaryMember, workBegunOn);
            
            BindGrid();
        }

        /// <summary>
        /// handle grid view link button clicks
        /// </summary>
        protected void lb_Click(object sender, EventArgs e)
        {
            LinkButton lb = sender as LinkButton;
            int requestID = Convert.ToInt32(lb.CommandArgument);
            switch (lb.CommandName)
            {
                case "Summary":
                    Response.Redirect(String.Format("summary.aspx?requestid={0}", requestID.ToString()));
                    break;
                case "EditReq":
                    Response.Redirect(String.Format("basicinfo.aspx?requestid={0}", requestID.ToString()));
                    break;
                case "AssignReq":
                    Adversary.DataLayer.Requests.BasicInformation basic = new DataLayer.Requests.BasicInformation(requestID);
                    RequestID = requestID;
                    LoadRequest();
                    basic.BeginWorkOnRequest(base.CurrentUser.CurrentUserLogin, (ThisRequest.WorkBegunDate ?? DateTime.Now));
                    BindGrid();
                    break;
                default: break;
            }
        }

        /// <summary>
        /// filter the gridview when the go button is clicked
        /// </summary>
        protected void btnFilterRequests_Click(object sender, EventArgs e)
        {
            BindGrid();
        }

        /// <summary>
        /// slurp data 
        /// </summary>
        private void BindGrid()
        {
            SetParameters();
            List<Adversary.DataLayer.Request> usersRequests = new List<DataLayer.Request>();
            if (this._myRequests)
                usersRequests = AdversaryDataLayer.GetRequests(this._minDate, this._includeCompleted, this._includeUnsubmitted, CurrentUser.CurrentUserPayrollID);
            else
                usersRequests = AdversaryDataLayer.GetRequests(this._minDate, this._includeCompleted, this._includeUnsubmitted, null);

            gvRequests.DataSource = usersRequests;
            gvRequests.DataBind();
        }

        /// <summary>
        /// inspect page controls and set filter params 
        /// </summary>
        private void SetParameters()
        {
            this.Page.Validate();
            if (this.Page.IsValid)
            {
                this._minDate = Convert.ToDateTime(tbSubmittedAfter.Text);
                this._myRequests = ((CurrentUser.IsAdversary || CurrentUser.IsAdversaryAdmin) ? cbMyRequests.Checked : true);
                this._includeUnsubmitted = (ddlShowRequests.SelectedValue == "unsubmitted");
                this._includeCompleted = (ddlShowRequests.SelectedValue == "pendingcompleted") ? true : false;
            }
        }

        /// <summary>
        /// validate the page input
        /// </summary>
        protected void valPage_ServerValidate(object sender, ServerValidateEventArgs e)
        {
            ClearErrors();

            if (!tbSubmittedAfter.Text.HH_IsValidDate())
            {
                tbSubmittedAfter.HH_SetError(divErrs);
                e.IsValid = false;
            }
        }

        /// <summary>
        /// clear errors 
        /// </summary>
        private void ClearErrors()
        {
            tbSubmittedAfter.HH_ClearError();
            divErrs.HH_ClearError();
        }

        /// <summary>
        /// handle page index change events
        /// rebind the grid
        /// </summary>
        protected void gvRequests_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvRequests.PageIndex = e.NewPageIndex;
            BindGrid();
        }
    }
}
