﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Requests/Requests.Master" AutoEventWireup="true"
    CodeBehind="default.aspx.cs" Inherits="Adversary.Web.Requests._default" Theme="HH" %>

<asp:Content ID="Content3" ContentPlaceHolderID="cphMainContent" runat="server">
    <div class="sidebar">
        <div class="menu">
            <h4>
                Adversary Requests</h4>
            <dl>
                <dt>
                    <asp:LinkButton ID="lbCreateAdversaryRequest" runat="server" Text="Create Adversary Request"
                        OnClick="lbCreateAdversaryRequest_OnClick" /><br />
                </dt>
                <dt>
                    <asp:LinkButton ID="lblSelectExisting" runat="server" Text="Select an Existing Adversary Request"
                        OnClick="lbSelectExistingRequest_Click" />
                </dt>
            </dl>
        </div>
    </div>
</asp:Content>
