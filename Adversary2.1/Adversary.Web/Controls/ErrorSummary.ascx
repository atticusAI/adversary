﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ErrorSummary.ascx.cs" Inherits="Adversary.Web.Controls.ErrorSummary" %>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td height="35" valign="middle" align="center" width="35">
            <asp:Image ID="imgIcon" runat="server" Width="30" Height="30" />
        </td>
        <td valign="middle" align="left">
            <asp:Label ID="lblErrorSummaryMessage" runat="server" />
        </td>
    </tr>
    <tr>
        <td colspan="2" align="left">
            <asp:Repeater runat="server" ID="rptErrors">
                <HeaderTemplate>
                    <ul>
                </HeaderTemplate>
                <ItemTemplate>
                    <li><font color="red"><strong><%#Container.DataItem %></li></font></strong>
                </ItemTemplate>
                <FooterTemplate>
                    </ul>
                </FooterTemplate>
            </asp:Repeater>
        </td>
    </tr>
</table>

