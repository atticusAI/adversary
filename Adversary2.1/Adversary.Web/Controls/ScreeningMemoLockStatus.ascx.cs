﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Adversary.Web.Controls
{
    public partial class ScreeningMemoLockStatus : System.Web.UI.UserControl
    {
        public string ScrMemID
        {
            get;
            set;
        }

        public string LoginName
        {
            get
            {
                return HttpContext.Current.User.Identity.Name;
            }
        }

        protected void imgLock_PreRender(object sender, EventArgs e)
        {
            this.imgLock.Visible = false;
            if (!string.IsNullOrWhiteSpace(this.ScrMemID))
            {
                int id = -1;
                if (Int32.TryParse(this.ScrMemID, out id))
                {
                    AdversaryLock al = AdversaryLocking.Instance.GetLock(id);
                    if (al != null)
                    {
                        if (al.Login != this.LoginName && al.Expired == false)
                        {
                            this.imgLock.Visible = true;
                            this.imgLock.ToolTip = "Locked by " + al.Login;
                        }
                    }
                }
            }
        }
    }
}