﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Adversary.Web.Controls
{
    public partial class ScrollPositionExtender : System.Web.UI.UserControl
    {
        private string _cookie;

        /// <summary>
        /// the optional name of the session cookie to save/restore the scroll position to/from
        /// </summary>
        public string Cookie
        {
            get { return this._cookie; }
            set { this._cookie = value; }
        }

        private string _targetControlID;

        /// <summary>
        /// the optional id of the control whose scroll position will be saved
        /// </summary>
        public string TargetControlID
        {
            get { return this._targetControlID; }
            set { this._targetControlID = value; }
        }

        /// <summary>
        /// the client id of the target control
        /// </summary>
        public string TargetControlClientID
        {
            get
            {
                string id = "";
                Control ctrl = this.NamingContainer.FindControl(TargetControlID);
                if (ctrl != null)
                {
                    id = ctrl.ClientID;
                }
                return id;
            }
        }

        /// <summary>
        /// cook the targetcontrolclientid for use with jquery
        /// </summary>
        public string jQueryTargetControlClientID
        {
            get
            {
                return string.IsNullOrWhiteSpace(TargetControlID) ? "window" : string.Format("'#{0}'", this.TargetControlClientID);
            }
        }
    }
}