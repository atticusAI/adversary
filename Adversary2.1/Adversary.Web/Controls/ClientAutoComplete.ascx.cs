﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.DataLayer;
using Adversary.Controls;

namespace Adversary.Web.Controls
{
    public partial class ClientAutoComplete : System.Web.UI.UserControl
    {
        public delegate void ClientAutoCompleteHandler(object sender,
            ClientAutoCompletePopulatedEventArgs e);

        public event ClientAutoCompleteHandler AutoCompletePopulated;

        
        // private string _SelectedClientCode;
        /// <summary>
        /// 
        /// blmcgee@hollandhart.com 3/7/2013
        ///     got rid of the intermediate storage of the selected client code
        ///     now the value of the hidden is the only storage place for that value
        ///     this makes the control more friendly to client side coding
        /// </summary>
        public string SelectedClientCode
        {
            get
            {
                //return string.IsNullOrEmpty(_SelectedClientCode) ?  "" : _SelectedClientCode;
                return this.hidClientCode.Value;
            }
            set 
            { 
                //_SelectedClientCode = value; 
                this.hidClientCode.Value = value;
            }
        }

        private string _SelectedClientName;
        public string SelectedClientName
        {
            get
            {
                return string.IsNullOrEmpty(_SelectedClientName) ?  "" : _SelectedClientName;
            }
            set 
            { 
                _SelectedClientName = value; 
            }
        }

        private string _ServiceMethod = "";
        public string ServiceMethod
        {
            get 
            { 
                return _ServiceMethod; 
            }
            set 
            { 
                _ServiceMethod = value; 
            }
        }

        private bool _RaiseEventOnItemSelected = true;
        public bool RaiseEventOnItemSelected
        {
            get 
            { 
                return _RaiseEventOnItemSelected; 
            }
            set 
            { 
                _RaiseEventOnItemSelected = value; 
            }
        }

        public string TextBoxCssClass
        {
            get
            {
                return this.tbClient.CssClass;
            }
            set
            {
                this.tbClient.CssClass = value;
            }
        }

        private const string C_DISPLAYBLOCK = "display:block;";
        private const string C_DISPLAYNONE = "display:none;";

        //public HiddenField hidClientCode { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (this.Page.IsPostBack)
            //{
            //    if (!string.IsNullOrWhiteSpace(this.hidClientCode.Value))
            //    {
            //        _SelectedClientCode = this.hidClientCode.Value;
            //    }
            //}

            aceClient.MinimumPrefixLength = 3;
            aceClient.ServiceMethod = _ServiceMethod;
            aceClient.CompletionInterval = 100;
            aceClient.CompletionSetCount = 20;
            aceClient.TargetControlID = this.tbClient.ID;
            aceClient.OnClientItemSelected = this.ClientID + "_OnItemSelected";
            if (String.IsNullOrEmpty(_ServiceMethod)) throw new Exception("You must specify the 'ServiceMethod' property");

            if (String.IsNullOrEmpty(this.SelectedClientCode))
            {
                tbClient.Attributes.Add("style", C_DISPLAYBLOCK);
                this.lblClient.Attributes.Add("style", C_DISPLAYNONE);
            }
            else
            {
                tbClient.Attributes.Add("style", C_DISPLAYNONE);
                this.lblClient.Attributes.Add("style", C_DISPLAYBLOCK);
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (String.IsNullOrEmpty(_ServiceMethod)) throw new Exception("You must specify the 'ServiceMethod' property");
            // if (!String.IsNullOrEmpty(_SelectedClientCode)) hidClientCode.Value = _SelectedClientCode;
            _SelectedClientName = "";

            //fill in the labels
            if (!string.IsNullOrEmpty(this.SelectedClientCode))
            {
                using (AdversaryData advData = new AdversaryData())
                {
                    DataLayer.FinancialDataService.Client c = advData.SearchClients(this.SelectedClientCode).FirstOrDefault();
                    if (c != null)
                    {
                        _SelectedClientName = c.ClientName;
                    }
                    lblClient.Text = _SelectedClientName + "(" + this.SelectedClientCode + ")";
                    // hidClientCode.Value = _SelectedClientCode;
                }
            }

            this.aceClient.ServiceMethod = _ServiceMethod;
            this.aceClient.TargetControlID = tbClient.UniqueID;

            if (string.IsNullOrEmpty(this.SelectedClientCode))
            {
                tbClient.Attributes.Add("style", C_DISPLAYBLOCK);
                this.lblClient.Attributes.Add("style", C_DISPLAYNONE);
            }
            else
            {
                tbClient.Attributes.Add("style", C_DISPLAYNONE);
                this.lblClient.Attributes.Add("style", C_DISPLAYBLOCK);
            }

            string __itemselectedKey = "itemselected" + this.ID;
            string __itemclearedKey = "itemcleared" + this.ID;

            if (!Page.ClientScript.IsClientScriptBlockRegistered(__itemselectedKey))
            {
                Page.ClientScript.RegisterClientScriptBlock(typeof(string),
                    __itemselectedKey,
                    BuildJavascript().ToString());
            }

            base.OnPreRender(e);
        }

        protected void raiseEvent(object sender, EventArgs e)
        {
            ClientAutoCompletePopulatedEventArgs args = new ClientAutoCompletePopulatedEventArgs() 
            { 
                ClientNumber = hidClientCode.Value 
            };
            OnAutoCompletePopulated(this, args);
        }

        protected void OnAutoCompletePopulated(object sender, ClientAutoCompletePopulatedEventArgs e)
        {
            if (RaiseEventOnItemSelected)
            {
                if (AutoCompletePopulated != null)
                    AutoCompletePopulated(this, e);
            }
        }


        /// <summary>
        /// 
        /// blmcgee@hollandhart.com 3/7/2013
        ///     updated to use jquery syntax instead of asp.net $get()...
        ///     also added .change() event to the hidden field so that 
        ///     other jquery can take advantage of that event when it happens
        /// </summary>
        /// <returns></returns>
        private StringBuilder BuildJavascript()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<script type='text/javascript'>");
            sb.AppendLine("function " + this.ClientID + "_OnItemSelected(source, eventArgs){");
            sb.AppendLine("$('#" + tbClient.ClientID + "').hide();");
            sb.AppendLine("$('#" + hidClientCode.ClientID + "').val(eventArgs.get_value()).change();");
            sb.AppendLine("$('#" + lblClient.ClientID + "').html(eventArgs.get_text()).show();");
            if (RaiseEventOnItemSelected)
            {
                sb.AppendLine("$('#" + btnRaiseEvent.ClientID + "').click();");
            }
            sb.AppendLine("}");

            sb.AppendLine("function " + this.ClientID + "_clearClient() {");
            sb.AppendLine("$('#" + tbClient.ClientID + "').val('').show();");
            sb.AppendLine("$('#" + hidClientCode.ClientID + "').val('').change();");
            sb.AppendLine("$('#" + lblClient.ClientID + "').html('').hide();");
            sb.AppendLine("}");
            sb.AppendLine("</script>");
            return sb;
        }
    }
}