﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.DataLayer;
using Adversary.Web.Extensions;
using Adversary.Web.Controls.SummarySections;

namespace Adversary.Web.Controls
{
    public partial class ScreeningMemoSummary : System.Web.UI.UserControl
    {
        private Adversary.DataLayer.ScreeningMemoList __screeningMemoListData = null;
        private Adversary.DataLayer.ScreeningMemoSummary __screeningMemoSummaryData = null;
        private Adversary.DataLayer.AdversaryData _adversaryData = null;

        public Adversary.DataLayer.Memo ThisMemo
        {
            get;
            set;
        }

        public DisplayMode DisplayMode
        {
            get
            {
                return (this.Page is ScreeningBasePage)
                    ? ((ScreeningBasePage)this.Page).DisplayMode
                    : DisplayMode.WEB;
            }

            set
            {
                if (this.Page is ScreeningBasePage)
                {
                    ((ScreeningBasePage)this.Page).DisplayMode = value;
                }
            }
        }

        private Adversary.DataLayer.ScreeningMemoList ScreeningMemoListData
        {
            get
            {
                if (__screeningMemoListData == null)
                {
                    __screeningMemoListData = new DataLayer.ScreeningMemoList();
                }
                return __screeningMemoListData;
            }
        }

        private Adversary.DataLayer.ScreeningMemoSummary ScreeningMemoSummaryData
        {
            get
            {
                if (__screeningMemoSummaryData == null)
                {
                    __screeningMemoSummaryData = new DataLayer.ScreeningMemoSummary();
                }
                return __screeningMemoSummaryData;
            }
        }

        private Adversary.DataLayer.AdversaryData AdversaryData
        {
            get
            {
                if (this._adversaryData == null)
                {
                    this._adversaryData = new AdversaryData();
                }
                return this._adversaryData;
            }
        }

        public override void Dispose()
        {
            if (__screeningMemoListData != null)
            {
                __screeningMemoListData.Dispose();
                __screeningMemoListData = null;
            }
            base.Dispose();
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            ThisMemo.HH_GuardAgainstNull("Memo");

            List<ScreeningMemoSummarySection> sums = ScreeningMemoSummaryData.GetScrMemSummarySection(Convert.ToInt32(ThisMemo.ScrMemType));

            // special ordering for Basic Info, Client Address in print mode
            if (this.DisplayMode == Web.DisplayMode.PRINT)
            {
                int i = 0;
                foreach (ScreeningMemoSummarySection sum in sums)
                {
                    if (sum.Name == "Basic Information" && i == 0)
                    {
                        AddSummarySection(sum);
                        i++;
                    }
                    else if (sum.Name == "Client Address" && i == 1)
                    {
                        if (ThisMemo.ScrMemType != 6)
                        {
                            AddSummarySection(sum);
                        }
                        i++;
                    }

                    if (i > 1)
                    {
                        break;
                    }
                }
            }

            foreach (ScreeningMemoSummarySection sum in sums)
            {
                if (this.DisplayMode == Web.DisplayMode.PRINT)
                {
                    if (sum.Name == "Additional Attached Files" ||
                        sum.Name == "Docketing Information" ||
                        (sum.Name == "Client Address" && ThisMemo.ScrMemType != 6) ||
                        (sum.Name == "Contact Information" && ThisMemo.ScrMemType != 6) ||
                        sum.Name == "Referring Party" || 
                        sum.Name == "Basic Information")
                    {
                        continue;
                    }
                }
                AddSummarySection(sum);
            }

            // special ordering for docketing info and attachments in print mode
            if (this.DisplayMode == Web.DisplayMode.PRINT)
            {
                if (ThisMemo.ScrMemType == 6)
                {
                    foreach (ScreeningMemoSummarySection sum in sums)
                    {
                        if (sum.Name == "Additional Attached Files")
                        {
                            AddSummarySection(sum);
                            break;
                        }
                    }
                }
                else
                {
                    int i = 0;
                    foreach (ScreeningMemoSummarySection sum in sums)
                    {
                        if (sum.Name == "Docketing Information" && i == 0)
                        {
                            AddSummarySection(sum);
                            i++;
                        }
                        else if (sum.Name == "Additional Attached Files" && i == 1)
                        {
                            AddSummarySection(sum);
                            i++;
                        }

                        if (i > 1)
                        {
                            break;
                        }
                    }
                }
            }
        }

        private void AddSummarySection(ScreeningMemoSummarySection sum)
        {
            List<Adversary.DataLayer.Memo> memoList = new List<Memo>() { ThisMemo };
            Control ctrl = LoadControl(sum.VirtualPath);
            ((SummarySectionBase)ctrl).Memos = memoList;

            if (sum.Name != "Docketing Information")
            {
                phSections.Controls.Add(ctrl);
            }
            else
            {
                if (ThisMemo.HH_IsDocketingInfoRequired())
                    phSections.Controls.Add(ctrl);
            }
        }

        public string NewMatterOnly(object o)
        {
            string newMatterOnly = "";
            if (Convert.ToString(o) == "yes")
            {
                newMatterOnly = "New matter for client for which I have previously received origination credit";
            }
            else if (Convert.ToString(o) == "no")
            {
                newMatterOnly = "Other";
            }
            return newMatterOnly;
        }

        public string YesNo(object o)
        {
            return Convert.ToBoolean(o) == true ? "Yes" : "No";
        }

        public bool SmallRetainer(object retainerDescYes)
        {
            return (!string.IsNullOrWhiteSpace(Convert.ToString(retainerDescYes)));
        }

        public string SmallRetainerYesNo(object retainerDescYes)
        {
            return SmallRetainer(retainerDescYes) ? "Yes" : "No";
        }

        public bool HasRetainerDesc(object retainerDesc)
        {
            return (!string.IsNullOrWhiteSpace(Convert.ToString(retainerDesc)));
        }

        public string GetPersonName(object o)
        {
            string name = "";
            string personID = Convert.ToString(o);
            if (!string.IsNullOrWhiteSpace(personID))
            {
                try
                {
                    name = string.Format("{0} ({1})",
                        this.ScreeningMemoListData.GetPersonName(personID),personID);
                }
                catch
                {
                    name = string.Format("<span class=\"error\">Name lookup failed. ({0})</span>",
                        Convert.ToString(o));
                }
            }
            return name;
        }

        public string GetClientDisplayName(Memo memo)
        {
            string name = "";
            if (memo.ScrMemType == 2 ||
                ((memo.ScrMemType == 3 || memo.ScrMemType == 6) && !string.IsNullOrWhiteSpace(memo.ClientNumber)))
            {
                name = GetClientName(memo.ClientNumber);
            }
            else
            {
                if (memo.Company.HasValue)
                {
                    name = (memo.Company ?? false)
                        ? memo.NewClients[0].CName
                        : memo.NewClients[0].FName + " " + memo.NewClients[0].MName + " " + memo.NewClients[0].LName;
                    name = name + " (" + memo.HHLegalWorkID + ")";
                }
            }
            return name;
        }

        public string GetBiller(object respAttID)
        {
            string biller = "";
            Adversary.DataLayer.HRDataService.Employee emp = this.AdversaryData.GetBiller(Convert.ToString(respAttID));
            if (emp != null)
            {
                biller = emp.FullName + " (" + emp.PersonID + ")";
            }
            return biller;
        }


        public string GetClientDisplayName()
        {
            return GetClientDisplayName(ThisMemo);
        }

        protected string GetClientName(string clientCode)
        {
            string name = "";
            try
            {
                name = string.Format("{0} ({1})",
                    this.ScreeningMemoListData.GetClientName(clientCode), 
                    clientCode);
            }
            catch
            {
                name = string.Format("<span class=\"error\">Name lookup failed. ({0})</span>", clientCode);
            }
            return name;
        }

        public string GetOfficeName(object o)
        {
            string name = "";
            try
            {
                name = string.Format("{0} ({1})",
                    this.ScreeningMemoListData.GetOfficeName(Convert.ToString(o)),
                    Convert.ToString(o));
            }
            catch
            {
                name = string.Format("<span class=\"error\">Name lookup failed. ({0})</span>",
                    Convert.ToString(o));
            }
            return name;
        }

        public string GetPracticeTypeName(object o)
        {
            string name = "";
            if (!string.IsNullOrWhiteSpace(Convert.ToString(o)))
            {
                try
                {
                    name = string.Format("{0} ({1})",
                        this.ScreeningMemoListData.GetPracticeCodeName(Convert.ToString(o)),
                        Convert.ToString(o));
                }
                catch
                {
                    name = string.Format("<span class=\"error\">Name lookup failed. ({0})</span>",
                        Convert.ToString(o));
                }
            }
            return name;
        }

        //public string DownloadUrl(object filename)
        //{
        //    return string.Format("~/Screening/Download.aspx?ScrMemID={0}&FileName={1}",
        //        ThisMemo.ScrMemID, Server.UrlEncode(Convert.ToString(filename)));           
        //}
        public string DownloadUrl(object attachmentID)
        {
            return string.Format("~/download.ashx?attachmentID={0}", attachmentID);
        }

        public bool HasAdditionalAttachments()
        {
            int count = 0;
            if (this.PrintMode())
            {
                count = ThisMemo.Attachments.Count();
            }
            else
            {
                count = ThisMemo.Attachments.Where(a => a.Description != "Written Policy"
                    && a.Description != "Engagement Letter"
                    && a.Description != "Conflict Report"
                    && a.Description != "Rejection Letter").Count();
            }
            return count > 0 ? true : false;
        }

        public bool PrintMode()
        {
            return this.DisplayMode == DisplayMode.PRINT;
        }

        public string ProBonoType()
        {
            var qry = from pb in this.AdversaryData.GetProBonoTypes()
                      where pb.ProBonoTypeID == ThisMemo.ProBonoTypeID
                      select pb.Name;

            return qry.FirstOrDefault();
        }

        public bool IsProBono()
        {
            return ((ThisMemo.ScrMemType == 3) || (ThisMemo.ScrMemType == 4));
        }

        internal string GetNoConflictReason()
        {
            return ThisMemo.ConflictDesc;
        }

        internal string GetNoEngagementReason()
        {
            return ThisMemo.EngageDesc;
        }

        internal string GetLegalWorkFor()
        {
            return !(ThisMemo.Company ?? false) ? "Employee" : "Firm";
        }

        internal string GetNoRejectReason()
        {
            return ThisMemo.NoRejectionLetterReason;
        }
    }
}