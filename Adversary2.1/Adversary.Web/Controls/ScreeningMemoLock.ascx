﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ScreeningMemoLock.ascx.cs"
    Inherits="Adversary.Web.Controls.ScreeningMemoLock" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:UpdatePanel ID="upLock" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:Timer ID="timLock" runat="server" OnTick="timLock_Tick" Interval="10000" />
        <asp:Label ID="lblMsg" CssClass="error" runat="server" />
        <asp:LinkButton ID="lnkHid" runat="server" Text="" Style="display: none;" />
        <asp:ModalPopupExtender ID="mpxLock" runat="server" TargetControlID="lnkHid" PopupControlID="pnlLocked"
            BackgroundCssClass="pop-back">
        </asp:ModalPopupExtender>
        <asp:Panel ID="pnlLocked" runat="server" CssClass="pop" Style="display: none;">
            <table>
                <tr>
                    <td align="center">
                        <strong>Memo Locked</strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblLockMsg" CssClass="error" runat="server" />
                        <asp:LinkButton ID="lnkUnlock" runat="server" Text="Click here to unlock." OnClick="lnkUnlock_Click"
                            Visible="false" />
                        <asp:LinkButton ID="lnkAcknowledge" runat="server" Text="Click here to acknowledge."
                            OnClick="lnkAcknowledge_Click" Visible="false" />
                        <asp:LinkButton ID="lnkBack" runat="server" Text="Back to Admin Queue" PostBackUrl="~/Admin/AdminQueue.aspx"
                            Visible="false" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
