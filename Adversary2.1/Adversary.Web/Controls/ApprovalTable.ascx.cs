﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.DataLayer;
using Adversary.Web.Extensions;
using System.Text;

namespace Adversary.Web.Controls
{
    public partial class ApprovalTable : System.Web.UI.UserControl
    {
        public Memo ThisMemo
        {
            get;
            set;
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            ThisMemo.HH_GuardAgainstNull("ThisMemo");
        }

        protected string ClientMatterNumbers()
        {
            StringBuilder sb = new StringBuilder();

            foreach (ClientMatterNumber cmn in ThisMemo.ClientMatterNumbers)
            {
                if (sb.Length > 0)
                {
                    sb.Append(", ");
                }
                sb.Append(cmn.ClientMatterNumber1);
            }

            return sb.ToString();
        }

        protected void frmApprovalTable_PreRender(object sender, EventArgs e)
        {
            ((FormView)sender).DataSource = ThisMemo.Trackings;
            ((FormView)sender).DataBind();
        }
    }
}