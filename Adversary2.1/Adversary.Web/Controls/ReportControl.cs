﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Reporting.WebForms;
using System.Web.UI;

namespace Adversary.Web.Controls
{
    public class ReportControl : UserControl 
    {
        private ReportViewer _reportViewer;
        public ReportViewer ReportViewer
        {
            get { return this._reportViewer; }
            set { this._reportViewer = value; }
        }

        private string _reportPath;
        public string ReportPath
        {
            get { return this._reportPath; }
            set { this._reportPath = value; }
        }

        public ReportControl()
        {
            this._reportViewer = null;
            this._reportPath = null;
        }

        public ReportControl(ReportViewer reportViewer, string reportPath)
        {
            this._reportViewer = reportViewer;
            this._reportPath = reportPath;
        }
        
    }
}
