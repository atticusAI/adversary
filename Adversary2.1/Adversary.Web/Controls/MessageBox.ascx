﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MessageBox.ascx.cs"
    Inherits="Adversary.Web.Controls.MessageBox" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:LinkButton ID="lbHid" runat="server" Text="" Style="display: none" />
<asp:ModalPopupExtender ID="mpxMessageBox" runat="server" TargetControlID="lbHid"
    PopupControlID="pnlMessageBox" />
<asp:Panel ID="pnlMessageBox" runat="server" Style="display: none">
    <table>
        <tr>
            <td>
                <asp:Label ID="lblMessage" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnOK" runat="server" Text="Ok" OnClick="btnOK_Click" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                <asp:Button ID="btnAbort" runat="server" Text="Abort" OnClick="btnAbort_Click" />
                <asp:Button ID="btnRetry" runat="server" Text="Retry" OnClick="btnRetry_Click" />
                <asp:Button ID="btnIgnore" runat="server" Text="Ignore" OnClick="btnIgnore_Click" />
                <asp:Button ID="btnYes" runat="server" Text="Yes" OnClick="btnIgnore_Click" />
                <asp:Button ID="btnNo" runat="server" Text="No" OnClick="btnNo_Click" />
            </td>
        </tr>
    </table>
</asp:Panel>
