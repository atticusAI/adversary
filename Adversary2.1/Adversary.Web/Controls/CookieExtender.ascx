﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CookieExtender.ascx.cs" Inherits="Adversary.Web.Controls.CookieExtender" %>
<script type="text/javascript">
    $(document).ready(function () {
        <%if (this.UseBlur) {%>
        $('#<%=this.TargetControlClientID%>').blur(function () {
            $.cookie('<%=this.ClientID%>', $(this).val());
        });
        <%} else {%>
        $('#<%=this.TargetControlClientID%>').change(function () {
            $.cookie('<%=this.ClientID%>', $(this).val());
        });
        <%}%>
        <%if (!string.IsNullOrWhiteSpace(this.ClearControlID)) {%>
        $('#<%=this.ClearControlClientID%>').click(function () {
            $.cookie('<%=this.ClientID%>', null);
        });
        <%}%>
    });
</script>