﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.Web.Extensions;
using Adversary.DataLayer;

namespace Adversary.Web.Controls
{
    public partial class ARTable : System.Web.UI.UserControl
    {
        protected bool HasBalance(object o)
        {
            return ((ARBalance)o).TotalAR > 0;
        }

        public string ClientNumber
        {
            get;
            set;
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            ClientNumber.HH_GuardAgainstNullOrWhiteSpace("ClientNumber");
        }

        protected void frmARBalance_PreRender(object sender, EventArgs e)
        {
            using (Adversary.DataLayer.ARTable dal = new DataLayer.ARTable())
            {
                ARBalance bal = dal.GetClientARBalance(ClientNumber);
                if (bal != null)
                {
                    ((FormView)sender).DataSource = new List<ARBalance>() { bal };
                }
                else
                {
                    ((FormView)sender).DataSource = new List<ARBalance>();
                }

                ((FormView)sender).DataBind();
            }
        }
    }
}