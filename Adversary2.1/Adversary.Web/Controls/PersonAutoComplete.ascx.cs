﻿using System;
using Adversary.Controls;
using Adversary.DataLayer;
using System.Web.UI;

namespace Adversary.Web.Controls
{
    public partial class PersonAutoComplete : System.Web.UI.UserControl
    {
        public delegate void PersonAutoCompleteHandler(object sender, PersonAutoCompletePopulatedEventArgs e);

        public event PersonAutoCompleteHandler AutoCompletePopulated;

        #region properties
        private string _TextBoxCssClass;
        [CssClassProperty]
        public string TextBoxCssClass
        {
            get 
            { 
                return _TextBoxCssClass; 
            }
            set 
            { 
                _TextBoxCssClass = value; 
            }
        }

        private string _JqueryRequiredMessage;
        public string JqueryRequiredMessage
        {
            get 
            { 
                return _JqueryRequiredMessage; 
            }
            set 
            { 
                _JqueryRequiredMessage = value; 
            }
        }

        // private string _SelectedEmployeeCode;
        public string SelectedEmployeeCode
        {
            get
            {
                // return String.IsNullOrEmpty(_SelectedEmployeeCode) ? "" : _SelectedEmployeeCode;
                return this.hidEmployeeCode.Value;
            }
            set 
            { 
                // _SelectedEmployeeCode = value; 
                this.hidEmployeeCode.Value = value;
            }
        }

        private string _SelectedEmployeeName;
        public string SelectedEmployeeName
        {
            get
            {
                return String.IsNullOrEmpty(_SelectedEmployeeName) ? "" : _SelectedEmployeeName;
            }
            set 
            { 
                _SelectedEmployeeName = value; 
            }
        }

        private string _ServiceMethod = "";
        public string ServiceMethod
        {
            get 
            { 
                return _ServiceMethod; 
            }
            set 
            { 
                _ServiceMethod = value; 
            }
        }

        private bool _PopulatedEventCausesValidation = false;
        public bool PopulatedEventCausesValidation
        {
            get 
            { 
                return _PopulatedEventCausesValidation; 
            }
            set 
            { 
                _PopulatedEventCausesValidation = value; 
            }
        }

        private bool _RaiseEventOnItemSelected = true;
        public bool RaiseEventOnItemSelected
        {
            get 
            { 
                return _RaiseEventOnItemSelected; 
            }
            set 
            { 
                _RaiseEventOnItemSelected = value; 
            }
        }

        private const string C_DISPLAYBLOCK = "display:block;";
        private const string C_DISPLAYNONE = "display:none;";

        private AdversaryData dataLayer;
        #endregion

        protected void Page_Load(object sender, EventArgs e) { }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            btnRaiseEvent.Click += new EventHandler(Local_PersonAutoCompleteHandler);
            btnRaiseEvent.CausesValidation = _PopulatedEventCausesValidation;
            this.AutoCompletePopulated += new PersonAutoCompleteHandler(PersonAutoComplete_AutoCompletePopulated);
            aceEmployee.MinimumPrefixLength = 3;
            aceEmployee.ServiceMethod = _ServiceMethod;
            aceEmployee.CompletionInterval = 100;
            aceEmployee.CompletionSetCount = 20;
            aceEmployee.TargetControlID = this.tbEmployee.ID;
            aceEmployee.OnClientItemSelected = this.ClientID + "_OnItemSelected";
            if (!Page.IsPostBack)
            {
                if (String.IsNullOrEmpty(_ServiceMethod)) throw new Exception("You must specify the 'ServiceMethod' property");
            }
        }

        void PersonAutoComplete_AutoCompletePopulated(object sender, PersonAutoCompletePopulatedEventArgs e)
        {
            SelectedEmployeeCode = hidEmployeeCode.Value;
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (!String.IsNullOrEmpty(TextBoxCssClass)) tbEmployee.CssClass = TextBoxCssClass;
            if (!String.IsNullOrEmpty(JqueryRequiredMessage)) tbEmployee.ToolTip = JqueryRequiredMessage;
            if (!String.IsNullOrEmpty(SelectedEmployeeCode)) hidEmployeeCode.Value = SelectedEmployeeCode;
            if (String.IsNullOrEmpty(_SelectedEmployeeName))
            {
                dataLayer = new AdversaryData();
                DataLayer.FinancialDataService.FinancialDataEmployee fEmployee = dataLayer.GetEmployee(SelectedEmployeeCode);
                if (fEmployee != null)
                {
                    _SelectedEmployeeName = fEmployee.EmployeeName;
                }
                else
                {
                    _SelectedEmployeeName = "No employee matching Employee code '" + SelectedEmployeeCode + "' could be found.";
                }
            }

            lblEmployee.Text = _SelectedEmployeeName + " (" + SelectedEmployeeCode + ")";
                
            if (String.IsNullOrEmpty(SelectedEmployeeCode))
            {
                tbEmployee.Attributes.Add("style", C_DISPLAYBLOCK);
                this.lblEmployee.Attributes.Add("style", C_DISPLAYNONE);
            }
            else
            {
                tbEmployee.Attributes.Add("style", C_DISPLAYNONE);
                this.lblEmployee.Attributes.Add("style", C_DISPLAYBLOCK);
            }

            string __itemselectedKey = "itemselected" + this.ClientID;
            string __itemclearedKey = "itemcleared" + this.ClientID;

            if(!Page.ClientScript.IsClientScriptBlockRegistered(__itemselectedKey))
                Page.ClientScript.RegisterClientScriptBlock(typeof(string), __itemselectedKey, ItemSelectedJavascript().ToString());
            if(!Page.ClientScript.IsClientScriptBlockRegistered(__itemclearedKey))
                Page.ClientScript.RegisterClientScriptBlock(typeof(string), __itemclearedKey, ItemClearedJavascript().ToString());

            aClear.Attributes.Add("onclick", "javascript:" + this.ClientID + "_clearEmployee();");
        }

        /// <summary>
        /// 
        /// blmcgee@hollandhart.com 3/7/2013
        ///     changed to use jquery syntax and raise the change event on the hidden field
        /// </summary>
        /// <returns></returns>
        private System.Text.StringBuilder ItemSelectedJavascript()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.AppendLine("<script language='javascript' type='text/javascript'>");
            sb.AppendLine("function " + this.ClientID + "_OnItemSelected(source, eventArgs) {");
            sb.AppendLine("$('#" + tbEmployee.ClientID + "').hide();");
            sb.AppendLine("$('#" + hidEmployeeCode.ClientID + "').val(eventArgs.get_value()).change();");
            sb.AppendLine("$('#" + lblEmployee.ClientID + "').html(eventArgs.get_text()).show();");
            sb.AppendLine("$('#" + btnRaiseEvent.ClientID + "').click();");
            sb.AppendLine("}</script>");
            return sb;
        }

        /// <summary>
        /// 
        /// blmcgee@hollandhart.com 3/7/2013
        ///     changed to use jquery syntax and raise the change event on the hidden field
        /// </summary>
        /// <returns></returns>
        private System.Text.StringBuilder ItemClearedJavascript()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.AppendLine("<script language='javascript' type='text/javascript'>");
            sb.AppendLine("function " + this.ClientID + "_clearEmployee() {");
            sb.AppendLine("$('#" + tbEmployee.ClientID + "').val('').show();");
            sb.AppendLine("$('#" + hidEmployeeCode.ClientID + "').val('').change();");
            sb.AppendLine("$('#" + lblEmployee.ClientID + "').html('').hide();");
            sb.AppendLine("}</script>");
            return sb;
        }

        protected virtual void Local_PersonAutoCompleteHandler(object sender, EventArgs e)
        {
            string[] names = Adversary.Utils.StringUtils.ParseNameFromAutoCompleteText(tbEmployee.Text);

            PersonAutoCompletePopulatedEventArgs acArgs = new PersonAutoCompletePopulatedEventArgs()
            {
                PersonnelID = names[0],
                EmployeeFirstName = names[1],
                EmployeeMiddleName = names[2],
                EmployeeLastName = names[3]
            };
            this.SelectedEmployeeCode = acArgs.PersonnelID;
            OnAutoCompletePopulated(sender, acArgs);
        }

        protected void OnAutoCompletePopulated(object sender, PersonAutoCompletePopulatedEventArgs e)
        {
            if (RaiseEventOnItemSelected)
            {
                if (AutoCompletePopulated != null)
                    AutoCompletePopulated(this, e);
            }
        }
    }
}
