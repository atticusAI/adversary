﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RequestSummary.ascx.cs" Inherits="Adversary.Web.Controls.RequestSummary" %>
<asp:Panel ID="pnlError" runat="server" Visible="false">
    <asp:Label ID="lblError" runat="server" CssClass="error">No Request is selected.</asp:Label>
</asp:Panel>
<asp:Panel ID="pnlRequest" runat="server">
    <table>
        <tr id="trStatus" runat="server">
            <td>Status</td>
            <td>
                <asp:Label ID="lblStatus" runat="server" />
            </td>
        </tr>
        <tr>
            <td>Request ID#
            </td>
            <td>
                <asp:Label ID="lblRequestID" runat="server" />
            </td>
        </tr>
        <tr>
            <td>Submitted On
            </td>
            <td>
                <asp:Label ID="lblSubmittedOn" runat="server" />
            </td>
        </tr>
        <tr>
            <td>Deadline Date
            </td>
            <td>
                <asp:Label ID="lblDeadlineDate" runat="server" />
            </td>
        </tr>
        <tr>
            <td>Attorney
            </td>
            <td>
                <asp:Label ID="lblAttorneyPayrollID" runat="server" />
            </td>
        </tr>
        <tr>
            <td>Submitted By
            </td>
            <td>
                <asp:Label ID="lblTypistLogin" runat="server" />
            </td>
        </tr>
        <tr>
            <td>Client Name
            </td>
            <td>
                <asp:Label ID="lblClientName" runat="server" />
            </td>
        </tr>
        <tr>
            <td>Client Status Category
            </td>
            <td>
                <asp:Label ID="lblClientStatusCategory" runat="server" />
            </td>
        </tr>
        <tr id="trClientMatterNumber" runat="server">
            <td>Client Matter Number (if existing client)
            </td>
            <td>
                <asp:Label ID="lblClientMatterNumber" runat="server" />
            </td>
        </tr>
        <tr>
            <td>Practice Type/Specialty Code
            </td>
            <td>
                <asp:Label ID="lblPracticeCode" runat="server" />
            </td>
        </tr>
        <tr>
            <td>Keep this adversary request confidential?
            </td>
            <td>
                <asp:Label ID="lblConfidential" runat="server" />
            </td>
        </tr>
        <tr>
            <td>Adverse Parties
            </td>
            <td>
                <asp:Label ID="lblAdverseParties" runat="server" />
            </td>
        </tr>
        <tr>
            <td>Affiliates of Adverse Parties</td>
            <td>
                <asp:Label ID="lblAffiliatesofAdverseParties" runat="server" />
            </td>
        </tr>
        <tr>
            <td>Related Co-Defendants
            </td>
            <td>
                <asp:Label ID="lblRelatedCoDefendants" runat="server" />
            </td>
        </tr>
        <tr>
            <td>Related Parties (not including co-defendants) </td>
            <td>
                <asp:Label ID="lblRelatedPartiesNotIncludingCoDefendants" runat="server" />
            </td>
        </tr>
        <tr>
            <td>Other Client Affiliates </td>
            <td>
                <asp:Label ID="lblOtherClientAffiliates" runat="server" />
            </td>
        </tr>
        <tr>
            <td>Parties to Mediation and Arbitration </td>
            <td>
                <asp:Label ID="lblPartiesToMediationAndArbitration" runat="server" />
            </td>
        </tr>
        <tr>
            <td>Opposing Counsel </td>
            <td>
                <asp:Label ID="lblOpposingCounsel" runat="server" />
            </td>
        </tr>
        <asp:DataList ID="lstConflicts" runat="server" RepeatLayout="Flow">
            <ItemTemplate>
                <tr>
                    <td>
                        <asp:Label ID="lblDesc" runat="server" Text='<%#Eval("Description")%>'></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblYes" runat="server" ForeColor="Red" Text="Yes" />
                    </td>
                </tr>
            </ItemTemplate>
        </asp:DataList>
        <tr>
            <td>Assigned To </td>
            <td>
                <asp:Label ID="lblAssignedTo" runat="server" />
            </td>
        </tr>
        <tr>
            <td>Work Begun Date
            </td>
            <td>
                <asp:Label ID="lblWorkBegunDate" runat="server" />
            </td>
        </tr>
        <tr>
            <td>Work Completed By </td>
            <td>
                <asp:Label ID="lblWorkCompletedBy" runat="server" />
            </td>
        </tr>
    </table>
</asp:Panel>
