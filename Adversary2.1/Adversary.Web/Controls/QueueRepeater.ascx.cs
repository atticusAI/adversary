﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.DataLayer;
using Adversary.Web.Pages;
using System.Web.UI.HtmlControls;

namespace Adversary.Web.Controls
{
    public partial class QueueRepeater : System.Web.UI.UserControl
    {

        public Adversary.DataLayer.Queue QueueDataLayer
        {
            get;
            set;
        }

        private Adversary.DataLayer.Approval _approvalDataLayer = null;

        private Adversary.DataLayer.Approval ApprovalDataLayer
        {
            get
            {
                if (this._approvalDataLayer == null)
                {
                    this._approvalDataLayer = new Adversary.DataLayer.Approval();
                }
                return this._approvalDataLayer;
            }
        }

        public List<Memo> Queue
        {
            get;
            set;
        }

        private string _notice = "";
        private string Notice
        {
            get
            {
                return this._notice;
            }
            set
            {
                if (this._notice == "" || value == "")
                {
                    this._notice = value;
                }
            }
        }

        private string _noticeClass = "";
        private string NoticeClass
        {
            get
            {
                return this._noticeClass;
            }
            set
            {
                if (this._noticeClass == "" || value == "")
                {
                    this._noticeClass = value;
                }
            }
        }

        public override void DataBind()
        {
            base.DataBind();
            this.repQueue.DataSource = Queue;
            this.repQueue.DataBind();
        }

        protected string GetStatusName(object o)
        {
            return Convert.ToString(((Memo)o).Trackings[0].StatusTypeID);
        }

        protected string GetClientDisplayName(object o)
        {
            return ((AdminBasePage)Page).GetClientDisplayName((Memo)o);
        }

        protected string GetMemoTypeDescription(object o)
        {
            Memo memo = (Memo)o;
            if (memo.Trackings[0].RejectionHold ?? false)
            {
                this.Notice = "* This memo required modification and has been resubmitted";
                this.NoticeClass = "warn";
            }

            return ((AdminBasePage)Page).GetMemoTypeDescription(memo);
        }

        protected string GetPracticeCodeName(object o)
        {
            Memo memo = (Memo)o;
            string pracCodeName = memo.PracCode;
            if (!string.IsNullOrWhiteSpace(memo.PracCode))
            {
                pracCodeName = ((AdminBasePage)Page).GetPracticeCodeName(memo);
                List<AdminLogin> adminLogins = this.QueueDataLayer.GetAdminLoginsByPrimaryPracticeCode(memo.PracCode);
                if (adminLogins == null || adminLogins.Count == 0)
                {
                    this.Notice = string.Format("* Could not determine the Primary PGL for this memo, a PGL has not been assigned for {0} ({1})",
                        pracCodeName,
                        memo.PracCode);
                    this.NoticeClass = "fail";
                }
            }
            return pracCodeName;
        }

        protected string GetOfficeName(object o)
        {
            Memo memo = (Memo)o;
            string officeName = "";

            if (memo.ScrMemType != 6)
            {
                Adversary.DataLayer.HRDataService.FirmOffice office = null;
                try
                {
                    office = ((AdminBasePage)Page).AdversaryDataLayer.GetAttorneyOffice(memo.RespAttID);
                }
                catch
                {
                }
                if (office == null)
                {
                    this.Notice = string.Format("* Could not determine the originating office for this memo, the attorney listed ({0}) does not resolve to a valid H&H employee ID",
                        memo.RespAttID);
                    this.NoticeClass = "fail";
                }
                else
                {
                    officeName = office.City;
                }
            }
            return officeName;
        }

        protected string GetSubmittedDate(RepeaterItem container, object o)
        {
            Memo memo = (Memo)o;
            string submitted = "";
            if (memo.Trackings[0].SubmittedOn.HasValue)
            {
                submitted = memo.Trackings[0].SubmittedOn.ToString();
                if (memo.Trackings[0].SubmittedOn <= DateTime.Now.AddDays(-4))
                {
                    TimeSpan span = (TimeSpan)(DateTime.Now - memo.Trackings[0].SubmittedOn);
                    this.Notice = string.Format("* Memo was submitted over {0} days ago.", span.Days);
                    this.NoticeClass = "fail";
                    ((Label)container.FindControl("lblDate")).CssClass = "error";

                }
            }
            return submitted;
        }

        protected string GetEnteringAttorneyName(object o)
        {
            Memo memo = (Memo)o;
            return ((AdminBasePage)Page).GetEnteringAttorneyName(memo);
        }

        protected string GetPersonnelName(object o)
        {
            Memo memo = (Memo)o;
            return ((AdminBasePage)Page).GetPersonnelName(memo);
        }

        protected string GetAPStatus(object o)
        {
            Memo memo = (Memo)o;
            Tracking trk = memo.Trackings[0];
            return this.GetApprovalSpan(trk.APApproved, trk.APAcknowledged, "AP", trk.APDate);
        }

        protected string GetPGLStatus(object o)
        {
            Memo memo = (Memo)o;
            Tracking trk = memo.Trackings[0];
            return this.GetApprovalSpan(trk.PGMApproved, trk.PGMAcknowledged, "PGL", trk.PGMDate);
        }

        protected string GetAdvStatus(object o)
        {
            Memo memo = (Memo)o;
            Tracking trk = memo.Trackings[0];
            return this.GetApprovalSpan(trk.AdversaryAdminApproved, trk.AdversaryAdminAcknowledged, "Adv", trk.AdversaryAdminDate);
        }

        public string GetPGL2Status(object o)
        {
            Memo memo = (Memo)o;
            Tracking trk = memo.Trackings[0];
            return this.GetApprovalSpan(trk.PGM2Approved, trk.PGM2Acknowledged, "2nd PGL", trk.PGM2Date);
        }

        public string GetFinancialStatus(object o)
        {
            Memo memo = (Memo)o;
            Tracking trk = memo.Trackings[0];
            return this.GetApprovalSpan(trk.FinApproved, trk.FinAcknowledged, "Fin", trk.FinDate);
        }

        private string GetApprovalSpan(bool? approved, bool? acknowledged, string name, DateTime? date)
        {
            string msg = "";
            string cls = "";
            if (approved ?? false)
            {
                msg = string.Format("{0} Approved {1}", name, (date.HasValue ? ((DateTime)date).ToString("MM/dd") : ""));
                cls = "good";
            }
            else if (acknowledged ?? false)
            {
                msg = string.Format("{0} Acknowledged", name);
                cls = "meh";
            }
            else
            {
                msg = string.Format("No {0} Response", name);
                cls = "bad";
            }
            return string.Format("<span class=\"queue {0}\">{1}</span><br />", cls, msg);
        }

        /// <summary>
        /// TODO: Refactor so that the CurrentUser is injected rather than siphoned from the tracking base page
        /// </summary>
        protected void repQueue_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "View")
            {
                Memo memo = ((AdminBasePage)Page).AdversaryDataLayer.GetMemo(Convert.ToInt32(e.CommandArgument));
                if (memo != null && memo.Trackings.Count > 0)
                {
                    this.Acknowledge(memo.Trackings[0], ((TrackBasePage)Page).CurrentUser);
                    if (((TrackBasePage)Page).CurrentUser.IsAdversary
                        || ((TrackBasePage)Page).CurrentUser.IsAdversaryAdmin 
                        || (((TrackBasePage)Page).CurrentUser.AdminLogin.UserID == memo.Trackings[0].APUserID)
                        || (((TrackBasePage)Page).CurrentUser.AdminLogin.UserID == memo.Trackings[0].PGMUserID))
                    {
                        Response.Redirect("~/Admin/Approval.aspx?ScrMemID=" + memo.ScrMemID);
                    }
                    else
                    {
                        Response.Redirect("~/Screening/Summary.aspx?type=approve&ScrMemType=" + memo.ScrMemType + "&ScrMemID=" + memo.ScrMemID);
                    }
                }
            }
        }

        /// <summary>
        /// TODO: move to business layer
        /// </summary>
        private void Acknowledge(Tracking trk, CurrentUser currentUser)
        {
            //acknowledge for AP
            if (currentUser.IsAP)
            {
                if (!trk.APUserID.HasValue)
                    trk.APUserID = currentUser.AdminLogin.UserID;
                trk.APAcknowledged = true;
            }

            //acknowledge PGM
            if (currentUser.IsPGM)
            {
                if (!trk.PGMUserID.HasValue)
                    trk.PGMUserID = currentUser.AdminLogin.UserID;
                trk.PGMAcknowledged = true;
            }

            //acknowledge for Adversary group
            if (currentUser.IsAdversaryAdmin) //admin acknowledge
            {
                trk.AdversaryAdminAcknowledged = true;
            }
            //or acknowledge for adversary group member if processing memo
            else if (currentUser.IsAdversary && (trk.StatusTypeID == TrackingStatusType.AdversaryGroupProcessing))
            {
                if (!trk.AdversaryUserID.HasValue)
                    trk.AdversaryUserID = currentUser.AdminLogin.UserID;
                trk.AdversaryAcknowledged = true;
            }

            this.ApprovalDataLayer.UpdateTracking(trk);
        }

        protected void repQueue_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (!string.IsNullOrWhiteSpace(this.Notice))
                {
                    Panel pnlQueue = (Panel)e.Item.FindControl("pnlQueue");
                    Label lblNotice = (Label)e.Item.FindControl("lblNotice");
                    lblNotice.Visible = true;
                    lblNotice.Text = this.Notice;
                    pnlQueue.CssClass = string.Concat(pnlQueue.CssClass, " ", this.NoticeClass);

                    this.Notice = "";
                    this.NoticeClass = "";
                }
            }
        }

        public override void Dispose()
        {
            base.Dispose();
            if (this._approvalDataLayer != null)
            {
                this._approvalDataLayer.Dispose();
                this._approvalDataLayer = null;
            }
        }
    }
}
