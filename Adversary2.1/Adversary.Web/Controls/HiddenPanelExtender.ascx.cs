﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Adversary.Web.Controls
{
    public partial class HiddenPanelExtender : System.Web.UI.UserControl
    {
        /// <summary>
        /// show or hide the panel
        /// </summary>
        public bool Shown
        {
            get { return Convert.ToBoolean(this.hid.Value); }
            set { this.hid.Value =  value.ToString(); }
        }

        private string _targetControlID;

        /// <summary>
        /// the required target control
        /// </summary>
        public string TargetControlID
        {
            get { return this._targetControlID; }
            set { this._targetControlID = value; }
        }

        /// <summary>
        /// the client id of the target control
        /// </summary>
        public string TargetControlClientID
        {
            get
            {
                string clientID = "";
                Control ctrl = this.NamingContainer.FindControl(this.TargetControlID);
                if (ctrl != null)
                {
                    clientID = ctrl.ClientID;
                }
                return clientID;
            }
        }

        private string _panelControlID;

        /// <summary>
        /// the required id of the panel to show/hide
        /// </summary>
        public string PanelControlID
        {
            get { return this._panelControlID; }
            set { this._panelControlID = value; }
        }

        private string _cookie;

        /// <summary>
        /// the optional name of a session cookie to save/restore the shown state from
        /// </summary>
        public string Cookie
        {
            get { return this._cookie; }
            set { this._cookie = value; }
        }

        /// <summary>
        /// the client id of the panel to show/hide
        /// </summary>
        public string PanelControlClientID
        {
            get
            {
                string clientID = "";
                Control ctrl = this.NamingContainer.FindControl(this.PanelControlID);
                if (ctrl != null)
                {
                    clientID = ctrl.ClientID;
                }
                return clientID;
            }
        }

        private string _shownCssClass;

        /// <summary>
        /// the optional css class of the target control while the panel is shown
        /// </summary>
        public string ShownCssClass
        {
            get { return this._shownCssClass; }
            set { this._shownCssClass = value; }
        }

        private string _hiddenCssClass;

        /// <summary>
        /// the optional css class of the target control while the panel is hidden
        /// </summary>
        public string HiddenCssClass
        {
            get { return this._hiddenCssClass; }
            set { this._hiddenCssClass = value; }
        }

        /// <summary>
        /// get the css class for the target control accounting for shown/hidden state of the panel
        /// </summary>
        public string CssClass
        {
            get { return this.Shown ? this.ShownCssClass : this.HiddenCssClass; }
        }
    }
}