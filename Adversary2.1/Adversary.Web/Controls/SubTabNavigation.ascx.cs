﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.DataLayer;
using System.Web.UI.HtmlControls;
using System.Reflection;

namespace Adversary.Web.Controls
{
    public partial class Navigation : System.Web.UI.UserControl
    {

        private ExpandProperties _expandableProperties = null;

        public ExpandProperties ExpandableProperties
        {
            get
            {
                if (this._expandableProperties == null)
                {
                    this._expandableProperties = new ExpandProperties();
                }
                return this._expandableProperties;
            }
        }

        private DataLayer.Navigation _navigationDataLayer = null;

        public DataLayer.Navigation NavigationDataLayer
        {
            get
            {
                if (this._navigationDataLayer == null)
                {
                    this._navigationDataLayer = new DataLayer.Navigation();
                }
                return this._navigationDataLayer;
            }
        }

        public override void Dispose()
        {
            if (this._navigationDataLayer != null)
            {
                this._navigationDataLayer.Dispose();
                this._navigationDataLayer = null;
            }
            base.Dispose();
        }

        protected void rptNav_PreRender(object sender, EventArgs e)
        {
            this.rptNav.DataSource = this.NavigationDataLayer.GetSubTabNavigationByPageUrl(this.PageUrl);
            this.rptNav.DataBind();
        }

        private string _pageUrl = null;

        public string PageUrl
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this._pageUrl))
                {
                    //_pageUrl = this.Request.ApplicationPath == "/"
                    //    ? this.Request.FilePath.ToLower()
                    //    : this.Request.FilePath.Replace(this.Request.ApplicationPath, "");
                    _pageUrl = this.Request.AppRelativeCurrentExecutionFilePath;
                }
                return this._pageUrl;
            }
            set
            {
                this._pageUrl = value;
            }
        }

        private string _actualFilePath = null;

        private string ActualFilePath
        {
            get
            {
                if (this._actualFilePath == null)
                {
                    _actualFilePath = this.Request.ApplicationPath == "/"
                        ? this.Request.FilePath.ToLower()
                        : this.Request.FilePath.Replace(this.Request.ApplicationPath, "");
                }
                return this._actualFilePath;
            }
        }

        protected void rptNav_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Adversary.DataLayer.SubTabNavigation a = (Adversary.DataLayer.SubTabNavigation)e.Item.DataItem;
            LinkButton l = (LinkButton)e.Item.FindControl("lbNavigate");
            Repeater r = (Repeater)e.Item.FindControl("rptSubNav");
            HtmlGenericControl d = (HtmlGenericControl)e.Item.FindControl("divSubNav");

            if (l.PostBackUrl.Replace("~", "").StartsWith(this.ActualFilePath))
            {
                l.CssClass += " activesubtab";
            }

            this.ExpandPostBackUrlFields(l);

            if (a != null)
            {
                List<Adversary.DataLayer.SubTabNavigation> subNavs = this.NavigationDataLayer.GetSubTabNavigationByParentSubTabNavigationID(a.SubTabNavigationID);
                if (subNavs != null && subNavs.Count > 0)
                {
                    l.CssClass = l.CssClass + " dropdown";
                    l.Attributes["rel"] = d.ClientID;
                    r.Visible = true;
                    d.Visible = true;
                    r.DataSource = subNavs;
                    r.DataBind();
                }
                else
                {
                    r.Visible = false;
                    d.Visible = false;
                }
            }
        }

        protected void rptSubNav_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            LinkButton l = (LinkButton)e.Item.FindControl("lbSubNavigate");
            this.ExpandPostBackUrlFields(l);
        }

        private void ExpandPostBackUrlFields(LinkButton lnk)
        {
            foreach (PropertyInfo prop in typeof(Adversary.Web.Controls.Navigation.ExpandProperties).GetProperties())
            {
                if (lnk.PostBackUrl.IndexOf("<" + prop.Name + ">") >= 0)
                {
                    string p = Convert.ToString(prop.GetValue(this.ExpandableProperties, null));
                    if (!string.IsNullOrWhiteSpace(p))
                    {
                        lnk.PostBackUrl = lnk.PostBackUrl.Replace("<" + prop.Name + ">", p);
                        lnk.Enabled = true;
                    }
                    else
                    {
                        lnk.Enabled = false;
                        break;
                    }
                }
            }

            if (lnk.PostBackUrl.StartsWith("javascript:"))
            {
                lnk.OnClientClick = lnk.PostBackUrl.Replace("~", this.Request.ApplicationPath == "/" ? "" : this.Request.ApplicationPath);
            }
        }

        public class ExpandProperties
        {
            private string _scrMemID = null;

            public string ScrMemID
            {
                get
                {
                    if (string.IsNullOrWhiteSpace(this._scrMemID))
                    {
                        this._scrMemID = HttpContext.Current.Request.QueryString["ScrMemID"];
                    }
                    return this._scrMemID;
                }
                set
                {
                    this._scrMemID = value;
                }
            }

            private string _requestID;

            public string RequestID
            {
                get
                {
                    if (string.IsNullOrWhiteSpace(this._requestID))
                    {
                        this._requestID = HttpContext.Current.Request.QueryString["RequestID"];
                    }
                    return this._requestID;
                }
                set
                {
                    this._requestID = value;
                }
            }
        }

    }
}