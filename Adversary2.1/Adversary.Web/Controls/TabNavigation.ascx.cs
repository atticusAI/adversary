﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Reflection;

namespace Adversary.Web.Controls
{
    public partial class TabNavigation : System.Web.UI.UserControl
    {
        public Adversary.Web.Pages.CurrentUser CurrentUser
        {
            get;
            set;
        }

        private DataLayer.Navigation _navigationDataLayer = null;

        public DataLayer.Navigation NavigationDataLayer
        {
            get
            {
                if (this._navigationDataLayer == null)
                {
                    this._navigationDataLayer = new DataLayer.Navigation();
                }
                return this._navigationDataLayer;
            }
        }

        public override void Dispose()
        {
            if (this._navigationDataLayer != null)
            {
                this._navigationDataLayer.Dispose();
                this._navigationDataLayer = null;
            }
            base.Dispose();
        }

        protected void rptNav_PreRender(object sender, EventArgs e)
        {
            this.rptNav.DataSource = this.NavigationDataLayer.GetPageNavigationByPageUrl(this.PageUrl);
            this.rptNav.DataBind();
        }

        private string _pageUrl = null;

        public string PageUrl
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this._pageUrl))
                {
                    //_pageUrl = this.Request.ApplicationPath == "/"
                    //    ? this.Request.FilePath.ToLower()
                    //    : this.Request.FilePath.Replace(this.Request.ApplicationPath, "");
                    _pageUrl = this.Request.AppRelativeCurrentExecutionFilePath;
                }
                return this._pageUrl;
            }
            set
            {
                this._pageUrl = value;
            }
        }

        protected void rptNav_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Adversary.DataLayer.PageNavigation a = (Adversary.DataLayer.PageNavigation)e.Item.DataItem;
            LinkButton l = (LinkButton)e.Item.FindControl("lbNavigate");
            Repeater r = (Repeater)e.Item.FindControl("rptSubNav");
            HtmlGenericControl d = (HtmlGenericControl)e.Item.FindControl("divSubNav");

            if (this.CurrentUser == null || !this.CurrentUser.HasAccessToPage(l.PostBackUrl))
            {
                l.Enabled = false;
            }
            
            if (l.PostBackUrl.Replace("~", "") == this.PageUrl)
            {
                l.CssClass += " activetab";
            }
        }
    }
}