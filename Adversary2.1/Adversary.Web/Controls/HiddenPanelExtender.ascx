﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HiddenPanelExtender.ascx.cs" Inherits="Adversary.Web.Controls.HiddenPanelExtender" %>
<script type="text/javascript">
    function <%=this.ClientID%>_toggle() {
        var shown = $('#<%=this.hid.ClientID%>').val();
        if (shown == 'False') {
            shown = 'True';
        }
        else {
            shown = 'False';
        }
        <%=this.ClientID%>_show(shown);
        $('#<%=this.hid.ClientID%>').val(shown);

        <%if (!string.IsNullOrWhiteSpace(this.Cookie)) {%>
        $.cookie('<%=this.Cookie%>', shown);
        <%}%>

        return false;
    }

    function <%=this.ClientID%>_show(shown) {
        if (shown == 'True') {
            $('#<%=this.PanelControlClientID%>').show();
            $('#<%=this.TargetControlClientID%>').removeClass('<%=this.HiddenCssClass%>').addClass('<%=this.ShownCssClass%>');
        }
        else {
            $('#<%=this.PanelControlClientID%>').hide();
            $('#<%=this.TargetControlClientID%>').addClass('<%=this.HiddenCssClass%>').removeClass('<%=this.ShownCssClass%>');
        }
        return false;
    }

    $(document).ready(function () {
        $('#<%=this.TargetControlClientID%>').click(function () {
            <%=this.ClientID%>_toggle();
        });

        <%if (!string.IsNullOrWhiteSpace(this.Cookie)) {%>
        var cok = $.cookie('<%=this.Cookie%>');
        if (cok != null) {
            $('#<%=this.hid.ClientID%>').val(cok);
        }

        <%=this.ClientID%>_show($('#<%=this.hid.ClientID%>').val());
        <%}%>
    });
</script>
<asp:HiddenField ID="hid" runat="server" />
