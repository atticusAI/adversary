﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Adversary.Web.Controls
{
    public partial class MessageBox : System.Web.UI.UserControl
    {
        public enum MessageBoxButtons
        {
            OK,                 /// The message box contains an OK button.  This is the DEFAULT.
            OKCancel,           /// The message box contains OK and Cancel buttons.
            AbortRetryIgnore,   /// The message box contains Abort, Retry, and Ignore buttons.
            YesNoCancel,        /// The message box contains Yes, No, and Cancel buttons.
            YesNo,              /// The message box contains Yes and No buttons.
            RetryCancel         /// The message box contains Retry and Cancel buttons.
        }

        public enum MessageBoxIcon
        {
            None,
            Information, 
            Warning, 
            Error
        }

        public string CancelButton
        {
            get
            {
                string button = "";
                if (this.mpxMessageBox.CancelControlID == this.btnAbort.ID)
                {
                    button = "Abort";
                }
                else if (this.mpxMessageBox.CancelControlID == this.btnCancel.ID)
                {
                    button = "Cancel";
                }
                else if (this.mpxMessageBox.CancelControlID == this.btnIgnore.ID)
                {
                    button = "Ignore";
                }
                else if (this.mpxMessageBox.CancelControlID == this.btnNo.ID)
                {
                    button = "No";
                }
                else if (this.mpxMessageBox.CancelControlID == this.btnOK.ID)
                {
                    button = "OK";
                }
                else if (this.mpxMessageBox.CancelControlID == this.btnRetry.ID)
                {
                    button = "Retry";
                }
                else if (this.mpxMessageBox.CancelControlID == this.btnYes.ID)
                {
                    button = "Yes";
                }
                return button;
            }

            set
            {
                switch(value.ToLowerInvariant())
                {
                    case "abort":
                        this.mpxMessageBox.CancelControlID = this.btnAbort.ID;
                        this.btnAbort.Click -= this.btnAbort_Click;
                        break;
                    case "cancel":
                        this.mpxMessageBox.CancelControlID = this.btnCancel.ID;
                        this.btnCancel.Click -= this.btnCancel_Click;
                        break;
                    case "ignore":
                        this.mpxMessageBox.CancelControlID = this.btnIgnore.ID;
                        this.btnIgnore.Click -= this.btnIgnore_Click;
                        break;
                    case "no":
                        this.mpxMessageBox.CancelControlID = this.btnNo.ID;
                        this.btnNo.Click -= this.btnNo_Click;
                        break;
                    case "ok":
                        this.mpxMessageBox.CancelControlID = this.btnOK.ID;
                        this.btnOK.Click -= this.btnNo_Click;
                        break;
                    case "retry":
                        this.mpxMessageBox.CancelControlID = this.btnRetry.ID;
                        this.btnRetry.Click -= this.btnRetry_Click;
                        break;
                    case "yes":
                        this.mpxMessageBox.CancelControlID = this.btnYes.ID;
                        this.btnYes.Click -= this.btnYes_Click;
                        break;
                }
            }
        }

        public string CssClass
        {
            get
            {
                return this.pnlMessageBox.CssClass;
            }
            set
            {
                this.pnlMessageBox.CssClass = value;
            }
        }

        public string BackgroundCssClass
        {
            get
            {
                return this.mpxMessageBox.BackgroundCssClass;
            }
            set
            {
                this.mpxMessageBox.BackgroundCssClass = value;
            }
        }

        public string Message
        {
            get
            {
                return this.lblMessage.Text;
            }

            set
            {
                this.lblMessage.Text = value;
            }
        }

        public string BehaviorID
        {
            get
            {
                return this.ClientID + "_behMessageBox";
            }
        }

        public void Show()
        {
            this.mpxMessageBox.Show();
        }

        private MessageBoxButtons _buttons = MessageBoxButtons.OK;

        public MessageBoxButtons Buttons
        {
            get
            {
                return this._buttons;
            }

            set
            {
                this._buttons = value;
            }
        }

        private MessageBoxIcon _icon = MessageBoxIcon.None;

        public MessageBoxIcon Icon
        {
            get
            {
                return this._icon;
            }
            set
            {
                this._icon = value;
            }
        }


        public event EventHandler AbortClick = null;

        protected void btnAbort_Click(object sender, EventArgs e)
        {
            if (this.AbortClick != null)
            {
                this.AbortClick(sender, e);
            }
        }

        public event EventHandler CancelClick = null;

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (this.CancelClick != null)
            {
                this.CancelClick(sender, e);
            }
        }

        public event EventHandler IgnoreClick = null;

        protected void btnIgnore_Click(object sender, EventArgs e)
        {
            if (this.IgnoreClick != null)
            {
                this.IgnoreClick(sender, e);
            }
        }

        public event EventHandler NoClick = null;

        protected void btnNo_Click(object sender, EventArgs e)
        {
            if (this.NoClick != null)
            {
                this.NoClick(sender, e);
            }
        }

        public event EventHandler OKClick = null;

        protected void btnOK_Click(object sender, EventArgs e)
        {
            if (this.OKClick != null)
            {
                this.OKClick(sender, e);
            }
        }

        public event EventHandler RetryClick = null;

        protected void btnRetry_Click(object sender, EventArgs e)
        {
            if (this.RetryClick != null)
            {
                this.RetryClick(sender, e);
            }
        }

        public event EventHandler YesClick = null;

        protected void btnYes_Click(object sender, EventArgs e)
        {
            if (this.YesClick != null)
            {
                this.YesClick(sender, e);
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            this.SetupButtons();
            this.SetupIcon();
            this.SetupMPX();
        }

        private string _errorCssClass = "erroricon";

        public string ErrorCssClass
        {
            get
            {
                return this._errorCssClass;
            }
            set
            {
                this._errorCssClass = value;
            }
        }

        private string _informationCssClass = "infoicon";

        public string InformationCssClass
        {
            get
            {
                return this._informationCssClass;
            }
            set
            {
                this._informationCssClass = value;
            }
        }

        private string _noneCssClass = "noicon";

        public string NoneCssClass
        {
            get
            {
                return this._noneCssClass;
            }
            set
            {
                this._noneCssClass = value;
            }
        }

        private string _warningCssClass = "warnicon";

        public string WarningCssClass
        {
            get
            {
                return this._warningCssClass;
            }
            set
            {
                this._warningCssClass = value;
            }
        }
        
        private void SetupIcon()
        {
            switch (this.Icon)
            {
                case MessageBoxIcon.Error:
                    this.lblMessage.CssClass = this.ErrorCssClass;
                    break;
                case MessageBoxIcon.Information:
                    this.lblMessage.CssClass = this.InformationCssClass;
                    break;
                case MessageBoxIcon.None:
                    this.lblMessage.CssClass = this.NoneCssClass;
                    break;
                case MessageBoxIcon.Warning:
                    this.lblMessage.CssClass = this.WarningCssClass;
                    break;
            }
        }

        private void SetupMPX()
        {
            this.mpxMessageBox.BehaviorID = this.ClientID + "_behMessageBox";
        }

        private void SetupButtons()
        {
            switch (Buttons)
            {
                case MessageBoxButtons.AbortRetryIgnore:
                    this.btnAbort.Visible = true;
                    this.btnCancel.Visible = false;
                    this.btnIgnore.Visible = true;
                    this.btnNo.Visible = false;
                    this.btnOK.Visible = false;
                    this.btnRetry.Visible = true;
                    this.btnYes.Visible = false;
                    break;
                case MessageBoxButtons.OK:
                    this.btnAbort.Visible = false;
                    this.btnCancel.Visible = false;
                    this.btnIgnore.Visible = false;
                    this.btnNo.Visible = false;
                    this.btnOK.Visible = true;
                    this.btnRetry.Visible = false;
                    this.btnYes.Visible = false;
                    break;
                case MessageBoxButtons.OKCancel:
                    this.btnAbort.Visible = false;
                    this.btnCancel.Visible = true;
                    this.btnIgnore.Visible = false;
                    this.btnNo.Visible = false;
                    this.btnOK.Visible = true;
                    this.btnRetry.Visible = false;
                    this.btnYes.Visible = false;
                    break;
                case MessageBoxButtons.RetryCancel:
                    this.btnAbort.Visible = false;
                    this.btnCancel.Visible = true;
                    this.btnIgnore.Visible = false;
                    this.btnNo.Visible = false;
                    this.btnOK.Visible = false;
                    this.btnRetry.Visible = true;
                    this.btnYes.Visible = false;
                    break;
                case MessageBoxButtons.YesNo:
                    this.btnAbort.Visible = false;
                    this.btnCancel.Visible = false;
                    this.btnIgnore.Visible = false;
                    this.btnNo.Visible = true;
                    this.btnOK.Visible = false;
                    this.btnRetry.Visible = false;
                    this.btnYes.Visible = true;
                    break;
                case MessageBoxButtons.YesNoCancel:
                    this.btnAbort.Visible = false;
                    this.btnCancel.Visible = true;
                    this.btnIgnore.Visible = false;
                    this.btnNo.Visible = true;
                    this.btnOK.Visible = false;
                    this.btnRetry.Visible = false;
                    this.btnYes.Visible = true;
                    break;

            }
        }

    }
}