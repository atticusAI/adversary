﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Adversary.Web.Controls
{
    public partial class HelpLabel : System.Web.UI.UserControl
    {
        public string HelpText
        {
            get
            {
                return this.s.InnerText;
            }
            set
            {
                this.s.InnerText = value;
            }
        }

        public string HelpHtml
        {
            get
            {
                return this.s.InnerHtml;
            }
            set
            {
                this.s.InnerHtml = value;
            }
        }

        public string Text
        {
            get
            {
                return this.l.InnerText;
            }
            set
            {
                this.l.InnerText = value;
            }
        }

        public string Html
        {
            get
            {
                return this.l.InnerHtml;
            }
            set
            {
                this.l.InnerHtml = value;
            }
        }

        private string _associatedControlID = null;

        public string AssociatedControlID
        {
            get
            {
                return this._associatedControlID;
            }
            set
            {
                this._associatedControlID = value;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.l.Attributes["onclick"] = string.Format("$('#{0}').toggle(); return false;", this.s.ClientID);
            this.s.Attributes["onclick"] = "$(this).toggle();";

            if (this.AssociatedControlID != null)
            {
                Control ctrl = this.NamingContainer.FindControl(this.AssociatedControlID);
                if (ctrl != null)
                {
                    this.l.Attributes["for"] = ctrl.ClientID;
                }
            }
        }
    }
}
