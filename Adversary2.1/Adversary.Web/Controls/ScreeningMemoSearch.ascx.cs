﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Adversary.Web.Controls
{
    public partial class ScreeningMemoSearch : System.Web.UI.UserControl
    {
        private bool _trim = true;

        public bool Trim
        {
            get
            {
                return this._trim;
            }
            set
            {
                this._trim = value;
            }
        }

        public string ClientServiceMethod
        {
            get
            {
                return this.cacClient.ServiceMethod;
            }
            set
            {
                this.cacClient.ServiceMethod = value;
            }
        }

        public string MatterServiceMethod
        {
            get
            {
                return this.actMatter.ServiceMethod;
            }
            set
            {
                this.actMatter.ServiceMethod = value;
            }
        }

        public string ClientMatterServiceMethod
        {
            get
            {
                return this.actClientMatter.ServiceMethod;
            }
            set
            {
                this.actClientMatter.ServiceMethod = value;
            }
        }

        public string TimekeeperServiceMethod
        {
            get
            {
                return this.pacAttorney.ServiceMethod;
            }
            set
            {
                this.pacAttorney.ServiceMethod = value;
                this.pacInput.ServiceMethod = value;
            }
        }

        public string ScreeningMemoServiceMethod
        {
            get
            {
                return this.actMemoNo.ServiceMethod;
            }
            set
            {
                this.actMemoNo.ServiceMethod = value;
            }
        }

        public string SelectedClientCode
        {
            get
            {
                return this.cacClient.SelectedClientCode;
            }
        }

        public string SelectedMatter
        {
            get
            {
                return this.actMatter.Text;
            }
        }

        public string SelectedClientMatter
        {
            get
            {
                return this.actClientMatter.Text;
            }
        }

        public string SelectedAttorneyCode
        {
            get
            {
                return this.pacAttorney.SelectedEmployeeCode;
            }
        }


        public string SelectedEmployeeCode
        {
            get
            {
                return this.pacInput.SelectedEmployeeCode;
            }
        }

        public string SelectedMemoNumber
        {
            get
            {
                return this.actMemoNo.Text;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.SearchClick != null)
            {
                this.btnSearch.Click += new EventHandler(btnSearch_Click);
            }

            if (this.ClearClick != null)
            {
                this.btnClear.Click += new EventHandler(btnClear_Click);
            }
        }

        public event EventHandler SearchClick = null;

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (this.SearchClick != null)
            {
                if (this.Trim)
                {
                    this.actClientMatter.Text = this.actClientMatter.Text.Trim();
                    this.actMatter.Text = this.actMatter.Text.Trim();
                    this.actMemoNo.Text = this.actMemoNo.Text.Trim();
                }
                this.SearchClick(sender, e);
            }
        }

        public event EventHandler ClearClick = null;

        protected void btnClear_Click(object sender, EventArgs e)
        {
            if (this.ClearClick != null)
            {
                this.ClearClick(sender, e);
            }
        }
    }
}