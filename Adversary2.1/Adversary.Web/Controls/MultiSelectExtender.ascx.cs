﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VCRequestForm.Controls
{
    public partial class MultiSelectExtender : System.Web.UI.UserControl
    {
        /// <summary>
        /// the required target control
        /// </summary>
        public string TargetControlID{ get; set; }

        private bool _showHeader = true;

        /// <summary>
        /// multilsect show header option
        /// </summary>
        public bool ShowHeader{
            get { return this._showHeader; }
            set { this._showHeader = value; }
        }

        /// <summary>
        /// cooked show header for use in javascript 
        /// </summary>
        public string jsShowHeader
        {
            get { return this.ShowHeader.ToString().ToLower(); }
        }

        private string _height = "175";

        /// <summary>
        /// multiselect height
        /// </summary>
        public string Height 
        {
            get { return this._height; }
            set { this._height = value; }
        }

        private string _minWidth = "225";

        /// <summary>
        /// multiselect minimum width
        /// </summary>
        public string MinWidth
        {
            get { return this._minWidth; }
            set { this._minWidth = value; }
        }

        private string _checkAllText = "Check all";

        /// <summary>
        /// text for "check all"
        /// </summary>
        public string CheckAllText
        {
            get { return this._checkAllText; }
            set { this._checkAllText = value; }
        }

        private string _uncheckAllText = "Uncheck all";

        /// <summary>
        /// text for "uncheck all"
        /// </summary>
        public string UncheckAllText
        {
            get { return this._uncheckAllText; }
            set { this._uncheckAllText = value; }
        }

        private string _noneSelectedText = "Select options";

        /// <summary>
        /// text displayed when none selected
        /// </summary>
        public string NoneSelectedText
        {
            get { return this._noneSelectedText; }
            set { this._noneSelectedText = value; }
        }

        private string _selectedText = "# selected";

        /// <summary>
        /// text displayed when some selected (# expanded to number selected)
        /// </summary>
        public string SelectedText
        {
            get { return this._selectedText; }
            set { this._selectedText = value; }
        }

        private int _selectedList = 0;

        /// <summary>
        /// 
        /// </summary>
        public int SelectedList
        {
            get { return this._selectedList; }
            set { this._selectedList = value; }
        }

        /// <summary>
        /// selected list cooked for javascript
        /// </summary>
        public string jsSelectedList
        {
            get { return this.SelectedList < 0 ? "false" : this.SelectedList.ToString(); }
        }

        private string _showEffect = "";

        /// <summary>
        /// the animation to use when showing
        /// </summary>
        public string ShowEffect
        {
            get { return this._showEffect; }
            set { this._showEffect = value; }
        }

        private string _hideEffect = "";

        /// <summary>
        /// the animation to use when hiding
        /// </summary>
        public string HideEffect
        {
            get { return this._hideEffect; }
            set { this._hideEffect = value; }
        }

        private bool _autoOpen = false;

        /// <summary>
        /// open multiselect automagically
        /// </summary>
        public bool AutoOpen
        {
            get { return this._autoOpen; }
            set { this._autoOpen = value; }
        }

        /// <summary>
        /// cooked autoopen for javascript
        /// </summary>
        public string jsAutoOpen
        {
            get { return this.AutoOpen.ToString().ToLower(); }
        }

        private bool _multiple = true;

        /// <summary>
        /// allow multiple
        /// </summary>
        public bool Multiple
        {
            get { return this._multiple; }
            set { this._multiple = value; }
        }

        /// <summary>
        /// cooked multiple for js
        /// </summary>
        public string jsMultiple
        {
            get { return this.Multiple.ToString().ToLower(); }
        }

        /// <summary>
        /// the client id of the target control
        /// </summary>
        public string TargetControlClientID
        {
            get
            {
                string id = "";
                Control ctrl = this.NamingContainer.FindControl(this.TargetControlID);
                if (ctrl != null)
                {
                    id = ctrl.ClientID;
                }
                return id;
            }
        }

        /// <summary>
        /// the name of the target control 
        /// </summary>
        public string TargetControlName
        {
            get
            {
                string name = "";
                Control ctrl = this.NamingContainer.FindControl(this.TargetControlID);
                if (ctrl != null)
                {
                    name = ctrl.UniqueID;
                }
                return name;
            }
        }

        private bool _groupOptions = false;

        /// <summary>
        /// show options as groups
        /// </summary>
        public bool GroupOptions
        {
            get { return this._groupOptions; }
            set { this._groupOptions = value; }
        }

        private string _groupSeparator = "";

        /// <summary>
        /// the seperator between the group name and the option name
        /// e.g. "GroupName|OptionName"
        /// </summary>
        public string GroupSeparator
        {
            get { return this._groupSeparator; }
            set { this._groupSeparator = value; }
        }

        private bool _autoPostBack = false;

        /// <summary>
        /// autopostback after options are selected
        /// exclusive of onclientclosed
        /// </summary>
        public bool AutoPostBack
        {
            get { return this._autoPostBack; }
            set { this._autoPostBack = value; }
        }

        private string _onClientClosed = "";

        /// <summary>
        /// client side after close event
        /// exclusive of autopostback
        /// </summary>
        public string OnClientClosed
        {
            get { return this._onClientClosed; }
            set { this._onClientClosed = value; }
        }
    }
}
