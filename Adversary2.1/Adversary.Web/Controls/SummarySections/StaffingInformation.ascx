﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StaffingInformation.ascx.cs"
    Inherits="Adversary.Web.Controls.SummarySections.StaffingInformation" %>
<asp:Panel ID="pnlStaff" runat="server">
    <table class="frv" cellspacing="0" style="border-collapse: collapse;">
        <tbody>
            <tr class="frvrow">
                <td colspan="2">
                    <a class="sum">Staffing Information</a>
                    <table class="sum">
                        <tr>
                            <td>
                                Billing Attorney
                            </td>
                            <td>
                                <asp:Label ID="lblBiller" runat="server" OnPreRender="lblBiller_PreRender" />
                            </td>
                        </tr>
                        <asp:Repeater ID="rptStaffInformation" runat="server" OnPreRender="rptStaffInformation_PreRender">
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        Personnel
                                    </td>
                                    <td>
                                        <%#GetPersonName(Eval("PersonnelID"))%>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                    <br />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Panel>
