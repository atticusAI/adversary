﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Adversary.Web.Controls.SummarySections
{
    public partial class ReferringParty : SummarySectionBase
    {
        protected void frmReferringParty_PreRender(object sender, EventArgs e)
        {
            ((FormView)sender).DataSource = Memos;
            ((FormView)sender).DataBind();

            Label lblNewClientOriginationComments = (Label)frmReferringParty.FindControl("lblNewClientOriginationComments");
            if (Memos[0].NewClients.Count > 0)
            {
                lblNewClientOriginationComments.Text = Memos[0].NewClients[0].OriginationComments;
            }
        }

        protected void rptReferringParty_PreRender(object sender, EventArgs e)
        {
            if (Memos != null && Memos.Count > 0)
            {
                ((Repeater)sender).DataSource = new List<Adversary.DataLayer.Party>(){
                    Memos[0].Parties.Where(p => p.PartyRelationshipCode == "902").FirstOrDefault()
                };
                ((Repeater)sender).DataBind();
            }
        }
    }
}