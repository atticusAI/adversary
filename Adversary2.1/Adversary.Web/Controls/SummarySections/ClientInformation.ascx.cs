﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Adversary.Web.Controls.SummarySections
{
    public partial class ClientInformation : SummarySectionBase 
    {
        protected void frmClient_PreRender(object sender, EventArgs e)
        {
            ((FormView)sender).DataSource = Memos;
            ((FormView)sender).DataBind();
        }

        protected string GetSectionName()
        {
            string sectionName = "Client Information";
            if (this.Memos[0].ScrMemType == 6)
            {
                sectionName = "Client Type";
            }
            else if (this.PrintMode())
            {
                sectionName = "Matter Info";
            }
            return sectionName;
        }

        protected bool ShowBankruptcyRow()
        {
            bool show = false;
            show = Memos[0].ScrMemType != 5 && Memos[0].ScrMemType != 6;
            return show;
        }
    }
}