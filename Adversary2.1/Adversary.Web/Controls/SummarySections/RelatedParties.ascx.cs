﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.DataLayer;

namespace Adversary.Web.Controls.SummarySections
{
    public partial class RelatedParties : SummarySectionBase 
    {
        protected void rptRelatedParties_PreRender(object sender, EventArgs e)
        {
            if (Memos != null && Memos.Count > 0)
            {
                //((Repeater)sender).DataSource = Memos[0].Parties.Where(p => p.PartyRelationshipCode == "202"
                //    || p.PartyRelationshipCode == "203"
                //    || p.PartyRelationshipCode == "222");
                //bugfix 7/17/2012 A Reimer - not pulling all parties.
                ((Repeater)sender).DataSource = Memos[0].Parties.Where(p => Adversary.DataLayer.AdversaryData.RelatedPartyCodes.Contains(p.PartyRelationshipCode));

                ((Repeater)sender).DataBind();
            }
        }
    }
}
