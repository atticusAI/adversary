﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Adversary.DataLayer;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Adversary.Web.Controls.SummarySections
{
    public class SummarySectionBase : System.Web.UI.UserControl
    {
        private ScreeningMemoSummary ThisScreeningMemoSummary = null;

        public List<Adversary.DataLayer.Memo> Memos
        {
            get;
            set;
        }
        
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (!(this.NamingContainer is ScreeningMemoSummary))
            {
                throw new InvalidOperationException("Initialization Failed!  This control can only be used within the ScreeningMemoSummary control.");
            }
            else
            {
                this.ThisScreeningMemoSummary = (ScreeningMemoSummary)this.NamingContainer;
            }
        }

        protected string YesNo(object o)
        {
            return ThisScreeningMemoSummary.YesNo(o);
        }

        protected string GetBiller(object o)
        {
            return ThisScreeningMemoSummary.GetBiller(o);
        }

        protected string GetPersonName(object o)
        {
            return ThisScreeningMemoSummary.GetPersonName(o);
        }

        protected string GetClientDisplayName()
        {
            return ThisScreeningMemoSummary.GetClientDisplayName();
        }

        protected string GetPracticeTypeName(object o)
        {
            return ThisScreeningMemoSummary.GetPracticeTypeName(o);
        }

        protected string GetOfficeName(object o)
        {
            return ThisScreeningMemoSummary.GetOfficeName(o);
        }

        protected string NewMatterOnly(object o)
        {
            return ThisScreeningMemoSummary.NewMatterOnly(o);
        }

        protected string DownloadUrl(object o)
        {
            return ThisScreeningMemoSummary.DownloadUrl(o);
        }

        protected string SmallRetainerYesNo(object o)
        {
            return ThisScreeningMemoSummary.SmallRetainerYesNo(o);
        }

        protected bool SmallRetainer(object o)
        {
            return ThisScreeningMemoSummary.SmallRetainer(o);
        }

        protected bool HasRetainerDesc(object o)
        {
            return ThisScreeningMemoSummary.HasRetainerDesc(o);
        }

        protected bool PrintMode()
        {
            return ThisScreeningMemoSummary.PrintMode();
        }

        protected bool HasAdditionalAttachments()
        {
            return ThisScreeningMemoSummary.HasAdditionalAttachments();
        }

        protected bool IsProBono()
        {
            return ThisScreeningMemoSummary.IsProBono();
        }

        protected string GetProBonoType()
        {
            return ThisScreeningMemoSummary.ProBonoType();
        }

        protected string GetNoConflictReason()
        {
            return ThisScreeningMemoSummary.GetNoConflictReason();
        }

        protected string GetNoEngagementReason()
        {
            return ThisScreeningMemoSummary.GetNoEngagementReason();
        }

        protected string GetLegalWorkFor()
        {
            return ThisScreeningMemoSummary.GetLegalWorkFor();
        }

        protected string GetNoRejectReason()
        {
            return ThisScreeningMemoSummary.GetNoRejectReason();
        }

        protected void rpt_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Repeater rpt = (Repeater)sender;
            if (rpt.Items == null || rpt.Items.Count < 1)
            {
                if (e.Item.ItemType == ListItemType.Footer)
                {
                    HtmlTableRow tr = (HtmlTableRow)e.Item.FindControl("trEmpty");
                    if (tr != null)
                    {
                        tr.Visible = true;
                    }

                    Label lbl = (Label)e.Item.FindControl("lblEmpty");
                    if (lbl != null)
                    {
                        lbl.Visible = true;
                    }

                    HtmlTable tbl = (HtmlTable)e.Item.FindControl("tblEmpty");
                    if (tbl != null)
                    {
                        tbl.Visible = true;
                    }
                }
            }
        }
    }
}