﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MatterInformation.ascx.cs"
    Inherits="Adversary.Web.Controls.SummarySections.MatterInformation" %>
<asp:Panel runat="server" ID="pnlMatter">
    <asp:FormView ID="frmMatter" runat="server" DefaultMode="ReadOnly" OnPreRender="frmMatter_PreRender">
        <EmptyDataTemplate>
            <a class="sum">Matter Information</a>
            <table class="sum">
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
            </table>
        </EmptyDataTemplate>
        <ItemTemplate>
            <a class="sum">Matter Information</a>
            <table class="sum">
                <tr>
                    <td>
                        Matter Name
                    </td>
                    <td>
                        <%#Eval("MatterName")%>
                    </td>
                </tr>
                <tr>
                    <td>
                        Work Description
                    </td>
                    <td>
                        <%#Eval("WorkDesc")%>
                    </td>
                </tr>
                <tr>
                    <td>
                        Practice Type Code
                    </td>
                    <td>
                        <%#GetPracticeTypeName(Eval("PracCode"))%>
                    </td>
                </tr>
                <tr id="trFees" runat="server" visible='<%#!IsProBono()%>'>
                    <td>
                        Estimate of Fees
                    </td>
                    <td>
                        <%#Eval("EstFees")%>
                    </td>
                </tr>
                <tr id="trCntFee" runat="server" visible='<%#!IsProBono()%>'>
                    <td>
                        Contingency Fee?
                    </td>
                    <td>
                        <%#YesNo(Eval("ContingFee"))%>
                    </td>
                </tr>
                <tr id="trNDrive" runat="server" visible='<%#!IsProBono()%>'>
                    <td>
                        Create an N Drive folder for this client?
                    </td>
                    <td>
                        <%#YesNo(Eval("NDrive"))%>
                    </td>
                </tr>
            </table>
            <br />
        </ItemTemplate>
    </asp:FormView>
</asp:Panel>
