﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Adversary.Web.Controls.SummarySections
{
    public partial class OpposingCounsel : SummarySectionBase
    {
        protected void frmOpposingCounsel_PreRender(object sender, EventArgs e)
        {
            ((FormView)sender).DataSource = Memos;
            ((FormView)sender).DataBind();
        }

        protected void rptOpposingCounsel_PreRender(object sender, EventArgs e)
        {
            if (Memos != null && Memos.Count > 0)
            {
                ((Repeater)sender).DataSource = Memos[0].OpposingCounsels;
                ((Repeater)sender).DataBind();
            }
        }
    }
}