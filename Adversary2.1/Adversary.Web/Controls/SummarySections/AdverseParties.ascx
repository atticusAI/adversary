﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdverseParties.ascx.cs"
    Inherits="Adversary.Web.Controls.SummarySections.AdverseParties" %>
<asp:Panel ID="pnlAdverseParties" runat="server">
    <table class="frv" cellspacing="0" style="border-collapse: collapse;">
        <tbody>
            <tr class="frvrow">
                <td colspan="2">
                    <a class="sum">Adverse Parties</a>
                    <table class="sum">
                        <asp:Repeater ID="rptAdverseParties" runat="server" OnPreRender="rptAdverseParties_PreRender" OnItemDataBound="rpt_ItemDataBound">
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <%#Eval("PartyRelationshipName")%>
                                        (<%#Eval("PartyRelationshipCode")%>)
                                    </td>
                                    <td>
                                        <%#Eval("PartyFName")%>
                                        <%#Eval("PartyMName")%>
                                        <%#Eval("PartyLName")%>
                                        <%#Eval("PartyOrganization")%>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                <tr id="trEmpty" runat="server" visible="false">
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                            </FooterTemplate>
                        </asp:Repeater>
                    </table>
                    <br />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Panel>
