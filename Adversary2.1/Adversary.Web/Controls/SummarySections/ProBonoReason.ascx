﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProBonoReason.ascx.cs"
    Inherits="Adversary.Web.Controls.SummarySections.ProBonoReason" %>
<asp:Panel ID="pnlProBonoReason" runat="server">
    <asp:FormView ID="frmProBonoReason" runat="server" OnPreRender="frmProBonoReason_PreRender">
        <EmptyDataTemplate>
            <a class="sum">Reason</a>
            <table class="sum">
                <tr>
                    <td colspan="2">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </EmptyDataTemplate>
        <ItemTemplate>
            <a class="sum">Reason</a>
            <table class="sum">
                <tr>
                    <td>
                        Please state why you feel that this memo is Pro Bono?
                    </td>
                    <td>
                        <%#Eval("ProBonoReason")%>
                    </td>
                </tr>
            </table>
        </ItemTemplate>
    </asp:FormView>
</asp:Panel>
