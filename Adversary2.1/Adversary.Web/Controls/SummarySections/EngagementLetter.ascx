﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EngagementLetter.ascx.cs"
    Inherits="Adversary.Web.Controls.SummarySections.EngagementLetter" %>
<asp:Panel ID="pnlEngagementLetter" runat="server">
    <table class="frv" cellspacing="0" style="border-collapse: collapse;">
        <tbody>
            <tr class="frvrow">
                <td colspan="2">
                    <a class="sum">Engagement Letter</a>
                    <table class="sum">
                        <tr id="trEngage" runat="server">
                            <td>
                                List of attached Engagement Letter files
                            </td>
                            <td>
                                <asp:Repeater ID="rptEngagementLetter" runat="server" OnPreRender="rptEngagementLetter_PreRender">
                                    <ItemTemplate>
                                        <asp:HyperLink CssClass="attach" ID="hypDownload" runat="server" NavigateUrl='<%#DownloadUrl(Eval("AttachmentID"))%>'
                                            Text='<%#Eval("Description")%>' Target="_blank" />
                                        (<%#Eval("FileName")%>)
                                        <br />
                                    </ItemTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                        <tr id="trEmpty" runat="server" onprerender="trEmpty_PreRender">
                            <td>
                                Reason why an Engagement Letter will not be sent
                            </td>
                            <td>
                                <%=GetNoEngagementReason()%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                List of attached Policy files
                            </td>
                            <td>
                                <asp:Repeater ID="rptPolicyFiles" runat="server" OnPreRender="rptPolicyFiles_PreRender" OnItemDataBound="rpt_ItemDataBound">
                                    <ItemTemplate>
                                        <asp:HyperLink CssClass="attach" ID="hypDownload" runat="server" NavigateUrl='<%#DownloadUrl(Eval("AttachmentID"))%>'
                                            Text='<%#Eval("Description")%>' Target="_blank" />
                                        (<%#Eval("FileName")%>)
                                        <br />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblEmpty" runat="server" Visible="false"><strong>- No Data Available -</strong></asp:Label>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                    <br />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Panel>
