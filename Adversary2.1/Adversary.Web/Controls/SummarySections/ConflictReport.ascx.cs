﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Adversary.Web.Controls.SummarySections
{
    public partial class ConflictReport : SummarySectionBase 
    {
        protected void rptConflictReport_PreRender(object sender, EventArgs e)
        {
            if (Memos != null && Memos.Count > 0)
            {
                ((Repeater)sender).DataSource = Memos[0].Attachments.Where(a => a.Description == "Conflict Report");
                ((Repeater)sender).DataBind();
            }
        }

        protected void tblEmpty_PreRender(object sender, EventArgs e)
        {
            if (rptConflictReport.Items == null || rptConflictReport.Items.Count < 1)
            {
                tblConflict.Visible = false;
                tblEmpty.Visible = true;
            }
            else
            {
                tblConflict.Visible = true;
                tblEmpty.Visible = false;
            }
        }
    }
}