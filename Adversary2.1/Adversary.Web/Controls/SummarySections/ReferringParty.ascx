﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReferringParty.ascx.cs"
    Inherits="Adversary.Web.Controls.SummarySections.ReferringParty" %>
<asp:Panel ID="pnlReferringParty" runat="server">
    <asp:FormView ID="frmReferringParty" runat="server" DefaultMode="ReadOnly" OnPreRender="frmReferringParty_PreRender">
        <EmptyDataTemplate>
            <a class="sum">Referring Party</a>
            <table class="sum">
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
            </table>
        </EmptyDataTemplate>
        <ItemTemplate>
            <a class="sum">Referring Party</a>
            <table class="sum">
                <tr>
                    <td>
                        Please Enter How This Client Came to Select H&amp;H for this work
                    </td>
                    <td>
                        <%--<%#Eval("NewClients[0].OriginationComments")%>--%>
                        <asp:Label ID="lblNewClientOriginationComments" runat="server" />
                    </td>
                </tr>
                <asp:Repeater ID="rptReferringParty" runat="server" OnPreRender="rptReferringParty_PreRender">
                    <ItemTemplate>
                        <tr>
                            <td>
                                Party Type
                            </td>
                            <td>
                                <%#Eval("PartyType")%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                First Name
                            </td>
                            <td>
                                <%#Eval("PartyFName")%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Middle Name
                            </td>
                            <td>
                                <%#Eval("PartyMName")%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Last Name
                            </td>
                            <td>
                                <%#Eval("PartyLName")%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Address 1
                            </td>
                            <td>
                                <%#Eval("PartyAddressInfos[0].Address_Line_1")%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Address 2
                            </td>
                            <td>
                                <%#Eval("PartyAddressInfos[0].Address_Line_2")%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Address 3
                            </td>
                            <td>
                                <%#Eval("PartyAddressInfos[0].Address_Line_3")%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                City
                            </td>
                            <td>
                                <%#Eval("PartyAddressInfos[0].City")%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                State
                            </td>
                            <td>
                                <%#Eval("PartyAddressInfos[0].State")%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Zip
                            </td>
                            <td>
                                <%#Eval("PartyAddressInfos[0].Zip")%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Country
                            </td>
                            <td>
                                <%#Eval("PartyAddressInfos[0].Country")%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Phone
                            </td>
                            <td>
                                <%#Eval("PartyAddressInfos[0].Phone")%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Fax
                            </td>
                            <td>
                                <%#Eval("PartyAddressInfos[0].Fax")%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Email
                            </td>
                            <td>
                                <%#Eval("PartyAddressInfos[0].Email")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
            <br />
        </ItemTemplate>
    </asp:FormView>
</asp:Panel>
