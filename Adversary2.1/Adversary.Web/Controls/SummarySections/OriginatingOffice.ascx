﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OriginatingOffice.ascx.cs"
    Inherits="Adversary.Web.Controls.SummarySections.OriginatingOffice" %>
<asp:Panel ID="pnlOriginatingOffice" runat="server">
    <asp:FormView ID="frmOriginatingOffice" runat="server" DefaultMode="ReadOnly" OnPreRender="frmOriginatingOffice_PreRender">
        <EmptyDataTemplate>
            <a class="sum">Originating Office</a>
            <table class="sum">
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
            </table>
        </EmptyDataTemplate>
        <ItemTemplate>
            <a class="sum">Originating Office</a>
            <table class="sum">
                <tr>
                    <td>
                        Originating Office
                    </td>
                    <td>
                        <%#GetOfficeName(Eval("OrigOffice"))%>
                    </td>
                </tr>
            </table>
            <br />
        </ItemTemplate>
    </asp:FormView>
</asp:Panel>
