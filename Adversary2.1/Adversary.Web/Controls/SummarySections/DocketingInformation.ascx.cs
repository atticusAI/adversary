﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Adversary.Web.Controls.SummarySections
{
    public partial class DocketingInformation : SummarySectionBase 
    {
        protected void frmDocketing_PreRender(object sender, EventArgs e)
        {
            ((FormView)sender).DataSource = Memos;
            ((FormView)sender).DataBind();
        }

        protected void rptDocketing_PreRender(object sender, EventArgs e)
        {
            if (Memos != null && Memos.Count > 0)
            {
                ((Repeater)sender).DataSource = Memos[0].DocketingTeams;
                ((Repeater)sender).DataBind();
            }
        }

        protected string GetDocketingPerson(object o)
        {
            string name = "";
            string personID = Convert.ToString(o);
            if (!string.IsNullOrWhiteSpace(personID))
            {
                name = this.GetPersonName(o);
            }
            return string.IsNullOrWhiteSpace(name) ? "<strong>- No Data Available -</strong>" : name;
        }
    }
}