﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Adversary.Web.Controls.SummarySections
{
    public partial class Rejection : SummarySectionBase
    {
        protected void rptRejectionLetter_PreRender(object sender, EventArgs e)
        {
            if (Memos != null && Memos.Count > 0)
            {
                ((Repeater)sender).DataSource = Memos[0].Attachments.Where(a => a.Description == "Rejection Letter");
                ((Repeater)sender).DataBind();
            }
        }

        protected void tblEmpty_PreRender(object sender, EventArgs e)
        {
            if (rptRejectionLetter.Items == null || rptRejectionLetter.Items.Count < 1)
            {
                tblEmpty.Visible = true;
                tblRejection.Visible = false;
            }
            else
            {
                tblEmpty.Visible = false;
                tblRejection.Visible = true;
            }
        }
    }
}