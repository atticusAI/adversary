﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.DataLayer;

namespace Adversary.Web.Controls.SummarySections
{
    public partial class ClientAddress : SummarySectionBase 
    {
        protected void rptClientAddress_PreRender(object sender, EventArgs e)
        {
            if (Memos != null && Memos.Count > 0)
            {
                ((Repeater)sender).DataSource = Memos[0].Addresses;
                ((Repeater)sender).DataBind();
            }
        }

        protected void rptContacts_PreRender(object sender, EventArgs e)
        {
            ((Repeater)sender).DataSource = Memos[0].ClientContacts.ToList();
            ((Repeater)sender).DataBind();
        }

        protected string GetSectionName()
        {
            string sectionName = "Client Address";
            if (PrintMode())
            {
                sectionName = "Client Info";
            }
            return sectionName;
        }

        protected string GetContactName(object o)
        {
            ClientContact cont = (ClientContact)o;
            return cont.ContactFName + " " + cont.ContactMName + " " + cont.ContactLName + " (" + cont.ContactTitle + ")";
        }
    }
}