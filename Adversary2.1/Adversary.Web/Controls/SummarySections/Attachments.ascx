﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Attachments.ascx.cs"
    Inherits="Adversary.Web.Controls.SummarySections.Attachments" %>
<asp:Panel ID="pnlAttach" runat="server" OnPreRender="pnlAttach_PreRender">
    <table class="frv" cellspacing="0" style="border-collapse: collapse;">
        <tbody>
            <tr class="frvrow">
                <td colspan="2">
                    <a class="sum">
                        <%=GetSectionName()%></a>
                    <table class="sum">
                        <tr>
                            <td colspan="2">
                                <asp:Repeater ID="rptAttach" runat="server" OnPreRender="rptAttach_PreRender" OnItemDataBound="rpt_ItemDataBound">
                                    <ItemTemplate>
                                        <asp:HyperLink CssClass="attach" ID="hypDownload" runat="server" NavigateUrl='<%#DownloadUrl(Eval("AttachmentID"))%>'
                                            Text='<%#Eval("Description")%>' Target="_blank" />
                                        (<%#Eval("FileName")%>)
                                        <br />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <tr id="trEmpty" runat="server" visible="false">
                                            <td colspan="2">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                    <br />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Panel>
