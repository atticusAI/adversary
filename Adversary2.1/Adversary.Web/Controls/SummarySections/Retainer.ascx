﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Retainer.ascx.cs" Inherits="Adversary.Web.Controls.SummarySections.Retainer" %>
<asp:Panel ID="pnlRetainer" runat="server">
    <asp:FormView ID="frmRetainer" runat="server" DefaultMode="ReadOnly" OnPreRender="frmRetainer_PreRender">
        <EmptyDataTemplate>
            <a class="sum">Retainer</a>
            <table class="sum">
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
            </table>
        </EmptyDataTemplate>
        <ItemTemplate>
            <a class="sum">Retainer</a>
            <table class="sum">
                <tr>
                    <td>
                        Is there a retainer for this engagement?
                    </td>
                    <td>
                        <%#YesNo(Eval("Retainer"))%>
                    </td>
                </tr>
                <tr id="Tr1" runat="server" visible='<%#!Convert.ToBoolean(Eval("Retainer"))%>'>
                    <td>
                        Reason why no retainer is required
                    </td>
                    <td>
                        <%#Eval("RetainerDescNo")%>
                    </td>
                </tr>
                <tr id="Tr2" runat="server" visible='<%#Convert.ToBoolean(Eval("Retainer"))%>'>
                    <td>
                        Is the retainer less than $5000?
                    </td>
                    <td>
                        <%#SmallRetainerYesNo(Eval("RetainerDescYes"))%>
                    </td>
                </tr>
                <tr id="Tr3" runat="server" visible='<%#SmallRetainer(Eval("RetainerDescYes"))%>'>
                    <td>
                        Reason why retainer is less than $5000
                    </td>
                    <td>
                        <%#Eval("RetainerDescYes")%>
                    </td>
                </tr>
                <tr id="Tr4" runat="server" visible='<%#HasRetainerDesc(Eval("RetainerDesc"))%>'>
                    <td colspan="2">
                        <%#Eval("RetainerDesc")%>
                    </td>
                </tr>
                <%--<tr id="Tr5" runat="server" visible='<%#PrintMode()%>'>
                    <td>
                        If no retainer is requested or if retainer is less than $5000 PGL or AP must sign
                        here:
                    </td>
                    <td>
                        <br />
                        <br />
                        <span style="text-decoration: underline; display: inline-block; width: 100%;">X____________________________________________________
                        </span>
                    </td>
                </tr>--%>
            </table>
            <br />
        </ItemTemplate>
    </asp:FormView>
</asp:Panel>
