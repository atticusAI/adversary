﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Adversary.Web.Controls.SummarySections
{
    public partial class EngagementLetter : SummarySectionBase 
    {
        protected void rptEngagementLetter_PreRender(object sender, EventArgs e)
        {
            if (Memos != null && Memos.Count > 0)
            {
                ((Repeater)sender).DataSource = Memos[0].Attachments.Where(a => a.Description == "Engagement Letter");
                ((Repeater)sender).DataBind();
            }
        }

        protected void rptPolicyFiles_PreRender(object sender, EventArgs e)
        {
            if (Memos != null && Memos.Count > 0)
            {
                ((Repeater)sender).DataSource = Memos[0].Attachments.Where(a => a.Description == "Written Policy");
                ((Repeater)sender).DataBind();
            }
        }

        protected void trEmpty_PreRender(object sender, EventArgs e)
        {
            if (rptEngagementLetter.Items == null || rptEngagementLetter.Items.Count < 1)
            {
                trEmpty.Visible = true;
                trEngage.Visible = false;
            }
            else
            {
                trEmpty.Visible = false;
                trEngage.Visible = true;
            }
        }

    }
}