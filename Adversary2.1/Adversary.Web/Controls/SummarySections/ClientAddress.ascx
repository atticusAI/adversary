﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientAddress.ascx.cs"
    Inherits="Adversary.Web.Controls.SummarySections.ClientAddress" %>
<asp:Panel ID="pnlClientAddress" runat="server">
    <table class="frv" cellspacing="0" style="border-collapse: collapse;">
        <tbody>
            <tr class="frvrow">
                <td colspan="2">
                    <a class="sum">
                        <%=GetSectionName()%></a>
                    <asp:Repeater ID="rptClientAddress" runat="server" OnPreRender="rptClientAddress_PreRender" OnItemDataBound="rpt_ItemDataBound">
                        <ItemTemplate>
                            <table class="sum">
                                <tr id="trClientName" runat="server" visible='<%#PrintMode()%>'>
                                    <td>
                                        Client Name
                                    </td>
                                    <td>
                                        <%#GetClientDisplayName()%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Address
                                    </td>
                                    <td>
                                        <%#Eval("Address1")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <%#Eval("Address2")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <%#Eval("Address3")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        City
                                    </td>
                                    <td>
                                        <%#Eval("City")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        State/Province
                                    </td>
                                    <td>
                                        <%#Eval("State")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Zip
                                    </td>
                                    <td>
                                        <%#Eval("Zip")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Country
                                    </td>
                                    <td>
                                        <%#Eval("Country")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Phone
                                    </td>
                                    <td>
                                        <%#Eval("Phone")%>
                                    </td>
                                </tr>
                                <tr id="trAlt" runat="server" visible='<%#!string.IsNullOrWhiteSpace(Convert.ToString(Eval("AltPhone")))%>'>
                                    <td>
                                        Alt Phone
                                    </td>
                                    <td>
                                        <%#Eval("AltPhone")%>
                                    </td>
                                </tr>
                                <tr id="trEmail" runat="server" visible='<%#!string.IsNullOrWhiteSpace(Convert.ToString(Eval("Email")))%>'>
                                    <td>
                                        Email
                                    </td>
                                    <td>
                                        <%#Eval("email")%>
                                    </td>
                                </tr>
                                <tr id="trFax" runat="server" visible='<%#!string.IsNullOrWhiteSpace(Convert.ToString(Eval("Fax")))%>'>
                                    <td>
                                        Fax
                                    </td>
                                    <td>
                                        <%#Eval("Fax")%>
                                    </td>
                                </tr>
                                <tr id="trContact" runat="server" visible='<%#PrintMode() %>'>
                                    <td>
                                        Contact
                                    </td>
                                    <td>
                                        <asp:Repeater ID="rptContacts" runat="server" OnPreRender="rptContacts_PreRender">
                                            <ItemTemplate>
                                                <%#GetContactName(Container.DataItem)%><br />
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                        <FooterTemplate>
                            <tr id="trEmpty" runat="server" visible="false">
                                <td colspan="2">&nbsp;</td>
                            </tr>
                        </FooterTemplate>
                    </asp:Repeater>
                    <br />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Panel>
