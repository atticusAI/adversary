﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.DataLayer;
using System.Linq;
using System.Data.Linq;
using System.ComponentModel;

namespace Adversary.Web.Controls
{
    public partial class PartyRelationshipCodeDropdown : System.Web.UI.UserControl
    {
        
        [Bindable(true)]
        public enum PartyRelationshipMode
        {
            AffiliateParties,
            RelatedParties,
            AdverseParties
        }

        private string _SelectedCode;
        public string SelectedCode
        {
            get
            {
                if (this.ddlRelationshipCodes != null)
                {
                    _SelectedCode = this.ddlRelationshipCodes.SelectedValue;
                }
                return _SelectedCode;
            }
        }

        private string _SelectedText;
        public string SelectedText
        {
            get
            {
                if (this.ddlRelationshipCodes != null)
                {
                    _SelectedText = this.ddlRelationshipCodes.SelectedItem.Text;
                }
                return _SelectedText;
            }
        }

        private PartyRelationshipMode _RelationshipDisplayMode;
        public PartyRelationshipMode RelationshipDisplayMode
        {
            set { _RelationshipDisplayMode = value; }
        }

        private bool _LoadAffiliateCodes = false;
        public bool LoadAffiliateCodes { get { return _LoadAffiliateCodes; } set { _LoadAffiliateCodes = value; } }
        

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ddlRelationshipCodes == null) ddlRelationshipCodes = new DropDownList();
                List<DataLayer.AdversaryDataService.PartyStatus> partyList = new List<DataLayer.AdversaryDataService.PartyStatus>();
                using (AdversaryData d = new AdversaryData())
                {
                    partyList = d.GetPartyStatusList();
                }

                string[] affiliateParties = new string[] { "203", "102" };

                switch (_RelationshipDisplayMode)
                {
                    case PartyRelationshipMode.AffiliateParties:
                        partyList = partyList.Where(P => affiliateParties.Contains(P.STATUS_CODE)).ToList();
                        break;
                    case PartyRelationshipMode.RelatedParties:
                        break;
                    case PartyRelationshipMode.AdverseParties:
                        break;
                    default:
                        break;
                }

                this.ddlRelationshipCodes.DataTextField = "DESCRIPTION";
                this.ddlRelationshipCodes.DataValueField = "STATUS_CODE";
                this.ddlRelationshipCodes.DataSource = partyList;
                this.ddlRelationshipCodes.DataBind();
            
        }
    }
}