﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ApproverComboBox.ascx.cs"
    Inherits="Adversary.Web.Controls.ApproverComboBox" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:ComboBox runat="server" ID="combo" AutoCompleteMode="Suggest" CaseSensitive="false"
    DropDownStyle="DropDownList" OnSelectedIndexChanged="combo_SelectedIndexChanged"
    OnPreRender="combo_PreRender" />
