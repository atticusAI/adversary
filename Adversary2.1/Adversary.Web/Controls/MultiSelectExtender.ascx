﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MultiSelectExtender.ascx.cs" Inherits="VCRequestForm.Controls.MultiSelectExtender" %>
<script type="text/javascript">
    $(document).ready(function () {
        <%if (this.GroupOptions && !string.IsNullOrWhiteSpace(this.GroupSeparator)) {%>
        <%=this.ClientID%>_makeOptGroups($('#<%=this.TargetControlClientID%>'), '<%=this.GroupSeparator%>');
        <%}%>

        $('#<%=this.TargetControlClientID%>').multiselect({
            header: <%=this.jsShowHeader%>
            ,height: '<%=this.Height%>'
            ,minWidth: '<%=this.MinWidth%>'
            ,checkAllText: '<%=this.CheckAllText %>'
            ,uncheckAllText: '<%=this.UncheckAllText%>'
            ,noneSelectedText: '<%=this.NoneSelectedText%>'
            ,selectedText: '<%=this.SelectedText%>'
            ,selectedList: <%=this.jsSelectedList%> 
            ,show: '<%=this.ShowEffect%>'
            ,hide: '<%=this.HideEffect%>'
            ,autoOpen: <%=this.jsAutoOpen%>
            ,multiple: <%=this.jsMultiple%>
            <%if (this.AutoPostBack && string.IsNullOrWhiteSpace(this.OnClientClosed)) {%>
            ,close: function(event, ui){ setTimeout('__doPostBack(\'<%=this.TargetControlName%>\')', 0); }
            <%}else if (!this.AutoPostBack && !string.IsNullOrWhiteSpace(this.OnClientClosed)) {%>
            ,close: function(event, ui){ setTimeout('<%=this.OnClientClosed%>', 0); }
            <%}else if (this.AutoPostBack && !string.IsNullOrWhiteSpace(this.OnClientClosed)) {%>
            ,close: function(event, ui){ setTimeout('if (<%=this.OnClientClosed%>) {setTimeout(\'__doPostBack(\'<%=this.TargetControlName%>\');}\'', 0)}
            <%}%>
        });
    });

    <%if (this.GroupOptions && !string.IsNullOrWhiteSpace(this.GroupSeparator)) {%>
    function <%=this.ClientID%>_makeOptGroups(select, separator) {
        var oldGrp, newGrp;
        var opts = [];
        $('option', select).each(function (i) {
            var opt = $(this).text().split(separator);
            newGrp = opt[0];
            $(this).text(opt[1]);
            if (oldGrp && oldGrp != newGrp) {
                $(opts.join(','), select).wrapAll($('<optgroup />').attr('label', oldGrp));
                opts = [];
                opts.push('option[value=' + $(this).val() + ']');
            }
            else {
                opts.push('option[value=' + $(this).val() + ']');
            }
            oldGrp = newGrp;
        });
        if (opts && opts.length > 0) {
            $(opts.join(','), select).wrapAll($('<optgroup />').attr('label', oldGrp));
        }
    }
    <%}%>
</script>
