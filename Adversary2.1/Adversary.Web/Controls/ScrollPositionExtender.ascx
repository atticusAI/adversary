﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ScrollPositionExtender.ascx.cs" Inherits="Adversary.Web.Controls.ScrollPositionExtender" %>
<script type="text/javascript">
    $(document).ready(function () {
        $(<%=this.jQueryTargetControlClientID%>).scroll(function () {
            var y = $(this).scrollTop();
            $('#<%=this.hid.ClientID%>').val(y);
            <%if (!string.IsNullOrWhiteSpace(this.Cookie)) {%>
            $.cookie('<%=this.Cookie%>', y);
            <%}%>
        });
        <%if (!string.IsNullOrWhiteSpace(this.Cookie)) {%>
        var c = $.cookie('<%=this.Cookie%>');
        if (c != null) {
            $('#<%=this.hid.ClientID%>').val(c);
        }
        <%}%>
        var y = $('#<%=this.hid.ClientID%>').val();
        if (y != null) {
            $(<%=this.jQueryTargetControlClientID%>).scrollTop(y);
        }
    });
</script>
<asp:HiddenField ID="hid" runat="server" />