﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Adversary.Web.Controls
{
    public partial class CookieExtender : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// look for cookies for the targetcontrol
        /// if found reload values from those cookies
        /// allow for a single child control
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            HttpCookie cookie = this.Request.Cookies[this.ClientID];
            if (cookie != null)
            {
                Control ctrl = this.NamingContainer.FindControl(TargetControlID);
                if (ctrl != null)
                {
                    if (!string.IsNullOrWhiteSpace(this.ChildControlID))
                    {
                        ctrl = ctrl.FindControl(this.ChildControlID);
                    }

                    if (ctrl is DropDownList)
                    {
                        ((DropDownList)ctrl).SelectedValue = Server.UrlDecode(cookie.Value);
                    }
                    else if (ctrl is CheckBoxList)
                    {
                        ((CheckBoxList)ctrl).SelectedValue = Server.UrlDecode(cookie.Value);
                    }
                    else if (ctrl is ListBox)
                    {
                        ((ListBox)ctrl).SelectedValue = Server.UrlDecode(cookie.Value);
                    }
                    else if (ctrl is HiddenField)
                    {
                        ((HiddenField)ctrl).Value = Server.UrlDecode(cookie.Value);
                    }
                    else if (ctrl is TextBox)
                    {
                        ((TextBox)ctrl).Text = Server.UrlDecode(cookie.Value);
                    }
                }
            }
        }


        private bool _useBlur = false;

        /// <summary>
        /// if true then grab the value on blur instead of on change
        /// this fixes an ie bug where a textbox will not throw a change event and a keyup event
        /// </summary>
        [DefaultValue(false)]
        public bool UseBlur
        {
            get { return this._useBlur; }
            set { this._useBlur = value; }
        }


        private string _targetControlID;

        /// <summary>
        /// the required id of the target control
        /// </summary>
        public string TargetControlID
        {
            get { return this._targetControlID; }
            set { this._targetControlID = value; }
        }

        private string _childControlID;

        /// <summary>
        /// the optional id of the target control's child that will actually be used by this extension
        /// </summary>
        public string ChildControlID
        {
            get { return this._childControlID; }
            set { this._childControlID = value; }
        }

        /// <summary>
        /// get the client ID of the target control or it's child
        /// </summary>
        public string TargetControlClientID
        {
            get
            {
                string id = "";
                Control ctrl = this.NamingContainer.FindControl(this.TargetControlID);
                if (ctrl != null)
                {
                    if (!string.IsNullOrWhiteSpace(this.ChildControlID))
                    {
                        Control child = ctrl.FindControl(this.ChildControlID);
                        if (child != null)
                        {
                            id = child.ClientID;
                        }
                    }
                    else
                    {
                        id = ctrl.ClientID;
                    }
                }
                return id;
            }
        }

        private string _clearControlID;

        /// <summary>
        /// the control to extend to clear the cookie
        /// this control must raise the click event 
        /// </summary>
        public string ClearControlID
        {
            get { return this._clearControlID; }
            set { this._clearControlID = value; }
        }

        /// <summary>
        /// the client id of the clear control
        /// </summary>
        public string ClearControlClientID
        {
            get
            {
                string id = "";
                Control ctrl = this.NamingContainer.FindControl(this.ClearControlID);
                if (ctrl != null)
                {
                    id = ctrl.ClientID;
                }
                return id;
            }
        }
    }
}