﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.DataLayer;
using Adversary.Web.Controls;
using Adversary.Controls;

namespace Adversary.Web.Admin
{
    public partial class Users1 : Pages.AdminBasePage
    {
        private List<Adversary.DataLayer.HRDataService.FirmOffice> _Offices;
        private List<Adversary.DataLayer.HRDataService.FirmOffice> Offices
        {
            get
            {
                if (_Offices == null)
                    _Offices = AdversaryDataLayer.FirmOffices;
                return _Offices;
            }
        }

        private List<Adversary.DataLayer.AdversaryDataService.PracticeCode> _PracticeCodes;
        private List<Adversary.DataLayer.AdversaryDataService.PracticeCode> PracticeCodes
        {
            get
            {
                if (_PracticeCodes == null)
                    _PracticeCodes = AdversaryDataLayer.PracticeCodes;
                return _PracticeCodes;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            _Offices = AdversaryDataLayer.FirmOffices;
            _PracticeCodes = AdversaryDataLayer.PracticeCodes;
        }

        protected void ucUsers_AddClick(object sender, EventArgs e)
        {
            PersonAutoComplete _pControl = (PersonAutoComplete)ucUsers.FindAddControl("ucEmployee");
            HiddenField hControl = (HiddenField)_pControl.FindControl("hidEmployeeCode");
            //get the employee info
            Adversary.DataLayer.HRDataService.Employee _employee = AdversaryDataLayer.GetHREmployee(hControl.Value);
            OfficeDropDown _oControl = (OfficeDropDown)ucUsers.FindAddControl("advOfficeDropDown");
            string _officecode = _oControl.ddlOffice.SelectedValue;

            //get the practice code selections
            ListBox _primary = (ListBox)ucUsers.FindAddControl("lbPracticeGroupsPrimary");
            ListBox _secondary = (ListBox)ucUsers.FindAddControl("lbPracticeGroupsSecondary");

            //get the Adversary Group/Adversary Group Admin checkboxes
            CheckBox _cbIsAdversary = (CheckBox)ucUsers.FindAddControl("cbIsAdversary");
            CheckBox _cbIsAdversaryAdmin = (CheckBox)ucUsers.FindAddControl("cbIsAdversaryAdmin");
            if (_cbIsAdversaryAdmin.Checked && !_cbIsAdversary.Checked) _cbIsAdversary.Checked = true;

            CheckBox _cbIsAP = (CheckBox)ucUsers.FindAddControl("cbIsAP");
            string _apType = ((DropDownList)ucUsers.FindAddControl("ddlAPType")).SelectedValue;

            CheckBox _cbIsPGL = (CheckBox)ucUsers.FindAddControl("cbIsPGL");

            Adversary.DataLayer.AdminLogin _newLogin = new AdminLogin();
            _newLogin.IsAdversary = _cbIsAdversary.Checked;
            _newLogin.IsAdversaryAdmin = _cbIsAdversaryAdmin.Checked;
            _newLogin.IsAP = _cbIsAP.Checked;
            _newLogin.IsPrimaryAP = _apType.Equals("Primary");
            _newLogin.Office = Convert.ToInt32(_officecode);
            _newLogin.Email = _employee.CompanyEmailAddress;
            
            if (_primary != null && _secondary != null)
            {
                if(_primary.SelectedIndex > -1)
                    _newLogin.AdminPracCodes.AddRange(GetSelectedPracticeCodes(_primary, true));
                if (_secondary.SelectedIndex > -1)
                    _newLogin.AdminPracCodes.AddRange(GetSelectedPracticeCodes(_secondary, false));

                
            }

            Adversary.DataLayer.Admin.UserAccounts userAccounts = new DataLayer.Admin.UserAccounts();
            userAccounts.CreateAdminLogin(_newLogin);

            
        }

        private List<AdminPracCode> GetSelectedPracticeCodes(ListBox lb, bool isPrimary)
        {
            List<AdminPracCode> codes = new List<AdminPracCode>();
            foreach (ListItem item in lb.Items)
            {
                if (item.Selected)
                    codes.Add(new AdminPracCode()
                    {
                         IsPrimary = isPrimary,
                         PracCode = item.Value,
                    });
            }
            return codes;
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            ListBox _primary = (ListBox)ucUsers.FindAddControl("lbPracticeGroupsPrimary");
            ListBox _secondary = (ListBox)ucUsers.FindAddControl("lbPracticeGroupsSecondary");

            _primary.DataSource = _PracticeCodes;
            _primary.DataTextField = "cat_plus_desc";
            _primary.DataValueField = "matt_cat_code";
            _primary.DataBind();

            _secondary.DataSource = _PracticeCodes;
            _secondary.DataTextField = "cat_plus_desc";
            _secondary.DataValueField = "matt_cat_code";
            _secondary.DataBind();

            ucUsers_DataBind();
        }


        protected void ucUsers_PreRender(object sender, EventArgs e)
        {
            //ListBox _primary = (ListBox)ucUsers.FindAddControl("lbPracticeGroupsPrimary");
            //ListBox _secondary = (ListBox)ucUsers.FindAddControl("lbPracticeGroupsSecondary");

            //_primary.DataSource = _PracticeCodes;
            //_primary.DataTextField = "cat_plus_desc";
            //_primary.DataValueField = "matt_cat_code";
            //_primary.DataBind();

            //_secondary.DataSource = _PracticeCodes;
            //_secondary.DataTextField = "cat_plus_desc";
            //_secondary.DataValueField = "matt_cat_code";
            //_secondary.DataBind();

            //ucUsers_DataBind();
        }

        protected void ucUsers_DataBind()
        {

            //get the admin user data
            Adversary.DataLayer.Admin.UserAccounts _Users = new DataLayer.Admin.UserAccounts();
            List<Adversary.DataLayer.AdminLogin> _AllLogins = new List<AdminLogin>();
            _AllLogins = _Users.GetAdminLogins().OrderBy(F => F.Login).ToList();
            ucUsers.DataSource = _AllLogins;
            ucUsers.DataBind();
        }

        protected void ucUserAccounts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Adversary.DataLayer.AdminLogin _login = (Adversary.DataLayer.AdminLogin)e.Row.DataItem;
                e.Row.Cells[3].Text = _Offices.Where(o => o.LocationCode == _login.Office.ToString()).First().LocationDescription;

                e.Row.Cells[4].Controls.Add(((_login.IsAdversary ?? false) || (_login.IsAdversaryAdmin ?? false)) ? this.ImgChecked : this.ImgUnchecked);

                e.Row.Cells[5].Controls.Add(((_login.IsAP ?? false) || (_login.IsPrimaryAP ?? false)) ? this.ImgChecked : this.ImgUnchecked);

                if (_login.IsPrimaryAP ?? false) e.Row.Cells[5].Controls.Add(this.LblPrimary);

                else if ((_login.IsAP ?? false) && (_login.IsPrimaryAP.HasValue && _login.IsPrimaryAP.Value == false))
                    e.Row.Cells[5].Controls.Add(this.LblSecondary);


                e.Row.Cells[6].Controls.Add((_login.IsPGM ?? false) ? this.ImgChecked : this.ImgUnchecked);

                e.Row.Cells[7].Controls.Add(GetLoginPracticeGroups(_login.AdminPracCodes.ToList()));

            }
        }

        private Label LblPrimary
        {
            get { return new Label() { Text = "(P)", ForeColor = System.Drawing.Color.Orange }; }
        }
        private Label LblSecondary
        {
            get { return new Label() { Text = "(S)", ForeColor = System.Drawing.Color.Orange }; }
        }

        private Image ImgChecked
        {
            get { return new Image() { ImageUrl = "~/Images/cb_checked.jpg", Width = Unit.Pixel(12), Height = Unit.Pixel(12) }; }
        }
        private Image ImgUnchecked
        {
            get { return new Image() { ImageUrl = "~/Images/cb_empty.jpg", Width = Unit.Pixel(12), Height = Unit.Pixel(12) }; }
        }

        private Label GetLoginPracticeGroups(List<AdminPracCode> Codes)
        {
            Label lbl = new Label();
            foreach (AdminPracCode code in Codes)
            {
                lbl.Controls.Add(new LiteralControl(
                    _PracticeCodes.Where(P => P.matt_cat_code.Equals(code.PracCode)).First().cat_plus_desc)
                    );
                lbl.Controls.Add((code.IsPrimary ?? false) ? LblPrimary : LblSecondary);
                lbl.Controls.Add(new LiteralControl("<br/>"));
            }
            return lbl;
        }


    }
}