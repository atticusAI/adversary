﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.Web.Pages;
using Adversary.Web.Extensions;

namespace Adversary.Web.Admin
{
	public partial class Impersonate : AdminBasePage
	{
        protected void btnView_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                string login = this.actApprover.Text;
                if (login.Contains(" - "))
                {
                    login = this.actApprover.Text.Substring(this.actApprover.Text.LastIndexOf(" - ") + 3);
                }
                Response.Redirect("~/Admin/Queue.aspx?impersonate=" + login, false);
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (this.Page.Master is AdminMaster)
            {
                ((AdminMaster)this.Page.Master).TabPageUrl = "/Admin/AdminDefault.aspx";
                ((AdminMaster)this.Page.Master).SubTabPageUrl = "/Admin/AdminDefault.aspx";
            }
        }

        protected void valPage_ServerValidate(object sender, ServerValidateEventArgs e)
        {
            this.ClearErrors();
            if (string.IsNullOrWhiteSpace(this.actApprover.Text))
            {
                this.actApprover.TextBox.HH_SetError(this.pnlErrs);
                e.IsValid = false;
            }
        }

        private void ClearErrors()
        {
            this.actApprover.TextBox.HH_ClearError();
            this.pnlErrs.HH_ClearError();
        }
	}
}