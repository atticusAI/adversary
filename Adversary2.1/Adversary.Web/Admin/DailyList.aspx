﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true"
    CodeBehind="DailyList.aspx.cs" Inherits="Adversary.Web.Admin.DailyList" Theme="HH" %>

<%@ Register TagPrefix="ajax" Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" %>
<%@ Register TagPrefix="uc" TagName="ErrorSummary" Src="~/Controls/ErrorSummary.ascx" %>
<asp:Content ID="cntMain" ContentPlaceHolderID="cphMainContent" runat="server">
    <asp:UpdatePanel ID="upnlDailyList" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="600">
                <tr>
                    <td>Start Date/Time:
                    </td>
                    <td>
                        <asp:TextBox ID="tbStartDate" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>End Date/Time:
                    </td>
                    <td>
                        <asp:TextBox ID="tbEndDate" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:RadioButtonList ID="rblAllIndividual" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Text="Just Me" Value="Me"></asp:ListItem>
                            <asp:ListItem Text="Everyone" Value="Everyone" Selected="True"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                    <td>
                        <asp:CheckBox ID="cbAllMemos" runat="server" Text="All Adversary Requests (including items that were not OK'd to be reported in the Daily List)" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right">
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                        <asp:UpdateProgress AssociatedUpdatePanelID="upnlDailyList" runat="server" ID="progress1"
                            DisplayAfter="0">
                            <ProgressTemplate>
                                Loading....
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">This is a partial list of the completed Adversary Requests for the dates noted above.
                        These are only the requests that were user OK'd to be reported in the Daily List.
                        The requests are listed from oldest to youngest. Existing clients are listed last.
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:UpdatePanel ID="upnlErrors" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                            <ContentTemplate>
                                <uc:ErrorSummary ID="ucErrors" runat="server" HideIcon="true" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Table ID="tblParties" runat="server">
                        </asp:Table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
