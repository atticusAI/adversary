﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true"
    CodeBehind="AdminQueue.aspx.cs" Inherits="Adversary.Web.Admin.AdminQueue" Theme="HH" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/AutoRefresh.ascx" TagName="AutoRefresh" TagPrefix="adv" %>
<%@ Register Src="~/Controls/ScreeningMemoLockStatus.ascx" TagName="LockStatus" TagPrefix="adv" %>
<asp:Content ID="cntMain" ContentPlaceHolderID="cphMainContent" runat="server">
    <asp:Label ID="lblRefresh" runat="server" Visible="false" />
    <adv:AutoRefresh ID="refQueue" runat="server" OnRefresh="refQueue_Refresh" IntervalSeconds="60" />
    <asp:Label ID="lblCount" runat="server" OnPreRender="lblCount_PreRender" /><br />
    <br />
    <asp:GridView ID="gvResults" runat="server" AutoGenerateColumns="false" OnPreRender="gvResults_PreRender"
        OnRowCommand="gvResults_RowCommand" Style="font-size: 9pt;">
        <Columns>
            <asp:TemplateField HeaderText="Memo">
                <ItemTemplate>
                    <asp:LinkButton ID="lbView" runat="server" Text="View" CommandName="View" CommandArgument='<%#GetTrackingID(Container.DataItem)%>'
                        name='<%#Eval("ScrMemID")%>' /><br />
                    <%#Eval("ScrMemID")%>
                    <adv:LockStatus ID="lckStat" runat="server" ScrMemID='<%#Eval("ScrMemID")%>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Matter Name">
                <ItemTemplate>
                    <%#Eval("MatterName")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Client">
                <ItemTemplate>
                    <%#GetClientDisplayName(Container.DataItem)%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Attorney">
                <ItemTemplate>
                    <%#GetEnteringAttorneyName(Container.DataItem)%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Entering Employee">
                <ItemTemplate>
                    <%#GetPersonnelName(Container.DataItem)%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Fee Split?">
                <ItemTemplate>
                    <%#GetFeeSplit(Container.DataItem)%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="AP Approval">
                <ItemTemplate>
                    <%#GetAPApproval(Container.DataItem)%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="PGL Approval">
                <ItemTemplate>
                    <%#GetPGMApproval(Container.DataItem)%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Adversary Approval">
                <ItemTemplate>
                    <%#GetAdversaryAdminApproval(Container.DataItem)%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="CM #'s Sent">
                <ItemTemplate>
                    <%#GetCMNumbersSentDate(Container.DataItem)%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>
