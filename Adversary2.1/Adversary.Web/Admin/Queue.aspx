﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true"
    CodeBehind="Queue.aspx.cs" Inherits="Adversary.Web.Admin.Queue" Theme="HH" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controls/QueueRepeater.ascx" TagName="QueueRepeater" TagPrefix="adv" %>
<%@ Register Src="~/Controls/AutoRefresh.ascx" TagName="AutoRefresh" TagPrefix="adv" %>
<%@ Register Src="~/Controls/HiddenPanelExtender.ascx" TagName="HiddenPanelExtender" TagPrefix="adv" %>
<%@ Register Src="~/Controls/ScrollPositionExtender.ascx" TagName="ScrollPositionExtender" TagPrefix="adv" %>
<%@ Register Src="~/Controls/CookieExtender.ascx" TagPrefix="adv" TagName="CookieExtender" %>


<asp:Content ID="cntHead" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="cntMain" ContentPlaceHolderID="cphMainContent" runat="server">
    <asp:Label ID="lblImpError" runat="server" OnPreRender="lblImpError_PreRender" CssClass="error" />
    <asp:Label ID="lblRefresh" runat="server" Visible="false" />
    <adv:AutoRefresh ID="refQueue" runat="server" OnRefresh="refQueue_Refresh" IntervalSeconds="60" />
    <asp:LinkButton ID="lbFilter" runat="server" Text="Filter Your Queue" />
    <asp:ModalPopupExtender ID="mpxFilter" runat="server" BackgroundCssClass="pop-back"
        TargetControlID="lbFilter" PopupControlID="pnlFilter" CancelControlID="btnClose" />
    <asp:Panel ID="pnlFilter" runat="server" CssClass="pop" Style="display: none">
        <table>
            <tr>
                <td class="lbl">Office
                </td>
                <td>
                    <asp:DropDownList ID="ddlAPOffice" runat="server" AutoPostBack="true" AppendDataBoundItems="true"
                        OnInit="ddlAPOffice_Init" OnDataBound="ddlAPOffice_DataBound">
                        <asp:ListItem Text="All Offices" Value="" />
                    </asp:DropDownList>
                    <adv:CookieExtender runat="server" ID="cxAPOffice" TargetControlID="ddlAPOffice" ClearControlID="btnRemove" />
                </td>
            </tr>
            <tr>
                <td class="lbl">Practice Group
                </td>
                <td>
                    <asp:DropDownList ID="ddlPracGroup" runat="server" AutoPostBack="true" AppendDataBoundItems="true"
                        OnInit="ddlPracGroup_Init">
                    </asp:DropDownList>
                    <adv:CookieExtender runat="server" ID="cxPracGroup" TargetControlID="ddlPracGroup" ClearControlID="btnRemove" />
                </td>
            </tr>
            <tr>
                <td class="lbl">Status
                </td>
                <td>
                    <asp:DropDownList ID="ddlStatus" runat="server" AutoPostBack="true" AppendDataBoundItems="true"
                        OnInit="ddlStatus_Init">
                        <asp:ListItem Text="All Open Memos" Value="all" />
                    </asp:DropDownList>
                    <adv:CookieExtender runat="server" ID="cxStatus" TargetControlID="ddlStatus" ClearControlID="btnRemove" />
                </td>
            </tr>
            <tr id="trOpen" runat="server" visible="false">
                <td class="lbl">Open Memo Visibility
                </td>
                <td>
                    <asp:DropDownList ID="ddlOpen" runat="server" AutoPostBack="true">
                    </asp:DropDownList>
                    <adv:CookieExtender runat="server" ID="cxOpen" TargetControlID="ddlOpen" ClearControlID="btnRemove" />
                </td>
            </tr>
            <tr>
                <td class="lbl">Assigned To
                </td>
                <td>
                    <asp:DropDownList ID="ddlAssignedTo" runat="server" AutoPostBack="true">
                        <asp:ListItem Text="Anyone" Value="Anyone" />
                        <asp:ListItem Text="Me" Value="me" />
                    </asp:DropDownList>
                    <adv:CookieExtender runat="server" ID="cxAssignedTo" TargetControlID="ddlAssignedTo" ClearControlID="btnRemove" />
                </td>
            </tr>
            <tr>
                <td class="lbl">Sort Order
                </td>
                <td>
                    <asp:DropDownList ID="ddlSort" runat="server" AutoPostBack="true">
                        <asp:ListItem Text="Oldest First" Value="true" />
                        <asp:ListItem Text="Newest First" Value="false" />
                    </asp:DropDownList>
                    <adv:CookieExtender runat="server" ID="cxSort" TargetControlID="ddlSort" ClearControlID="btnRemove" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="btnRemove" runat="server" Text="Remove Filter" OnClick="btnRemove_Click" />
                    <asp:Button ID="btnClose" runat="server" Text="Close" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlHelp" runat="server" OnPreRender="pnlHelp_PreRender">
        <adv:HiddenPanelExtender runat="server" ID="hpxAbout" TargetControlID="aAbout" PanelControlID="pnlAbout"
            Shown="false" HiddenCssClass="hide" Cookie="hpxAbout" />
        <a id="aAbout" runat="server" class="queue about hide" target="aboutPage">About This Screen</a>
        <asp:Panel ID="pnlAbout" runat="server" CssClass="queuewrap about" Style="display: none;">
            <ul style="font-size: 10pt; text-align: left;">
                <li><strong>Primary Queue</strong><br />
                    Items in this queue require your approval as Primary Approver <strong>(OR)</strong>
                    are items over four days old that require your approval as Backup Approver.<br />
                    <br />
                </li>
                <li><strong>Backup Queue</strong><br />
                    Items in this queue are awaiting approval by the Primary Approver.<br />
                    <br />
                </li>
                <li><strong>Approved Queue</strong><br />
                    Items in this queue have already been approved by you or another approver and are
                    awaiting further approval and/or processing by Adversary.<br />
                    <br />
                </li>
            </ul>
        </asp:Panel>
    </asp:Panel>
    <asp:Label ID="lblCount" runat="server" OnPreRender="lblCount_PreRender" />
    <asp:Panel ID="pnlBackup" runat="server" Visible="true" OnPreRender="pnlBackup_PreRender">
        <adv:HiddenPanelExtender runat="server" ID="hpxBackup" TargetControlID="aBackup" PanelControlID="pnlBakupRepeater"
            Shown="false" HiddenCssClass="hide" Cookie="hpxBackup" />
        <a id="aBackup" runat="server" class="queue backup hide">Show items in your backup queue</a>
        <asp:Panel CssClass="queuewrap backup" ID="pnlBackupRepeater" runat="server" Style="display: none;">
            <adv:QueueRepeater ID="repBackup" OnPreRender="repBackup_PreRender" runat="server" />
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlPrimary" runat="server" Visible="true" OnPreRender="pnlPrimary_PreRender">
        <adv:HiddenPanelExtender runat="server" ID="hpxPrimary" TargetControlID="aPrimary" PanelControlID="pnlPrimaryRepeater"
            Shown="True" HiddenCssClass="hide" Cookie="hpxPrimary" />
        <a id="aPrimary" runat="server" class="queue primary">Show items in your primary queue</a>
        <asp:Panel CssClass="queuewrap primary" ID="pnlPrimaryRepeater" runat="server">
            <adv:QueueRepeater ID="repPrimary" OnPreRender="repPrimary_PreRender" runat="server" />
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlProcessing" runat="server" Visible="true" OnPreRender="pnlProcessing_PreRender">
        <adv:HiddenPanelExtender runat="server" ID="hpxProcessing" TargetControlID="aProcessing" PanelControlID="pnlProcessingRepeater"
            Shown="False" HiddenCssClass="hide" Cookie="hpxProcessing" />
        <a id="aProcessing" runat="server" class="queue processing hide">Show items being processed by Adversary</a>
        <asp:Panel CssClass="queuewrap processing" ID="pnlProcessingRepeater" runat="server" Style="display: none;">
            <adv:QueueRepeater ID="repProcessing" OnPreRender="repProcessing_PreRender" runat="server" />
        </asp:Panel>
    </asp:Panel>
    <adv:ScrollPositionExtender runat="server" ID="spxQueue" Cookie="spxQueue" />
</asp:Content>
