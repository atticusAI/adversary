﻿<%@ Page Theme="HH" Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master"
    AutoEventWireup="true" CodeBehind="Users.aspx.cs" Inherits="Adversary.Web.Admin.Users" %>

<%@ Register TagPrefix="uc" TagName="ListContainerControl" Src="~/Controls/ListContainerControl.ascx" %>
<%@ Register TagPrefix="uc" TagName="PersonAutoComplete" Src="~/Controls/PersonAutoComplete.ascx" %>
<%@ Register TagPrefix="uc" TagName="ErrorControl" Src="~/Controls/ErrorSummary.ascx" %>
<%@ Register TagPrefix="advcontrols" Namespace="Adversary.Controls" Assembly="Adversary.Controls" %>
<%@ Register TagPrefix="ajax" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMainContent" runat="server">
    <script type="text/javascript">
        $('document').ready(function () {
            $(".pglCheckbox input").change(function () {
                $(".trPracticeGroups").toggle(this.checked);
            });
            $(".trPracticeGroups").hide();

            $(".pglCheckboxEdit input").change(function () {
                $(".trPracticeGroupsEdit").toggle(this.checked);
            });
            $(".trPracticeGroupsEdit").show();

            formValidation = hh.validateWithjQuery('#form1');
            jQuery.validator.addMethod("employee", function (value, element) {
                var id = element.id.replace("tbEmployee", "hidEmployeeCode");
                var h = $get(id);
                return (h && h.value != "");
            }, "You must select a valid employee.");

            //add user
            $('#<%=btnMoveRightPrimary.ClientID %>').click(function () {
                var selectedOptions = $('#<%=lbPracticeGroupsPrimary.ClientID %> option:selected');
                if (selectedOptions.length == 0) {
                    alert("Please select option to move");
                    return false;
                }

                $('#<%=lbPracticeGroupsPrimarySelected.ClientID %>').append($(selectedOptions).clone());
                $(selectedOptions).remove();

                //move to hidden field - START
                var options = "";
                var placedOptions = $('#<%=lbPracticeGroupsPrimarySelected.ClientID%> option');
                for (var i = 0; i < placedOptions.length; i++) {
                    options += placedOptions[i].value + "$$";
                }
                $('#<%=hidPracticeGroupsPrimarySelected.ClientID%>').val(options);

                //move to hidden field - END

                return false;
            });

            $('#<%=btnMoveRightSecondary.ClientID %>').click(function () {
                var selectedOptions = $('#<%=lbPracticeGroupsSecondary.ClientID %> option:selected');
                if (selectedOptions.length == 0) {
                    alert("Please select option to move");
                    return false;
                }

                $('#<%=lbPracticeGroupsSecondarySelected.ClientID %>').append($(selectedOptions).clone());
                $(selectedOptions).remove();

                //move to hidden field - START
                var options = "";
                var placedOptions = $('#<%=lbPracticeGroupsSecondarySelected.ClientID%> option');
                for (var i = 0; i < placedOptions.length; i++) {
                    options += placedOptions[i].value + "$$";
                }
                $('#<%=hidPracticeGroupsSecondarySelected.ClientID%>').val(options);

                //move to hidden field - END

                return false;
            });

            $('#<%=btnMoveLeftPrimary.ClientID %>').click(function () {
                var selectedOptions = $('#<%=lbPracticeGroupsPrimarySelected.ClientID %> option:selected');
                if (selectedOptions.length == 0) {
                    alert("Please select option to move");
                    return false;
                }

                $('#<%=lbPracticeGroupsPrimary.ClientID %>').append($(selectedOptions).clone());
                $(selectedOptions).remove();

                //clear the hidden field
                $('#<%=hidPracticeGroupsPrimarySelected.ClientID%>').val('');
                //loop through what was selected
                var options = "";
                var leftoverOptions = $('#<%=lbPracticeGroupsPrimarySelected.ClientID %> option');
                for (var i = 0; i < leftoverOptions.length; i++) {
                    options += leftoverOptions[i].value + "$$";
                }
                $('#<%=hidPracticeGroupsPrimarySelected.ClientID%>').val(options);

                return false;
            });

            $('#<%=btnMoveLeftSecondary.ClientID %>').click(function () {
                var selectedOptions = $('#<%=lbPracticeGroupsSecondarySelected.ClientID %> option:selected');
                if (selectedOptions.length == 0) {
                    alert("Please select option to move");
                    return false;
                }

                $('#<%=lbPracticeGroupsSecondary.ClientID %>').append($(selectedOptions).clone());
                $(selectedOptions).remove();

                //clear the hidden field
                $('#<%=hidPracticeGroupsSecondarySelected.ClientID%>').val('');
                //loop through what was selected
                var options = "";
                var leftoverOptions = $('#<%=lbPracticeGroupsSecondarySelected.ClientID %> option');
                for (var i = 0; i < leftoverOptions.length; i++) {
                    options += leftoverOptions[i].value + "$$";
                }
                $('#<%=hidPracticeGroupsSecondarySelected.ClientID%>').val(options);

                return false;
            });

            //edit user
            $('#<%=btnMoveRightPrimaryEdit.ClientID %>').click(function () {
                var selectedOptions = $('#<%=lbPracticeCodesPrimaryEdit.ClientID %> option:selected');
                if (selectedOptions.length == 0) {
                    alert("Please select option to move");
                    return false;
                }

                $('#<%=lbPracticeCodesPrimaryEditSelected.ClientID %>').append($(selectedOptions).clone());
                $(selectedOptions).remove();

                //move to hidden field - START
                var options = "";
                var placedOptions = $('#<%=lbPracticeCodesPrimaryEditSelected.ClientID%> option');
                for (var i = 0; i < placedOptions.length; i++) {
                    options += placedOptions[i].value + "$$";
                }
                $('#<%=hidPracticeCodesPrimaryEditSelected.ClientID%>').val(options);

                //move to hidden field - END

                return false;
            });

            $('#<%=btnMoveRightSecondaryEdit.ClientID %>').click(function () {
                var selectedOptions = $('#<%=lbPracticeCodesSecondaryEdit.ClientID %> option:selected');
                if (selectedOptions.length == 0) {
                    alert("Please select option to move");
                    return false;
                }

                $('#<%=lbPracticeCodesSecondaryEditSelected.ClientID %>').append($(selectedOptions).clone());
                $(selectedOptions).remove();

                //move to hidden field - START
                var options = "";
                var placedOptions = $('#<%=lbPracticeCodesSecondaryEditSelected.ClientID%> option');
                for (var i = 0; i < placedOptions.length; i++) {
                    options += placedOptions[i].value + "$$";
                }
                $('#<%=hidPracticeCodesSecondaryEditSelected.ClientID%>').val(options);

                //move to hidden field - END

                return false;
            });

            $('#<%=btnMoveLeftPrimaryEdit.ClientID %>').click(function () {
                var selectedOptions = $('#<%=lbPracticeCodesPrimaryEditSelected.ClientID %> option:selected');
                if (selectedOptions.length == 0) {
                    alert("Please select option to move");
                    return false;
                }

                $('#<%=lbPracticeCodesPrimaryEdit.ClientID %>').append($(selectedOptions).clone());
                $(selectedOptions).remove();

                //clear the hidden field
                $('#<%=hidPracticeCodesPrimaryEditSelected.ClientID%>').val('');
                //loop through what was selected
                var options = "";
                var leftoverOptions = $('#<%=lbPracticeCodesPrimaryEditSelected.ClientID %> option');
                for (var i = 0; i < leftoverOptions.length; i++) {
                    options += leftoverOptions[i].value + "$$";
                }
                $('#<%=hidPracticeCodesPrimaryEditSelected.ClientID%>').val(options);

                return false;
            });

            $('#<%=btnMoveLeftSecondaryEdit.ClientID %>').click(function () {
                var selectedOptions = $('#<%=lbPracticeCodesSecondaryEditSelected.ClientID %> option:selected');
                if (selectedOptions.length == 0) {
                    alert("Please select option to move");
                    return false;
                }

                $('#<%=lbPracticeCodesSecondaryEdit.ClientID %>').append($(selectedOptions).clone());
                $(selectedOptions).remove();

                //clear the hidden field
                $('#<%=hidPracticeCodesSecondaryEditSelected.ClientID%>').val('');
                //loop through what was selected
                var options = "";
                var leftoverOptions = $('#<%=lbPracticeCodesSecondaryEditSelected.ClientID %> option');
                for (var i = 0; i < leftoverOptions.length; i++) {
                    options += leftoverOptions[i].value + "$$";
                }
                $('#<%=hidPracticeCodesSecondaryEditSelected.ClientID%>').val(options);

                return false;
            });
            
            $get('<%= pnlAddUser.ClientID %>').style.height = document.documentElement.clientHeight * 0.9 + "px";
            $get('<%= pnlEditUser.ClientID %>').style.height = document.documentElement.clientHeight * 0.9 + "px";
            //$get('<%= pnlEditUser.ClientID%>').style.height = document.documentElement.clientHeight * 0.9 + "px";
        });
    </script>
    
    <uc:ErrorControl ID="ucErrorControl" runat="server" />
    <asp:LinkButton ID="btnShowAddUserPopup" runat="server" Text="Add User" CausesValidation="false" />
    <asp:GridView ID="gvUsers" runat="server" AutoGenerateColumns="false" SkinID="HH_Gridview"
        OnRowEditing="gvUsers_RowEditing" OnRowDataBound="gvUsers_RowDataBound" OnRowCommand="gvUsers_RowCommand"
        OnRowDeleting="gvUsers_RowDeleting">
        <Columns>
            <%--<asp:ButtonField Text="Edit" ButtonType="Link" CausesValidation="false" CommandName="Edit" />--%>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="lbEdit" runat="server" Text="Edit" CommandName="EditUser" CommandArgument='<%#Eval("UserID")%>' />
                </ItemTemplate>
            </asp:TemplateField>
            <%--<asp:ButtonField Text="Delete" ButtonType="Link" CausesValidation="false" CommandName="Delete" />--%>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="lbDelete" runat="server" Text="Delete" CommandName="DeleteUser"
                        CommandArgument='<%#Eval("UserID") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField HeaderText="User Name" DataField="Login" />
            <asp:BoundField HeaderText="Email" DataField="Email" />
            <asp:BoundField HeaderText="Office" /><%--will be an int, convert to office name--%>
            
            <asp:BoundField HeaderText="Adversary Group" /> <%--will show a checkbox and indicate membership in the adversary group --%>
           
            <asp:BoundField HeaderText="AP" /> <%--will indicate whether the users is an AP, and put a (P) for primary or (S) for secondary. Fed off IsAP/IsPrimaryAP fields. --%>
           
            <asp:BoundField HeaderText="PGL" /><%--like the AP field, will indicate whether the user is a PGL (no primary or secondary here, that's determined by practice code) --%>
            
            <asp:BoundField HeaderText="Practice Groups" />
        </Columns>
    </asp:GridView>
    <asp:Button ID="btnDummy" runat="server" CssClass="dummyControl" />
    <ajax:ModalPopupExtender ID="mdlAddUser" runat="server" PopupControlID="pnlAddUser" 
        TargetControlID="btnShowAddUserPopup" Y="25" X="-30" BackgroundCssClass="pop-back" />
    <asp:Panel ID="pnlAddUser" runat="server" CssClass="pop" Width="800" Scrollbars="auto">
        <table border="0" cellpadding="2" cellspacing="3">
            <tr>
                <td>
                    New User:
                </td>
                <td>
                    <uc:PersonAutoComplete ID="ucEmployee" runat="server" ServiceMethod="TimekeeperService"
                        TextBoxCssClass="required employee" JqueryRequiredMessage="You must select a valid employee." />
                </td>
            </tr>
            <tr>
                <td>
                    Office:
                </td>
                <td>
                    <advcontrols:OfficeDropDown ID="advOfficeDropDown" runat="server" CssClass="dropDownList" />
                </td>
            </tr>
            <tr>
                <td valign="top">
                    Choose Role:
                </td>
                <td valign="top">
                    <table>
                        <tr>
                            <td>
                                User is in Adversary Group?
                            </td>
                            <td>
                                <asp:CheckBox ID="cbIsAdversary" runat="server" AutoPostBack="false" />&nbsp;Yes
                            </td>
                            <td>
                                Is Adversary Administrator?
                                <asp:CheckBox ID="cbIsAdversaryAdmin" runat="server" AutoPostBack="false" />&nbsp;Yes
                            </td>
                        </tr>
                        <tr>
                            <td>
                                User is AP?
                            </td>
                            <td>
                                <asp:CheckBox ID="cbIsAP" runat="server" AutoPostBack="false" />&nbsp;Yes
                            </td>
                            <td>
                                AP Type:&nbsp;<asp:DropDownList ID="ddlAPType" runat="server">
                                    <asp:ListItem Text="Primary" />
                                    <asp:ListItem Text="Secondary" />
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                User is PGL?
                            </td>
                            <td>
                                <asp:CheckBox ID="cbIsPGL" runat="server" CssClass="pglCheckbox" />&nbsp;Yes
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="trPracticeGroups">
                <td valign="top" colspan="2">
                    <table border="0" cellpadding="1" cellspacing="0" width="100%">
                        <tr>
                            <td colspan="2">
                                Choose Practice Codes:
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" colspan="2">
                                Primary Practice Codes:
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ListBox ID="lbPracticeGroupsPrimary" runat="server" Rows="15" SelectionMode="Multiple"
                                    SkinID="HH_PracticeCodes"></asp:ListBox>
                                <br />
                                <asp:Button ID="btnMoveRightPrimary" runat="server" Text="Select &gt;&gt;" />
                            </td>
                            <td>
                                <asp:ListBox ID="lbPracticeGroupsPrimarySelected" runat="server" Rows="15" SelectionMode="Multiple"
                                    SkinID="HH_PracticeCodes" />
                                <br />
                                <asp:Button ID="btnMoveLeftPrimary" runat="server" Text="&lt;&lt; Deselect" />
                                <asp:HiddenField ID="hidPracticeGroupsPrimarySelected" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" colspan="2">
                                Secondary Practice Codes:
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ListBox ID="lbPracticeGroupsSecondary" runat="server" Rows="15" SelectionMode="Multiple"
                                    SkinID="HH_PracticeCodes"></asp:ListBox>
                                <br />
                                <asp:Button ID="btnMoveRightSecondary" runat="server" Text="Select &gt;&gt;" />
                            </td>
                            <td>
                                <asp:ListBox ID="lbPracticeGroupsSecondarySelected" runat="server" Rows="15" SelectionMode="Multiple"
                                    SkinID="HH_PracticeCodes" />
                                <br />
                                <asp:Button ID="btnMoveLeftSecondary" runat="server" Text="&lt;&lt; Deselect" />
                                <asp:HiddenField ID="hidPracticeGroupsSecondarySelected" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="btnAddUser" runat="server" Text="Add" OnClick="btnAddUser_Click" />
                    &nbsp;&nbsp;
                    <asp:Button ID="btnCancelAddUser" runat="server" Text="Cancel" CssClass="cancel"
                        OnClick="btnCancelAddUser_Click" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajax:ModalPopupExtender ID="mdlEditUser" runat="server" PopupControlID="pnlEditUser"
        TargetControlID="btnDummy" Y="25" X="-30" BackgroundCssClass="pop-back"  />
    <asp:Panel ID="pnlEditUser" runat="server" CssClass="pop" Width="800" Scrollbars="Auto">
        <asp:HiddenField ID="hidUserIDEdit" runat="server" />
        <table border="0" cellpadding="2" cellspacing="3">
            <tr>
                <td>
                    Edit User:
                </td>
                <td>
                    <asp:Label ID="lblUserNetLogin" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    Office:
                </td>
                <td>
                    <advcontrols:OfficeDropDown ID="advOfficeDropDownEdit" runat="server" />
                    <%--<asp:Panel ID="pnlOfficeDropDown" runat="server" />--%>
                </td>
            </tr>
            <tr>
                <td valign="top">
                    Choose Role:
                </td>
                <td valign="top">
                    <table>
                        <tr>
                            <td>
                                User is in Adversary Group?
                            </td>
                            <td>
                                <asp:CheckBox ID="cbAdversaryEdit" runat="server" AutoPostBack="false" />&nbsp;Yes
                            </td>
                            <td>
                                Is Adversary Administrator?
                                <asp:CheckBox ID="cbAdversaryAdminEdit" runat="server" AutoPostBack="false" />&nbsp;Yes
                            </td>
                        </tr>
                        <tr>
                            <td>
                                User is AP?
                            </td>
                            <td>
                                <asp:CheckBox ID="cbIsAPEdit" runat="server" AutoPostBack="false" />&nbsp;Yes
                            </td>
                            <td>
                                AP Type:&nbsp;<asp:DropDownList ID="ddlAPTypeEdit" runat="server">
                                    <asp:ListItem Text="Primary" />
                                    <asp:ListItem Text="Secondary" />
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                User is PGL?
                            </td>
                            <td>
                                <asp:CheckBox ID="cbIsPGLEdit" runat="server" CssClass="pglCheckboxEdit" />&nbsp;Yes
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="trPracticeGroupsEdit">
                <td valign="top" colspan="2">
                    <table border="0" cellpadding="1" cellspacing="0" width="100%">
                        <tr>
                            <td colspan="2">
                                Choose Practice Codes:
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" colspan="2">
                                Primary Practice Codes:
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ListBox ID="lbPracticeCodesPrimaryEdit" runat="server" Rows="15" SelectionMode="Multiple"
                                    SkinID="HH_PracticeCodes"></asp:ListBox>
                                <br />
                                <asp:Button ID="btnMoveRightPrimaryEdit" runat="server" Text="Select &gt;&gt;" />
                            </td>
                            <td>
                                <asp:ListBox ID="lbPracticeCodesPrimaryEditSelected" runat="server" Rows="15" SelectionMode="Multiple"
                                    SkinID="HH_PracticeCodes" />
                                <br />
                                <asp:Button ID="btnMoveLeftPrimaryEdit" runat="server" Text="&lt;&lt; Deselect" />
                                <asp:HiddenField ID="hidPracticeCodesPrimaryEditSelected" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" colspan="2">
                                Secondary Practice Codes:
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ListBox ID="lbPracticeCodesSecondaryEdit" runat="server" Rows="15" SelectionMode="Multiple"
                                    SkinID="HH_PracticeCodes"></asp:ListBox>
                                <br />
                                <asp:Button ID="btnMoveRightSecondaryEdit" runat="server" Text="Select &gt;&gt;" />
                            </td>
                            <td>
                                <asp:ListBox ID="lbPracticeCodesSecondaryEditSelected" runat="server" Rows="15" SelectionMode="Multiple"
                                    SkinID="HH_PracticeCodes" />
                                <br />
                                <asp:Button ID="btnMoveLeftSecondaryEdit" runat="server" Text="&lt;&lt; Deselect" />
                                <asp:HiddenField ID="hidPracticeCodesSecondaryEditSelected" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="btnEditUser" runat="server" Text="Update" OnClick="btnEditUser_Click" />
                    &nbsp;&nbsp;
                    <asp:Button ID="btnCancelEditUser" runat="server" Text="Cancel" OnClick="btnCancelEditUser_Click" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajax:ModalPopupExtender ID="mdlDeleteUser" runat="server" PopupControlID="pnlDeleteUser"
        TargetControlID="btnDummy" Y="25" />
    <asp:Panel ID="pnlDeleteUser" runat="server" SkinID="HH_PanelPopup_ConfirmBox">
        <asp:HiddenField ID="hidUserIDDelete" runat="server" />
        <div align="center">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        Are you sure you wish to delete user
                        <asp:Label ID="lblDeleteUser" runat="server" />?
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnDeleteUser" runat="server" Text="Delete User" OnClick="btnDeleteUser_Click" />
                        &nbsp;&nbsp;
                        <asp:Button ID="btnDeleteUserCancel" runat="server" Text="Cancel" OnClick="btnDeleteUserCancel_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
</asp:Content>
