﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.DataLayer;
using Adversary.Web.Pages;

namespace Adversary.Web.Admin
{
    public partial class Queue : Adversary.Web.Pages.AdminBasePage
    {
        #region Impersonation

        private AdminLogin ActiveLogin
        {
            get
            {
                return this.Impersonating ? this.ImpersonateLogin : this.CurrentUser.AdminLogin;
            }
        }

        private AdminLogin _impersonateLogin = null;

        public AdminLogin ImpersonateLogin
        {
            get
            {
                if (this._impersonateLogin == null)
                {
                    _impersonateLogin = ApprovalDataLayer.GetAdminLogin(ImpersonateName);
                }
                return this._impersonateLogin;
            }
        }

        private string _impersonateName = null;

        public string ImpersonateName
        {
            get
            {
                if (_impersonateName == null)
                {
                    if (!string.IsNullOrWhiteSpace(Request.QueryString["impersonate"]))
                    {
                        _impersonateName = Request.QueryString["impersonate"];
                    }
                }
                return _impersonateName;
            }
        }

        private bool? _impersonating = null;

        private bool Impersonating
        {
            get
            {
                if (this._impersonating == null)
                {
                    this._impersonating = IsImpersonating();
                }
                return this._impersonating ?? false;
            }
        }

        private bool IsImpersonating()
        {
            bool impersonating = false;
            if (this.CurrentUser.IsAdversary)
            {
                impersonating = (!(string.IsNullOrWhiteSpace(this.ImpersonateName)) && (this.ImpersonateLogin != null));
            }
            return impersonating;
        }

        public bool IsAdversary
        {
            get
            {
                bool adversary = false;
                if (this.Impersonating)
                {
                    adversary = this.ImpersonateLogin.IsAdversary ?? false;
                }
                else
                {
                    adversary = this.CurrentUser.IsAdversary;
                }
                return adversary;
            }
        }

        public bool IsPGM
        {
            get
            {
                bool pgm = false;
                if (this.Impersonating)
                {
                    pgm = this.ImpersonateLogin.IsPGM ?? false;
                }
                else
                {
                    pgm = this.CurrentUser.IsPGM;
                }
                return pgm;
            }
        }

        public bool IsAP
        {
            get
            {
                bool ap = false;
                if (this.Impersonating)
                {
                    ap = this.ImpersonateLogin.IsAP ?? false;
                }
                else
                {
                    ap = this.CurrentUser.IsAP;
                }
                return ap;
            }
        }

        #endregion

        #region Queue Storage

        private void runQueues()
        {
            int? office = this.ddlAPOffice.SelectedValue == "" ? null : (int?)Convert.ToInt32(this.ddlAPOffice.SelectedValue);
            int? statusTypeID = this.ddlStatus.SelectedValue == "all" ? null : (int?)Convert.ToInt32(this.ddlStatus.SelectedValue);
            int? userID = this.ddlAssignedTo.SelectedValue == "me" ? (int?)CurrentUser.AdminLogin.UserID : null;
            string pracCode = this.ddlPracGroup.SelectedValue == "all" ? "" : this.ddlPracGroup.SelectedValue;
            bool sortOldestFirst = Convert.ToBoolean(this.ddlSort.SelectedValue);
            bool setForDualRole = IsPGM && IsAP && !IsAdversary;
            bool setForPGM = IsPGM && !IsAP && !IsAdversary;

            this.QueueDataLayer.ProduceQueues(this.ActiveLogin, office, statusTypeID, userID, pracCode, sortOldestFirst, setForDualRole, setForPGM);
            this._primaryQueue = this.QueueDataLayer.PrimaryList;
            this._backupQueue = this.QueueDataLayer.BackupList;
            this._processingQueue = this.QueueDataLayer.ProcessingList;
        }

        private List<Memo> _primaryQueue = null;

        public List<Memo> PrimaryQueue
        {
            get
            {
                if (!this.QueueDataLayer.QueuesRun)
                {
                    this.runQueues();
                }
                return _primaryQueue;
            }
        }

        private List<Memo> _backupQueue = null;

        public List<Memo> BackupQueue
        {
            get
            {
                if (!this.QueueDataLayer.QueuesRun)
                {
                    this.runQueues();
                }
                return _backupQueue;
            }
        }

        private List<Memo> _processingQueue = null;

        public List<Memo> ProcessingQueue
        {
            get
            {
                if (!this.QueueDataLayer.QueuesRun)
                {
                    this.runQueues();
                }
                return _processingQueue;
            }
        }

        #endregion

        //#region Queue Switches

        ////private List<Memo> GetPrimaryQueue()
        ////{
        ////    List<Memo> memos = null;
        ////    switch (this.CurrentPageMode)
        ////    {
        ////        case PageMode.ADVERSARY:
        ////            memos = this.GetAdversaryPrimaryQueue();
        ////            break;
        ////        case PageMode.DUAL:
        ////            memos = this.GetDualRolePrimaryQueue();
        ////            break;
        ////        case PageMode.PGM:
        ////            memos = this.GetPGMPrimaryQueue();
        ////            break;
        ////        case PageMode.AP:
        ////            memos = this.GetAPPrimaryQueue(false);
        ////            break;
        ////        case PageMode.DEFAULT:
        ////            memos = this.GetDefaultPrimaryQueue();
        ////            break;
        ////    }
        ////    return memos;
        ////}

        ////private List<Memo> GetBackupQueue()
        ////{
        ////    List<Memo> memos = null;
        ////    switch (this.CurrentPageMode)
        ////    {
        ////        case PageMode.ADVERSARY:
        ////            // no backup queue for adversary
        ////            break;
        ////        case PageMode.DUAL:
        ////            memos = this.GetDualRoleBackupQueue();
        ////            break;
        ////        case PageMode.PGM:
        ////            memos = this.GetPGMBackupQueue();
        ////            break;
        ////        case PageMode.AP:
        ////            memos = this.GetAPBackupQueue();
        ////            break;
        ////        case PageMode.DEFAULT:
        ////            memos = this.GetDefaultBackupQueue();
        ////            break;
        ////    }
        ////    return memos;
        ////}
        
        ////private List<Memo> GetProcessingQueue()
        ////{
        ////    List<Memo> memos = null;
        ////    switch (this.CurrentPageMode)
        ////    {
        ////        case PageMode.ADVERSARY:
        ////            memos = this.GetAdversaryProcessingQueue();
        ////            break;
        ////        case PageMode.DUAL:
        ////            memos = this.GetDualRoleProcessingQueue();
        ////            break;
        ////        case PageMode.PGM:
        ////            memos = this.GetPGMProcessingQueue();
        ////            break;
        ////        case PageMode.AP:
        ////            memos = GetAPProcessingQueue(false);
        ////            break;
        ////        case PageMode.DEFAULT:
        ////            memos = GetDefaultProcessingQueue();
        ////            break;
        ////    }
        ////    return memos;
        ////}

        //#endregion

        //#region Default Queues

        ////private List<Memo> GetDefaultProcessingQueue()
        ////{
        ////    List<string> includePracCodes = new List<string>();
        ////    string officeCode = this.ddlAPOffice.SelectedValue;
        ////    int? userID = this.ddlAssignedTo.SelectedValue == "me" ? (int?)this.ActiveLogin.UserID : null;
        ////    bool sortNewestFirst = Convert.ToBoolean(this.ddlSort.SelectedValue);

        ////    includePracCodes.AddRange(this.QueueDataLayer.GetPracticeCodes(this.ActiveLogin.UserID).Select(p => p.PracCode));

        ////    return this.QueueDataLayer.GetDefaultProcessingQueue(userID, officeCode, includePracCodes, sortNewestFirst);
        ////}

        ////private List<Memo> GetDefaultBackupQueue()
        ////{
        ////    List<string> includePracCodes = new List<string>();
        ////    string officeCode = this.ddlAPOffice.SelectedValue;
        ////    int? userID = this.ddlAssignedTo.SelectedValue == "me" ? (int?)this.ActiveLogin.UserID : null;
        ////    bool sortNewestFirst = Convert.ToBoolean(this.ddlSort.SelectedValue);

        ////    includePracCodes.AddRange(this.QueueDataLayer.GetBackupPracticeCodes(this.ActiveLogin.UserID).Select(p => p.PracCode));

        ////    return this.QueueDataLayer.GetDefaultBackupQueue(userID, officeCode, includePracCodes, sortNewestFirst);
        ////}

        ////private List<Memo> GetDefaultPrimaryQueue()
        ////{
        ////    List<string> includePracCodes = new List<string>();
        ////    string officeCode = this.ddlAPOffice.SelectedValue;
        ////    int? userID = this.ddlAssignedTo.SelectedValue == "me" ? (int?)this.ActiveLogin.UserID : null;
        ////    bool sortNewestFirst = Convert.ToBoolean(this.ddlSort.SelectedValue);
        ////    int? statusTypeID = this.ddlStatus.SelectedValue == "all" ? null : (int?)Convert.ToInt32(this.ddlStatus.SelectedValue);
            
        ////    if (this.ddlPracGroup.SelectedValue == "my")
        ////    {
        ////        includePracCodes.AddRange(this.QueueDataLayer.GetPracticeCodes(this.ActiveLogin.UserID).Select(p => p.PracCode));
        ////    }
        ////    else if (this.ddlPracGroup.SelectedValue != "all")
        ////    {
        ////        includePracCodes.Add(this.ddlPracGroup.SelectedValue);
        ////    }

        ////    return this.QueueDataLayer.GetDefaultPrimaryQueue(userID, statusTypeID, officeCode, includePracCodes, sortNewestFirst);
        ////}

        //#endregion

        //#region AP Queues

        ////private List<Memo> GetAPProcessingQueue(bool isPrimaryAP)
        ////{
        ////    List<string> includePracCodes = new List<string>();

        ////    int? userID = this.ddlAssignedTo.SelectedValue == "me" ? (int?)this.ActiveLogin.UserID : null;
        ////    string officeCode = Convert.ToString(this.ActiveLogin.Office);
        ////    int? statusTypeID = this.ddlStatus.SelectedValue == "all" ? null : (int?)Convert.ToInt32(this.ddlStatus.SelectedValue);
        ////    bool sortNewestFirst = Convert.ToBoolean(this.ddlSort.SelectedValue);
        ////    if (this.ddlPracGroup.SelectedValue == "my")
        ////    {
        ////        includePracCodes.AddRange(this.QueueDataLayer.GetPracticeCodes(this.ActiveLogin.UserID).Select(p => p.PracCode));
        ////    }
        ////    else if (this.ddlPracGroup.SelectedValue != "all")
        ////    {
        ////        includePracCodes.Add(this.ddlPracGroup.SelectedValue);
        ////    }

        ////    return this.QueueDataLayer.GetAPProcessingQueue(userID, officeCode, statusTypeID, includePracCodes, sortNewestFirst);
        ////}

        ////private List<Memo> GetAPPrimaryQueue(bool isPrimaryAP)
        ////{
        ////    List<string> includePracCodes = new List<string>();

        ////    int? userID = this.ddlAssignedTo.SelectedValue == "me" ? (int?)this.ActiveLogin.UserID : null;
        ////    string officeCode = Convert.ToString(this.ActiveLogin.Office);
        ////    int? statusTypeID = this.ddlStatus.SelectedValue == "all" ? null : (int?)Convert.ToInt32(this.ddlStatus.SelectedValue);
        ////    bool sortNewestFirst = Convert.ToBoolean(this.ddlSort.SelectedValue);
        ////    if (this.ddlPracGroup.SelectedValue == "my")
        ////    {
        ////        includePracCodes.AddRange(this.QueueDataLayer.GetPracticeCodes(this.ActiveLogin.UserID).Select(p => p.PracCode));
        ////    }
        ////    else if (this.ddlPracGroup.SelectedValue != "all")
        ////    {
        ////        includePracCodes.Add(this.ddlPracGroup.SelectedValue);
        ////    }

        ////    return this.QueueDataLayer.GetAPPrimaryQueue(userID, officeCode, statusTypeID, includePracCodes, sortNewestFirst);
        ////}

        ////private List<Memo> GetAPBackupQueue()
        ////{
        ////    List<string> includePracCodes = new List<string>();

        ////    int? userID = this.ddlAssignedTo.SelectedValue == "me" ? (int?)this.ActiveLogin.UserID : null;
        ////    string officeCode = Convert.ToString(this.ActiveLogin.Office);
        ////    int? statusTypeID = this.ddlStatus.SelectedValue == "all" ? null : (int?)Convert.ToInt32(this.ddlStatus.SelectedValue);
        ////    bool sortNewestFirst = Convert.ToBoolean(this.ddlSort.SelectedValue);
        ////    if (this.ddlPracGroup.SelectedValue == "my")
        ////    {
        ////        includePracCodes.AddRange(this.QueueDataLayer.GetBackupPracticeCodes(this.ActiveLogin.UserID).Select(p => p.PracCode));
        ////    }
        ////    else if (this.ddlPracGroup.SelectedValue != "all")
        ////    {
        ////        includePracCodes.Add(this.ddlPracGroup.SelectedValue);
        ////    }

        ////    return this.QueueDataLayer.GetAPBackupQueue(userID, officeCode, statusTypeID, includePracCodes, sortNewestFirst);
        ////}

        //#endregion 

        //#region PGM Queues

        ////private List<Memo> GetPGMProcessingQueue()
        ////{
        ////    List<string> includePracCodes = new List<string>();

        ////    int? userID = this.ddlAssignedTo.SelectedValue == "me" ? (int?)this.ActiveLogin.UserID : null;
        ////    string officeCode = this.ddlAPOffice.SelectedValue;
        ////    int? statusTypeID = this.ddlStatus.SelectedValue == "all" ? null : (int?)Convert.ToInt32(this.ddlStatus.SelectedValue);
        ////    bool sortNewestFirst = Convert.ToBoolean(this.ddlSort.SelectedValue);
        ////    if (this.ddlPracGroup.SelectedValue == "my")
        ////    {
        ////        includePracCodes.AddRange(this.QueueDataLayer.GetAllPracticeCodes(this.ActiveLogin.UserID).Select(p => p.PracCode));
        ////    }
        ////    else if (this.ddlPracGroup.SelectedValue != "all")
        ////    {
        ////        includePracCodes.Add(this.ddlPracGroup.SelectedValue);
        ////    }

        ////    return this.QueueDataLayer.GetPGMProcessingQueue(userID, officeCode, statusTypeID, includePracCodes, sortNewestFirst);
        ////}

        ////private List<Memo> GetPGMPrimaryQueue()
        ////{
        ////    List<string> includePracCodes = new List<string>();

        ////    int? userID = this.ddlAssignedTo.SelectedValue == "me" ? (int?)this.ActiveLogin.UserID : null;
        ////    string officeCode = this.ddlAPOffice.SelectedValue;
        ////    int? statusTypeID = this.ddlStatus.SelectedValue == "all" ? null : (int?)Convert.ToInt32(this.ddlStatus.SelectedValue);
        ////    bool sortNewestFirst = Convert.ToBoolean(this.ddlSort.SelectedValue);

        ////    includePracCodes.AddRange(this.QueueDataLayer.GetPracticeCodes(this.ActiveLogin.UserID).Select(p => p.PracCode));
        ////    includePracCodes.AddRange(this.QueueDataLayer.GetBackupPracticeCodes(this.ActiveLogin.UserID).Select(p => p.PracCode));

        ////    return this.QueueDataLayer.GetPGMPrimaryQueue(userID, officeCode, statusTypeID, includePracCodes, sortNewestFirst);
        ////}

        ////private List<Memo> GetPGMBackupQueue()
        ////{
        ////    List<string> includePracCodes = new List<string>();

        ////    int? userID = this.ddlAssignedTo.SelectedValue == "me" ? (int?)this.ActiveLogin.UserID : null;
        ////    string officeCode = this.ddlAPOffice.SelectedValue;
        ////    int? statusTypeID = this.ddlStatus.SelectedValue == "all" ? null : (int?)Convert.ToInt32(this.ddlStatus.SelectedValue);
        ////    bool sortNewestFirst = Convert.ToBoolean(this.ddlSort.SelectedValue);

        ////    includePracCodes.AddRange(this.QueueDataLayer.GetBackupPracticeCodes(this.ActiveLogin.UserID).Select(p => p.PracCode));

        ////    return this.QueueDataLayer.GetPGMBackupQueue(userID, officeCode, statusTypeID, includePracCodes, sortNewestFirst);
        ////}

        //#endregion

        //#region Dual Role Queues

        ////private List<Memo> GetDualRolePrimaryQueue()
        ////{
        ////    int? userID = this.ddlAssignedTo.SelectedValue == "me" ? (int?)this.ActiveLogin.UserID : null;
        ////    int? statusTypeID = this.ddlStatus.SelectedValue == "all" ? null : (int?)Convert.ToInt32(this.ddlStatus.SelectedValue);
        ////    string officeCode = this.ddlAPOffice.SelectedValue;
        ////    List<string> includePracCodes = new List<string>();
        ////    if (this.ddlPracGroup.SelectedValue == "my")
        ////    {
        ////        includePracCodes.AddRange(this.QueueDataLayer.GetAllPracticeCodes(this.ActiveLogin.UserID).Select(p => p.PracCode));
        ////    }
        ////    else if (this.ddlPracGroup.SelectedValue != "all")
        ////    {
        ////        includePracCodes.Add(this.ddlPracGroup.SelectedValue);
        ////    }
        ////    bool sortNewestFirst = Convert.ToBoolean(this.ddlSort.SelectedValue);

        ////    return this.QueueDataLayer.GetDualRolePrimaryQueue(userID, statusTypeID, officeCode, includePracCodes, sortNewestFirst);
        ////}

        ////private List<Memo> GetDualRoleProcessingQueue()
        ////{
        ////    int? userID = this.ddlAssignedTo.SelectedValue == "me" ? (int?)this.ActiveLogin.UserID : null;
        ////    int? statusTypeID = this.ddlStatus.SelectedValue == "all" ? null : (int?)Convert.ToInt32(this.ddlStatus.SelectedValue);
        ////    string officeCode = this.ddlAPOffice.SelectedValue;
        ////    List<string> includePracCodes = new List<string>();
        ////    includePracCodes.AddRange(this.QueueDataLayer.GetAllPracticeCodes(this.ActiveLogin.UserID).Select(p => p.PracCode));
        ////    bool sortNewestFirst = Convert.ToBoolean(this.ddlSort.SelectedValue);

        ////    return this.QueueDataLayer.GetDualRoleProcessingQueue(userID, statusTypeID, officeCode, includePracCodes, sortNewestFirst);
        ////}

        ////private List<Memo> GetDualRoleBackupQueue()
        ////{
        ////    int? userID = this.ddlAssignedTo.SelectedValue == "me" ? (int?)this.ActiveLogin.UserID : null;
        ////    int? statusTypeID = this.ddlStatus.SelectedValue == "all" ? null : (int?)Convert.ToInt32(this.ddlStatus.SelectedValue);
        ////    string officeCode = this.ddlAPOffice.SelectedValue;
        ////    List<string> includePracCodes = new List<string>();
        ////    includePracCodes.AddRange(this.QueueDataLayer.GetAllPracticeCodes(this.ActiveLogin.UserID).Select(p => p.PracCode));
        ////    bool sortNewestFirst = Convert.ToBoolean(this.ddlSort.SelectedValue);

        ////    return this.QueueDataLayer.GetDualRoleBackupQueue(userID, statusTypeID, officeCode, includePracCodes, sortNewestFirst);
        ////}

        //#endregion

        //#region Adversary Queues

        ////private List<Memo> GetAdversaryProcessingQueue()
        ////{
        ////    List<string> includePracCodes = new List<string>();
        ////    string officeCode = this.ddlAPOffice.SelectedValue;
        ////    int? userID = this.ddlAssignedTo.SelectedValue == "me" ? (int?)this.ActiveLogin.UserID : null;
        ////    bool sortNewestFirst = Convert.ToBoolean(this.ddlSort.SelectedValue);
            
        ////    if (this.ddlPracGroup.SelectedValue == "my")
        ////    {
        ////        includePracCodes.AddRange(this.QueueDataLayer.GetPracticeCodes(this.ActiveLogin.UserID).Select(p => p.PracCode));
        ////    }
        ////    else if (this.ddlPracGroup.SelectedValue != "all")
        ////    {
        ////        includePracCodes.Add(this.ddlPracGroup.SelectedValue);
        ////    }

        ////    return this.QueueDataLayer.GetAdversaryProcessingQueue(userID, officeCode, includePracCodes, sortNewestFirst);
        ////}

        ////private List<Memo> GetAdversaryPrimaryQueue()
        ////{
        ////    List<string> includePracCodes = new List<string>();
        ////    string officeCode = this.ddlAPOffice.SelectedValue;
        ////    int? userID = this.ddlAssignedTo.SelectedValue == "me" ? (int?)this.ActiveLogin.UserID : null;
        ////    bool sortNewestFirst = Convert.ToBoolean(this.ddlSort.SelectedValue);
        ////    int? statusTypeID = this.ddlStatus.SelectedValue == "all" ? null : (int?)Convert.ToInt32(this.ddlStatus.SelectedValue);

        ////    if (this.ddlPracGroup.SelectedValue == "my")
        ////    {
        ////        includePracCodes.AddRange(this.QueueDataLayer.GetPracticeCodes(this.ActiveLogin.UserID).Select(p => p.PracCode));
        ////    }
        ////    else if (this.ddlPracGroup.SelectedValue != "all")
        ////    {
        ////        includePracCodes.Add(this.ddlPracGroup.SelectedValue);
        ////    }

        ////    return this.QueueDataLayer.GetAdversaryPrimaryQueue(userID, statusTypeID, officeCode, includePracCodes, sortNewestFirst);
        ////}

        //#endregion

        #region Page Modes

        public enum PageMode
        {
            DEFAULT = 0,
            ADVERSARY,
            DUAL,
            PGM,
            AP
        }

        private PageMode _currentPageMode = PageMode.DEFAULT;

        public PageMode CurrentPageMode
        {
            get
            {
                if (_currentPageMode == PageMode.DEFAULT)
                {
                    _currentPageMode = this.GetCurrentPageMode();
                }
                return _currentPageMode;
            }
        }

        private PageMode GetCurrentPageMode()
        {
            PageMode mode = PageMode.DEFAULT;
            if (this.IsAdversary)
            {
                mode = PageMode.ADVERSARY;
            }
            else if (this.IsPGM && this.IsAP)
            {
                mode = PageMode.DUAL;
            }
            else if (this.IsPGM)
            {
                mode = PageMode.PGM;
            }
            else if (this.IsAP)
            {
                mode = PageMode.AP;
            }
            return mode;
        }

        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            this.ShowAllControls();
        }

        /// <summary>
        /// reset all controls to visible they will hide themselves in prerender 
        /// if they need to 
        /// </summary>
        private void ShowAllControls()
        {
            this.pnlBackup.Visible = true;
            this.pnlPrimary.Visible = true;
            this.pnlProcessing.Visible = true;
            this.lblCount.Visible = true;
        }

        #endregion

        #region Control PreRender

        protected void lblCount_PreRender(object sender, EventArgs e)
        {
            if (this.Impersonating)
            {
                this.lblCount.ForeColor = System.Drawing.Color.Green;
                this.lblCount.Text = string.Format("Showing queue for {0} - {1} memos awaiting approval", 
                    this.ImpersonateLogin.Login, 
                    this.PrimaryQueue == null ? 0 : this.PrimaryQueue.Count);
            }
            else if ((this.PrimaryQueue != null && this.PrimaryQueue.Count > 0)
                && (this.BackupQueue != null && this.BackupQueue.Count > 0))
            {
                this.lblCount.ForeColor = System.Drawing.Color.Black;
                this.lblCount.Text = string.Format("{0} total items were found in your queues that are awaiting approval",  this.PrimaryQueue.Count);
            }
            else if ((this.PrimaryQueue == null || this.PrimaryQueue.Count == 0)
                && (this.BackupQueue == null || this.BackupQueue.Count == 0))
            {
                this.lblCount.ForeColor = System.Drawing.Color.Green;
                if (this.CurrentPageMode == PageMode.ADVERSARY)
                {
                    this.lblCount.Text = "There are no items in the Adversary Approval Queue";
                }
                else
                {
                    this.lblCount.Text = "There are no items in your queue";
                }
            }
            else
            {
                this.lblCount.Visible = false;
            }
        }

        protected void pnlHelp_PreRender(object sender, EventArgs e)
        {
            ((Panel)sender).Visible = (this.CurrentPageMode != PageMode.ADVERSARY);
        }

        protected void ddlAPOffice_Init(object sender, EventArgs e)
        {
            DropDownList ddl = (DropDownList)sender;
            ddl.DataSource = this.ApprovalDataLayer.FirmOffices.OrderBy(o => o.City);
            ddl.DataTextField = "City";
            ddl.DataValueField = "LocationCode";
            ddl.DataBind();
        }

        protected void repBackup_PreRender(object sender, EventArgs e)
        {
            this.repBackup.QueueDataLayer = this.QueueDataLayer;
            this.repBackup.Queue = this.BackupQueue;
            this.repBackup.DataBind();
        }

        protected void repPrimary_PreRender(object sender, EventArgs e)
        {
            this.repPrimary.QueueDataLayer = this.QueueDataLayer;
            this.repPrimary.Queue = this.PrimaryQueue;
            this.repPrimary.DataBind();
        }

        protected void repProcessing_PreRender(object sender, EventArgs e)
        {
            this.repProcessing.QueueDataLayer = this.QueueDataLayer;
            this.repProcessing.Queue = this.ProcessingQueue;
            this.repProcessing.DataBind();
        }

        protected void pnlBackup_PreRender(object sender, EventArgs e)
        {
            if (this.BackupQueue == null || this.BackupQueue.Count == 0)
            {
                ((Panel)sender).Visible = false;
            }
            else
            {
                this.aBackup.InnerHtml = string.Format("{0} items found in your Backup Queue (click to expand)", this.BackupQueue.Count);
            }
        }

        protected void pnlPrimary_PreRender(object sender, EventArgs e)
        {
            if (this.PrimaryQueue == null || this.PrimaryQueue.Count == 0)
            {
                ((Panel)sender).Visible = false;
            }
            else
            {
                if (this.CurrentPageMode == PageMode.ADVERSARY)
                {
                    this.aPrimary.InnerHtml = string.Format("{0} items found in the Adversary Approval Queue", this.PrimaryQueue.Count);
                }
                else
                {
                    this.aPrimary.InnerHtml = string.Format("{0} items found in your Primary Queue", this.PrimaryQueue.Count);
                }
            }
        }

        protected void pnlProcessing_PreRender(object sender, EventArgs e)
        {
            if (this.ProcessingQueue == null || this.ProcessingQueue.Count == 0)
            {
                ((Panel)sender).Visible = false;
            }
            else
            {
                if (this.CurrentPageMode == PageMode.ADVERSARY)
                {
                    this.aProcessing.InnerHtml = string.Format("{0} items found in the Adversary Processing Queue", this.ProcessingQueue.Count);
                }
                else
                {
                    this.aProcessing.InnerHtml = string.Format("{0} items have been approved", this.ProcessingQueue.Count);
                }
            }
        }

        #endregion

        #region Control Data Binding

        protected void ddlAPOffice_DataBound(object sender, EventArgs e)
        {
            if (this.CurrentPageMode == PageMode.AP || this.CurrentPageMode == PageMode.DUAL)
            {
                string myOfficeCode = Convert.ToString(this.ActiveLogin.Office);
                ListItem myOfficeItem = this.ddlAPOffice.Items.FindByValue(myOfficeCode);
                if (myOfficeItem != null)
                {
                    myOfficeItem.Selected = true;
                }

                if (this.CurrentPageMode == PageMode.AP)
                {
                    this.ddlAPOffice.Enabled = false;
                }
                else
                {
                    this.ddlAPOffice.Enabled = true;
                }
            }
        }

        protected void ddlPracGroup_Init(object sender, EventArgs e)
        {
            DropDownList ddl = (DropDownList)sender;

            if (this.CurrentPageMode == PageMode.PGM)
            {
                List<AdminPracCode> myPracCodes = this.QueueDataLayer.GetAllPracticeCodes(this.ActiveLogin.UserID);

                if (myPracCodes != null && myPracCodes.Count > 0)
                {
                    ddl.Items.Add(new ListItem("My Practice Groups", "my"));

                    ddl.DataSource = this.AdversaryDataLayer.GetPracticeGroupsList()
                        .Where(p => myPracCodes.Select(c => c.PracCode).Contains(p.matt_cat_code))
                        .OrderBy(p => p.cat_plus_desc);
                    ddl.DataTextField = "cat_plus_desc";
                    ddl.DataValueField = "matt_cat_code";
                }
                else
                {
                    ddl.Enabled = false;
                }
            }
            else
            {
                ddl.Items.Add(new ListItem("All Practice Groups", "all"));
                if (this.CurrentPageMode == PageMode.DUAL)
                {
                    ListItem my = new ListItem("My Practice Groups", "my");
                    my.Selected = true;
                    ddl.Items.Add(my);
                }

                ddl.DataSource = this.AdversaryDataLayer.GetPracticeGroupsList()
                    .OrderBy(p => p.cat_plus_desc);
                ddl.DataTextField = "cat_plus_desc";
                ddl.DataValueField = "matt_cat_code";
            }
            ddl.DataBind();
        }

        protected void ddlStatus_Init(object sender, EventArgs e)
        {
            DropDownList ddl = (DropDownList)sender;
            ddl.DataSource = TrackingStatusType.TrackingStatusTypes
                .Where(t => t.StatusTypeID != TrackingStatusType.NA)
                .OrderBy(t => t.StatusMessage);
            ddl.DataTextField = "Status";
            ddl.DataValueField = "StatusTypeID";
            ddl.DataBind();
        }

        #endregion

        #region Button Click etc...

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admin/Queue.aspx");
        }

        protected void refQueue_Refresh(object sender, EventArgs e)
        {
            this.lblRefresh.Visible = true;
            this.lblRefresh.Text = string.Format("Your queue was last refreshed {0}<br />", DateTime.Now.ToLongTimeString());
        }
        
        # endregion

        #region Data Layer

        Adversary.DataLayer.Queue _queueDataLayer = null;

        Adversary.DataLayer.Queue QueueDataLayer
        {
            get
            {
                if (this._queueDataLayer == null)
                {
                    this._queueDataLayer = new DataLayer.Queue();
                }
                return this._queueDataLayer;
            }
        }

        public override void Dispose()
        {
            if (this._queueDataLayer != null)
            {
                this._queueDataLayer.Dispose();
                this._queueDataLayer = null;
            }
            base.Dispose();
        }


        #endregion

        #region Init

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (this.Page.Master is AdminMaster)
            {
                ((AdminMaster)this.Page.Master).TabPageUrl = "/Admin/ApprovalDefault.aspx";
                ((AdminMaster)this.Page.Master).SubTabPageUrl = "/Admin/ApprovalDefault.aspx";
            }
        }

        #endregion

        protected void lblImpError_PreRender(object sender, EventArgs e)
        {
            if (!Impersonating && !string.IsNullOrWhiteSpace(ImpersonateName))
            {
                lblImpError.Text = "Could not show queue for " + ImpersonateName + "<br /><br />";
                lblImpError.Visible = true;
            }
            else
            {
                lblImpError.Visible = false;
            }
        }
    }
}