﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.Web.Pages;

namespace Adversary.Web.Admin
{
    public partial class FilesAdmin : AdminBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (ThisMemo != null)
            {
                this.pnlMain.Visible = true;
                this.pnlError.Visible = false;

                ucFilesControl.ScreeningMemoID = ThisMemo.ScrMemID;
                ucFilesControl.FileUploadDirectory = base.FileUploadDirectory;

                ucExistingFiles.ScreeningMemoID = ThisMemo.ScrMemID;
                ucExistingFiles.FileUploadDirectory = base.FileUploadDirectory;

                this.lblScreeningMemoNumber.Text = ThisMemo.ScrMemID.ToString();
                if (ThisMemo.Trackings.Count >= 1)
                {
                    if (ThisMemo.Trackings[0].SubmittedOn != null)
                    {
                        DateTime submittedOn = (DateTime)ThisMemo.Trackings[0].SubmittedOn;
                        this.lblSubmittedOn.Text = submittedOn.ToString();
                    }
                }
            }
            else
            {
                this.pnlMain.Visible = false;
                this.pnlError.Visible = true;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (this.Page.Master is AdminMaster)
            {
                ((AdminMaster)this.Page.Master).TabPageUrl = "/Admin/ApprovalDefault.aspx";
                ((AdminMaster)this.Page.Master).SubTabPageUrl = "/Admin/ApprovalDefault.aspx";
            }
        }
    }
}