﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.DataLayer;
using Adversary.Web.Controls;

namespace Adversary.Web.Admin
{
    public partial class ARApprovalUsers : Pages.AdminBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public void ucARApproval_AddClick(object sender, EventArgs e)
        {
            ARApprovalUser newUser = new ARApprovalUser();

            //get the entered ID
            //string personnelID = ((HiddenField)ucARApproval.FindControl("hidEmployeeCode")).Value;

            PersonAutoComplete pautoComplete = (PersonAutoComplete)ucARApproval.FindAddControl("ucEmployeeListControl");
            string personnelID = ((HiddenField)pautoComplete.FindControl("hidEmployeeCode")).Value;

            //get the user name and e-mail address

            DataLayer.HRDataService.Employee emp =
                AdversaryDataLayer.GetHREmployee(personnelID);
            if (emp != null)
            {
                if (AdversaryDataLayer.GetARApprovalUsers().Any(i => i.UserName.ToUpperInvariant().Equals(emp.ADLogin.ToUpperInvariant())))
                {
                    ucErrorSummary.DisplayMessage = "That employee already exists.";
                    ucErrorSummary.DisplayType = Web.Controls.MessageDisplayType.DisplayWarning;
                    ucErrorSummary.HideIcon = true;
                    upnlErrors.Update();
                    
                }
                else
                {
                    

                    newUser.UserName = emp.ADLogin.ToUpperInvariant();
                    newUser.EmailAddress = emp.CompanyEmailAddress;
                    Adversary.DataLayer.ARApprovalUsers arApproval = new DataLayer.ARApprovalUsers();

                    ARApprovalUser createdUser = arApproval.Add(newUser);

                    ucErrorSummary.DisplayMessage = String.Format("Successfully added new user {0} to the list of AR Approvers", newUser.UserName);
                    ucErrorSummary.DisplayType = Web.Controls.MessageDisplayType.DisplayInfo;
                    ucErrorSummary.HideIcon = true;
                    upnlErrors.Update();


                }
            }
            




        }

        public void ucARApproval_DeleteClick(object sender, EventArgs e)
        {
            HiddenField hidDelete = (HiddenField)ucARApproval.FindDeleteControl("hidDeleteARApprover");
            int intApproverID;
            Int32.TryParse(hidDelete.Value, out intApproverID);
            Adversary.DataLayer.ARApprovalUsers ar = new DataLayer.ARApprovalUsers();
            ar.Delete(intApproverID);
        }

        public void ucARApproval_RowCommand(object sender, EventArgs e)
        {

        }

        public void ucARApproval_OnPreRender(object sender, EventArgs e)
        {
            ucARApproval_DataBind();
        }

        private void ucARApproval_DataBind()
        {
            ucARApproval.DataSource = AdversaryDataLayer.GetARApprovalUsers();
            ucARApproval.DataBind();
        }

        public void ucEmployeeListControl_OnAutoCompletePopulated(object sender, Adversary.Controls.PersonAutoCompletePopulatedEventArgs e)
        {
            string F = e.PersonnelID;
        }


        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (this.Page.Master is AdminMaster)
            {
                ((AdminMaster)this.Page.Master).TabPageUrl = "/Admin/AdminDefault.aspx";
                ((AdminMaster)this.Page.Master).SubTabPageUrl = "/Admin/AdminDefault.aspx";
            }
        }
    }
}