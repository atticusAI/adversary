﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true"
    CodeBehind="Approval.aspx.cs" Inherits="Adversary.Web.Admin.Approval" Theme="HH" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controls/ARTable.ascx" TagName="ARTable" TagPrefix="adv" %>
<%@ Register Src="~/Controls/ApproverComboBox.ascx" TagName="ApproverComboBox" TagPrefix="adv" %>
<%@ Register Src="~/Controls/ListContainerControl.ascx" TagName="ListContainerControl"
    TagPrefix="adv" %>
<%@ Register Src="~/Controls/MessageBox.ascx" TagName="MessageBox" TagPrefix="adv" %>
<%@ Register Src="~/Controls/PrintViewLink.ascx" TagName="PrintViewLink" TagPrefix="adv" %>
<asp:Content ID="cntHead" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" language="javascript">
        $('document').ready(function () {
            formValidation = hh.validateWithjQuery('#form1');
        });
    </script>
</asp:Content>
<asp:Content ID="cntMain" ContentPlaceHolderID="cphMainContent" runat="server">
    <span class="lbl" id="spnMemo" runat="server" onprerender="spnMemo_PreRender"></span>
    <asp:FormView ID="frmApproval" runat="server" DefaultMode="ReadOnly" OnPreRender="frmApproval_PreRender">
        <EmptyDataTemplate>
            <span class="error" id="spnEmpty" runat="server" onprerender="spnEmpty_PreRender"></span>
        </EmptyDataTemplate>
        <ItemTemplate>
            <asp:HiddenField ID="hidTrackingID" runat="server" Value='<%#Eval("TrackingID")%>' />
            <asp:Panel ID="pnlSubmittedOn" runat="server">
                <adv:ARTable ID="arBal" runat="server" OnPreRender="arBal_PreRender" />
                <table class="app">
                    <tr>
                        <td class="lbl">Client:
                        </td>
                        <td>
                            <span>
                                <%#GetClientDisplayName()%></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="lbl">Matter:
                        </td>
                        <td>
                            <span>
                                <%#GetMatterName(Container.DataItem)%></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="lbl">Current Status:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlStatus" runat="server" OnPreRender="ddlStatus_PreRender" />
                            &nbsp;&nbsp;
                            <asp:Button ID="btnStatus" runat="server" Text="Change" OnClick="btnStatus_Click" />
                        </td>
                    </tr>
                    <tr runat="server" visible="<%#IsRejectedMemo(Container.DataItem)%>">
                        <td class="lbl">This screening memo has a Modification Request and can be resubmitted.
                        </td>
                        <td>
                            <asp:CheckBox ID="cbRejectionHold" runat="server" AutoPostBack="true" OnCheckedChanged="cbRejectionHold_CheckedChanged"
                                Checked='<%#Convert.ToBoolean(Eval("RejectionHold"))%>' />
                        </td>
                    </tr>
                    <tr>
                        <td class="lbl">Lock this screening memo from editing:
                        </td>
                        <td>
                            <asp:CheckBox ID="cbLocked" runat="server" Text="" AutoPostBack="true" OnCheckedChanged="cbLocked_CheckedChanged"
                                Checked="<%#IsLockedMemo(Container.DataItem)%>" />
                            <asp:Label ID="lblLocked" runat="server" ForeColor="Red" Text="(Locked)" OnPreRender="lblLocked_PreRender"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <span class="lbl">Notes:</span><br />
                            <asp:TextBox ID="tbNotes" runat="server" TextMode="MultiLine" Text='<%#Eval("Notes")%>' Rows="8" Columns="20" />
                        </td>
                    </tr>
                </table>
                <hr />
            </asp:Panel>
            <asp:Panel ID="pnlAP" runat="server" Visible="<%#ShowAP(Container.DataItem)%>">
                <table class="app">
                    <tr>
                        <td class="lbl">Administrative Partner Approval<br />
                            Acknowledged By:
                        </td>
                        <td>
                            <adv:ApproverComboBox ID="acbAP" runat="server" OnSelectedIndexChanged="combo_SelectedIndexChanged"
                                AutoPostBack="true" OnPreRender="approverCombo_PreRender" ApproverComboBoxType="AdministrativePartner"
                                Enabled='<%#!IsComplete() && (IsAPEnabled() || IsAdversaryAdmin())%>' />
                        </td>
                    </tr>
                    <tr runat="server" visible="<%#RequiresRetainerApproval()%>">
                        <td class="lbl">
                            <%#GetRetainerLabel()%>
                        </td>
                        <td>
                            <asp:CheckBox ID="cbAPRetainerAcknowledged" runat="server" OnCheckedChanged="cbRetainer_CheckedChanged"
                                AutoPostBack="true" Checked="<%#RetainerAcknowledged(Container.DataItem)%>" Enabled='<%#(IsAPEnabled() || IsAdversaryAdmin())%>' />
                        </td>
                    </tr>
                    <tr>
                        <td class="lbl">AP Approval Status:
                        </td>
                        <td>
                            <span>
                                <%#GetAPStatus(Container.DataItem)%></span>
                        </td>
                    </tr>
                    <%--<tr>
                        <td colspan="2">
                            <uc:screen id="scrAP" runat="server" screenid="AP" />
                            Concidental ---^---but not ironic
                            scrap (v): Discard or remove from service (a retired, old, or inoperative vehicle, vessel, 
                                       or machine), esp. so as to convert it to scrap metal.
                            irony (n): The expression of one's meaning by using language that normally signifies the opposite, 
                                       typically for humorous or emphatic effect.
                        </td>
                    </tr>--%>
                    <tr>
                        <td class="lbl">Approved By:
                        </td>
                        <td>
                            <span>
                                <%#Eval("APSignature")%></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="lbl">Approval Date:
                        </td>
                        <td>
                            <span>
                                <%#Eval("APDate")%></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="lbl">Approval Signature for A/R over 90 Days:
                        </td>
                        <td>
                            <asp:TextBox ID="txtAPARSignature" runat="server" Text='<%#Eval("APARSignature")%>'
                                Enabled='<%#IsAPEnabled()%>' />
                        </td>
                    </tr>
                    <tr>
                        <td class="lbl">Exception approval for signed engagement letter:
                        </td>
                        <td>
                            <asp:TextBox ID="txtAPExceptionSignature" runat="server" Text='<%#Eval("APExceptionSignature")%>'
                                Enabled='<%#IsAPEnabled()%>' />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <span class="lbl">Additional Notes:</span><br />
                            <asp:TextBox ID="txtAPNotes" runat="server" TextMode="MultiLine" Text='<%#Eval("APNotes") %>'
                                Enabled='<%#IsAPEnabled()%>' />
                        </td>
                    </tr>
                    <tr id="trAPRejection" runat="server" visible="false">
                        <td colspan="2">AP Modification Request Notes<br />
                            <br />
                            <span>
                                <%#Eval("APRejectionReason")%></span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="btnAPApprove" runat="server" CommandArgument="AP" AutoPostBack="true"
                                OnClick="btnApproval_Click" OnPreRender="btnAPApprove_PreRender" Visible='<%#(IsAPEnabled() || IsAdversaryAdmin())%>' />
                            <asp:Button ID="btnAPReject" runat="server" Text="Request Modification of Memo" OnClientClick="showMPX('behReject'); return false;"
                                Visible='<%#!IsComplete() && (IsAPEnabled() || IsAdversaryAdmin())%>' />
                        </td>
                    </tr>
                </table>
                <br />
            </asp:Panel>
            <asp:Panel ID="pnlPGM" runat="server" Visible="<%#ShowPGM(Container.DataItem)%>">
                <table class="app">
                    <tr>
                        <td class="lbl">Practice Group Leader Approval<br />
                            Acknowledged By:
                        </td>
                        <td>
                            <adv:ApproverComboBox ID="acbPGM" runat="server" OnSelectedIndexChanged="combo_SelectedIndexChanged"
                                AutoPostBack="true" OnPreRender="approverCombo_PreRender" ApproverComboBoxType="PracticeGroupLeader"
                                Enabled='<%#!IsComplete() && (IsPGMEnabled() || IsAdversaryAdmin())%>' />
                        </td>
                    </tr>
                    <tr id="trPGMRetainerAcknowledged" runat="server">
                        <td class="lbl">
                            <%#GetRetainerLabel()%>
                        </td>
                        <td>
                            <asp:CheckBox ID="cbPGMRetainerAcknowledged" runat="server" OnCheckedChanged="cbRetainer_CheckedChanged"
                                AutoPostBack="true" Checked="<%#RetainerAcknowledged(Container.DataItem)%>" Enabled='<%#!IsComplete() && (IsPGMEnabled() || IsAdversaryAdmin()) %>' />
                        </td>
                    </tr>
                    <tr>
                        <td class="lbl">PGL Approval Status:
                        </td>
                        <td>
                            <span>
                                <%#GetPGLStatus(Container.DataItem)%></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="lbl">Approved By:
                        </td>
                        <td>
                            <span>
                                <%#Eval("PGMSignature")%></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="lbl">Approval Date:
                        </td>
                        <td>
                            <span>
                                <%#Eval("PGMDate")%></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="lbl">Approval Signature for A/R over 90 Days:
                        </td>
                        <td>
                            <asp:TextBox ID="txtPGMARSignature" runat="server" Text='<%#Eval("PGMARSignature")%>'
                                Enabled='<%#IsPGMEnabled()%>' />
                        </td>
                    </tr>
                    <tr>
                        <td class="lbl">Exception approval for signed engagement letter:
                        </td>
                        <td>
                            <asp:TextBox ID="txtPGMExceptionSignature" runat="server" Text='<%#Eval("PGMExceptionSignature")%>'
                                Enabled='<%#IsPGMEnabled()%>' />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <span class="lbl">Additional Notes:</span><br />
                            <asp:TextBox ID="txtPGMNotes" runat="server" TextMode="MultiLine" Text='<%#Eval("PGMNotes")%>'
                                Enabled='<%#IsPGMEnabled()%>' />
                        </td>
                    </tr>
                    <tr id="trPGMRejection" runat="server" visible="true">
                        <td colspan="2">
                            <span class="lbl">PGL Modification Request Notes:</span><br />
                            <span>
                                <%#Eval("PGMRejectionReason")%></span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="btnPGMApprove" runat="server" CommandArgument="PGM" OnClick="btnApproval_Click"
                                OnPreRender="btnPGMApprove_PreRender" Visible='<%#(IsPGMEnabled() || IsAdversaryAdmin())%>' />
                            <asp:Button ID="btnPGMReject" runat="server" Text="Request Modification of Memo"
                                OnClientClick="showMPX('behReject'); return false;" Visible='<%#(IsPGMEnabled() || IsAdversaryAdmin())%>' />
                        </td>
                    </tr>
                </table>
                <br />
            </asp:Panel>
            <asp:Panel ID="pnlPGM2" runat="server" Visible='<%#ShowPGM2(Container.DataItem)%>'>
                <table class="app">
                    <tr>
                        <td class="lbl">Second Practice Group Leader Approval<br />
                            Acknowledged By:
                        </td>
                        <td>
                            <adv:ApproverComboBox ID="acbPGM2" runat="server" OnSelectedIndexChanged="combo_SelectedIndexChanged"
                                AutoPostBack="true" OnPreRender="approverCombo_PreRender" ApproverComboBoxType="PracticeGroupLeader"
                                Enabled='<%#IsPGM2Enabled()%>' />
                        </td>
                    </tr>
                    <tr id="trPGM2RetainerAcknowledged" runat="server">
                        <td class="lbl">
                            <%#GetRetainerLabel()%>
                        </td>
                        <td>
                            <asp:CheckBox ID="cbPGM2RetainerAcknowledged" runat="server" OnCheckedChanged="cbRetainer_CheckedChanged"
                                AutoPostBack="true" Checked="<%#RetainerAcknowledged(Container.DataItem)%>" Enabled='<%#IsPGM2Enabled()%>' />
                        </td>
                    </tr>
                    <tr>
                        <td class="lbl">Second PGL Approval Status:
                        </td>
                        <td>
                            <span>
                                <%#GetPGL2Status(Container.DataItem)%></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="lbl">Approved By:
                        </td>
                        <td>
                            <span>
                                <%#Eval("PGM2Signature")%></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="lbl">Approval Date:
                        </td>
                        <td>
                            <span>
                                <%#Eval("PGM2Date")%></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="lbl">Approval Signature for A/R over 90 Days:
                        </td>
                        <td>
                            <asp:TextBox ID="txtPGM2ARSignature" runat="server" Text='<%#Eval("PGM2ARSignature")%>'
                                Enabled='<%#IsPGM2Enabled()%>' />
                        </td>
                    </tr>
                    <tr>
                        <td class="lbl">Exception approval for signed engagement letter:
                        </td>
                        <td>
                            <asp:TextBox ID="txtPGM2ExceptionSignature" runat="server" Text='<%#Eval("PGM2ExceptionSignature")%>'
                                Enabled='<%#IsPGM2Enabled()%>' />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <span class="lbl">Additional Notes:</span><br />
                            <asp:TextBox ID="txtPGM2Notes" runat="server" TextMode="MultiLine" Text='<%#Eval("PGM2Notes")%>'
                                Enabled='<%#IsPGM2Enabled()%>' />
                        </td>
                    </tr>
                    <tr id="tr2" runat="server" visible="true">
                        <td colspan="2">
                            <span class="lbl">Second PGL Modification Request Notes:</span><br />
                            <span>
                                <%#Eval("PGM2RejectionReason")%></span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="btnPGM2Approve" runat="server" CommandArgument="PGM2" OnClick="btnApproval_Click"
                                OnPreRender="btnPGM2Approve_PreRender" Visible='<%#IsPGM2Enabled()%>' />
                            <asp:Button ID="btnPGM2Reject" runat="server" Text="Request Modification of Memo"
                                OnClientClick="showMPX('behReject'); return false;" Visible='<%#IsPGM2Enabled()%>' />
                        </td>
                    </tr>
                </table>
                <br />
            </asp:Panel>

            <asp:Panel ID="pnlFin" runat="server" Visible='<%#ShowFin(Container.DataItem)%>'>
                <table class="app">
                    <tr>
                        <td class="lbl">Financial Partner Approval<br />
                            Acknowledged By:
                        </td>
                        <td>
                            <adv:ApproverComboBox ID="acbFinancial" runat="server" OnSelectedIndexChanged="combo_SelectedIndexChanged"
                                AutoPostBack="true" OnPreRender="approverCombo_PreRender" ApproverComboBoxType="PracticeGroupLeader"
                                Enabled='<%#IsFinEnabled()%>' />
                        </td>
                    </tr>
                    <tr id="trFinRetainerAcknowledged" runat="server">
                        <td class="lbl">
                            <%#GetRetainerLabel()%>
                        </td>
                        <td>
                            <asp:CheckBox ID="cbFinRetainerAcknowledged" runat="server" OnCheckedChanged="cbRetainer_CheckedChanged"
                                AutoPostBack="true" Checked="<%#RetainerAcknowledged(Container.DataItem)%>" Enabled='<%#IsFinEnabled()%>' />
                        </td>
                    </tr>
                    <tr>
                        <td class="lbl">Financial Partner Approval Status:
                        </td>
                        <td>
                            <span>
                                <%#GetFinancialStatus(Container.DataItem)%></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="lbl">Approved By:
                        </td>
                        <td>
                            <span>
                                <%#Eval("FinSignature")%></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="lbl">Approval Date:
                        </td>
                        <td>
                            <span>
                                <%#Eval("FinDate")%></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="lbl">Approval Signature for A/R over 90 Days:
                        </td>
                        <td>
                            <asp:TextBox ID="txtFinARSignature" runat="server" Text='<%#Eval("FinARSignature")%>'
                                Enabled='<%#IsFinEnabled()%>' />
                        </td>
                    </tr>
                    <tr>
                        <td class="lbl">Exception approval for signed engagement letter:
                        </td>
                        <td>
                            <asp:TextBox ID="txtFinExceptionSignature" runat="server" Text='<%#Eval("FinExceptionSignature")%>'
                                Enabled='<%#IsFinEnabled()%>' />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <span class="lbl">Additional Notes:</span><br />
                            <asp:TextBox ID="txtFinNotes" runat="server" TextMode="MultiLine" Text='<%#Eval("FinNotes")%>'
                                Enabled='<%#IsFinEnabled()%>' />
                        </td>
                    </tr>
                    <tr id="tr3" runat="server" visible="true">
                        <td colspan="2">
                            <span class="lbl">Financial Partner Modification Request Notes:</span><br />
                            <span>
                                <%#Eval("FinRejectionReason")%></span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="btnFinApprove" runat="server" CommandArgument="FIN" OnClick="btnApproval_Click"
                                OnPreRender="btnFinApprove_PreRender" Visible='<%#IsFinEnabled()%>' />
                            <asp:Button ID="btnFinReject" runat="server" Text="Request Modification of Memo"
                                OnClientClick="showMPX('behReject'); return false;" Visible='<%#IsFinEnabled()%>' />
                        </td>
                    </tr>
                </table>
                <br />
            </asp:Panel>

            <asp:Panel ID="pnlAdversary" runat="server">
                <table class="app">
                    <tr id="trARApproval" runat="server" visible='<%#ShowTRARApproval(Container.DataItem)%>'>
                        <td class="lbl">Approval received for A/R over 90 Days:
                        </td>
                        <td>
                            <asp:CheckBox ID="cbARApproval" runat="server" Enabled='<%#(IsAdversaryAdmin() && !IsComplete())%>'
                                Checked='<%#Convert.ToBoolean(Eval("ARApprovalReceived"))%>' OnCheckedChanged="cbARApproval_CheckedChanged"
                                AutoPostBack="true" />
                            <asp:LinkButton ID="lbAR" runat="server" Text="Send request for approval" OnClientClick="showMPX('behAR'); return false;" />
                        </td>
                    </tr>
                    <tr>
                        <td class="lbl">Adversary Group Processing<br />
                            Processed By:
                        </td>
                        <td>
                            <adv:ApproverComboBox ID="acbAdversary" runat="server" OnSelectedIndexChanged="combo_SelectedIndexChanged"
                                AutoPostBack="true" OnPreRender="approverCombo_PreRender" ApproverComboBoxType="AdversaryGroup"
                                Enabled='<%#!IsComplete() && IsAdversary()%>' />
                        </td>
                    </tr>
                    <tr>
                        <td class="lbl">Adversary Approval Status:
                        </td>
                        <td>
                            <span>
                                <%#GetADVStatus(Container.DataItem)%></span>
                        </td>
                    </tr>
                    <tr id="trAdvAdminApproval" runat="server">
                        <td class="lbl">Adversary Approved:
                        </td>
                        <td>
                            <span>
                                <%#Eval("AdversarySignature")%>
                                (
                                <%#Eval("AdversaryAdminDate")%>
                                )</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="lbl">Opened:
                        </td>
                        <td>
                            <asp:TextBox ID="txtOpened" runat="server" Text='<%#Eval("Opened")%>' Enabled='<%#(IsAdversary() && !IsComplete())%>' />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <span class="lbl">Conflicts:</span><br />
                            <asp:TextBox ID="txtConflicts" runat="server" TextMode="MultiLine" Text='<%#Eval("Conflicts")%>'
                                Enabled='<%#(IsAdversary() && !IsComplete())%>' />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <span class="lbl">Fee Splits:</span><br />
                            <asp:TextBox ID="txtFeeSplits" runat="server" TextMode="MultiLine" Text='<%#Eval("FeeSplits")%>'
                                Enabled='<%#(IsAdversary() && !IsComplete())%>' />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <span class="lbl">Final Check:</span><br />
                            <asp:TextBox ID="txtFinalCheck" runat="server" TextMode="MultiLine" Text='<%#Eval("FinalCheck")%>'
                                Enabled='<%#(IsAdversary() && !IsComplete())%>' />
                        </td>
                    </tr>
                    <tr runat="server" visible='<%#(!string.IsNullOrWhiteSpace(Convert.ToString(Eval("AdversaryRejectionReason"))))%>'>
                        <td colspan="2">
                            <div class="app">
                                <span class="lbl">Adversary Modification Request Notes:</span><br />
                                <span>
                                    <%#Eval("AdversaryRejectionReason")%></span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="btnAdvAdminApprove" runat="server" CommandArgument="AdvAdmin" OnClick="btnApproval_Click"
                                Text="Approve for Processing" OnPreRender="btnAdvAdminApprove_PreRender" Visible='<%#(IsAdversary() && !IsSubmittedToAdversary() && !IsComplete())%>' />
                            <asp:Button ID="btnComplete" runat="server" Text="Mark Complete" OnPreRender="btnComplete_PreRender"
                                OnClientClick="return confirm('Are you sure you would like to set this memo\'s status to complete and remove it form the queue?');"
                                Visible='<%#(IsAdversary() && IsSubmittedToAdversary() && !IsComplete())%>' />
                            <asp:Button ID="btnAdvAdminReject" runat="server" Text="Request Modification of Memo"
                                OnClientClick="showMPX('behReject'); return false;" Visible='<%#(!IsComplete() && IsAdversary())%>'
                                Enabled='<%#IsAdversaryAdmin()%>' />
                        </td>
                    </tr>
                </table>
                <br />
                <table class="app">
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="lblCMTimeStamp" runat="server" Visible='<%#ShowClientMatterNumberStatus(Container.DataItem)%>'
                                Text='<%#GetClientMatterNumberStatus(Container.DataItem)%>' /><br />
                            <adv:ListContainerControl ID="cmList" runat="server" Text="Add Client Matter Number"
                                AutoGenerateColumns="false" OnPreRender="cmList_PreRender" OnRowCommand="cmList_RowCommand"
                                OnAddClick="cmList_AddClick" OnDeleteClick="cmList_DeleteClick" PopupCssClass="pop"
                                BackgroundCssClass="pop-back">
                                <EmptyDataTemplate>
                                    No Client Matter Numbers found for this memo.
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <span class="lbl">Client Matter Number:</span>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%#Eval("ClientMatterNumber1")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <InsertItemTemplate>
                                    <div>
                                        Add a new Client Matter Number to this Screening Memo<br />
                                        Client Matter Number:
                                        <asp:TextBox ID="txtClientMatterNumber" runat="server" />
                                    </div>
                                </InsertItemTemplate>
                                <DeleteItemTemplate>
                                    <div>
                                        <asp:HiddenField ID="hidClientMatterID" runat="server" Value='<%#Eval("ClientMatterID")%>' />
                                        Are you sure you want to delete this Client Matter Number?<br />
                                        Client Matter Number:
                                        <asp:Label ID="lblDel" runat="server" Text='<%#Eval("ClientMatterNumber1")%>' />
                                    </div>
                                </DeleteItemTemplate>
                            </adv:ListContainerControl>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <!-- Bottom form controls -->
                            <adv:PrintViewLink ID="lnkPrintView" runat="server" ScrMemID='<%#Eval("ScrMemID")%>' />
                            <br />
                            <asp:LinkButton ID="lbSend" runat="server" OnClick="lbSend_Click" OnPreRender="lbSend_PreRender"
                                Visible='<%#IsAdversary()%>' />
                        </td>
                    </tr>
                </table>
                <br />
            </asp:Panel>
        </ItemTemplate>
    </asp:FormView>
    <asp:LinkButton ID="btnPop" runat="server" Text="" />
    <asp:ModalPopupExtender ID="mpxReject" runat="server" TargetControlID="btnPop" PopupControlID="pnlReject"
        CancelControlID="btnRejectionCancel" BackgroundCssClass="pop-back" BehaviorID="behReject" />
    <asp:Panel ID="pnlReject" runat="server" CssClass="pop" OnPreRender="pnlReject_PreRender"
        Style="display: none;">
        <table>
            <tr>
                <td>
                    <span class="lbl">Please specify the modification request reason (this message will
                        be included in the message to the memo originator and originating attorney).</span>
                </td>
            </tr>
            <tr id="trAPRejectionReason" runat="server">
                <td>
                    <span class="lbl">AP Modification Request Reason:</span>
                    <asp:TextBox ID="tbAPRejectionReason" runat="server" TextMode="MultiLine" class="required"
                        title="* AP Modification Request Reason is required." />
                </td>
            </tr>
            <tr id="trPGMRejectionReason" runat="server">
                <td>
                    <span class="lbl">PGL Modification Request Reason:</span>
                    <asp:TextBox ID="tbPGMRejectionReason" runat="server" TextMode="MultiLine" class="required"
                        title="* PGL Modification Request Reason is required." />
                </td>
            </tr>
            <tr id="trPGM2RejectionReason" runat="server">
                <td>
                    <span class="lbl">Second PGL Modification Request Reason:</span>
                    <asp:TextBox ID="tbPGM2RejectionReason" runat="server" TextMode="MultiLine" class="required"
                        title="* Second PGL Modification Request Reason is required." />
                </td>
            </tr>
            <tr id="trFinRejectionReason" runat="server">
                <td>
                    <span class="lbl">Financial Partner Modification Request Reason:</span>
                    <asp:TextBox ID="tbFinrejectionReason" runat="server" TextMode="MultiLine" class="required"
                        title="* Financial Partner Modification Request Reason is required." />
                </td>
            </tr>
            <tr id="trAdvRejectionReason" runat="server">
                <td>
                    <span class="lbl">Adversary Modification Request Reason:</span>
                    <asp:TextBox ID="tbAdvRejectionReason" runat="server" TextMode="MultiLine" class="required"
                        title="* Adversary Modification Request Reason is required." />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:CheckBox ID="cbModalRejectionHold" runat="server" Text="This memo can be resubmitted by the orignator after amendments have been made"
                        Checked="true" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnRejectionSubmit" runat="server" Text="Request Modification and Send"
                        OnClick="btnRejectionSubmit_Click" />
                    <asp:Button ID="btnRejectionCancel" runat="server" Text="Cancel" /><br />
                    <asp:Panel ID="pnlRejectErrors" runat="server" CssClass="errdiv" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:ModalPopupExtender ID="mpxAR" runat="server" TargetControlID="btnPop" PopupControlID="pnlAR"
        CancelControlID="btnARCancel" BackgroundCssClass="pop-back" BehaviorID="behAR" />
    <asp:Panel ID="pnlAR" runat="server" CssClass="pop" Style="display: none;" OnPreRender="pnlAR_PreRender">
        <table>
            <tr>
                <td colspan="2">
                    <span class="lbl">Send Email Notification that Approval is required for A/R Balance
                        over 90 Days</span>
                </td>
            </tr>
            <tr>
                <td class="lbl">From:
                </td>
                <td>
                    <asp:Label ID="lblFrom" runat="server" OnPreRender="lblFrom_PreRender" />
                </td>
            </tr>
            <tr>
                <td class="lbl">To:
                </td>
                <td>
                    <asp:TextBox Width="472px" ID="tbTo" runat="server" CssClass="required" title="* To email address is required." /><br />
                    <span class="error">(Separate multiple email addresses with a semi-colon)</span>
                </td>
            </tr>
            <tr>
                <td class="lbl">Subject:
                </td>
                <td>
                    <asp:TextBox ID="tbSubject" runat="server" CssClass="required" title="* Subject is required." />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:TextBox ID="tbARApprovalEmail" runat="server" TextMode="MultiLine" CssClass="required"
                        title="* Approval email message is required." />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="btnARSubmit" runat="server" Text="Send Message" OnClick="btnARSubmit_Click" />
                    <asp:Button ID="btnARCancel" runat="server" Text="Cancel" /><br />
                    <asp:Panel ID="pnlARErrors" runat="server" CssClass="errdiv" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <adv:MessageBox ID="msgComplete" runat="server" CssClass="pop" BackgroundCssClass="pop-back"
        CancelButton="Cancel" OnOKClick="msgComplete_OKClick" Buttons="OKCancel" Icon="Warning"
        Message="Are you sure you want to mark this memo as complete and remove it from the queue?" />
</asp:Content>
