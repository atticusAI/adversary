﻿<%@ Page Theme="HH" Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master"
    AutoEventWireup="true" CodeBehind="ARApprovalUsers.aspx.cs" Inherits="Adversary.Web.Admin.ARApprovalUsers" %>

<%@ Register TagPrefix="uc" TagName="ListContainerControl" Src="~/Controls/ListContainerControl.ascx" %>
<%@ Register TagPrefix="uc" TagName="ErrorSummary" Src="~/Controls/ErrorSummary.ascx" %>
<%@ Register TagPrefix="uc" TagName="PersonDropDown" Src="~/Controls/PersonAutoComplete.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="Adversary.Controls" Namespace="Adversary.Controls" TagPrefix="advControls" %>
<asp:Content ID="cntMain" ContentPlaceHolderID="cphMainContent" runat="server">
    <span class="content">If additional A/R approval is needed, email notification will
        be sent to the following default users: </span>
    <br />
    <asp:UpdatePanel ID="upnlErrors" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
        <ContentTemplate>
            <uc:ErrorSummary ID="ucErrorSummary" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <uc:ListContainerControl ID="ucARApproval" runat="server" AutoGenerateColumns="false"
        OnAddClick="ucARApproval_AddClick" OnDeleteClick="ucARApproval_DeleteClick" OnPreRender="ucARApproval_OnPreRender"
        BorderColor="Navy" BorderWidth="1px" OnRowCommand="ucARApproval_RowCommand" Text="Add New Record"
        PopupCssClass="pop" BackgroundCssClass="pop-back">
        <EmptyDataTemplate>
            No A/R Approvers have been identified.
        </EmptyDataTemplate>
        <Columns>
            <asp:BoundField HeaderText="ID #" DataField="ApprovalUserID" />
            <asp:BoundField HeaderText="User Name" DataField="UserName" />
            <asp:BoundField HeaderText="Email Address" DataField="EmailAddress" />
        </Columns>
        <DeleteItemTemplate>
            Are you sure you wish to delete this record?<br />
            <asp:HiddenField ID="hidDeleteARApprover" runat="server" Value='<%#Eval("ApprovalUserID")%>' />
            UserName:
            <asp:Label ID="lblUserName" runat="server" Text='<%#Eval("UserName")%>' />
        </DeleteItemTemplate>
        <InsertItemTemplate>
            Please choose an employee to grant A/R Approval permissions:<br />
            (enter an employee last name or personnel ID number)<br />
            <uc:PersonDropDown ID="ucEmployeeListControl" runat="server" RaiseEventOnItemSelected="false"
                PopulatedEventCausesValidation="false" ServiceMethod="TimekeeperService" />
        </InsertItemTemplate>
    </uc:ListContainerControl>
</asp:Content>
