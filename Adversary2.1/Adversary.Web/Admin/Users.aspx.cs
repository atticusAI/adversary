﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.DataLayer;
using Adversary.Web.Controls;
using Adversary.Controls;
using Adversary.DataLayer.HRDataService;
using System.Data.Linq;

namespace Adversary.Web.Admin
{
    public partial class Users : Pages.AdminBasePage
    {
        private List<Adversary.DataLayer.HRDataService.FirmOffice> _Offices;
        private List<Adversary.DataLayer.HRDataService.FirmOffice> Offices
        {
            get
            {
                if (_Offices == null)
                    _Offices = AdversaryDataLayer.FirmOffices;
                return _Offices;
            }
        }

        private List<Adversary.DataLayer.AdversaryDataService.PracticeCode> _PracticeCodes;
        private List<Adversary.DataLayer.AdversaryDataService.PracticeCode> PracticeCodes
        {
            get
            {
                if (_PracticeCodes == null)
                    _PracticeCodes = AdversaryDataLayer.PracticeCodes;
                return _PracticeCodes;
            }
        }

        private List<Adversary.DataLayer.AdminLogin> _AllLogins;
        private List<Adversary.DataLayer.AdminLogin> AllLogins
        {
            get
            {
                if (_AllLogins == null)
                {
                    Adversary.DataLayer.Admin.UserAccounts _Users = new DataLayer.Admin.UserAccounts();
                    _AllLogins = _Users.GetAdminLogins().OrderBy(F => F.Login).ToList();
                }
                return _AllLogins;
            }
        }

        private Label LblPrimary
        {
            get { return new Label() { Text = "(P)", ForeColor = System.Drawing.Color.Orange }; }
        }
        private Label LblSecondary
        {
            get { return new Label() { Text = "(S)", ForeColor = System.Drawing.Color.Orange }; }
        }

        private Image ImgChecked
        {
            get { return new Image() { ImageUrl = "~/Images/cb_checked.jpg", Width = Unit.Pixel(12), Height = Unit.Pixel(12) }; }
        }
        private Image ImgUnchecked
        {
            get { return new Image() { ImageUrl = "~/Images/cb_empty.jpg", Width = Unit.Pixel(12), Height = Unit.Pixel(12) }; }
        }

        private string[] PRACTICE_CODE_SEPARATOR = new string[] { "$$" };

        private Label GetLoginPracticeGroups(List<AdminPracCode> Codes)
        {
            Label lbl = new Label();
            foreach (AdminPracCode code in Codes)
            {
                try
                {
                    lbl.Controls.Add(new LiteralControl(
                        this.PracticeCodes.Where(P => P.matt_cat_code.Equals(code.PracCode)).First().cat_plus_desc)
                        );
                    lbl.Controls.Add((code.IsPrimary ?? false) ? LblPrimary : LblSecondary);
                }
                catch (Exception)
                {
                    lbl.Controls.Add(new LiteralControl("**NO PRACTICE CODE DESCRIPTION FOUND FOR " + code.PracCode));
                }
                lbl.Controls.Add(new LiteralControl("<br/>"));
            }
            return lbl;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            

            if (!Page.IsPostBack)
            {
                BindGrid();
                lbPracticeGroupsPrimary.DataSource = this.PracticeCodes;
                lbPracticeGroupsPrimary.DataTextField = "cat_plus_desc";
                lbPracticeGroupsPrimary.DataValueField = "matt_cat_code";
                lbPracticeGroupsPrimary.DataBind();

                lbPracticeGroupsSecondary.DataSource = this.PracticeCodes;
                lbPracticeGroupsSecondary.DataTextField = "cat_plus_desc";
                lbPracticeGroupsSecondary.DataValueField = "matt_cat_code";
                lbPracticeGroupsSecondary.DataBind();


                lbPracticeCodesPrimaryEdit.DataSource = this.PracticeCodes;
                lbPracticeCodesPrimaryEdit.DataTextField = "cat_plus_desc";
                lbPracticeCodesPrimaryEdit.DataValueField = "matt_cat_code";
                lbPracticeCodesPrimaryEdit.DataBind();

                lbPracticeCodesSecondaryEdit.DataSource = this.PracticeCodes;
                lbPracticeCodesSecondaryEdit.DataTextField = "cat_plus_desc";
                lbPracticeCodesSecondaryEdit.DataValueField = "matt_cat_code";
                lbPracticeCodesSecondaryEdit.DataBind();
            }
        }
      
        protected void gvUsers_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Adversary.DataLayer.AdminLogin _login = (Adversary.DataLayer.AdminLogin)e.Row.DataItem;
                e.Row.Cells[4].Text = this.Offices.Where(o => o.LocationCode == _login.Office.ToString()).First().LocationDescription;

                e.Row.Cells[5].Controls.Add(((_login.IsAdversary ?? false) || (_login.IsAdversaryAdmin ?? false)) ? this.ImgChecked : this.ImgUnchecked);

                e.Row.Cells[6].Controls.Add(((_login.IsAP ?? false) || (_login.IsPrimaryAP ?? false)) ? this.ImgChecked : this.ImgUnchecked);

                if (_login.IsPrimaryAP ?? false) e.Row.Cells[6].Controls.Add(this.LblPrimary);

                else if ((_login.IsAP ?? false) && (_login.IsPrimaryAP.HasValue && _login.IsPrimaryAP.Value == false))
                    e.Row.Cells[6].Controls.Add(this.LblSecondary);


                e.Row.Cells[7].Controls.Add((_login.IsPGM ?? false) ? this.ImgChecked : this.ImgUnchecked);

                e.Row.Cells[8].Controls.Add(GetLoginPracticeGroups(_login.AdminPracCodes.ToList()));

            }
        }

        protected void gvUsers_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            BindGrid();

            string __commandName = e.CommandName;
            string __commandArgument = e.CommandArgument.ToString();
            AdminLogin __editLogin = AdversaryDataLayer.GetAdminLogin(Convert.ToInt32(__commandArgument));
            if (__commandName.Equals("EditUser"))
            {
                //get the admin login info
                
                if(__editLogin!=null)
                {
                    hidUserIDEdit.Value = __editLogin.UserID.ToString();
                    lblUserNetLogin.Text = __editLogin.Login;
                    advOfficeDropDownEdit.SelectedOffice = __editLogin.Office.ToString();
                    //advOfficeDropDown.ddlOffice.SelectedIndex = -1;
                    //advOfficeDropDownEdit.ddlOffice.Items.FindByValue(__editLogin.Office.ToString()).Selected = true;

                    cbAdversaryEdit.Checked = (__editLogin.IsAdversary ?? false);
                    cbAdversaryAdminEdit.Checked = (__editLogin.IsAdversaryAdmin ?? false);
                    cbIsAPEdit.Checked = (__editLogin.IsAP ?? false);
                    cbIsPGLEdit.Checked = (__editLogin.IsPGM ?? false);

                    if (__editLogin.IsPrimaryAP ?? false) ddlAPTypeEdit.SelectedIndex = 0;

                    //populate all the controls on the modal popup
                    //SetSelectedCodes(ref lbPracticeCodesPrimaryEdit, __editLogin.AdminPracCodes.Where(A => A.IsPrimary == true).ToList());
                    //SetSelectedCodes(ref lbPracticeCodesSecondaryEdit, __editLogin.AdminPracCodes.Where(A => A.IsPrimary == false).ToList());

                    //SetSelectedCodes(ref lbPracticeCodesPrimaryEdit, 
                    //    ref lbPracticeCodesPrimaryEditSelected, 
                    //    __editLogin.AdminPracCodes.Where(A => A.IsPrimary == true).ToList());

                    //SetSelectedCodes(ref lbPracticeCodesSecondaryEdit,
                    //    ref lbPracticeCodesSecondaryEditSelected,
                    //    __editLogin.AdminPracCodes.Where(A => A.IsPrimary == false).ToList());

                    //get new lists removing/adding practice codes and then re-bind
                    List<DataLayer.AdversaryDataService.PracticeCode> _primaryNotSelected = _PracticeCodes;
                    List<DataLayer.AdversaryDataService.PracticeCode> _secondaryNotSelected = _PracticeCodes;

                    List<string> _primarySelectedCodes = new List<string>();
                    List<string> _secondarySelectedCodes = new List<string>();
                    foreach (AdminPracCode code in __editLogin.AdminPracCodes.Where(p => p.IsPrimary == true).ToArray())
                    {
                        _primarySelectedCodes.Add(code.PracCode);
                    }
                    foreach (AdminPracCode code in __editLogin.AdminPracCodes.Where(p => p.IsPrimary == false).ToArray())
                    {
                        _secondarySelectedCodes.Add(code.PracCode);
                    }

                    List<DataLayer.AdversaryDataService.PracticeCode> _primarySelected = (from P in _PracticeCodes
                                                                                          where _primarySelectedCodes.Contains(P.matt_cat_code)
                                                                                          select P).ToList();

                    List<DataLayer.AdversaryDataService.PracticeCode> _secondarySelected = (from P in _PracticeCodes
                                                                                            where _secondarySelectedCodes.Contains(P.matt_cat_code)
                                                                                            select P).ToList();

                    _primaryNotSelected = _primaryNotSelected.Except(_primarySelected).ToList();
                    _secondaryNotSelected = _secondaryNotSelected.Except(_secondarySelected).ToList();

                    lbPracticeCodesPrimaryEdit.DataSource = _primaryNotSelected;
                    lbPracticeCodesPrimaryEdit.DataBind();

                    lbPracticeCodesPrimaryEditSelected.DataSource = _primarySelected;
                    lbPracticeCodesPrimaryEditSelected.DataTextField = "cat_plus_desc";
                    lbPracticeCodesPrimaryEditSelected.DataValueField = "matt_cat_code";
                    lbPracticeCodesPrimaryEditSelected.DataBind();

                    lbPracticeCodesSecondaryEdit.DataSource = _secondaryNotSelected;
                    lbPracticeCodesSecondaryEdit.DataTextField = "cat_plus_desc";
                    lbPracticeCodesSecondaryEdit.DataValueField = "matt_cat_code";
                    lbPracticeCodesSecondaryEdit.DataBind();

                    lbPracticeCodesSecondaryEditSelected.DataSource = _secondarySelected;
                    lbPracticeCodesSecondaryEditSelected.DataTextField = "cat_plus_desc";
                    lbPracticeCodesSecondaryEditSelected.DataValueField = "matt_cat_code";
                    lbPracticeCodesSecondaryEditSelected.DataBind();

                    foreach (Adversary.DataLayer.AdversaryDataService.PracticeCode code in _primarySelected)
                    {
                        hidPracticeCodesPrimaryEditSelected.Value += code.matt_cat_code + "$$";
                    }
                    
                    foreach (Adversary.DataLayer.AdversaryDataService.PracticeCode code in _secondarySelected)
                    {
                        hidPracticeCodesSecondaryEditSelected.Value += code.matt_cat_code + "$$";
                    }
                    
                    mdlEditUser.Show();
                }
            }
            else if (__commandName.Equals("DeleteUser"))
            {
                hidUserIDDelete.Value = __commandArgument;
                lblDeleteUser.Text = __editLogin.Login;
                mdlDeleteUser.Show();
            }
            gvUsers.EditIndex = -1;
        }

        protected void gvUsers_RowEditing(object sender, GridViewEditEventArgs e) { }
        
        protected void gvUsers_RowDeleting(object sender, GridViewDeleteEventArgs e) { }

        protected void btnAddUser_Click(object sender, EventArgs e)
        {
            string primarySelected = hidPracticeGroupsPrimarySelected.Value;
            string secondarySelected = hidPracticeGroupsSecondarySelected.Value;

            string userEmployeeID = ((HiddenField)ucEmployee.FindControl("hidEmployeeCode")).Value;
            Adversary.DataLayer.HRDataService.Employee employee = AdversaryDataLayer.GetHREmployee(userEmployeeID);

            if (employee == null || string.IsNullOrWhiteSpace(employee.ADLogin))
            {
                ucErrorControl.DisplayMessage = "User creation failed.";
                ucErrorControl.DisplayType = MessageDisplayType.DisplayError;
            }
            else
            {
                string officeCode = advOfficeDropDown.ddlOffice.SelectedValue;

                if (cbIsAdversaryAdmin.Checked && !cbIsAdversary.Checked) cbIsAdversary.Checked = true;

                string APType = ddlAPType.SelectedValue;

                Adversary.DataLayer.AdminLogin newLogin = new AdminLogin();
                newLogin.IsAdversary = cbIsAdversary.Checked;
                newLogin.IsAdversaryAdmin = cbIsAdversaryAdmin.Checked;
                newLogin.IsAP = cbIsAP.Checked;
                newLogin.IsPrimaryAP = APType.Equals("Primary");
                newLogin.Office = Convert.ToInt32(officeCode);
                newLogin.Email = employee.CompanyEmailAddress;
                newLogin.Login = employee.ADLogin.ToUpperInvariant();

                if (lbPracticeGroupsPrimary != null && lbPracticeGroupsSecondary != null)
                {
                    //if (lbPracticeGroupsPrimary.SelectedIndex > -1)
                    //if(__primarySelected.Length > 0)
                    //{
                    //_newLogin.AdminPracCodes.AddRange(GetSelectedPracticeCodes(lbPracticeGroupsPrimary, true));
                    newLogin.AdminPracCodes.AddRange(
                        GetSelectedPracticeCodes(
                            primarySelected.Split(PRACTICE_CODE_SEPARATOR, StringSplitOptions.RemoveEmptyEntries),
                                    true)
                                    );
                    //}
                    //if (lbPracticeGroupsSecondary.SelectedIndex > -1)
                    //{
                    //_newLogin.AdminPracCodes.AddRange(GetSelectedPracticeCodes(lbPracticeGroupsSecondary, false));
                    newLogin.AdminPracCodes.AddRange(
                        GetSelectedPracticeCodes(
                            secondarySelected.Split(PRACTICE_CODE_SEPARATOR, StringSplitOptions.RemoveEmptyEntries),
                                false)
                                );
                    //}
                }

                Adversary.DataLayer.Admin.UserAccounts userAccounts = new DataLayer.Admin.UserAccounts();
                userAccounts.CreateAdminLogin(newLogin);
                ucErrorControl.DisplayMessage = String.Format("User {0} successfully created.", newLogin.Login);
                ucErrorControl.DisplayType = MessageDisplayType.DisplayValid;
            }
            BindGrid();
        }

        protected void btnCancelAddUser_Click(object sender, EventArgs e)
        {
            string f = ((Button)sender).NamingContainer.ID;
            mdlAddUser.Hide();
            BindGrid();
        }

        protected void btnEditUser_Click(object sender, EventArgs e)
        {
            string userEmployeeID = hidUserIDEdit.Value;
            string officeCode = advOfficeDropDownEdit.ddlOffice.SelectedValue;

            AdminLogin login = AdversaryDataLayer.GetAdminLogin(Convert.ToInt32(userEmployeeID));
            login.Office = Convert.ToInt32(officeCode);
            login.IsPGM = cbIsPGLEdit.Checked;
            login.IsAdversary = cbAdversaryEdit.Checked;
            login.IsAdversaryAdmin = cbAdversaryAdminEdit.Checked;
            login.IsAP = cbIsAPEdit.Checked;
            login.IsPrimaryAP = (ddlAPTypeEdit.SelectedValue.ToUpperInvariant().Contains("PRIMARY"));
            login.AdminPracCodes.Clear();
            if (lbPracticeCodesPrimaryEdit != null && lbPracticeCodesSecondaryEdit != null)
            {
                //if (lbPracticeCodesPrimaryEdit.SelectedIndex > -1)
                //{
                    //__login.AdminPracCodes.AddRange(GetSelectedPracticeCodes(lbPracticeCodesPrimaryEdit, true));
                    login.AdminPracCodes.AddRange(
                        GetSelectedPracticeCodes(
                        hidPracticeCodesPrimaryEditSelected.Value.Split(PRACTICE_CODE_SEPARATOR, StringSplitOptions.RemoveEmptyEntries), 
                        true)
                        );
                //}
                //if (lbPracticeCodesSecondaryEdit.SelectedIndex > -1)
                //{
                    //__login.AdminPracCodes.AddRange(GetSelectedPracticeCodes(lbPracticeCodesSecondaryEdit, false));
                    login.AdminPracCodes.AddRange(
                        GetSelectedPracticeCodes(
                        hidPracticeCodesSecondaryEditSelected.Value.Split(PRACTICE_CODE_SEPARATOR, StringSplitOptions.RemoveEmptyEntries), 
                        false)
                        );
                //}
            }
            Adversary.DataLayer.Admin.UserAccounts userAccounts = new DataLayer.Admin.UserAccounts();
            userAccounts.ChangeAdminLogin(login);
            ucErrorControl.DisplayMessage = String.Format("User {0} successfully changed.", login.Login);
            ucErrorControl.DisplayType = MessageDisplayType.DisplayValid;
            BindGrid();
            //upnlGrid.Update();
        }

        protected void btnCancelEditUser_Click(object sender, EventArgs e)
        {
            mdlEditUser.Hide();
            BindGrid();
        }

        protected void btnDeleteUser_Click(object sender, EventArgs e)
        {
            try
            {
                int deletedUserID = Convert.ToInt32(hidUserIDDelete.Value);
                Adversary.DataLayer.Admin.UserAccounts userAccounts = new DataLayer.Admin.UserAccounts();
                userAccounts.DeleteAdminLogin(deletedUserID);
                ucErrorControl.DisplayType = MessageDisplayType.DisplayValid;
                ucErrorControl.DisplayMessage = "User successfully deleted.";
                //BindGrid();
            }
            catch (Exception ex)
            {
                ucErrorControl.DisplayMessage = "Error deleting user: " + ex.Message;
                ucErrorControl.DisplayType = MessageDisplayType.DisplayError;
            }
            BindGrid();
        }

        protected void btnDeleteUserCancel_Click(object sender, EventArgs e)
        {
            BindGrid();
            mdlDeleteUser.Hide();
        }

        private List<AdminPracCode> GetSelectedPracticeCodes(ListBox lb, bool isPrimary)
        {
            List<AdminPracCode> codes = new List<AdminPracCode>();
            foreach (ListItem item in lb.Items.Cast<ListItem>().Where(li=>li.Selected))
            {
                codes.Add(new AdminPracCode()
                {
                    IsPrimary = isPrimary,
                    PracCode = item.Value
                });
            }
            return codes;
        }
        private List<AdminPracCode> GetSelectedPracticeCodes(string[] codes, bool isPrimary)
        {
            List<AdminPracCode> returnCodes = new List<AdminPracCode>();
            foreach (string code in codes)
            {
                returnCodes.Add(new AdminPracCode()
                {
                    IsPrimary = isPrimary,
                    PracCode = code

                });
                
            }
            return returnCodes;
        }

        private void SetSelectedCodes(ref ListBox lb, List<AdminPracCode> selectedCodes)
        {
            foreach (ListItem item in lb.Items)
            {
                if (selectedCodes.Any(code => code.PracCode == item.Value))
                    item.Selected = true;
            }
            
        }

        private void SetSelectedCodes(ref ListBox unselectedLB, ref ListBox selectedLB, List<AdminPracCode> selectedCodes)
        {
            foreach (ListItem item in unselectedLB.Items)
            {
                if (selectedCodes.Exists(code => code.PracCode == item.Value))
                    unselectedLB.Items.Remove(item);
            }
            foreach (AdminPracCode adminpraccode in selectedCodes)
            {
                selectedLB.Items.Add(new ListItem(PracticeCodes.Find(pc => pc.matt_cat_code == adminpraccode.PracCode).cat_plus_desc,
                    adminpraccode.PracCode));
            }
        }

        private void BindGrid()
        {
            gvUsers.DataSource = this.AllLogins;
            gvUsers.DataKeyNames = new string[] { "UserID" };
            gvUsers.DataBind();
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (this.Page.Master is AdminMaster)
            {
                ((AdminMaster)this.Page.Master).TabPageUrl = "/Admin/AdminDefault.aspx";
                ((AdminMaster)this.Page.Master).SubTabPageUrl = "/Admin/AdminDefault.aspx";
            }
        }
    }
}