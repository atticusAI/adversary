﻿<%@ Page Title="" Language="C#" Theme="HH" MasterPageFile="~/Screening/Screening.Master"
    AutoEventWireup="true" CodeBehind="ClientAddress_Old.aspx.cs" Inherits="Adversary.Web.Screening.ClientAddress" %>

<%@ Register TagPrefix="ajax" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="cntHead" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            formValidation = hh.validateWithjQuery("#form1");

            jQuery.validator.addMethod("email_required", function (value, element) {
                var valid = false;
                if (element.type == "radio") {
                    var n = $(".email_required:checked").length;
                    valid = (n >= 1);
                }
                else if (element.type == "text") {
                    valid = (value.trim() != "");
                }
                return valid;
            }, "* You must either provide an email address or a reason why no email address is available.");
            
            $(".email_required input:radio").addClass("email_required");

            $(".email_required:radio").click(function () {
                toggleYesNo();
            });

            toggleYesNo();
        });

        function toggleYesNo() {
            var chk = $(".email_required:checked");
            if (chk) {
                if (chk.val() == "rbNoEmail") {
                    $("tr.has_email").hide();
                    $("tr.no_email").show();
                }
                else if (chk.val() == "rbYesEmail") {
                    $("tr.has_email").show();
                    $("tr.no_email").hide();
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="cntMain" ContentPlaceHolderID="cphMainContent" runat="server">
    <asp:HiddenField ID="hidAddressID" runat="server" />
    <table border="0" cellpadding="4" cellspacing="2">
        <tr>
            <td rowspan="3" valign="top" class="lbl required">
                Address:
            </td>
            <td>
                <asp:TextBox ID="tbAddress1" runat="server" SkinID="HH_Textbox" CssClass="required"
                    title="* You must enter at least one address line" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="tbAddress2" runat="server" SkinID="HH_Textbox" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="tbAddress3" runat="server" SkinID="HH_Textbox" />
            </td>
        </tr>
        <tr>
            <td class="lbl required">
                City:
            </td>
            <td>
                <asp:TextBox ID="tbCity" runat="server" CssClass="required" title="* You must enter a city." />
            </td>
        </tr>
        <tr>
            <td class="lbl required">
                State:
            </td>
            <td>
                <asp:TextBox ID="tbState" runat="server" MaxLength="2" CssClass="required"
                    title="* You must enter a State/Province" />
            </td>
        </tr>
        <tr>
            <td class="lbl">
                ZIP:
            </td>
            <td>
                <asp:TextBox ID="tbZIP" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="lbl required">
                Country:
            </td>
            <td>
                <asp:TextBox ID="tbCountry" runat="server" CssClass="required" title="* You must enter a Country" />
            </td>
        </tr>
        <tr>
            <td class="lbl required">
                Phone:
            </td>
            <td>
                <asp:TextBox ID="tbPhone" runat="server" CssClass="required" title="* You must enter a telephone number" />
            </td>
        </tr>
        <tr>
            <td class="lbl">
                Alt Phone:
            </td>
            <td>
                <asp:TextBox ID="tbAltPhone" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="lbl required">
                Does the client have an email address?
            </td>
            <td>
                <asp:RadioButton ID="rbYesEmail" runat="server" GroupName="email" Text="Yes" CssClass="email_required" />
                <asp:RadioButton ID="rbNoEmail" runat="server" GroupName="email" Text="No"  CssClass="email_required" />
            </td>
        </tr>
        <tr class="has_email" style="display:none;">
            <td class="lbl required">
                Email:
            </td>
            <td>
                <asp:TextBox ID="tbEmail" runat="server" CssClass="email_required" title="* You must provide an email address."></asp:TextBox>
            </td>
        </tr>

        <tr class="no_email" style="display:none;">
            <td class="lbl required">
                Missing Email Reason:
            </td>
            <td>
                <asp:TextBox ID="tbNoEmail" runat="server" CssClass="email_required" title="* You must provide a reason why no email address is available."></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="lbl">
                Fax:
            </td>
            <td>
                <asp:TextBox ID="tbFax" runat="server" />
            </td>
        </tr>
    </table>
    <asp:Panel ID="pnlErrs" runat="server" CssClass="errdiv">
    </asp:Panel>
    <asp:CustomValidator ID="valPage" runat="server" OnServerValidate="valPage_ServerValidate" />
</asp:Content>
<asp:Content ContentPlaceHolderID="cphLowerContent" ID="LowerContentPlaceHolder"
    runat="server">
    <ajax:ModalPopupExtender ID="mdlClientAddressList" runat="server" TargetControlID="lbAddressList"
        PopupControlID="pnlClientAddressForm" BackgroundCssClass="pop-back" CancelControlID="btnCancel" />
    <asp:Panel ID="pnlClientAddressForm" Width="35%" runat="server" CssClass="modalPopup">
        <div style="height:500px; overflow:auto;">
            <asp:Repeater runat="server" ID="rptClientAddresses">
                <ItemTemplate>
                    <asp:Panel ID="pnlClientAddressDisplay" runat="server" />
                    <asp:Panel ID="pnlClientAddressPhone" runat="server" />
                    <asp:Panel ID="pnlClientAddressSelectButton" runat="server" />
                    <hr />
                </ItemTemplate>
            </asp:Repeater>
        </div><br />
        <div style="text-align:right; padding:5px;">
            <asp:LinkButton ID="btnCancel" runat="server" Text="Cancel" />
        </div>
    </asp:Panel>
    <asp:LinkButton ID="lbAddressList" runat="server" Font-Bold="true" CausesValidation="false"></asp:LinkButton>
</asp:Content>
