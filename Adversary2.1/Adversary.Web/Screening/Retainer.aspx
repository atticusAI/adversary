﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Screening/Screening.Master" AutoEventWireup="true" CodeBehind="Retainer.aspx.cs" Inherits="Adversary.Web.Retainer" Theme="HH" %>
<%@ Register TagPrefix="ajax" Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/AccordionPane.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMainContent" runat="server">
    <table width="900">
        <tr>
            <td>Will this matter have a retainer? (new matters require a $5000 retainer)</td>
        </tr>
        <tr>
            <td>
                <asp:RadioButtonList ID="rblWillHaveRetainer" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Text="yes" />
                    <asp:ListItem Text="no" />
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td>
                <ajax:Accordion ID="accRetainer" runat="server" Width="100%" FadeTransitions="true" FramesPerSecond="60" Height="199"
                    RequireOpenedPane="false" TransitionDuration="200" SelectedIndex="-1" AutoSize="None">
                    <Panes>
                        
                        <ajax:AccordionPane ID="apRetainerYes" runat="server">
                            <Content>
                                Will the retainer be less than $5000?
                                <asp:RadioButtonList ID="rblRetainerYes" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="yes" />
                                    <asp:ListItem Text="no" />
                                </asp:RadioButtonList>
                                <ajax:Accordion ID="accRetainerYes" runat="server" Width="100%" FadeTransitions="true" FramesPerSecond="60" Height="199"
                RequireOpenedPane="false" TransitionDuration="200" SelectedIndex="-1" AutoSize="None">
                                    <Panes>
                                        <ajax:AccordionPane ID="apRetainerYesDescription" runat="server">
                                            <Content>
                                                Please explain why the retainer is less than $5000<br />
                                                <asp:TextBox runat="server" ID="tbRetainerDescYes" TextMode="MultiLine" Columns="50" Rows="4" />
                                            </Content>
                                        </ajax:AccordionPane>
                                        <ajax:AccordionPane ID="apRetainerYesBlank" runat="server"><Content><!--blank--></Content></ajax:AccordionPane>
                                 </Panes>
                                </ajax:Accordion>
                            </Content>
                        </ajax:AccordionPane>
                        
                        <ajax:AccordionPane ID="apRetainerNo" runat="server">
                            <Content>
                                Please explain why no retainer will be required<br />
                                <asp:TextBox runat="server" ID="tbRetainerDescNo" TextMode="MultiLine" Columns="50" Rows="4" />
                            </Content>
                        </ajax:AccordionPane>
                    </Panes>
                
                
                </ajax:Accordion>
            </td>
        </tr>
    </table>
</asp:Content>
