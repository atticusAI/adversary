﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Adversary.Utils;
using Adversary.Controls;
using AjaxControlToolkit;
using System.Web.UI.HtmlControls;


namespace Adversary.Web.Screening
{
    public partial class ClientDetails : ScreeningBasePage
    {
        public string SetAccordionPane;

        protected void trPendingBankruptcyRejected_PreRender(object sender, EventArgs e)
        {
            if (ThisMemo.ScrMemType == 6)
            {
                ((HtmlTableRow)sender).Visible = false;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            advRejectedClientName.RaiseEventOnItemSelected = false;
        }

        protected override void OnPreRenderComplete(EventArgs e)
        {
            base.OnPreRenderComplete(e);
            
            if (ThisMemo.ScrMemType == 6 || ThisMemo.ScrMemType == 3)
            {
                if (ThisMemo.Company.HasValue)
                {
                    if (Convert.ToBoolean(ThisMemo.Company))
                    {
                        rblClientTypeRejected.Items.FindByText("Organization").Selected = true;
                        accClientTypeRejected.SelectedIndex = 0;
                        tbCompanyNameRejected.Text = ThisMemo.NewClients[0].CName;
                    }
                }
                if (ThisMemo.Individual.HasValue)
                {
                    if (Convert.ToBoolean(ThisMemo.Individual))
                    {
                        rblClientTypeRejected.Items.FindByText("Individual").Selected = true;
                        accClientTypeRejected.SelectedIndex = 1;
                        tbFirstNameRejected.Text = ThisMemo.NewClients[0].FName;
                        tbMiddleNameRejected.Text = ThisMemo.NewClients[0].MName;
                        tbLastNameRejected.Text = ThisMemo.NewClients[0].LName;
                    }
                }
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            //set panes visible etc
            if (ThisMemo.ScrMemType == 6 || ThisMemo.ScrMemType == 3)
            {
                pnlClientDetails_RejectedMatter.Visible = true;
                pnlClientDetails_AllOthers.Visible = false;

                rblNewExistingClientRejected.HH_AddAccordionPaneJavascript(accRejected.ClientID);
                rblClientTypeRejected.HH_AddAccordionPaneJavascript(accClientTypeRejected.ClientID);

                //pnlClientAffiliateModify.Visible = false;
                //pnlClientAffiliateEntry.Visible = false;

                if (!String.IsNullOrEmpty(ThisMemo.ClientNumber))
                {
                    rblNewExistingClientRejected.Items.FindByText("Existing").Selected = true;
                    accRejected.SelectedIndex = 1;
                    advRejectedClientName.SelectedClientCode = ThisMemo.ClientNumber;
                }
                else if (ThisMemo.NewClients.Count > 0)
                {
                    rblNewExistingClientRejected.Items.FindByText("New").Selected = true;
                    accRejected.SelectedIndex = 0;
                    if (ThisMemo.Company.HasValue)
                    {
                        if (Convert.ToBoolean(ThisMemo.Company))
                        {
                            rblClientTypeRejected.Items.FindByText("Organization").Selected = true;
                            accClientTypeRejected.SelectedIndex = 0;
                        }
                    }
                    if (ThisMemo.Individual.HasValue)
                    {
                        if (Convert.ToBoolean(ThisMemo.Individual))
                        {
                            rblClientTypeRejected.Items.FindByText("Individual").Selected = true;
                            accClientTypeRejected.SelectedIndex = 1;
                        }
                    }
                }
                if (ThisMemo.PendingBankruptcy != null)
                {
                    string pendingBK = (Convert.ToBoolean(ThisMemo.PendingBankruptcy) ? "1" : "0");
                    rblPendingBankruptcyRejected.SelectedIndex = -1;
                    rblPendingBankruptcyRejected.Items.FindByValue(pendingBK).Selected = true;
                }
            }
            else
            {
                pnlClientDetails_AllOthers.Visible = true;
                pnlClientDetails_RejectedMatter.Visible = false;

                rblClientType.HH_AddAccordionPaneJavascript(accClientType.ClientID);

                //gvPartyRelationships.RowCreated += new GridViewRowEventHandler(gvPartyRelationships_RowCreated);


                //rblClientAffiliateModifyPartyType.AddAccordionPaneJavascript(accClientAffiliateModify.ClientID);

                //gvClientAffiliates.DataSource = ThisMemo.Parties.Where(P => P.PC.Equals(0) &&
                //    StringUtils.AffiliatePartyCodes.Contains(P.PartyRelationshipCode));
                //gvClientAffiliates.DataBind();


                if (!IsPostBack)
                {
                    if (ThisMemo.Company != null && ThisMemo.Individual != null)
                    {
                        rblClientType.SelectedIndex = -1;
                        if (Convert.ToBoolean(ThisMemo.Individual))
                        {
                            rblClientType.Items.FindByValue("Individual").Selected = true;
                            accClientType.SelectedIndex = 1;
                        }
                        else
                        {
                            rblClientType.Items.FindByValue("Organization").Selected = true;
                            accClientType.SelectedIndex = 0;

                        }
                    }

                    if (!String.IsNullOrEmpty(ThisMemo.ClientNumber))
                    {
                        advRejectedClientName.SelectedClientCode = ThisMemo.ClientNumber;
                    }

                    if (ThisMemo.NewClients.Count > 0)
                    {
                        tbFirstName.Text = ThisMemo.NewClients[0].FName;
                        tbMiddleName.Text = ThisMemo.NewClients[0].MName;
                        tbLastName.Text = ThisMemo.NewClients[0].LName;
                        tbCompanyName.Text = ThisMemo.NewClients[0].CName;
                    }
                    if (ThisMemo.PendingBankruptcy != null)
                    {
                        string pendingBK = (Convert.ToBoolean(ThisMemo.PendingBankruptcy) ? "1" : "0");
                        rblPendingBankruptcy.SelectedIndex = -1;
                        rblPendingBankruptcy.Items.FindByValue(pendingBK).Selected = true;
                    }
                }
            }
        }

        public void gvPartyRelationships_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //PartyRelationshipCodeDropdown p = new PartyRelationshipCodeDropdown();
            //p.RelationshipDisplayMode = PartyRelationshipCodeDropdown.PartyRelationshipMode.AffiliateParties;
            //e.Row.Cells[2].Controls.Add(p);
        }

        public void gvPartyRelationships_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string s = (string)e.Row.DataItem;
                TextBox t = (TextBox)e.Row.Cells[0].FindControl("tbName");
                t.Text = s;
            }
        }

        //protected void btnClientAffiliateModifySubmit_Click(object sender, EventArgs e)
        //{
        //    ValidationSummary valSum = (ValidationSummary)thisMasterPage.FindControl("valsumErrorSummary");
        //    Adversary.DataLayer.AffiliateParty p = new DataLayer.AffiliateParty(ThisMemo.ScrMemID);
        //    p.PartyID = Convert.ToInt32(hidClientAffiliateModifyPartyID.Value);
        //    p.PartyType = rblClientAffiliateModifyPartyType.SelectedValue;
        //    p.PartyFName = tbClientAffiliateModifyFirstName.Text;
        //    p.PartyMName = tbClientAffiliateModifyMiddleName.Text;
        //    p.PartyLName = tbClientAffiliateModifyLastName.Text;
        //    p.PartyOrganization = tbClientAffiliateModifyOrganization.Text;
        //    p.PartyRelationshipCode = partyrelClientAffiliateModify.ddlPartyRelationship.SelectedValue;
        //    p.Save();
        //    valSum.Enabled = true;
        //    this.mdlModifyClientAffiliate.Hide();

        //}

        //protected void gvClientAffiliates_RowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    string f = e.CommandArgument.ToString();
        //    Adversary.DataLayer.AffiliateParty affiliateParty = new DataLayer.AffiliateParty(ThisMemo.ScrMemID);
        //    Adversary.DataLayer.Party p = affiliateParty.GetParty(Convert.ToInt32(f));
        //    hidClientAffiliateModifyPartyID.Value = p.PartyID.ToString();
        //    rblClientAffiliateModifyPartyType.SelectedIndex = -1;
        //    if (p.PartyType == "Individual")
        //    {
        //        rblClientAffiliateModifyPartyType.Items.FindByValue("Individual").Selected = true;
        //    }
        //    else if (p.PartyType == "Organization")
        //    {
        //        rblClientAffiliateModifyPartyType.Items.FindByValue("Organization").Selected = true;
        //    }
        //    tbClientAffiliateModifyFirstName.Text = p.PartyFName;
        //    tbClientAffiliateModifyLastName.Text = p.PartyLName;
        //    tbClientAffiliateModifyMiddleName.Text = p.PartyMName;
        //    tbClientAffiliateModifyOrganization.Text = p.PartyOrganization;
        //    partyrelClientAffiliateModify.ddlPartyRelationship.SelectedIndex = -1;
        //    if(partyrelClientAffiliateModify.ddlPartyRelationship.Items.FindByValue(p.PartyRelationshipCode) != null)
        //    {
        //        partyrelClientAffiliateModify.ddlPartyRelationship.Items.FindByValue(p.PartyRelationshipCode).Selected = true;
        //    }
        //    ValidationSummary valSum = (ValidationSummary)thisMasterPage.FindControl("valsumErrorSummary");
        //    valSum.Enabled = false;
        //    this.mdlModifyClientAffiliate.Show();
        //}

        public override void Validate()
        {
            base.Validate();
            Adversary.DataLayer.NewClientInformation newClient = new DataLayer.NewClientInformation(ThisMemo.ScrMemID);
            if (ThisMemo.ScrMemType != 6 && ThisMemo.ScrMemType != 3)
            {
                newClient.ClientFirstName = tbFirstName.Text;
                newClient.ClientMiddleName = tbMiddleName.Text;
                newClient.ClientLastName = tbLastName.Text;
                newClient.ClientType = rblClientType.SelectedValue;
                newClient.OrganizationName = tbCompanyName.Text;
                if (rblPendingBankruptcy.SelectedIndex > -1)
                    newClient.PendingBankruptcy = (rblPendingBankruptcy.SelectedValue.Equals("1") ? true : false);
                newClient.Save();
            }
            else
            {
                if (rblNewExistingClientRejected.SelectedItem.Text == "New")
                {
                    if (rblClientTypeRejected.SelectedIndex > -1)
                    {
                        if (rblClientTypeRejected.SelectedItem.Text == "Organization")
                        {
                            newClient.OrganizationName = tbCompanyNameRejected.Text;
                            newClient.ClientType = "Organization";
                        }
                        else if (rblClientTypeRejected.SelectedItem.Text == "Individual")
                        {
                            newClient.ClientFirstName = tbFirstNameRejected.Text;
                            newClient.ClientMiddleName = tbMiddleNameRejected.Text;
                            newClient.ClientLastName = tbLastNameRejected.Text;
                            newClient.ClientType = rblClientTypeRejected.SelectedValue;
                            newClient.ClientType = "Individual";
                            if (rblPendingBankruptcyRejected.SelectedIndex > -1)
                                newClient.PendingBankruptcy = (rblPendingBankruptcyRejected.SelectedValue.Equals("1") ? true : false);
                        }
                    }
                    newClient.Save();
                }
                else if (rblNewExistingClientRejected.SelectedItem.Text == "Existing")
                {
                    Adversary.DataLayer.MatterInformation matterInfo = new DataLayer.MatterInformation(ThisMemo.ScrMemID);
                    HiddenField hidClientCode = (HiddenField)advRejectedClientName.FindControl("hidClientCode");
                    matterInfo.ClientCode = hidClientCode.Value;
                    matterInfo.ContingencyFee = ThisMemo.ContingFee ?? false;
                    matterInfo.FeeEstimate = ThisMemo.EstFees;
                    matterInfo.MatterName = ThisMemo.MatterName;
                    matterInfo.NDrive = ThisMemo.NDrive ?? false;
                    matterInfo.NDriveType = ThisMemo.NDriveType ?? -1;
                    matterInfo.PracticeTypeCode = ThisMemo.PracCode;
                    matterInfo.WorkDescription = ThisMemo.WorkDesc;
                    matterInfo.Save();
                }
            }
        }

        #region Affiliate Wizard methods
        //public void affiliateWizard_NextButtonClick(object sender, System.Web.UI.WebControls.WizardNavigationEventArgs e)
        //{
        //    if (affiliateWizard.ActiveStepIndex == 0)
        //    {
        //        SetTextboxes();
        //    }
        //}
        //public void affiliateWizard_FinishButtonClick(object sender, System.Web.UI.WebControls.WizardNavigationEventArgs e)
        //{
        //    GridViewRowCollection gvCol = gvPartyRelationships.Rows;
        //    Adversary.DataLayer.AffiliateParty affiliatesClass = new DataLayer.AffiliateParty(ThisMemo.ScrMemID);
        //    List<DataLayer.Party> newPartiesList = new List<DataLayer.Party>();
            
        //    foreach (GridViewRow row in gvCol)
        //    {
        //        Adversary.DataLayer.Party p = new DataLayer.Party();

        //        DropDownList ddlPartyType = (DropDownList)row.Cells[1].FindControl("ddlPartyType");
        //        PartyRelationshipDropDown partyddl = (PartyRelationshipDropDown)row.Cells[2].FindControl("PartyRelationshipDropDown");
        //        DropDownList ddlPartyRel = (DropDownList)partyddl.ddlPartyRelationship;
        //        TextBox tbName = (TextBox)row.Cells[0].FindControl("tbName");

        //        if (ddlPartyType.SelectedValue == "Individual")
        //        {
        //            string[] name = Adversary.Utils.PartyNameSeparator.SeparateIndividualName(tbName.Text);
        //            p.PartyFName = name[0];
        //            p.PartyMName = name[1];
        //            p.PartyLName = name[2];
        //        }
        //        else
        //        {
        //            p.PartyOrganization = tbName.Text;
        //        }
        //        p.ScrMemID = ThisMemo.ScrMemID;
        //        p.PartyType = ddlPartyType.SelectedValue;
        //        p.PartyRelationshipCode = ddlPartyRel.SelectedValue;
        //        p.PartyRelationshipName = ddlPartyRel.SelectedItem.Text;
        //        p.PC = 0;
        //        //add the new party to the list
        //        newPartiesList.Add(p);
        //    }
        //    affiliatesClass.SaveGroup(newPartiesList);
        //    //mdlClientAffiliates.Hide();

        //}
        //public void affiliateWizard_SideBarButtonClick(object sender, System.Web.UI.WebControls.WizardNavigationEventArgs e)
        //{
        //    if (e.NextStepIndex == 1) SetTextboxes();
        //}
        //public void affiliateWizard_CancelButtonClick(object sender, EventArgs e)
        //{
        //    //mdlClientAffiliates.Hide();
        //}
        #endregion

        //private void SetTextboxes()
        //{
        //    string f = tbPartyNames.Text;
        //    string[] f1 = f.Split('\n');

        //    gvPartyRelationships.DataSource = f1;
        //    gvPartyRelationships.DataBind();
            
        //}
        
    }
}