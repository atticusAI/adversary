﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Screening/Screening.Master" AutoEventWireup="true"
    CodeBehind="Contact.aspx.cs" Inherits="Adversary.Web.Screening.Contact" Theme="HH" %>

<%@ Register TagPrefix="ajax" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="cntMain" ContentPlaceHolderID="cphMainContent" runat="server">
    <asp:UpdatePanel ID="upnlContacts" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView runat="server" ID="gvContacts" AutoGenerateColumns="false" SkinID="HH_Gridview"
                ShowFooter="false" ShowHeader="true" OnPreRender="gvContacts_PreRender" OnRowCommand="gvContacts_RowCommand" OnRowDeleting="gvContacts_RowDeleting">
                <Columns>
                    <asp:TemplateField HeaderText="">
                        <ItemTemplate>
                            <asp:LinkButton ID="lbModifyClientAffiliate" runat="server" Text="Modify" CommandArgument='<%#Eval("ContactID") %>' />
                            &nbsp;&nbsp;<asp:LinkButton ID="lbClientAffiliateDelete" runat="server" Text="Delete"
                                CommandName="Delete" CommandArgument='<%#Eval("ContactID")%>' OnClientClick="javascript:return confirm('Are you sure you want to delete this record?');" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="ContactFName" HeaderText="First Name" />
                    <asp:BoundField DataField="ContactMName" HeaderText="Middle Name" />
                    <asp:BoundField DataField="ContactLName" HeaderText="Last Name" />
                    <asp:BoundField DataField="ContactTitle" HeaderText="Contact's Title" />
                </Columns>
            </asp:GridView>
            <asp:LinkButton ID="lbAddNewContact" runat="server" CausesValidation="false" Text="Add New Contact" />
            <ajax:ModalPopupExtender ID="mdlContactEntry" TargetControlID="lbAddNewContact" PopupControlID="pnlContactEntryPopup"
                runat="server" CancelControlID="btnCancelContact" BackgroundCssClass="pop-back" />
            <asp:Button ID="btnModifyContact_Hidden" runat="server" Style="display: none;" />
            <ajax:ModalPopupExtender ID="mdlModifyContact" runat="server" TargetControlID="btnModifyContact_Hidden"
                PopupControlID="pnlContactEntryPopup" CancelControlID="btnCancelContact" BackgroundCssClass="pop-back" />
            <asp:Panel ID="pnlContactEntryPopup" runat="server" SkinID="HH_PanelPopup" CssClass="pop" DefaultButton="btnSubmitContact">
                <asp:HiddenField ID="hidContactID" runat="server" />
                <table>
                    <tr>
                        <td>
                            First Name:
                        </td>
                        <td>
                            <asp:TextBox ID="tbFirstName" runat="server" SkinID="HH_Textbox"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Middle Name:
                        </td>
                        <td>
                            <asp:TextBox ID="tbMiddleName" runat="server" SkinID="HH_Textbox"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Last Name:
                        </td>
                        <td>
                            <asp:TextBox ID="tbLastName" runat="server" SkinID="HH_Textbox"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Title:
                        </td>
                        <td>
                            <asp:TextBox ID="tbTitle" runat="server" SkinID="HH_Textbox"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:LinkButton ID="btnSubmitContact" runat="server" Text="Submit" OnClick="btnSubmitContact_Click" />
                            &nbsp;&nbsp;&nbsp;
                            <asp:LinkButton ID="btnCancelContact" runat="server" Text="Cancel" CausesValidation="false" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSubmitContact" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
