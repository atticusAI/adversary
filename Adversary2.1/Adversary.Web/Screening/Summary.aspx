﻿<%@ Page Title="" Language="C#" MasterPageFile="Screening.Master" AutoEventWireup="true"
    CodeBehind="Summary.aspx.cs" Inherits="Adversary.Web.Screening.Summary" Theme="HH" %>

<%@ Register Src="../Controls/ScreeningMemoSummary.ascx" TagName="ScreeningMemoSummary"
    TagPrefix="uc1" %>
<%@ Register Src="../Controls/ApprovalTable.ascx" TagName="ApprovalTable" TagPrefix="uc2" %>
<asp:Content ID="cntHead" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="cntHeader" ContentPlaceHolderID="cphHeader" runat="server">
</asp:Content>
<asp:Content ID="cntMain" ContentPlaceHolderID="cphMainContent" runat="server">
    <uc2:ApprovalTable ID="aptSummary" runat="server" />
    <uc1:ScreeningMemoSummary ID="smsView" runat="server" />
</asp:Content>
<asp:Content ID="cntLower" ContentPlaceHolderID="cphLowerContent" runat="server">
</asp:Content>
