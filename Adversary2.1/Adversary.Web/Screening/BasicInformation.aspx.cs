﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.DataLayer;
using Adversary.Web.Extensions;

namespace Adversary.Web.Screening
{
    public partial class BasicInformation : ScreeningBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (ThisMemo.ScrMemType != 3)
            {
                trProBonoType.Visible = false;
            }
            else
            {
                this.advProBonoTypes.SelectedProBonoType = ThisMemo.ProBonoTypeID ?? -1;
            }
            lblScreeningMemoMember.Text = ThisMemo.ScrMemID.ToString();

            ucAttorneyPreparingMemo.ServiceMethod = "TimekeeperService";
            ucInputBy.ServiceMethod = "TimekeeperService";

            if (ThisMemo.RefDate != null)
            {
                this.tbMemoDate.Text = ((DateTime)ThisMemo.RefDate).ToShortDateString();
            }

            if (!Page.IsPostBack)
            {
                ucAttorneyPreparingMemo.SelectedEmployeeCode = ThisMemo.AttEntering;
                ucInputBy.SelectedEmployeeCode =
                    (String.IsNullOrEmpty(ThisMemo.PersonnelID) ? base.CurrentUser.CurrentUserPayrollID : ThisMemo.PersonnelID);

                if (!ThisMemo.Confidential.HasValue && ThisMemo.ScrMemType == 5)
                {
                    rblConfidential.SelectedIndex = 0; //select confidential by default for employee work memos
                }
                else
                {
                    rblConfidential.Items.FindByValue((Convert.ToBoolean(base.ThisMemo.Confidential) ? "Yes" : "No")).Selected = true;
                }  
            }
        }

        
        public override void Validate()
        {
            base.Validate();
            if (this.IsInputValid())
            {
                HiddenField hdfPreparing = ((HiddenField)this.ucAttorneyPreparingMemo.FindControl("hidEmployeeCode"));
                HiddenField hdfInputBy = ((HiddenField)this.ucInputBy.FindControl("hidEmployeeCode"));
                bool isConfidential = (rblConfidential.SelectedValue.Equals("Yes") ? true : false);
                DateTime refDate = Convert.ToDateTime(tbMemoDate.Text);

                Adversary.DataLayer.BasicInformation b = new Adversary.DataLayer.BasicInformation(base.ScreeningMemoID);
                b.Confidential = isConfidential;
                b.MemoDate = refDate;
                b.PreparingAttorney = hdfPreparing.Value;
                b.InputAttorney = hdfInputBy.Value;
                b.ProBonoTypeID = advProBonoTypes.SelectedProBonoType;
                b.Save();
            }
        }

        protected void valPage_ServerValidate(object sender, ServerValidateEventArgs e)
        {
            e.IsValid = this.IsInputValid();
        }

        private bool IsInputValid()
        {
            bool isValid = true;

            this.ClearErrors();
            if (!this.tbMemoDate.Text.HH_IsValidDate())
            {
                this.tbMemoDate.HH_SetError(this.pnlErrs);
                isValid = false;
            }

            return isValid;
        }

        private void ClearErrors()
        {
            this.tbMemoDate.HH_ClearError();
            this.pnlErrs.HH_ClearError();
        }
    }
}