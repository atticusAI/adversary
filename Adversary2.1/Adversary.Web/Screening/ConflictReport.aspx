﻿<%@ Page Title="" Theme="HH" Language="C#" MasterPageFile="~/Screening/Screening.Master" AutoEventWireup="true" CodeBehind="ConflictReport.aspx.cs" Inherits="Adversary.Web.Screening.ConflictReport" %>
<%@ Register TagPrefix="ajax" Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" %>
<%@ Register TagPrefix="adv" TagName="fileUpload" Src="~/Controls/FileUpload.ascx" %>
<%@ Register TagPrefix="adv" TagName="files" Src="~/Controls/ExistingFiles.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/AccordionPane.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMainContent" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" width="900">
        <tr>
            <td>
                <asp:RadioButtonList runat="server" ID="rblConflictReport">
                    <asp:ListItem Text="Upload a conflict report" Value="yes" />
                    <asp:ListItem Text="A conflict report will not be attached to this memo" Value="no" />
                </asp:RadioButtonList><br />
               
            </td>
        </tr>
        <tr>
            <td>
                <ajax:Accordion ID="accConflictReport" runat="server" Width="100%" FadeTransitions="true" FramesPerSecond="60" Height="199"
                RequireOpenedPane="true" TransitionDuration="200" SelectedIndex="-1" AutoSize="None">
                    <Panes>
                        <ajax:AccordionPane ID="apUploadConflictReport" runat="server">
                            <Content>
                                <asp:UpdatePanel ID="upnlUploadConflictReport" runat="server" ChildrenAsTriggers="true" UpdateMode="Always">
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="fileUpload" EventName="" />
                                    </Triggers>
                                    <ContentTemplate>
                                        <asp:Label ID="lblConflictReportMessage" runat="server" />
                                        <%--<asp:GridView ID="gvConflictReportFiles" runat="server"  
                                        AutoGenerateColumns="false" ShowHeader="false"
                                        CellPadding="3" CellSpacing="2" BorderStyle="None">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnDeleteFile" runat="server" 
                                                            CommandName="DeleteFile" CommandArgument='<%#Eval("AttachmentID")%>'
                                                            Text="Delete" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Description" />
                                                <asp:BoundField DataField="FileName" />
                                                <asp:BoundField DataField="AttachmentID" />
                                            </Columns>
                                        </asp:GridView>--%>
                                        <adv:files ID="advFiles" runat="server" />
                                        <b>Please Upload a Conflict Report</b><br />
                                        <asp:Label ID="lblMessage" runat="server" Visible="false" />
                                        <adv:fileUpload id="fileUpload" runat="server" FileUploadMode="ConflictReport" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </Content>
                        </ajax:AccordionPane>
                        <ajax:AccordionPane ID="apNoConflictReport" runat="server">
                            <Content>
                                Reason a conflict report will not be included<br /><br />
                                <asp:TextBox runat="server" ID="tbNoConflictReport" TextMode="MultiLine" Rows="5" Columns="50" />
                            </Content>
                        </ajax:AccordionPane>
                        <ajax:AccordionPane ID="apConflictReportBlank" runat="server" />
                    </Panes>
                </ajax:Accordion>
            </td>
        </tr>
    </table>
</asp:Content>
