﻿<%@ Page Title="" Theme="HH" Language="C#" MasterPageFile="~/Screening/Screening.Master" AutoEventWireup="true" CodeBehind="Fees.aspx.cs" Inherits="Adversary.Web.Screening.Fees" %>
<%@ Register TagPrefix="ajax" Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" %>
<%@ Register TagPrefix="adv" TagName="PersonAutoComplete" Src="~/Controls/PersonAutoComplete.ascx" %>
<%@ Register TagPrefix="adv" TagName="ListContainerControl" Src="~/Controls/ListContainerControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="../Scripts/AccordionPane.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMainContent" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" width="900">
        <tr>
            <td>
                If you are an associate and you claim a fee split, please be sure to include the basis for the Origination, or your request will be denied.  
            </td>
        </tr>
        <tr>
            <td>
                Employee numbers of person(s) claiming origination
            </td>
        </tr>
        <tr>
            <td>
                <adv:ListContainerControl ID="advFeeSplit" runat="server" AddY="50" AutoGenerateColumns="false"
                    DeleteY="50"
                 PopupCssClass="pop"
                 BackgroundCssClass="pop-back"
                 EmptyDataText="No Fee Split Staff specified."
                 Text="Add Staff"
                 OnPreRender="FeeSplit_PreRender"
                 OnRowDataBound="FeeSplit_RowDataBound"
                 OnDeleteClick="FeeSplit_DeleteClick"
                 OnRowCommand="FeeSplit_RowCommand"
                 OnAddClick="FeeSplit_AddClick">
                 <Columns>
                    <asp:BoundField ShowHeader="true" HeaderText="Staff" />
                 </Columns>
                 <DeleteItemTemplate>
                    Are you sure you wish to delete this record?
                    <asp:HiddenField ID="hidDeleteFeeSplitStaff" runat="server"
                        Value='<%#Eval("FeeSplitStaffID")%>' />
                 </DeleteItemTemplate>
                 <InsertItemTemplate>
                    Employee Name or Personnel ID:
                    <adv:PersonAutoComplete ID="advFeeSplitStaffMember" runat="server"
                        RaiseEventOnItemSelected="false" PopulatedEventCausesValidation="false"
                        ServiceMethod="TimekeeperService" />
                 </InsertItemTemplate>
                 </adv:ListContainerControl>
                <%--<asp:UpdatePanel ID="upnlFeeSplit" runat="server" ChildrenAsTriggers="true" UpdateMode="Always">
                    <ContentTemplate>
                        <asp:GridView ID="gvFeeSplitAssociates" runat="server" AutoGenerateColumns="false" ShowHeader="true" SkinID="HH_Gridview">
                            <Columns>
                                <asp:TemplateField ShowHeader="false">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbDeleteFeeSplit" runat="server" Text="Delete" CommandName="Delete"
                                            CausesValidation="false" CommandArgument='<%#Eval("FeeSplitStaffID")%>' />          
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField ShowHeader="true" HeaderText="Staff" />
                            </Columns>
                        </asp:GridView>
                        <asp:LinkButton Text="Add an Associate" runat="server" ID="lbAddFeeSplitAssociate" CausesValidation="false" />
                        <ajax:ModalPopupExtender ID="mdlAddAssociate" runat="server" TargetControlID="lbAddFeeSplitAssociate"
                            PopupControlID="pnlAddAssociate" CancelControlID="btnCancelAddAssociate" />
                    </ContentTemplate>
                </asp:UpdatePanel>--%>
            </td>
        </tr>
        <tr>
            <td>
                <div id="feesplitdiv" runat="server" style="display:none;">
                <asp:Panel runat="server" ID="pnlFeeSplitJustification">
                    <asp:Label ID="Label1" Text="* If claiming an origination, provide justification including the percentage of origination claimed by the associate(s)" 
                    runat="server" />
                    <asp:RadioButtonList runat="server" ID="rblFeeSplitDescriptionToggle">
                        <asp:ListItem Value="yes" Text="New matter for client for which I have previously received origination credit" />
                        <asp:ListItem Value="no" Text="Other" />
                    </asp:RadioButtonList>
                    <ajax:Accordion ID="accFeeSplit" runat="server"  Width="100%" FadeTransitions="true" FramesPerSecond="60" Height="199"
                RequireOpenedPane="true" TransitionDuration="200" SelectedIndex="-1" AutoSize="None">
                        <Panes>
                            <ajax:AccordionPane ID="apNewMatter" runat="server">
                                <Content><!--blank--></Content>
                            </ajax:AccordionPane>
                            <ajax:AccordionPane ID="apOther" runat="server">
                                <Content>
                                    <asp:TextBox ID="tbFeeSplitOther" runat="server" Columns="40" Rows="4" TextMode="MultiLine" />
                                </Content>
                            </ajax:AccordionPane>
                        </Panes>
                    </ajax:Accordion>
                </asp:Panel>
                </div>
            </td>
        </tr>
    </table>
    <%--<asp:Panel ID="pnlAddAssociate" runat="server" SkinID="HH_PanelPopup">
        <table>
            <tr>
                <td>Employee Name/ID</td>
                <td>
                    <adv:PersonAutoComplete ID="advAddAssociatePersonControl" runat="server" ServiceMethod="TimekeeperService" />
                </td>
            </tr>
            <tr>
                <td colspan="2" align="right">
                    <asp:LinkButton ID="btnSubmitAddAssociate" runat="server" CausesValidation="false" Text="Add" />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:LinkButton ID="btnCancelAddAssociate" runat="server" CausesValidation="false" Text="Cancel" />
                </td>
            </tr>
        </table>
    </asp:Panel>--%>
</asp:Content>
