﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using Adversary.Utils;
using Adversary.Web.Controls;

namespace Adversary.Web.Screening
{
    public partial class Fees : ScreeningBasePage
    {
        private bool showOriginationPanel = false;
        private Adversary.DataLayer.FeeSplit _FeeSplit;
        //private Adversary.DataLayer.AdversaryData advData;
        //protected void btnSubmitAddAssociate_Click(object sender, EventArgs e)
        //{
        //    Adversary.DataLayer.FeeSplitStaff fstaff = new DataLayer.FeeSplitStaff();
        //    fstaff.PersonnelID = ((HiddenField)advAddAssociatePersonControl.FindControl("hidEmployeeCode")).Value;
        //    fstaff.ScrMemID = ThisMemo.ScrMemID;
        //    feeSplit.SaveFeeSplitStaff(fstaff);
        //    BindData();
        //}

        protected void Page_Load(object sender, EventArgs e)  { }

        protected override void OnLoad(EventArgs e)
        {
            _FeeSplit = new DataLayer.FeeSplit(ThisMemo.ScrMemID);
            _FeeSplit.Get();
            //advData = new DataLayer.AdversaryData();
            //btnSubmitAddAssociate.Click += new EventHandler(btnSubmitAddAssociate_Click);
            //gvFeeSplitAssociates.RowCommand += new GridViewCommandEventHandler(gvFeeSplitAssociates_RowCommand);
            //gvFeeSplitAssociates.RowDeleting += new GridViewDeleteEventHandler(gvFeeSplitAssociates_RowDeleting);
            //gvFeeSplitAssociates.RowDataBound += new GridViewRowEventHandler(gvFeeSplitAssociates_RowDataBound);
            //if (!IsPostBack) BindData();

            //if (_FeeSplit.GetFeeSplitStaff().Count > 0)
            //{
            //    ShowFeeSplitDiv();
            //}
            

            rblFeeSplitDescriptionToggle.HH_AddAccordionPaneJavascript(accFeeSplit.ClientID);
        }

        public void FeeSplit_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Adversary.DataLayer.FeeSplitStaff F =
                    (Adversary.DataLayer.FeeSplitStaff)e.Row.DataItem;
                e.Row.Cells[1].Text = AdversaryDataLayer.GetEmployee(F.PersonnelID).EmployeeName + "(" + F.PersonnelID.ToString() + ")";

            }
        }

        public void FeeSplit_PreRender(object sender, EventArgs e)
        {
            BindGrid();
        }

        public void FeeSplit_AddClick(object sender, EventArgs e)
        {
            PersonAutoComplete pautoComplete = (PersonAutoComplete)
                advFeeSplit.FindAddControl("advFeeSplitStaffMember");
            HiddenField hidEmployeeCode = (HiddenField)pautoComplete.FindControl("hidEmployeeCode");
            _FeeSplit.AddFeeSplitStaff(hidEmployeeCode.Value);
        }

        public void FeeSplit_DeleteClick(object sender, EventArgs e)
        {
            HiddenField hidDeleteStaff = (HiddenField)advFeeSplit.FindDeleteControl("hidDeleteFeeSplitStaff");

            Adversary.DataLayer.FeeSplitStaff _feesplitStaff;
            _feesplitStaff = _FeeSplit.GetFeeSplitStaff().Find(fs =>
                fs.FeeSplitStaffID.Equals(Int32.Parse(hidDeleteStaff.Value)));

            if (_FeeSplit.GetFeeSplitStaff().Exists(fs => fs.Equals(_feesplitStaff)))
            {
                _FeeSplit.DeleteFeeSplitStaff(_feesplitStaff.FeeSplitStaffID);

            }
            BindGrid();
        }

        public void FeeSplit_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            advFeeSplit.DataSource = _FeeSplit.GetFeeSplitStaff();
        }
        //void gvFeeSplitAssociates_RowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    if (e.CommandName.ToLower().Equals("delete"))
        //    {
        //        feeSplit.Delete(Int32.Parse(e.CommandArgument.ToString()));
        //        BindData();
        //    }
        //}

        //void gvFeeSplitAssociates_RowDeleting(object sender, GridViewDeleteEventArgs e)
        //{
            
        //}

        //void gvFeeSplitAssociates_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        Adversary.DataLayer.FeeSplitStaff feeSplitDataItem = ((Adversary.DataLayer.FeeSplitStaff)e.Row.DataItem);
        //        e.Row.Cells[1].Text = AdversaryDataLayer.GetEmployee(feeSplitDataItem.PersonnelID).EmployeeName;
        //    }
        //}

        public override void Validate()
        {
            base.Validate();
            _FeeSplit.NewMatterOnly = rblFeeSplitDescriptionToggle.SelectedValue.ToLower();
            _FeeSplit.FeeSplitDescription = tbFeeSplitOther.Text;
            _FeeSplit.Save();
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            showOriginationPanel = (_FeeSplit.GetFeeSplitStaff().Count > 0);

            if (showOriginationPanel)
            {
                ShowFeeSplitDiv();
            }
            else
            {
                HideFeeSplitDiv();
            }
            if (_FeeSplit != null)
            {
                if (!String.IsNullOrEmpty(_FeeSplit.FeeSplitDescription))
                {
                    rblFeeSplitDescriptionToggle.SelectedIndex = 1;
                    accFeeSplit.SelectedIndex = 1;
                    tbFeeSplitOther.Text = _FeeSplit.FeeSplitDescription;
                }
                else if (_FeeSplit.NewMatterOnly != null)
                {
                    if(_FeeSplit.NewMatterOnly.ToLowerInvariant().Equals("yes"))
                        rblFeeSplitDescriptionToggle.SelectedIndex = 0;
                }
            }
        }

        private void BindGrid()
        {
            //gvFeeSplitAssociates.DataSource = feeSplit.GetFeeSplitStaff();
            //gvFeeSplitAssociates.DataBind();
            advFeeSplit.DataSource = _FeeSplit.GetFeeSplitStaff()
                .OrderBy(f => f.PersonnelID);
            advFeeSplit.DataBind();
        }

        private void ShowFeeSplitDiv()
        {
            feesplitdiv.Attributes.Clear();
            feesplitdiv.Attributes.Add("style", "display:visible");
        }
        private void HideFeeSplitDiv()
        {
            feesplitdiv.Attributes.Clear();
            feesplitdiv.Attributes.Add("style", "display:none");
        }
    }
}