﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="Adversary.Web.Screening.WebForm1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script src="../Scripts/jquery-1.7.1.min.js" type="text/javascript"></script>
    <title></title>
    <script type="text/javascript">
        $(document).ready(function () {

            $('#<%=btnMoveRight.ClientID %>').click(function () {

                var selectedOptions = $('#<%=lstBox1.ClientID %> option:selected');

                if (selectedOptions.length == 0) {

                    alert("Please select option to move");

                    return false;

                }

                $('#<%=lstBox2.ClientID %>').append($(selectedOptions).clone());

                $(selectedOptions).remove();

                return false;

            });

            $('#<%=btnMoveLeft.ClientID %>').click(function () {

                var selectedOptions = $('#<%=lstBox2.ClientID %> option:selected');

                if (selectedOptions.length == 0) {

                    alert("Please select option to move");

                    return false;

                }

                $('#<%=lstBox1.ClientID %>').append($(selectedOptions).clone());

                $(selectedOptions).remove();

                return false;

            });

        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <asp:ListBox ID="lstBox1" runat="server" SelectionMode="Multiple">

            <asp:ListItem Text="A" Value="1"></asp:ListItem>

            <asp:ListItem Text="B" Value="2"></asp:ListItem>

            <asp:ListItem Text="C" Value="3"></asp:ListItem>

            <asp:ListItem Text="D" Value="4"></asp:ListItem>

</asp:ListBox>

<asp:Button ID="btnMoveRight" runat="server" Text=">>" />

<asp:Button ID="btnMoveLeft" runat="server" Text="<<" />

<asp:ListBox ID="lstBox2" runat="server" SelectionMode="Multiple">

            <asp:ListItem Text="E" Value="5"></asp:ListItem>

            <asp:ListItem Text="F" Value="6"></asp:ListItem>

            <asp:ListItem Text="G" Value="7"></asp:ListItem>

            <asp:ListItem Text="H" Value="8"></asp:ListItem>

</asp:ListBox>

<asp:Button ID="btn1" OnClick="btn1_Click" Text="Do" runat="server" />


    </div>
    </form>
</body>
</html>
