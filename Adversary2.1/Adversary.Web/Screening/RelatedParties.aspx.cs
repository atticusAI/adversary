﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.Utils;
using Adversary.DataLayer;
using Adversary.Controls;

namespace Adversary.Web.Screening
{
    public partial class RelatedParties : ScreeningBasePage
    {
        public string SetAccordionPane;
        protected void Page_Load(object sender, EventArgs e)
        {
            rblRelatedPartyModifyPartyType.HH_AddAccordionPaneJavascript(accRelatedPartyModify.ClientID);
            partyrelRelatedPartyModify.RelationshipDisplayMode = PartyRelationshipDropDown.PartyRelationshipMode.RelatedParties;
            gvRelatedPartys.RowDeleting += new GridViewDeleteEventHandler(gvRelatedPartys_RowDeleting);
            gvRelatedPartys.RowDataBound += new GridViewRowEventHandler(gvRelatedPartys_RowDataBound);
            if (!Page.IsPostBack)
            {
                BindData();
            }
        }

        void gvRelatedPartys_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //set the party name
                Party pData = (Party)e.Row.DataItem;
                string __name = string.Empty;
                if (pData.PartyType.Equals("Individual"))
                {
                    __name = pData.PartyFName + " "
                        + pData.PartyMName + " "
                        + pData.PartyLName;
                }
                else if (pData.PartyType.Equals("Organization"))
                {
                    __name = pData.PartyOrganization;
                }
                e.Row.Cells[3].Text = __name;
            }
        }

        void gvRelatedPartys_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            BindData();
        }
        public void gvPartyRelationships_RowCreated(object sender, GridViewRowEventArgs e)
        {

        }

        public void gvPartyRelationships_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string s = (string)e.Row.DataItem;
                TextBox t = (TextBox)e.Row.Cells[0].FindControl("tbName");
                t.Text = s;
            }
        }

        protected void btnRelatedPartyModifySubmit_Click(object sender, EventArgs e)
        {
            Adversary.DataLayer.RelatedParty p = new DataLayer.RelatedParty(ThisMemo.ScrMemID);
            p.PartyID = Convert.ToInt32(hidRelatedPartyModifyPartyID.Value);
            p.PartyType = rblRelatedPartyModifyPartyType.SelectedValue;
            p.PartyFName = tbRelatedPartyModifyFirstName.Text;
            p.PartyMName = tbRelatedPartyModifyMiddleName.Text;
            p.PartyLName = tbRelatedPartyModifyLastName.Text;
            p.PartyOrganization = tbRelatedPartyModifyOrganization.Text;
            p.PartyRelationshipCode = partyrelRelatedPartyModify.ddlPartyRelationship.SelectedValue;
            p.PartyRelationshipName = partyrelRelatedPartyModify.ddlPartyRelationship.SelectedItem.Text;
            p.Save();
            this.mdlModifyRelatedParty.Hide();
            BindData();
            upnlRelatedPartyGrid.Update();
        }

        protected void btnRelatedPartyModifyCancel_Click(object sender, EventArgs e)
        {
            BindData();
            mdlModifyRelatedParty.Hide();
        }

        protected void gvRelatedPartys_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            if (e.CommandName == "Delete")
            {
                Adversary.DataLayer.RelatedParty del_RelParty =
                    new RelatedParty(ThisMemo.ScrMemID);
                del_RelParty.PartyID = Convert.ToInt32(e.CommandArgument);
                del_RelParty.Delete();
                del_RelParty.Dispose();
            }
            else if (e.CommandName == "Modify")
            {
                string f = e.CommandArgument.ToString();
                Adversary.DataLayer.RelatedParty RelatedParty = new DataLayer.RelatedParty(ThisMemo.ScrMemID);
                Adversary.DataLayer.Party p = RelatedParty.GetParty(Convert.ToInt32(f));
                hidRelatedPartyModifyPartyID.Value = p.PartyID.ToString();
                rblRelatedPartyModifyPartyType.SelectedIndex = -1;
                if (p.PartyType == "Individual")
                {
                    rblRelatedPartyModifyPartyType.Items.FindByValue("Individual").Selected = true;
                }
                else if (p.PartyType == "Organization")
                {
                    rblRelatedPartyModifyPartyType.Items.FindByValue("Organization").Selected = true;
                }
                accRelatedPartyModify.SelectedIndex = (p.PartyType.Equals("Individual") ? 0 : 1);
                tbRelatedPartyModifyFirstName.Text = p.PartyFName;
                tbRelatedPartyModifyLastName.Text = p.PartyLName;
                tbRelatedPartyModifyMiddleName.Text = p.PartyMName;
                tbRelatedPartyModifyOrganization.Text = p.PartyOrganization;
                partyrelRelatedPartyModify.ddlPartyRelationship.SelectedIndex = -1;
                if (partyrelRelatedPartyModify.ddlPartyRelationship.Items.FindByValue(p.PartyRelationshipCode) != null)
                {
                    partyrelRelatedPartyModify.ddlPartyRelationship.Items.FindByValue(p.PartyRelationshipCode).Selected = true;
                }
                this.mdlModifyRelatedParty.Show();
                RelatedParty.Dispose();
                BindData();
            }
        }

        private void BindData()
        {
            Adversary.DataLayer.RelatedParty rParty = new RelatedParty(ThisMemo.ScrMemID);
            List<Party> _RelatedParties = rParty.GetPartyList();
            gvRelatedPartys.DataSource = _RelatedParties;
            gvRelatedPartys.DataBind();
            rParty.Dispose();
        }

        public override void Validate()
        {



        }

        public void RelatedWizard_NextButtonClick(object sender, System.Web.UI.WebControls.WizardNavigationEventArgs e)
        {
            if (RelatedWizard.ActiveStepIndex == 0)
            {
                SetTextboxes();
            }
        }
        public void RelatedWizard_FinishButtonClick(object sender, System.Web.UI.WebControls.WizardNavigationEventArgs e)
        {
            GridViewRowCollection gvCol = gvPartyRelationships.Rows;
            Adversary.DataLayer.RelatedParty RelatedClass = new DataLayer.RelatedParty(ThisMemo.ScrMemID);
            List<DataLayer.Party> newPartiesList = new List<DataLayer.Party>();

            foreach (GridViewRow row in gvCol)
            {
                Adversary.DataLayer.Party p = new DataLayer.Party();

                DropDownList ddlPartyType = (DropDownList)row.Cells[1].FindControl("ddlPartyType");
                PartyRelationshipDropDown partyddl = (PartyRelationshipDropDown)row.Cells[2].FindControl("partyrelPartyRelationshipDropDown");
                DropDownList ddlPartyRel = (DropDownList)partyddl.ddlPartyRelationship;
                TextBox tbName = (TextBox)row.Cells[0].FindControl("tbName");

                if (ddlPartyType.SelectedValue == "Individual")
                {
                    string[] name = Adversary.Utils.PartyNameSeparator.SeparateIndividualName(tbName.Text);
                    p.PartyFName = name[0];
                    p.PartyMName = name[1];
                    p.PartyLName = name[2];
                }
                else
                {
                    p.PartyOrganization = tbName.Text;
                }
                p.ScrMemID = ThisMemo.ScrMemID;
                p.PartyType = ddlPartyType.SelectedValue;
                p.PartyRelationshipCode = ddlPartyRel.SelectedValue;
                p.PartyRelationshipName = ddlPartyRel.SelectedItem.Text;
                p.PC = 0;
                //add the new party to the list
                newPartiesList.Add(p);
                tbPartyNames.Text = "";
                RelatedWizard.MoveTo(RelatedWizard.WizardSteps[0]);
            }
            RelatedClass.SaveGroup(newPartiesList);
            mdlRelatedPartys.Hide();
            BindData();
            upnlRelatedPartyGrid.Update();
        }

        private void RequirePostback()
        {
            Response.Write("<script>document.forms[0].submit();</script>");
        }
        public void RelatedWizard_SideBarButtonClick(object sender, System.Web.UI.WebControls.WizardNavigationEventArgs e)
        {
            if (e.NextStepIndex == 1) SetTextboxes();
        }
        public void RelatedWizard_CancelButtonClick(object sender, EventArgs e)
        {
            mdlRelatedPartys.Hide();
        }
        private void SetTextboxes()
        {
            string f = tbPartyNames.Text;
            string[] f1 = f.Split('\n');

            gvPartyRelationships.DataSource = f1;
            gvPartyRelationships.DataBind();

        }
    }
}