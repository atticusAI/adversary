﻿<%@ Page Title="" Theme="HH" Language="C#" MasterPageFile="~/Screening/Screening.Master" AutoEventWireup="true" CodeBehind="RejectionLetter.aspx.cs" Inherits="Adversary.Web.Screening.RejectionLetter" %>
<%@ Register Assembly="AjaxControlToolkit" TagPrefix="ajax" Namespace="AjaxControlToolkit" %>
<%@ Register TagPrefix="adv" TagName="fileUpload" Src="~/Controls/FileUpload.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/AccordionPane.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMainContent" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" width="900">
        <tr>
            <td>
                <asp:RadioButtonList runat="server" ID="rblRejectionLetter">
                    <asp:ListItem Text="Upload a rejection letter" Value="yes" />
                    <asp:ListItem Text="A rejection letter will not be attached to this memo" Value="no" />
                </asp:RadioButtonList><br />
               
            </td>
        </tr>
        <tr>
            <td>
                <ajax:Accordion ID="accRejectionLetter" runat="server" Width="100%" FadeTransitions="true" FramesPerSecond="60" Height="199"
                RequireOpenedPane="false" TransitionDuration="200" SelectedIndex="-1" AutoSize="None">
                    <Panes>
                        <ajax:AccordionPane ID="apUploadRejectionLetter" runat="server">
                            <Content>
                                <asp:UpdatePanel ID="upnlUploadRejectionLetter" runat="server" ChildrenAsTriggers="true" UpdateMode="Always">
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="fileUpload" EventName="" />
                                    </Triggers>
                                    <ContentTemplate>
                                        <asp:Label ID="lblRejectionLetterMessage" runat="server" />
                                        <asp:GridView ID="gvRejectionLetterFiles" runat="server" 
                                            AutoGenerateColumns="false" ShowHeader="false"
                                            CellPadding="3" CellSpacing="2" BorderStyle="None">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnDeleteFile" runat="server" 
                                                            CommandName="DeleteFile" CommandArgument='<%#Eval("AttachmentID")%>'
                                                            Text="Delete" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Description" />
                                                <asp:BoundField DataField="FileName" />
                                                <asp:BoundField DataField="AttachmentID" />
                                            </Columns>
                                        </asp:GridView>
                                        <b>Please Upload a Rejection Letter</b><br />
                                        <asp:Label ID="lblMessage" runat="server" Visible="false" />
                                        <adv:fileUpload id="fileUpload" runat="server" FileUploadMode="RejectionLetter" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </Content>
                        </ajax:AccordionPane>
                        <ajax:AccordionPane ID="apNoRejectionLetter" runat="server">
                            <Content>
                                Reason why a rejection letter will not be attached<br /><br />
                                <asp:TextBox runat="server" ID="tbNoRejectionLetter" TextMode="MultiLine" Rows="5" Columns="50" />
                            </Content>
                        </ajax:AccordionPane>
                        <ajax:AccordionPane ID="apRejectionLetterBlank" runat="server" />
                    </Panes>
                </ajax:Accordion>
            </td>
        </tr>
    </table>

</asp:Content>
