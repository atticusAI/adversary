﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.DataLayer;
using AjaxControlToolkit;
using Adversary.Utils;
using Adversary.Web.Controls;

namespace Adversary.Web.Screening
{
    public partial class Staffing : ScreeningBasePage
    {
        private MemoStaffing __MemoStaffing;

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            __MemoStaffing = new MemoStaffing(ThisMemo.ScrMemID);
            if (!Page.IsPostBack)
            {
                __MemoStaffing.Get();
                ucBillingAttorney.SelectedEmployeeCode = __MemoStaffing.BillingAttorney;
                BindGrid();
            }
        }

        public override void Validate()
        {
            __MemoStaffing.BillingAttorney = ((HiddenField)ucBillingAttorney.FindControl("hidEmployeeCode")).Value;
            __MemoStaffing.Save();
        }

        private void BindGrid()
        {
            StaffingControl.DataSource = __MemoStaffing.GetStaffingList()
                .OrderBy(f => f.PersonnelID);
            StaffingControl.DataBind();
        }

        public void StaffingControl_PreRender(object sender, EventArgs e)
        {
            BindGrid();
        }

        public void StaffingControl_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            BindGrid();
        }

        public void StaffingControl_AddClick(object sender, EventArgs e)
        {
            PersonAutoComplete p = (PersonAutoComplete)StaffingControl.FindAddControl("ucStaffingID");
            HiddenField hidStaffingID = (HiddenField)p.FindControl("hidEmployeeCode");
            __MemoStaffing.AddStaffMember(hidStaffingID.Value);
            BindGrid();
        }

        public void StaffingControl_DeleteClick(object sender, EventArgs e)
        {
            HiddenField hidDeleteStaffing = (HiddenField)StaffingControl.FindDeleteControl("hidDeleteStaffingID");
            __MemoStaffing.DeleteStaffMember(Int32.Parse(hidDeleteStaffing.Value));
            BindGrid();
        }

        public void StaffingControl_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Adversary.DataLayer.Staffing S =
                    (Adversary.DataLayer.Staffing)e.Row.DataItem;
                e.Row.Cells[1].Text = AdversaryDataLayer.GetEmployee(S.PersonnelID).EmployeeName +
                    "(" + S.PersonnelID.ToString() + ")";
            }
        }
    }
}