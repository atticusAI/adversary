﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Screening/Screening.Master" AutoEventWireup="true" CodeBehind="ConflictIdentification.aspx.cs" 
    Inherits="Adversary.Web.Screening.ConflictIdentification" Theme="HH" %>

<asp:Content ID="cntHead" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            formValidation = hh.validateWithjQuery("#form1");

            $("span.answer_required input:checkbox").addClass("answer_required");
            $("input.answer_required:checkbox").first().attr("title", "* You have not checked any boxes. If the client or adverse party does not fit any of the criteria listed please select 'None of the above'.");

            jQuery.validator.addMethod("answer_required", function (value, element) {
                return true;
                var valid = false;
                if (element.type == "checkbox") {
                    var n = $(".answer_required:checked").length;
                    valid = (n >= 1);
                }
                return valid;
            }, "");

            var id = $("span.answer_required.exclusive input:checkbox").attr("id");
            if (id) {
                var chk = $get(id);
                if (chk) {
                    chkExclusive(chk, id);
                }
            }
        });

        function chkExclusive(chk, id) {
            if (chk.checked) {
                $("input.answer_required:checkbox:not(#" + id + ")").each(function () {
                    $(this).attr("checked", false);
                    $(this).attr("disabled", "disabled");
                });
            }
            else {
                $("input.answer_required:checkbox").removeAttr("disabled");
            }
        }
    </script>
</asp:Content>
<asp:Content ID="cntHeader" ContentPlaceHolderID="cphHeader" runat="server">
</asp:Content>
<asp:Content ID="cntMain" ContentPlaceHolderID="cphMainContent" runat="server">
    <asp:DataList ID="lstConflicts" runat="server" OnInit="lstConflicts_Init" DataKeyField="ConflictID"
        OnItemDataBound="lstConflicts_ItemDataBound">
        <HeaderTemplate>
            <table><tbody>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td>
                    <asp:Label ID="lblAnswer" runat="server" AssociatedControlID="chkAnswer" Text='<%#Eval("Description")%>'></asp:Label>
                </td>
                <td>
                    <asp:CheckBox CssClass="answer_required" ID="chkAnswer"  runat="server" Checked='<%#Eval("Answer")%>' />
                    <asp:HiddenField ID="hidConflict" runat="server" Value='<%#Eval("ConflictID")%>' />
                </td>
            </tr>
        </ItemTemplate>
        <AlternatingItemTemplate>
            <tr style="background-color:silver;">
                <td>
                    <asp:Label ID="lblAnswer" runat="server" AssociatedControlID="chkAnswer" Text='<%#Eval("Description")%>'></asp:Label>
                </td>
                <td>
                    <asp:CheckBox CssClass="answer_required" ID="chkAnswer" runat="server" Checked='<%#Eval("Answer")%>' />
                    <asp:HiddenField ID="hidConflict" runat="server" Value='<%#Eval("ConflictID")%>' />
                </td>
            </tr>
        </AlternatingItemTemplate>
        <FooterTemplate>
            </tbody></table>
        </FooterTemplate>
    </asp:DataList>
    <asp:Panel ID="pnlErrs" runat="server" CssClass="errdiv">
    </asp:Panel>
    <asp:CustomValidator ID="valPage" runat="server" OnServerValidate="valPage_ServerValidate" />
</asp:Content>
<asp:Content ID="cntLower" ContentPlaceHolderID="cphLowerContent" runat="server">
</asp:Content>
