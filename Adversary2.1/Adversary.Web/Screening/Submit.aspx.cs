﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.DataLayer;
using Adversary.Screening;
using Adversary.Utils;

namespace Adversary.Web.Screening
{
    public partial class Submit : ScreeningBasePage
    {
        Adversary.Screening.MemoMetadata __MemoData;
        Adversary.DataLayer.TrackingData __TrackingData;
        Adversary.DataLayer.AdversaryData __AdversaryData;
       
        protected override void OnLoad(EventArgs e)
        {
            if (base.HasScreeningMemoError) return;

            __MemoData = new MemoMetadata(ThisMemo);
            __TrackingData = new TrackingData(ThisMemo);
            __AdversaryData = new AdversaryData();
            


            //assign the screening memo ID to the existing files control
            advExistingFiles.ScreeningMemoID = ThisMemo.ScrMemID;
            advExistingFiles.FileUploadDirectory = base.FileUploadDirectory;

            fileUploadAdditionalFiles.ScreeningMemoID = ThisMemo.ScrMemID;
            fileUploadAdditionalFiles.FileUploadDirectory = base.FileUploadDirectory;

            //set the URL property of the hyperlink to print view
            // hlPrintView.NavigateUrl = "PrintView.aspx?ScrMemID=" + ThisMemo.ScrMemID.ToString();
            //this.lnkPrint.OnClientClick = "hh.popWindow(\"PrintView.aspx?ScrMemID=" + ThisMemo.ScrMemID.ToString()
            //    + "\"); return false;";


            //set up all events
            fileUploadAdditionalFiles.FileUploadSuccessHandler += new Web.Controls.FileUpload.OnFileUploadSuccess(fileUploadAdditionalFiles_FileUploadSuccessHandler);
            
            //lbSubmitMemo.Click += new EventHandler(lbSubmitMemo_Click);
            btnSubmitMemo.Click += new EventHandler(lbSubmitMemo_Click);


            //evaluate tracking status - if the tracking status is set, then the
            //rules don't need to be run and this method will end.
            __TrackingData.Get();
            if (__TrackingData.Tracking != null)
            {
                if (__TrackingData.Tracking.StatusTypeID.HasValue && __TrackingData.Tracking.StatusTypeID.Value >= 1)
                {
                    base.ErrorSummary.DisplayMessage = GetStatusMessage(Convert.ToInt32(__TrackingData.Tracking.StatusTypeID));
                    base.ErrorSummary.DisplayType = Web.Controls.MessageDisplayType.DisplayMessage;
                    base.ErrorSummary.ErrorList = __TrackingData.RejectionReasons;
                    //trSubmitRow.Visible = (__TrackingData.Tracking.RejectionHold.HasValue && __TrackingData.Tracking.RejectionHold.Value);
                    trSubmitRow.Visible = __MemoData.CanSubmit();
                    return; //tracking status set, exit
                }
            }

           
            //run the rules
            List<string> __violatedRules = RunRules();
            if (__violatedRules.Count > 0)
            {
                trSubmitRow.Visible = false;
                base.ErrorSummary.ErrorList.AddRange(__violatedRules);
                base.ErrorSummary.DisplayMessage = "This screening memo has errors that require attention.";
                base.ErrorSummary.DisplayType = Web.Controls.MessageDisplayType.DisplayError;
            }
            else
            {
                base.ErrorSummary.DisplayMessage = "This screening memo passes validation and can be submitted.";
                base.ErrorSummary.DisplayType = Web.Controls.MessageDisplayType.DisplayValid;
            }


           

            if (!Page.IsPostBack) BindGrids();
        }

        private List<string> RunRules()
        {
            //run rules on the memo
            Adversary.Screening.MemoRuleEngine _memoRule = new MemoRuleEngine(ThisMemo);
            List<string> _errorMessages = new List<string>();
            _memoRule.RunRules();
            if (_memoRule.HasErrors)
            {
                foreach (AllRule __rule in _memoRule.RuleViolations)
                {
                    _errorMessages.Add(__rule.RuleText);
                }
            }
           
            return _errorMessages;
           
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            //base.HideErrorSummary();
            if (__TrackingData.StatusTypeID == 1)
            {
                //lbSubmitMemo.Visible = false;
                btnSubmitMemo.Visible = false;
            }
            if (ThisMemo.HH_IsMemoResubmit())
            {
                btnSubmitMemo.Text = "Re-Submit This Memo";
            }
        }

        //public void lbPrintView_Click(object sender, EventArgs e)
        //{
        //    Page.ClientScript.RegisterClientScriptBlock(
        //        typeof(string),
        //        "newPrintWindow",
        //        "openExternalWindow('" + Request.ApplicationPath + "/Screening/PrintView.aspx?ScrMemType=" + this.ThisMemo.ScrMemType.ToString() + "&ScrMemID=" + this.ThisMemo.ScrMemID.ToString() + "');",
        //        true);
        //}

        public void lbSubmitMemo_Click(object sender, EventArgs e)
        {
            

            if (!__MemoData.CanSubmit())
            {
                //memo can't be submitted

                //set the status message in ErrorSummary
                //if (__TrackingData.RejectionReasons.Count > 0)
                //{
                //    //build the error string
                //    advErrorSummary.ErrorList = __TrackingData.RejectionReasons;
                //}


                return;
            }
            
            //get (or set up) the tracking
            __TrackingData = new TrackingData(ThisMemo.ScrMemID);
            __TrackingData.Get();
            __TrackingData.StatusTypeID = 1; //submitted
            __TrackingData.TrackingID = __TrackingData.Tracking.TrackingID;

            bool __isResubmit = (__TrackingData.Tracking.RejectionHold.HasValue && __TrackingData.Tracking.RejectionHold.Value);
            if (!__isResubmit)
            {
                __TrackingData.SubmittedOn = DateTime.Now;
            }
            else
            {
                __TrackingData.SubmittedOn = (__TrackingData.Tracking.SubmittedOn ?? DateTime.Now);
            }
            __TrackingData.Save();
            __TrackingData.Get();


            base.ErrorSummary.DisplayMessage = __AdversaryData.GetTrackingStatusTypes().Where(
                    T => T.StatusTypeID.Equals(__TrackingData.StatusTypeID)
                    ).FirstOrDefault().StatusMessage;
            base.ErrorSummary.DisplayType = Web.Controls.MessageDisplayType.DisplayInfo;

            try
            {
                ReloadMemo();
                Adversary.Screening.ScreeningEmail email = new ScreeningEmail();
                email.SendApproversEmail(ThisMemo, __isResubmit, base.BaseURL);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void fileUploadAdditionalFiles_FileUploadSuccessHandler(Adversary.Controls.UploadEventArgs e)
        {
            //run rules after files are uploaded
            RunRules();
            BindGrids();
        }

        void gvAdditionalFiles_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int _attachmentID = Convert.ToInt32(e.CommandArgument);
            Adversary.DataLayer.FileUploadClass fileUploadClass = new DataLayer.FileUploadClass(ThisMemo.ScrMemID);
            fileUploadClass.Delete(_attachmentID);

            //delete the file
            FileUtils.DeleteFile(base.FileUploadDirectory + "\\" + ThisMemo.ScrMemID.ToString() + "\\" + fileUploadClass.FileName);
            BindGrids();
        }

        private void BindGrids()
        {
            FileUploadClass fupFiles = new FileUploadClass(ThisMemo.ScrMemID);
            fupFiles.FileUploadType = FileUploadClass.FileUploadMode.None;
            //gvAdditionalFiles.DataSource = fupFiles.GetAttachments();
            //gvAdditionalFiles.DataBind();

        }

        protected void lnkPrintView_PreRender(object sender, EventArgs e)
        {
            this.lnkPrintView.ScrMemID = ThisMemo.ScrMemID;
        }

        private string GetStatusMessage(int statusTypeID)
        {
            try
            {
                return __AdversaryData.GetTrackingStatusTypes().Where(T => T.StatusTypeID.Equals(statusTypeID)).FirstOrDefault().StatusMessage;
            }
            catch
            {
                return "unknown status";
            }
           
        }

    }
}
