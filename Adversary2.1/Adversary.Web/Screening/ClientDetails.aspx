﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Screening/Screening.Master" AutoEventWireup="true"
    CodeBehind="ClientDetails.aspx.cs" Inherits="Adversary.Web.Screening.ClientDetails"
    Theme="HH" %>

<%@ Register TagPrefix="ajax" Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" %>
<%@ Register Assembly="Adversary.Controls" Namespace="Adversary.Controls" TagPrefix="advControls" %>
<%@ Register TagPrefix="advcontrols" TagName="ClientAutoComplete" Src="~/Controls/ClientAutoComplete.ascx" %>
<%@ MasterType VirtualPath="~/Screening/Screening.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $.validator.addMethod("indivrequired", function (value, element) {
                var valid = true;
                if (getSelectedClientCategory() == "New") {
                    var clientType = getSelectedClientType();
                    if (clientType == "Individual") {
                        valid = false;
                        $(".indivrequired").each(function () {
                            if ($.trim(this.value) != "") {
                                valid = true;
                            }
                        });
                    }
                }
                return valid;
            }, "* You must provide a Fisrt Name, Middle Name, OR Last Name.");

            $.validator.addMethod("orgrequired", function (value, element) {
                var valid = true;
                if (getSelectedClientCategory() == "New") {
                    var clientType = getSelectedClientType();
                    if (clientType == "Organization") {
                        valid = ($.trim(value) != "");
                    }
                }
                return valid;
            }, "* This field is required.");

            $.validator.addMethod("clienttype", function (value, element) {
                var valid = true;
                if (getSelectedClientCategory() == "New") {
                    valid = (getSelectedClientType() != "");
                }
                return valid;
            }, "* You must select a Client Type.");

            $(".clienttype input").addClass("clienttype");

            $.validator.addMethod("clientcat", function (value, element) {
                return (getSelectedClientCategory() != "");
            }, "* You must select a Client Category.");

            $.validator.addMethod("client", function (value, element) {
                var valid = true;
                if (getSelectedClientCategory() == "Existing") {
                    var id = element.id.replace("tbClient", "hidClientCode");
                    var h = $get(id);
                    valid = (h && h.value != "");
                }
                return valid;
            }, "* You must select a valid client.");

            function getSelectedClientType() {
                var clientType = "";
                $(".clienttype input").each(function () {
                    if (this.checked) {
                        clientType = this.value;
                    }
                });
                return clientType;
            }

            function getSelectedClientCategory() {
                var clientCat = "";
                $(".clientcat input").each(function () {
                    if (this.checked) {
                        clientCat = this.value;
                    }
                });
                return clientCat;
            }

            formValidation = hh.validateWithjQuery("#form1");
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMainContent" runat="server">
    <script src="../Scripts/AccordionPane.js" type="text/javascript"></script>
    <%--this first panel is for all other screening memos except rejected --%>
    <asp:Panel ID="pnlClientDetails_AllOthers" runat="server">
        <table border="0" cellpadding="0" cellspacing="0" width="900">
            <tr>
                <td class="mainform" colspan="2">
                    Client Type
                </td>
            </tr>
            <tr>
                <td class="mainForm" colspan="2">
                    <asp:RadioButtonList runat="server" ID="rblClientType" RepeatLayout="Table" RepeatDirection="Horizontal"
                        CausesValidation="false" AutoPostBack="false" CssClass="clienttype">
                        <asp:ListItem Text="Organization" Value="Organization" />
                        <asp:ListItem Text="Individual" Value="Individual" />
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2" height="200" class="mainForm">
                    <ajax:Accordion ID="accClientType" Width="100%" FadeTransitions="true" FramesPerSecond="60"
                        Height="199" RequireOpenedPane="false" runat="server" TransitionDuration="200"
                        SelectedIndex="-1" AutoSize="None">
                        <Panes>
                            <ajax:AccordionPane ID="apOrganization" runat="server">
                                <Content>
                                    <table border="0" cellpadding="1" cellspacing="0" width="100%">
                                        <tr>
                                            <td class="lbl required">
                                                Organization Name:
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" TextMode="MultiLine" AutoPostBack="false" ID="tbCompanyName"
                                                    Rows="3" Columns="60" CssClass="orgrequired" title="* Organization Name is required." />
                                            </td>
                                        </tr>
                                        <%--<tr>
                                                <td valign="top">Does this client have affiliates?</td>
                                                <td valign="top">
                                                    <asp:RadioButtonList runat="server" ID="rblClientAffiliates" RepeatDirection="Horizontal">
                                                        <asp:ListItem Text="Yes" />
                                                        <asp:ListItem Text="No" />
                                                    </asp:RadioButtonList>
                                                    <asp:LinkButton ID="lbAddClientAffiliates" runat="server" Text="Add Affiliates" CausesValidation="false" />
                                                    <ajax:ModalPopupExtender ID="mdlClientAffiliates" runat="server" TargetControlID="lbAddClientAffiliates"
                                                        PopupControlID="pnlClientAffiliateEntry" PopupDragHandleControlID="pnlClientAffiliateEntry" />
                                                    <asp:GridView ID="gvClientAffiliates" runat="server" AutoGenerateColumns="false" OnRowCommand="gvClientAffiliates_RowCommand"
                                                        SkinID="HH_Gridview">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lbModifyClientAffiliate" runat="server" Text="Modify" 
                                                                        CausesValidation="false" CommandArgument='<%#Eval("PartyID")%>' />
                                                                    
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="PartyRelationshipName" />
                                                        </Columns>
                                                    </asp:GridView>     
                                                    <asp:Button ID="btnModifyClientAffiliate_Hidden" runat="server" style="display:none;"/>
                                                                    <ajax:ModalPopupExtender ID="mdlModifyClientAffiliate" runat="server" 
                                                                        TargetControlID="btnModifyClientAffiliate_Hidden"
                                                                        PopupControlID="pnlClientAffiliateModify" CancelControlID="btnClientAffiliateModifyCancel" />                                               
                                                </td>
                                            </tr>--%>
                                    </table>
                                </Content>
                            </ajax:AccordionPane>
                            <ajax:AccordionPane ID="apIndividual" runat="server">
                                <Content>
                                    <table border="0" cellpadding="2" cellspacing="3" width="100%">
                                        <tr>
                                            <td class="lbl required">
                                                First Name:
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="tbFirstName" CssClass="indivrequired" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="lbl required">
                                                Middle Name:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbMiddleName" runat="server" CssClass="indivrequired" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="lbl required">
                                                Last Name:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbLastName" runat="server" CssClass="indivrequired" />
                                            </td>
                                        </tr>
                                    </table>
                                </Content>
                            </ajax:AccordionPane>
                        </Panes>
                    </ajax:Accordion>
                </td>
            </tr>
            <tr>
                <td class="mainForm">
                    Is this client a debtor in a pending bankruptcy?<br />
                    <i>If you do not know, please ask the client.</i>
                </td>
                <td class="mainForm">
                    <asp:RadioButtonList runat="server" ID="rblPendingBankruptcy" RepeatDirection="Horizontal">
                        <asp:ListItem Text="Yes" Value="1" />
                        <asp:ListItem Text="No" Value="0" />
                    </asp:RadioButtonList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <%--this panel is for the rejected memo type --%>
    <asp:Panel ID="pnlClientDetails_RejectedMatter" runat="server">
        <table border="0" cellpadding="0" cellspacing="0" width="900">
            <tr>
                <td class="mainForm" colspan="2">
                    Please select if this is New or Existing Client<br />
                    <asp:RadioButtonList runat="server" RepeatDirection="Horizontal" ID="rblNewExistingClientRejected" CssClass="clientcat">
                        <asp:ListItem Text="New" />
                        <asp:ListItem Text="Existing" />
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <ajax:Accordion ID="accRejected" Width="100%" FadeTransitions="true" FramesPerSecond="60"
                        Height="199" RequireOpenedPane="false" runat="server" TransitionDuration="200"
                        SelectedIndex="-1" AutoSize="None">
                        <Panes>
                            <ajax:AccordionPane ID="apRejectedNewClient" runat="server">
                                <Content>
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td colspan="2">
                                                Client Type
                                            </td>   
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RadioButtonList runat="server" RepeatDirection="Horizontal" ID="rblClientTypeRejected" CssClass="clienttype">
                                                    <asp:ListItem Text="Organization" Value="Organization" />
                                                    <asp:ListItem Text="Individual" Value="Individual" />
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <ajax:Accordion ID="accClientTypeRejected" Width="100%" FadeTransitions="true" FramesPerSecond="60"
                                                    Height="199" RequireOpenedPane="true" runat="server" TransitionDuration="200"
                                                    SelectedIndex="1" AutoSize="None">
                                                    <Panes>
                                                        <ajax:AccordionPane ID="apClientTypeRejectedOrganization" runat="server">
                                                            <Content>
                                                                <table border="0" cellpadding="1" cellspacing="0" width="100%">
                                                                    <tr>
                                                                        <td class="lbl required">
                                                                            Organization Name:
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="tbCompanyNameRejected" runat="server" AutoPostBack="false" CausesValidation="false"
                                                                                TextMode="MultiLine" Rows="3" Columns="60" CssClass="orgrequired" title="* Organization Name is required." />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </Content>
                                                        </ajax:AccordionPane>
                                                        <ajax:AccordionPane ID="apClientTypeRejectedIndividual" runat="server">
                                                            <Content>
                                                                <table border="0" cellpadding="2" cellspacing="3" width="100%">
                                                                    <tr>
                                                                        <td class="lbl required">
                                                                            First Name:
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox runat="server" ID="tbFirstNameRejected" SkinID="HH_Textbox" CssClass="indivrequired" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="lbl required">
                                                                            Middle Name:
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="tbMiddleNameRejected" runat="server" SkinID="HH_Textbox" CssClass="indivrequired"  />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="lbl required">
                                                                            Last Name:
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="tbLastNameRejected" runat="server" SkinID="HH_Textbox" CssClass="indivrequired" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </Content>
                                                        </ajax:AccordionPane>
                                                    </Panes>
                                                </ajax:Accordion>
                                            </td>
                                        </tr>
                                        <tr id="trPendingBankruptcyRejected" runat="server" onprerender="trPendingBankruptcyRejected_PreRender">
                                            <td class="mainForm">
                                                Is this client a debtor in a pending bankruptcy?<br />
                                                <i>If you do not know, please ask the client.</i>
                                            </td>
                                            <td class="mainForm">
                                                <asp:RadioButtonList runat="server" ID="rblPendingBankruptcyRejected" RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="Yes" Value="1" />
                                                    <asp:ListItem Text="No" Value="0" />
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                    </table>
                                </Content>
                            </ajax:AccordionPane>
                            <ajax:AccordionPane ID="apRejectedExistingClient" runat="server">
                                <Content>
                                    <advcontrols:ClientAutoComplete ID="advRejectedClientName" runat="server" ServiceMethod="ClientService" TextBoxCssClass="client" />
                                </Content>
                            </ajax:AccordionPane>
                        </Panes>
                    </ajax:Accordion>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <%--<asp:Panel ID="pnlClientAffiliateEntry" runat="server">
            <asp:UpdatePanel ID="upnlClientAffiliates" runat="server" UpdateMode="Always" ChildrenAsTriggers="true">
                <ContentTemplate>
                    <asp:Wizard runat="server" ID="affiliateWizard" OnNextButtonClick="affiliateWizard_NextButtonClick" CancelButtonText="Cancel"
                        OnCancelButtonClick="affiliateWizard_CancelButtonClick" CancelButtonType="Button"
                        DisplayCancelButton="true" BackColor="White"
                        OnFinishButtonClick="affiliateWizard_FinishButtonClick" OnSideBarButtonClick="affiliateWizard_SideBarButtonClick"
                        Width="450px" Height="200px" BorderColor="Navy" BorderStyle="Solid" BorderWidth="1px">
                        <SideBarStyle VerticalAlign="Top" />
                        <WizardSteps>
                            <asp:WizardStep ID="partyNameStep" runat="server" Title="Enter Party Names">
                                <asp:TextBox runat="server" ID="tbPartyNames" TextMode="MultiLine" Columns="40" Rows="4" />
                            </asp:WizardStep>
                            <asp:WizardStep ID="definePartyRelationshipsStep" runat="server" 
                                Title="Define Party Relationships">
                                <asp:GridView runat="server" ID="gvPartyRelationships" AutoGenerateColumns="false" 
                                    OnRowDataBound="gvPartyRelationships_RowDataBound"
                                    ShowHeader="false"
                                    ShowFooter="false">
                                    <Columns>
                                        <asp:TemplateField ShowHeader="false">
                                            <ItemTemplate>
                                                <asp:TextBox ID="tbName" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ShowHeader="false">
                                            <ItemTemplate>
                                                <asp:DropDownList runat="server" ID="ddlPartyType">
                                                    <asp:ListItem Text="Individual" />
                                                    <asp:ListItem Text="Organization" />
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ShowHeader="false">
                                            <ItemTemplate>
                                                <advControls:PartyRelationshipDropDown ID="PartyRelationshipDropDown" runat="server" RelationshipDisplayMode="AffiliateParties" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </asp:WizardStep>
                        </WizardSteps>
                    </asp:Wizard>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>--%>
    <%--<asp:Panel ID="pnlClientAffiliateModify" runat="server">
            <asp:UpdatePanel ID="upnlClientAffiliateModify" runat="server" UpdateMode="Always" ChildrenAsTriggers="true">
                <ContentTemplate>
                    <asp:HiddenField ID="hidClientAffiliateModifyPartyID" runat="server" />
                    <table border="0" cellpadding="0" cellspacing="0" style="background-color:wheat;">
                        <tr>
                            <td colspan="2">
                                Party Type:
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:RadioButtonList runat="server" ID="rblClientAffiliateModifyPartyType">
                                    <asp:ListItem Text="Individual" />
                                    <asp:ListItem Text="Organization" />
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <ajax:Accordion ID="accClientAffiliateModify" Width="100%" FadeTransitions="true" FramesPerSecond="60" Height="199"
                                        RequireOpenedPane="true" runat="server" TransitionDuration="200" SelectedIndex="-1" AutoSize="None">
                                        <Panes>
                                        <ajax:AccordionPane ID="apClientAffiliateModifyIndividual" runat="server">
                                            <Content>
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>First Name</td>
                                                        <td><asp:TextBox ID="tbClientAffiliateModifyFirstName" runat="server" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Middle Name</td>
                                                        <td><asp:TextBox ID="tbClientAffiliateModifyMiddleName" runat="server" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Last Name</td>
                                                        <td><asp:TextBox ID="tbClientAffiliateModifyLastName" runat="server" /></td>
                                                    </tr>
                                                </table>
                                            </Content>
                                        </ajax:AccordionPane>
                                        <ajax:AccordionPane ID="apClientAffiliateModifyOrganization" runat="server">
                                            <Content>
                                                <table border="0" cellpadding="1" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td valign="top">
                                                            Organization Name:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox runat="server" TextMode="MultiLine"
                                                                AutoPostBack="false" ID="tbClientAffiliateModifyOrganization"
                                                                Rows="3" Columns="60" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </Content>
                                        </ajax:AccordionPane>
                                        </Panes>
                                </ajax:Accordion>
                            </td>
                        </tr>
                            
                        <tr>
                            <td>
                                Party Relationship:
                            </td>
                            <td>
                                <advControls:PartyRelationshipDropDown ID="partyrelClientAffiliateModify" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <asp:LinkButton ID="btnClientAffiliateModifySubmit" runat="server" Text="Submit" OnClick="btnClientAffiliateModifySubmit_Click" />
                                    &nbsp;&nbsp;&nbsp;
                                <asp:LinkButton ID="btnClientAffiliateModifyCancel" runat="server" Text="Cancel" />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>--%>
    <asp:Panel ID="pnlErrs" runat="server" CssClass="errdiv">
    </asp:Panel>
</asp:Content>
