﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Adversary.Web.Screening
{
    
    public partial class Default : ScreeningBasePage
    {
        Adversary.DataLayer.ScreeningDefault _dataLayer = null;
        Adversary.DataLayer.ScreeningDefault DataLayer
        {
            get
            {
                if (_dataLayer == null)
                {
                    _dataLayer = new DataLayer.ScreeningDefault();
                }
                return _dataLayer;
            }
        }

        Adversary.DataLayer.AdversaryData _AdversaryData;

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (this.Page.Master is ScreeningMasterPage)
            {
                ((ScreeningMasterPage)this.Page.Master).ShowNavigationButtons = false;
            }
        }

        public override void Dispose()
        {
            if (_dataLayer != null)
            {
                _dataLayer.Dispose();
                _dataLayer = null;
            }
            base.Dispose();
        }

        protected void lnkNew_Click(object sender, EventArgs e)
        {
            int scrMemType = -1;
            int scrMemID = -1;
            if (Int32.TryParse(((LinkButton)sender).CommandArgument, out scrMemType))
            {
                //call create memo function here
                _AdversaryData = new Adversary.DataLayer.AdversaryData();
                scrMemID = _AdversaryData.CreateMemo(scrMemType).ScrMemID;
                //pass screening memo ID to the page
                Response.Redirect(string.Format("~/Screening/BasicInformation.aspx?ScrMemType={0}&scrmemid={1}", scrMemType, scrMemID));
            }
        }

        protected void smlMyMemos_PreRender(object sender, EventArgs e)
        {
            ParameterCollection parms = this.smlMyMemos.SelectParameters;
            parms["personnelID"].DefaultValue = this.CurrentUser.CurrentUserPayrollID;
            smlMyMemos.ObjectInstance = DataLayer;
            smlMyMemos.DataBind();
        }
    }
}

