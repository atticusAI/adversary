﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.Utils;
using Adversary.DataLayer;

namespace Adversary.Web.Screening
{
    public partial class ProBonoReason : ScreeningBasePage
    {
        private ProBonoReasonScreen _proBonoScreen;

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this._proBonoScreen = new ProBonoReasonScreen(ThisMemo.ScrMemID);
            this._proBonoScreen.Get();
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            this.tbProBonoReason.Text = this._proBonoScreen.ProBonoReason;
            if (this._proBonoScreen.ProBonoLimitedMeans ?? false)
            {
                this.rbNo.Checked = false;
                this.rbYes.Checked = true;
            }
            else
            {
                this.rbNo.Checked = true;
                this.rbYes.Checked = false;
            }
        }

        public override void Validate()
        {
            base.Validate();
            this._proBonoScreen.ProBonoReason = this.tbProBonoReason.Text;
            this._proBonoScreen.ProBonoLimitedMeans = this._proBonoLimitedMeans;
            this._proBonoScreen.Save();
        }

        private bool? _proBonoLimitedMeans
        {
            get
            {
                bool? val = null;

                if (this.rbYes.Checked)
                {
                    val = true;
                }
                else if (this.rbNo.Checked)
                {
                    val = false;
                }
                return val;
            }
        }
    }
}