﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.Utils;
using Adversary.DataLayer;
using Adversary.Web.Controls;
using Adversary.Web.Extensions;
using System.Text;

namespace Adversary.Web.Screening
{
    public partial class CM : ScreeningBasePage
    {
        private bool _blnEnableNDrive = true;
        private bool _NDriveSelected = false;
        private bool _IsAjaxPostback = false;
        private bool _NDriveMemberAdded = false;

        private Adversary.DataLayer.NDriveStaff _NDriveStaffClass;
        List<NDriveAccess> _NDriveAccessTeamMembers;
        private bool __ItemWasDeleted = false;

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            _NDriveStaffClass = new DataLayer.NDriveStaff(ThisMemo.ScrMemID);
            _NDriveAccessTeamMembers = _NDriveStaffClass.GetNDriveAccess();
                        
            //rblCreateNDriveFolder.SelectedIndexChanged += new EventHandler(rblCreateNDriveFolder_SelectedIndexChanged);
            //ndriveDropDown.NDriveSelected += new Adversary.Controls.NDriveFolderTypeDropDown.NDriveSelectedEventHandler(ndriveDropDown_NDriveSelected);

            advClientDropDown.AutoCompletePopulated += new ClientAutoComplete.ClientAutoCompleteHandler(advClientDropDown_AutoCompletePopulated);

            if (!String.IsNullOrEmpty(ThisMemo.ClientNumber))
            {
                Adversary.Web.Controls.ARTable arTable = new Controls.ARTable();
                arTable.ClientNumber = ThisMemo.ClientNumber;
                pnlARInfo.Controls.Add(arTable);
            }
        }

        void advClientDropDown_AutoCompletePopulated(object sender, Adversary.Controls.ClientAutoCompletePopulatedEventArgs e)
        {
            pnlARInfo.Controls.Clear();
            Adversary.Web.Controls.ARTable arTable = (Web.Controls.ARTable)LoadControl("~/controls/ARTable.ascx");
            arTable.ClientNumber = e.ClientNumber;
            pnlARInfo.Controls.Add(arTable);
            upnlARInfo.Update();
        }

        protected override void OnPreRenderComplete(EventArgs e)
        {
            base.OnPreRenderComplete(e);
            //create N-Drive javascript
            StringBuilder sbRadio = new StringBuilder();
            sbRadio.Append(Adversary.Utils.StringUtils.JAVASCRIPT_TAG_STARTER);
            sbRadio.Append("function enableNDrive(enabled){");
            sbRadio.Append("if(enabled){");
            sbRadio.Append("$get('" + ndriveDropDown.ddlNDriveTypes.ClientID + "').disabled = true;");
            sbRadio.Append("$get('" + ndriveDropDown.ddlNDriveTypes.ClientID + "').selectedIndex = 0;");
            sbRadio.Append("$get('" + divNDriveStaff.ClientID + "').style.display='none';");
            sbRadio.Append("}else{");
            sbRadio.Append("$get('" + ndriveDropDown.ddlNDriveTypes.ClientID + "').disabled = false;");
            //sbRadio.Append("alert($get('" + ndriveDropDown.ddlNDriveTypes.ClientID + "').selectedIndex);");
            sbRadio.Append("if($get('" + ndriveDropDown.ddlNDriveTypes.ClientID + "').selectedIndex >= 2){");
            sbRadio.Append("$get('" + divNDriveStaff.ClientID + "').style.display='block';");
            sbRadio.Append("}");
            sbRadio.Append("else{$get('" + divNDriveStaff.ClientID + "').style.display='none';}");
            sbRadio.Append("}");
            sbRadio.Append("}</script>");

            StringBuilder sbDropDown = new StringBuilder();
            sbDropDown.Append(Adversary.Utils.StringUtils.JAVASCRIPT_TAG_STARTER);
            sbDropDown.Append("function showNDriveStaff(selectedVal){");
            //sbDropDown.Append("alert(selectedVal);");
            sbDropDown.Append("if(selectedVal >= 2){");
            sbDropDown.Append("$get('" + divNDriveStaff.ClientID + "').style.display='block';");
            sbDropDown.Append("}");
            sbDropDown.Append("else{$get('" + divNDriveStaff.ClientID + "').style.display='none';");
            sbDropDown.Append("}");
            sbDropDown.Append("}</script>");

            StringBuilder sbStartup = new StringBuilder();
            sbStartup.Append(Adversary.Utils.StringUtils.JAVASCRIPT_TAG_STARTER);
            sbStartup.Append("showNDriveStaff($get('" + ndriveDropDown.ddlNDriveTypes.ClientID + "').selectedIndex);");
            sbStartup.Append("</script>");


            if (!ClientScript.IsClientScriptBlockRegistered(typeof(string), "enableNDrive"))
            {
                ClientScript.RegisterClientScriptBlock(typeof(string), "enableNDrive", sbRadio.ToString());
            }
            if (!ClientScript.IsClientScriptBlockRegistered(typeof(string), "showNDriveStaff"))
            {
                ClientScript.RegisterClientScriptBlock(typeof(string), "showNDriveStaff", sbDropDown.ToString());
            }
            if (!ClientScript.IsStartupScriptRegistered(typeof(string),"startupscript"))
            {
                ClientScript.RegisterStartupScript(typeof(string), "startupscript", sbStartup.ToString());
            }

            rblCreateNDriveFolder.Items[0].Attributes.Add("onclick", "enableNDrive(false)");
            rblCreateNDriveFolder.Items[1].Attributes.Add("onclick", "enableNDrive(true)");
            ndriveDropDown.ddlNDriveTypes.Attributes.Add("onchange", "showNDriveStaff(this.selectedIndex)");
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            //Change request #12 - disallow changing the practice code when the memo has already been approved.
            //only members of the adversary/adversary admin groups can make any change.
            if (ThisMemo.Trackings != null && ThisMemo.Trackings.Count > 0)
            {
                if (ThisMemo.Trackings[0].AdversaryAdminApproved.HasValue && 
                    ThisMemo.Trackings[0].AdversaryAdminApproved.Value &&
                    !(CurrentUser.IsAdversary || CurrentUser.IsAdversaryAdmin))
                {
                    pcDropDown.RestrictToSelectedCodeOnly = true;
                }
            }

            if (ThisMemo.ScrMemType == MemoType.LegalWork)
            {
                this.trSecondCode.Visible = true;
                this.pcSecondDropDown.RemovedCodes = Adversary.DataLayer.AdversaryData.ProBonoPracticeCodes;
                this.pcSecondDropDown.SelectedPracticeCode = ThisMemo.SecondPracCode;
            }
            else
            {
                this.trSecondCode.Visible = false;
            }

            #region rendering to happen the first time the page is called or re-entry, not after a post back

            if (!Page.IsPostBack && !__ItemWasDeleted)
            {
                if (!_NDriveSelected && !_IsAjaxPostback)
                {
                    if (ThisMemo.ScrMemType == MemoType.ProBono) //pro bono
                    {
                        //pcDropDown.SelectedPracticeCode = "105X";
                        //pcDropDown.RestrictToSelectedCodeOnly = true;
                        pcDropDown.IsProBonoOnly = true;
                        trEstimateOfFees.Visible = false;
                        pcDropDown.SelectedPracticeCode = ThisMemo.PracCode;
                    }
                    else if (ThisMemo.ScrMemType == MemoType.LegalWork) //legal work
                    {
                        pcDropDown.IsEmployeeLegalWork = true;
                    }
                    else
                    {
                        //pcDropDown.RemovedCodes = Adversary.Utils.StringUtils.ProBonoPracticeCodes;
                        pcDropDown.RemovedCodes = Adversary.DataLayer.AdversaryData.ProBonoPracticeCodes;
                        pcDropDown.SelectedPracticeCode = ThisMemo.PracCode;
                    }
                }

                if (!String.IsNullOrEmpty(base.ThisMemo.EstFees))
                {
                    estFeesDropdown.SelectedEstimateOfFees = ThisMemo.EstFees;
                }

                if (ThisMemo.NDrive.HasValue)
                {
                    rblCreateNDriveFolder.Items.FindByValue(ThisMemo.NDrive.ToString().ToLower()).Selected = true;

                    ndriveDropDown.ddlNDriveTypes.Enabled = ThisMemo.NDrive.Value;


                    if(!ThisMemo.NDriveType.HasValue)
                    {
                        ndriveDropDown.ddlNDriveTypes.SelectedIndex = -1;
                    }
                    else
                    {
                        if (ThisMemo.NDriveType.HasValue || __ItemWasDeleted)
                        {
                            ndriveDropDown.SetSelectedNDriveType(ThisMemo.NDriveType.ToString());
                            //display the n drive staff if required.
                            if (ndriveDropDown.RequiresNDriveStaff) divNDriveStaff.Style.Add(HtmlTextWriterStyle.Display, "block");
                            else divNDriveStaff.Style.Add(HtmlTextWriterStyle.Display, "none");
                        }
                    }
                }

                advClientDropDown.SelectedClientCode = ThisMemo.ClientNumber;

                //set up the AR table
                //if (!String.IsNullOrEmpty(ThisMemo.ClientNumber))
                //{
                //    advARTable.ClientNumber = ThisMemo.ClientNumber;
                //}
                //else
                //{
                //    pnlARInfo.Visible = false;
                //}

                tbMatterName.Text = ThisMemo.MatterName;
                tbWorkDescription.Text = ThisMemo.WorkDesc;

                if (ThisMemo.ContingFee.HasValue)
                {
                    rblContingencyFee.SelectedIndex = -1;
                    rblContingencyFee.Items.FindByValue((Convert.ToBoolean(ThisMemo.ContingFee)) ? "Yes" : "No").Selected = true;
                }

                if (ThisMemo.ScrMemType.Equals(2))
                {
                    pnlClientDropDown.Visible = true;
                }
                else
                {
                    pnlClientDropDown.Visible = false;
                }
            }

            #endregion
        }

        public override void Validate()
        {
            base.Validate();
            if (this.IsInputValid())
            {
                Adversary.DataLayer.MatterInformation matterinfo = new DataLayer.MatterInformation(ThisMemo.ScrMemID);
                matterinfo.MatterName = tbMatterName.Text;

                if (rblContingencyFee.SelectedIndex > -1)
                    matterinfo.ContingencyFee = (rblContingencyFee.SelectedValue.ToLower().Equals("yes") ? true : false);

                if (rblCreateNDriveFolder.SelectedIndex > -1)
                    matterinfo.NDrive = (rblCreateNDriveFolder.SelectedValue.ToLower().Equals("true") ? true : false);

                HiddenField hdfPreparing = ((HiddenField)this.advClientDropDown.FindControl("hidClientCode"));
                matterinfo.ClientCode = hdfPreparing.Value;

                //if (ndriveDropDown.ddlNDriveTypes != null && ndriveDropDown.ddlNDriveTypes.Enabled && !String.IsNullOrEmpty(ndriveDropDown.ddlNDriveTypes.SelectedValue))
                //    matterinfo.NDriveType = Convert.ToInt32(ndriveDropDown.ddlNDriveTypes.SelectedValue);
                //else
                //    matterinfo.NDriveType = -1;

                if (String.IsNullOrEmpty(ndriveDropDown.ddlNDriveTypes.SelectedValue))
                { matterinfo.NDriveType = -1; }
                else
                { matterinfo.NDriveType = Convert.ToInt32(ndriveDropDown.ddlNDriveTypes.SelectedValue); }

                if (estFeesDropdown.ddlEstimateOfFees != null)
                    matterinfo.FeeEstimate = estFeesDropdown.ddlEstimateOfFees.SelectedValue;
                else
                    matterinfo.FeeEstimate = null;

                matterinfo.PracticeTypeCode = pcDropDown.ddlCodes.SelectedValue;
                matterinfo.SecondPracticeTypeCode = this.pcSecondDropDown.ddlCodes.SelectedValue;
                matterinfo.WorkDescription = tbWorkDescription.Text;
                matterinfo.Save();
            }
        }

        protected void valPage_ServerValidate(object sender, ServerValidateEventArgs e)
        {
            e.IsValid = this.IsInputValid();
        }

        private bool IsInputValid()
        {
            bool isValid = true;
            this.ClearErrors();

            if (string.IsNullOrWhiteSpace(this.tbMatterName.Text))
            {
                this.tbMatterName.HH_SetError(this.pnlErrs);
                isValid = false;
            }

            if (string.IsNullOrWhiteSpace(this.tbWorkDescription.Text))
            {
                this.tbWorkDescription.HH_SetError(this.pnlErrs);
                isValid = false;
            }

            if (string.IsNullOrWhiteSpace(this.pcDropDown.ddlCodes.SelectedValue))
            {
                this.pcDropDown.ddlCodes.HH_SetError(this.pnlErrs);
                isValid = false;
            }

            return isValid;
        }

        private void ClearErrors()
        {
            this.tbMatterName.HH_ClearError();
            this.tbWorkDescription.HH_ClearError();
            this.pcDropDown.HH_ClearError();
            this.pnlErrs.HH_ClearError();
        }

        public void ndriveDropDown_NDriveSelected(object sender, EventArgs e)
        {
            //ndriveDropDown.NDriveEnabled = (rblCreateNDriveFolder.SelectedIndex == 0);
            //_IsAjaxPostback = true;
            //divNDriveStaff.Visible = ndriveDropDown.RequiresNDriveStaff;
        }

        protected void rblCreateNDriveFolder_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            //upnlNDriveDropDown.Update();
        }

        #region List container control methods
        public void NDriveStaff_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Adversary.DataLayer.NDriveAccess N = (NDriveAccess)e.Row.DataItem;
                e.Row.Cells[1].Text = AdversaryDataLayer.GetEmployee(N.PersonnelID).EmployeeName +
                        " (" + N.PersonnelID.ToString() + ")";
            }
        }

        public void NDriveStaff_OnPreRender(object sender, EventArgs e)
        {
            NDriveStaff_DataBind();
            //per PS request on 7/28/2011, removed "Patent Files" and "Patent Working Files", options will enable in the drop down list for backwards compatability 
            this.ndriveDropDown.ddlNDriveTypes.Items[1].Enabled = (ThisMemo.NDriveType.HasValue && ThisMemo.NDriveType.Value.Equals(1));
            this.ndriveDropDown.ddlNDriveTypes.Items[2].Enabled = (ThisMemo.NDriveType.HasValue && ThisMemo.NDriveType.Value.Equals(2));
        }

        private void NDriveStaff_DataBind()
        {
            _NDriveStaffClass.RefreshNDriveTeam();
            NDriveStaff.DataSource = _NDriveStaffClass.NDriveTeamMembers;
            NDriveStaff.DataBind();
        }

        public void NDriveStaff_OnDeleteClick(object sender, EventArgs e)
        {
            HiddenField hidDeletedStaff = (HiddenField)NDriveStaff.FindDeleteControl("hidDeleteNDriveStaff");

            Adversary.DataLayer.NDriveAccess _teamMember;

            _teamMember = _NDriveStaffClass.NDriveTeamMembers.Find(i => i.NDriveAccessID.Equals(Int32.Parse(hidDeletedStaff.Value)));

            if (_NDriveStaffClass.NDriveTeamMembers.Exists(i => i.Equals(_teamMember)))
            {
                _NDriveStaffClass.NDriveTeamMembers.Remove(_teamMember);
                _NDriveStaffClass.Save();
                
            }
            NDriveStaff_DataBind();
            __ItemWasDeleted = true;
        }

        public void NDriveStaff_OnAddClick(object sender, EventArgs e)
        {
            Adversary.DataLayer.NDriveAccess _teamMember = new DataLayer.NDriveAccess();
            _teamMember.ScrMemID = ThisMemo.ScrMemID;
            PersonAutoComplete pautoComplete = (PersonAutoComplete)NDriveStaff.FindAddControl("advPersonDropDown");
            _teamMember.PersonnelID = ((HiddenField)pautoComplete.FindControl("hidEmployeeCode")).Value;
            _NDriveStaffClass.NDriveTeamMembers.Add(_teamMember);
            _NDriveStaffClass.Save();
            _NDriveStaffClass.RefreshNDriveTeam();
        }

        public void NDriveStaff_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            NDriveStaff.DataSource = _NDriveStaffClass.NDriveTeamMembers;
        }
        #endregion

    }
}