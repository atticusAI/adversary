﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Adversary.Web.Screening
{
    public partial class Opposing : ScreeningBasePage
    {
        private Adversary.DataLayer.Screening.OpposingCounselScreen _opposingCounselScreen;

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            _opposingCounselScreen = new Adversary.DataLayer.Screening.OpposingCounselScreen(ThisMemo.ScrMemID);
            advOpposingCounselControl.DataKeyNames = new string[] { "opposingID" };
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (ThisMemo.OppQuestion.HasValue)
            {
                rblOpposingCounsel.Items.FindByValue(ThisMemo.OppQuestion.Value == true ? "Yes" : "No").Selected = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void opposingCounsel_AddClick(object sender, EventArgs e)
        {
            TextBox tbLawFirmName = (TextBox)this.advOpposingCounselControl.FindAddControl("tbInsertLawFirmName");
            TextBox tbLawyerName = (TextBox)this.advOpposingCounselControl.FindAddControl("tbInsertLawyerName");

            _opposingCounselScreen = new DataLayer.Screening.OpposingCounselScreen(ThisMemo.ScrMemID);
            _opposingCounselScreen.LawyerName = tbLawyerName.Text;
            _opposingCounselScreen.LawfirmName = tbLawFirmName.Text;
            _opposingCounselScreen.Save();

            BindGrid();
            advOpposingCounselControl.EditIndex = -1;
        }
        protected void opposingCounsel_AddCancelClick(object sender, EventArgs e)
        {
            
        }
        protected void opposingCounsel_ChangeClick(object sender, EventArgs e)
        {
            TextBox tbLawFirmName = (TextBox)this.advOpposingCounselControl.FindChangeControl("tbEditLawFirmName");
            TextBox tbLawyerName = (TextBox)this.advOpposingCounselControl.FindChangeControl("tbEditLawyerName");
            HiddenField hidEditOpposingID = (HiddenField)this.advOpposingCounselControl.FindChangeControl("hidEditOpposingID");

            _opposingCounselScreen = new DataLayer.Screening.OpposingCounselScreen(ThisMemo.ScrMemID);
            _opposingCounselScreen.LawfirmName = tbLawFirmName.Text;
            _opposingCounselScreen.LawyerName = tbLawyerName.Text;
            _opposingCounselScreen.opposingID = Convert.ToInt32(hidEditOpposingID.Value);
            _opposingCounselScreen.Save();

            BindGrid();
            advOpposingCounselControl.EditIndex = -1;

        }
        protected void opposingCounsel_DeleteClick(object sender, EventArgs e)
        {

        }
        protected void opposingCounsel_PreRender(object sender, EventArgs e)
        {
            BindGrid();
        }
        protected void opposingCounsel_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //LinkButton lb = (LinkButton)e.CommandSource;
            //GridViewRow gvr = (GridViewRow)lb.NamingContainer;
            //int id = (int)advOpposingCounselControl.DataKeys[gvr.RowIndex].Value;

            //Adversary.DataLayer.Screening.OpposingCounselScreen opp = new DataLayer.Screening.OpposingCounselScreen(ThisMemo.ScrMemID);
            //opp.opposingID = id;
            //opp.Get();

            ////GridViewRow row = advOpposingCounselControl.Rows[Convert.ToInt32(f)];
            ////DataLayer.OpposingCounsel opp = (DataLayer.OpposingCounsel)row.DataItem;
            //TextBox tbLawFirmName = (TextBox)this.advOpposingCounselControl.FindChangeControl("tbEditLawFirmName");
            //TextBox tbLawyerName = (TextBox)this.advOpposingCounselControl.FindChangeControl("tbEditLawyerName");
            //HiddenField hidEditOpposingID = (HiddenField)this.advOpposingCounselControl.FindChangeControl("hidEditOpposingID");

            //tbLawFirmName.Text = opp.LawfirmName;
            //tbLawyerName.Text = opp.LawyerName;
            //hidEditOpposingID.Value = opp.opposingID.ToString();
            
        }

        public override void Validate()
        {
            //save the memo information
            if (rblOpposingCounsel.SelectedIndex > -1)
            {
                _opposingCounselScreen = new DataLayer.Screening.OpposingCounselScreen(ThisMemo.ScrMemID);
                _opposingCounselScreen.HasOpposingCounsel = (rblOpposingCounsel.SelectedValue == "Yes" ? true : false);
                _opposingCounselScreen.SaveMemoInformationOnly();
            }
        }

        private void BindGrid()
        {
            advOpposingCounselControl.DataSource = _opposingCounselScreen.GetOpposingCounsel();
            advOpposingCounselControl.DataBind();
        }
    }
}