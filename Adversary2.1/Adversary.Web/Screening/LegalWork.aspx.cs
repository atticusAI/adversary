﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.DataLayer;
using Adversary.Controls;
using Adversary.Utils;

namespace Adversary.Web
{
    public partial class LegalWork : ScreeningBasePage
    {
        NewClientInformation newClient;
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            advLegalWorkEmployeeID.AutoCompletePopulated += new Web.Controls.PersonAutoComplete.PersonAutoCompleteHandler(advLegalWorkEmployeeID_AutoCompletePopulated);
            rblLegalWorkType.HH_AddAccordionPaneJavascript(accLegalWork.ClientID);
        }

        public void advLegalWorkEmployeeID_AutoCompletePopulated(object sender, PersonAutoCompletePopulatedEventArgs e)
        {
            hidLegalWorkEmployeeID.Value = e.PersonnelID;
            tbLegalWorkEmployeeFirstName.Text = e.EmployeeFirstName;
            tbLegalWorkEmployeeMiddleName.Text = e.EmployeeMiddleName;
            tbLegalWorkEmployeeLastName.Text = e.EmployeeLastName;
            //send to the database

            newClient = new NewClientInformation(ThisMemo.ScrMemID);
            newClient.ClientFirstName = e.EmployeeFirstName;
            newClient.ClientMiddleName = e.EmployeeMiddleName;
            newClient.ClientLastName = e.EmployeeLastName;
            newClient.HHLegalWorkID = Convert.ToInt32(e.PersonnelID);
            newClient.ClientType = "Individual";
            newClient.Save();

            advLegalWorkEmployeeID.SelectedEmployeeCode = e.PersonnelID;
        }

        //public void advLegalWorkEmployeeID_AutoCompletePopulated(Adversary.Controls.AutoCompletePopulatedEventArgs e)
        //{
        //    hidLegalWorkEmployeeID.Value = e.PersonnelID;
        //    tbLegalWorkEmployeeFirstName.Text = e.EmployeeFirstName;
        //    tbLegalWorkEmployeeMiddleName.Text = e.EmployeeMiddleName;
        //    tbLegalWorkEmployeeLastName.Text = e.EmployeeLastName;
        //}

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            newClient = new NewClientInformation(ThisMemo.ScrMemID);
            newClient.Get();

            rblLegalWorkType.SelectedIndex = -1;

            if (newClient == null)
                tbLegalWorkFirm.Text = "Holland & Hart";
            else if (newClient.ClientType == null)
                tbLegalWorkFirm.Text = "Holland & Hart";

            else if (newClient.ClientType == "Organization")
            {
                rblLegalWorkType.Items.FindByValue("Firm").Selected = true;
                accLegalWork.SelectedIndex = 0;
                tbLegalWorkFirm.Text = newClient.OrganizationName;
            }
            else if (newClient.ClientType == "Individual")
            {
                rblLegalWorkType.Items.FindByValue("Employee").Selected = true;
                accLegalWork.SelectedIndex = 1;
                advLegalWorkEmployeeID.SelectedEmployeeCode = newClient.HHLegalWorkID.ToString().PadLeft(4, '0');
                tbLegalWorkEmployeeFirstName.Text = newClient.ClientFirstName;
                tbLegalWorkEmployeeMiddleName.Text = newClient.ClientMiddleName;
                tbLegalWorkEmployeeLastName.Text = newClient.ClientLastName;
                upnlNames.Update();
            }
        }

        //protected override void OnPreRenderComplete(EventArgs e)
        //{
        //    base.OnPreRenderComplete(e);
            
        //}

        public override void Validate()
        {
            newClient = new NewClientInformation(ThisMemo.ScrMemID);
            if (rblLegalWorkType.SelectedValue == "Firm")
            {
                newClient.ClientType = "Organization";
                newClient.OrganizationName = tbLegalWorkFirm.Text;
            }
            else if (rblLegalWorkType.SelectedValue == "Employee")
            {
                HiddenField hidEmployeeCode = (HiddenField)advLegalWorkEmployeeID.FindControl("hidEmployeeCode");
                int employeeCode;
                bool success = Int32.TryParse(hidEmployeeCode.Value, out employeeCode);
                if(employeeCode == null)
                {
                    ErrorSummary.DisplayMessage = "There was a problem parsing the employee code";
                    ErrorSummary.DisplayType=Adversary.Web.Controls.MessageDisplayType.DisplayWarning;
                    return;
                }
                newClient.ClientType = "Individual";
                newClient.ClientFirstName = tbLegalWorkEmployeeFirstName.Text;
                newClient.ClientMiddleName = tbLegalWorkEmployeeMiddleName.Text;
                newClient.ClientLastName = tbLegalWorkEmployeeLastName.Text;
                newClient.HHLegalWorkID = employeeCode;
            }
            newClient.Save();
            
        }

        public override void Dispose()
        {
            if (newClient != null)
            {
                newClient.Dispose();
                newClient = null;
            }
            base.Dispose();
        }
    }
}