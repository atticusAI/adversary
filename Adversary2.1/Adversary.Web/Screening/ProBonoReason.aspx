﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Screening/Screening.Master" AutoEventWireup="true"
    CodeBehind="ProBonoReason.aspx.cs" Inherits="Adversary.Web.Screening.ProBonoReason"
    Theme="HH" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            formValidation = hh.validateWithjQuery("#form1");
            $("span.required input").addClass("required");
            $("span.required input").attr("title", "* This question must be answered.");
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMainContent" runat="server">
    <table width="750" cellpadding="2" cellspacing="1" border="0">
        <tr>
            <td valign="top">Please state why you feel that this memo is Pro Bono?<br />
                <asp:TextBox ID="tbProBonoReason" runat="server" TextMode="MultiLine" Columns="50"
                    Rows="16" CssClass="required" title="* A Pro Bono reason is required." />
            </td>
        </tr>
        <tr>
            <td>
                <br />
            </td>
        </tr>
        <tr>
            <td>Are the proposed legal services to an individual of limited financial means or to a charitable, religious, civic,
                community, governmental or educational organization group in a matter that is designed primarily to address the 
                needs of persons of limited means?<br />
                <asp:RadioButton ID="rbYes" runat="server" Text="Yes" GroupName="grpLimitedMeans" CssClass="required" />
                <asp:RadioButton ID="rbNo" runat="server" Text="No" GroupName="grpLimitedMeans" CssClass="required" />
            </td>
        </tr>
    </table>
    <div class="errdiv"></div>
</asp:Content>
