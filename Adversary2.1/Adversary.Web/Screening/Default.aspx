﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Screening/Screening.Master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="Adversary.Web.Screening.Default" Theme="HH" %>

<%@ Register Src="../Controls/ScreeningMemoList.ascx" TagName="ScreeningMemoList"
    TagPrefix="uc1" %>
<asp:Content ID="cntHead" ContentPlaceHolderID ="head" runat="server">
    <script type="text/javascript" language="javascript">
        function toggleQueue(cls, btn) {
            $(cls).toggle();
            $(btn).toggleClass("hide");
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="cntMain" ContentPlaceHolderID="cphMainContent" runat="server">
    <div class="sidebar">
        <div class="menu">
            <h4>
                Please choose an option below:</h4>
            <dl>
                <dt>
                    <asp:LinkButton ID="lnkNewType1" runat="server" Text="Start a New Matter for a New Client"
                        OnClick="lnkNew_Click" CommandArgument="1" /></dt>
                <dt>
                    <asp:LinkButton ID="lnkNewType2" runat="server" Text="Start a New Matter for an Existing Client"
                        OnClick="lnkNew_Click" CommandArgument="2" /></dt>
                <dt>
                    <asp:LinkButton ID="lnkNewType3" runat="server" Text="Start a Pro Bono Screening Memo"
                        OnClick="lnkNew_Click" CommandArgument="3" /></dt>
                <dt>
                    <asp:LinkButton ID="lnkNewType5" runat="server" Text="Start Legal Work for H&H or an Employee Screening Memo"
                        OnClick="lnkNew_Click" CommandArgument="5" /></dt>
                <dt>
                    <asp:LinkButton ID="lnkNewType6" runat="server" Text="Start a Rejected Matter Screening Memo"
                        OnClick="lnkNew_Click" CommandArgument="6" /></dt>
                <dt>
                    <asp:LinkButton ID="lnkSearch" runat="server" Text="Search for an existing Screening Memo"
                        PostBackUrl="~/Screening/Search.aspx" /></dt>
            </dl>
        </div>
    </div>
    <div class="contentbar">
        <a runat="server" class="queue memo hide" target="aboutPage" href="#aboutPage" onclick="return toggleQueue('.queuewrap.memos', '.queue.memo');">
            Show/Hide my list of previously created Screening Memos</a>
        <asp:Panel ID="pnlMyMemos" runat="server" Visible="true" CssClass="queuewrap memos">
            <uc1:ScreeningMemoList ID="smlMyMemos" runat="server" SelectMethod="GetMyScreeningMemos"
                TypeName="Adversary.DataLayer.ScreeningDefault" SortParameterName="sortExpression"
                AllowSorting="true" AllowPaging="true" OnPreRender="smlMyMemos_PreRender">
                <SelectParameters>
                    <asp:Parameter Name="personnelID" Type="String" DefaultValue="-1" />
                </SelectParameters>
            </uc1:ScreeningMemoList>
        </asp:Panel>
    </div>
</asp:Content>
