﻿<%@ Page Theme="HH" Title="" Language="C#" MasterPageFile="~/Screening/Screening.Master" AutoEventWireup="true" CodeBehind="EngagementLetter.aspx.cs" Inherits="Adversary.Web.Screening.EngagementLetter" %>
<%@ Register TagPrefix="ajax" Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" %>
<%@ Register TagPrefix="adv" TagName="fileUpload" Src="~/Controls/FileUpload.ascx" %>
<%@ Register TagPrefix="adv" TagName="files" Src="~/Controls/ExistingFiles.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/AccordionPane.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMainContent" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" width="900">
        <tr>
            <td>
                Will an Engagement Letter be Sent?<br />
                <asp:RadioButtonList runat="server" ID="rblEngagementLetter" RepeatDirection="Horizontal">
                    <asp:ListItem Text="Yes" Value="yes" />
                    <asp:ListItem Text="No" Value="no" />
                </asp:RadioButtonList><br />
            </td>
        </tr>
        <tr>
            <td>
                <ajax:Accordion ID="accEngagementLetter" runat="server" Width="100%" FadeTransitions="true" FramesPerSecond="60" Height="199"
                RequireOpenedPane="true" TransitionDuration="200" SelectedIndex="-1" AutoSize="None">
                    <Panes>
                        <ajax:AccordionPane ID="apUploadEngagementLetter" runat="server">
                            <Content>
                                <asp:UpdatePanel ID="upnlUploadEngagementLetter" runat="server" ChildrenAsTriggers="true" UpdateMode="Always">
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="fileUpload" EventName="" />
                                    </Triggers>
                                    <ContentTemplate>
                                        <%--<asp:GridView ID="gvEngagementLetterFiles" runat="server"  
                                        AutoGenerateColumns="false" ShowHeader="false"
                                        CellPadding="3" CellSpacing="2" BorderStyle="None">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnDeleteFile" runat="server" 
                                                            CommandName="DeleteFile" CommandArgument='<%#Eval("AttachmentID")%>'
                                                            Text="Delete" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Description" />
                                                <asp:BoundField DataField="FileName" />
                                                <asp:BoundField DataField="AttachmentID" />
                                            </Columns>
                                        </asp:GridView>--%><adv:files ID="advFilesEngagementLetter" runat="server" />
                                        <b>Please Upload an Engagement Letter</b><br />
                                        <asp:Label ID="lblMessage" runat="server" Visible="false" />
                                        <adv:fileUpload id="fileUpload" runat="server" FileUploadMode="EngagementLetter" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </Content>
                        </ajax:AccordionPane>
                        <ajax:AccordionPane ID="apNoEngagementLetter" runat="server">
                            <Content>
                                Reason an Engagement Letter will not be sent:<br />
                                <asp:TextBox runat="server" ID="tbNoEngagementLetter" TextMode="MultiLine" Rows="5" Columns="50" />
                            </Content>
                        </ajax:AccordionPane>
                        <ajax:AccordionPane runat="server" ID="ap1"><Content><!--blank--></Content></ajax:AccordionPane>
                    </Panes>
                </ajax:Accordion>
   
            </td>
        </tr>
        <tr>
            <td>
                <hr class="textDivider" />
            </td>
        </tr>
        <tr>
            <td>
                <b>REMINDER:</b> Whenever an engagement letter is written, the attorney should consider the wisdom of including a clause giving Holland & Hart 
                the reserved right to be adverse in future, unrelated matters. Such clauses should be included only if the attorney can identify the types of 
                future, unrelated matters in which Holland & Hart is likely to be adverse to the client.
            </td>
        </tr>
        <tr>
            <td>
                <hr class="textDivider" />
            </td>
        </tr>
        <tr>
            <td>
                Does this client or potential client require outside counsel to follow written policies or procedures related to billing or other matters?
            </td>
        </tr>
        <tr>
            <td>
                <asp:RadioButtonList runat="server" ID="rblWrittenPolicies" CausesValidation="false" RepeatDirection="Horizontal">
                    <asp:ListItem Text="Yes" Value="yes" />
                    <asp:ListItem Text="No" Value="no" />
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td>
                <ajax:Accordion ID="accWrittenPolicies" runat="server" Width="100%" FadeTransitions="true" FramesPerSecond="60" Height="199"
                RequireOpenedPane="true" TransitionDuration="200" SelectedIndex="-1" AutoSize="None">
                    <Panes>
                        <ajax:AccordionPane ID="apWrittenPoliciesYes" runat="server">
                            <Content>
                                <ul type="disc">
                                    <li>Attach a copy of the written policy to this screening memo.</li>
                                    <li>Do not send an engangement letter until these policies have been reviewed and approved by the Financial Partner and the Firm Screener.</li>
                                </ul>
                                <%--<asp:GridView ID="gvWrittenPolicies" runat="server" 
                                    AutoGenerateColumns="false" ShowHeader="false"
                                    CellPadding="3" CellSpacing="2" BorderStyle="None">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnDeleteFile" runat="server" 
                                                    CommandName="DeleteFile" CommandArgument='<%#Eval("AttachmentID")%>'
                                                    Text="Delete" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Description" />
                                        <asp:BoundField DataField="FileName" />
                                        <asp:BoundField DataField="AttachmentID" />
                                    </Columns>
                                </asp:GridView>--%><adv:files ID="advFilesWrittenPolicies" runat="server" />
                                <br />
                                <adv:fileUpload ID="fileUploadWrittenPolicies" runat="server" FileUploadMode="WrittenPolicies" />
                            </Content>
                           
                        </ajax:AccordionPane>
                        <ajax:AccordionPane ID="apWrittenPoliciesNo" runat="server">
                            <Content><!--blank--></Content>
                        </ajax:AccordionPane>
                        <ajax:AccordionPane runat="server" ID="ap2"></ajax:AccordionPane>
                    </Panes>
                </ajax:Accordion>
            </td>
        </tr>
    </table>
</asp:Content>
