﻿using Adversary.DataLayer.Screening;
using Adversary.Web.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Adversary.Web.Screening
{
    public partial class ConflictIdentification : ScreeningBasePage
    {
        /// <summary>
        /// bind the data list to the conflicts for this memo
        /// </summary>
        protected void lstConflicts_Init(object sender, EventArgs e)
        {
            this.lstConflicts.DataSource = this.Conflicts;
            this.lstConflicts.DataBind();
        }

        /// <summary>
        /// storage for memo conflicts
        /// </summary>
        private MemoConflictCollection _conflicts = null;

        /// <summary>
        /// lazy loader for memo conflicts
        /// </summary>
        private MemoConflictCollection Conflicts
        {
            get
            {
                if (this._conflicts == null)
                {
                    this._conflicts = new MemoConflictCollection(ThisMemo.ScrMemID, ThisMemo.ScrMemType ?? -1);
                }
                return this._conflicts;
            }
        }


        private bool _errSet = false;

        /// <summary>
        /// validate input
        /// </summary>
        private bool IsInputValid()
        {
            bool isValid = false;

            foreach (DataListItem item in this.lstConflicts.Items)
            {
                if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                {
                    CheckBox chkAnswer = (CheckBox)item.FindControl("chkAnswer");
                    if (chkAnswer != null && chkAnswer.Checked)
                    {
                        isValid = true;
                        break;
                    }
                }
            }

            if (!isValid)
            {
                foreach (DataListItem item in this.lstConflicts.Items)
                {
                    if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                    {
                        CheckBox chkAnswer = (CheckBox)item.FindControl("chkAnswer");
                        if (chkAnswer != null)
                        {
                            if (!this._errSet)
                            {
                                chkAnswer.HH_SetError(this.pnlErrs, "* You have not checked any boxes. If the client or adverse party does not fit any of the criteria listed please select 'None of the above'.");
                                this._errSet = true;
                            }
                            else
                            {
                                chkAnswer.HH_SetError(this.pnlErrs);
                            }
                        }
                    }
                }
            }

            return isValid;
        }

        /// <summary>
        /// add the exclusive javascript callback to any checkbox that is exclusive
        /// also add the exclusive class so that on $(document).ready() we can find out if any exclusive checkbox is already checked
        /// </summary>
        protected void lstConflicts_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                CheckBox chkAnswer = (CheckBox)e.Item.FindControl("chkAnswer");
                if (chkAnswer != null)
                {
                    if (((MemoConflict)e.Item.DataItem).Name == "None")
                    {
                        chkAnswer.Attributes.Add("onclick", "chkExclusive(this, \"" + chkAnswer.ClientID + "\")");
                        chkAnswer.CssClass += " exclusive";
                    }
                }
            }
        }

        /// <summary>
        /// page validator
        /// </summary>
        protected void valPage_ServerValidate(object sender, ServerValidateEventArgs e)
        {
            e.IsValid = this.IsInputValid();
            if (e.IsValid)
            {
                this.Save();
            }
        }

        private bool _saved = false;

        /// <summary>
        /// save the memo conflicts back to the database
        /// </summary>
        private void Save()
        {
            if (!this._saved)
            {
                this._saved = true;
                foreach (DataListItem item in this.lstConflicts.Items)
                {
                    if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                    {
                        CheckBox chkAnswer = (CheckBox)item.FindControl("chkAnswer");
                        HiddenField hidConflict = (HiddenField)item.FindControl("hidConflict");
                        if (chkAnswer != null && hidConflict != null)
                        {
                            MemoConflict con = (from MemoConflict c in this.Conflicts
                                                where c.ConflictID == Convert.ToInt32(hidConflict.Value)
                                                select c).FirstOrDefault();
                            if (con != null) con.Update(chkAnswer.Checked);
                        }
                    }
                }
            }
        }
    }
}
