﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.DataLayer;
using Adversary.Utils;

namespace Adversary.Web.Screening
{
    public partial class ReferringParty : ScreeningBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            rblPartyType.HH_AddAccordionPaneJavascript(accParty.ClientID);

            if (!IsPostBack)
            {
                Adversary.DataLayer.ReferringParty rp = new DataLayer.ReferringParty(ThisMemo.ScrMemID);
                rp.GetParty();
                tbAddress1.Text = rp.Address1;
                tbAddress2.Text = rp.Address2;
                tbAddress3.Text = rp.Address3;
                tbCity.Text = rp.City;
                tbState.Text = rp.State;
                tbZIP.Text = rp.ZIP;
                tbCountry.Text = rp.Country;
                tbEmail.Text = rp.Email;
                tbFax.Text = rp.Fax;
                tbFirstName.Text = rp.PartyFirstName;
                tbLastName.Text = rp.PartyLastName;
                tbMiddleName.Text = rp.PartyMiddleName;
                tbOrganization.Text = rp.PartyOrganizationName;
                tbOriginationNotes.Text = rp.HowClientCameToHH;
                tbPhone.Text = rp.Phone;
                hidPartyID.Value = rp.PartyID.ToString();
                if(rp.PartyType != null)
                {
                    rblPartyType.SelectedIndex = -1;
                    if (rp.PartyType.Equals("Individual"))
                    {
                        rblPartyType.Items.FindByValue("Individual").Selected = true;
                        accParty.SelectedIndex = 0;
                    }
                    else if (rp.PartyType.Equals("Organization"))
                    {
                        rblPartyType.Items.FindByValue("Organization").Selected = true;
                        accParty.SelectedIndex = 1;
                    }
                }
                rp.Dispose();
                
            }

        }

        public override void Validate()
        {
            base.Validate();
            int iOut = 0;
            Adversary.DataLayer.ReferringParty refParty =
                new DataLayer.ReferringParty(ThisMemo.ScrMemID);

            refParty.Address1 = tbAddress1.Text;
            refParty.Address2 = tbAddress2.Text;
            refParty.Address3 = tbAddress3.Text;
            refParty.City = tbCity.Text;
            refParty.Country = tbCountry.Text;
            refParty.Email = tbEmail.Text;
            refParty.Fax = tbFax.Text;
            refParty.HowClientCameToHH = tbOriginationNotes.Text;
            refParty.PartyFirstName = tbFirstName.Text;
            refParty.PartyMiddleName = tbMiddleName.Text;
            refParty.PartyLastName = tbLastName.Text;
            refParty.PartyOrganizationName = tbOrganization.Text;
            refParty.PartyType = rblPartyType.SelectedValue;
            refParty.Phone = tbPhone.Text;
            refParty.State = tbState.Text;
            refParty.ZIP = tbZIP.Text;
            if(Int32.TryParse(hidPartyID.Value, out iOut))
            {
                refParty.PartyID = iOut;
            }
            refParty.Save();
            refParty.Dispose();
        }
    }
}