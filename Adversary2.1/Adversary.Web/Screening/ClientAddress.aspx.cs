﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.DataLayer;
using Adversary.Utils;
using System.Web.UI.HtmlControls;
using Adversary.Web.Extensions;
using Adversary.DataLayer.Screening;

namespace Adversary.Web.Screening
{
    public partial class ClientAddress2 : ScreeningBasePage
    {
        Adversary.DataLayer.AdversaryData advData;

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            advData = new AdversaryData();

            List<Adversary.DataLayer.ClientMatterLookupService.ClientAddress> __Addresses =
                        new List<DataLayer.ClientMatterLookupService.ClientAddress>();

            rptClientAddresses.ItemDataBound += new RepeaterItemEventHandler(rptClientAddresses_ItemDataBound);

            Adversary.DataLayer.Address _address = ThisMemo.Addresses.FirstOrDefault();

            lbAddressList.Text = "View addresses for ";

            //if this is a new client, then the link for addresses should not display
            if (ThisMemo.NewClients.Count <= 0 || String.IsNullOrEmpty(ThisMemo.ClientNumber))
            {
                lbAddressList.Visible = false;
                pnlClientAddressForm.Visible = false;
            }
            else
            {
                lbAddressList.Text += advData.SearchClients(ThisMemo.ClientNumber).FirstOrDefault().ClientName;
            }

            if (!IsPostBack)
            {
                if (_address != null)
                {
                    if (!String.IsNullOrEmpty(_address.AddressID.ToString()))
                    {
                        hidAddressID.Value = _address.AddressID.ToString();
                        tbAddress1.Text = _address.Address1;
                        tbAddress2.Text = _address.Address2;
                        tbAddress3.Text = _address.Address3;
                        tbAltPhone.Text = _address.AltPhone;
                        tbCity.Text = _address.City;
                        this.SetCountry(_address.Country);
                        tbEmail.Text = _address.email;
                        tbNoEmail.Text = _address.NoEmailReason;
                        rbYesEmail.Checked = (!string.IsNullOrEmpty(_address.email));
                        rbNoEmail.Checked = (!rbYesEmail.Checked && !string.IsNullOrEmpty(_address.NoEmailReason));
                        tbFax.Text = _address.Fax;
                        tbPhone.Text = _address.Phone;
                        this.SetState(_address.State);
                        tbZIP.Text = _address.Zip;
                    }
                    else
                    {
                        hidAddressID.Value = "0";
                    }
                }
                else
                {
                    hidAddressID.Value = "0";
                }
            }

            if (!String.IsNullOrEmpty(ThisMemo.ClientNumber))
            {
                __Addresses = advData.GetClientAddresses(ThisMemo.ClientNumber);
                rptClientAddresses.DataSource = __Addresses;
                rptClientAddresses.DataBind();
            }
            else
            {

            }
        }

        private bool SetState(string state)
        {
            bool set = false;

            ListItem itm = (from ListItem i in this.ddlState.Items
                            where String.Equals(i.Value, state, StringComparison.CurrentCultureIgnoreCase)
                            || String.Equals(i.Text, state, StringComparison.CurrentCultureIgnoreCase)
                            select i).FirstOrDefault();
            if (itm != null)
            {
                this.ddlState.ClearSelection();
                itm.Selected = true;
                set = true;
            }

            return set;
        }

        /// <summary>
        /// lookup the country based on the name and set the ddl if it's found
        /// </summary>
        private bool SetCountry(string country)
        {
            bool set = false;

            string code = this.Countries.LookupCode(country);

            if (code != null)
            {
                ListItem itm = (from ListItem i in this.ddlCountry.Items
                                where String.Equals(i.Value, code, StringComparison.CurrentCultureIgnoreCase)
                                select i).FirstOrDefault();
                if (itm != null)
                {
                    this.ddlCountry.ClearSelection();
                    itm.Selected = true;
                    set = true;
                }
            }

            return set;
        }

        void rptClientAddresses_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Adversary.DataLayer.ClientMatterLookupService.ClientAddress __Address =
                    (Adversary.DataLayer.ClientMatterLookupService.ClientAddress)e.Item.DataItem;

                Panel __pnlClientAddressDisplay = (Panel)e.Item.FindControl("pnlClientAddressDisplay");
                Panel __pnlClientAddressPhone = (Panel)e.Item.FindControl("pnlClientAddressPhone");
                Panel __pnlClientAddressSelectButton = (Panel)e.Item.FindControl("pnlClientAddressSelectButton");
                HtmlGenericControl hrClientAddressPhoneSeparator = (HtmlGenericControl)e.Item.FindControl("hrClientAddressPhoneSeparator");

                if (!String.IsNullOrEmpty(__Address.ADDRESS1))
                    __pnlClientAddressDisplay.Controls.Add(new LiteralControl(__Address.ADDRESS1 + "<br/>"));
                if (!String.IsNullOrEmpty(__Address.ADDRESS2))
                    __pnlClientAddressDisplay.Controls.Add(new LiteralControl(__Address.ADDRESS2 + "<br/>"));
                if (!String.IsNullOrEmpty(__Address.ADDRESS3))
                    __pnlClientAddressDisplay.Controls.Add(new LiteralControl(__Address.ADDRESS3 + "<br/>"));
                bool hasCity = false;
                if (!String.IsNullOrEmpty(__Address.CITY))
                {
                    hasCity = true;
                    __pnlClientAddressDisplay.Controls.Add(new LiteralControl(__Address.CITY + " "));
                }
                bool hasState = false;
                if (!String.IsNullOrEmpty(__Address.STATE_CODE))
                {
                    hasState = true;
                    if (hasCity)
                    {
                        __pnlClientAddressDisplay.Controls.Add(new LiteralControl(", "));
                    }
                    __pnlClientAddressDisplay.Controls.Add(new LiteralControl(__Address.STATE_CODE + " "));
                }
                if (!String.IsNullOrEmpty(__Address.POST_CODE))
                {
                    //if (hasCity || hasState)
                    //    __pnlClientAddressDisplay.Controls.Add(new LiteralControl(__Address.POST_CODE));
                    __pnlClientAddressDisplay.Controls.Add(new LiteralControl(__Address.POST_CODE));
                }
                if (!String.IsNullOrEmpty(__Address.COUNTRY_CODE))
                {
                    __pnlClientAddressDisplay.Controls.Add(new LiteralControl("<br/>" + __Address.COUNTRY_CODE));
                }

                if (!String.IsNullOrEmpty(__Address.PHONE_NUMBER))
                    __pnlClientAddressPhone.Controls.Add(new LiteralControl("Phone: " + __Address.PHONE_NUMBER));
                if (!String.IsNullOrEmpty(__Address.FAX_NUMBER))
                    __pnlClientAddressPhone.Controls.Add(new LiteralControl("<br/>Fax: " + __Address.FAX_NUMBER));

                //hrClientAddressPhoneSeparator.Visible = (__pnlClientAddressPhone.Controls.Count > 0);

                //quit the databinding routine if nothing was found in any field and no literal controls were added to
                //the panels
                if (__pnlClientAddressDisplay.Controls.Count <= 0 && __pnlClientAddressPhone.Controls.Count <= 0)
                {
                    __pnlClientAddressDisplay.Controls.Add(new LiteralControl("No addresses were found."));
                }

                LinkButton lb = new LinkButton();
                lb.ID = "lbClientAddressChoose___" + e.Item.ItemIndex.ToString();
                lb.SetDataItem(__Address);
                lb.Text = "Choose this Address";
                lb.Click += new EventHandler(lb_Click);
                lb.CausesValidation = false;
                __pnlClientAddressSelectButton.Controls.Add(lb);
            }
        }

        void lb_Click(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            this.ddlCountry.ClearSelection();
            Adversary.DataLayer.ClientMatterLookupService.ClientAddress _address =
                (Adversary.DataLayer.ClientMatterLookupService.ClientAddress)lb.GetDataItem();

            tbAddress1.Text = _address.ADDRESS1;
            tbAddress2.Text = _address.ADDRESS2;
            if (!this.SetCountry(_address.ADDRESS3))
            {
                tbAddress3.Text = _address.ADDRESS3;
                this.SetCountry(_address.COUNTRY_CODE);
            }
            tbAltPhone.Text = string.Empty;
            tbCity.Text = _address.CITY;
            tbEmail.Text = string.Empty;
            tbFax.Text = _address.FAX_NUMBER;
            tbPhone.Text = _address.PHONE_NUMBER;
            this.SetState(_address.STATE_CODE);
            tbZIP.Text = _address.POST_CODE;
        }
              
        public override void Validate()
        {
            base.Validate();
            if (this.IsInputValid())
            {
                Adversary.DataLayer.ClientAddress clientAddress = new DataLayer.ClientAddress(ThisMemo.ScrMemID);
                clientAddress.AddressID = Convert.ToInt32(hidAddressID.Value);
                clientAddress.Address1 = tbAddress1.Text;
                clientAddress.Address2 = tbAddress2.Text;
                clientAddress.Address3 = tbAddress3.Text;
                clientAddress.AltPhone = tbAltPhone.Text;
                clientAddress.City = tbCity.Text;
                clientAddress.Country = ddlCountry.SelectedItem.Text;

                if (rbYesEmail.Checked)
                {
                    clientAddress.Email = tbEmail.Text;
                    clientAddress.NoEmailReason = null;
                }
                else
                {
                    clientAddress.Email = null;
                    clientAddress.NoEmailReason = tbNoEmail.Text;
                }

                clientAddress.Email = tbEmail.Text;
                clientAddress.Fax = tbFax.Text;
                clientAddress.Phone = tbPhone.Text;
                clientAddress.StateProvince = ddlState.SelectedValue;
                clientAddress.ZIPCode = tbZIP.Text;
                clientAddress.Save();
            }
        }

        public override void Dispose()
        {
            if (advData != null)
            {
                advData.Dispose();
                advData = null;
            }
            base.Dispose();
        }

        protected void valPage_ServerValidate(object sender, ServerValidateEventArgs e)
        {
            e.IsValid = this.IsInputValid();
        }

        /// <summary>
        /// validate the input
        /// </summary>
        private bool IsInputValid()
        {
            bool isValid = true;

            this.ClearErrors();

            if (string.IsNullOrWhiteSpace(this.tbAddress1.Text) &&
                string.IsNullOrWhiteSpace(this.tbAddress2.Text) &&
                string.IsNullOrWhiteSpace(this.tbAddress3.Text))
            {
                this.tbAddress1.HH_SetError(this.pnlErrs);
                isValid = false;
            }
            
            if (string.IsNullOrWhiteSpace(this.tbPhone.Text))
            {
                this.tbPhone.HH_SetError(this.pnlErrs);
                isValid = false;
            }

            string country = this.ddlCountry.SelectedValue;
            if (country == "US")
            {
                if (string.IsNullOrWhiteSpace(this.tbCity.Text))
                {
                    this.tbCity.HH_SetError(this.pnlErrs);
                    isValid = false;
                }

                if (string.IsNullOrWhiteSpace(this.ddlState.SelectedValue))
                {
                    this.ddlState.HH_SetError(this.pnlErrs);
                    isValid = false;
                }
            }
            else if (string.IsNullOrWhiteSpace(country))
            {
                this.ddlCountry.HH_SetError(this.pnlErrs);
                isValid = false;
            }

            if (!this.rbYesEmail.Checked && !this.rbNoEmail.Checked)
            {
                this.rbYesEmail.HH_SetError(this.pnlErrs, "* You must either provide an email address or a reason why no email address is available.");
                this.rbNoEmail.HH_SetError(this.pnlErrs, "");
                isValid = false;
            }
            else
            {
                if (this.rbYesEmail.Checked && string.IsNullOrWhiteSpace(this.tbEmail.Text))
                {
                    this.tbEmail.HH_SetError(this.pnlErrs);
                    isValid = false;
                }
                else if (this.rbNoEmail.Checked && string.IsNullOrWhiteSpace(this.tbNoEmail.Text))
                {
                    this.tbNoEmail.HH_SetError(this.pnlErrs);
                    isValid = false;
                }
            }

            return isValid;
        }

        private void ClearErrors()
        {
            this.tbAddress1.HH_ClearError();
            this.tbCity.HH_ClearError();
            this.ddlState.HH_ClearError();
            this.ddlCountry.HH_ClearError();
            this.tbPhone.HH_ClearError();
            this.pnlErrs.HH_ClearError();
        }

        private CountryCollection _countries = null;

        private CountryCollection Countries
        {
            get
            {
                if (this._countries == null)
                {
                    this._countries = new CountryCollection();
                }
                return this._countries;
            }
        }

        protected void ddlCountry_Init(object sender, EventArgs e)
        {
            this.ddlCountry.DataSource = this.Countries.Where(c => c.Primary == true);
            this.ddlCountry.DataValueField = "Code";
            this.ddlCountry.DataTextField = "Name";
            this.ddlCountry.DataBind();
        }

        protected void ddlState_Init(object sender, EventArgs e)
        {
            this.ddlState.DataSource = new StateCollection();
            this.ddlState.DataValueField = "Code";
            this.ddlState.DataTextField = "Name";
            this.ddlState.DataBind();
        }
    }
}