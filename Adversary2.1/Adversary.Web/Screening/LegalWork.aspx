﻿<%@ Page Title="" Theme="HH" Language="C#" MasterPageFile="Screening.Master" AutoEventWireup="true" CodeBehind="LegalWork.aspx.cs" Inherits="Adversary.Web.LegalWork" %>
<%@ Register TagPrefix="ajax" Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" %>
<%@ Register TagPrefix="adv" TagName="PersonAutoComplete" Src="~/Controls/PersonAutoComplete.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/AccordionPane.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMainContent" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" width="900">
        <tr>
            <td>
                Please choose the type of legal work:<br />
                <asp:RadioButtonList runat="server" ID="rblLegalWorkType" RepeatDirection="Vertical">
                    <asp:ListItem Text="Work for H&H As A Firm" Value="Firm" />
                    <asp:ListItem Text="Work for an H&H Employee or Partner" Value="Employee" />
                </asp:RadioButtonList>

            </td>
        </tr>
        <tr>
            <td>
                <ajax:Accordion ID="accLegalWork" runat="server" Width="100%" RequireOpenedPane="false" FadeTransitions="true" FramesPerSecond="60" Height="199"
                    TransitionDuration="200" SelectedIndex="-1" AutoSize="None">
                    <Panes>
                        <ajax:AccordionPane ID="apLegalWorkFirm" runat="server">
                            <Content>
                                Verify and complete the name of the organization that work will be performed for
                                <br /><br />
                                <asp:TextBox ID="tbLegalWorkFirm" runat="server" TextMode="MultiLine" Columns="50" Rows="2" />
                            </Content>
                        </ajax:AccordionPane>
                        <ajax:AccordionPane ID="apLegalWorkEmployee" runat="server">
                            <Content>
                            <asp:UpdatePanel id="upnlNames" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                                    <ContentTemplate>
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                           Employee Number or Name: 
                                        </td>
                                        <td>
                                            <adv:PersonAutoComplete ID="advLegalWorkEmployeeID" runat="server" ServiceMethod="TimekeeperService" PopulatedEventCausesValidation="false" />
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                            First Name:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbLegalWorkEmployeeFirstName" runat="server" />
                                            <asp:HiddenField ID="hidLegalWorkEmployeeID" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Middle Name:</td>
                                        <td>
                                            <asp:TextBox ID="tbLegalWorkEmployeeMiddleName" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Last Name:</td>
                                        <td>
                                            <asp:TextBox ID="tbLegalWorkEmployeeLastName" runat="server" />
                                        </td>
                                    </tr>
                                    
                                </table>
                                </ContentTemplate>
                                    </asp:UpdatePanel>
                            </Content>
                        </ajax:AccordionPane>
                    </Panes>    
                </ajax:Accordion>
            
            </td>
        </tr>
    </table>
</asp:Content>
