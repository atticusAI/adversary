﻿using Adversary.DataLayer.HRDataService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Adversary.Web.Screening
{
    public partial class Office : ScreeningBasePage
    {
        Adversary.DataLayer.ScreeningOffice ofc;
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            FirmOffice of = AdversaryDataLayer.GetAttorneyOffice(ThisMemo.RespAttID);
            if (of != null)
            {
                advOfficeDropDown.SelectedOffice = of.LocationCode;
            }
            else
            {
                ofc = new DataLayer.ScreeningOffice(base.ScreeningMemoID);
                ofc.Get();
                if (!string.IsNullOrWhiteSpace(ofc.OrigOffice))
                {
                    advOfficeDropDown.SelectedOffice = ofc.OrigOffice;
                }
            }
        }

        public override void Validate()
        {
            base.Validate();
            Adversary.DataLayer.ScreeningOffice ofc = new DataLayer.ScreeningOffice(base.ScreeningMemoID);
            ofc.OrigOffice = advOfficeDropDown.ddlOffice.SelectedValue;
            ofc.Save();
        }
    }
}   