﻿<%@ Page Title="" Theme="HH" Language="C#" MasterPageFile="~/Screening/Screening.Master" AutoEventWireup="true" CodeBehind="docketing.aspx.cs" Inherits="Adversary.Web.Screening.docketing" %>
<%@ Register TagPrefix="ajax" Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" %>
<%@ Register TagPrefix="uc" TagName="EmployeeControl" Src="~/Controls/PersonAutoComplete.ascx" %>
<%@ Register TagPrefix="uc" TagName="DocketingStaff" Src="~/Controls/ListContainerControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../App_Themes/HH/HH.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphMainContent" runat="server">
    <asp:UpdatePanel ID="upnlBoxes" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
    <ContentTemplate>
    <table border="0" cellpadding="3" cellspacing="2">
        <tr>
            <td class="lbl">
                Docketing Attorney:
            </td>
            <td>
                <uc:EmployeeControl ID="ucDocketingAtty" runat="server" ServiceMethod="TimekeeperService" RaiseEventOnItemSelected="true"
                    PopulatedEventCausesValidation="false" />
            </td>
        </tr>
        <tr>
            <td class="lbl">
                Docketing Staff:
            </td>
            <td>
                <uc:EmployeeControl ID="ucDocketingStaff" runat="server" ServiceMethod="TimekeeperService" RaiseEventOnItemSelected="true" 
                    PopulatedEventCausesValidation="false" />
            </td>
        </tr>
    </table>
    </ContentTemplate>
                </asp:UpdatePanel>
                
            Docketing Team Members:<br />       
                <uc:DocketingStaff ID="DocketingStaff" runat="server"
                    AutoGenerateColumns="false"
                    Text="Add Team Members"
                    PopupCssClass="pop"
                    BackgroundCssClass="pop-back"
                    AddY="25"
                    OnPreRender="DocketingStaff_OnPreRender"
                    OnRowDataBound="DocketingStaff_OnRowDataBound"
                    OnDeleteClick="DocketingStaff_OnDeleteClick"
                    OnRowCommand="DocketingStaff_OnRowCommand"
                    OnAddClick="DocketingStaff_OnAddClick">
                    <Columns>
                        <asp:BoundField HeaderText="Staff" />
                    </Columns>
                    <DeleteItemTemplate>
                        Are you sure you wish to delete this record?
                        <asp:HiddenField ID="hidDeleteDocketingStaff" runat="server"
                            Value='<%#Eval("DocketingTeamID")%>' />
                    </DeleteItemTemplate>
                    <InsertItemTemplate>
                        Enter Employee ID/Last Name:
                        <uc:EmployeeControl ID="advPersonDropDown" runat="server"
                            RaiseEventOnItemSelected="false"
                            PopulatedEventCausesValidation="false"
                            ServiceMethod="TimekeeperService" />
                    </InsertItemTemplate>
                </uc:DocketingStaff>
        
</asp:Content>
