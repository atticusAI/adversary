﻿<%@ Page Theme="HH" Title="" Language="C#" MasterPageFile="~/Screening/Screening.Master" AutoEventWireup="true" CodeBehind="Opposing.aspx.cs" Inherits="Adversary.Web.Screening.Opposing" %>
<%@ Register TagPrefix="ajax" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="adv" TagName="listcontainercontrol" Src="~/Controls/ListContainerControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphMainContent" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" width="800">
        <tr>
            <td>
                Is there opposing counsel on this matter?
            </td>
            <td>
                <asp:RadioButtonList ID="rblOpposingCounsel" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Text="Yes" />
                    <asp:ListItem Text="No" />
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <adv:listcontainercontrol
                    ID="advOpposingCounselControl"
                    runat="server"
                 AutoGenerateColumns="false"
                 AddY="25"
                 AllowPaging="false"
                 AllowSorting="false"
                 ChangeY="25"
                 DeleteY="25"
                 EmptyDataText="No opposing counsel specified"
                 PopupCssClass="pop"
                 BackgroundCssClass="pop-back"
                 OnAddClick="opposingCounsel_AddClick"
                 OnChangeClick="opposingCounsel_ChangeClick"
                 OnDeleteClick="opposingCounsel_DeleteClick"
                 
                 Text="Add Opposing Counsel"
                 OnPreRender="opposingCounsel_PreRender"
                 >
                    <Columns>
                        <asp:BoundField DataField="LawfirmName" HeaderText="Law Firm Name" ShowHeader="true" />
                        <asp:BoundField DataField="LawyerName" HeaderText="Lawyer Name" ShowHeader="true" />
                    </Columns>
                    <InsertItemTemplate>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    Law Firm Name
                                </td>
                                <td>
                                    <asp:TextBox ID="tbInsertLawFirmName" runat="server"
                                        TextMode="MultiLine" Rows="3" SkinID="HH_Textbox" />
                                </td>
                            </tr>
                            <tr>
                                <td>Lawyer Name</td>
                                <td>
                                    <asp:TextBox ID="tbInsertLawyerName" runat="server"
                                        SkinID="HH_Textbox" />
                                </td>
                            </tr>
                        </table>
                    </InsertItemTemplate>
                    <EditItemTemplate>
                        <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        Law Firm Name
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbEditLawFirmName" runat="server" Text='<%#Eval("LawfirmName") %>'
                                            TextMode="MultiLine" Rows="3" SkinID="HH_Textbox" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Lawyer Name</td>
                                    <td>
                                        <asp:TextBox ID="tbEditLawyerName" runat="server" Text='<%#Eval("LawyerName") %>'
                                            SkinID="HH_Textbox" />
                                    </td>
                                </tr>
                            </table>
                        <asp:HiddenField ID="hidEditOpposingID" runat="server" Value='<%#Eval("opposingID") %>' />
                    </EditItemTemplate>
                    <DeleteItemTemplate>
                        Are you sure you wish to delete this record?
                        <asp:HiddenField ID="hidDeleteOpposingID" runat="server" />
                    </DeleteItemTemplate>
                 </adv:listcontainercontrol>
            </td>
        </tr>
    </table>
</asp:Content>

