﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.Utils;
using Adversary.DataLayer;
using Adversary.Controls;

namespace Adversary.Web.Screening
{
    public partial class AdverseParties : ScreeningBasePage
    {
        public string SetAccordionPane;
        protected void Page_Load(object sender, EventArgs e)
        {
            rblAdversePartyModifyPartyType.HH_AddAccordionPaneJavascript(accAdversePartyModify.ClientID);
            partyrelAdversePartyModify.RelationshipDisplayMode = PartyRelationshipDropDown.PartyRelationshipMode.AdverseParties;
            gvAdversePartys.RowDeleting += new GridViewDeleteEventHandler(gvAdversePartys_RowDeleting);
            gvAdversePartys.RowDataBound += new GridViewRowEventHandler(gvAdversePartys_RowDataBound);
            if (!Page.IsPostBack)
            {
                BindData();
            }
        }

        void gvAdversePartys_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //set the party name
                Party pData = (Party)e.Row.DataItem;
                string __name = string.Empty;
                if (pData.PartyType.Equals("Individual"))
                {
                    __name = pData.PartyFName + " "
                        + pData.PartyMName + " "
                        + pData.PartyLName;
                }
                else if (pData.PartyType.Equals("Organization"))
                {
                    __name = pData.PartyOrganization;
                }
                e.Row.Cells[3].Text = __name;
            }
        }

        void gvAdversePartys_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            BindData();
        }
        public void gvPartyRelationships_RowCreated(object sender, GridViewRowEventArgs e)
        {

        }

        public void gvPartyRelationships_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string s = (string)e.Row.DataItem;
                TextBox t = (TextBox)e.Row.Cells[0].FindControl("tbName");
                t.Text = s;
            }
        }

        protected void btnAdversePartyModifySubmit_Click(object sender, EventArgs e)
        {
            Adversary.DataLayer.AdverseParty p = new DataLayer.AdverseParty(ThisMemo.ScrMemID);
            p.PartyID = Convert.ToInt32(hidAdversePartyModifyPartyID.Value);
            p.PartyType = rblAdversePartyModifyPartyType.SelectedValue;
            p.PartyFName = tbAdversePartyModifyFirstName.Text;
            p.PartyMName = tbAdversePartyModifyMiddleName.Text;
            p.PartyLName = tbAdversePartyModifyLastName.Text;
            p.PartyOrganization = tbAdversePartyModifyOrganization.Text;
            p.PartyRelationshipCode = partyrelAdversePartyModify.ddlPartyRelationship.SelectedValue;
            p.PartyRelationshipName = partyrelAdversePartyModify.ddlPartyRelationship.SelectedItem.Text;
            p.Save();
            this.mdlModifyAdverseParty.Hide();
            BindData();
            upnlAdversePartyGrid.Update();
        }

        protected void btnAdversePartyModifyCancel_Click(object sender, EventArgs e)
        {
            BindData();
            mdlModifyAdverseParty.Hide();
        }

        protected void gvAdversePartys_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            if (e.CommandName == "Delete")
            {
                Adversary.DataLayer.AdverseParty del_RelParty =
                    new AdverseParty(ThisMemo.ScrMemID);
                del_RelParty.PartyID = Convert.ToInt32(e.CommandArgument);
                del_RelParty.Delete();
                del_RelParty.Dispose();
            }
            else if (e.CommandName == "Modify")
            {
                string f = e.CommandArgument.ToString();
                Adversary.DataLayer.AdverseParty AdverseParty = new DataLayer.AdverseParty(ThisMemo.ScrMemID);
                Adversary.DataLayer.Party p = AdverseParty.GetParty(Convert.ToInt32(f));
                hidAdversePartyModifyPartyID.Value = p.PartyID.ToString();
                rblAdversePartyModifyPartyType.SelectedIndex = -1;
                if (p.PartyType == "Individual")
                {
                    rblAdversePartyModifyPartyType.Items.FindByValue("Individual").Selected = true;
                }
                else if (p.PartyType == "Organization")
                {
                    rblAdversePartyModifyPartyType.Items.FindByValue("Organization").Selected = true;
                }
                accAdversePartyModify.SelectedIndex = (p.PartyType.Equals("Individual") ? 0 : 1);
                tbAdversePartyModifyFirstName.Text = p.PartyFName;
                tbAdversePartyModifyLastName.Text = p.PartyLName;
                tbAdversePartyModifyMiddleName.Text = p.PartyMName;
                tbAdversePartyModifyOrganization.Text = p.PartyOrganization;
                partyrelAdversePartyModify.ddlPartyRelationship.SelectedIndex = -1;
                if (partyrelAdversePartyModify.ddlPartyRelationship.Items.FindByValue(p.PartyRelationshipCode) != null)
                {
                    partyrelAdversePartyModify.ddlPartyRelationship.Items.FindByValue(p.PartyRelationshipCode).Selected = true;
                }
                this.mdlModifyAdverseParty.Show();
                AdverseParty.Dispose();
                BindData();
            }
        }

        private void BindData()
        {
            Adversary.DataLayer.AdverseParty rParty = new AdverseParty(ThisMemo.ScrMemID);
            List<Party> _AdverseParties = rParty.GetPartyList();
            gvAdversePartys.DataSource = _AdverseParties;
            gvAdversePartys.DataBind();
            rParty.Dispose();
        }

        public override void Validate()
        {



        }

        public void adverseWizard_NextButtonClick(object sender, System.Web.UI.WebControls.WizardNavigationEventArgs e)
        {
            if (adverseWizard.ActiveStepIndex == 0)
            {
                SetTextboxes();
            }
        }
        public void adverseWizard_FinishButtonClick(object sender, System.Web.UI.WebControls.WizardNavigationEventArgs e)
        {
            GridViewRowCollection gvCol = gvPartyRelationships.Rows;
            Adversary.DataLayer.AdverseParty adverseClass = new DataLayer.AdverseParty(ThisMemo.ScrMemID);
            List<DataLayer.Party> newPartiesList = new List<DataLayer.Party>();

            foreach (GridViewRow row in gvCol)
            {
                Adversary.DataLayer.Party p = new DataLayer.Party();

                DropDownList ddlPartyType = (DropDownList)row.Cells[1].FindControl("ddlPartyType");
                PartyRelationshipDropDown partyddl = (PartyRelationshipDropDown)row.Cells[2].FindControl("partyrelPartyRelationshipDropDown");
                DropDownList ddlPartyRel = (DropDownList)partyddl.ddlPartyRelationship;
                TextBox tbName = (TextBox)row.Cells[0].FindControl("tbName");

                if (ddlPartyType.SelectedValue == "Individual")
                {
                    string[] name = Adversary.Utils.PartyNameSeparator.SeparateIndividualName(tbName.Text);
                    p.PartyFName = name[0];
                    p.PartyMName = name[1];
                    p.PartyLName = name[2];
                }
                else
                {
                    p.PartyOrganization = tbName.Text;
                }
                p.ScrMemID = ThisMemo.ScrMemID;
                p.PartyType = ddlPartyType.SelectedValue;
                p.PartyRelationshipCode = ddlPartyRel.SelectedValue;
                p.PartyRelationshipName = ddlPartyRel.SelectedItem.Text;
                p.PC = 0;
                //add the new party to the list
                newPartiesList.Add(p);
                tbPartyNames.Text = "";
                adverseWizard.MoveTo(adverseWizard.WizardSteps[0]);
            }
            adverseClass.SaveGroup(newPartiesList);
            mdlAdversePartys.Hide();
            BindData();
            upnlAdversePartyGrid.Update();
        }

        private void RequirePostback()
        {
            Response.Write("<script>document.forms[0].submit();</script>");
        }
        public void adverseWizard_SideBarButtonClick(object sender, System.Web.UI.WebControls.WizardNavigationEventArgs e)
        {
            if (e.NextStepIndex == 1) SetTextboxes();
        }
        public void adverseWizard_CancelButtonClick(object sender, EventArgs e)
        {
            mdlAdversePartys.Hide();
        }
        private void SetTextboxes()
        {
            string f = tbPartyNames.Text;
            string[] f1 = f.Split('\n');

            gvPartyRelationships.DataSource = f1;
            gvPartyRelationships.DataBind();

        }
    }
}