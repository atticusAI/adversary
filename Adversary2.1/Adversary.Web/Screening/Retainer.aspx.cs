﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.Utils;
using Adversary.DataLayer;

namespace Adversary.Web
{
    public partial class Retainer : ScreeningBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            rblRetainerYes.HH_AddAccordionPaneJavascript(accRetainerYes.ClientID);
            rblWillHaveRetainer.HH_AddAccordionPaneJavascript(accRetainer.ClientID);
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (ThisMemo.Retainer.HasValue)
            {
                rblWillHaveRetainer.SelectedIndex = -1;
                rblRetainerYes.SelectedIndex = -1;
                accRetainerYes.SelectedIndex = -1;
                accRetainer.SelectedIndex = -1;

                if (tbRetainerDescYes == null)
                {
                    tbRetainerDescYes = (TextBox)accRetainerYes.Panes["apRetainerYesDescription"].FindControl("tbRetainerDescYes");
                }

                if (Convert.ToBoolean(ThisMemo.Retainer))
                {
                    rblWillHaveRetainer.Items.FindByText("yes").Selected = true;
                    accRetainer.SelectedIndex = 0;
                }
                else
                {
                    rblWillHaveRetainer.Items.FindByText("no").Selected = true;
                    accRetainer.SelectedIndex = 1;
                }

                if (!String.IsNullOrEmpty(ThisMemo.RetainerDescYes))
                {
                    if(ThisMemo.RetainerDescYes.Length > 0)
                    {
                        rblRetainerYes.Items.FindByText("yes").Selected = true;
                        accRetainerYes.SelectedIndex = 0;
                    }
                }
                else
                {
                    rblRetainerYes.Items.FindByValue("no").Selected = true;
                    accRetainerYes.SelectedIndex = 1;
                }
            }

            TextBox __tbRetainerDescYes = (TextBox)this.apRetainerYesDescription.FindControl("tbRetainerDescYes");

            tbRetainerDescNo.Text = (ThisMemo.RetainerDescNo ?? string.Empty);
            //tbRetainerDescYes.Text = (ThisMemo.RetainerDescYes ?? string.Empty);
            __tbRetainerDescYes.Text = (ThisMemo.RetainerDescYes ?? string.Empty);

        }

        public override void Validate()
        {
            base.Validate();
            MemoRetainer mRetainer = new MemoRetainer(ThisMemo.ScrMemID);
            if (rblWillHaveRetainer.SelectedValue.ToLower().Equals("no"))
            {
                mRetainer.Retainer = false;
                mRetainer.RetainerDescNo = tbRetainerDescNo.Text;
                mRetainer.RetainerDesc = null;
                mRetainer.RetainerDescYes = null;
            }
            else
            {
                mRetainer.Retainer = true;
                mRetainer.RetainerDescNo = null;
                if(tbRetainerDescYes.Text.Trim().Length > 0)
                {
                    mRetainer.RetainerDescYes = tbRetainerDescYes.Text;
                }
                else
                {
                    mRetainer.RetainerDescYes = null;
                }
            }
            mRetainer.Save();
        }
    }
}