﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Runtime.Serialization;
using System.Web.Script.Serialization;
using AjaxControlToolkit;
using System.Web.Services;
using System.Web.Script.Services;
using Adversary.Web.Pages;
using Adversary.DataLayer;

namespace Adversary.Web
{
    public partial class ScreeningMasterPage : MasterBase, IMemoTrack
    {
        private int _CurrentLocation;
        public int CurrentLocation
        {
            set { _CurrentLocation = value; }
        }

        private int _CurrentTrack;
        public int CurrentTrack
        {
            set { _CurrentTrack = value; }
        }

        private string _CurrentPageFileName;
        public string CurrentPageFileName
        {
            set { _CurrentPageFileName = value; }
        }

        private bool _ShowNavigationButtons = true;
        public bool ShowNavigationButtons
        {
            get { return _ShowNavigationButtons; }
            set { _ShowNavigationButtons = value; }
        }

        private int _ScreeningMemoID;
        public int ScreeningMemoID
        {
            get { return _ScreeningMemoID; }
            set { _ScreeningMemoID = value; }
        }

        private bool _DisableTrackButtons;

        public bool DisableTrackButtons
        {
            get { return _DisableTrackButtons; }
            set { _DisableTrackButtons = value; }
        }
        


        public string ValidationGroupName
        {
            set
            { 
            }
        }

        public bool ShowDocketing
        {
            get
            {
                return string.IsNullOrWhiteSpace(this.hidShowDocketing.Value)
                    ? false
                    : Convert.ToBoolean(this.hidShowDocketing.Value);
            }

            set
            {
                this.hidShowDocketing.Value = Convert.ToString(value);
            }
        }
       
        protected void Page_Load(object sender, EventArgs e)
        {
            Adversary.DataLayer.MemoNavigation memoNav = new MemoNavigation(_CurrentTrack, _CurrentPageFileName);
            List<MemoApplicationTrack> allTracks = memoNav.GetAllApplicationTracks(this._CurrentTrack);
            rptToolbar.DataSource = allTracks;
            rptToolbar.DataBind();

            string nextURL = memoNav.NextSectionUrl;
            string previousURL = memoNav.PreviousSectionUrl;

            btnNext.Command += new CommandEventHandler(btnNext_Command);
            btnPrevious.Command += new CommandEventHandler(btnPrevious_Command);

            if (string.IsNullOrWhiteSpace(nextURL))
            {
                btnNext.Visible = false;
            }
            else
            {
                btnNext.CommandName = "Next";
                btnNext.CommandArgument = nextURL;
                //btnNext.Text = nextURL;
                btnNext.Text = "Next";
                btnNext.CausesValidation = true;
                btnNext.ValidationGroup = "valGroup_Screen";
            }

            if (string.IsNullOrWhiteSpace(previousURL))
            {
                btnPrevious.Visible = false;
            }
            else
            {
                btnPrevious.CommandName = "Previous";
                btnPrevious.CommandArgument = previousURL;
                //btnPrevious.Text = previousURL;
                btnPrevious.Text = "Previous";
                btnPrevious.CausesValidation = true;
                btnPrevious.ValidationGroup = "valGroup_Screen";
            }

            pnlPreviousNext.Visible = _ShowNavigationButtons;
            this.AddIncludes(this.head1);
            this.HighlightCurrentTab();
        }

        private void HighlightCurrentTab()
        {
            foreach (Control ctl in this.tdTabs.Controls)
            {
                if ((ctl is LinkButton) && ((LinkButton)ctl).CommandArgument == this._CurrentTrack.ToString())
                {
                    ((LinkButton)ctl).CssClass += " activetab";
                    break;
                }
            }
        }

        protected void btnPrevious_Command(object sender, CommandEventArgs e)
        {
            (cphMainContent.Page as ScreeningBasePage).Validate();
            if (this.Page.IsValid)
            {
                GotoPreviousPage(e.CommandArgument.ToString());
            }
        }

        private void GotoPreviousPage(string prevPage)
        {
            if (!ShowDocketing && prevPage.ToLowerInvariant() == "docketing.aspx")
            {
                Adversary.DataLayer.MemoNavigation nav = new MemoNavigation(_CurrentTrack, prevPage);
                Response.Redirect(this.AddQueryStringParam(nav.PreviousSectionUrl, "scrmemid", _ScreeningMemoID.ToString()));
            }
            else
            {
                Response.Redirect(this.AddQueryStringParam(prevPage, "scrmemid", _ScreeningMemoID.ToString()));
            }
        }

        private string AddQueryStringParam(string url, string name, string value)
        {
            return url.Contains("?") 
                ? url + "&" + name + "=" + value
                : url + "?" + name + "=" + value;
        }

        protected void btnNext_Command(object sender, CommandEventArgs e)
        {
            (cphMainContent.Page as ScreeningBasePage).Validate();
            if (this.Page.IsValid)
            {
                GotoNextPage(e.CommandArgument.ToString());
            }
        }

        private void GotoNextPage(string nextPage)
        {
            if (!ShowDocketing && nextPage.ToLowerInvariant() == "docketing.aspx")
            {
                Adversary.DataLayer.MemoNavigation nav = new MemoNavigation(_CurrentTrack, nextPage);
                Response.Redirect(this.AddQueryStringParam(nav.NextSectionUrl, "scrmemid", _ScreeningMemoID.ToString()));
            }
            else
            {
                Response.Redirect(this.AddQueryStringParam(nextPage, "scrmemid", _ScreeningMemoID.ToString()));
            }
        }
       
        protected void rptToolbar_ItemCreated(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton l = (LinkButton)e.Item.FindControl("lbNavigate");
                l.Command += new CommandEventHandler(l_Command);
            }
        }

        protected void l_Command(object sender, CommandEventArgs e)
        {
            cphMainContent.Page.Validate();
            if (this.Page.IsValid)
            {
                Response.Redirect(e.CommandArgument.ToString());
            }
        }

        protected void rptToolbar_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            MemoApplicationTrack a = (MemoApplicationTrack)e.Item.DataItem;
            LinkButton l = (LinkButton)e.Item.FindControl("lbNavigate");
            if (l != null && a != null)
            {
                l.CommandArgument = this.AddQueryStringParam(a.PageURL, "scrmemid", _ScreeningMemoID.ToString()); 
                l.Text = a.Name;
                l.ToolTip = a.Description;
                l.CausesValidation = true;
                if (string.Compare(_CurrentPageFileName, a.PageURL, true) == 0)
                {
                    l.CssClass += " activesubtab";
                }

                if (a.Name.ToLowerInvariant() == "docketing")
                {
                    l.CssClass += " docketingsubtab";
                }

                // disable sub-tabs when depending upon the state of the current memo
                if (this.Page is ScreeningBasePage)
                {
                    DataLayer.Memo memo = ((ScreeningBasePage)this.Page).ThisMemo;
                    if (DisableTrackButtons || ((memo.HH_IsMemoArchived() || memo.HH_IsMemoLocked()) && l.Text.ToLowerInvariant() != "memo summary" ))
                    {
                        l.Enabled = false;
                    }
                }
            }
        }

        protected void lnkNew_Click(object sender, EventArgs e)
        {
            int scrMemType = -1;
            int scrMemID = -1;
            if (Int32.TryParse(((LinkButton)sender).CommandArgument, out scrMemType))
            {
                //call create memo function here
                AdversaryData AdversaryDataLayer = new Adversary.DataLayer.AdversaryData();
                scrMemID = AdversaryDataLayer.CreateMemo(scrMemType).ScrMemID;
                //pass screening memo ID to the page
                Response.Redirect(string.Format("~/Screening/BasicInformation.aspx?ScrMemType={0}&scrmemid={1}", scrMemType, scrMemID));
            }
        }

        protected void hypApprovalRedir_Init(object sender, EventArgs e)
        {
            if (this.Page is ScreeningBasePage)
            {
                Memo memo = ((ScreeningBasePage)this.Page).ThisMemo;
                if (memo != null)
                {
                    if (memo.Trackings != null && memo.Trackings.Count > 0)
                    {
                        CurrentUser currentUser = ((ScreeningBasePage)this.Page).CurrentUser;
                        if (currentUser.IsAP || currentUser.IsPGM || currentUser.IsAdversary)
                        {
                            ((HyperLink)sender).Visible = true;
                            ((HyperLink)sender).NavigateUrl = "~/Admin/Approval.aspx?ScrMemID=" + memo.ScrMemID;
                        }
                        else 
                        {
                            ((HyperLink)sender).Visible = false;   
                        }
                    }
                }
            }
        }

        //protected void tabNav_PreRender(object sender, EventArgs e)
        //{
        //    if (this.Page is ScreeningBasePage)
        //    {
        //        this.nav.CurrentUser = ((ScreeningBasePage)this.Page).CurrentUser;
        //    }
        //}

        
    }
}
