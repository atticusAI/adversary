﻿<%@ Page Theme="HH" Title="" Language="C#" MasterPageFile="~/Screening/Screening.Master" AutoEventWireup="true" CodeBehind="BillingInfo.aspx.cs" Inherits="Adversary.Web.Screening.BillingInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphMainContent" runat="server">
    <asp:HiddenField ID="hidBillingID" runat="server" />
    <table border="0" cellpadding="3" cellspacing="2">
        <tr>
            <td class="lbl">Please describe any special fee arrangements:</td>
            <td><asp:TextBox ID="tbSpecialFeeArrangements" runat="server" TextMode="MultiLine" Columns="50" Rows="5" /></td>
        </tr>
        <tr>
            <td class="lbl">Special billing instructions:</td>
            <td><asp:TextBox ID="tbSpecialBillingInstructions" runat="server" TextMode="MultiLine" Columns="50" Rows="5" /></td>
        </tr>
        <tr>
            <td class="lbl">Name of special bill format:</td>
            <td><asp:TextBox ID="tbSpecialBillFormat" runat="server" SkinID="HH_Textbox" /> </td>
        </tr>
        <tr>
            <td class="lbl">Client level billing:</td>
            <td><asp:RadioButtonList ID="rblClientLevelBilling" runat="server" SkinID="HH_RadioButtonList_Horizontal">
                <asp:ListItem Text="Yes" Value="yes" />
                <asp:ListItem Text="No" Value="no" />
            </asp:RadioButtonList></td>
        </tr>
        <tr>
            <td class="lbl">Interest on Late Payments?</td>
            <td>
                <asp:RadioButtonList ID="rblInterestLatePayments" runat="server" SkinID="HH_RadioButtonList_Horizontal">
                    <asp:ListItem Text="Yes" Value="yes" />
                    <asp:ListItem Text="No" Value="no" />
                </asp:RadioButtonList>
            </td>
        </tr>
    </table>
</asp:Content>

