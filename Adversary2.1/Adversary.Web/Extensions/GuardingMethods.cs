﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Adversary.Web.Extensions
{
    public static class GuardingMethods
    {
        public static void HH_GuardAgainstNull(this object o, string paramName)
        {
            if (o == null)
            {
                throw new ArgumentNullException(paramName);
            }
        }

        public static void HH_GuardAgainstNullOrWhiteSpace(this string s, string paramName)
        {
            if (string.IsNullOrWhiteSpace(s))
            {
                throw new ArgumentException("Parameter cannot be null, empty, or white space.", paramName);
            }
        }
    }
}