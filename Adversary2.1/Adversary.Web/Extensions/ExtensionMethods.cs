﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;


namespace Adversary.Web
{
    public static class ExtensionMethods
    {
        public static void HH_AddAccordionPaneJavascript(this RadioButtonList rblButtons, string accordionPaneClientID)
        {
            rblButtons.Items[0].Attributes.Add("onclick", "javascript:setAccordionPane('" + accordionPaneClientID + "', 0);");
            rblButtons.Items[1].Attributes.Add("onclick", "javascript:setAccordionPane('" + accordionPaneClientID + "', 1);");
        }

        public static Control HH_FindControlInFamilyTreeByType(this Control ctrl, Type t)
        {
            Control found = ctrl.Parent;
            if (found != null && found.GetType() != t && found.GetType().BaseType != t)
            {
                found = ctrl.Parent.HH_FindControlInFamilyTreeByType(t);
            }
            return found;
        }

        public static string HH_StripDomain(this string userName)
        {
            string name = userName;
            if (!string.IsNullOrWhiteSpace(userName))
            {
                string[] parts = userName.Split('\\');
                if (parts.Length > 1)
                {
                    name = parts[1];
                }
            }
            return name;
        }

        public static string HH_ErrorMessageHTML(this Exception x)
        {
            StringBuilder sb = new StringBuilder();
            if (x != null)
            {
                sb.AppendFormat("Error Message: {0}<br /><br />", x.Message);
                sb.AppendFormat("Stack Trace: {0}<br /><br />", x.StackTrace.Replace("\r\n", "<br />"));
            }
            return sb.ToString();
        }

        public static string HH_ErrorMessage(this Exception x)
        {
            StringBuilder sb = new StringBuilder();
            if (x != null)
            {
                sb.AppendFormat("Error Message: {0}\r\n\r\n", x.Message);
                sb.AppendFormat("Stack Trace: {0}\r\n\r\n", x.StackTrace);
            }
            return sb.ToString();
        }
    }
}