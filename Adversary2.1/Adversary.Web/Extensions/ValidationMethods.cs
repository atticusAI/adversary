﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Adversary.Web.Extensions
{
    public static class ValidationMethods
    {
        /// <summary>
        /// string is an integer and not empty
        /// </summary>
        public static bool HH_IsValidInt(this string s)
        {
            int i;
            return (!string.IsNullOrWhiteSpace(s) && int.TryParse(s, out i));
        }

        public static bool HH_IsValidInt(this object o)
        {
            bool valid = false;
            if (o is string)
            {
                valid = ((string)o).HH_IsValidInt();
            }
            else if (o is int)
            {
                valid = true;
            }
            return valid;
        }

        public static DateTime SqlMinDate
        {
            get
            {
                DateTime dt;
                DateTime.TryParse("1/1/1753", out dt);
                return dt;
            }
        }

        public static DateTime SqlMaxDate
        {
            get
            {
                DateTime dt;
                DateTime.TryParse("12/31/9999", out dt);
                return dt;
            }
        }

        /// <summary>
        /// generic validation function for datetime
        /// </summary>
        public static bool HH_IsValidDate(this string s)
        {
            bool valid = false;
            if (!string.IsNullOrWhiteSpace(s))
            {
                // SqlDateTime overflow. Must be between 1/1/1753 12:00:00 AM and 12/31/9999 11:59:59 PM.
                DateTime dt;
                valid = (DateTime.TryParse(s, out dt) &&
                    dt >=  SqlMinDate && 
                    dt <= SqlMaxDate);
            }
            return valid;
        }

        public static bool HH_IsAfterDate(this string s, string a)
        {
            bool valid = false;

            if (!string.IsNullOrWhiteSpace(s) && !string.IsNullOrWhiteSpace(a))
            {
                DateTime dt1;
                DateTime dt2;

                valid = (DateTime.TryParse(s, out dt1) & DateTime.TryParse(a, out dt2));

                if (valid)
                {
                    valid = dt1 > dt2;
                }
            }

            return valid;
        }

        public static bool HH_IsOnOrAfterDate(this string s, string a)
        {
            bool valid = false;

            if (!string.IsNullOrWhiteSpace(s) && !string.IsNullOrWhiteSpace(a))
            {
                DateTime dt1;
                DateTime dt2;

                valid = (DateTime.TryParse(s, out dt1) & DateTime.TryParse(a, out dt2));

                if (valid)
                {
                    valid = dt1 >= dt2;
                }
            }

            return valid;
        }

        public static bool HH_IsBeforeDate(this string s, string b)
        {
            bool valid = false;

            if (!string.IsNullOrWhiteSpace(s) && !string.IsNullOrWhiteSpace(b))
            {
                DateTime dt1;
                DateTime dt2;

                valid = (DateTime.TryParse(s, out dt1) & DateTime.TryParse(b, out dt2));

                if (valid)
                {
                    valid = dt1 < dt2;
                }
            }

            return valid;
        }

        public static void HH_SetError(this WebControl ctl, Panel pnl)
        {
            ctl.CssClass += " error";
            AddError(pnl, ctl.Attributes["title"], ctl.ClientID);
        }

        public static void HH_SetError(this WebControl ctl, Panel pnl, string message)
        {
            ctl.CssClass += " error";
            AddError(pnl, message, ctl.ClientID);
        }

        public static void HH_ClearError(this WebControl ctl)
        {
            ctl.CssClass = ctl.CssClass.Replace("error", "");
        }

        private static void AddError(Panel pnl, string errMsg, string forID)
        {
            if (pnl != null && !string.IsNullOrWhiteSpace(errMsg))
            {
                pnl.Controls.Add(ErrorLabel(errMsg, forID));
            }
        }

        public static void HH_ClearError(this Panel pnl)
        {
            pnl.Controls.Clear();
        }

        private static HtmlGenericControl ErrorLabel(string errMsg, string forID)
        {
            HtmlGenericControl lbl = new HtmlGenericControl("label");
            lbl.InnerText = errMsg;
            lbl.Attributes["for"] = forID;
            lbl.Attributes["class"] = "error";
            return lbl;
        }
    }

}