﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;
using System.Reflection;
using System.Linq.Expressions;


namespace Adversary.Web.Extensions
{
    public static class GridviewExtensionMethods
    {
        public static string HH_AsSortExpression(this GridView gridView)
        {
            SortDirection direction = gridView.Attributes["SrtDir"] == SortDirection.Ascending.ToString()
                    ? SortDirection.Ascending
                    : SortDirection.Descending;

            string columns = gridView.Attributes["SrtCol"];

            StringBuilder sb = new StringBuilder();
            if (columns != null && columns != "")
            {
                int i = 0;
                foreach (string s in columns.Split(','))
                {
                    if (i > 0)
                        sb.Append(", ");
                    sb.Append(s);
                    sb.Append(" ");
                    sb.Append(direction == SortDirection.Ascending ? "ASC" : "DESC");
                    i++;
                }
            }

            return sb.ToString();
        }

        
        public static void HH_TrackSortOrder(this GridView gridView, GridViewSortEventArgs e)
        {
            string prevCol = gridView.Attributes["SrtCol"];
            gridView.Attributes["SrtCol"] = e.SortExpression;
            if (prevCol == e.SortExpression)
            {
                string prevDir = gridView.Attributes["SrtDir"] == null
                    ? SortDirection.Descending.ToString()
                    : gridView.Attributes["SrtDir"];

                e.SortDirection = prevDir == SortDirection.Ascending.ToString()
                    ? SortDirection.Descending
                    : SortDirection.Ascending;

                gridView.Attributes["SrtDir"] = e.SortDirection.ToString();
            }
            else
            {
                gridView.Attributes["SrtDir"] = SortDirection.Ascending.ToString();
            }
        }
    }
}