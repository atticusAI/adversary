﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Adversary.Web.Pages;

namespace Adversary.Web
{
    public partial class BaseMaster : MasterBase 
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.AddIncludes(this.head1);
            this.HighlightCurrentTab(this.tdTabs);
        }

        protected void tabNav_PreRender(object sender, EventArgs e)
        {
            if (this.Page is TrackBasePage)
            {
                this.tabNav.CurrentUser = ((TrackBasePage)this.Page).CurrentUser;
            }
        }

        public string TabPageUrl
        {
            get
            {
                return this.tabNav.PageUrl;
            }
            set
            {
                this.tabNav.PageUrl = value;
            }
        }

        public string SubTabPageUrl
        {
            get
            {
                return this.subTabNav.PageUrl;
            }
            set
            {
                this.subTabNav.PageUrl = value;
            }
        }
    }
}