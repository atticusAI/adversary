﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Adversary.DataLayer;
using Adversary.Utils;
using System.IO;

namespace Adversary.Web.Screening
{
    /// <summary>
    /// Summary description for download
    /// </summary>
    public class download : IHttpHandler
    {

        private string _FileUploadDirectory;
        private string FileUploadDirectory
        {
            get
            {
                try
                {
                    _FileUploadDirectory = System.Configuration.ConfigurationManager.AppSettings["FileUploadDirectory"].ToString();
                }
                catch { }
                return _FileUploadDirectory;
            }
        }


        public void ProcessRequest(HttpContext context)
        {
            int attachmentID;
            string safeTitle = string.Empty;
            HttpResponse response = context.Response;
            HttpRequest request = context.Request;
            if (String.IsNullOrEmpty(FileUploadDirectory) || String.IsNullOrWhiteSpace(FileUploadDirectory))
            {
                WriteInvalidRequestID(context, "The file upload directory was not specified.  Processing cannot continue");
                return;
            }

            try
            {
                bool success = Int32.TryParse(request.QueryString["attachmentID"].ToString(), out attachmentID);
                if (!success) WriteInvalidRequestID(context);
                else
                {
                    AdversaryData adv = new AdversaryData();
                    Attachment att = adv.GetAttachment(attachmentID);
                    if (att != null)
                    {
                        string filePath = FileUploadDirectory + att.ScrMemID.ToString() + "\\" + att.FileName;
                        string fileExtension = FileUtils.GetFileExtension(filePath).ToLowerInvariant().Replace(".", "");
                        Dictionary<string,string> mimeTypes = StringUtils.MIMEExtensionsAndTypes();
                        if (FileUtils.FileExists(filePath))
                        {

                            KeyValuePair<string, string> mimeType =
                                mimeTypes.Where(x => x.Key.ToLowerInvariant().Equals(fileExtension)).FirstOrDefault();
                            if (mimeType.Key != null)
                            {
                                response.Clear();
                                response.ClearContent();
                                response.ClearHeaders();
                                response.ContentType = mimeType.Value;
                                response.AddHeader("Content-Disposition", "attachment; filename=" + att.FileName);
                                response.WriteFile(filePath);
                                //response.End();
                            }
                        }
                        else
                        {
                            throw new Exception("Could not downlaod file.  File at " + filePath + " does not exist.");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteInvalidRequestID(context, String.Format("There was a problem with your download request: {0}", ex.Message));
            }
        }

        public void WriteInvalidRequestID(HttpContext context, string message)
        {
            if (string.IsNullOrEmpty(message)) message = "You have specified an improper/invalid attachment ID";
            HttpResponse resp = context.Response;
            resp.Clear();
            resp.ClearHeaders();
            resp.ContentType = "text/plain";
            resp.Write(message);
            resp.End();
        }


        public void WriteInvalidRequestID(HttpContext context)
        {
            WriteInvalidRequestID(context, "");
        }



        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}

/*
 
 using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

namespace VCRequestForm
{
    /// <summary>
    /// This provides the ics file to populate outlook with the conference request entry
    /// </summary>
    public class Calendar : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            string safeTitle = string.Empty;
            HttpResponse resp = context.Response;
            HttpRequest req = context.Request;
            CalendarHelper calHelper = new CalendarHelper();
            int iRequestID = 0;

            if (req.QueryString["RequestID"] == null) WriteInvalidRequestID(context);

            //parse the query string
            if (!Int32.TryParse(req.QueryString["RequestID"].ToString(), out iRequestID))
            {
                WriteInvalidRequestID(context);
            }
            else
            {
                iRequestID = Int32.Parse(req.QueryString["RequestID"].ToString());
            }

            //get the conference request from the database
            ConferenceRequest cReq = new ConferenceRequest();
            using(VCRequestDataContext vc = new VCRequestDataContext())
	        {
		        cReq = vc.ConferenceRequests.FirstOrDefault(R => R.RequestID == iRequestID);
                resp.ContentType = "text/calendar";
                resp.AddHeader("Content-disposition", "attachment;filename=" + Utils.MakeValidFileName(cReq.ConferenceTitle) + ".ics");
                resp.Write(calHelper.CreateConferenceRequestCalendar(cReq));
                resp.End();
	        }

            if (cReq == null) WriteInvalidRequestID(context);

            

            safeTitle = cReq.ConferenceTitle.Replace('/', '-');
            safeTitle = safeTitle.Replace('#', '-');

            //set up the response
            
        }

        public void WriteInvalidRequestID(HttpContext context)
        {
            HttpResponse resp = context.Response;
            resp.ContentType = "text/plain";
            resp.Write("incorrect conference request ID");
            resp.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }

}
 */