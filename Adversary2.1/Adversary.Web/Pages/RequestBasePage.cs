﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Adversary.DataLayer;
using Adversary.Requests;

namespace Adversary.Web.Pages
{
    public class RequestBasePage : TrackBasePage
    {

        private int? _requestID = null;

        public int RequestID
        {
            get
            {
                if (this._requestID == null)
                {
                    int refID = -1;
                    if (Int32.TryParse(Request.QueryString["RequestID"], out refID))
                    {
                        this._requestID = refID;
                    }
                }
                return this._requestID ?? -1;
            }
            set
            {
                this._requestID = value;
            }
        }

        private string _PageFileName;
        public string PageFileName
        {
            get
            {
                if (_PageFileName == null)
                {
                    System.IO.FileInfo oInfo = new System.IO.FileInfo(System.Web.HttpContext.Current.Request.Url.AbsolutePath);
                    //get the page file name for putting together the next/previous buttons
                    _PageFileName = oInfo.Name;
                }
                return _PageFileName;
            }
        }

        public Requests.RequestsMasterPage thisMasterPage;

        private Adversary.DataLayer.Request _ThisRequest = null;
        public Adversary.DataLayer.Request ThisRequest
        {
            get 
            {
                if (_ThisRequest == null)
                {
                    LoadRequest();
                }
                return _ThisRequest; 
            }
            set { _ThisRequest = value; }
        }

        private bool _HasRequestError;
        public bool HasRequestError
        {
            get { return _HasRequestError; }
            set { _HasRequestError = value; }
        }

        public void LoadRequest()
        {
            _ThisRequest = AdversaryDataLayer.GetRequest(this.RequestID);
            if (_ThisRequest == null)
                _ThisRequest = new Request();
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            string sPath = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
            string[] standalonePages = new string[] { "default", "search", "viewrequests", "reportviewer" };

            string applicationPath = "http://" + Request.Url.Authority;

            //fetch this page's master page into a variable
            thisMasterPage = this.Master as Requests.RequestsMasterPage;

            //access the error summary control on the master page
            Adversary.Web.Controls.ErrorSummary _ErrorSummary = (Adversary.Web.Controls.ErrorSummary)thisMasterPage.FindControl("master_ErrorSummary");

            //check to see if there is a request ID specified.
            //If not, then determine the url of the page. Redirect if the page is not in the list of pages that do not require a Request ID
            //Else, load the request into the ThisRequest object and continue.
            if (string.IsNullOrWhiteSpace(Request.QueryString["requestid"]))
            {
                //no request ID specified, determine page location.
                if (!standalonePages.Any(s => sPath.ToLowerInvariant().Contains(s)))
                {
                    //page location isn't one that allows no request, so redirect to default.
                    Response.Redirect("~/Requests/Default.aspx");
                    return;
                }
            }
            else
            {
                // if the requestID is bad then complain
                if (this.RequestID == -1){
                    _ErrorSummary.DisplayMessage = "You have entered an invalid adversary request";
                    _ErrorSummary.DisplayType = Web.Controls.MessageDisplayType.DisplayError;
                    _ErrorSummary.ErrorList.Add(String.Format("{0} is an invalid adversary request number. Please evaluate your input and try again.", Request.QueryString["requestid"]));
                    _HasRequestError = true;
                    thisMasterPage.ShowNavigationButtons = false;
                }
            }

            thisMasterPage.CurrentTrack = 8;
            thisMasterPage.CurrentPageFileName = PageFileName;
            thisMasterPage.RequestID = this.RequestID;

            //set the message (if any) which can be overridden by the inheriting page
            if (ThisRequest != null)
            {
                if (ThisRequest.HH_IsRequestComplete())
                {
                    thisMasterPage.SetErrorMessage(Adversary.Requests.RequestMessages.REQ_COMPLETED_MSG, Web.Controls.MessageDisplayType.DisplayInfo);
                    thisMasterPage.ShowNavigationButtons = false;
                    thisMasterPage.DisableTrackButtons = true;
                }
                else if (ThisRequest.HH_IsRequestWorkBegun())
                {
                    thisMasterPage.SetErrorMessage(Adversary.Requests.RequestMessages.REQ_WORK_BEGUN_MSG, Web.Controls.MessageDisplayType.DisplayValid);
                }
                else if (ThisRequest.HH_IsRequestSubmitted())
                {
                    thisMasterPage.SetErrorMessage(Adversary.Requests.RequestMessages.REQ_SUBMITTED_MSG, Web.Controls.MessageDisplayType.DisplayValid);
                }
            }
        }

        private void RedirectToDefault()
        {
            Response.Redirect(Page.TemplateSourceDirectory + "/default.aspx");
        }
    }
}