﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Diagnostics;

namespace Adversary.Web
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {

        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// TODO: global error handling as below, and a custom errors page in the web config
        /// </summary>
        protected void Application_Error(object sender, EventArgs e)
        {
            //Exception objErr = Server.GetLastError().GetBaseException();
            //string err = "URL: " + Request.Url.AbsolutePath +
            //        "\nUser: " + Request.LogonUserIdentity.Name + 
            //        "\nMessage:" + objErr.Message +
            //        "\nStack Trace:" + objErr.StackTrace;
            //EventLog.WriteEntry("HH_NBIS Web App", err, EventLogEntryType.Error);
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}