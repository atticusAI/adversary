﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Runtime.Caching;

namespace Adversary.SessionService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "CacheService" in code, svc and config file together.
    public class CacheService : ICacheService
    {
        static readonly ObjectCache _Cache = MemoryCache.Default;

        public object Get(string key)
        {
            try
            {
                return (object)_Cache[key];
            }
            catch
            {
                return null;
            }
        }
        public object Get(Guid guid)
        {
            try
            {
                return (object)_Cache[guid.ToString()];
            }
            catch
            {
                return null;
            }
            
        }


        public void PlaceObject(object objectToPlace, string key)
        {
            ObjectCache cache = MemoryCache.Default;
            cache.Add(key, objectToPlace, new DateTimeOffset(DateTime.Now.AddMinutes(5)));
            
        }

        public void RemoveObject(string key)
        {
            ObjectCache cache = MemoryCache.Default;
            cache.Remove(key);
        }





        public string TestMe()
        {
            return "This is the cache service";
        }
    }
}
