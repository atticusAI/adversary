﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Xml.Linq;

namespace Adversary.Utils
{
    public static class ReportUtils
    {
        public static string GetReportName(string reportFile)
        {
            return ReportUtils.GetReportVariable(reportFile, "ReportName");
        }

        public static string GetReportVariable(string reportFile, string variableName)
        {
            string val = "";

            try
            {
                if (!string.IsNullOrWhiteSpace(reportFile))
                {
                    XElement report = XElement.Load(reportFile);
                    XNamespace ns = "http://schemas.microsoft.com/sqlserver/reporting/2008/01/reportdefinition";

                    var qry = from e in report.Descendants(ns + "Variable")
                              where e.Attribute("Name").Value == variableName
                              select e.Value;

                    val = qry.FirstOrDefault();
                }
            }
            catch (Exception x)
            {
                val = null;
            }

            return val;
        }

        public static ListItem[] GetReportsList(string directoryName)
        {
            List<ListItem> items = new List<ListItem>();

            if (Directory.Exists(directoryName))
            {
                foreach (string f in Directory.GetFiles(directoryName, "*.rdlc"))
                {
                    string name = ReportUtils.GetReportName(f);
                    if (!string.IsNullOrWhiteSpace(name))
                    {
                        items.Add(new ListItem(name, f));
                    }
                }
            }

            return items.ToArray();
        }

        public static string GetReportControlsFile(string path)
        {
            string r = path;

            if (!string.IsNullOrWhiteSpace(path))
            {
                r = path.ReplaceLast(".rdlc", ".ascx");
            }

            return r;
        }

    }
}
