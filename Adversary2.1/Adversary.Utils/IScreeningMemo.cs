﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.Utils.Interfaces
{
    public interface IScreeningMemo
    {
        int ScreeningMemoID { get; }
    }
    public interface IScreeningMemoGet : IScreeningMemo
    {
        bool Get();
    }
    public interface IScreeningMemoSave : IScreeningMemo
    {
        bool Save();
    }
    public interface IScreeningMemoDelete : IScreeningMemo
    {
        bool Delete();
    }
}
