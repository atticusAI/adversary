﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Adversary.Utils")]
[assembly: AssemblyDescription("New Business Intake utility methods")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Holland and Hart LLP")]
[assembly: AssemblyProduct("Adversary.Utils")]
[assembly: AssemblyCopyright("Copyright © Holland and Hart LLP 2009-2012")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("dea43f32-224f-4f30-b741-f78d88f5b25d")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2.0.0.1")]
[assembly: AssemblyFileVersion("2.0.0.1")]
[assembly: AssemblyInformationalVersion("2.0.0.1")]

