﻿using System;
using System.IO;

namespace Adversary.Utils
{
    public static class FileUtils
    {
        public static bool FileExists(string path)
        {
            return File.Exists(path);
        }

        public static bool DirectoryExists(string path)
        {
            return Directory.Exists(path);
        }

        public static bool CreateDirectory(string path)
        {
            return Directory.CreateDirectory(path).Exists;
        }

        public static bool DeleteFile(string path)
        {
            System.IO.File.Delete(path);
            return true;
        }

        public static string GetFileExtension(string path)
        {
            return Path.GetExtension(path);
        }
    }
    
}
