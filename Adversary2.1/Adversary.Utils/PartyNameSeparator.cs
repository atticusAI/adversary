﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.Utils
{
    public static class PartyNameSeparator
    {
        public static string[] SeparateIndividualName(string fullName)
        {
            string[] splitName = new string[3] { "", "", "" };
            string[] parts = fullName.Split();
            if (parts.Length == 2)
            {
                splitName[0] = parts[0];
                splitName[1] = null;
                splitName[2] = parts[1];
            }
            else
            {
                for (int i = 0; i < parts.Length; i++)
                {
                    if (i < 2)
                    {
                        splitName[i] = parts[i];
                    }
                    else
                    {
                        splitName[2] += parts[i] + " ";
                    }
                }
                splitName[2] = splitName[2].Trim();
            }
            return splitName;
        }

    }
}
