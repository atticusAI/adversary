﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Web;

namespace Adversary.Utils
{
    public static class WebUtils
    {
        public static bool TestMode
        {
            get
            {
                bool testMode = false;
                if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["TestMode"]))
                {
                    Boolean.TryParse(ConfigurationManager.AppSettings["TestMode"], out testMode);
                }
                return testMode;
            }
        }


        public static string UnMapPath(this HttpServerUtility server, string path)
        {
            string r = path;
            if (server != null && !string.IsNullOrWhiteSpace(path))
            {
                string applicationPath = server.MapPath("~/");
                if (path.Contains(applicationPath))
                {
                    r = path.Substring(applicationPath.Length);
                    r = r.Replace('\\', '/');
                    r = r.Insert(0, "~/");
                }
            }
            return r;
        }
    }
}
