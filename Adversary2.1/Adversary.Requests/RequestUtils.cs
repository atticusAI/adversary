﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adversary.Requests
{
    public static class RequestUtils
    {
        public static DateTime ComputeDeadlineDate(DateTime startDate)
        {
            return startDate.AddDays(1);
        }

       
    }

    public static class RequestMessages
    {
        public const string REQ_CAN_BE_SUBMITTED_MSG = "This adversary request can be submitted.";
        public const string REQ_SUBMITTED_MSG = "This adversary request has been submitted.";
        public const string REQ_VALIDATION_FAILED_MSG = "This adversary request does not pass validation.";
        public const string REQ_WORK_BEGUN_MSG = "This adversary request is currently being processed.";
        public const string REQ_COMPLETED_MSG = "This adversary request has been completed.";
    }
}
