﻿CREATE TABLE [dbo].[AdditionalNames] (
    [AdditionalNameID] INT            IDENTITY (1, 1) NOT NULL,
    [ClientNumber]     VARCHAR (50)   NULL,
    [Comments]         NVARCHAR (MAX) NULL,
    [UploadDate]       DATETIME       NULL,
    [IsComplete]       BIT            CONSTRAINT [DF_AdditionalNames_IsComplete] DEFAULT ((0)) NULL,
    [FileName]         VARCHAR (500)  NULL,
    [UploaderPersonID] VARCHAR (20)   NULL,
    CONSTRAINT [PK_AdditionalNames] PRIMARY KEY CLUSTERED ([AdditionalNameID] ASC)
);

