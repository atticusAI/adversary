﻿CREATE TABLE [dbo].[State] (
    [StateID]   INT           IDENTITY (1, 1) NOT NULL,
    [Code]      NVARCHAR (50) NOT NULL,
    [Name]      NVARCHAR (50) NOT NULL,
    [SortOrder] INT           NULL,
    CONSTRAINT [PK_State] PRIMARY KEY CLUSTERED ([StateID] ASC)
);

