﻿CREATE TABLE [dbo].[MemoConflict] (
    [ScrMemID]   INT NOT NULL,
    [ConflictID] INT NOT NULL,
    [Answer]     BIT NOT NULL,
    CONSTRAINT [PK_MemoConflict] PRIMARY KEY CLUSTERED ([ScrMemID] ASC, [ConflictID] ASC),
    CONSTRAINT [FK_MemoConflict_Conflict] FOREIGN KEY ([ConflictID]) REFERENCES [dbo].[Conflict] ([ConflictID]),
    CONSTRAINT [FK_MemoConflict_Memo] FOREIGN KEY ([ScrMemID]) REFERENCES [dbo].[Memo] ([ScrMemID])
);

