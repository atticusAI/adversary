﻿CREATE TABLE [dbo].[RequestConflict] (
    [ReqID]      INT NOT NULL,
    [ConflictID] INT NOT NULL,
    [Answer]     BIT NOT NULL,
    CONSTRAINT [PK_RequestConflict] PRIMARY KEY CLUSTERED ([ReqID] ASC, [ConflictID] ASC),
    CONSTRAINT [FK_RequestConflict_Conflict] FOREIGN KEY ([ConflictID]) REFERENCES [dbo].[Conflict] ([ConflictID]),
    CONSTRAINT [FK_RequestConflict_Requests] FOREIGN KEY ([ReqID]) REFERENCES [dbo].[Requests] ([ReqID])
);

