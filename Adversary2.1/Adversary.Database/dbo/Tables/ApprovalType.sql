﻿CREATE TABLE [dbo].[ApprovalType] (
    [ApprovalTypeID] INT           IDENTITY (1, 1) NOT NULL,
    [ApprovalType]   NVARCHAR (50) NOT NULL,
    [TrackNumber]    INT           NULL,
    [Active]         BIT           NULL,
    CONSTRAINT [PK_ApprovalType] PRIMARY KEY CLUSTERED ([ApprovalTypeID] ASC)
);

