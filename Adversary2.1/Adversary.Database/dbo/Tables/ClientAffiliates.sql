﻿CREATE TABLE [dbo].[ClientAffiliates] (
    [ClientAffiliateID] INT           IDENTITY (1, 1) NOT NULL,
    [CName]             VARCHAR (255) NULL,
    [LName]             VARCHAR (50)  NULL,
    [FName]             VARCHAR (50)  NULL,
    [MName]             VARCHAR (50)  NULL,
    [ContactName]       NVARCHAR (50) NULL,
    [ContactTitle]      NVARCHAR (50) NULL,
    [RelationshipCode]  NVARCHAR (50) NULL,
    [NwClientID]        INT           NULL,
    CONSTRAINT [PK_ClientAffiliates] PRIMARY KEY CLUSTERED ([ClientAffiliateID] ASC),
    CONSTRAINT [FK_ClientAffiliates_NewClient] FOREIGN KEY ([NwClientID]) REFERENCES [dbo].[NewClient] ([NwClientID])
);

