﻿CREATE TABLE [dbo].[MemoApproval] (
    [ScrMemID]         INT          NOT NULL,
    [ApprovalTypeID]   INT          NOT NULL,
    [Acknowledged]     BIT          NULL,
    [AcknowledgedDate] DATETIME     NULL,
    [Approved]         BIT          NULL,
    [ApprovedDate]     DATETIME     NULL,
    [UserID]           INT          NULL,
    [Signature]        VARCHAR (50) NULL,
    [ARSignature]      VARCHAR (50) NULL,
    CONSTRAINT [PK_MemoApproval] PRIMARY KEY CLUSTERED ([ScrMemID] ASC, [ApprovalTypeID] ASC)
);

