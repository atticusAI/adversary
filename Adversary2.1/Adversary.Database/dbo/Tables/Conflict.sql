﻿CREATE TABLE [dbo].[Conflict] (
    [ConflictID]     INT            IDENTITY (1, 1) NOT NULL,
    [TrackNumber]    INT            NOT NULL,
    [Description]    NVARCHAR (255) NULL,
    [SortOrder]      INT            NULL,
    [NoneOfTheAbove] BIT            NULL,
    [RequestField]   NVARCHAR (50)  NULL,
    [Active]         BIT            NULL,
    CONSTRAINT [PK_Conflict] PRIMARY KEY CLUSTERED ([ConflictID] ASC)
);

