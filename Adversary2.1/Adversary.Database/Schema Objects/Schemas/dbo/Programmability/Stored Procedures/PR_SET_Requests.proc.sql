﻿
CREATE PROCEDURE [dbo].[PR_SET_Requests]
	@ReqID [int],
	@TypistLogin [nvarchar](50),
	@SubmittalDate [datetime],
	@DeadlineDate [datetime],
	@AttyLogin [nvarchar](50),
	@OKforNewMatter [bit],
	@ClientCategory [nvarchar](50),
	@ClientName [nvarchar](255),
	@ClientMatter [nvarchar](50),
	@PracticeCode [nvarchar](50),
	@ClientAddress [nvarchar](255),
	@Priority [nvarchar](50),
	@OpposingCounsel [nvarchar](150),
	@RelatedParties [nvarchar](max),
	@AdverseParties [nvarchar](max),
	@WorkBegunBy [nvarchar](50),
	@WorkBegunDate [datetime],
	@WorkCompletedBy [nvarchar](50),
	@CompletionDate [datetime],
	@Insurance [bit],
	@SkiResort [bit],
	@LawFirm [bit],
	@Communications [bit],
	@Government [bit],
	@IndianTribe [bit],
	@Pharm [bit],
	@AccountingFirm [bit],
	@NoneOfTheAbove [bit],
	@OilGas [bit],
	@LegalFees [bit],
	@ElectricUtility [bit],
	@Bankruptcy [bit],
	@PartiesToMediationArbitration [nvarchar](max),
	@RelatedCoDefendants [nvarchar](max)
	--@AffiliatesOfAdverseParties [nvarchar] (max),
	--@OtherClientAffiliates [nvarchar] (max)
AS
IF @ReqID < 1
BEGIN
 INSERT INTO [dbo].[Requests] (
 [TypistLogin], [SubmittalDate], [DeadlineDate], [AttyLogin], [OKforNewMatter], [ClientCategory], 
 [ClientName], [ClientMatter], [PracticeCode], [ClientAddress], [Priority], [OpposingCounsel], 
 [RelatedParties], [AdverseParties], [WorkBegunBy], [WorkBegunDate], [WorkCompletedBy], [CompletionDate], 
 [Insurance], [SkiResort], [LawFirm], [Communications], [Government], [IndianTribe], [Pharm], [AccountingFirm], 
 [OilGas], [LegalFees], [ElectricUtility], [NoneOfTheAbove], [Bankruptcy], [PartiesToMediationArbitration],
 [RelatedCoDefendants]--, [AffiliatesOfAdverseParties], [OtherClientAffiliates]
  )
  VALUES (
 @TypistLogin, @SubmittalDate, @DeadlineDate, @AttyLogin, @OKforNewMatter, @ClientCategory,
  @ClientName, @ClientMatter, @PracticeCode, @ClientAddress, @Priority, @OpposingCounsel, @RelatedParties,
   @AdverseParties, @WorkBegunBy, @WorkBegunDate, @WorkCompletedBy, @CompletionDate, @Insurance, @SkiResort,
    @LawFirm, @Communications, @Government, @IndianTribe, @Pharm, @AccountingFirm, @OilGas, @LegalFees,
     @ElectricUtility, @NoneOfTheAbove, @Bankruptcy, @PartiesToMediationArbitration, @RelatedCoDefendants--,
     --@AffiliatesOfAdverseParties, @OtherClientAffiliates
  );
SELECT @ReqID=@@IDENTITY;
END
ELSE
 UPDATE [dbo].[Requests]
 SET 
  [TypistLogin]=@TypistLogin, [SubmittalDate]=@SubmittalDate, [DeadlineDate]=@DeadlineDate, 
  [AttyLogin]=@AttyLogin, [OKforNewMatter]=@OKforNewMatter, [ClientCategory]=@ClientCategory, 
  [ClientName]=@ClientName, [ClientMatter]=@ClientMatter, [PracticeCode]=@PracticeCode, 
  [ClientAddress]=@ClientAddress, [Priority]=@Priority, [OpposingCounsel]=@OpposingCounsel, 
  [RelatedParties]=@RelatedParties, [AdverseParties]=@AdverseParties, [WorkBegunBy]=@WorkBegunBy, 
  [WorkBegunDate]=@WorkBegunDate, [WorkCompletedBy]=@WorkCompletedBy, [CompletionDate]=@CompletionDate,
   [Insurance]=@Insurance, [SkiResort]=@SkiResort, [LawFirm]=@LawFirm, [Communications]=@Communications,
    [Government]=@Government, [IndianTribe]=@IndianTribe, [Pharm]=@Pharm, [AccountingFirm]=@AccountingFirm,
     [OilGas]=@OilGas, [LegalFees]=@LegalFees, [ElectricUtility]=@ElectricUtility, [NoneOfTheAbove]=@NoneOfTheAbove,
     [Bankruptcy]=@Bankruptcy, [PartiesToMediationArbitration]=@PartiesToMediationArbitration,
     [RelatedCoDefendants]=@RelatedCoDefendants
     --, [AffiliatesOfAdverseParties]=@AffiliatesOfAdverseParties,
     --[OtherClientAffiliates]=@OtherClientAffiliates
 WHERE ReqID=@ReqID;

SELECT * FROM [dbo].[Requests] WHERE ReqID=@ReqID;

