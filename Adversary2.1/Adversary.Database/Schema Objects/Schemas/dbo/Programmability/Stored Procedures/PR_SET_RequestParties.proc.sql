﻿CREATE PROCEDURE [dbo].[PR_SET_RequestParties]
	@PartyID [int],
	@ReqID [int],
	@PartyName [nvarchar](255),
	@PartyRelationship [nvarchar](255)
AS
IF @PartyID < 1
BEGIN
 INSERT INTO [dbo].[RequestParties] (
 [ReqID], [PartyName], [PartyRelationship]
  )
  VALUES (
 @ReqID, @PartyName, @PartyRelationship
  );
SELECT @PartyID=@@IDENTITY;
END
ELSE
 UPDATE [dbo].[RequestParties]
 SET 
  [ReqID]=@ReqID, [PartyName]=@PartyName, [PartyRelationship]=@PartyRelationship
 WHERE PartyID=@PartyID;

SELECT * FROM [dbo].[RequestParties] WHERE PartyID=@PartyID;
