﻿CREATE PROCEDURE [dbo].[PR_SET_PartyAddressInfo]
	@PartyAddressID [int],
	@PartyID [int],
	@Address_Line_1 [nvarchar](50),
	@Address_Line_2 [nvarchar](50),
	@Address_Line_3 [nvarchar](50),
	@City [nvarchar](50),
	@State [nvarchar](50),
	@Zip [nvarchar](50),
	@Country [nvarchar](50),
	@Phone [nvarchar](50),
	@Fax [nvarchar](50),
	@EMail [nvarchar](50)
AS
IF @PartyAddressID < 1
BEGIN
 INSERT INTO [dbo].[PartyAddressInfo] (
 [PartyID], [Address_Line_1], [Address_Line_2], [Address_Line_3], [City], [State], [Zip], [Country], [Phone], [Fax], [EMail]
  )
  VALUES (
 @PartyID, @Address_Line_1, @Address_Line_2, @Address_Line_3, @City, @State, @Zip, @Country, @Phone, @Fax, @EMail
  );
SELECT @PartyAddressID=@@IDENTITY;
END
ELSE
 UPDATE [dbo].[PartyAddressInfo]
 SET 
  [PartyID]=@PartyID, [Address_Line_1]=@Address_Line_1, [Address_Line_2]=@Address_Line_2, [Address_Line_3]=@Address_Line_3, [City]=@City, [State]=@State, [Zip]=@Zip, [Country]=@Country, [Phone]=@Phone, [Fax]=@Fax, [EMail]=@EMail
 WHERE PartyAddressID=@PartyAddressID;

SELECT * FROM [dbo].[PartyAddressInfo] WHERE PartyAddressID=@PartyAddressID;
