﻿CREATE PROCEDURE [dbo].[PR_GET_Tracking]
	@TrackingID [int] = null,
	@ScrMemID [int] = null
AS
SELECT * FROM [dbo].[Tracking] WHERE
 ((@TrackingID is null) OR (TrackingID = @TrackingID)) AND
 ((@ScrMemID is null) OR (ScrMemID = @ScrMemID))
