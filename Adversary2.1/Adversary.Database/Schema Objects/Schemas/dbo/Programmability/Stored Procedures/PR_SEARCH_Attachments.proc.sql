﻿CREATE PROCEDURE [dbo].[PR_SEARCH_Attachments]
	@AttachmentID [int] = null,
	@ScrMemID [int] = null,
	@Description [nvarchar](400) = null,
	@FileName [nvarchar](200) = null
AS
SELECT * FROM [dbo].[Attachments] WHERE
 ((@AttachmentID is null) OR (AttachmentID = @AttachmentID)) AND 
 ((@ScrMemID is null) OR (ScrMemID = @ScrMemID)) AND 
 ((@Description is null) OR ([Description] = @Description)) AND 
 ((@FileName is null) OR ([FileName] = @FileName))
