﻿CREATE PROCEDURE [dbo].[PR_SET_ProBonoType]
	@ProBonoTypeID [int],
	@Name [nvarchar](50)
AS
IF @ProBonoTypeID < 1
BEGIN
 INSERT INTO [dbo].[ProBonoType] (
 [Name]
  )
  VALUES (
 @Name
  );
SELECT @ProBonoTypeID=@@IDENTITY;
END
ELSE
 UPDATE [dbo].[ProBonoType]
 SET 
  [Name]=@Name
 WHERE ProBonoTypeID=@ProBonoTypeID;

SELECT * FROM [dbo].[ProBonoType] WHERE ProBonoTypeID=@ProBonoTypeID;
