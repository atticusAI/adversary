﻿CREATE PROCEDURE [dbo].[PR_SET_Address]
	@ScrMemID [int],
	@AddressID [int],
	@Address1 [nvarchar](50),
	@Address2 [nvarchar](50),
	@Address3 [nvarchar](50),
	@City [nvarchar](50),
	@State [nvarchar](2),
	@Zip [nvarchar](12),
	@Country [nvarchar](50),
	@Phone [nvarchar](50),
	@AltPhone [nvarchar](50),
	@email [nvarchar](50),
	@Fax [nvarchar](50)
AS
IF @AddressID < 1
BEGIN
 INSERT INTO [dbo].[Address] (
 [ScrMemID], [Address1], [Address2], [Address3], [City], [State], [Zip], [Country], [Phone], [AltPhone], [email], [Fax]
  )
  VALUES (
 @ScrMemID, @Address1, @Address2, @Address3, @City, @State, @Zip, @Country, @Phone, @AltPhone, @email, @Fax
  );
SELECT @AddressID=@@IDENTITY;
END
ELSE
 UPDATE [dbo].[Address]
 SET 
  [ScrMemID]=@ScrMemID, [Address1]=@Address1, [Address2]=@Address2, [Address3]=@Address3, [City]=@City, [State]=@State, [Zip]=@Zip, [Country]=@Country, [Phone]=@Phone, [AltPhone]=@AltPhone, [email]=@email, [Fax]=@Fax
 WHERE AddressID=@AddressID;

SELECT * FROM [dbo].[Address] WHERE AddressID=@AddressID;
