﻿CREATE PROCEDURE [dbo].[PR_SET_FeeSplitStaff]
	@ScrMemID [int],
	@PersonnelID nvarchar(50)
AS
IF NOT EXISTS(SELECT * FROM FeeSplitStaff WHERE ScrMemID=@ScrMemID AND @PersonnelID=PersonnelID)
BEGIN
 INSERT INTO [dbo].[FeeSplitStaff] (
 [ScrMemID], [PersonnelID]
  )
  VALUES (
 @ScrMemID, @PersonnelID
  );

END

SELECT * FROM FeeSplitStaff WHERE ScrMemID=@ScrMemID AND @PersonnelID=PersonnelID
