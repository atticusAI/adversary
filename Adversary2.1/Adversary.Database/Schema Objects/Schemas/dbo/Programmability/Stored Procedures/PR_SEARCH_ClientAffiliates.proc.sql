﻿
CREATE PROC [dbo].[PR_SEARCH_ClientAffiliates]
(
	@NwClientID INT = NULL,
	@ClientAffiliateID INT = NULL,
	@CName varchar(255) = null,
	@LName varchar(50) = null,
	@FName varchar(50) = null,
	@MName varchar(50) = null,
	@ContactName nvarchar(50) = null,
	@ContactTitle nvarchar(50) = null,
	@RelationshipCode nvarchar(50) = null
)
AS

SELECT * FROM [dbo].[ClientAffiliates] WHERE
((@NwClientID IS NULL) OR (NwClientID = @NwClientID)) AND
((@ClientAffiliateID IS NULL) OR (ClientAffiliateID = @ClientAffiliateID)) AND
((@CName is null) OR (CName = @CName)) AND 
((@LName is null) OR (LName = @LName)) AND 
((@FName is null) OR (FName = @FName)) AND 
((@MName is null) OR (MName = @MName)) AND 
((@ContactName is null) OR (ContactName = @ContactName)) AND 
((@ContactTitle is null) OR (ContactTitle = @ContactTitle)) AND 
((@RelationshipCode IS NULL) OR (RelationshipCode = @RelationshipCode))