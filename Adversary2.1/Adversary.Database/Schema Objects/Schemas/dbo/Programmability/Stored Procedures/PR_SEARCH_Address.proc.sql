﻿CREATE PROCEDURE [dbo].[PR_SEARCH_Address]
	@ScrMemID [int] = null,
	@AddressID [int] = null,
	@Address1 [nvarchar](50) = null,
	@Address2 [nvarchar](50) = null,
	@Address3 [nvarchar](50) = null,
	@City [nvarchar](50) = null,
	@State [nvarchar](2) = null,
	@Zip [nvarchar](12) = null,
	@Country [nvarchar](50) = null,
	@Phone [nvarchar](50) = null,
	@AltPhone [nvarchar](50) = null,
	@email [nvarchar](50) = null,
	@Fax [nvarchar](50) = null
AS
SELECT * FROM [dbo].[Address] WHERE
 ((@ScrMemID is null) OR (ScrMemID = @ScrMemID)) AND 
 ((@AddressID is null) OR (AddressID = @AddressID)) AND 
 ((@Address1 is null) OR (Address1 = @Address1)) AND 
 ((@Address2 is null) OR (Address2 = @Address2)) AND 
 ((@Address3 is null) OR (Address3 = @Address3)) AND 
 ((@City is null) OR (City = @City)) AND 
 ((@State is null) OR (State = @State)) AND 
 ((@Zip is null) OR (Zip = @Zip)) AND 
 ((@Country is null) OR (Country = @Country)) AND 
 ((@Phone is null) OR (Phone = @Phone)) AND 
 ((@AltPhone is null) OR (AltPhone = @AltPhone)) AND 
 ((@email is null) OR (email = @email)) AND 
 ((@Fax is null) OR (Fax = @Fax))
