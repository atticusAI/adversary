﻿CREATE PROCEDURE [dbo].[PR_SET_ClientContact]
	@ContactID [int],
	@ContactFName [nvarchar](50),
	@ContactMName [nvarchar](50),
	@ContactLName [nvarchar](50),
	@ContactTitle [nvarchar](50),
	@ScrMemID [int]
AS
IF @ContactID < 1
BEGIN
 INSERT INTO [dbo].[ClientContact] (
 [ContactFName], [ContactMName], [ContactLName], [ContactTitle], [ScrMemID]
  )
  VALUES (
 @ContactFName, @ContactMName, @ContactLName, @ContactTitle, @ScrMemID
  );
SELECT @ContactID=@@IDENTITY;
END
ELSE
 UPDATE [dbo].[ClientContact]
 SET 
  [ContactFName]=@ContactFName, [ContactMName]=@ContactMName, [ContactLName]=@ContactLName, [ContactTitle]=@ContactTitle, [ScrMemID]=@ScrMemID
 WHERE ContactID=@ContactID;

SELECT * FROM [dbo].[ClientContact] WHERE ContactID=@ContactID;
