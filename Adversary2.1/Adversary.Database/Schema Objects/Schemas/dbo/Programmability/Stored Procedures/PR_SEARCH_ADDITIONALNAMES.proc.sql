﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PR_SEARCH_ADDITIONALNAMES] 
	@AdditionalNameID INT = null,
	@ClientNumber varchar(50) = null,
	@StartDate datetime = null,
	@EndDate datetime = null,
	@UploaderPersonID varchar(20) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		AdditionalNameID,
		ClientNumber,
		Comments,
		UploadDate,
		IsComplete,
		[FileName],
		UploaderPersonID
	FROM AdditionalNames
	WHERE
		(@ClientNumber is null OR @ClientNumber = ClientNumber)
		AND
		(@AdditionalNameID is null OR @AdditionalNameID = AdditionalNameID)
		AND
		(@StartDate is NULL OR @StartDate <= UploadDate)
		AND
		(@EndDate IS NULL OR @EndDate >= UploadDate)
END
