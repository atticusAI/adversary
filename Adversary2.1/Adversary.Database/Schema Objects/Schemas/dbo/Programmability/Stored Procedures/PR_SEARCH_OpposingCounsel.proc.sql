﻿CREATE PROCEDURE [dbo].[PR_SEARCH_OpposingCounsel]
	@opposingID [int] = null,
	@LawfirmName [nvarchar](150) = null,
	@LawyerName [nvarchar](50) = null,
	@ScrMemID [int] = null
AS
SELECT * FROM [dbo].[OpposingCounsel] WHERE
 ((@opposingID is null) OR (opposingID = @opposingID)) AND 
 ((@LawfirmName is null) OR (LawfirmName = @LawfirmName)) AND 
 ((@LawyerName is null) OR (LawyerName = @LawyerName)) AND 
 ((@ScrMemID is null) OR (ScrMemID = @ScrMemID))
