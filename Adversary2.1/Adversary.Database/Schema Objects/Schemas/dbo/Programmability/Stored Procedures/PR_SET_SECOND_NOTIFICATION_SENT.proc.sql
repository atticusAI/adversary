﻿
CREATE proc [dbo].[PR_SET_SECOND_NOTIFICATION_SENT]
(@NOTIFICATION_ID INT)
AS

UPDATE dbo.Notifications SET
	Notification1Sent = 1,
	Notification1Date = GETDATE()
WHERE
	NotificationID = @NOTIFICATION_ID

