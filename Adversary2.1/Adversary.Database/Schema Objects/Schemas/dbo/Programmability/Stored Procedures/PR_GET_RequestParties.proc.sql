﻿CREATE PROCEDURE [dbo].[PR_GET_RequestParties]
	@PartyID [int] = null,
	@ReqID [int] = null
AS
SELECT * FROM [dbo].[RequestParties] WHERE
 ((@PartyID is null) OR (PartyID = @PartyID)) AND 
 ((@ReqID is null) OR (ReqID = @ReqID))
