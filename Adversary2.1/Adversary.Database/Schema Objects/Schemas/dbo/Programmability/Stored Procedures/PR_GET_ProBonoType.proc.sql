﻿CREATE PROCEDURE [dbo].[PR_GET_ProBonoType]
	@ProBonoTypeID [int] = null
AS
SELECT * FROM [dbo].[ProBonoType] WHERE
 ((@ProBonoTypeID is null) OR (ProBonoTypeID = @ProBonoTypeID))
