﻿CREATE PROCEDURE [dbo].[PR_DELETE_Staffing]
	@ScrMemID [int],
	@PersonnelID nvarchar(50)
AS
DELETE FROM [dbo].[Staffing] 
WHERE ScrMemID=@ScrMemID AND PersonnelID=@PersonnelID
