﻿CREATE PROCEDURE [dbo].[PR_GET_AdminPracCodes]
	@UserID [int] = null
AS
SELECT * FROM [dbo].[AdminPracCodes] WHERE
 ((@UserID is null) OR (UserID = @UserID))
