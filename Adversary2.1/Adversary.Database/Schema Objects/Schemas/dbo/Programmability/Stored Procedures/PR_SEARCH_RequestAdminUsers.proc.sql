﻿CREATE PROCEDURE [dbo].[PR_SEARCH_RequestAdminUsers]
	@AdminUserID [int] = null,
	@AdminLoginName [nvarchar](50) = null,
	@AdminEmail [nvarchar](50) = null,
	@SortOrder [int] = null
AS
SELECT * FROM [dbo].[RequestAdminUsers] WHERE
 ((@AdminUserID is null) OR (AdminUserID = @AdminUserID)) AND 
 ((@AdminLoginName is null) OR (AdminLoginName = @AdminLoginName)) AND 
 ((@AdminEmail is null) OR (AdminEmail = @AdminEmail)) AND 
 ((@SortOrder is null) OR (SortOrder = @SortOrder))
