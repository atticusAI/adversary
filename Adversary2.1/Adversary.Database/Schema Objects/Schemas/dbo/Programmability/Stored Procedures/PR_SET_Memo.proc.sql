﻿

CREATE PROCEDURE [dbo].[PR_SET_Memo]
	@ScrMemID [int],
	@ScrMemType [int],
	@RefDate [datetime]=null,
	@InputDate [money]=null,
	@AttEntering [nvarchar](50)=null,
	@PersonnelID [nvarchar](50)=null,
	@RespAttID [nvarchar](4)=null,
	@PreScreen [bit]=null,
	@PreScreenDesc [nvarchar](max)=null,
	@PreScreenNames [tinyint]=null,
	@PreScreenNameList [nvarchar](max)=null,
	@ClearedConflicts [bit]=null,
	@EstFees [nvarchar](50)=null,
	@WorkDesc [nvarchar](max)=null,
	@FeeSplit [bit]=null,
	@FeeSplitDesc [nvarchar](max)=null,
	@OrigOffice [nvarchar](50)=null,
	@PracCode [nvarchar](5)=null,
	@ConflictReport [bit]=null,
	@ConflictDesc [nvarchar](max)=null,
	@EngageLetter [bit],
	@EngageDesc [nvarchar](max)=null,
	@DocumentID [nvarchar](50)=null,
	@Imanage [bit]=null,
	@DocumentOffice [nvarchar](50)=null,
	@ContigDocumentID [nvarchar](50)=null,
	@ContigDocumentOffice [nvarchar](50)=null,
	@Retainer [bit]=null,
	@RetainerDescNo [nvarchar](max)=null,
	@RetainerDescYes [nvarchar](max)=null,
	@Confidential [bit]=null,
	@Company [bit]=null,
	@Individual [bit]=null,
	@ClientNumber [nvarchar](10)=null,
	@EmpEntering [nvarchar](50)=null,
	@EmpClient [nvarchar](50)=null,
	@EstHours [int]=null,
	@RetainerDesc [nvarchar](50)=null,
	@ContingFee [bit]=null,
	@ProBonoTypeID [int]=null,
	@HHLegalWorkID [int]=null,
	@MatterName [nvarchar](255)=null,
	@RejectDesc [nvarchar](max)=null,
    @ProBonoReason [nvarchar](max)=null,
	@NewMatterOnly [nvarchar](50)=null,
	@OppQuestion [bit]=null,
	@PatentMatter [nvarchar](255)=null,
	@TreasuryList [bit]=null,
	@CommerceList [bit]=null,
	@WrittenPolicies [bit]=null,
	@PendingBankruptcy [bit]=null,
	@NDrive [bit]=null,
	@NDriveType int,
	@RespAttOfficeCode [nvarchar](50)=null,
	@NoRejectionLetterReason [nvarchar](max)=null,
	@NoRejectionLetter [bit]= null,
	@DocketingAtty [nvarchar](50)=null,
	@DocketingStaff [nvarchar](50)=null
	
AS
IF @ScrMemID < 1
BEGIN
 INSERT INTO [dbo].[Memo] (
 [ScrMemType], [RefDate], [InputDate], [AttEntering], [PersonnelID], [RespAttID], [PreScreen], [PreScreenDesc], 

[PreScreenNames], [PreScreenNameList], [ClearedConflicts], [EstFees], [WorkDesc], [FeeSplit], [FeeSplitDesc], 

[OrigOffice], [PracCode], [ConflictReport], [ConflictDesc], [EngageLetter], [EngageDesc], [DocumentID], [Imanage], 

[DocumentOffice], [ContigDocumentID], [ContigDocumentOffice], [Retainer], [RetainerDescNo], [RetainerDescYes], 

[Confidential], [Company], [Individual], [ClientNumber], [EmpEntering], [EmpClient], [EstHours], [RetainerDesc], 

[ContingFee], [ProBonoTypeID], [HHLegalWorkID], [MatterName], [RejectDesc], [ProBonoReason], [NewMatterOnly], 

[OppQuestion], [PatentMatter], [TreasuryList], [CommerceList], [WrittenPolicies], [PendingBankruptcy], [NDrive], 

[NDriveType], [RespAttOfficeCode], [NoRejectionLetterReason], [NoRejectionLetter], [DocketingAtty], [DocketingStaff]
  )
  VALUES (
 @ScrMemType, @RefDate, @InputDate, @AttEntering, @PersonnelID, @RespAttID, @PreScreen, @PreScreenDesc, 

@PreScreenNames, @PreScreenNameList, @ClearedConflicts, @EstFees, @WorkDesc, @FeeSplit, @FeeSplitDesc, @OrigOffice, 

@PracCode, @ConflictReport, @ConflictDesc, @EngageLetter, @EngageDesc, @DocumentID, @Imanage, @DocumentOffice, 

@ContigDocumentID, @ContigDocumentOffice, @Retainer, @RetainerDescNo, @RetainerDescYes, @Confidential, @Company, 

@Individual, @ClientNumber, @EmpEntering, @EmpClient, @EstHours, @RetainerDesc, @ContingFee, @ProBonoTypeID, 

@HHLegalWorkID, @MatterName, @RejectDesc, @ProBonoReason, @NewMatterOnly, @OppQuestion, @PatentMatter, 

@TreasuryList, @CommerceList, @WrittenPolicies, @PendingBankruptcy, @NDrive, @NDriveType, @RespAttOfficeCode, @NoRejectionLetterReason, 

@NoRejectionLetter, @DocketingAtty, @DocketingStaff
  );
SELECT @ScrMemID=@@IDENTITY;
END
ELSE
 UPDATE [dbo].[Memo]
 SET 
  [ScrMemType]=@ScrMemType, [RefDate]=@RefDate, [InputDate]=@InputDate, [AttEntering]=@AttEntering, [PersonnelID]

=@PersonnelID, [RespAttID]=@RespAttID, [PreScreen]=@PreScreen, [PreScreenDesc]=@PreScreenDesc, [PreScreenNames]

=@PreScreenNames, [PreScreenNameList]=@PreScreenNameList, [ClearedConflicts]=@ClearedConflicts, [EstFees]=@EstFees, 

[WorkDesc]=@WorkDesc, [FeeSplit]=@FeeSplit, [FeeSplitDesc]=@FeeSplitDesc, [OrigOffice]=@OrigOffice, [PracCode]

=@PracCode, [EngageLetter]=@EngageLetter, [EngageDesc]=@EngageDesc, [ConflictReport]=@ConflictReport, 

[ConflictDesc]=@ConflictDesc, [DocumentID]=@DocumentID, [Imanage]=@Imanage, [DocumentOffice]=@DocumentOffice, 

[ContigDocumentID]=@ContigDocumentID, [ContigDocumentOffice]=@ContigDocumentOffice, [Retainer]=@Retainer, 

[RetainerDescNo]=@RetainerDescNo, [RetainerDescYes]=@RetainerDescYes, [Confidential]=@Confidential, [Company]

=@Company, [Individual]=@Individual, [ClientNumber]=@ClientNumber, [EmpEntering]=@EmpEntering, [EmpClient]

=@EmpClient, [EstHours]=@EstHours, [RetainerDesc]=@RetainerDesc, [ContingFee]=@ContingFee, [ProBonoTypeID]

=@ProBonoTypeID, [HHLegalWorkID]=@HHLegalWorkID, [MatterName]=@MatterName, [RejectDesc]=@RejectDesc, 

[ProBonoReason]=@ProBonoReason, [NewMatterOnly]=@NewMatterOnly, [OppQuestion]=@OppQuestion, [PatentMatter]

=@PatentMatter, [TreasuryList]=@TreasuryList, [CommerceList]=@CommerceList, [WrittenPolicies]=@WrittenPolicies, 

[PendingBankruptcy]=@PendingBankruptcy, [NDrive]=@NDrive, [NDriveType]=@NDriveType, [RespAttOfficeCode]=@RespAttOfficeCode, 

NoRejectionLetterReason=@NoRejectionLetterReason, NoRejectionLetter=@NoRejectionLetter, DocketingAtty=@DocketingAtty, DocketingStaff=@DocketingStaff
 
 
 WHERE ScrMemID=@ScrMemID;

SELECT * FROM [dbo].[Memo] WHERE ScrMemID=@ScrMemID;
