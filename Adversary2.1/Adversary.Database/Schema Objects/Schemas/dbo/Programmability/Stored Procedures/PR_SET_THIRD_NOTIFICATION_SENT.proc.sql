﻿
CREATE proc [dbo].[PR_SET_THIRD_NOTIFICATION_SENT]
(@NOTIFICATION_ID INT)
AS

UPDATE dbo.Notifications SET
	Notification2Sent = 1,
	Notification2Date = GETDATE()
WHERE
	NotificationID = @NOTIFICATION_ID

