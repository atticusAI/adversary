﻿


CREATE PROCEDURE [dbo].[PR_SET_Party]
	@ScrMemID [int],
	@PartyType [nvarchar](MAX),
	@PartyFName [nvarchar](MAX),
	@PartyMName [nvarchar](MAX),
	@PartyLName [nvarchar](MAX),
	@PartyOrganization [nvarchar](MAX),
	@PartyRelationshipName [nvarchar](MAX),
	@PartyRelationshipCode [nvarchar](50),
	@PartyID [int],
	@PC [int]
AS
IF @PartyID < 1
BEGIN
 INSERT INTO [dbo].[Party] (
 [ScrMemID], [PartyType], [PartyFName], [PartyMName], [PartyLName], 
 [PartyOrganization], [PartyRelationshipName], [PartyRelationshipCode], [PC]  )
  VALUES (
 @ScrMemID, @PartyType, @PartyFName, @PartyMName, @PartyLName, @PartyOrganization, 
 @PartyRelationshipName, @PartyRelationshipCode, @PC
  );
SELECT @PartyID=@@IDENTITY;
END
ELSE
 UPDATE [dbo].[Party]
 SET 
  [ScrMemID]=@ScrMemID, [PartyType]=@PartyType, [PartyFName]=@PartyFName, 
  [PartyMName]=@PartyMName, [PartyLName]=@PartyLName, [PartyOrganization]=@PartyOrganization, 
  [PartyRelationshipName]=@PartyRelationshipName, [PartyRelationshipCode]=@PartyRelationshipCode, 
  [PC]=@PC
 WHERE PartyID=@PartyID;

SELECT * FROM [dbo].[Party] WHERE PartyID=@PartyID;

