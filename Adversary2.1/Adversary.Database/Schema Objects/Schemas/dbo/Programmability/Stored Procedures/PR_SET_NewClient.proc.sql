﻿CREATE PROCEDURE [dbo].[PR_SET_NewClient]
	@ScrMemID [int],
	@NwClientID [int],
	@CName [nvarchar](255),
	@LName [nvarchar](30),
	@FName [nvarchar](30),
	@MName [nvarchar](50),
	@ContactName [nvarchar](50),
	@ContactTitle [nvarchar](50),
	@OriginationComments [nvarchar](max),
	@ClientHasAffiliates bit = 0
AS
IF @NwClientID < 1
BEGIN
 INSERT INTO [dbo].[NewClient] (
 [ScrMemID], [CName], [LName], [FName], [MName], [ContactName], [ContactTitle], [OriginationComments],
 [ClientHasAffiliates]
  )
  VALUES (
 @ScrMemID, @CName, @LName, @FName, @MName, @ContactName, @ContactTitle, @OriginationComments, @ClientHasAffiliates
  );
SELECT @NwClientID=@@IDENTITY;
END
ELSE
 UPDATE [dbo].[NewClient]
 SET 
  [ScrMemID]=@ScrMemID, [CName]=@CName, [LName]=@LName, [FName]=@FName, 
  [MName]=@MName, [ContactName]=@ContactName, 
  [ContactTitle]=@ContactTitle, [OriginationComments]=@OriginationComments,
  [ClientHasAffiliates]=@ClientHasAffiliates
 WHERE NwClientID=@NwClientID;

SELECT * FROM [dbo].[NewClient] WHERE NwClientID=@NwClientID;
