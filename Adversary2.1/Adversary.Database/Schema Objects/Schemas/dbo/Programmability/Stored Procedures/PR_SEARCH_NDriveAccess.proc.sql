﻿
/* SEARCH PROC */

CREATE PROCEDURE [dbo].[PR_SEARCH_NDriveAccess]
	@ScrMemID [int] = null,
	@PersonnelID [nvarchar](50) = null
AS
SELECT * FROM [dbo].[NDriveAccess] WHERE
 ((@ScrMemID is null) OR (ScrMemID = @ScrMemID)) AND 
 ((@PersonnelID is null) OR (PersonnelID = @PersonnelID))
