﻿
CREATE PROCEDURE [dbo].[PR_SEARCH_DocketingTeam]
	@ScrMemID [int] = null,
	@PersonnelID [nvarchar](50) = null
AS
SELECT * FROM [dbo].[DocketingTeam] WHERE
 ((@ScrMemID is null) OR (ScrMemID = @ScrMemID)) AND 
 ((@PersonnelID is null) OR (PersonnelID = @PersonnelID))
