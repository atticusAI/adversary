﻿


/** Get Open Memos Proc **/

CREATE PROCEDURE [dbo].[PR_GET_OpenMemos]
	@StatusTypeID int = null,
	@UserID int = null,
	@SortNewestFirst bit = null
AS


IF @SortNewestFirst IS NULL
	SET @SortNewestFirst = 1

-- DECLARE @Mult AS int

--SET @Mult = CASE @SortNewestFirst 
--				WHEN 1 THEN -1 
--				ELSE 1 
--				END 

SELECT     TOP 500 *
FROM       Memo INNER JOIN
           Tracking ON Memo.ScrMemID = Tracking.ScrMemID
WHERE	((@UserID is null) OR 
		 (Tracking.APUserID = @UserId) OR 
		 (Tracking.PGMUserID = @UserId) OR 
		 (Tracking.AdversaryUserID = @UserId)) AND 
		(((@StatusTypeID is null) AND 
		  (Tracking.StatusTypeID IS NOT NULL) AND (Tracking.StatusTypeID > 0) AND (Tracking.StatusTypeID < 100)) OR
		 (Tracking.StatusTypeID = @StatusTypeID))
--ORDER BY @Mult * Memo.ScrMemID
ORDER BY 
	CASE @SortNewestFirst WHEN 1 THEN Tracking.SubmittedOn END ASC,
	CASE @SortNewestFirst WHEN 0 THEN Tracking.SubmittedOn END DESC						
