﻿
CREATE PROCEDURE [dbo].[PR_DELETE_DocketingTeam]
	@ScrMemID [int],
	@PersonnelID nvarchar(50)
AS
DELETE FROM [dbo].[DocketingTeam] 
WHERE ScrMemID=@ScrMemID AND PersonnelID=@PersonnelID
