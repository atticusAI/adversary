﻿CREATE PROCEDURE [dbo].[PR_SEARCH_AdminPracCodes]
	@UserID [int] = null,
	@PracCode [nvarchar](5) = null,
	@IsPrimary [bit] = null
AS
SELECT * FROM [dbo].[AdminPracCodes] WHERE
 ((@UserID is null) OR (UserID = @UserID)) AND 
 ((@PracCode is null) OR (PracCode = @PracCode)) AND
 ((@IsPrimary is null) OR (IsPrimary = @IsPrimary))
