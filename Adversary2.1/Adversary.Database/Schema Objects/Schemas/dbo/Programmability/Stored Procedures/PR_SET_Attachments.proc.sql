﻿CREATE PROCEDURE [dbo].[PR_SET_Attachments]
	@AttachmentID [int],
	@ScrMemID [int],
	@Description [nvarchar](400),
	@FileName [nvarchar](200)
	/*@FileData [varbinary](max)*/
AS
IF @AttachmentID < 1
BEGIN
 INSERT INTO [dbo].[Attachments] (
 [ScrMemID], [Description], [FileName]/*, [FileData]*/
  )
  VALUES (
 @ScrMemID, @Description, @FileName /*, @FileData*/
  );
SELECT @AttachmentID=@@IDENTITY;
END
ELSE
 UPDATE [dbo].[Attachments]
 SET 
  [ScrMemID]=@ScrMemID, [Description]=@Description, [FileName]=@FileName /*, [FileData]=@FileData*/
 WHERE AttachmentID=@AttachmentID;

SELECT * FROM [dbo].[Attachments] WHERE AttachmentID=@AttachmentID;
