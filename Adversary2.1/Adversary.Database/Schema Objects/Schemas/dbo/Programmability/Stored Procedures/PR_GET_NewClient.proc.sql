﻿CREATE PROCEDURE [dbo].[PR_GET_NewClient]
	@NwClientID [int] = null,
	@ScrMemID [int] = null
AS
SELECT * FROM [dbo].[NewClient] WHERE
 ((@NwClientID is null) OR (NwClientID = @NwClientID)) AND 
 ((@ScrMemID is null) OR (ScrMemID = @ScrMemID))
