﻿CREATE PROCEDURE [dbo].[PR_SEARCH_SummaryOrder]
	@PageID [int] = null,
	@TrackNumber [int] = null,
	@SummaryOrder [int] = null
AS
SELECT * FROM [dbo].[SummaryOrder] WHERE
 ((@PageID is null) OR (PageID = @PageID)) AND 
 ((@TrackNumber is null) OR (TrackNumber = @TrackNumber)) AND 
 ((@SummaryOrder is null) OR (SummaryOrder = @SummaryOrder))
