﻿CREATE PROCEDURE [dbo].[PR_SEARCH_ClientMatterNumbers]
	@ClientMatterID[int] = null,
	@ScrMemID [int] = null,
	@ClientMatterNumber [nvarchar](150) = null
AS
SELECT DISTINCT Memo.ScrMemID, Memo.*, ClientMatterNumbers.*
FROM Memo LEFT OUTER JOIN ClientMatterNumbers
ON Memo.ScrMemID=ClientMatterNumbers.ScrMemID
WHERE
 ((@ClientMatterID is null) OR (ClientMatterID = @ClientMatterID)) AND 
 ((@ScrMemID is null) OR (ClientMatterNumbers.ScrMemID = @ScrMemID)) AND 
 ((@ClientMatterNumber is null) OR (ClientMatterNumber like @ClientMatterNumber + '%'))
