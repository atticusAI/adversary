﻿CREATE PROCEDURE [dbo].[PR_SET_Staffing]
	@ScrMemID [int],
	@PersonnelID nvarchar(50)
AS
IF NOT EXISTS(SELECT * FROM Staffing WHERE ScrMemID=@ScrMemID AND @PersonnelID=PersonnelID)
BEGIN
 INSERT INTO [dbo].[Staffing] (
 [ScrMemID], [PersonnelID]
  )
  VALUES (
 @ScrMemID, @PersonnelID
  );

END

SELECT * FROM Staffing WHERE ScrMemID=@ScrMemID AND @PersonnelID=PersonnelID
