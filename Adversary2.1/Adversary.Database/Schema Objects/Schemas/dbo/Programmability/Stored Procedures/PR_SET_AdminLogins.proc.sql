﻿CREATE PROCEDURE [dbo].[PR_SET_AdminLogins]
	@UserID [int],
	@Login [varchar](50),
	@Email [varchar](50),
	@Office [int],
	@IsAdversary [bit],
	@IsAdversaryAdmin [bit],
	@IsAP [bit],
	@IsPrimaryAP [bit],
	@IsPGM [bit],
	@SortOrder [int]
AS
IF @UserID < 1
BEGIN
 INSERT INTO [dbo].[AdminLogins] (
 [Login], [Email], [Office], [IsAdversary], [IsAdversaryAdmin], [IsAP], [IsPGM], [IsPrimaryAP], [SortOrder]
  )
  VALUES (
 @Login, @Email, @Office, @IsAdversary, @IsAdversaryAdmin, @IsAP, @IsPGM, @IsPrimaryAP, @SortOrder
  );
SELECT @UserID=@@IDENTITY;
END
ELSE
 UPDATE [dbo].[AdminLogins]
 SET 
  [Login]=@Login, [Email]=@Email, [Office]=@Office, [IsAdversary]=@IsAdversary, [IsAdversaryAdmin]=@IsAdversaryAdmin, [IsAP]=@IsAP, [IsPrimaryAP]=@IsPrimaryAP, [IsPGM]=@IsPGM, [SortOrder]=@SortOrder
 WHERE UserID=@UserID;

SELECT * FROM [dbo].[AdminLogins] WHERE UserID=@UserID;
