﻿CREATE PROCEDURE [dbo].[PR_SET_Billing]
	@ScrMemID [int],
	@BillingID [int],
	@SpecialFee [nvarchar](max),
	@BillInstructions [nvarchar](max),
	@BillFormat [nvarchar](50),
	@ClientLevelBill [bit],
	@LateInterest [bit]
AS
IF @BillingID < 1
BEGIN
 INSERT INTO [dbo].[Billing] (
 [ScrMemID], [SpecialFee], [BillInstructions], [BillFormat], [ClientLevelBill], [LateInterest]
  )
  VALUES (
 @ScrMemID, @SpecialFee, @BillInstructions, @BillFormat, @ClientLevelBill, @LateInterest
  );
SELECT @BillingID=@@IDENTITY;
END
ELSE
 UPDATE [dbo].[Billing]
 SET 
  [ScrMemID]=@ScrMemID, [SpecialFee]=@SpecialFee, [BillInstructions]=@BillInstructions, [BillFormat]=@BillFormat, [ClientLevelBill]=@ClientLevelBill, [LateInterest]=@LateInterest
 WHERE BillingID=@BillingID;

SELECT * FROM [dbo].[Billing] WHERE BillingID=@BillingID;
