﻿CREATE PROCEDURE [dbo].[PR_SET_ARApprovalUser]
	@ApprovalUserID [int] = null,
	@UserName [nvarchar](150) = null,
	@EmailAddress [nvarchar](100) = null
AS
IF @ApprovalUserID < 1
BEGIN
 INSERT INTO [dbo].[ARApprovalUser] (
 [UserName], [EmailAddress]
  )
  VALUES (
 @UserName, @EmailAddress
  );
SELECT @ApprovalUserID=@@IDENTITY;
END
ELSE
 UPDATE [dbo].[ARApprovalUser]
 SET 
  [UserName]=@UserName, [EmailAddress]=@EmailAddress
 WHERE ApprovalUserID=@ApprovalUserID;

SELECT * FROM [dbo].[ARApprovalUser] WHERE ApprovalUserID=@ApprovalUserID;
