﻿CREATE PROCEDURE [dbo].[PR_GET_Address]
	@AddressID [int] = null,
	@ScrMemID [int] = null
AS
SELECT * FROM [dbo].[Address] WHERE
 ((@AddressID is null) OR (AddressID = @AddressID)) AND 
 ((@ScrMemID is null) OR (ScrMemID = @ScrMemID))
