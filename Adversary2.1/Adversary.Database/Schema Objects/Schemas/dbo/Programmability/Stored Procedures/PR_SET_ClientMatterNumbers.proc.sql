﻿CREATE PROCEDURE [dbo].[PR_SET_ClientMatterNumbers]
	@ScrMemID [int],
	@ClientMatterNumber nvarchar(150)
AS
IF NOT EXISTS(SELECT * FROM ClientMatterNumbers WHERE ScrMemID=@ScrMemID AND ClientMatterNumber=@ClientMatterNumber)
BEGIN
 INSERT INTO [dbo].[ClientMatterNumbers] (
 [ScrMemID], [ClientMatterNumber]
  )
  VALUES (
 @ScrMemID, @ClientMatterNumber
  );

END

SELECT * FROM ClientMatterNumbers WHERE ScrMemID=@ScrMemID AND ClientMatterNumber=@ClientMatterNumber
