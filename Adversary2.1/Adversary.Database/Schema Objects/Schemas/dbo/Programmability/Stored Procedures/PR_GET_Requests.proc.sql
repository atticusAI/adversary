﻿

/* RETURN TOP 100 Adversary Requests */

CREATE PROCEDURE [dbo].[PR_GET_Requests]
	@ReqID [int] = null,
	@SubmittalDate [datetime] = null,
	@WorkBegunBy nvarchar(50) = null,
	@TypistLogin nvarchar(50) = null,
	@PendingOnly  bit = null
AS
SELECT TOP 100 * FROM [dbo].[Requests] WHERE
 ((@ReqID is null) OR (ReqID = @ReqID)) AND
 ((@SubmittalDate is null) OR (SubmittalDate >= @SubmittalDate)) AND
 ((@WorkBegunBy is null) OR (WorkBegunBy = @WorkBegunBy)) AND   
 ((@TypistLogin is null) OR (TypistLogin = @TypistLogin)) AND
 ((@PendingOnly is null) OR (@PendingOnly = 0) OR (@PendingOnly = 1 AND 
													(CompletionDate IS NULL OR 
													 WorkCompletedBy IS NULL)))
ORDER BY SubmittalDate ASC