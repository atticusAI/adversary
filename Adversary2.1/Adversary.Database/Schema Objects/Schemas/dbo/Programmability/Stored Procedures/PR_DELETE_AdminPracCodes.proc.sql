﻿CREATE PROCEDURE [dbo].[PR_DELETE_AdminPracCodes]
	@UserID [int],
	@PracCode [nvarchar](5)=null
AS


DELETE FROM [dbo].[AdminPracCodes] WHERE UserID=@UserID AND (@PracCode IS Null OR
															 PracCode = @PracCode)
