﻿CREATE PROCEDURE [dbo].[PR_GET_OpposingCounsel]
	@opposingID [int] = null,
	@ScrMemID [int] = null
AS
SELECT * FROM [dbo].[OpposingCounsel] WHERE
 ((@opposingID is null) OR (opposingID = @opposingID)) AND 
 ((@ScrMemID is null) OR (ScrMemID = @ScrMemID))
