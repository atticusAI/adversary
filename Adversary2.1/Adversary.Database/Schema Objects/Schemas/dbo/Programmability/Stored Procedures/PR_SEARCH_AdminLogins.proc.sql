﻿CREATE PROCEDURE [dbo].[PR_SEARCH_AdminLogins]
	@UserID [int] = null,
	@Login [varchar](50) = null,
	@Email [varchar](50) = null,
	@Office [int] = null,
	@IsAdversary [bit] = null,
	@IsAdversaryAdmin [bit] = null,
	@IsAP [bit] = null,
	@IsPrimaryAP [bit] = null,
	@IsPGM [bit] = null,
	@SortOrder [int] = null
AS
SELECT * FROM [dbo].[AdminLogins] WHERE
 ((@UserID is null) OR (UserID = @UserID)) AND 
 ((@Login is null) OR (Login = @Login)) AND 
 ((@Email is null) OR (Email = @Email)) AND 
 ((@Office is null) OR (Office = @Office)) AND 
 ((@IsAdversary is null) OR (IsAdversary = @IsAdversary)) AND 
 ((@IsAdversaryAdmin is null) OR (IsAdversaryAdmin = @IsAdversaryAdmin)) AND 
 ((@IsAP is null) OR (IsAP = @IsAP)) AND 
 ((@IsPrimaryAP is null) OR (IsPrimaryAP = @IsPrimaryAP)) AND 
 ((@IsPGM is null) OR (IsPGM = @IsPGM)) AND 
 ((@SortOrder is null) OR (SortOrder = @SortOrder))
