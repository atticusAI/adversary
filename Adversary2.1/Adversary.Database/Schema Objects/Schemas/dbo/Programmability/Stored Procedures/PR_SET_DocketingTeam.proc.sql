﻿

CREATE PROCEDURE [dbo].[PR_SET_DocketingTeam]
	@ScrMemID [int],
	@PersonnelID nvarchar(50)
AS
IF NOT EXISTS(SELECT * FROM DocketingTeam WHERE ScrMemID=@ScrMemID AND @PersonnelID=PersonnelID)
BEGIN
 INSERT INTO [dbo].[DocketingTeam] (
 [ScrMemID], [PersonnelID]
  )
  VALUES (
 @ScrMemID, @PersonnelID
  );

END

SELECT * FROM DocketingTeam WHERE ScrMemID=@ScrMemID AND @PersonnelID=PersonnelID

