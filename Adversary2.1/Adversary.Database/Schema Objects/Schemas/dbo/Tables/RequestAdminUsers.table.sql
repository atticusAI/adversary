﻿CREATE TABLE [dbo].[RequestAdminUsers] (
    [AdminUserID]    INT           IDENTITY (1, 1) NOT NULL,
    [AdminLoginName] NVARCHAR (50) NULL,
    [AdminEmail]     NVARCHAR (50) NULL,
    [SortOrder]      INT           NULL
);

