﻿CREATE TABLE [dbo].[Billing] (
    [ScrMemID]         INT            NOT NULL,
    [BillingID]        INT            IDENTITY (1, 1) NOT NULL,
    [SpecialFee]       NVARCHAR (MAX) NULL,
    [BillInstructions] NVARCHAR (MAX) NULL,
    [BillFormat]       NVARCHAR (50)  NULL,
    [ClientLevelBill]  BIT            NULL,
    [LateInterest]     BIT            NULL,
    CONSTRAINT [PK_Billing] PRIMARY KEY CLUSTERED ([BillingID] ASC),
    CONSTRAINT [FK_Billing_Memo] FOREIGN KEY ([ScrMemID]) REFERENCES [dbo].[Memo] ([ScrMemID])
);



