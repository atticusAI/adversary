﻿CREATE TABLE [dbo].[NDriveAccess] (
    [ScrMemID]       INT           NULL,
    [PersonnelID]    NVARCHAR (50) NOT NULL,
    [NDriveAccessID] INT           IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_NDriveAccess] PRIMARY KEY CLUSTERED ([NDriveAccessID] ASC),
    CONSTRAINT [FK_NDriveAccess_Memo] FOREIGN KEY ([ScrMemID]) REFERENCES [dbo].[Memo] ([ScrMemID])
);



