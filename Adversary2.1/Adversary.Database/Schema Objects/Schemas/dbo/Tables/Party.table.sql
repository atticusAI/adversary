﻿CREATE TABLE [dbo].[Party] (
    [ScrMemID]              INT            NULL,
    [PartyType]             NVARCHAR (50)  NULL,
    [PartyFName]            NVARCHAR (MAX) NULL,
    [PartyMName]            NVARCHAR (MAX) NULL,
    [PartyLName]            NVARCHAR (MAX) NULL,
    [PartyOrganization]     NVARCHAR (MAX) NULL,
    [PartyRelationshipName] NVARCHAR (100) NULL,
    [PartyRelationshipCode] NVARCHAR (50)  NULL,
    [PartyID]               INT            IDENTITY (1, 1) NOT NULL,
    [PC]                    INT            NULL,
    [PartyAffiliates]       NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Party] PRIMARY KEY CLUSTERED ([PartyID] ASC),
    CONSTRAINT [FK_Party_Memo] FOREIGN KEY ([ScrMemID]) REFERENCES [dbo].[Memo] ([ScrMemID])
);



