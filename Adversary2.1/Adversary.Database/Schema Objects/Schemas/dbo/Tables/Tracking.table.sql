﻿CREATE TABLE [dbo].[Tracking] (
    [TrackingID]                 INT            IDENTITY (1, 1) NOT NULL,
    [ScrMemID]                   INT            NULL,
    [StatusTypeID]               INT            NULL,
    [Locked]                     BIT            NULL,
    [Notes]                      VARCHAR (MAX)  NULL,
    [RejectionHold]              BIT            NULL,
    [APAcknowledged]             BIT            NULL,
    [APApproved]                 BIT            NULL,
    [APUserID]                   INT            NULL,
    [APSignature]                VARCHAR (50)   NULL,
    [APDate]                     SMALLDATETIME  NULL,
    [APNotes]                    VARCHAR (MAX)  NULL,
    [APRejectionReason]          VARCHAR (MAX)  NULL,
    [APARSignature]              VARCHAR (50)   NULL,
    [APExceptionSignature]       VARCHAR (50)   NULL,
    [PGMAcknowledged]            BIT            NULL,
    [PGMApproved]                BIT            NULL,
    [PGMUserID]                  INT            NULL,
    [PGMSignature]               VARCHAR (50)   NULL,
    [PGMDate]                    SMALLDATETIME  NULL,
    [PGMNotes]                   VARCHAR (MAX)  NULL,
    [PGMRejectionReason]         VARCHAR (MAX)  NULL,
    [PGMARSignature]             VARCHAR (50)   NULL,
    [PGMExceptionSignature]      VARCHAR (50)   NULL,
    [AdversaryAdminAcknowledged] BIT            NULL,
    [ARApprovalReceived]         BIT            NULL,
    [AdversaryAdminApproved]     BIT            NULL,
    [AdversarySignature]         VARCHAR (50)   NULL,
    [AdversaryRejectionReason]   VARCHAR (MAX)  NULL,
    [AdversaryAcknowledged]      BIT            NULL,
    [AdversaryUserID]            INT            NULL,
    [Opened]                     NVARCHAR (MAX) NULL,
    [Conflicts]                  VARCHAR (MAX)  NULL,
    [FeeSplits]                  VARCHAR (MAX)  NULL,
    [FinalCheck]                 VARCHAR (MAX)  NULL,
    [SubmittedOn]                DATETIME       NULL,
    [AdversaryAdminDate]         SMALLDATETIME  NULL,
    [RetainerAcknowledged]       BIT            NULL,
    [CMNumbersSentDate]          SMALLDATETIME  NULL,
    [PGM2Acknowledged]           BIT            NULL,
    [PGM2Approved]               BIT            NULL,
    [PGM2UserID]                 INT            NULL,
    [PGM2Signature]              VARCHAR (50)   NULL,
    [PGM2Date]                   SMALLDATETIME  NULL,
    [PGM2Notes]                  VARCHAR (MAX)  NULL,
    [PGM2RejectionReason]        VARCHAR (MAX)  NULL,
    [PGM2ARSignature]            VARCHAR (50)   NULL,
    [PGM2ExceptionSignature]     VARCHAR (50)   NULL,
    [FinAcknowledged]            BIT            NULL,
    [FinApproved]                BIT            NULL,
    [FinUserID]                  INT            NULL,
    [FinSignature]               VARCHAR (50)   NULL,
    [FinDate]                    SMALLDATETIME  NULL,
    [FinNotes]                   VARCHAR (MAX)  NULL,
    [FinRejectionReason]         VARCHAR (MAX)  NULL,
    [FinARSignature]             VARCHAR (50)   NULL,
    [FinExceptionSignature]      VARCHAR (50)   NULL,
    CONSTRAINT [PK_Tracking] PRIMARY KEY CLUSTERED ([TrackingID] ASC),
    CONSTRAINT [FK_Tracking_AdminLogins] FOREIGN KEY ([APUserID]) REFERENCES [dbo].[AdminLogins] ([UserID]),
    CONSTRAINT [FK_Tracking_AdminLogins1] FOREIGN KEY ([PGMUserID]) REFERENCES [dbo].[AdminLogins] ([UserID]),
    CONSTRAINT [FK_Tracking_AdminLogins2] FOREIGN KEY ([AdversaryUserID]) REFERENCES [dbo].[AdminLogins] ([UserID]),
    CONSTRAINT [FK_Tracking_Memo] FOREIGN KEY ([ScrMemID]) REFERENCES [dbo].[Memo] ([ScrMemID])
);





