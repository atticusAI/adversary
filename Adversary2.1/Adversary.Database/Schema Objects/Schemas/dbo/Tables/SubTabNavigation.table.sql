﻿CREATE TABLE [dbo].[SubTabNavigation] (
    [SubTabNavigationID]       INT            IDENTITY (1, 1) NOT NULL,
    [PageNavigationID]         INT            NOT NULL,
    [SubTabOrder]              INT            NOT NULL,
    [SubTabURL]                VARCHAR (2000) NOT NULL,
    [Name]                     VARCHAR (50)   NOT NULL,
    [Description]              VARCHAR (2000) NULL,
    [ParentSubTabNavigationID] INT            NULL
);

