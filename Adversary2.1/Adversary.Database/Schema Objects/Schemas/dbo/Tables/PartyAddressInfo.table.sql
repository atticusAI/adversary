﻿CREATE TABLE [dbo].[PartyAddressInfo] (
    [PartyAddressID] INT           IDENTITY (1, 1) NOT NULL,
    [PartyID]        INT           NOT NULL,
    [Address_Line_1] NVARCHAR (50) NULL,
    [Address_Line_2] NVARCHAR (50) NULL,
    [Address_Line_3] NVARCHAR (50) NULL,
    [City]           NVARCHAR (50) NULL,
    [State]          NVARCHAR (50) NULL,
    [Zip]            NVARCHAR (50) NULL,
    [Country]        NVARCHAR (50) NULL,
    [Phone]          NVARCHAR (50) NULL,
    [Fax]            NVARCHAR (50) NULL,
    [EMail]          NVARCHAR (50) NULL
);

