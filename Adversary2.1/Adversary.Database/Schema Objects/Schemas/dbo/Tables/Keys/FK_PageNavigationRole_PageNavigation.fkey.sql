﻿ALTER TABLE [dbo].[PageNavigationRole]
    ADD CONSTRAINT [FK_PageNavigationRole_PageNavigation] FOREIGN KEY ([PageNavigationID]) REFERENCES [dbo].[PageNavigation] ([PageNavigationID]) ON DELETE NO ACTION ON UPDATE NO ACTION;

