﻿ALTER TABLE [dbo].[ScreeningMemoTypeSummarySection]
    ADD CONSTRAINT [FK_ScreeningMemoTypeSummarySection_ScreeningMemoSummarySection] FOREIGN KEY ([ScrMemSummarySectionID]) REFERENCES [dbo].[ScreeningMemoSummarySection] ([ScrMemSummarySectionID]) ON DELETE NO ACTION ON UPDATE NO ACTION;

