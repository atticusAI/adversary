﻿ALTER TABLE [dbo].[AdminPracCodes]
    ADD CONSTRAINT [FK_AdminPracCodes_AdminLogins] FOREIGN KEY ([UserID]) REFERENCES [dbo].[AdminLogins] ([UserID]) ON DELETE NO ACTION ON UPDATE NO ACTION;

