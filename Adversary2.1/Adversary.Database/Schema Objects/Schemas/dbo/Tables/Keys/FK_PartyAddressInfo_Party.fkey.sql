﻿ALTER TABLE [dbo].[PartyAddressInfo]
    ADD CONSTRAINT [FK_PartyAddressInfo_Party] FOREIGN KEY ([PartyID]) REFERENCES [dbo].[Party] ([PartyID]) ON DELETE NO ACTION ON UPDATE NO ACTION;

