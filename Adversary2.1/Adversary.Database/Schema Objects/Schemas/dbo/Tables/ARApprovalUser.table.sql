﻿CREATE TABLE [dbo].[ARApprovalUser] (
    [ApprovalUserID] INT            IDENTITY (1, 1) NOT NULL,
    [UserName]       NVARCHAR (150) NULL,
    [EmailAddress]   NVARCHAR (100) NULL
);

