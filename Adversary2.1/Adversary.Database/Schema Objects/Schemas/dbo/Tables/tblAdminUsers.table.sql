﻿CREATE TABLE [dbo].[tblAdminUsers] (
    [AdminUserID]    INT           NOT NULL,
    [AdminLoginName] NVARCHAR (50) NULL,
    [AdminEmail]     NVARCHAR (50) NULL,
    [SortOrder]      INT           NULL
);

