﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[TRG_NOTIFICATION_STATUS_CHANGE]
   ON [dbo].[Tracking]
   AFTER UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @STATUS INT
	DECLARE @TRACKINGID INT
	DECLARE @RESPATTOFFICECODE VARCHAR(50)
	DECLARE @SUBMITTEDON SMALLDATETIME
	DECLARE @APDATE SMALLDATETIME
    
    -- Insert statements for trigger here
    IF UPDATE (StatusTypeID)
    BEGIN
		--get all necessary info from inserted/updated row
		SELECT
			@STATUS = I.StatusTypeID,
			@TRACKINGID = I.TrackingID,
			@SUBMITTEDON = I.SubmittedOn,
			@APDATE = I.APDate,
			@RESPATTOFFICECODE = M.RespAttOfficeCode
		FROM
			Inserted I
			LEFT JOIN
			Memo M
			ON I.ScrMemID = M.ScrMemID
	    
		IF (@STATUS = 1 AND @RESPATTOFFICECODE <> '10')
		BEGIN
			--if status is 1, then set Notifications.SubmittedOn
			--to match the Tracking.SubmittedOn.
			UPDATE Notifications SET
				Acknowledged = 0,
				Notification1Sent = NULL,
				Notification1Date = NULL,
				Notification2Sent = NULL,
				Notification2Date = NULL,
				NotificationFinalSent = NULL,
				NotificationFinalDate = NULL,
				SubmittedOn = @SUBMITTEDON
			WHERE
				TrackingID = @TRACKINGID
		END
		ELSE IF (@STATUS = 3)
		BEGIN
			--If the office is set to Denver,
			--set Notifications.SubmittedOn equal to
			--the Tracking.SubmittedOn.
			IF (@RESPATTOFFICECODE = '10')
			BEGIN
				UPDATE Notifications SET
					Notification1Sent = NULL,
					Notification1Date = NULL,
					Notification2Sent = NULL,
					Notification2Date = NULL,
					NotificationFinalSent = NULL,
					NotificationFinalDate = NULL,
					SubmittedOn = @SUBMITTEDON
				WHERE
					TrackingID = @TRACKINGID
			END
			ELSE
			BEGIN
				--set Notifications.SubmittedOn equal to
				--Tracking.APDate field instead when NOT denver.
				UPDATE Notifications SET
					Notification1Sent = NULL,
					Notification1Date = NULL,
					Notification2Sent = NULL,
					Notification2Date = NULL,
					NotificationFinalSent = NULL,
					NotificationFinalDate = NULL,
					SubmittedOn = @APDATE
				WHERE
					TrackingID = @TRACKINGID	
			END
		END
		ELSE
		BEGIN
			UPDATE Notifications SET
				Acknowledged = 1
			WHERE
				TrackingID = @TRACKINGID
		END
	END
END

