﻿
-- =============================================
-- Author:		Arbon Reimer
-- Create date: 09/16/2009
-- Description:	This trigger selects all new rows created in the dbo.Tracking
--	table and inserts them into the dbo.Notifications table.  It needs to track
--	the originating office, time created, appropriate IDs.
-- =============================================
CREATE TRIGGER [dbo].[TRG_TRACKING_NOTIFICATION]
   ON  [dbo].[Tracking]
   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
    INSERT dbo.Notifications
    (
		TrackingID,
		ScrMemID,
		SubmittedOn,
		Office,
		PracCode
    )
    SELECT
		I.TrackingID,
		I.ScrMemID,
		I.SubmittedOn,
		S.RespAttOfficeCode,
		S.PracCode
	FROM
		Inserted I
		LEFT JOIN
		Memo S
		ON I.ScrMemID = S.ScrMemID
	WHERE I.StatusTypeID IN (1,3,5)

END

