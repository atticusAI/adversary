﻿-- =============================================
-- Author:		unknown
-- Create date: unknown
-- Description:	keep praccode and respattofficecode fields in sync between 
--              memo and notifications tables
--              blmcgee@hollandhart.com 2013-01-09
--              removed logic error where updates were assumed to only 
--              affect one row at a time
-- =============================================
CREATE TRIGGER [dbo].[TRG_OFFICE_PGROUP_CHANGE]
   ON  [dbo].[Memo]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

    IF (UPDATE (PracCode)) OR (UPDATE (RespAttOfficeCode))
    BEGIN
		UPDATE Notifications
		SET Notifications.Office = CAST(Inserted.RespAttOfficeCode AS INT),
			Notifications.PracCode = Inserted.PracCode
		FROM Notifications
		INNER JOIN Inserted on Inserted.ScrMemID = Notifications.ScrMemID
	END
END

