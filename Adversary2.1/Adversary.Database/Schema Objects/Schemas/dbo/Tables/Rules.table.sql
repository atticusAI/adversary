﻿CREATE TABLE [dbo].[Rules] (
    [RuleID]     INT            IDENTITY (1, 1) NOT NULL,
    [RuleText]   NVARCHAR (500) NOT NULL,
    [RuleActive] BIT            CONSTRAINT [DF_Rules2_RuleActive] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Rules] PRIMARY KEY CLUSTERED ([RuleID] ASC)
);



