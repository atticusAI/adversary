﻿CREATE TABLE [dbo].[ScreeningMemoTypeSummarySection] (
    [ScrMemTypeSummarySectionID] INT IDENTITY (1, 1) NOT NULL,
    [TrackNumber]                INT NOT NULL,
    [ScrMemSummarySectionID]     INT NOT NULL,
    [SectionOrder]               INT NOT NULL
);

