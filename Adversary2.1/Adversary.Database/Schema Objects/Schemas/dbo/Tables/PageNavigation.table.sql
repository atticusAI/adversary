﻿CREATE TABLE [dbo].[PageNavigation] (
    [PageNavigationID] INT            IDENTITY (1, 1) NOT NULL,
    [PageUrl]          VARCHAR (500)  NOT NULL,
    [Name]             VARCHAR (50)   NULL,
    [Description]      VARCHAR (2000) NULL
);

