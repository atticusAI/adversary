﻿CREATE TABLE [dbo].[OpposingCounsel] (
    [OpposingID]  INT            IDENTITY (1, 1) NOT NULL,
    [LawfirmName] NVARCHAR (150) NULL,
    [LawyerName]  NVARCHAR (50)  NULL,
    [ScrMemID]    INT            NULL,
    CONSTRAINT [PK_OpposingCounsel] PRIMARY KEY CLUSTERED ([OpposingID] ASC),
    CONSTRAINT [FK_OpposingCounsel_Memo] FOREIGN KEY ([ScrMemID]) REFERENCES [dbo].[Memo] ([ScrMemID])
);



