﻿CREATE TABLE [dbo].[ClientMatterNumbers] (
    [ClientMatterID]     INT            IDENTITY (1, 1) NOT NULL,
    [ScrMemID]           INT            NULL,
    [ClientMatterNumber] NVARCHAR (150) NULL,
    CONSTRAINT [PK_ClientMatterNumbers] PRIMARY KEY CLUSTERED ([ClientMatterID] ASC),
    CONSTRAINT [FK_ClientMatterNumbers_Memo] FOREIGN KEY ([ScrMemID]) REFERENCES [dbo].[Memo] ([ScrMemID])
);



