﻿CREATE TABLE [dbo].[Role] (
    [RoleID] INT          IDENTITY (1, 1) NOT NULL,
    [Role]   VARCHAR (50) NOT NULL,
    [Active] BIT          NULL,
    CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED ([RoleID] ASC)
);



