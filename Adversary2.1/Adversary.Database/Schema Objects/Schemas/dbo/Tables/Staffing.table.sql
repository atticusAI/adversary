﻿CREATE TABLE [dbo].[Staffing] (
    [ScrMemID]    INT           NULL,
    [PersonnelID] NVARCHAR (50) NOT NULL,
    [StaffingID]  INT           IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_Staffing] PRIMARY KEY CLUSTERED ([StaffingID] ASC),
    CONSTRAINT [FK_Staffing_Memo] FOREIGN KEY ([ScrMemID]) REFERENCES [dbo].[Memo] ([ScrMemID])
);



