﻿CREATE TABLE [dbo].[Files] (
    [FileID]    INT            IDENTITY (1, 1) NOT NULL,
    [ScrMemID]  INT            NULL,
    [FileType]  NVARCHAR (50)  NULL,
    [FileLabel] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Files] PRIMARY KEY CLUSTERED ([FileID] ASC),
    CONSTRAINT [FK_Files_Memo] FOREIGN KEY ([ScrMemID]) REFERENCES [dbo].[Memo] ([ScrMemID])
);



