﻿CREATE TABLE [dbo].[DocketingTeam] (
    [ScrMemID]        INT           NULL,
    [PersonnelID]     NVARCHAR (50) NOT NULL,
    [DocketingTeamID] INT           IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_DocketingTeamID] PRIMARY KEY CLUSTERED ([DocketingTeamID] ASC),
    CONSTRAINT [FK_DocketingTeam_Memo] FOREIGN KEY ([ScrMemID]) REFERENCES [dbo].[Memo] ([ScrMemID])
);



