﻿CREATE TABLE [dbo].[PageNavigationRole] (
    [PageNavigationRoleID] INT IDENTITY (1, 1) NOT NULL,
    [PageNavigationID]     INT NOT NULL,
    [RoleID]               INT NOT NULL
);

