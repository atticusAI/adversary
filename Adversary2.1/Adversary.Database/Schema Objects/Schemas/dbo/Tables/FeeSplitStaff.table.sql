﻿CREATE TABLE [dbo].[FeeSplitStaff] (
    [ScrMemID]        INT           NOT NULL,
    [PersonnelID]     NVARCHAR (50) NULL,
    [FeeSplitStaffID] INT           IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_FeeSplitStaff] PRIMARY KEY CLUSTERED ([FeeSplitStaffID] ASC),
    CONSTRAINT [FK_FeeSplitStaff_Memo] FOREIGN KEY ([ScrMemID]) REFERENCES [dbo].[Memo] ([ScrMemID])
);



