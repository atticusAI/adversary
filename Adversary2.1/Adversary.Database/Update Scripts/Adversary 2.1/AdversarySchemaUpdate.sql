﻿/*
Deployment script for Adversary2.1

*/

USE [Adversary]
GO

GO
PRINT N'Dropping FK_DocketingTeam_Memo...';


GO
ALTER TABLE [dbo].[DocketingTeam] DROP CONSTRAINT [FK_DocketingTeam_Memo];


GO
PRINT N'Dropping FK_Billing_Memo...';


GO
ALTER TABLE [dbo].[Billing] DROP CONSTRAINT [FK_Billing_Memo];


GO
PRINT N'Dropping FK_Party_Memo...';


GO
ALTER TABLE [dbo].[Party] DROP CONSTRAINT [FK_Party_Memo];


GO
PRINT N'Dropping FK_NewClient_Memo...';


GO
ALTER TABLE [dbo].[NewClient] DROP CONSTRAINT [FK_NewClient_Memo];


GO
PRINT N'Dropping FK_Files_Memo...';


GO
ALTER TABLE [dbo].[Files] DROP CONSTRAINT [FK_Files_Memo];


GO
PRINT N'Dropping FK_Tracking_Memo...';


GO
ALTER TABLE [dbo].[Tracking] DROP CONSTRAINT [FK_Tracking_Memo];


GO
PRINT N'Dropping FK_Address_Memo...';


GO
ALTER TABLE [dbo].[Address] DROP CONSTRAINT [FK_Address_Memo];


GO
PRINT N'Dropping FK_ClientContact_Memo...';


GO
ALTER TABLE [dbo].[ClientContact] DROP CONSTRAINT [FK_ClientContact_Memo];


GO
PRINT N'Dropping FK_Attachments_Memo...';


GO
ALTER TABLE [dbo].[Attachments] DROP CONSTRAINT [FK_Attachments_Memo];


GO
PRINT N'Dropping FK_Staffing_Memo...';


GO
ALTER TABLE [dbo].[Staffing] DROP CONSTRAINT [FK_Staffing_Memo];


GO
PRINT N'Dropping FK_FeeSplitStaff_Memo...';


GO
ALTER TABLE [dbo].[FeeSplitStaff] DROP CONSTRAINT [FK_FeeSplitStaff_Memo];


GO
PRINT N'Dropping FK_ClientMatterNumbers_Memo...';


GO
ALTER TABLE [dbo].[ClientMatterNumbers] DROP CONSTRAINT [FK_ClientMatterNumbers_Memo];


GO
PRINT N'Dropping FK_NDriveAccess_Memo...';


GO
ALTER TABLE [dbo].[NDriveAccess] DROP CONSTRAINT [FK_NDriveAccess_Memo];


GO
PRINT N'Dropping [dbo].[PR_DELETE_Paste Errors]...';


GO
DROP PROCEDURE [dbo].[PR_DELETE_Paste Errors];


GO
PRINT N'Dropping [dbo].[PR_GET_Paste Errors]...';


GO
DROP PROCEDURE [dbo].[PR_GET_Paste Errors];


GO
PRINT N'Dropping [dbo].[PR_SEARCH_Paste Errors]...';


GO
DROP PROCEDURE [dbo].[PR_SEARCH_Paste Errors];


GO
PRINT N'Dropping [dbo].[PR_SET_Paste Errors]...';


GO
DROP PROCEDURE [dbo].[PR_SET_Paste Errors];


GO
PRINT N'Altering [dbo].[Address]...';


GO
ALTER TABLE [dbo].[Address]
    ADD [NoEmailReason] NVARCHAR (50) NULL;


GO
PRINT N'Starting rebuilding table [dbo].[Memo]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Memo] (
    [ScrMemID]                        INT            IDENTITY (1, 1) NOT NULL,
    [ScrMemType]                      INT            NULL,
    [RefDate]                         DATETIME       NULL,
    [InputDate]                       MONEY          NULL,
    [AttEntering]                     NVARCHAR (50)  NULL,
    [PersonnelID]                     NVARCHAR (50)  NULL,
    [RespAttID]                       NVARCHAR (4)   NULL,
    [PreScreen]                       BIT            NULL,
    [PreScreenDesc]                   NVARCHAR (MAX) NULL,
    [PreScreenNames]                  TINYINT        NULL,
    [PreScreenNameList]               NVARCHAR (MAX) NULL,
    [ClearedConflicts]                BIT            NULL,
    [EstFees]                         NVARCHAR (50)  NULL,
    [WorkDesc]                        NVARCHAR (MAX) NULL,
    [FeeSplit]                        BIT            NULL,
    [FeeSplitDesc]                    NVARCHAR (MAX) NULL,
    [OrigOffice]                      NVARCHAR (50)  NULL,
    [PracCode]                        NVARCHAR (5)   NULL,
    [SecondPracCode]                  NVARCHAR (5)   NULL,
    [ConflictReport]                  BIT            NULL,
    [ConflictDesc]                    NVARCHAR (MAX) NULL,
    [EngageLetter]                    BIT            NULL,
    [EngageDesc]                      NVARCHAR (MAX) NULL,
    [DocumentID]                      NVARCHAR (50)  NULL,
    [Imanage]                         BIT            NULL,
    [DocumentOffice]                  NVARCHAR (50)  NULL,
    [ContigDocumentID]                NVARCHAR (50)  NULL,
    [ContigDocumentOffice]            NVARCHAR (50)  NULL,
    [Retainer]                        BIT            NULL,
    [RetainerDescNo]                  NVARCHAR (MAX) NULL,
    [RetainerDescYes]                 NVARCHAR (MAX) NULL,
    [Confidential]                    BIT            NULL,
    [Company]                         BIT            NULL,
    [Individual]                      BIT            NULL,
    [ClientNumber]                    NVARCHAR (10)  NULL,
    [EmpEntering]                     NVARCHAR (50)  NULL,
    [EmpClient]                       NVARCHAR (50)  NULL,
    [EstHours]                        INT            NULL,
    [RetainerDesc]                    NVARCHAR (50)  NULL,
    [ContingFee]                      BIT            NULL,
    [ProBonoTypeID]                   INT            NULL,
    [HHLegalWorkID]                   INT            NULL,
    [MatterName]                      NVARCHAR (255) NULL,
    [RejectDesc]                      NVARCHAR (MAX) NULL,
    [ProBonoReason]                   NVARCHAR (MAX) NULL,
    [NewMatterOnly]                   NVARCHAR (50)  NULL,
    [OppQuestion]                     BIT            NULL,
    [PatentMatter]                    NVARCHAR (255) NULL,
    [TreasuryList]                    BIT            NULL,
    [CommerceList]                    BIT            NULL,
    [WrittenPolicies]                 BIT            NULL,
    [PendingBankruptcy]               BIT            NULL,
    [NDrive]                          BIT            NULL,
    [NDriveType]                      INT            NULL,
    [RespAttOfficeCode]               NVARCHAR (50)  NULL,
    [NoRejectionLetterReason]         NVARCHAR (MAX) NULL,
    [NoRejectionLetter]               BIT            NULL,
    [DocketingAtty]                   NVARCHAR (50)  NULL,
    [DocketingStaff]                  NVARCHAR (50)  NULL,
    [OutsideCounselPolicyDescription] NVARCHAR (MAX) NULL,
    [ProBonoLimitedMeans]             BIT            NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_Memo] PRIMARY KEY CLUSTERED ([ScrMemID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Memo])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_Memo] ON;
        INSERT INTO [dbo].[tmp_ms_xx_Memo] ([ScrMemID], [ScrMemType], [RefDate], [InputDate], [AttEntering], [PersonnelID], [RespAttID], [PreScreen], [PreScreenDesc], [PreScreenNames], [PreScreenNameList], [ClearedConflicts], [EstFees], [WorkDesc], [FeeSplit], [FeeSplitDesc], [OrigOffice], [PracCode], [ConflictReport], [ConflictDesc], [EngageLetter], [EngageDesc], [DocumentID], [Imanage], [DocumentOffice], [ContigDocumentID], [ContigDocumentOffice], [Retainer], [RetainerDescNo], [RetainerDescYes], [Confidential], [Company], [Individual], [ClientNumber], [EmpEntering], [EmpClient], [EstHours], [RetainerDesc], [ContingFee], [ProBonoTypeID], [HHLegalWorkID], [MatterName], [RejectDesc], [ProBonoReason], [NewMatterOnly], [OppQuestion], [PatentMatter], [TreasuryList], [CommerceList], [WrittenPolicies], [PendingBankruptcy], [NDrive], [NDriveType], [RespAttOfficeCode], [NoRejectionLetterReason], [NoRejectionLetter], [DocketingAtty], [DocketingStaff], [OutsideCounselPolicyDescription])
        SELECT   [ScrMemID],
                 [ScrMemType],
                 [RefDate],
                 [InputDate],
                 [AttEntering],
                 [PersonnelID],
                 [RespAttID],
                 [PreScreen],
                 [PreScreenDesc],
                 [PreScreenNames],
                 [PreScreenNameList],
                 [ClearedConflicts],
                 [EstFees],
                 [WorkDesc],
                 [FeeSplit],
                 [FeeSplitDesc],
                 [OrigOffice],
                 [PracCode],
                 [ConflictReport],
                 [ConflictDesc],
                 [EngageLetter],
                 [EngageDesc],
                 [DocumentID],
                 [Imanage],
                 [DocumentOffice],
                 [ContigDocumentID],
                 [ContigDocumentOffice],
                 [Retainer],
                 [RetainerDescNo],
                 [RetainerDescYes],
                 [Confidential],
                 [Company],
                 [Individual],
                 [ClientNumber],
                 [EmpEntering],
                 [EmpClient],
                 [EstHours],
                 [RetainerDesc],
                 [ContingFee],
                 [ProBonoTypeID],
                 [HHLegalWorkID],
                 [MatterName],
                 [RejectDesc],
                 [ProBonoReason],
                 [NewMatterOnly],
                 [OppQuestion],
                 [PatentMatter],
                 [TreasuryList],
                 [CommerceList],
                 [WrittenPolicies],
                 [PendingBankruptcy],
                 [NDrive],
                 [NDriveType],
                 [RespAttOfficeCode],
                 [NoRejectionLetterReason],
                 [NoRejectionLetter],
                 [DocketingAtty],
                 [DocketingStaff],
                 [OutsideCounselPolicyDescription]
        FROM     [dbo].[Memo]
        ORDER BY [ScrMemID] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_Memo] OFF;
    END

DROP TABLE [dbo].[Memo];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Memo]', N'Memo';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_Memo]', N'PK_Memo', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
PRINT N'Altering [dbo].[Role]...';


GO
ALTER TABLE [dbo].[Role]
    ADD [Active] BIT NULL;


GO
PRINT N'Altering [dbo].[Tracking]...';


GO
ALTER TABLE [dbo].[Tracking]
    ADD [PGM2Acknowledged]       BIT           NULL,
        [PGM2Approved]           BIT           NULL,
        [PGM2UserID]             INT           NULL,
        [PGM2Signature]          VARCHAR (50)  NULL,
        [PGM2Date]               SMALLDATETIME NULL,
        [PGM2Notes]              VARCHAR (MAX) NULL,
        [PGM2RejectionReason]    VARCHAR (MAX) NULL,
        [PGM2ARSignature]        VARCHAR (50)  NULL,
        [PGM2ExceptionSignature] VARCHAR (50)  NULL,
        [FinAcknowledged]        BIT           NULL,
        [FinApproved]            BIT           NULL,
        [FinUserID]              INT           NULL,
        [FinSignature]           VARCHAR (50)  NULL,
        [FinDate]                SMALLDATETIME NULL,
        [FinNotes]               VARCHAR (MAX) NULL,
        [FinRejectionReason]     VARCHAR (MAX) NULL,
        [FinARSignature]         VARCHAR (50)  NULL,
        [FinExceptionSignature]  VARCHAR (50)  NULL;


GO
PRINT N'Creating [dbo].[ApprovalType]...';


GO
CREATE TABLE [dbo].[ApprovalType] (
    [ApprovalTypeID] INT           IDENTITY (1, 1) NOT NULL,
    [ApprovalType]   NVARCHAR (50) NOT NULL,
    [TrackNumber]    INT           NULL,
    [Active]         BIT           NULL,
    CONSTRAINT [PK_ApprovalType] PRIMARY KEY CLUSTERED ([ApprovalTypeID] ASC)
);

SET IDENTITY_INSERT [dbo].[ApprovalType] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[ApprovalType]([ApprovalTypeID], [ApprovalType], [TrackNumber], [Active])
SELECT 1, N'AP', 1, 1 UNION ALL
SELECT 2, N'PGM', 1, 1 UNION ALL
SELECT 3, N'Adversary', 1, 1 UNION ALL
SELECT 4, N'SecondPGM', 1, 1 UNION ALL
SELECT 5, N'Finance', 1, 1
COMMIT;
RAISERROR (N'[dbo].[ApprovalType]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[ApprovalType] OFF;


GO
PRINT N'Creating [dbo].[Conflict]...';


GO
CREATE TABLE [dbo].[Conflict] (
    [ConflictID]     INT            IDENTITY (1, 1) NOT NULL,
    [TrackNumber]    INT            NOT NULL,
    [Description]    NVARCHAR (255) NULL,
    [SortOrder]      INT            NULL,
    [NoneOfTheAbove] BIT            NULL,
    [RequestField]   NVARCHAR (50)  NULL,
    [Active]         BIT            NULL,
    CONSTRAINT [PK_Conflict] PRIMARY KEY CLUSTERED ([ConflictID] ASC)
);

SET IDENTITY_INSERT [dbo].[Conflict] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[Conflict]([ConflictID], [TrackNumber], [Description], [SortOrder], [NoneOfTheAbove], [RequestField], [Active])
SELECT 1, 1, N'This is a bankruptcy matter.', 100, NULL, NULL, 1 UNION ALL
SELECT 2, 1, N'This is a telecommunications client or matter.', 200, NULL, NULL, 1 UNION ALL
SELECT 3, 1, N'This client is an insurance company.', 300, NULL, NULL, 1 UNION ALL
SELECT 4, 1, N'This client is a governmental entity.', 400, NULL, NULL, 1 UNION ALL
SELECT 5, 1, N'This client is an electric utility.', 500, NULL, NULL, 1 UNION ALL
SELECT 6, 1, N'This client''s legal fees will be paid by an insurance company or other third party.', 600, NULL, NULL, 1 UNION ALL
SELECT 7, 1, N'This client or adversary is a lawyer or a law firm.', 700, NULL, NULL, 1 UNION ALL
SELECT 8, 1, N'This client or adversary is a ski resort.', 800, NULL, NULL, 1 UNION ALL
SELECT 9, 1, N'This client or adversary is an Indian tribe.', 900, NULL, NULL, 1 UNION ALL
SELECT 10, 1, N'This client or adversary is in any segment of the oil and gas industry.', 1000, NULL, NULL, 1 UNION ALL
SELECT 11, 1, N'This adversary is an accounting firm.', 1100, NULL, NULL, 1 UNION ALL
SELECT 353, 2, N'This is a bankruptcy matter.', 100, NULL, NULL, 1 UNION ALL
SELECT 354, 2, N'This is a telecommunications client or matter.', 200, NULL, NULL, 1 UNION ALL
SELECT 355, 2, N'This client is an insurance company.', 300, NULL, NULL, 1 UNION ALL
SELECT 356, 2, N'This client is a governmental entity.', 400, NULL, NULL, 1 UNION ALL
SELECT 357, 2, N'This client is an electric utility.', 500, NULL, NULL, 1 UNION ALL
SELECT 358, 2, N'This client''s legal fees will be paid by an insurance company or other third party.', 600, NULL, NULL, 1 UNION ALL
SELECT 359, 2, N'This client or adversary is a lawyer or a law firm.', 700, NULL, NULL, 1 UNION ALL
SELECT 360, 2, N'This client or adversary is a ski resort.', 800, NULL, NULL, 1 UNION ALL
SELECT 361, 2, N'This client or adversary is an Indian tribe.', 900, NULL, NULL, 1 UNION ALL
SELECT 362, 2, N'This client or adversary is an any segment of the oil and gas industry.', 1000, NULL, NULL, 1 UNION ALL
SELECT 363, 2, N'This adversary is an accounting firm.', 1100, NULL, NULL, 1 UNION ALL
SELECT 364, 3, N'This is a bankruptcy matter.', 100, NULL, NULL, 1 UNION ALL
SELECT 365, 3, N'This is a telecommunications client or matter.', 200, NULL, NULL, 1 UNION ALL
SELECT 366, 3, N'This client is an insurance company.', 300, NULL, NULL, 1 UNION ALL
SELECT 367, 3, N'This client is a governmental entity.', 400, NULL, NULL, 1 UNION ALL
SELECT 368, 3, N'This client is an electric utility.', 500, NULL, NULL, 1 UNION ALL
SELECT 369, 3, N'This client''s legal fees will be paid by an insurance company or other third party.', 600, NULL, NULL, 1 UNION ALL
SELECT 370, 3, N'This client or adversary is a lawyer or a law firm.', 700, NULL, NULL, 1 UNION ALL
SELECT 371, 3, N'This client or adversary is a ski resort.', 800, NULL, NULL, 1 UNION ALL
SELECT 372, 3, N'This client or adversary is an Indian tribe.', 900, NULL, NULL, 1 UNION ALL
SELECT 373, 3, N'This client or adversary is an any segment of the oil and gas industry.', 1000, NULL, NULL, 1 UNION ALL
SELECT 374, 3, N'This adversary is an accounting firm.', 1100, NULL, NULL, 1 UNION ALL
SELECT 375, 4, N'This is a bankruptcy matter.', 100, NULL, NULL, 1 UNION ALL
SELECT 376, 4, N'This is a telecommunications client or matter.', 200, NULL, NULL, 1 UNION ALL
SELECT 377, 4, N'This client is an insurance company.', 300, NULL, NULL, 1 UNION ALL
SELECT 378, 4, N'This client is a governmental entity.', 400, NULL, NULL, 1 UNION ALL
SELECT 379, 4, N'This client is an electric utility.', 500, NULL, NULL, 1 UNION ALL
SELECT 380, 4, N'This client''s legal fees will be paid by an insurance company or other third party.', 600, NULL, NULL, 1 UNION ALL
SELECT 381, 4, N'This client or adversary is a lawyer or a law firm.', 700, NULL, NULL, 1 UNION ALL
SELECT 382, 4, N'This client or adversary is a ski resort.', 800, NULL, NULL, 1 UNION ALL
SELECT 383, 4, N'This client or adversary is an Indian tribe.', 900, NULL, NULL, 1 UNION ALL
SELECT 384, 4, N'This client or adversary is an any segment of the oil and gas industry.', 1000, NULL, NULL, 1 UNION ALL
SELECT 385, 4, N'This adversary is an accounting firm.', 1100, NULL, NULL, 1 UNION ALL
SELECT 386, 5, N'This is a bankruptcy matter.', 100, NULL, NULL, 1 UNION ALL
SELECT 387, 5, N'This is a telecommunications client or matter.', 200, NULL, NULL, 1 UNION ALL
SELECT 388, 5, N'This client is an insurance company.', 300, NULL, NULL, 1 UNION ALL
SELECT 389, 5, N'This client is a governmental entity.', 400, NULL, NULL, 1 UNION ALL
SELECT 390, 5, N'This client is an electric utility.', 500, NULL, NULL, 1 UNION ALL
SELECT 391, 5, N'This client''s legal fees will be paid by an insurance company or other third party.', 600, NULL, NULL, 1
COMMIT;
RAISERROR (N'[dbo].[Conflict]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[Conflict]([ConflictID], [TrackNumber], [Description], [SortOrder], [NoneOfTheAbove], [RequestField], [Active])
SELECT 392, 5, N'This client or adversary is a lawyer or a law firm.', 700, NULL, NULL, 1 UNION ALL
SELECT 393, 5, N'This client or adversary is a ski resort.', 800, NULL, NULL, 1 UNION ALL
SELECT 394, 5, N'This client or adversary is an Indian tribe.', 900, NULL, NULL, 1 UNION ALL
SELECT 395, 5, N'This client or adversary is an any segment of the oil and gas industry.', 1000, NULL, NULL, 1 UNION ALL
SELECT 396, 5, N'This adversary is an accounting firm.', 1100, NULL, NULL, 1 UNION ALL
SELECT 397, 6, N'This is a bankruptcy matter.', 100, NULL, NULL, 1 UNION ALL
SELECT 398, 6, N'This is a telecommunications client or matter.', 200, NULL, NULL, 1 UNION ALL
SELECT 399, 6, N'This client is an insurance company.', 300, NULL, NULL, 1 UNION ALL
SELECT 400, 6, N'This client is a governmental entity.', 400, NULL, NULL, 1 UNION ALL
SELECT 401, 6, N'This client is an electric utility.', 500, NULL, NULL, 1 UNION ALL
SELECT 402, 6, N'This client''s legal fees will be paid by an insurance company or other third party.', 600, NULL, NULL, 1 UNION ALL
SELECT 403, 6, N'This client or adversary is a lawyer or a law firm.', 700, NULL, NULL, 1 UNION ALL
SELECT 404, 6, N'This client or adversary is a ski resort.', 800, NULL, NULL, 1 UNION ALL
SELECT 405, 6, N'This client or adversary is an Indian tribe.', 900, NULL, NULL, 1 UNION ALL
SELECT 406, 6, N'This client or adversary is an any segment of the oil and gas industry.', 1000, NULL, NULL, 1 UNION ALL
SELECT 407, 6, N'This adversary is an accounting firm.', 1100, NULL, NULL, 1 UNION ALL
SELECT 408, 7, N'This is a bankruptcy matter.', 100, NULL, NULL, 1 UNION ALL
SELECT 409, 7, N'This is a telecommunications client or matter.', 200, NULL, NULL, 1 UNION ALL
SELECT 410, 7, N'This client is an insurance company.', 300, NULL, NULL, 1 UNION ALL
SELECT 411, 7, N'This client is a governmental entity.', 400, NULL, NULL, 1 UNION ALL
SELECT 412, 7, N'This client is an electric utility.', 500, NULL, NULL, 1 UNION ALL
SELECT 413, 7, N'This client''s legal fees will be paid by an insurance company or other third party.', 600, NULL, NULL, 1 UNION ALL
SELECT 414, 7, N'This client or adversary is a lawyer or a law firm.', 700, NULL, NULL, 1 UNION ALL
SELECT 415, 7, N'This client or adversary is a ski resort.', 800, NULL, NULL, 1 UNION ALL
SELECT 416, 7, N'This client or adversary is an Indian tribe.', 900, NULL, NULL, 1 UNION ALL
SELECT 417, 7, N'This client or adversary is an any segment of the oil and gas industry.', 1000, NULL, NULL, 1 UNION ALL
SELECT 418, 7, N'This adversary is an accounting firm.', 1100, NULL, NULL, 1 UNION ALL
SELECT 419, 8, N'This is a bankruptcy matter.', 100, NULL, N'Bankruptcy', 1 UNION ALL
SELECT 420, 8, N'This is a telecommunications client or matter.', 200, NULL, N'Communications', 1 UNION ALL
SELECT 421, 8, N'This client is an insurance company.', 300, NULL, N'Insurance', 1 UNION ALL
SELECT 422, 8, N'This client is a governmental entity.', 400, NULL, N'Government', 1 UNION ALL
SELECT 423, 8, N'This client is an electric utility.', 500, NULL, N'ElectricUtility', 1 UNION ALL
SELECT 424, 8, N'This client''s legal fees will be paid by an insurance company or other third party.', 600, NULL, N'Insurance', 1 UNION ALL
SELECT 425, 8, N'This client or adversary is a lawyer or a law firm.', 700, NULL, N'LawFirm', 1 UNION ALL
SELECT 426, 8, N'This client or adversary is a ski resort.', 800, NULL, N'SkiResort', 1 UNION ALL
SELECT 427, 8, N'This client or adversary is an Indian tribe.', 900, NULL, N'IndianTribe', 1 UNION ALL
SELECT 428, 8, N'This client or adversary is an any segment of the oil and gas industry.', 1000, NULL, N'OilGas', 1 UNION ALL
SELECT 429, 8, N'This adversary is an accounting firm.', 1100, NULL, N'AccountingFirm', 1 UNION ALL
SELECT 430, 1, N'None of the above.', 1200, 1, NULL, 1 UNION ALL
SELECT 431, 8, N'None of the above.', 1200, 1, N'NoneOfTheAbove', 1 UNION ALL
SELECT 432, 2, N'None of the above.', 1200, 1, NULL, 1 UNION ALL
SELECT 433, 3, N'None of the above.', 1200, 1, NULL, 1 UNION ALL
SELECT 434, 6, N'None of the above.', 1200, 1, NULL, 1 UNION ALL
SELECT 435, 4, N'None of the above.', 1200, 1, NULL, 1 UNION ALL
SELECT 436, 5, N'None of the above.', 1200, 1, NULL, 1 UNION ALL
SELECT 437, 7, N'None of the above.', 1200, 1, NULL, 1
COMMIT;
RAISERROR (N'[dbo].[Conflict]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[Conflict] OFF;

GO
PRINT N'Creating [dbo].[Country]...';


GO
CREATE TABLE [dbo].[Country] (
    [CountryID] INT           IDENTITY (1, 1) NOT NULL,
    [Code]      NVARCHAR (50) NOT NULL,
    [Name]      NVARCHAR (50) NOT NULL,
    [SortOrder] INT           NULL,
    [IsPrimary] BIT           NULL,
    CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED ([CountryID] ASC)
);

SET IDENTITY_INSERT [dbo].[Country] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[Country]([CountryID], [Code], [Name], [SortOrder], [IsPrimary])
SELECT 1, N'AD', N'Andorra', 6, 1 UNION ALL
SELECT 2, N'AE', N'United Arab Emirates', 226, 1 UNION ALL
SELECT 3, N'AF', N'Afghanistan', 2, 1 UNION ALL
SELECT 4, N'AG', N'Antigua and Barbuda', 10, 1 UNION ALL
SELECT 5, N'AI', N'Anguilla', 8, 1 UNION ALL
SELECT 6, N'AL', N'Albania', 3, 1 UNION ALL
SELECT 7, N'AM', N'Armenia', 12, 1 UNION ALL
SELECT 8, N'AN', N'Netherlands Antilles', 152, 1 UNION ALL
SELECT 9, N'AO', N'Angola', 7, 1 UNION ALL
SELECT 10, N'AQ', N'Antarctica', 9, 1 UNION ALL
SELECT 11, N'AR', N'Argentina', 11, 1 UNION ALL
SELECT 12, N'AS', N'American Samoa', 5, 1 UNION ALL
SELECT 13, N'AT', N'Austria', 15, 1 UNION ALL
SELECT 14, N'AU', N'Australia', 14, 1 UNION ALL
SELECT 15, N'AW', N'Aruba', 13, 1 UNION ALL
SELECT 16, N'AZ', N'Azerbaijan', 16, 1 UNION ALL
SELECT 17, N'BA', N'Bosnia and Herzegovina', 28, 1 UNION ALL
SELECT 18, N'BB', N'Barbados', 20, 1 UNION ALL
SELECT 19, N'BD', N'Bangladesh', 19, 1 UNION ALL
SELECT 20, N'BE', N'Belgium', 22, 1 UNION ALL
SELECT 21, N'BF', N'Burkina Faso', 35, 1 UNION ALL
SELECT 22, N'BG', N'Bulgaria', 34, 1 UNION ALL
SELECT 23, N'BH', N'Bahrain', 18, 1 UNION ALL
SELECT 24, N'BI', N'Burundi', 36, 1 UNION ALL
SELECT 25, N'BJ', N'Benin', 24, 1 UNION ALL
SELECT 26, N'BM', N'Bermuda', 25, 1 UNION ALL
SELECT 27, N'BN', N'Brunei Darussalam', 33, 1 UNION ALL
SELECT 28, N'BO', N'Bolivia', 27, 1 UNION ALL
SELECT 29, N'BR', N'Brazil', 31, 1 UNION ALL
SELECT 30, N'BS', N'Bahamas', 17, 1 UNION ALL
SELECT 31, N'BT', N'Bhutan', 26, 1 UNION ALL
SELECT 32, N'BV', N'Bouvet Island', 30, 1 UNION ALL
SELECT 33, N'BW', N'Botswana', 29, 1 UNION ALL
SELECT 34, N'BY', N'Belarus', 21, 1 UNION ALL
SELECT 35, N'BZ', N'Belize', 23, 1 UNION ALL
SELECT 36, N'CA', N'Canada', 39, 1 UNION ALL
SELECT 37, N'CC', N'Cocos (Keeling) Islands', 47, 1 UNION ALL
SELECT 38, N'CD', N'Congo (Democratic Republic)', 51, 1 UNION ALL
SELECT 39, N'CF', N'Central African Republic', 42, 1 UNION ALL
SELECT 40, N'CG', N'Congo', 50, 1 UNION ALL
SELECT 41, N'CH', N'Switzerland', 209, 1 UNION ALL
SELECT 42, N'CI', N'Cote D&rsquo;Ivoire', 54, 1 UNION ALL
SELECT 43, N'CK', N'Cook Islands', 52, 1 UNION ALL
SELECT 44, N'CL', N'Chile', 44, 1 UNION ALL
SELECT 45, N'CM', N'Cameroon', 38, 1 UNION ALL
SELECT 46, N'CN', N'China', 45, 1 UNION ALL
SELECT 47, N'CO', N'Colombia', 48, 1 UNION ALL
SELECT 48, N'CR', N'Costa Rica', 53, 1 UNION ALL
SELECT 49, N'CU', N'Cuba', 56, 1 UNION ALL
SELECT 50, N'CV', N'Cape Verde', 40, 1
COMMIT;
RAISERROR (N'[dbo].[Country]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[Country]([CountryID], [Code], [Name], [SortOrder], [IsPrimary])
SELECT 51, N'CX', N'Christmas Island', 46, 1 UNION ALL
SELECT 52, N'CY', N'Cyprus', 57, 1 UNION ALL
SELECT 53, N'CZ', N'Czech Republic', 58, 1 UNION ALL
SELECT 54, N'DE', N'Germany', 82, 1 UNION ALL
SELECT 55, N'DJ', N'Djibouti', 60, 1 UNION ALL
SELECT 56, N'DK', N'Denmark', 59, 1 UNION ALL
SELECT 57, N'DM', N'Dominica', 61, 1 UNION ALL
SELECT 58, N'DO', N'Dominican Republic', 62, 1 UNION ALL
SELECT 59, N'DZ', N'Algeria', 4, 1 UNION ALL
SELECT 60, N'EC', N'Ecuador', 64, 1 UNION ALL
SELECT 61, N'EE', N'Estonia', 69, 1 UNION ALL
SELECT 62, N'EG', N'Egypt', 65, 1 UNION ALL
SELECT 63, N'EH', N'Western Sahara', 238, 1 UNION ALL
SELECT 64, N'ER', N'Eritrea', 68, 1 UNION ALL
SELECT 65, N'ES', N'Spain', 203, 1 UNION ALL
SELECT 66, N'ET', N'Ethiopia', 70, 1 UNION ALL
SELECT 67, N'FI', N'Finland', 74, 1 UNION ALL
SELECT 68, N'FJ', N'Fiji', 73, 1 UNION ALL
SELECT 69, N'FK', N'Falkland Islands', 71, 1 UNION ALL
SELECT 70, N'FM', N'Micronesia', 139, 1 UNION ALL
SELECT 71, N'FO', N'Faroe Islands', 73, 1 UNION ALL
SELECT 72, N'FR', N'France', 75, 1 UNION ALL
SELECT 73, N'FX', N'France (European Territory)', 76, 1 UNION ALL
SELECT 74, N'GA', N'Gabon', 79, 1 UNION ALL
SELECT 75, N'GB', N'United Kingdom', 227, 1 UNION ALL
SELECT 76, N'GD', N'Grenada', 87, 1 UNION ALL
SELECT 77, N'GE', N'Georgia', 81, 1 UNION ALL
SELECT 78, N'GF', N'French Guiana', 77, 1 UNION ALL
SELECT 79, N'GH', N'Ghana', 83, 1 UNION ALL
SELECT 80, N'GI', N'Gibraltar', 84, 1 UNION ALL
SELECT 81, N'GL', N'Greenland', 86, 1 UNION ALL
SELECT 82, N'GM', N'Gambia', 80, 1 UNION ALL
SELECT 83, N'GN', N'Guinea', 91, 1 UNION ALL
SELECT 84, N'GP', N'Guadeloupe', 88, 1 UNION ALL
SELECT 85, N'GQ', N'Equatorial Guinea', 67, 1 UNION ALL
SELECT 86, N'GR', N'Greece', 85, 1 UNION ALL
SELECT 87, N'GS', N'S. Georgia &amp; S. Sandwich Isls.', 182, 1 UNION ALL
SELECT 88, N'GT', N'Guatemala', 90, 1 UNION ALL
SELECT 89, N'GU', N'Guam', 89, 1 UNION ALL
SELECT 90, N'GW', N'Guinea Bissau', 92, 1 UNION ALL
SELECT 91, N'GY', N'Guyana', 93, 1 UNION ALL
SELECT 92, N'HK', N'Hong Kong', 98, 1 UNION ALL
SELECT 93, N'HM', N'Heard and McDonald Islands', 95, 1 UNION ALL
SELECT 94, N'HN', N'Honduras', 97, 1 UNION ALL
SELECT 95, N'HR', N'Croatia', 55, 1 UNION ALL
SELECT 96, N'HT', N'Haiti', 94, 1 UNION ALL
SELECT 97, N'HU', N'Hungary', 99, 1 UNION ALL
SELECT 98, N'ID', N'Indonesia', 102, 1 UNION ALL
SELECT 99, N'IE', N'Ireland', 105, 1 UNION ALL
SELECT 100, N'IL', N'Israel', 106, 1
COMMIT;
RAISERROR (N'[dbo].[Country]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[Country]([CountryID], [Code], [Name], [SortOrder], [IsPrimary])
SELECT 101, N'IN', N'India', 101, 1 UNION ALL
SELECT 102, N'IO', N'British Indian Ocean Territory', 32, 1 UNION ALL
SELECT 103, N'IQ', N'Iraq', 104, 1 UNION ALL
SELECT 104, N'IR', N'Iran', 103, 1 UNION ALL
SELECT 105, N'IS', N'Iceland', 100, 1 UNION ALL
SELECT 106, N'IT', N'Italy', 107, 1 UNION ALL
SELECT 107, N'JM', N'Jamaica', 108, 1 UNION ALL
SELECT 108, N'JO', N'Jordan', 110, 1 UNION ALL
SELECT 109, N'JP', N'Japan', 109, 1 UNION ALL
SELECT 110, N'KE', N'Kenya', 112, 1 UNION ALL
SELECT 111, N'KG', N'Kyrgyzstan', 115, 1 UNION ALL
SELECT 112, N'KH', N'Cambodia', 37, 1 UNION ALL
SELECT 113, N'KI', N'Kiribati', 113, 1 UNION ALL
SELECT 114, N'KM', N'Comoros', 49, 1 UNION ALL
SELECT 115, N'KN', N'Saint Kitts &amp; Nevis Anguilla', 184, 1 UNION ALL
SELECT 116, N'KP', N'North Korea', 160, 1 UNION ALL
SELECT 117, N'KR', N'South Korea', 202, 1 UNION ALL
SELECT 118, N'KW', N'Kuwait', 114, 1 UNION ALL
SELECT 119, N'KY', N'Cayman Islands', 41, 1 UNION ALL
SELECT 120, N'KZ', N'Kazakhstan', 111, 1 UNION ALL
SELECT 121, N'LA', N'Laos', 116, 1 UNION ALL
SELECT 122, N'LB', N'Lebanon', 118, 1 UNION ALL
SELECT 123, N'LC', N'Saint Lucia', 185, 1 UNION ALL
SELECT 124, N'LI', N'Liechtenstein', 122, 1 UNION ALL
SELECT 125, N'LK', N'Sri Lanka', 204, 1 UNION ALL
SELECT 126, N'LR', N'Liberia', 120, 1 UNION ALL
SELECT 127, N'LS', N'Lesotho', 119, 1 UNION ALL
SELECT 128, N'LT', N'Lithuania', 123, 1 UNION ALL
SELECT 129, N'LU', N'Luxembourg', 124, 1 UNION ALL
SELECT 130, N'LV', N'Latvia', 117, 1 UNION ALL
SELECT 131, N'LY', N'Libya', 121, 1 UNION ALL
SELECT 132, N'MA', N'Morocco', 145, 1 UNION ALL
SELECT 133, N'MC', N'Monaco', 141, 1 UNION ALL
SELECT 134, N'MD', N'Moldova', 140, 1 UNION ALL
SELECT 135, N'ME', N'Montenegro', 143, 1 UNION ALL
SELECT 136, N'MG', N'Madagascar', 127, 1 UNION ALL
SELECT 137, N'MH', N'Marshall Islands', 133, 1 UNION ALL
SELECT 138, N'MK', N'Macedonia', 126, 1 UNION ALL
SELECT 139, N'ML', N'Mali', 131, 1 UNION ALL
SELECT 140, N'MM', N'Myanmar', 147, 1 UNION ALL
SELECT 141, N'MN', N'Mongolia', 142, 1 UNION ALL
SELECT 142, N'MO', N'Macau', 125, 1 UNION ALL
SELECT 143, N'MP', N'Northern Mariana Islands', 161, 1 UNION ALL
SELECT 144, N'MQ', N'Martinique', 134, 1 UNION ALL
SELECT 145, N'MR', N'Mauritania', 135, 1 UNION ALL
SELECT 146, N'MS', N'Montserrat', 144, 1 UNION ALL
SELECT 147, N'MT', N'Malta', 132, 1 UNION ALL
SELECT 148, N'MU', N'Mauritius', 136, 1 UNION ALL
SELECT 149, N'MV', N'Maldives', 130, 1 UNION ALL
SELECT 150, N'MW', N'Malawi', 128, 1
COMMIT;
RAISERROR (N'[dbo].[Country]: Insert Batch: 3.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[Country]([CountryID], [Code], [Name], [SortOrder], [IsPrimary])
SELECT 151, N'MX', N'Mexico', 138, 1 UNION ALL
SELECT 152, N'MY', N'Malaysia', 129, 1 UNION ALL
SELECT 153, N'MZ', N'Mozambique', 146, 1 UNION ALL
SELECT 154, N'NA', N'Namibia', 148, 1 UNION ALL
SELECT 155, N'NC', N'New Caledonia', 153, 1 UNION ALL
SELECT 156, N'NE', N'Niger', 156, 1 UNION ALL
SELECT 157, N'NF', N'Norfolk Island', 159, 1 UNION ALL
SELECT 158, N'NG', N'Nigeria', 157, 1 UNION ALL
SELECT 159, N'NI', N'Nicaragua', 155, 1 UNION ALL
SELECT 160, N'NL', N'Netherlands', 151, 1 UNION ALL
SELECT 161, N'NO', N'Norway', 162, 1 UNION ALL
SELECT 162, N'NP', N'Nepal', 150, 1 UNION ALL
SELECT 163, N'NR', N'Nauru', 149, 1 UNION ALL
SELECT 164, N'NU', N'Niue', 158, 1 UNION ALL
SELECT 165, N'NZ', N'New Zealand', 154, 1 UNION ALL
SELECT 166, N'OM', N'Oman', 163, 1 UNION ALL
SELECT 167, N'PA', N'Panama', 167, 1 UNION ALL
SELECT 168, N'PE', N'Peru', 170, 1 UNION ALL
SELECT 169, N'PF', N'Polynesia', 174, 1 UNION ALL
SELECT 170, N'PG', N'Papua New Guinea', 168, 1 UNION ALL
SELECT 171, N'PH', N'Philippines', 171, 1 UNION ALL
SELECT 172, N'PK', N'Pakistan', 164, 1 UNION ALL
SELECT 173, N'PL', N'Poland', 173, 1 UNION ALL
SELECT 174, N'PM', N'Saint Pierre and Miquelon', 186, 1 UNION ALL
SELECT 175, N'PN', N'Pitcairn', 172, 1 UNION ALL
SELECT 176, N'PR', N'Puerto Rico', 176, 1 UNION ALL
SELECT 177, N'PS', N'Palestinian Territory', 166, 1 UNION ALL
SELECT 178, N'PT', N'Portugal', 175, 1 UNION ALL
SELECT 179, N'PW', N'Palau', 165, 1 UNION ALL
SELECT 180, N'PY', N'Paraguay', 169, 1 UNION ALL
SELECT 181, N'QA', N'Qatar', 177, 1 UNION ALL
SELECT 182, N'RE', N'Reunion', 178, 1 UNION ALL
SELECT 183, N'RO', N'Romania', 179, 1 UNION ALL
SELECT 184, N'RS', N'Serbia', 193, 1 UNION ALL
SELECT 185, N'RU', N'Russian Federation', 180, 1 UNION ALL
SELECT 186, N'RW', N'Rwanda', 181, 1 UNION ALL
SELECT 187, N'SA', N'Saudi Arabia', 191, 1 UNION ALL
SELECT 188, N'SB', N'Solomon Islands', 199, 1 UNION ALL
SELECT 189, N'SC', N'Seychelles', 194, 1 UNION ALL
SELECT 190, N'SD', N'Sudan', 205, 1 UNION ALL
SELECT 191, N'SE', N'Sweden', 208, 1 UNION ALL
SELECT 192, N'SG', N'Singapore', 196, 1 UNION ALL
SELECT 193, N'SH', N'Saint Helena', 183, 1 UNION ALL
SELECT 194, N'SI', N'Slovenia', 198, 1 UNION ALL
SELECT 195, N'SK', N'Slovakia', 197, 1 UNION ALL
SELECT 196, N'SL', N'Sierra Leone', 195, 1 UNION ALL
SELECT 197, N'SM', N'San Marino', 189, 1 UNION ALL
SELECT 198, N'SN', N'Senegal', 192, 1 UNION ALL
SELECT 199, N'SO', N'Somalia', 200, 1 UNION ALL
SELECT 200, N'SR', N'Suriname', 206, 1
COMMIT;
RAISERROR (N'[dbo].[Country]: Insert Batch: 4.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[Country]([CountryID], [Code], [Name], [SortOrder], [IsPrimary])
SELECT 201, N'ST', N'Sao Tome and Principe', 190, 1 UNION ALL
SELECT 202, N'SV', N'El Salvador', 66, 1 UNION ALL
SELECT 203, N'SY', N'Syrian Arab Republic', 210, 1 UNION ALL
SELECT 204, N'SZ', N'Swaziland', 207, 1 UNION ALL
SELECT 205, N'TC', N'Turks and Caicos Islands', 222, 1 UNION ALL
SELECT 206, N'TD', N'Chad', 43, 1 UNION ALL
SELECT 207, N'TF', N'French Southern Territories', 78, 1 UNION ALL
SELECT 208, N'TG', N'Togo', 215, 1 UNION ALL
SELECT 209, N'TH', N'Thailand', 214, 1 UNION ALL
SELECT 210, N'TJ', N'Tajikistan', 212, 1 UNION ALL
SELECT 211, N'TK', N'Tokelau', 216, 1 UNION ALL
SELECT 212, N'TM', N'Turkmenistan', 221, 1 UNION ALL
SELECT 213, N'TN', N'Tunisia', 219, 1 UNION ALL
SELECT 214, N'TO', N'Tonga', 217, 1 UNION ALL
SELECT 215, N'TP', N'East Timor', 63, 1 UNION ALL
SELECT 216, N'TR', N'Turkey', 220, 1 UNION ALL
SELECT 217, N'TT', N'Trinidad and Tobago', 218, 1 UNION ALL
SELECT 218, N'TV', N'Tuvalu', 223, 1 UNION ALL
SELECT 219, N'TW', N'Taiwan', 211, 1 UNION ALL
SELECT 220, N'TZ', N'Tanzania', 213, 1 UNION ALL
SELECT 221, N'UA', N'Ukraine', 225, 1 UNION ALL
SELECT 222, N'UG', N'Uganda', 224, 1 UNION ALL
SELECT 223, N'UM', N'USA Minor Outlying Islands', 230, 1 UNION ALL
SELECT 224, N'US', N'United States', 1, 1 UNION ALL
SELECT 225, N'UY', N'Uruguay', 229, 1 UNION ALL
SELECT 226, N'UZ', N'Uzbekistan', 231, 1 UNION ALL
SELECT 227, N'VA', N'Holy See (Vatican)', 96, 1 UNION ALL
SELECT 228, N'VC', N'Saint Vincent &amp; Grenadines', 187, 1 UNION ALL
SELECT 229, N'VE', N'Venezuela', 233, 1 UNION ALL
SELECT 230, N'VG', N'Virgin Islands (British)', 235, 1 UNION ALL
SELECT 231, N'VI', N'Virgin Islands (USA)', 236, 1 UNION ALL
SELECT 232, N'VN', N'Vietnam', 234, 1 UNION ALL
SELECT 233, N'VU', N'Vanuatu', 232, 1 UNION ALL
SELECT 234, N'WF', N'Wallis and Futuna Islands', 237, 1 UNION ALL
SELECT 235, N'WS', N'Samoa', 188, 1 UNION ALL
SELECT 236, N'YE', N'Yemen', 239, 1 UNION ALL
SELECT 237, N'YT', N'Mayotte', 137, 1 UNION ALL
SELECT 238, N'ZA', N'South Africa', 201, 1 UNION ALL
SELECT 239, N'ZM', N'Zambia', 241, 1 UNION ALL
SELECT 240, N'ZR', N'Zaire', 240, 1 UNION ALL
SELECT 241, N'ZW', N'Zimbabwe', 242, 1 UNION ALL
SELECT 243, N'US', N'United States of America', NULL, NULL UNION ALL
SELECT 244, N'US', N'United States', NULL, NULL UNION ALL
SELECT 245, N'US', N'USA', NULL, NULL UNION ALL
SELECT 246, N'US', N'U. S. ', NULL, NULL UNION ALL
SELECT 247, N'US', N'U. S. A. ', NULL, NULL UNION ALL
SELECT 248, N'US', N'America', NULL, NULL UNION ALL
SELECT 249, N'GB', N'UK', NULL, NULL UNION ALL
SELECT 250, N'GB', N'England', NULL, NULL UNION ALL
SELECT 251, N'GB', N'England (UK)', NULL, NULL
COMMIT;
RAISERROR (N'[dbo].[Country]: Insert Batch: 5.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[Country]([CountryID], [Code], [Name], [SortOrder], [IsPrimary])
SELECT 252, N'GB', N'Great Britain', NULL, NULL UNION ALL
SELECT 254, N'AU', N'Western Australia', NULL, NULL UNION ALL
SELECT 255, N'AU', N'Western AU', NULL, NULL UNION ALL
SELECT 256, N'DE', N'Federal Republic of Germany', NULL, NULL UNION ALL
SELECT 257, N'BH', N'Kingdom of Bahrain', NULL, NULL UNION ALL
SELECT 258, N'KR', N'Korea', NULL, NULL UNION ALL
SELECT 259, N'KR', N'Republic of Korea', NULL, NULL UNION ALL
SELECT 260, N'IE', N'Northern Ireland', NULL, NULL UNION ALL
SELECT 261, N'CN', N'People''s Republic of China', NULL, NULL UNION ALL
SELECT 262, N'CN', N'ROC', NULL, NULL UNION ALL
SELECT 263, N'CN', N'R. O. C.', NULL, NULL UNION ALL
SELECT 264, N'TW', N'Taiwan R.O.C.', NULL, NULL UNION ALL
SELECT 265, N'TW', N'Taiwan ROC', NULL, NULL UNION ALL
SELECT 266, N'TW', N'Taiwan, China', NULL, NULL UNION ALL
SELECT 267, N'TW', N'Taiwan, R.O.C.', NULL, NULL UNION ALL
SELECT 268, N'TW', N'Taiwan China', NULL, NULL UNION ALL
SELECT 269, N'PA', N'Republic of Panama', NULL, NULL UNION ALL
SELECT 270, N'RW', N'Republic of Rwanda', NULL, NULL UNION ALL
SELECT 271, N'ZA', N'Republic of South Africa', NULL, NULL UNION ALL
SELECT 272, N'RU', N'Russia', NULL, NULL UNION ALL
SELECT 273, N'NL', N'The Netherlands', NULL, NULL
COMMIT;
RAISERROR (N'[dbo].[Country]: Insert Batch: 6.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[Country] OFF;

GO
PRINT N'Creating [dbo].[MemoApproval]...';


GO
CREATE TABLE [dbo].[MemoApproval] (
    [ScrMemID]         INT          NOT NULL,
    [ApprovalTypeID]   INT          NOT NULL,
    [Acknowledged]     BIT          NULL,
    [AcknowledgedDate] DATETIME     NULL,
    [Approved]         BIT          NULL,
    [ApprovedDate]     DATETIME     NULL,
    [UserID]           INT          NULL,
    [Signature]        VARCHAR (50) NULL,
    [ARSignature]      VARCHAR (50) NULL,
    CONSTRAINT [PK_MemoApproval] PRIMARY KEY CLUSTERED ([ScrMemID] ASC, [ApprovalTypeID] ASC)
);


GO
PRINT N'Creating [dbo].[MemoConflict]...';


GO
CREATE TABLE [dbo].[MemoConflict] (
    [ScrMemID]   INT NOT NULL,
    [ConflictID] INT NOT NULL,
    [Answer]     BIT NOT NULL,
    CONSTRAINT [PK_MemoConflict] PRIMARY KEY CLUSTERED ([ScrMemID] ASC, [ConflictID] ASC)
);


GO
PRINT N'Creating [dbo].[RequestConflict]...';


GO
CREATE TABLE [dbo].[RequestConflict] (
    [ReqID]      INT NOT NULL,
    [ConflictID] INT NOT NULL,
    [Answer]     BIT NOT NULL,
    CONSTRAINT [PK_RequestConflict] PRIMARY KEY CLUSTERED ([ReqID] ASC, [ConflictID] ASC)
);


GO
PRINT N'Creating [dbo].[State]...';


GO
CREATE TABLE [dbo].[State] (
    [StateID]   INT           IDENTITY (1, 1) NOT NULL,
    [Code]      NVARCHAR (50) NOT NULL,
    [Name]      NVARCHAR (50) NOT NULL,
    [SortOrder] INT           NULL,
    CONSTRAINT [PK_State] PRIMARY KEY CLUSTERED ([StateID] ASC)
);

SET IDENTITY_INSERT [dbo].[State] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[State]([StateID], [Code], [Name], [SortOrder])
SELECT 1, N'AK', N'Alaska', 2 UNION ALL
SELECT 2, N'AL', N'Alabama', 1 UNION ALL
SELECT 3, N'AR', N'Arkansas', 4 UNION ALL
SELECT 4, N'AZ', N'Arizona', 3 UNION ALL
SELECT 5, N'CA', N'California', 5 UNION ALL
SELECT 6, N'CO', N'Colorado', 6 UNION ALL
SELECT 7, N'CT', N'Connecticut', 7 UNION ALL
SELECT 8, N'DC', N'District of Columbia', 9 UNION ALL
SELECT 9, N'DE', N'Delaware', 8 UNION ALL
SELECT 10, N'FL', N'Florida', 10 UNION ALL
SELECT 11, N'GA', N'Georgia', 11 UNION ALL
SELECT 12, N'HI', N'Hawaii', 12 UNION ALL
SELECT 13, N'IA', N'Iowa', 16 UNION ALL
SELECT 14, N'ID', N'Idaho', 13 UNION ALL
SELECT 15, N'IL', N'Illinois', 14 UNION ALL
SELECT 16, N'IN', N'Indiana', 15 UNION ALL
SELECT 17, N'KS', N'Kansas', 17 UNION ALL
SELECT 18, N'KY', N'Kentucky', 18 UNION ALL
SELECT 19, N'LA', N'Louisiana', 19 UNION ALL
SELECT 20, N'MA', N'Massachusetts', 22 UNION ALL
SELECT 21, N'MD', N'Maryland', 21 UNION ALL
SELECT 22, N'ME', N'Maine', 20 UNION ALL
SELECT 23, N'MI', N'Michigan', 23 UNION ALL
SELECT 24, N'MN', N'Minnesota', 24 UNION ALL
SELECT 25, N'MO', N'Missouri', 26 UNION ALL
SELECT 26, N'MS', N'Mississippi', 25 UNION ALL
SELECT 27, N'MT', N'Montana', 27 UNION ALL
SELECT 28, N'NC', N'North Carolina', 34 UNION ALL
SELECT 29, N'ND', N'North Dakota', 35 UNION ALL
SELECT 30, N'NE', N'Nebraska', 28 UNION ALL
SELECT 31, N'NH', N'New Hampshire', 30 UNION ALL
SELECT 32, N'NJ', N'New Jersey', 31 UNION ALL
SELECT 33, N'NM', N'New Mexico', 32 UNION ALL
SELECT 34, N'NV', N'Nevada', 29 UNION ALL
SELECT 35, N'NY', N'New York', 33 UNION ALL
SELECT 36, N'OH', N'Ohio', 36 UNION ALL
SELECT 37, N'OK', N'Oklahoma', 37 UNION ALL
SELECT 38, N'OR', N'Oregon', 38 UNION ALL
SELECT 39, N'PA', N'Pennsylvania', 39 UNION ALL
SELECT 40, N'RI', N'Rhode Island', 40 UNION ALL
SELECT 41, N'SC', N'South Carolina', 41 UNION ALL
SELECT 42, N'SD', N'South Dakota', 42 UNION ALL
SELECT 43, N'TN', N'Tennessee', 43 UNION ALL
SELECT 44, N'TX', N'Texas', 44 UNION ALL
SELECT 45, N'UT', N'Utah', 45 UNION ALL
SELECT 46, N'VA', N'Virginia', 47 UNION ALL
SELECT 47, N'VT', N'Vermont', 46 UNION ALL
SELECT 48, N'WA', N'Washington', 48 UNION ALL
SELECT 49, N'WI', N'Wisconsin', 50 UNION ALL
SELECT 50, N'WV', N'West Virginia', 49
COMMIT;
RAISERROR (N'[dbo].[State]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[State]([StateID], [Code], [Name], [SortOrder])
SELECT 51, N'WY', N'Wyoming', 51 UNION ALL
SELECT 52, N'', N'None', 0
COMMIT;
RAISERROR (N'[dbo].[State]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[State] OFF;

GO
PRINT N'Updating Obsolete Pro Bono Memo Type...';

GO
UPDATE [dbo].[MemoTypes]
SET [MemoType] = 'Pro Bono (Obsolete)'
WHERE [MemoType] = 'Pro Bono'
	AND [TrackNumber] = 4


GO
PRINT N'Creating FK_DocketingTeam_Memo...';


GO
ALTER TABLE [dbo].[DocketingTeam] WITH NOCHECK
    ADD CONSTRAINT [FK_DocketingTeam_Memo] FOREIGN KEY ([ScrMemID]) REFERENCES [dbo].[Memo] ([ScrMemID]);


GO
PRINT N'Creating FK_Billing_Memo...';


GO
ALTER TABLE [dbo].[Billing] WITH NOCHECK
    ADD CONSTRAINT [FK_Billing_Memo] FOREIGN KEY ([ScrMemID]) REFERENCES [dbo].[Memo] ([ScrMemID]);


GO
PRINT N'Creating FK_Party_Memo...';


GO
ALTER TABLE [dbo].[Party] WITH NOCHECK
    ADD CONSTRAINT [FK_Party_Memo] FOREIGN KEY ([ScrMemID]) REFERENCES [dbo].[Memo] ([ScrMemID]);


GO
PRINT N'Creating FK_NewClient_Memo...';


GO
ALTER TABLE [dbo].[NewClient] WITH NOCHECK
    ADD CONSTRAINT [FK_NewClient_Memo] FOREIGN KEY ([ScrMemID]) REFERENCES [dbo].[Memo] ([ScrMemID]);


GO
PRINT N'Creating FK_Files_Memo...';


GO
ALTER TABLE [dbo].[Files] WITH NOCHECK
    ADD CONSTRAINT [FK_Files_Memo] FOREIGN KEY ([ScrMemID]) REFERENCES [dbo].[Memo] ([ScrMemID]);


GO
PRINT N'Creating FK_Tracking_Memo...';


GO
ALTER TABLE [dbo].[Tracking] WITH NOCHECK
    ADD CONSTRAINT [FK_Tracking_Memo] FOREIGN KEY ([ScrMemID]) REFERENCES [dbo].[Memo] ([ScrMemID]);


GO
PRINT N'Creating FK_Address_Memo...';


GO
ALTER TABLE [dbo].[Address] WITH NOCHECK
    ADD CONSTRAINT [FK_Address_Memo] FOREIGN KEY ([ScrMemID]) REFERENCES [dbo].[Memo] ([ScrMemID]);


GO
PRINT N'Creating FK_ClientContact_Memo...';


GO
ALTER TABLE [dbo].[ClientContact] WITH NOCHECK
    ADD CONSTRAINT [FK_ClientContact_Memo] FOREIGN KEY ([ScrMemID]) REFERENCES [dbo].[Memo] ([ScrMemID]);


GO
PRINT N'Creating FK_Attachments_Memo...';


GO
ALTER TABLE [dbo].[Attachments] WITH NOCHECK
    ADD CONSTRAINT [FK_Attachments_Memo] FOREIGN KEY ([ScrMemID]) REFERENCES [dbo].[Memo] ([ScrMemID]);


GO
PRINT N'Creating FK_Staffing_Memo...';


GO
ALTER TABLE [dbo].[Staffing] WITH NOCHECK
    ADD CONSTRAINT [FK_Staffing_Memo] FOREIGN KEY ([ScrMemID]) REFERENCES [dbo].[Memo] ([ScrMemID]);


GO
PRINT N'Creating FK_FeeSplitStaff_Memo...';


GO
ALTER TABLE [dbo].[FeeSplitStaff] WITH NOCHECK
    ADD CONSTRAINT [FK_FeeSplitStaff_Memo] FOREIGN KEY ([ScrMemID]) REFERENCES [dbo].[Memo] ([ScrMemID]);


GO
PRINT N'Creating FK_ClientMatterNumbers_Memo...';


GO
ALTER TABLE [dbo].[ClientMatterNumbers] WITH NOCHECK
    ADD CONSTRAINT [FK_ClientMatterNumbers_Memo] FOREIGN KEY ([ScrMemID]) REFERENCES [dbo].[Memo] ([ScrMemID]);


GO
PRINT N'Creating FK_NDriveAccess_Memo...';


GO
ALTER TABLE [dbo].[NDriveAccess] WITH NOCHECK
    ADD CONSTRAINT [FK_NDriveAccess_Memo] FOREIGN KEY ([ScrMemID]) REFERENCES [dbo].[Memo] ([ScrMemID]);


GO
PRINT N'Creating FK_MemoConflict_Conflict...';


GO
ALTER TABLE [dbo].[MemoConflict] WITH NOCHECK
    ADD CONSTRAINT [FK_MemoConflict_Conflict] FOREIGN KEY ([ConflictID]) REFERENCES [dbo].[Conflict] ([ConflictID]);


GO
PRINT N'Creating FK_MemoConflict_Memo...';


GO
ALTER TABLE [dbo].[MemoConflict] WITH NOCHECK
    ADD CONSTRAINT [FK_MemoConflict_Memo] FOREIGN KEY ([ScrMemID]) REFERENCES [dbo].[Memo] ([ScrMemID]);


GO
PRINT N'Creating FK_RequestConflict_Conflict...';


GO
ALTER TABLE [dbo].[RequestConflict] WITH NOCHECK
    ADD CONSTRAINT [FK_RequestConflict_Conflict] FOREIGN KEY ([ConflictID]) REFERENCES [dbo].[Conflict] ([ConflictID]);


GO
PRINT N'Creating FK_RequestConflict_Requests...';


GO
ALTER TABLE [dbo].[RequestConflict] WITH NOCHECK
    ADD CONSTRAINT [FK_RequestConflict_Requests] FOREIGN KEY ([ReqID]) REFERENCES [dbo].[Requests] ([ReqID]);


GO
PRINT N'Creating FK_OpposingCounsel_Memo...';


GO
ALTER TABLE [dbo].[OpposingCounsel] WITH NOCHECK
    ADD CONSTRAINT [FK_OpposingCounsel_Memo] FOREIGN KEY ([ScrMemID]) REFERENCES [dbo].[Memo] ([ScrMemID]);


GO
PRINT N'Creating [dbo].[TRG_OFFICE_PGROUP_CHANGE]...';


GO
-- =============================================
-- Author:		unknown
-- Create date: unknown
-- Description:	keep praccode and respattofficecode fields in sync between 
--              memo and notifications tables
--              blmcgee@hollandhart.com 2013-01-09
--              removed logic error where updates were assumed to only 
--              affect one row at a time
-- =============================================
CREATE TRIGGER [dbo].[TRG_OFFICE_PGROUP_CHANGE]
   ON  [dbo].[Memo]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

    IF (UPDATE (PracCode)) OR (UPDATE (RespAttOfficeCode))
    BEGIN
		UPDATE Notifications
		SET Notifications.Office = CAST(Inserted.RespAttOfficeCode AS INT),
			Notifications.PracCode = Inserted.PracCode
		FROM Notifications
		INNER JOIN Inserted on Inserted.ScrMemID = Notifications.ScrMemID
	END
END
GO
PRINT N'Refreshing [dbo].[VW_UserPageNavigation]...';


GO
EXECUTE sp_refreshsqlmodule N'dbo.VW_UserPageNavigation';


GO
PRINT N'Creating [dbo].[VW_MemoApproval]...';

GO

CREATE VIEW [dbo].[VW_MemoApproval] as 

SELECT [TrackingID]
	,[ScrMemID]
	,'Financial' as [ApprovalType]
	,[FinAcknowledged] as [Acknowledged]
	,[FinApproved] as [Approved]
	,[FinUserID] as [UserID]
	,[FinSignature] as [Signature]
	,[FinDate] as [Date]
	,[FinNotes] as [Notes]
	,[FinRejectionReason] as [RejectionReason]
	,[FinARSignature] as [ARSignature]
	,[FinExceptionSignature] as [ExceptionSignature]
FROM [dbo].[Tracking]

UNION ALL

SELECT [TrackingID]
	,[ScrMemID]
	,'AP' as [ApprovalType]
	,[APAcknowledged] as [Acknowledged]
	,[APApproved] as [Approved]
	,[APUserID] as [UserID]
	,[APSignature] as [Signature]
	,[APDate] as [Date]
	,[APNotes] as [Notes]
	,[APRejectionReason] as [RejectionReason]
	,[APARSignature] as [Signature]
	,[APExceptionSignature] as [ExceptionSignature]
FROM [dbo].[Tracking]

UNION ALL

SELECT [TrackingID]
	,[ScrMemID]
	,'PGM' as [ApprovalType]
	,[PGMAcknowledged] as [Acknowledged]
	,[PGMApproved] as [Approved]
	,[PGMUserID] as [UserID]
	,[PGMSignature] as [Signature]
	,[PGMDate] as [PGMDate]
	,[PGMNotes] as [Notes]
	,[PGMRejectionReason] as [RejectionReason]
	,[PGMARSignature] as [ARSignature]
	,[PGMExceptionSignature] as [ExceptionSignature]
FROM [dbo].[Tracking]

UNION ALL

SELECT [TrackingID]
	,[ScrMemID]
	,'PGM2' as [ApprovalType]
	,[PGM2Acknowledged] as [Acknowledged]
	,[PGM2Approved] as [Approved]
	,[PGM2UserID] as [UserID]
	,[PGM2Signature] as [Signature]
	,[PGM2Date] as [Date]
	,[PGM2Notes] as [Notes]
	,[PGM2RejectionReason] as [RejectionReason]
	,[PGM2ARSignature] as [ARSignataure]
	,[PGM2ExceptionSignature] as [ExceptionSignature]
FROM [dbo].[Tracking]

UNION ALL

SELECT [TrackingID]
	,[ScrMemID]
	, 'Adversary' as [ApprovalType]
	,[AdversaryAcknowledged] as [Acknowledged]
	,[AdversaryAdminApproved] as [Approved]
	,[AdversaryUserID] as [UserID]
	,[AdversarySignature] as [Signature]
	,[AdversaryAdminDate] as [Date]
	,NULL as [Notes]
	,[AdversaryRejectionReason] as [RejectionReason]
	,NULL as [ARSignataure]
	,NULL as [ExceptionSignature]
FROM [dbo].[Tracking]
GO
PRINT N'Altering [dbo].[PR_SET_ClientAffiliates]...';


GO
ALTER PROC [dbo].[PR_SET_ClientAffiliates]
(
	@ClientAffiliateID INT,
	@CName varchar(255) = null,
	@LName varchar(50) = null,
	@FName varchar(50) = null,
	@MName varchar(50) = null,
	@ContactName nvarchar(50) = null,
	@ContactTitle nvarchar(50) = null,
	@RelationshipCode nvarchar(50) = null,
	@NwClientID INT
)
AS

IF (@ClientAffiliateID < 1)
BEGIN
	INSERT INTO [dbo].[ClientAffiliates]
           ([CName]
           ,[LName]
           ,[FName]
           ,[MName]
           ,[ContactName]
           ,[ContactTitle]
           ,[RelationshipCode]
           ,[NwClientID])
     VALUES
           (@CName
           ,@LName
           ,@FName
           ,@MName
           ,@ContactName
           ,@ContactTitle
           ,@RelationshipCode
           ,@NwClientID)
           SELECT @ClientAffiliateID = @@IDENTITY
END
ELSE
BEGIN
	UPDATE [dbo].[ClientAffiliates]
   SET [CName] = @CName
      ,[LName] = @LName
      ,[FName] = @FName
      ,[MName] = @MName
      ,[ContactName] = @ContactName
      ,[ContactTitle] = @ContactTitle
      ,[RelationshipCode] = @RelationshipCode
 WHERE ClientAffiliateID = @ClientAffiliateID
 
 SELECT * FROM [dbo].[ClientAffiliates] WHERE ClientAffiliateID = @ClientAffiliateID
END
GO
PRINT N'Refreshing [dbo].[PR_DELETE_Address]...';


GO
EXECUTE sp_refreshsqlmodule N'dbo.PR_DELETE_Address';


GO
PRINT N'Refreshing [dbo].[PR_GET_Address]...';


GO
EXECUTE sp_refreshsqlmodule N'dbo.PR_GET_Address';


GO
PRINT N'Refreshing [dbo].[PR_SEARCH_Address]...';


GO
EXECUTE sp_refreshsqlmodule N'dbo.PR_SEARCH_Address';


GO
PRINT N'Refreshing [dbo].[PR_SET_Address]...';


GO
EXECUTE sp_refreshsqlmodule N'dbo.PR_SET_Address';


GO
PRINT N'Refreshing [dbo].[PR_DELETE_Memo]...';


GO
EXECUTE sp_refreshsqlmodule N'dbo.PR_DELETE_Memo';


GO
PRINT N'Refreshing [dbo].[PR_GET_Memo]...';


GO
EXECUTE sp_refreshsqlmodule N'dbo.PR_GET_Memo';


GO
PRINT N'Refreshing [dbo].[PR_GET_OpenMemos]...';


GO
EXECUTE sp_refreshsqlmodule N'dbo.PR_GET_OpenMemos';


GO
PRINT N'Refreshing [dbo].[PR_SEARCH_ClientMatterNumbers]...';


GO
EXECUTE sp_refreshsqlmodule N'dbo.PR_SEARCH_ClientMatterNumbers';


GO
PRINT N'Refreshing [dbo].[PR_SEARCH_Memo]...';


GO
EXECUTE sp_refreshsqlmodule N'dbo.PR_SEARCH_Memo';


GO
PRINT N'Refreshing [dbo].[PR_SELECT_FINAL_NOTIFICATIONS]...';


GO
EXECUTE sp_refreshsqlmodule N'dbo.PR_SELECT_FINAL_NOTIFICATIONS';


GO
PRINT N'Refreshing [dbo].[PR_SELECT_FOURTH_NOTIFICATIONS]...';


GO
EXECUTE sp_refreshsqlmodule N'dbo.PR_SELECT_FOURTH_NOTIFICATIONS';


GO
PRINT N'Refreshing [dbo].[PR_SELECT_SECOND_NOTIFICATIONS]...';


GO
EXECUTE sp_refreshsqlmodule N'dbo.PR_SELECT_SECOND_NOTIFICATIONS';


GO
PRINT N'Refreshing [dbo].[PR_SELECT_THIRD_NOTIFICATIONS]...';


GO
EXECUTE sp_refreshsqlmodule N'dbo.PR_SELECT_THIRD_NOTIFICATIONS';


GO
PRINT N'Refreshing [dbo].[PR_SET_Memo]...';


GO
EXECUTE sp_refreshsqlmodule N'dbo.PR_SET_Memo';


GO
PRINT N'Refreshing [dbo].[PR_DELETE_Tracking]...';


GO
EXECUTE sp_refreshsqlmodule N'dbo.PR_DELETE_Tracking';


GO
PRINT N'Refreshing [dbo].[PR_GET_Tracking]...';


GO
EXECUTE sp_refreshsqlmodule N'dbo.PR_GET_Tracking';


GO
PRINT N'Refreshing [dbo].[PR_SEARCH_Tracking]...';


GO
EXECUTE sp_refreshsqlmodule N'dbo.PR_SEARCH_Tracking';


GO
PRINT N'Refreshing [dbo].[PR_SET_Tracking]...';


GO
EXECUTE sp_refreshsqlmodule N'dbo.PR_SET_Tracking';


GO
PRINT N'Checking existing data against newly created constraints';


GO
ALTER TABLE [dbo].[DocketingTeam] WITH CHECK CHECK CONSTRAINT [FK_DocketingTeam_Memo];

ALTER TABLE [dbo].[Billing] WITH CHECK CHECK CONSTRAINT [FK_Billing_Memo];

ALTER TABLE [dbo].[Party] WITH CHECK CHECK CONSTRAINT [FK_Party_Memo];

ALTER TABLE [dbo].[NewClient] WITH CHECK CHECK CONSTRAINT [FK_NewClient_Memo];

ALTER TABLE [dbo].[Files] WITH CHECK CHECK CONSTRAINT [FK_Files_Memo];

ALTER TABLE [dbo].[Tracking] WITH CHECK CHECK CONSTRAINT [FK_Tracking_Memo];

ALTER TABLE [dbo].[Address] WITH CHECK CHECK CONSTRAINT [FK_Address_Memo];

ALTER TABLE [dbo].[ClientContact] WITH CHECK CHECK CONSTRAINT [FK_ClientContact_Memo];

ALTER TABLE [dbo].[Attachments] WITH CHECK CHECK CONSTRAINT [FK_Attachments_Memo];

ALTER TABLE [dbo].[Staffing] WITH CHECK CHECK CONSTRAINT [FK_Staffing_Memo];

ALTER TABLE [dbo].[FeeSplitStaff] WITH CHECK CHECK CONSTRAINT [FK_FeeSplitStaff_Memo];

ALTER TABLE [dbo].[ClientMatterNumbers] WITH CHECK CHECK CONSTRAINT [FK_ClientMatterNumbers_Memo];

ALTER TABLE [dbo].[NDriveAccess] WITH CHECK CHECK CONSTRAINT [FK_NDriveAccess_Memo];

ALTER TABLE [dbo].[MemoConflict] WITH CHECK CHECK CONSTRAINT [FK_MemoConflict_Conflict];

ALTER TABLE [dbo].[MemoConflict] WITH CHECK CHECK CONSTRAINT [FK_MemoConflict_Memo];

ALTER TABLE [dbo].[RequestConflict] WITH CHECK CHECK CONSTRAINT [FK_RequestConflict_Conflict];

ALTER TABLE [dbo].[RequestConflict] WITH CHECK CHECK CONSTRAINT [FK_RequestConflict_Requests];

ALTER TABLE [dbo].[OpposingCounsel] WITH CHECK CHECK CONSTRAINT [FK_OpposingCounsel_Memo];


GO
PRINT N'Update complete.'
GO
