﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HH.UI.ModalPopupForm
{
    public enum ModalFormButtonMode
    {
        None,
        SubmitCancel,
        SubmitOnly,
        OkCancel,
        OkOnly,
        YesNo
    }


    [ControlBuilderAttribute(typeof(ModalPanelBuilder)), ParseChildren(false)]
    public class ModalForm : WebControl, IModalForm, INamingContainer
    {
        private AjaxControlToolkit.ModalPopupExtender _modal = new AjaxControlToolkit.ModalPopupExtender();
        //private Panel _pnlMain = new Panel();
        private Panel _pnlForm = new Panel();
        private PlaceHolder _phContent = new PlaceHolder();
        private Panel _pnlButtons = new Panel();
        private LinkButton _btnOk = new LinkButton();
        private LinkButton _btnCancel = new LinkButton();
        private Button _btnHidden = new Button();

        public event ModalFormEventHandler OkClick;
        public event ModalFormEventHandler CancelClick;

        public ModalFormButtonMode ButtonMode { get; set; }
        public string TargetControlID { get; set; }
        public string OkControlID { get; set; }
        public string OnOkScript { get; set; }
        public string OnCancelScript { get; set; }
        public string CancelControlID { get; set; }
        public string BackgroundCssClass { get; set; }
        public AjaxControlToolkit.ModalPopupRepositionMode RepositionMode { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public string ContentCssClass { get; set; }
        public Unit Width { get; set; }
        public Unit Height { get; set; }
        public string ButtonCssClass { get; set; }
        public string BehaviorID 
        {
            get
            {
                return _modal.BehaviorID;
            }
            set
            {
                _modal.BehaviorID = value;
            }
        }
        public string ModalPopupExtenderID
        {
            get
            {
                return _modal.ID;
            }
        }

        /// <summary>
        /// Custom ASP.Net Modal Popup control, encapsulates AjaxControlToolkit ModalPopupExtender functionality
        /// </summary>
        public ModalForm()
        {
            //set some defaults here
            ButtonMode = ModalFormButtonMode.None;
            BackgroundCssClass = "modalFormDisablePane";
            RepositionMode = AjaxControlToolkit.ModalPopupRepositionMode.RepositionOnWindowResizeAndScroll;
            X = -1;
            Y = 150;
            ContentCssClass = "modalFormPane";
            //Width = Unit.Percentage(75);
            ButtonCssClass = "sbControl";
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            
            if (string.IsNullOrEmpty(this.ID))
                throw new Exception("The modal form user control could not be initialized. No ID was specified for the control.");
            
            //_pnlMain.ID = "pnlMain___" + this.ID;
            _pnlForm.ID = "pnlContentForm___" + this.ID;
            _phContent.ID = "phContentContainer___" + this.ID;

            _btnOk.ID = "btnSubmit___" + this.ID;
            _btnOk.CommandName = "OK";
            _btnOk.Click += new EventHandler(this.btn_Click);
            
            _btnCancel.ID = "btnCancel___" + this.ID;
            _btnCancel.CommandName = "Cancel";
            _btnCancel.Click += new EventHandler(this.btn_Click);

            _btnHidden.ID = "btnHidden___" + this.ID;
            _btnHidden.CommandName = "Hidden";
            _btnHidden.Style.Add("visibility", "hidden");
            _btnHidden.Width = Unit.Pixel(1);
            _btnHidden.Height = Unit.Pixel(1);

            _modal.ID = "modal___" + this.ID;
            _modal.PopupControlID = _pnlForm.ID;

            _pnlForm.Controls.Add(_phContent);
            _pnlButtons.Controls.Add(new LiteralControl("<br/>"));
            _pnlButtons.Controls.Add(new LiteralControl("<div style='text-align: center;'>"));
            _pnlButtons.Controls.Add(_btnOk);
            _pnlButtons.Controls.Add(new LiteralControl("&nbsp;&nbsp;&nbsp;"));
            _pnlButtons.Controls.Add(_btnCancel);
            _pnlButtons.Controls.Add(new LiteralControl("<br/>"));
            _pnlButtons.Controls.Add(new LiteralControl("<br/>"));
            _pnlForm.Controls.Add(_pnlButtons);

            base.Controls.Add(_modal);//_pnlMain.Controls.Add(_modal);
            base.Controls.Add(_pnlForm);
            base.Controls.Add(_btnHidden);
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (string.IsNullOrEmpty(TargetControlID))
                TargetControlID = _btnHidden.ID;
                //throw new Exception("The modal form user control with ID " + this.ID + " could not be initialized. No target control ID was specified for the control.");

            _modal.TargetControlID = TargetControlID;
            _modal.BackgroundCssClass = BackgroundCssClass;
            _modal.RepositionMode = RepositionMode;
            _modal.X = X;
            _modal.Y = Y;

            switch (ButtonMode)
            {
                case ModalFormButtonMode.None:
                    _pnlButtons.Visible = false;
                    break;
                case ModalFormButtonMode.OkCancel:
                    _btnOk.Text = "OK";
                    _btnCancel.Text = "Cancel";
                    break;
                case ModalFormButtonMode.OkOnly:
                    _btnOk.Text = "OK";
                    _btnCancel.Visible = false;
                    break;
                case ModalFormButtonMode.SubmitCancel:
                    _btnOk.Text = "Submit";
                    _btnCancel.Text = "Cancel";
                    break;
                case ModalFormButtonMode.SubmitOnly:
                    _btnOk.Text = "Submit";
                    _btnCancel.Visible = false;
                    break;
                case ModalFormButtonMode.YesNo:
                    _btnOk.Text = "Yes";
                    _btnCancel.Text = "No";
                    break;
            }

            if (!string.IsNullOrEmpty(OkControlID))
                _modal.OkControlID = OkControlID;
            if (!string.IsNullOrEmpty(CancelControlID))
                _modal.CancelControlID = CancelControlID;
            else if (CancelClick == null && (ButtonMode == ModalFormButtonMode.OkCancel || 
                                             ButtonMode == ModalFormButtonMode.SubmitCancel ||
                                             ButtonMode == ModalFormButtonMode.YesNo))
                _modal.CancelControlID = _btnCancel.ID;
            
            _modal.OnOkScript = OnOkScript;
            _modal.OnCancelScript = OnCancelScript;

            _pnlForm.CssClass = ContentCssClass;
            _pnlForm.Width = Width;
            _pnlForm.Height = Height;
            _pnlForm.Style.Add("display", "none");

            base.OnPreRender(e);
        }

        protected override void AddedControl(Control control, int index)
        {
            if (control == _pnlForm || control == _btnHidden || control == _modal)
            {
                base.AddedControl(control, index);
            }
            else
            {
                if (control is Panel || 
                    control is PlaceHolder ||
                    control is UpdatePanel)
                {
                    Controls.Remove(control);
                    _phContent.Controls.Add(control);
                }
                else
                {
                    throw new ArgumentException("Could not parse control " + control.ID + " of type " + control.GetType() + ". Only controls of type Panel or PlaceHolder are allowed inside ModalForm tags.");
                }
            }
        }

        public void AddContent(Control c)
        {
            _phContent.Controls.Add(c);
        }

        public void AddContentAt(int index, Control c)
        {
            _phContent.Controls.AddAt(index, c);
        }

        void btn_Click(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;

            if (OkClick != null && lb.CommandName == "OK")
                OkClick(this, new ModalFormEventArgs(ModalFormEvent.OK));
            else if (CancelClick != null && lb.CommandName == "Cancel")
                CancelClick(this, new ModalFormEventArgs(ModalFormEvent.Cancel));
        }

        public void Show()
        {
            _modal.Show();
        }

        public void Hide()
        {
            _modal.Hide();
        }
    }
}
