﻿using System;

namespace HH.UI.ModalPopupForm
{
    /// <summary>
    /// Interface for the ModalForm user control
    /// </summary>
    public interface IModalForm
    {
        void AddContent(System.Web.UI.Control c);
        void AddContentAt(int index, System.Web.UI.Control c);
        string BackgroundCssClass { get; set; }
        string ButtonCssClass { get; set; }
        ModalFormButtonMode ButtonMode { get; set; }
        event ModalFormEventHandler CancelClick;
        string CancelControlID { get; set; }
        string ContentCssClass { get; set; }
        System.Web.UI.WebControls.Unit Height { get; set; }
        void Hide();
        string ModalPopupExtenderID { get; }
        event ModalFormEventHandler OkClick;
        string OkControlID { get; set; }
        string OnCancelScript { get; set; }
        string OnOkScript { get; set; }
        AjaxControlToolkit.ModalPopupRepositionMode RepositionMode { get; set; }
        void Show();
        string TargetControlID { get; set; }
        System.Web.UI.WebControls.Unit Width { get; set; }
        int X { get; set; }
        int Y { get; set; }
    }
}
