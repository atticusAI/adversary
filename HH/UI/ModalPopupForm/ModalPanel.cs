﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace HH.UI.ModalPopupForm
{
    /// <summary>
    /// Support class for custom user control
    /// </summary>
    internal class ModalPanel : Panel, INamingContainer { }
}
