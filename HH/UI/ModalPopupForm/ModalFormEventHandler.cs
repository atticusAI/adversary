﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HH.UI.ModalPopupForm
{
    public enum ModalFormEvent
    {
        OK,
        Cancel
    }

    /// <summary>
    /// Args for OK and Cancel click events
    /// </summary>
    public class ModalFormEventArgs : EventArgs
    {
        private ModalFormEvent _event;
        public ModalFormEvent ClickEvent
        {
            get
            {
                return _event;
            }
        }

        public ModalFormEventArgs(ModalFormEvent e)
        {
            _event = e;
        }
    }

    /// <summary>
    /// Fired on modal popup OK or cancel button click
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void ModalFormEventHandler(ModalForm sender, ModalFormEventArgs e);
}
