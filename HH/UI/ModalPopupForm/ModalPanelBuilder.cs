﻿using System;
using System.Collections;
using System.Web.UI;

namespace HH.UI.ModalPopupForm
{
    /// <summary>
    /// Support ControlBuilder class for ModalForm
    /// </summary>
    internal class ModalPanelBuilder : ControlBuilder
    {
        public override Type GetChildControlType(string tagName, IDictionary attributes)
        {
            if (tagName.Contains("ModalPanel"))
                return typeof(ModalPanel);
            else
                return null;
        }

        public override void AppendLiteralString(string s)
        {
        }
    }
}
