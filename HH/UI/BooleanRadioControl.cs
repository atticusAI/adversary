﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


namespace HH.UI
{
    /// <summary>
    /// Simple radio control group that has two states
    /// </summary>
    public class BooleanRadioControl : RadioButtonList
    {
        public bool Value
        {
            get
            {
                return this.SelectedIndex == 0;
            }
            set
            {
                this.SelectedIndex = (value) ? 0 : 1;
            }
        }

        public BooleanRadioControl()
        {
            this.RepeatDirection = RepeatDirection.Horizontal;
            this.Items.Add(new ListItem("Yes", "True"));
            this.Items.Add(new ListItem("No", "False"));
        }
    }
}