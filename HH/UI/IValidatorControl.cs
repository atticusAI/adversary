﻿using System;
using System.Web.UI;

namespace HH.UI
{
    public enum ValidationMode
    {
        RequiredField,
        Date,
        Numeric,
        Alpha,
        AlphaNumeric,
        Email,
        Length,
        RegEx
    }

    /// <summary>
    /// Implemented by the ValidatorControl.ascx control
    /// </summary>
    public interface IValidatorControl
    {
        #region MISC PROPS
        /// <summary>
        /// Set what to validate
        /// </summary>
        ValidationMode Mode { get; set; }

        /// <summary>
        /// Control ID
        /// </summary>
        string ID { get; set; }

        /// <summary>
        /// The control to be validated
        /// </summary>
        string TargetControlID { get; set; }

        /// <summary>
        /// Message to display next to the target control on error
        /// </summary>
        string ErrorMessage { get; set; }

        /// <summary>
        /// Regular expression to apply on validation. Works with RegEx mode set.   
        /// </summary>
        string RegEx { get; set; }

        /// <summary>
        /// Place holder control to add the IValidator control to.
        /// If null, the ValidationControl will attempt to add the IValidator
        /// as a child of the Target (TargetControlID) control
        /// </summary>
        Control ValidatorContainer { get; set; }

        /// <summary>
        /// Focus on the target control if invalid
        /// </summary>
        bool SetFocusOnError { get; set; }

        /// <summary>
        /// Text of the validation suggestion to show if HintsVisible is set to true
        /// </summary>
        string Hint { get; set; }

        /// <summary>
        /// Show validation suggestions (i.e. * required field), based on what's getting validated
        /// </summary>
        bool HintsVisible { get; set; }
        #endregion

        #region LENGTH SPECIFIC
        /// <summary>
        /// Minimum string length, available when Length mode is selected
        /// </summary>
        int? MinLength { get; set; }

        /// <summary>
        /// Max string length, available when Length mode is selected
        /// </summary>
        int? MaxLength { get; set; }
        #endregion

        #region MASK SPECIFIC
        /// <summary>
        /// Show an AJAX mask control in a text box
        /// </summary>
        bool MaskVisible { get; set; }
        /// <summary>
        /// The mask to apply
        /// </summary>
        string Mask { get; set; }
        #endregion

        #region DATE SPECIFIC
        /// <summary>
        /// Default AJAX calendar date for DateTime Validation
        /// </summary>
        DateTime? SelectedDate { get; set; }
        /// <summary>
        /// Show AJAX Calendar for DateTime Validation
        /// </summary>
        bool CalendarVisible { get; set; }
        /// <summary>
        /// Minimum allowed Date for DateTime Validation
        /// </summary>
        DateTime? MinDate { get; set; }
        /// <summary>
        /// Maximum allowed Date for DateTime Validation
        /// </summary>
        DateTime? MaxDate { get; set; }
        #endregion

        #region VALIDATION METHODS
        bool IsValid { get; }
        void Validate();
        #endregion
    }
}
