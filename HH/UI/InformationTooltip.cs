﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;

namespace HH.UI
{
    /// <summary>
    /// Tool tip that is typically used to show/hide help for a form element
    /// </summary>
    public class InformationTooltip : Control
    {
        public string Text { get; set; }
        public string ImgSrc { get; set; }
        public Unit Width { get; set; }


        public InformationTooltip(string toolTipText)
        {
            Text = toolTipText;
        }
        public InformationTooltip(string toolTipText, string imgSrc)
        {
            Text = toolTipText;
            ImgSrc = imgSrc;
        }


        protected override void  OnPreRender(EventArgs e)
        {
 	        base.OnPreRender(e);

            if (string.IsNullOrEmpty(ImgSrc))
                ImgSrc = Page.Request.ApplicationPath + "/_css/about.gif";

            if (!Page.ClientScript.IsClientScriptBlockRegistered(this.GetType(), "infoToolTipJS"))
            {
                Page.ClientScript.RegisterClientScriptBlock(
                    this.GetType(),
                    "infoToolTipJS",
                    BuildJavaScript(),
                    true);
            }

            string uniqueId = "InfoToolTip___" + Guid.NewGuid().ToString();
            StringBuilder img = new StringBuilder();
            img.Append("<img src=\"" + ImgSrc + "\" class=\"toolTipImg\" ");
            img.Append("alt=\"Click for more information\" ");
            img.Append("id=\"" + uniqueId + "Img\" ");
            img.AppendLine("onclick=\"toggleToolTip('" + uniqueId + "');\" />");

            StringBuilder div = new StringBuilder();
            div.Append("<div class=\"toolTip\" style=\"visibility: hidden;\"");
            div.Append("onclick=\"toggleToolTip('" + uniqueId + "')\" ");
            div.AppendLine("id=\"" + uniqueId + "\">");
            div.AppendLine(Text + " ");
            div.AppendLine("</div>");
            this.Controls.Add(new LiteralControl("<span style=\"text-align: center; " + ((Width.IsEmpty) ? "" : "width: " + Width.ToString()) + ";\">"));
            this.Controls.Add(new LiteralControl(img.ToString()));
            this.Controls.Add(new LiteralControl("<br/>"));
            this.Controls.Add(new LiteralControl(div.ToString()));
            this.Controls.Add(new LiteralControl("</span>"));
        }

        public static string BuildJavaScript()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("var visibleToolTip = '';");
            sb.AppendLine("function toggleToolTip(id)");
            sb.AppendLine("{");
            sb.AppendLine("    var elem = $get(id);");
            sb.AppendLine("    if (visibleToolTip != '' && visibleToolTip != id)");
            sb.AppendLine("    {");
            sb.AppendLine("        $get(visibleToolTip).style.visibility = 'hidden';");
            sb.AppendLine("        visibleToolTip = '';");
            sb.AppendLine("    }");
            sb.AppendLine();    
            sb.AppendLine("    elem.style.visibility  = (elem.style.visibility == 'hidden') ? 'visible' : 'hidden';");
            sb.AppendLine();    
            sb.AppendLine("    if (elem.style.visibility == 'visible')");
            sb.AppendLine("    {");
            sb.AppendLine("        visibleToolTip = id;");
            sb.AppendLine("        if (!elem.style.left)");
            sb.AppendLine("        {");
            sb.AppendLine("            elem.style.left = $get(id).offsetLeft + 25;");
            sb.AppendLine("        }");
            sb.AppendLine("        if (!elem.style.top)");
            sb.AppendLine("        {");    
            sb.AppendLine("            elem.style.top = $get(id).offsetTop - 25;");
            sb.AppendLine("        }");
            sb.AppendLine("    }");
            sb.AppendLine("}");

            return sb.ToString();
        }
    }
}