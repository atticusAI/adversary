﻿
using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;


namespace HH.UI
{
    

    /// <summary>
    /// Misc util methods for doing repeteative UI tasks, TODO: move the AutoComplete and BuildCollapsiblePanelExtender to user controls
    /// </summary>
    public static class UIUtils
    {
        public static string BuildErrorMessage(Exception ex)
        {
            string error = "";
            while (ex != null)
            {
                error += "<strong>Message:</strong> " + ex.Message + "<br/><br/>";
                error += "<strong>Type:</strong> " + ex.GetType() + "<br/><br/>";
                error += "<strong>Stack:</strong> " + ex.StackTrace + "<br/><br/>";

                ex = ex.InnerException;
                if (ex != null)
                {
                    error += "Inner Exception<br/>--------------------<br/>";
                }
            }

            return error;
        }

        public static void BindEnumToListControl(ListControl lc, Type enumType)
        {
            if (!enumType.IsEnum)
                throw new Exception("Could not bind enum type " + enumType + ". Type is not a valid enum.");

            string[] names = Enum.GetNames(enumType);
            lc.Items.Clear();
            foreach (string name in names)
                lc.Items.Add(name);
        }

        public static AjaxControlToolkit.AutoCompleteExtender BuildAutoComplete(string targetControlId, string serviceMethod, string contextKey) { return BuildAutoComplete(targetControlId, serviceMethod, contextKey, 2); }
        public static AjaxControlToolkit.AutoCompleteExtender BuildAutoComplete(string targetControlId, string serviceMethod, string contextKey, int minimumCharacters)
        {
            if (string.IsNullOrEmpty(targetControlId))
                throw new ArgumentException("The target control ID was not specified");

            AjaxControlToolkit.AutoCompleteExtender auto = new AjaxControlToolkit.AutoCompleteExtender();
            auto.ID = "autoComplete___" + targetControlId;
            auto.ServiceMethod = serviceMethod;
            if (!string.IsNullOrEmpty(contextKey))
            {
                auto.ContextKey = contextKey;
                auto.UseContextKey = true;
            }
            auto.TargetControlID = targetControlId;
            auto.EnableCaching = true;
            auto.MinimumPrefixLength = minimumCharacters;
            auto.CompletionSetCount = 50;
            auto.CompletionInterval = 500;
            auto.CompletionListItemCssClass = "autoCompleteExtender";
            auto.CompletionListHighlightedItemCssClass = "autoCompleteExtenderHover";
            return auto;
        }

        public static AjaxControlToolkit.CollapsiblePanelExtender BuildCollapsiblePanelExtender(string targetControlId, string expandControlId, string collapseControlId, bool isCollapsed) { return BuildCollapsiblePanelExtender(targetControlId, expandControlId, collapseControlId, isCollapsed, null); }
        public static AjaxControlToolkit.CollapsiblePanelExtender BuildCollapsiblePanelExtender(string targetControlId, string expandControlId, string collapseControlId, bool isCollapsed, HtmlImage img)
        {
            const string EXPAND = "~/_css/expand.jpg";
            const string COLLAPSE = "~/_css/collapse.jpg";

            AjaxControlToolkit.CollapsiblePanelExtender panel = new AjaxControlToolkit.CollapsiblePanelExtender();
            panel.ID = "collapsiblePanel____" + targetControlId;
            panel.TargetControlID = targetControlId;
            panel.ExpandControlID = expandControlId;
            panel.CollapseControlID = collapseControlId;
            if (img != null)
            {
                img.Src = (isCollapsed) ? EXPAND : COLLAPSE;
                panel.ImageControlID = img.ID;
                panel.ExpandedImage = COLLAPSE;
                panel.CollapsedImage = EXPAND;
            }
            panel.Collapsed = isCollapsed;
            panel.AutoCollapse = false;
            panel.AutoExpand = false;
            panel.ScrollContents = false;
            panel.CollapsedText = "Show Details...";
            panel.ExpandedText = "Hide Details";
            panel.ExpandDirection = AjaxControlToolkit.CollapsiblePanelExpandDirection.Vertical;

            return panel;
        }
    }
}