﻿using System.Web.UI;
using System.Web.UI.WebControls;

namespace HH.UI.AutoComplete
{
    /// <summary>
    /// Support class for custom user control
    /// </summary>
    internal class AutoCompleteFieldPanel : Panel, INamingContainer { }
}
