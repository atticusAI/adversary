﻿using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using HH.Utils;


namespace HH.UI.AutoComplete
{

    public enum AutoCompleteFieldMode
    {
        ShowSearchBox,
        ShowResults,
        ShowResultsAndDisable
    }
    public delegate AutoCompleteFieldMode AutoCompleteSearchRequest(IAutoCompleteField sender, string searchText);
    public delegate AutoCompleteFieldMode AutoCompleteFieldCleared(IAutoCompleteField sender);

    /// <summary>
    /// AutoComplete TextBox control
    /// </summary>

    [ControlBuilderAttribute(typeof(AutoCompleteFieldPanelBuilder)), ParseChildren(false)]
    public class AutoCompleteField : UpdatePanel, IAutoCompleteField, INamingContainer
    {
        Panel pnlResults = new Panel();
        Label lblResults = new Label();
        LinkButton lbClear = new LinkButton();
        Panel pnlSearch = new Panel();
        TextBox tbSearch = new TextBox();
        Panel pnlAjax = new Panel();

        /// <summary>
        /// Event fired when text is updated in the textbox
        /// </summary>
        public event AutoCompleteSearchRequest SearchRequest;
        /// <summary>
        /// Event fired when text is cleared in textbox
        /// </summary>
        public event AutoCompleteFieldCleared Clear;

        private AutoCompleteFieldMode _mode = AutoCompleteFieldMode.ShowSearchBox;
        public AutoCompleteFieldMode Mode 
        {
            get
            {
                return _mode;
            }
            set
            {
                _mode = value;
                SetVisibilty();
            }
        }

        public string Text 
        {
            get
            {
                return tbSearch.Text.Trim();
            }
            set
            {
                tbSearch.Text = value;
            }
        }


        public string TargetControlID
        {
            get
            {
                return tbSearch.ID;
            }
        }

        public string OnClientFocus { get; set; }

        public string OnClientBlur { get; set; }

        public string TextBoxClientID
        {
            get
            {
                return tbSearch.ClientID;
            }
        }
        public string TextBoxSearchMessage { get; set; }
        public string ClearButtonText { get; set; }
        public string ServiceMethod { get; set; }
        public string ContextKey { get; set; }
        public int? MinimumCharacters { get; set; }
        public bool? TextChangeCausesPostBack { get; set; }
        
        private bool _doInit = false;

        public AutoCompleteField() { }

        public AutoCompleteField(string id, string serviceMethod, string contextKey)
        {
            this.ID = id;
            ServiceMethod = serviceMethod;
            ContextKey = contextKey;
            InitControl();
        }

        private void InitControl()
        {
            if (TextBoxSearchMessage == null)
                TextBoxSearchMessage = " (Enter your search here) ";
            if (ClearButtonText == null)
                ClearButtonText = "Clear Selection";
            if (!MinimumCharacters.HasValue)
                MinimumCharacters = 2;
            if (!TextChangeCausesPostBack.HasValue)
                TextChangeCausesPostBack = true;

            string id = this.ID;
            pnlResults.ID = "pnlResults___" + id;
            pnlResults.Visible = false;
            lblResults.ID = "lblResults___" + id;
            pnlResults.Controls.Add(lblResults);

            lbClear.ID = "lbClear___" + id;
            lbClear.Font.Size = FontUnit.Smaller;
            lbClear.Click += new EventHandler(lbClear_Click);
            PostBackTrigger lbTrigger = new PostBackTrigger();
            lbTrigger.ControlID = "lbClear___" + id;
            this.Triggers.Add(lbTrigger);
            pnlResults.Controls.Add(new LiteralControl("&nbsp;&nbsp;"));
            pnlResults.Controls.Add(lbClear);
            this.ContentTemplateContainer.Controls.Add(pnlResults);

            pnlSearch.ID = "pnlSearch___" + id;

            tbSearch.ID = "tbSearch___" + id;
            tbSearch.CssClass = "sbControl";
            tbSearch.AutoPostBack = true;
            tbSearch.TextChanged += new EventHandler(tbSearch_TextChanged);
            AsyncPostBackTrigger tbTrigger = new AsyncPostBackTrigger();
            tbTrigger.ControlID = "tbSearch___" + id;
            tbTrigger.EventName = "TextChanged";
            this.Triggers.Add(tbTrigger);
            tbSearch.Attributes.Add("autocomplete", "off");
            pnlSearch.Controls.Add(tbSearch);

            pnlAjax.ID = "pnlAjax___" + id;
            pnlSearch.Controls.Add(pnlAjax);
            this.ContentTemplateContainer.Controls.Add(pnlSearch);
            _doInit = false;
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (!_doInit)
                InitControl();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            tbSearch.Attributes.Add("onfocus", "if (this.value.indexOf('" + TextBoxSearchMessage.Replace("'", "") + "') != -1) { this.value = ''; }");

            if (tbSearch.Text.Trim() == string.Empty)
                tbSearch.Text = TextBoxSearchMessage.Replace("'", "");

            if (!string.IsNullOrEmpty(OnClientFocus))
                tbSearch.Attributes["onfocus"] += "; " + OnClientFocus;
            if (!string.IsNullOrEmpty(OnClientBlur))
                tbSearch.Attributes["onblur"] += "; " + OnClientBlur;

            tbSearch.AutoPostBack = TextChangeCausesPostBack.Value;

            lbClear.Text = ClearButtonText;
            pnlAjax.Controls.Add(UIUtils.BuildAutoComplete(tbSearch.ID, ServiceMethod, ContextKey, MinimumCharacters.Value));
            SetVisibilty();
        }


        private void SetVisibilty()
        {
            pnlSearch.Visible = (_mode == AutoCompleteFieldMode.ShowSearchBox);
            pnlResults.Visible = (_mode != AutoCompleteFieldMode.ShowSearchBox);
            if (pnlResults.Visible)
                lblResults.Text = Text;
            lbClear.Visible = (_mode != AutoCompleteFieldMode.ShowResultsAndDisable);


        }

        public void tbSearch_TextChanged(object sender, EventArgs e)
        {
            string text = tbSearch.Text.Trim();
            if (text == string.Empty || (!string.IsNullOrEmpty(TextBoxSearchMessage) && text.Contains(TextBoxSearchMessage.Trim())))
                return;
            if (SearchRequest != null)
                _mode = SearchRequest(this, text);
            SetVisibilty();
        }

        public void lbClear_Click(object sender, EventArgs e)
        {
            tbSearch.Text = TextBoxSearchMessage;
            lblResults.Text = string.Empty;

            if (Clear != null)
                _mode = Clear(this);
            SetVisibilty();
        }

    }
    
}