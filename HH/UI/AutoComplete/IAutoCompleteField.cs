﻿using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using HH.Utils;


namespace HH.UI.AutoComplete
{

    //public enum AutoCompleteFieldType
    //{
    //    ShowAutoCompleteSearchBox,
    //    ShowResults,
    //    ShowResultsAndDisable
    //}

    
    /// <summary>
    /// Interface implemented by AutoCompleteBox.ascx
    /// </summary>
    public interface IAutoCompleteField
    {
        event AutoCompleteSearchRequest SearchRequest;
        event AutoCompleteFieldCleared Clear;

        AutoCompleteFieldMode Mode { get; set; }
        string ID { get; set; }
        string TargetControlID { get; }
        string Text { get; set; }
        string TextBoxSearchMessage { get; set; }
        string ClearButtonText { get; set; }
        string ServiceMethod { get; set; }
        string ContextKey { get; set; }
        int? MinimumCharacters { get; set; }
        string OnClientFocus { get; set; }
        string OnClientBlur { get; set; }
        string TextBoxClientID { get; }
        bool? TextChangeCausesPostBack { get; set; }
    }
    
}