﻿using System;
using System.Collections;
using System.Web.UI;

namespace HH.UI.AutoComplete
{
    /// <summary>
    /// Support ControlBuilder class for AutoCompleteFields
    /// </summary>
    internal class AutoCompleteFieldPanelBuilder : ControlBuilder
    {
        public override Type GetChildControlType(string tagName, IDictionary attributes)
        {
            if (tagName.Contains("AutoCompleteFieldPanel"))
                return typeof(AutoCompleteFieldPanel);
            else
                return null;
        }

        public override void AppendLiteralString(string s)
        {
        }
    }
}
