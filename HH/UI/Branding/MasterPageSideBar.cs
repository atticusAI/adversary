﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace HH.Branding
{
    /// <summary>
    /// Contains the navigation controls if side bar menuing is used
    /// </summary>
    public class MasterPageSideBar : MasterPageNavControl
    {
        private List<MasterPageSideBarItem> _items = new List<MasterPageSideBarItem>();
        public List<MasterPageSideBarItem> Items
        {
            get
            {
                return _items;
            }
        }

        public MasterPageSideBar(string title) : base(title) { }
    } 
}