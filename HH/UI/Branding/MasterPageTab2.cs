﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;


namespace HH.Branding
{
    /// <summary>
    /// Collection of Master Page Tab controls
    /// </summary>
    public class MasterPageTabCollection : ObservableCollection<MasterPageTab>
    {
        public MasterPageTab this[string title] 
        {
            get
            {
                MasterPageTab tab = null;
                foreach (MasterPageTab t in this)
                {
                    if (t.Title == title)
                    {
                        tab = t;
                        break;
                    }
                }

                //if (tab == null)
                //    throw new Exception("Tab item " + title + " not found");
                return tab;
            }
        }

        public MasterPageTabCollection()
        {
            CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(Collection_CollectionChanged);
        }

        void Collection_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case System.Collections.Specialized.NotifyCollectionChangedAction.Add:
                    foreach (MasterPageTab newItem in e.NewItems)
                    {
                        if (newItem.Selected)
                        {
                            foreach (MasterPageTab t in this)
                            {
                                if (t != newItem)
                                    t.Selected = false;
                            }
                        }
                    }
                    break;
            }
        }

        private void newItem_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "Selected")
                return;

            foreach (MasterPageTab tab in this)
            {
                if (tab != sender)
                {
                    tab.Selected = false;
                    //foreach (MasterPageTabItem2 item in tab.Items)
                    //{
                    //    item.Selected = false;
                    //}
                }
            }
        }

    }

    /// <summary>
    /// Master page tab navigation control
    /// </summary>
    public class MasterPageTab : MasterPageNavControl
    {
        private MasterPageTabItemCollection _items;
        public MasterPageTabItemCollection Items
        {
            get
            {
                return _items;
            }
        }
        
        
        public MasterPageTab(string title) : base(title) 
        {
            _items = new MasterPageTabItemCollection(this);
        }
    }
}