﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace HH.Branding
{
    public class MasterPageMenuItem
    {
        private WebControl _control = null;
        public WebControl Control
        {
            get
            {
                return _control;
            }
        }
        public bool ConfirmRedirect { get; set; }

        protected internal MasterPageMenuItem(WebControl control)
        {
            _control = control;
            ConfirmRedirect = false;
        }
    }

    public class SubMenuItem : MasterPageMenuItem
    {
        public SubMenuItem(WebControl control) : base(control) { }
    }

    public class SideBarMenuItem : MasterPageMenuItem
    {
        public SideBarMenuItem(WebControl control) : base(control) { }
    }

    public class FooterItem : MasterPageMenuItem
    {
        public FooterItem(WebControl control) : base(control) { }
    }
}