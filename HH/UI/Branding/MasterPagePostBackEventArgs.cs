﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace HH.Branding
{
    /// <summary>
    /// Handles postback events that originate from navigation controls on the master page. This event
    /// fires before the MasterPage redirects to a new page.
    /// </summary>
    /// <param name="sender">The control that was clicked</param>
    /// <param name="e">Control event args</param>
    /// <returns>return true if the master page should redirect to the new page</returns>
    public delegate bool MasterPagePostBackEventHandler(object sender, MasterPagePostBackEventArgs e);

    public enum MasterPagePostBackSource
    {
        Tab,
        SubMenu,
        SideBar,
        NavButton,
        PopupYes,
        PopupNo
    }

    public class MasterPagePostBackEventArgs : EventArgs
    {
        private EventArgs _args = null;
        public EventArgs ControlEventArgs
        {
            get
            {
                return _args;
            }
        }
        private string _url = string.Empty;
        public string RedirectUrl
        {
            get
            {
                return _url;
            }
        }
        private MasterPagePostBackSource _source;
        public MasterPagePostBackSource PostBackSource
        {
            get
            {
                return _source;
            }
        }
        

        public MasterPagePostBackEventArgs(string redirectUrl, EventArgs controlEventArgs, MasterPagePostBackSource source) : base()
        {
            _url = redirectUrl;
            _args = controlEventArgs;
            _source = source;
        }
    }
}
