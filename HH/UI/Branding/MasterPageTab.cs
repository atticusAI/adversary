﻿using System.Collections.Generic;

namespace HH.Branding
{
    public class MasterPageTab
    {
        public string Title { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        public bool Selected { get; set; }
        public bool Enabled { get; set; }
        public bool Visible { get; set; }
        public bool Locked { get; set; }
        public bool ConfirmRedirect { get; set; }
        private List<string> roles = new List<string>();
        public List<string> Roles
        {
            get
            {
                return roles;
            }
        }
        private List<SubMenuItem> subMenu = new List<SubMenuItem>();
        public List<SubMenuItem> SubMenu
        {
            get
            {
                return subMenu;
            }
        }

        public MasterPageTab()
        {
            Title = string.Empty;
            Url = string.Empty;
            Selected = false;
            Enabled = true;
            Visible = true;
            Locked = false;
            ConfirmRedirect = false;
        }
    }


}