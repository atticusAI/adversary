﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace HH.Branding
{
    /// <summary>
    /// THe navigation items under each tab
    /// </summary>
    public class MasterPageTabItemCollection : ObservableCollection<MasterPageTabItem>
    {
        MasterPageNavControl _parent;
        public MasterPageNavControl Parent
        {
            get
            {
                return _parent;
            }
        }

        public MasterPageTabItem this[string title]
        {
            get
            {
                MasterPageTabItem i = null;
                foreach (MasterPageTabItem item in this)
                {
                    if (item.Title == title)
                    {
                        i = item;
                        break;
                    }
                }

                //if (i == null)
                //    throw new Exception("Tab item " + title + " not found");
                return i;
            }
        }

        public MasterPageTabItemCollection(MasterPageNavControl parent)
        {
            _parent = parent;
            CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(Collection_CollectionChanged);
        }

        void Collection_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case System.Collections.Specialized.NotifyCollectionChangedAction.Add:
                    foreach (MasterPageTabItem newItem in e.NewItems)
                    {
                        if (newItem.Selected)
                        {
                            foreach (MasterPageTabItem t in this)
                            {
                                if (t != newItem)
                                    t.Selected = false;
                            }
                            _parent.Selected = true;
                        }
                    }
                    break;
            }
        }

       

    }

    /// <summary>
    /// Master Page navigation tab sub-control 
    /// </summary>
    public class MasterPageTabItem : MasterPageNavControl
    {
        MasterPageNavControl _parent;
        public MasterPageNavControl Parent
        {
            get
            {
                return _parent;
            }
        }

        private MasterPageTabItemCollection _items;
        public MasterPageTabItemCollection Items
        {
            get
            {
                return _items;
            }
        }

        public MasterPageTabItem(MasterPageNavControl parent, string title) : base(title)
        {
            _parent = parent;
            _items = new MasterPageTabItemCollection(this);
        }
    } 
}