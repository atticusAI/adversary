﻿using System;
using System.Linq;
using System.Collections.Generic;


namespace HH.Branding
{
    /// <summary>
    /// Navigation control for the side bar, TODO: consolidate with TabItems to create a single control class
    /// </summary>
    public class MasterPageSideBarItem : MasterPageNavControl
    {
        private List<MasterPageSideBarItem> _items = new List<MasterPageSideBarItem>();
        public List<MasterPageSideBarItem> Items
        {
            get
            {
                return _items;
            }
        }

        public MasterPageSideBarItem(string title) : base(title) { }
    }
}