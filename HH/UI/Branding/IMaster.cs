﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI;
using HH.Screen;
using System.Web.Security;

namespace HH.Branding
{
    public enum MasterPageFeedbackMode
    {
        Information,
        Warning,
        Error,
        Success,
        Confirm,
        None
    }

    public enum MasterPageMenuMode
    {
        Tabs,
        SideBar,
        None
    }


    public enum MasterPagePopupDialogMode
    {
        Popup,
        ModalYesNo,
        ModalYesNoCancel,
        ModalContinueCancel,
        ModalOkCancel,
        ModalOK,
        ModalCancel
    }

    /// <summary>
    /// Interface for manipulating Master Page in App_Code, TODO: Move the MasterPage control from the web directory to this library
    /// </summary>
    public interface IMaster
    {
        /// <summary>
        /// Text that appears under the bread crumb section
        /// </summary>
        string HeaderText { get; set; }
        /// <summary>
        /// The pages title tag
        /// </summary>
        string Title { get; set; }
        /// <summary>
        /// Setting this overrides any default bread crumb text set by the masterpage
        /// </summary>
        string BreadCrumbText { get; set; }
        /// <summary>
        /// Settings this overrides the default copyright disclaimer at the bottom of the page
        /// </summary>
        string CopyrightText { get; set; }
        //string ErrorMessage { get; set; }
        
        /// <summary>
        /// This message along with feedback mode presents error warning and success messages to the user, appears below the bread crumb text
        /// </summary>
        string FeedbackMessage { get; set; }
        /// <summary>
        /// Along with feedback message presents error warning and success messages to the user, appears below the bread crumb text
        /// </summary>
        MasterPageFeedbackMode FeedbackMode { get; set; }

        bool TabMenuEnabled { get; set; }
        bool TabMenuVisible { get; set; }
        bool SideBarEnabled { get; set; }
        bool SideBarVisible { get; set; }
        /// <summary>
        /// Will hide all content in the center of the page if false (Upper/lower content controls and Screen.aspx control)
        /// </summary>
        bool PageContentVisible { get; set; }
        /// <summary>
        /// Hides the footer bar
        /// </summary>
        bool FooterVisible { get; set; }
        /// <summary>
        /// Hides the prev/next buttons
        /// </summary>
        bool FooterNavVisible { get; set; }
        bool NextButtonVisible { get; set; }
        bool NextButtonEnabled { get; set; }
        bool PreviousButtonVisible { get; set; }
        bool PreviousButtonEnabled { get; set; }

        /// <summary>
        /// Fired whenever any nav control is clicked
        /// </summary>
        event MasterPagePostBackEventHandler PostBackEvent;
        /// <summary>
        /// True if POstBackEvent has been subscribed to
        /// </summary>
        bool HasRegisteredPostBackEvent { get; }
        //bool IsMasterPagePostback { get; }

        /// <summary>
        /// Appends the specified Logout link and URL to the Master Page's Nav menu
        /// </summary>
        string LogoutUrl { get; set; }
        /// <summary>
        /// Appends the specified key value pairs to links in the Master Page's Nav menu
        /// </summary>
        Dictionary<string, string> UrlParams { get; }

        /// <summary>
        /// Accesses the ScriptManager embeded in the master page
        /// </summary>
        ScriptManager ScriptManager { get; }
        /// <summary>
        /// Accesses the Header's Content PlaceHolder
        /// </summary>
        ContentPlaceHolder HeaderContent { get; }
        /// <summary>
        /// Accesses the Top Content PlaceHolder
        /// </summary>
        ContentPlaceHolder UpperContent { get; }
        /// <summary>
        /// Accesses the embeded Screen.ascx control
        /// </summary>
        IScreen Screen { get; }
        /// <summary>
        /// Accesses the lower Content PlaceHolder
        /// </summary>
        ContentPlaceHolder LowerContent { get; }

        /// <summary>
        /// True references the role provider and will disable access to pages based on the User's specified roles
        /// </summary>
        bool RoleSecurityEnabled { get; set; }
        /// <summary>
        /// THe role provider to use if role security is enalbed (deprecated)
        /// </summary>
        RoleProvider CustomRoleProvider { get; set; }
        /// <summary>
        /// Sets the position/function of the nav menu
        /// </summary>
        MasterPageMenuMode MenuMode { get; set; }
        /// <summary>
        /// The site map to bind the nav to
        /// </summary>
        SiteMapProvider SiteMapProvider { get; set; }
        /// <summary>
        /// List of tabs on the master page
        /// </summary>
        MasterPageTabCollection Tabs { get; }
        /// <summary>
        /// The current tab
        /// </summary>
        MasterPageTab SelectedTab { get; }
        /// <summary>
        /// THe title of the current tab
        /// </summary>
        string SelectedTabTitle { get; }
        /// <summary>
        /// List of side bar menus
        /// </summary>
        List<MasterPageSideBar> SideBarMenu { get; }
        MasterPageSideBar SelectedSideBar { get; }
        string SelectedSideBarTitle { get; }
        List<Control> FooterControls { get; }

        /// <summary>
        /// Displays the yellow error box
        /// </summary>
        /// <param name="errorMessage"></param>
        void ShowError(string errorMessage);
        /// <summary>
        /// Displays the yellow error box
        /// </summary>
        /// <param name="ex"></param>
        void ShowError(Exception ex);

        /// <summary>
        /// Adds a CommandArgument property to the modal popup pane's yes button
        /// </summary>
        string PopupYesCommandArgument { get; set; }
        /// <summary>
        /// Adds a CommandArgument property to the modal popup pane's no button
        /// </summary>
        string PopupNoCommandArgument { get; set; }
        /// <summary>
        /// True if the modal popup or JS popup are showing
        /// </summary>
        bool PopupDialogVisible { get; }
        /// <summary>
        /// Fired when the Popup's yes button is clicked
        /// </summary>
        event EventHandler PopupYesClick;
        /// <summary>
        /// Fired when the popup's no button is clicked
        /// </summary>
        event EventHandler PopupNoClick;
        /// <summary>
        /// Shows a modal or JavaScript dialog box center screen
        /// </summary>
        /// <param name="message"></param>
        /// <param name="mode"></param>
        void ShowPopupDialog(string message, MasterPagePopupDialogMode mode);
        /// <summary>
        /// Shows a modal or JavaScript dialog box center screen
        /// </summary>
        /// <param name="message"></param>
        /// <param name="mode"></param>
        /// <param name="width"></param>
        /// <param name="dataItem">Will embed an associated entity with the popup message, entity can be retrieved in the popup yes and no click event handlers</param>
        void ShowPopupDialog(string message, MasterPagePopupDialogMode mode, Unit width, object dataItem);
        void HidePopupDialog();
        
        /// <summary>
        /// Shows a simple javascript alert() message
        /// </summary>
        /// <param name="message"></param>
        void ShowJavaScriptAlert(string message);

        /// <summary>
        /// Binds the SiteMapProvider to the master page for creating the navigation
        /// </summary>
        void DataBind();
    }
}