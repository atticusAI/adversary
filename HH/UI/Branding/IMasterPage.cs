﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HH.Authentication;
using HH.Screen;

namespace HH.Branding
{
    /// <summary>
    /// Handles postback events that originate from navigation controls on the master page. This event
    /// fires before the MasterPage redirects to a new page.
    /// </summary>
    /// <param name="sender">The control that was clicked</param>
    /// <param name="e">Control event args</param>
    /// <returns>return true if the master page should redirect to the new page</returns>
    public delegate bool MasterPagePostBackEventHandler(object sender, MasterPagePostBackEventArgs e);


    public enum MasterPageFeedbackMode
    {
        Information,
        Warning,
        Error,
        Success,
        Confirm,
        None
    }

    public enum MasterPageMenuMode
    {
        Tabs,
        SideBar,
        None
    }

    public enum MasterPagePostBackSource
    {
        Tab,
        SubMenu,
        SideBar,
        NavButton,
        PopupYes,
        PopupNo
    }

    public enum MasterPagePopupDialogMode
    {
        Popup,
        ModalYesNo,
        ModalYesNoCancel,
        ModalContinueCancel,
        ModalOkCancel,
        ModalOK,
        ModalCancel
    }

    public interface IMasterPage
    {
        event MasterPagePostBackEventHandler PostBackEvent;
        event EventHandler PopupYesClick;
        event EventHandler PopupNoClick;

        bool HasRegisteredPostBackEvent { get; }

        string Title { get; set; }
        string BreadCrumbText { get; set; }
        string HeaderText { get; set; }
        string ErrorMessage { get; set; }
        string FeedbackMessage { get; set; }
        MasterPageFeedbackMode FeedbackMode { get; set; }
        bool IsMasterPagePostback { get; }

        string SelectedMenuTitle { get; }
        MasterPageTab SelectedTab { get; }
        MasterPageMenuMode MenuMode { get; set; }
        SiteMapProvider SiteMapProvider { get; set; }
        bool TabMenuEnabled { get; set; }
        bool TabMenuVisible { get; set; }
        bool SideBarMenuEnabled { get; set; }
        bool SideBarMenuVisible { get; set; }

        List<MasterPageTab> Tabs { get; }
        List<SideBarMenuItem> SideBarMenuItems { get; }
        ScriptManager ScriptManager { get; }
        MasterPagePopupDialogMode PopupDialogMode { get; set; }
        bool PopupDialogOnTabClick { get; set; }
        bool PopupDialogOnSubMenuClick { get; set; }
        bool PopupDialogOnSideBarClick { get; set; }
        bool PopupDialogOnNavButtonClick { get; set; }
        string PopupDialogText { get; set; }
        bool MainContentVisible { get; set; }
        ContentPlaceHolder UpperContent { get; }
        ContentPlaceHolder LowerContent { get; }
        IScreen Screen { get; }
        ILoginPopup Login { get; }
        List<FooterItem> FooterControls { get; }
        Button BtnPrevious { get; }
        Button BtnNext { get; }
        bool FooterVisible { get; set; }
        bool FooterNavVisible { get; set; }
        string CopyrightText { get; set; }

        string LogoutUrl { get; set; }
        bool RoleSecurityEnabled { get; set; }

        void ShowPopupDialog();
    }
}