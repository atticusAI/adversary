﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using System.ComponentModel;

namespace HH.Branding
{
    
    /// <summary>
    /// Base class for Master Page Navigation controls
    /// </summary>
    public class MasterPageNavControl : INotifyPropertyChanged
    {
        public string ID { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        public string Target { get; set; }
        public bool Enabled { get; set; }
        public bool Visible { get; set; }
        public bool Locked { get; set; }
        public bool ConfirmRedirect { get; set; }


        private bool _selected = false;
        public bool Selected
        {
            get
            {
                return _selected;
            }
            set
            {
                bool changed = (_selected != value);
                _selected = value;
                if (changed && PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("Selected"));

            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        
        private List<string> roles = new List<string>();
        public List<string> Roles
        {
            get
            {
                return roles;
            }
        }

        internal MasterPageNavControl(string title)
        {
            Title = title;
            Selected = false;
            Enabled = true;
            Visible = true;
            Locked = false;
            ConfirmRedirect = false;
        }

        public void AddRoleList(IList roles)
        {
            foreach (object role in roles)
                Roles.Add(role.ToString());
        }

        
    }
}