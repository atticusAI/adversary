﻿using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace HH.UI.FileList
{
    public enum FileListControlMode
    {
        FileListOnly,
        Upload,
        Edit
    }

    public delegate bool FileUploaded(IFileListControl sender, FileUpload file, string description);
    public delegate bool FileEditClick(IFileListControl sender, object dataItem);
    public delegate bool FileDeleteClick(IFileListControl sender, object dataItem);
    public delegate bool FileEditSubmit(IFileListControl sender, object dataItem, string newFileName, string description);

    /// <summary>
    /// Interface that the FileListControl.ascx file implements , TODO: Move the FileListControl.ascx user control from the web directory to this library
    /// </summary>
    public interface IFileListControl
    {
        event FileUploaded OnFileUploaded;
        event FileEditClick OnFileEditClick;
        event FileDeleteClick OnFileDeleteClick;
        event FileEditSubmit OnFileEditSubmit;
        
        bool EnableDescription { get; set; }
        bool AllowFileEdit { get; set; }
        bool AllowFileDelete { get; set; }
        FileListControlMode Mode { get; set; }
        string FileListHeader { get; set; }
        object DataSource { get; set; }
        Repeater FileList { get; }
        string FileDescription { get; set;  }
        string FileName { get; set; }
        Uri FileDirectory { get; set; }
        Uri DownloadPage { get; set; }
        //UpdatePanel UpdatePanel { get; }

        void DataBind();
        void SetStatusMessage(string msg, Color color);
        void ClearStatus();
    }
}
