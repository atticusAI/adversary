﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HH.UI.FileList
{
    /// <summary>
    /// Dependency data item for the FileListItemControl, TODO: Move the FileListControl.ascx user control from the web directory to this library
    /// </summary>
    public interface IFileListDataItem
    {
        string FileName { get; set; }
        string FileDescription { get; set; }
        Uri FileURI { get; set; }
    }
}
