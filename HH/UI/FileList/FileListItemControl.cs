﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Security.Policy;
using System.Web.UI.HtmlControls;

namespace HH.UI.FileList
{

    /// <summary>
    /// ASP.Net Control for displaying links to downloadable files, TODO: Move the FileListControl.ascx user control from the web directory to this library
    /// </summary>
    public class FileListItemControl : PlaceHolder
    {
        HtmlImage _icon = new HtmlImage();
        HtmlAnchor _link = new HtmlAnchor();

        public IFileListDataItem DataItem { get; set; }
        public Uri FileDirectory { get; set; }
        public Uri DownloadPage { get; set; }
        

        public FileListItemControl(IFileListDataItem item)
        {
            DataItem = item;
        }

        
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            _icon.Src = Page.Request.ApplicationPath + "/_css/file_icon.jpg";
            _icon.Border = 0;
            _link.Attributes.Add("target", "_blank");

            if (DataItem == null)
                throw new Exception("Could not bind to FileListItemControl, no file data was found");
            if ((DownloadPage == null) && (DataItem.FileURI == null) && (string.IsNullOrEmpty(DataItem.FileName) || FileDirectory == null))
                throw new Exception("Could not bind object " + DataItem.ToString() + " to FileListItemControl, no location found for the file");

            //try the data object's URI first, 
            Uri uri = DataItem.FileURI;
            if (uri == null) //if that isn't found, try the download page
                uri = DownloadPage;
            if (uri == null) //if that doesn't work, create one with the directory and file name
                uri = new Uri(FileDirectory + "\\" + DataItem.FileName);
            
            _icon.Attributes.Add("onclick", "location.href='" + uri.ToString() + "'");
            _link.HRef = uri.AbsoluteUri;
            
            this.Controls.Add(_icon);
            this.Controls.Add(new LiteralControl("&nbsp;&nbsp;"));
            this.Controls.Add(_link);
            if (!string.IsNullOrEmpty(DataItem.FileDescription))
            {
                _link.InnerText = DataItem.FileDescription;
                if (!string.IsNullOrEmpty(DataItem.FileName))
                    this.Controls.Add(new LiteralControl("&nbsp;(" + DataItem.FileName + ")"));
            }
            else if (!string.IsNullOrEmpty(DataItem.FileName))
            {
                _link.InnerText = DataItem.FileName;
            }
            else if (uri != null)
            {
                _link.InnerText = uri.ToString();
            }
        }
    }
}
