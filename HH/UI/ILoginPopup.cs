﻿using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace HH.Authentication
{
    public delegate void LoggingInEventHandler(ILoginPopup sender, string userName, string password);
    public delegate void LoggedInEventHandler(ILoginPopup sender, string userName, string password);
    public delegate void LoginErrorEventHandler(ILoginPopup sender, string errorMessage, Exception ex);

    public enum LoginAuthenticationMode
    {
        Off,
        ActiveDirectory,
        Custom
    }

    /// <summary>
    /// Interface implemented by LoginPopup.ascx
    /// </summary>
    public interface ILoginPopup
    {
        event LoggingInEventHandler LoggingIn;
        event LoggedInEventHandler LoggedIn;
        event LoginErrorEventHandler LoginError;

        UserControl ParentUserControl { get; }
        string LoginHeader { get; set; }
        string StatusMessage { get; }
        string UserName { get; set; }
        string Password { get; set; }
        bool SaveLoginChecked { get; set; }
        bool AllowLoginCancel { get; set; }
        bool AllowDomainSelect { get; set; }
        bool AllowSaveLogin { get; set; }
        LoginAuthenticationMode AuthenticationMode { get; set; }
        string ActiveDirectoryServer { get; set; }
        string Domain { get; set; }
        Unit Width { get; set; }

        void Show();
        void Hide();
        void SetStatusMessage(string msg, Color textColor);
    }
}