﻿using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace HH.Screen
{
    /// <summary>
    /// interface for ReportBuilder.ascx user control, TODO: move the user control to HH library
    /// </summary>
    public interface IReportBuilder
    {
        List<Table> Tables { get; }
        string ScreenID { get; set; }
        object DataSource { get; set; }
        string CssClass { get; set; }
        string RowCssClass { get; set; }
        string RowValueCssClass { get; set; }
        Unit Width { get; set; }
        bool Visible { get; set; }
        bool ShowNullValues { get; set; }

        void DataBind();
        void ClearReport();
        void AddRow(string caption, object value);
        void AddRowAt(int index, string caption, object value);
    }
}