﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using HH.Screen;
using System.Reflection;



namespace HH.Screen
{

    /// <summary>
    /// Fired before a new control is created for a new row
    /// </summary>
    /// <param name="attrib">The screen builder attribute that corresponds to the row/control and it's associated DAL property</param>
    /// <param name="info">The property info for the associated DAL property</param>
    /// <param name="dataSource">THe DAL object</param>
    /// <returns>A new control to insert in the row</returns>
    public delegate Control ScreenRowControlCreatingEventHandler(ScreenBuilder attrib, PropertyInfo info, object dataSource);
    /// <summary>
    /// Fired when a new control is created for a new row
    /// </summary>
    /// <param name="attrib">The screen builder attribute that corresponds to the row/control and it's associated DAL property</param>
    /// <param name="info">The property info for the associated DAL property</param>
    /// <param name="dataSource">THe DAL object</param>
    /// <param name="value">The associated value of the DAL property</param>
    /// <param name="control">The control that was created</param>
    public delegate void ScreenRowControlCreatedEventHandler(ScreenBuilder attrib, PropertyInfo info, object dataSource, object value, Control control);
    /// <summary>
    /// Fired when a new form row is created
    /// </summary>
    /// <param name="attrib">The screen builder attribute that corresponds to the row/control and it's associated DAL property</param>
    /// <param name="info">The property info for the associated DAL property</param>
    /// <param name="dataSource">THe DAL object</param>
    /// <param name="value">The associated value of the DAL property</param>
    /// <param name="tr">The new row that was created</param>
    public delegate void ScreenRowCreatedEventHandler(PropertyInfo info, object dataSource, object value, TableRow tr);

    /// <summary>
    /// Interface for the Screen.ascx user control
    /// </summary>
    public interface IScreen
    {
        #region IScreen Members
        event ScreenRowControlCreatingEventHandler ScreenRowControlCreating;
        event ScreenRowControlCreatedEventHandler ScreenRowControlCreated;
        event ScreenRowCreatedEventHandler ScreenRowCreated;

        event GridFieldsAdding FieldsAdding;
        event GridViewRowEventHandler RowDataBound;
        event GridViewRowEventHandler RowCreated;
        event GridViewCommandEventHandler RowCommand;
        event EntitySubmitEventHandler OnEntityPreSubmit;
        event EntitySubmitEventHandler OnEntitySubmitted;
        event EntitySubmitEventHandler OnEntitySubmitComplete;
        

        string ID { get; set; }
        Table Form { get; }
        object DataSource { get; set; }
        string ScreenID { get; set; }
        string ConnectionString { get; set; }
        
        string CssClass { get; set; }
        string CaptionCssClass { get; set; }
        bool ShowInformationToolTips { get; set; }
        Unit Width { get; set; }
        bool Visible { get; set; }
        bool Enabled { get; set; }
        bool ValidationEnabled { get; set; }
        bool IsValid { get; }
        UpdatePanelTriggerCollection Triggers { get; }
        
        void DataBind();
        void AddControlToForm(Control control);
        void AddControlToForm(Control control, string caption);
        void AddControlToForm(Control control, string caption, string toolTip);
        void AddControlToFormAt(int index, Control control);
        void AddControlToFormAt(int index, Control control, string caption);
        void AddControlToFormAt(int index, Control control, string caption, string toolTip);
        Control FindControl(string id);
        TableRow FindControlRow(string p);
        void ClearForm();
        void Update();
        void Validate();
        #endregion
    }
}