﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;



namespace HH.Screen
{
    /// <summary>
    /// Interface for the AccordianContainerControl.ascx, TODO: move the user control into this library
    /// </summary>
    public interface IAccordianContainerControl
    {
        /// <summary>
        /// Fired before the control for a screen row is created
        /// </summary>
        event ScreenRowControlCreatingEventHandler ScreenRowControlCreating;
        /// <summary>
        /// Fired after the control associated with a screen row is created
        /// </summary>
        event ScreenRowControlCreatedEventHandler ScreenRowControlCreated;
        /// <summary>
        /// Fired when the screen row has been crated
        /// </summary>
        event ScreenRowCreatedEventHandler ScreenRowCreated;

        string ID { get; set; }
        string Caption { get; set; }
        object DataSource { get; set; }
        string ScreenID { get; set; }
        RepeatDirection RepeatDirection { get; set; }
        Unit Width { get; set; }
        string CssClass { get; set; }
        string CaptionCssClass { get; set; }
        string InformationToolTip { get; set; }

        void DataBind();
    }
}
