﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Reflection;
using HH.Utils;
using HH.UI;

namespace HH.Screen
{
    /// <summary>
    /// Creates a default WebControl based on the parameters passed to Build()
    /// </summary>
    public static class ScreenControlBuilder
    {
        public static WebControl Build(ScreenBuilder sb, PropertyInfo info, object value) { return Build(sb, info, value, null); }
        public static WebControl Build(ScreenBuilder sb, PropertyInfo info, object value, object dataSource)
        {
            WebControl wc;

            switch (sb.BuilderType)
            {
                case BuilderType.Label:
                    Label lbl = new Label();
                    if (value == null)
                    {
                        wc = lbl;
                        break;
                    }

                    lbl.Text = value.ToString();
                    if (sb.ListItemValues != null &&
                        sb.ListItemText != null)
                    {
                        //change to list item text if available
                        for (int i = 0; i < sb.ListItemValues.Length; i++)
                        {
                            if (sb.ListItemValues[i] == lbl.Text)
                            {
                                try
                                {
                                    lbl.Text = sb.ListItemText[i];
                                }
                                catch
                                {
                                    lbl.Text = value.ToString();
                                }
                                break;
                            }
                        }
                    }

                    wc = lbl;
                    break;

                case BuilderType.TextBox:
                    TextBox tb = new TextBox();
                    //tb.Width = Unit.Percentage(80);
                    if (value != null)
                    {
                        string textboxText = "";
                        if (value.GetType() == typeof(DateTime))
                        {
                            textboxText = ((DateTime)value).ToShortDateString();
                        }
                        else
                        {
                            textboxText = value.ToString();
                        }
                        //tb.Text = value.ToString();
                        tb.Text = textboxText;
                    }
                    tb.CssClass = "sbControl";
                    if (sb.ValidateMaxLength > 0)
                        tb.MaxLength = sb.ValidateMaxLength;
                    wc = tb;
                    break;

                case BuilderType.TextArea:
                    TextBox ta = new TextBox();
                    if (value != null)
                        ta.Text = value.ToString();
                    ta.TextMode = TextBoxMode.MultiLine;
                    //ta.Width = Unit.Percentage(80);
                    if (sb.TextAreaRows > 0)
                        ta.Rows = sb.TextAreaRows;
                    else
                        ta.Rows = 3;
                    ta.CssClass = "sbControl";
                    wc = ta;
                    break;

                case BuilderType.CheckBox:
                    CheckBox cb = new CheckBox();
                    if (value != null)
                    {
                        if (value.ToString().ToLower() == "on")
                            value = true;
                        cb.Checked = Boolean.Parse(value.ToString());
                    }
                    //cb.CssClass = "sbControl";
                    wc = cb;
                    break;

                case BuilderType.BooleanRadioControl:
                    BooleanRadioControl brc = new BooleanRadioControl();
                    if (value != null)
                        brc.SelectedValue = value.ToString();
                    wc = brc;
                    break;

                case BuilderType.Hidden:
                    TextBox tbHidden = new TextBox();
                    tbHidden.Visible = false;
                    if (value != null)
                        tbHidden.Text = value.ToString();
                    wc = tbHidden;
                    break;

                case BuilderType.DropDownList:
                    DropDownList ddl = (DropDownList)BuildListControl(sb, info);
                    if (!string.IsNullOrEmpty(sb.DataSource))
                        BindListControl(sb, info, dataSource, ddl);
                    if (value != null && ddl.Items.FindByValue(value.ToString()) != null)
                        ddl.SelectedValue = value.ToString();
                    ddl.CssClass = "sbControl";
                    wc = ddl;
                    break;

                case BuilderType.RadioButtonList:
                    RadioButtonList rbl = (RadioButtonList)BuildListControl(sb, info);
                    if (!string.IsNullOrEmpty(sb.DataSource))
                        BindListControl(sb, info, dataSource, rbl);
                    if (value != null && rbl.Items.FindByValue(value.ToString()) != null)
                        rbl.SelectedValue = value.ToString();
                    rbl.RepeatDirection = sb.ListRepeatDirection;
                    wc = rbl;
                    break;

                case BuilderType.GridView:
                    GridView gv = new GridView();
                    if (value != null)
                    {
                        gv.DataSource = value;
                        gv.DataBind();
                    }
                    wc = gv;
                    break;

                default:
                    wc = AutoDetermineControl(info, value);
                    break;
            }

            if (!string.IsNullOrEmpty(sb.CssClass))
                wc.CssClass = sb.CssClass;
            wc.ID = info.Name;
            return wc;
        }

        private static ListControl BuildListControl(ScreenBuilder sb, PropertyInfo info)
        {
            ListControl lc;
            if (sb.BuilderType == BuilderType.RadioButtonList)
                lc = new RadioButtonList();
            else
                lc = new DropDownList();

            if (sb.ListItemText != null &&
                string.IsNullOrEmpty(sb.DataSource))
            {
                for (int i = 0; i < sb.ListItemText.Length; i++)
                {
                    try
                    {
                        ListItem item = new ListItem(sb.ListItemText[i]);
                        if (sb.ListItemValues != null)
                            item.Value = sb.ListItemValues[i];
                        lc.Items.Add(item);
                    }
                    catch (Exception ex)
                    {
                        throw new ArgumentException("Could not populate drop down list for property " + info.Name + " with the specified list items", ex);
                    }
                }
            }
            if (sb.EnumDataSource != null &&
                string.IsNullOrEmpty(sb.DataSource))
                UIUtils.BindEnumToListControl(lc, sb.EnumDataSource);
            if (!string.IsNullOrEmpty(sb.DataMember))
                lc.DataMember = sb.DataMember;
            if (!string.IsNullOrEmpty(sb.DataTextField))
                lc.DataTextField = sb.DataTextField;
            if (!string.IsNullOrEmpty(sb.DataTextFormatString))
                lc.DataTextFormatString = sb.DataTextFormatString;
            if (!string.IsNullOrEmpty(sb.DataValueField))
                lc.DataValueField = sb.DataValueField;

            //lc.Width = Unit.Percentage(80);
            return lc;
        }

        private static void BindListControl(ScreenBuilder sb, PropertyInfo info, object dataSource, ListControl lc)
        {
            PropertyInfo dataSourceInfo = dataSource.GetType().GetProperty(sb.DataSource);
            if (dataSourceInfo == null)
                throw new Exception("Could not populate drop down list for property " + info.Name + ". The data source " + sb.DataSource + " was not found in the parent object.");
            object source = dataSourceInfo.GetValue(dataSource, null);
            if (source == null)
                throw new Exception("Could not populate drop down list for property " + info.Name + ". The data source " + sb.DataSource + " was null");
            lc.DataSource = source;
            lc.DataBind();
        }
        private static WebControl AutoDetermineControl(PropertyInfo info, object value)
        {
            Type t = info.PropertyType;

            if (t == typeof(bool))
            {
                BooleanRadioControl brc = new BooleanRadioControl();
                if (value != null)
                    brc.SelectedValue = value.ToString();
                return brc;
            }

            if (t == typeof(System.Nullable) ||
                t == typeof(string) ||
                t == typeof(DateTime) ||
                t == typeof(decimal))
            {
                TextBox tb = new TextBox();
                if (value != null)
                    tb.Text = value.ToString();
                tb.CssClass = "sbControl";
                return tb;
            }

            if (t.IsEnum)
            {
                ListControl ddl = new DropDownList();
                UIUtils.BindEnumToListControl(ddl, t);
                if (value != null)
                    ddl.SelectedValue = value.ToString();
                ddl.CssClass = "sbControl";
                return ddl;
            }

            throw new Exception("Could not auto-determine control type for property " + info.Name + " of type " + t);
        }


        

    }
}
