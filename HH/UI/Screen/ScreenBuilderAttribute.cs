﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace HH.Screen
{
    public enum BuilderType
    {
        Auto,
        Label,
        TextBox,
        TextArea,
        CheckBox,
        BooleanRadioControl,
        FileUploadTextBox,
        CheckBoxList,
        DropDownList,
        RadioButtonList,
        GridView,
        GridViewRedirect,
        EntityControl,
        EntityAssociation,
        AccordianControl,
        Hidden
    }

    public enum ValidationType
    {
        Auto,
        ValidateDate,
        ValidateNumericOnly,
        ValidateAlphaOnly,
        ValidateAlphaNumeric,
        BooleanIsTrue,
        BooleanIsFalse,
        Email,
        None
    }

    /// <summary>
    /// Attribute class used to adorn DAL properties, the Screen.ascx control reads these attributes to determine how to present the property on a web form
    /// </summary>
    public class ScreenBuilder : Attribute
    {
        #region BASE PROPERTIES FOR ALL CONTROLS

        public string[] ScreenIDs { get; set; }
        public string[] EntityControlTargetIDs { get; set; }

        public BuilderType BuilderType { get; set; }

        public string Caption { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }
        public string CssClass { get; set; }
        public string InformationToolTip { get; set; }

        
        public int FieldOrder { get; set; }

        //a reimer 08/08/11 added to order the fields on summary view
        public Int32 SummaryViewFieldOrder { get; set; }

        #endregion

        #region VALIDATION PROPS

        public ValidationType ValidationType { get; set; }
        public bool ValidateAsRequiredField { get; set; }
        public int ValidateMinLength { get; set; }
        public int ValidateMaxLength { get; set; }
        public bool ValidationMaskVisible { get; set; }
        public bool ValidationCalendarVisible { get; set; }
        public string ValidationMask { get; set; }

        #endregion

        #region STANDARD CONTROL PROPERTIES

        public int TextAreaRows { get; set; }

        #endregion

        #region LIST CONTROL PROPERTIES

        public string[] ListItemText { get; set; }
        public string[] ListItemValues { get; set; }
        public RepeatDirection ListRepeatDirection { get; set; }
        public Type EnumDataSource { get; set; }
        public string DataMember { get; set; }
        public string DataSource { get; set; }
        public string DataTextField { get; set; }
        public string DataTextFormatString { get; set; }
        public string DataValueField { get; set; }
        public string[] DataKeyNames { get; set; }
        public string GridViewRedirectBaseUrl { get; set; }
        public string GridViewRedirectButtonText { get; set; }

        #endregion

        #region ACCORDIAN CONTROL PROPERTIES
        
        public string AccordianControlID { get; set; }
        public string[] AccordianTargetIDs { get; set; }
        public string[] AccordianPaneIDs { get; set; }
        public string[] PaneItemText { get; set; }
        public string[] PaneItemValues { get; set; }
        public Type PaneEnumDataSource { get; set; }
        
        #endregion

        #region ENTITY CONTROL PROPERTIES

        public string EntityControlID { get; set; }
        public bool AllowInsert { get; set; }
        public bool AllowUpdate { get; set; }
        public bool AllowDelete { get; set; }
        /// <summary>
        /// Set this if the Entity property that contains the data is different than the property being referenced
        /// </summary>
        public string DataTarget { get; set; }

        #endregion

        #region ENTITY ASSOCIATION PROPERTIES

        //no specific props

        #endregion


        public ScreenBuilder()
        {
            BuilderType = BuilderType.Auto;
            FieldOrder = -1;

            //a reimer 08/08/11
            SummaryViewFieldOrder = Int32.MaxValue;

            ValidationType = ValidationType.Auto;
            ValidateAsRequiredField = false;
            ValidationMaskVisible = false;
            ValidationCalendarVisible = true;
            ValidateMinLength = -1;
            ValidateMaxLength = -1;

            TextAreaRows = 0;

            ListRepeatDirection = RepeatDirection.Horizontal;

            AllowInsert = true;
            AllowUpdate = true;
            AllowDelete = true;
        }
    }
}
