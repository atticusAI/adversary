﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using HH.UI.ModalPopupForm;


namespace HH.Screen
{
    public enum EntitySubmitType
    {
        Create,
        Modify,
        Delete,
        Cancel
    }

    public enum ListContainerFormMode
    {
        AutoGenerate,
        Custom,
        Off
    }


    public delegate void EntitySubmitEventHandler(IListContainerControl sender, object dataSource, object entity, EntitySubmitType type);
    public delegate void GridFieldsAdding(GridView sender, object dataSource);

    /// <summary>
    /// Interface for the ListContainerControl.ascx user control, TODO: move user control to HH library
    /// Best used to represent a list of entities
    /// </summary>
    public interface IListContainerControl
    {
        event ScreenRowControlCreatingEventHandler ScreenRowControlCreating;
        event ScreenRowControlCreatedEventHandler ScreenRowControlCreated;
        event ScreenRowCreatedEventHandler ScreenRowCreated;
        event GridFieldsAdding FieldsAdding;
        event GridViewRowEventHandler RowDataBound;
        event GridViewRowEventHandler RowCreated;
        event GridViewCommandEventHandler RowCommand;
        event EntitySubmitEventHandler OnEntityPreSubmit;
        event EntitySubmitEventHandler OnEntitySubmitted;
        event EntitySubmitEventHandler OnEntitySubmitComplete;

        ListContainerFormMode FormMode { get; set; }
        IModalForm CustomForm { get; set; }

        string ID { get; set; }
        string EntityID { get; set; }
        string ScreenID { get; set; }
        bool AutoGenerateColumns { get; set; }
        bool IsChildOfScreenControl { get; set; }
        string ConnectionString { get; set; }
        Unit FormWidth { get; set; }
        Unit GridWidth { get; set; }
        string CssClass { get; set; }
        string CaptionCssClass { get; set; }
        object DataSource { get; set; }
        string Caption { get; set; }
        string InformationToolTip { get; set; }
        string AddButtonText { get; set; }
        bool AllowAdd { get; set; }
        bool AllowModify { get; set; }
        bool AllowDelete { get; set; }

        void DataBind();
        void Update();
    }
}