﻿using System.Web.UI;

/// <summary>
/// Summary description for EmbedTemplate
/// </summary>
namespace HH.Extensions
{
    public class EmbedTemplate : TemplateControl
    {
        private object _data;

        public EmbedTemplate(object data)
        {
            _data = data;
        }

        public object GetEmbeddedData()
        {
            return _data;
        }
    }

}
