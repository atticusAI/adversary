﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Reflection;
using HH.Utils;

/// <summary>
/// Summary description for ExtensionMethods
/// </summary>
namespace HH.Extensions
{
    public static class ExtensionMethods
    {
        #region CollectionConvertWorker Methods
        /// <summary>
        /// Populates a DataTable with a with an IEnumerable source
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerableSource"></param>
        /// <param name="destinationTable"></param>
        public static void FillDataTable<T>(this IEnumerable<T> enumerableSource, DataTable destinationTable) where T : class
        {
            using (IEnumerator<T> e = enumerableSource.GetEnumerator())
                new CollectionConvertWorker<T>().FillDataTable(e, destinationTable);
        }
        /// <summary>
        /// Populates a DataTable with IList source
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="listSource"></param>
        /// <param name="destinationTable"></param>
        public static void FillDataTable<T>(this IList<T> listSource, DataTable destinationTable) where T : class
        {
            using (IEnumerator<T> e = listSource.GetEnumerator())
                new CollectionConvertWorker<T>().FillDataTable(e, destinationTable);
        }
        /// <summary>
        /// Populates and IList with a DataTable source
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dtSource"></param>
        /// <param name="destinationObject"></param>
        public static void FillIList<T>(this DataTable dtSource, IList<T> destinationObject) where T : class
        {
            new CollectionConvertWorker<T>().FillIList(dtSource, destinationObject);
        }
        #endregion

        #region EmbedTemplate methods
        /// <summary>
        /// Allows a DataContext to be associated with an ASP.Net web control
        /// </summary>
        /// <param name="c"></param>
        /// <param name="data"></param>
        public static void SetDataItem(this WebControl c, object data)
        {
            c.TemplateControl = new EmbedTemplate(data);   
        }
        /// <summary>
        /// Gets the DataContext associated with an ASP.Net web control
        /// </summary>
        /// <param name="c"></param>
        /// <returns>null if no EmbedTemplate exists in the WebControl's TemplateControl property or if the SetDataItem() method was not called</returns>
        public static object GetDataItem(this WebControl c)
        {
            if (c.TemplateControl == null)
                return null;
            if (!(c.TemplateControl is EmbedTemplate))
                return null;

            return ((EmbedTemplate)c.TemplateControl).GetEmbeddedData();
        }
        #endregion
    }

    
    
}
