﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq.Mapping;
using System.Reflection;
using HH.Utils;

/// <summary>
/// Summary description for CollectionConverterWorker
/// </summary>
namespace HH.Extensions
{
    public class CollectionConvertWorker<T>
    {
        private Type _type;
        private Dictionary<string, int> _ordinalMap;

        /// <summary>
        /// Back and forth conversion between IEnumerator collections and DataTables
        /// </summary>
        public CollectionConvertWorker()
        {
            _type = typeof(T);
        }

        #region Convert to DataTable
        /// <summary>
        /// Use an IEnumerator to populate a DataTable
        /// </summary>
        /// <param name="e">The data</param>
        /// <param name="destination">The datatable to populate</param>
        public void FillDataTable(IEnumerator<T> e, DataTable destination)
        {
            _ordinalMap = new Dictionary<string, int>();

            BuildDataTable(destination);
            destination.BeginLoadData();
            while (e.MoveNext())
                destination.LoadDataRow(GetRowData(destination, e.Current), true);
            destination.EndLoadData();
        }

        private DataTable BuildDataTable(DataTable dt)
        {
            if (string.IsNullOrEmpty(dt.TableName))
                dt.TableName = _type.Name;
            foreach (FieldInfo info in _type.GetFields())
            {
                if (!info.IsInitOnly && !info.IsPrivate)
                    AddDataColumn(dt, info.Name, info.FieldType);
            }
            foreach (PropertyInfo info in _type.GetProperties())
            {
                if (!info.CanRead)
                    continue;
                MethodInfo m = info.GetGetMethod();
                if (!m.IsStatic && !m.IsPrivate)
                    AddDataColumn(dt, info.Name, info.PropertyType);
            }
            return dt;
        }

        private void AddDataColumn(DataTable dt, string name, Type type)
        {
            if (_ordinalMap.ContainsKey(name))
                return;

            DataColumn dc;
            if (type.IsGenericType &&
                type.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
                dc = dt.Columns.Add(name, Nullable.GetUnderlyingType(type));
            else
                dc = dt.Columns.Add(name, type);
            if (!_ordinalMap.ContainsKey(name))
                _ordinalMap.Add(name, dc.Ordinal);
        }

        private object[] GetRowData(DataTable dt, T rowObject)
        {
            object[] values = new object[dt.Columns.Count];
            foreach (FieldInfo info in _type.GetFields())
            {
                if (!info.IsInitOnly && !info.IsPrivate)
                    values[_ordinalMap[info.Name]] = info.GetValue(rowObject);
            }
            foreach (PropertyInfo info in _type.GetProperties())
            {
                MethodInfo m = info.GetGetMethod();
                if (!m.IsStatic && !m.IsPrivate)
                    values[_ordinalMap[info.Name]] = m.Invoke(rowObject, null);
            }
            return values;
        }
        #endregion

        #region Convert to IList
        /// <summary>
        /// Fill an IList< with data from a DataTable
        /// </summary>
        /// <param name="source">The data to use</param>
        /// <param name="destination">The destination list to populate</param>
        public void FillIList(DataTable source, IList<T> destination)
        {
            foreach (DataRow dr in source.Rows)
            {
                T destinationObj = (T)ReflectionUtils.CreateNewInstanceOfType(_type);

                foreach (DataColumn dc in source.Columns)
                {
                    object val = dr[dc];
                    if (val == DBNull.Value)
                        val = null;

                    PropertyInfo p = _type.GetProperty(dc.ColumnName);
                    if (p != null)
                    {
                        AssociationAttribute assoc = ReflectionUtils.GetAttribute<AssociationAttribute>(p);
                        if (assoc != null && assoc.IsForeignKey) //weed out entity refs
                            continue;

                        MethodInfo m = p.GetSetMethod();
                        if (m != null && !m.IsStatic && !m.IsPrivate)
                        {
                            val = HandleCast(val, p.PropertyType);
                            m.Invoke(destinationObj, new object[] { val });
                        }
                        continue;
                    }

                    FieldInfo f = _type.GetField(dc.ColumnName);
                    if (f != null && !f.IsInitOnly && !f.IsPrivate && f.FieldType != _type)
                    {
                        val = HandleCast(val, p.PropertyType);
                        f.SetValue(destinationObj, val);
                    }
                }
                destination.Add(destinationObj);
            }
        }

        /// <summary>
        /// For runtime casting, TODO: make this a private method
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="o"></param>
        /// <returns></returns>
        public static CastType Cast<CastType>(object o)
        {
            return (CastType)o;
        }

        /// <summary>
        /// Handles the runtime casting calls
        /// </summary>
        /// <param name="val"></param>
        /// <param name="convertTo"></param>
        /// <returns></returns>
        private object HandleCast(object val, Type convertTo)
        {
            if (convertTo.IsGenericType &&
                convertTo.GetGenericTypeDefinition() == typeof(Nullable<>))
                return ReflectionUtils.InvokeCast(val, convertTo);
            MethodInfo castMethod = this.GetType().GetMethod("Cast").MakeGenericMethod(convertTo);
            return castMethod.Invoke(null, new object[] { val });
        }
        #endregion
    }


    //public static class DataSetLinqOperators
    //{
    //    public static DataTable CopyToDataTable2<T>(this IEnumerable<T> source)
    //    {
    //        return new ObjectShredder<T>().Shred(source, null, null);
    //    }

    //    public static DataTable CopyToDataTable2<T>(this IEnumerable<T> source,
    //                                                DataTable table, LoadOption? options)
    //    {
    //        return new ObjectShredder<T>().Shred(source, table, options);
    //    }

    //}

    //public class ObjectShredder<T>
    //{
    //    private FieldInfo[] _fi;
    //    private PropertyInfo[] _pi;
    //    private Dictionary<string, int> _ordinalMap;
    //    private Type _type;

    //    public ObjectShredder()
    //    {
    //        _type = typeof(T);
    //        _fi = _type.GetFields();
    //        _pi = _type.GetProperties();
    //        _ordinalMap = new Dictionary<string, int>();
    //    }

    //    public DataTable Shred(IEnumerable<T> source, DataTable table, LoadOption? options)
    //    {
    //        if (typeof(T).IsPrimitive)
    //        {
    //            return ShredPrimitive(source, table, options);
    //        }


    //        if (table == null)
    //        {
    //            table = new DataTable(typeof(T).Name);
    //        }

    //        // now see if need to extend datatable base on the type T + build ordinal map
    //        table = ExtendTable(table, typeof(T));

    //        table.BeginLoadData();
    //        using (IEnumerator<T> e = source.GetEnumerator())
    //        {
    //            while (e.MoveNext())
    //            {
    //                if (options != null)
    //                {
    //                    table.LoadDataRow(ShredObject(table, e.Current), (LoadOption)options);
    //                }
    //                else
    //                {
    //                    table.LoadDataRow(ShredObject(table, e.Current), true);
    //                }
    //            }
    //        }
    //        table.EndLoadData();
    //        return table;
    //    }

    //    public DataTable ShredPrimitive(IEnumerable<T> source, DataTable table, LoadOption? options)
    //    {
    //        if (table == null)
    //        {
    //            table = new DataTable(typeof(T).Name);
    //        }

    //        if (!table.Columns.Contains("Value"))
    //        {
    //            table.Columns.Add("Value", typeof(T));
    //        }

    //        table.BeginLoadData();
    //        using (IEnumerator<T> e = source.GetEnumerator())
    //        {
    //            Object[] values = new object[table.Columns.Count];
    //            while (e.MoveNext())
    //            {
    //                values[table.Columns["Value"].Ordinal] = e.Current;

    //                if (options != null)
    //                {
    //                    table.LoadDataRow(values, (LoadOption)options);
    //                }
    //                else
    //                {
    //                    table.LoadDataRow(values, true);
    //                }
    //            }
    //        }
    //        table.EndLoadData();
    //        return table;
    //    }

    //    public DataTable ExtendTable(DataTable table, Type type)
    //    {
    //        // value is type derived from T, may need to extend table.
    //        foreach (FieldInfo f in type.GetFields())
    //        {
    //            if (!_ordinalMap.ContainsKey(f.Name))
    //            {
    //                DataColumn dc = table.Columns.Contains(f.Name) ? table.Columns[f.Name] : table.Columns.Add(f.Name);
    //                _ordinalMap.Add(f.Name, dc.Ordinal);
    //            }
    //        }
    //        foreach (PropertyInfo p in type.GetProperties())
    //        {
    //            if (!_ordinalMap.ContainsKey(p.Name))
    //            {
    //                DataColumn dc = table.Columns.Contains(p.Name) ? table.Columns[p.Name]
    //                    : table.Columns.Add(p.Name);//, p.PropertyType);
    //                _ordinalMap.Add(p.Name, dc.Ordinal);
    //            }
    //        }
    //        return table;
    //    }

    //    public object[] ShredObject(DataTable table, T instance)
    //    {

    //        FieldInfo[] fi = _fi;
    //        PropertyInfo[] pi = _pi;

    //        if (instance.GetType() != typeof(T))
    //        {
    //            ExtendTable(table, instance.GetType());
    //            fi = instance.GetType().GetFields();
    //            pi = instance.GetType().GetProperties();
    //        }

    //        Object[] values = new object[table.Columns.Count];
    //        foreach (FieldInfo f in fi)
    //        {
    //            values[_ordinalMap[f.Name]] = f.GetValue(instance);
    //        }

    //        foreach (PropertyInfo p in pi)
    //        {
    //            values[_ordinalMap[p.Name]] = p.GetValue(instance, null);
    //        }
    //        return values;
    //    }
    //}
}
