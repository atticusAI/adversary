﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace HH.Utils
{
    /// <summary>
    /// Summary description for SiteMapUtils
    /// </summary>
    public static class SiteMaps
    {
        public static bool IsSiteMapNodeVisible(SiteMapNode node)
        {
            return ((node["Visible"] == null) ||
                    ((node["Visible"] != null) &&
                    Boolean.Parse(node["Visible"])));
        }

        public static bool IsSiteMapNodeEnabled(SiteMapNode node)
        {
            return ((node["Enabled"] == null) ||
                    ((node["Enabled"] != null) &&
                    Boolean.Parse(node["Enabled"])));
        }

        public static bool IsConfirmRedirectEnabled(SiteMapNode node)
        {
            return ((node["ConfirmRedirect"] != null) &&
                    Boolean.Parse(node["ConfirmRedirect"]));
        }



        public static string GetTarget(SiteMapNode n)
        {
            if (string.IsNullOrEmpty(n["target"]))
                return null;
            return n["target"];
        }

        public static bool IsUserAuthorized(SiteMapNode node)
        {
            if (node == null)
                return true; //throw new ArgumentException("Could not determine if page is locked. SiteMap data was not available.");

            if (!Roles.Enabled)
                return true;

            int safeCount = 0;
            while (node != null)
            {
                if (safeCount >= 200)
                    return true;

                System.Collections.IList authorizedRoles = node.Roles;
                if (authorizedRoles != null && authorizedRoles.Count > 0)
                {
                    string[] arr = (string[])Array.CreateInstance(typeof(string), authorizedRoles.Count);
                    authorizedRoles.CopyTo(arr, 0);

                    if (!IsUserAuthorized(arr))
                        return false;
                }
                node = node.ParentNode;
                safeCount++;
            }

            return true;
        }

        public static bool IsUserAuthorized(string[] authorizedRoles)
        {
            if (!Roles.Enabled)
                return true;

            bool authorized = authorizedRoles.Length == 0;
            foreach (string role in authorizedRoles)
            {
                if (Roles.IsUserInRole(role))
                {
                    authorized = true;
                    break;
                }
            }
            return authorized;
        }
    }
}