﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.IO;

namespace HH.Utils
{
    public static class FileUtils
    {
        public static void UploadFile(HttpPostedFile postedFile, string uploadDirectoryPath)
        {
            string fileName = postedFile.FileName;
            if (fileName.Contains("\\"))
                fileName = fileName.Substring(fileName.LastIndexOf("\\")+1);

            if (!Directory.Exists(uploadDirectoryPath))
                Directory.CreateDirectory(uploadDirectoryPath);
            if (!uploadDirectoryPath.EndsWith("\\"))
                uploadDirectoryPath += "\\";
            string path = uploadDirectoryPath + fileName;
            postedFile.SaveAs(path);
        }

        public static void DownloadFile(HttpResponse response, string filePath)
        {
            if (!File.Exists(filePath))
                throw new Exception("File at " + filePath + " does not exist");

            FileInfo fileInfo = new FileInfo(filePath);
            response.Clear();
            response.ContentType = MimeMap.getContentType(fileInfo.Extension);
            response.AddHeader("Content-Disposition", "attachment; filename=" + fileInfo.Name);
            response.TransmitFile(fileInfo.FullName);
            response.End();
        }

        public static void DeleteFile(string path)
        {
            if (File.Exists(path))
                File.Delete(path);
        }

        public static void MoveFile(string oldPath, string newPath)
        {
            if (File.Exists(oldPath))
                File.Move(oldPath, newPath);
        }

        
    }
}
