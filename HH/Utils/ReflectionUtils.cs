﻿using System;
using System.Reflection;
using System.Collections;
using System.Data.Linq.Mapping;
using System.Data;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text.RegularExpressions;

namespace HH.Utils
{
    public static class ReflectionUtils
    {
        public static bool HasAttribute<T>(PropertyInfo info)
        {
            return GetAttribute<T>(info) != null;
        }

        public static T GetAttribute<T>(object obj, string propertyName)
        {
            MemberInfo[] info = obj.GetType().GetMember(propertyName);
            foreach (MemberInfo member in info)
            {
                T attribute = GetAttribute<T>(member);
                if (attribute != null)
                    return attribute;
            }
            return (T)((object)null);
        }
        public static T GetAttribute<T>(MemberInfo member)
        {
            return GetAttribute<T>((Attribute[])member.GetCustomAttributes(typeof(T), true));
        }
        public static T GetAttribute<T>(Attribute[] attributes)
        {
            foreach (Attribute attribute in attributes)
            {
                if (attribute is T)
                    return (T)((object)attribute);
            }
            return (T)((object)null);
        }


        public static object CreateNewInstanceOfType(Type type) 
        {
            if (type.IsGenericType)
            {
                Type genericType = type.GetGenericArguments()[0];
                return genericType.Assembly.CreateInstance(genericType.Name);
            }
            else if (type.IsArray)
            {
                Type elem = type.GetElementType();
                return elem.Assembly.CreateInstance(elem.Name);
            }
            else
            {
                return type.Assembly.CreateInstance(type.Name);
            }
        }

        public static void SetEntityKey(ref object entity, object keyValue)
        {
            PropertyInfo key = null;
            foreach (PropertyInfo info in entity.GetType().GetProperties())
            {
                ColumnAttribute c = GetAttribute<ColumnAttribute>(info);
                if (c != null && c.IsPrimaryKey)
                {
                    key = info;
                    break;
                }

                AssociationAttribute a = GetAttribute<AssociationAttribute>(info);
                if (a != null && a.IsForeignKey)
                    key = info;
            }

            if (key != null)
                key.SetValue(entity, keyValue, null);
            else
                throw new Exception("Could not set key to value " + keyValue + " for data object " + entity.GetType().Name + ". No key was found.");
        }

        public static void CastAndSetProperty(object parentObject, string propertyName, object value)
        {
            PropertyInfo info = parentObject.GetType().GetProperty(propertyName);
            object castedObject = InvokeCast(value, info.PropertyType);
            info.SetValue(parentObject, castedObject, null);
        }
        public static object InvokeCast(object value, Type castToType)
        {
            MethodInfo convertMethod = typeof(ReflectionUtils).GetMethod("CastToType", BindingFlags.Static | BindingFlags.NonPublic).MakeGenericMethod(castToType);
            return convertMethod.Invoke(null, new object[] { value });
        }

        private static T CastToType<T>(object value)
        {

            //if (typeof(T).IsEnum)
            if (value != null &&
                Nullable.GetUnderlyingType(typeof(T)) != null &&
                Nullable.GetUnderlyingType(typeof(T)).IsEnum)
            {
                Regex hasAlpha = new Regex("[^0-9]");
                if (hasAlpha.IsMatch(value.ToString())) 
                    return (T)Enum.Parse(Nullable.GetUnderlyingType(typeof(T)), value.ToString(), true);

                if (!(value is int))
                    value = Int32.Parse(value.ToString());

                return (T)Enum.ToObject(Nullable.GetUnderlyingType(typeof(T)), value);
            }
            TypeConverter tc = TypeDescriptor.GetConverter(typeof(T));
            return (T)tc.ConvertFrom(value);
        }

        public static void RegisterTypeConverter<T, TC>() where TC : TypeConverter
        {
            TypeDescriptor.AddAttributes(typeof(T), new TypeConverterAttribute(typeof(TC)));
        }
    }


}
