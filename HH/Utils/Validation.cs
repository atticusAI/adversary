﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HH
{
    #region ValidationException class
    public class ValidationException : Exception
    {
        private List<string> _reasons = new List<string>();
        public string[] Reasons
        {
            get
            {
                return _reasons.ToArray();
            }
        }

        public string GetInvalidMessage()
        {
            string message = this.Message;
            if (HasInvalidReasons())
            {
                message += "<ul>";
                foreach (string reason in _reasons)
                {
                    message += "<li>" + reason + "</li>";
                }
                message += "</ul>";
            }
            return message;
        }

        public ValidationException(string message) : base(message) { }

        public void AddInvalidReason(string reason)
        {
            _reasons.Add(reason);
        }

        public bool HasInvalidReasons()
        {
            return (_reasons.Count > 0);
        }
    }
    #endregion

    #region CONTROL VALIDATION
    public static class Validation
    {
        /// <summary>
        /// Makes the control a required field to submit the form
        /// </summary>
        /// <param name="validatorPlaceHolder">The control that the validation control and ajax extender will be add to</param>
        /// <param name="controlToValidate">The control to check</param>
        public static void BindRequiredFieldValidation(ref Control validatorPlaceHolder, Control controlToValidate)
        {
            RequiredFieldValidator v = new RequiredFieldValidator();
            v.ID = "Validator___" + controlToValidate.ID;
            //if (value != null)
            //    v.InitialValue = value.ToString();
            v.ControlToValidate = controlToValidate.ID;
            v.ErrorMessage = "This field canot be blank";
            v.Display = ValidatorDisplay.None;
            v.EnableClientScript = true;
            validatorPlaceHolder.Controls.Add(v);
            BindCalloutExtender(ref validatorPlaceHolder, v.ID);
        }

        /// <summary>
        /// Checks a text box for a numeric value
        /// </summary>
        /// <param name="validatorPlaceHolder">The control that the validation control and ajax extender will be add to</param>
        /// <param name="tbToValidate">The control to check</param>
        public static void BindNumericValidation(ref Control validatorPlaceHolder, TextBox tbToValidate)
        {
            BindRegExValidation(ref validatorPlaceHolder, tbToValidate, "[0-9]+" /* [0-9][.]+ */, "This field accepts numeric charcters (0-9) only");
        }

        /// <summary>
        /// Adds regular expression validation to a text box control
        /// </summary>
        /// <param name="validatorPlaceHolder">The control that the validation control and ajax extender will be add to</param>
        /// <param name="tbToValidate">The control to check</param>
        /// <param name="regEx">The regular expression</param>
        /// <param name="errorMessage">The message to display if the control is invalid</param>
        public static void BindRegExValidation(ref Control validatorPlaceHolder, TextBox tbToValidate, string regEx, string errorMessage)
        {
            //add the validation
            RegularExpressionValidator v = new RegularExpressionValidator();
            v.ID = "Validator___" + tbToValidate.ID;
            v.ControlToValidate = tbToValidate.ID;
            v.ValidationExpression = regEx;
            v.ErrorMessage = errorMessage;
            v.Display = ValidatorDisplay.None;
            v.EnableClientScript = true;
            validatorPlaceHolder.Controls.Add(v);
            BindCalloutExtender(ref validatorPlaceHolder, v.ID);
            tbToValidate.CausesValidation = true;
        }

        /// <summary>
        /// Adds date validation and a popup calendar to a textbox control
        /// </summary>
        // <param name="validatorPlaceHolder">The control that the validation control and ajax extender will be add to</param>
        /// <param name="tbToValidate">The control to check</param>
        public static void BindDateValidation(ref Control validatorPlaceHolder, TextBox tbToValidate) { Validation.BindDateValidation(ref validatorPlaceHolder, tbToValidate, null); }
        /// <summary>
        /// Adds date validation and a popup calendar to a textbox control
        /// </summary>
        /// <param name="validatorPlaceHolder">The control that the validation control and ajax extender will be add to</param>
        /// <param name="tbToValidate">The control to check</param>
        /// <param name="value">the initial date value (if any)</param>
        public static void BindDateValidation(ref Control validatorPlaceHolder, TextBox tbToValidate, DateTime? value)
        {
            //add the calendar control
            ImageButton btn = new ImageButton();
            btn.ID = "btnCal" + tbToValidate.ID;
            btn.ToolTip = "Click to display calendar";
            btn.ImageUrl = "~/_css/calendar.png";
            btn.Style.Add("margin", "6px 5px 0px 8px");
            btn.Style.Add("padding", "0px 0px 0px 0px");
            AjaxControlToolkit.CalendarExtender cal = new AjaxControlToolkit.CalendarExtender();
            cal.ID = "cal" + tbToValidate.ID;
            cal.PopupButtonID = btn.ID;
            cal.TargetControlID = tbToValidate.ID;
            cal.Format = "d";
            if (value == null)
            {
                cal.SelectedDate = (DateTime?)DateTime.Now;
                tbToValidate.Text = DateTime.Now.ToString("d");
            }
            else
            {
                cal.SelectedDate = value;
            }
            cal.CssClass = "popupCalendarExtender";
            validatorPlaceHolder.Page.UICulture = "auto";
            validatorPlaceHolder.Page.Culture = "auto";
            validatorPlaceHolder.Page.Controls.Add(btn);
            validatorPlaceHolder.Page.Controls.Add(cal);

            //add the mask
            AjaxControlToolkit.MaskedEditExtender mask = new AjaxControlToolkit.MaskedEditExtender();
            mask.ID = "Mask_" + tbToValidate.ID;
            mask.TargetControlID = tbToValidate.ID;
            mask.MaskType = AjaxControlToolkit.MaskedEditType.Date;
            mask.Mask = "99/99/9999";
            mask.AutoComplete = true;
            mask.AutoCompleteValue = "0";
            mask.AcceptAMPM = false;
            mask.AcceptNegative = AjaxControlToolkit.MaskedEditShowSymbol.None;
            mask.ClipboardEnabled = true;
            validatorPlaceHolder.Controls.Add(mask);

            //add the validation
            RangeValidator v = new RangeValidator();
            v.Type = ValidationDataType.Date;
            v.ID = "Validator___" + tbToValidate.ID;
            v.ControlToValidate = tbToValidate.ID;
            v.MinimumValue = "01/01/1500";
            v.MaximumValue = "12/31/2500";
            v.ErrorMessage = "Please enter a valid date in the format MM/DD/YYYY";
            v.Display = ValidatorDisplay.None;
            v.EnableClientScript = true;
            validatorPlaceHolder.Controls.Add(v);
            BindCalloutExtender(ref validatorPlaceHolder, v.ID);
            //tbToValidate.CausesValidation = true;
        }

        /// <summary>
        /// Adds the ajax validator animation control to the validator place holder
        /// </summary>
        /// <param name="validatorPlaceHolder">The control that the validation control and ajax extender will be add to</param>
        /// <param name="validatorTargetControlID">The ID of the validator control that will activate the popup</param>
        public static void BindCalloutExtender(ref Control validatorPlaceHolder, string validatorTargetControlID)
        {
            //adds the dialog box
            AjaxControlToolkit.ValidatorCalloutExtender ve = new AjaxControlToolkit.ValidatorCalloutExtender();
            ve.ID = validatorTargetControlID + "_Ext";
            ve.TargetControlID = validatorTargetControlID;
            validatorPlaceHolder.Controls.Add(ve);
        }


        /// <summary>
        /// Validates
        /// </summary>
        /// <param name="validatorPlaceHolder"></param>
        /// <param name="c"></param>
        /// <param name="screenBuilderValidationType"></param>
        //public static void AddBooleanValidation(ref Control validatorPlaceHolder, Control c, ScreenBuilderValidationType screenBuilderValidationType)
        //{
        //    throw new NotImplementedException();
        //}
    }
    #endregion
}
