﻿using System;
using System.Configuration;
using System.Net.Mail;
using System.Web.UI;
using System.Net;

namespace HH
{
    public static class Email
    {
        public static void SendErrorEmail(Exception ex) { SendErrorEmail(ex, null); }
        public static void SendErrorEmail(Exception ex, Page page)
        {
            if (!Boolean.Parse(ConfigurationManager.AppSettings["ErrorEmailOn"]))
                return;

            SmtpClient host = new SmtpClient(ConfigurationManager.AppSettings["ErrorEmailHost"].Trim());
            MailAddressCollection to = BuildAddressCollection(ConfigurationManager.AppSettings["ErrorEmailTo"]);
            MailAddress from;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["ErrorEmailFromName"]))
                from = new MailAddress(ConfigurationManager.AppSettings["ErrorEmailFrom"].Trim(),
                                       ConfigurationManager.AppSettings["ErrorEmailFromName"].Trim());
            else
                from = new MailAddress(ConfigurationManager.AppSettings["ErrorEmailFrom"]);
            string subject = ConfigurationManager.AppSettings["ErrorEmailSubject"].Trim();
            string msg = UI.UIUtils.BuildErrorMessage(ex);
            if (page != null)
                msg = "<strong>File Path:</strong> " + page.Request.CurrentExecutionFilePath + "<br /><br />" + msg;

            SendEmail(host, to, from, subject, msg, true);
        }

        public static void SendEmail(string host, string to, string from, string subject, string message, bool isBodyHtml) { SendEmail(host, to, from, subject, message, isBodyHtml, null); }
        public static void SendEmail(string host, string to, string from, string subject, string message, bool isBodyHtml, NetworkCredential credentials)
        {
            SmtpClient client = new SmtpClient(host);
            if (credentials != null)
                client.Credentials = credentials;
            MailAddressCollection col = new MailAddressCollection();
            col.Add(to);

            SendEmail(client, col, new MailAddress(from), subject, message, isBodyHtml);
        }
        public static void SendEmail(SmtpClient host, MailAddressCollection to, MailAddress from, string subject, string message, bool isBodyHtml)
        {
            MailMessage msg = new MailMessage();

            foreach (MailAddress address in to)
                msg.To.Add(address);
            msg.From = from;
            msg.Subject = subject;
            msg.Body = message.Trim();

            SendEmail(host, msg, isBodyHtml);
        }
        public static void SendEmail(SmtpClient host, MailMessage msg, bool isBodyHtml)
        {
            
            //msg.Priority = MailPriority.Normal;
            //msg.IsBodyHtml = isBodyHtml;
            //testing: set the to field to the qa person

            MailMessage modifiedMsg = new MailMessage();
            foreach (MailAddress address in msg.To)
            {
                modifiedMsg.To.Add(address);
            }
            modifiedMsg.From = msg.From;
            modifiedMsg.Body = msg.Body;
            modifiedMsg.Priority = MailPriority.Normal;
            modifiedMsg.IsBodyHtml = isBodyHtml;
            modifiedMsg.Subject = msg.Subject;

            //host.Send(msg);
            host.Send(modifiedMsg);
        }

        private static MailAddressCollection BuildAddressCollection(string recipientList)
        {
            string[] recipients = recipientList.Trim().Split(new Char[] { ';' }, StringSplitOptions.None);
            MailAddressCollection to = new MailAddressCollection();
            foreach (string recipient in recipients)
            {
                if (recipient != null && recipient.Trim() != string.Empty)
                {
                    to.Add(new MailAddress(recipient.Trim()));
                }
            }

            return to;
        }
    
    }
}