﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace HH.Utils
{
    /// <summary>
    /// Summary description for ConfigUtils
    /// </summary>
    public static class Config
    {
        /// <summary>
        /// Check the status debug flag in web.config.
        /// </summary>
        /// <returns>True if debug flag is true</returns>
        public static bool GetDebugStatus()
        {
            try
            {
                Configuration webConfig = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");
                ConfigurationElement compilationElem = webConfig.SectionGroups["system.web"].Sections["compilation"];
                return Convert.ToBoolean(compilationElem.ElementInformation.Properties["debug"].Value);
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the connection string for the specified key. If debug flag is set to true in the web.config file
        /// this method will append 'Debug' to the specified key and return the debug string.
        /// </summary>
        /// <param name="dbKey">the key to get</param>
        /// <returns>db conn string</returns>
        public static string GetConnectionString(string dbKey)
        {
            //string conn = (GetDebugStatus()) ? dbKey + "Debug" : dbKey;
            //if (ConfigurationManager.ConnectionStrings[conn] != null)
            //    return ConfigurationManager.ConnectionStrings[conn].ToString();
            //else
            //    throw new ConfigurationErrorsException("Connection string with key " + dbKey + " is unavailable");
            string conn = "";
            try
            {
                using (ConnectionManagerService.ConnectionManagerServiceClient client = 
                    new ConnectionManagerService.ConnectionManagerServiceClient())
                {
                    conn = client.GetConnectionString(dbKey, false);
                }
            }
            catch (Exception ex)
            {
                throw new ConfigurationErrorsException("Connection string with key " + dbKey + " could not be found on the service." +
                " Additional information: " + ex.Message);
            }
            
            return conn;

        }
    }
}