﻿using System;
using System.DirectoryServices;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Security;

namespace HH.Authentication
{
    public static class SecurityUtils
    {
        public static bool AuthenticateAgainstActiveDirectory(string adServer, string userName, string password, string domain)
        {
            if (string.IsNullOrEmpty(adServer))
                throw new ArgumentException("Could not authenticate this request. No active directory host was specified");
            if (string.IsNullOrEmpty(domain))
                throw new ArgumentException("Could not authenticate this request. No domain was specified");

            userName = userName.Trim();

            ValidationException validation = new ValidationException("Your login attempt was invalid");
            if (string.IsNullOrEmpty(userName))
                validation.AddInvalidReason("User name was not supplied");
            if (string.IsNullOrEmpty(password))
                validation.AddInvalidReason("Password was not supplied");

            if (validation.HasInvalidReasons())
                throw validation;

            DirectoryEntry entry = new DirectoryEntry(adServer, userName, password);

            //check authentication
            try
            {
                Object obj = entry.NativeObject;
            }
            catch (DirectoryServicesCOMException ex)
            {
                if (ex.Message.ToLower().Contains("logon failure"))
                {
                    validation.AddInvalidReason(ex.Message);
                    throw validation;
                }
                throw ex;
            }

            return true;
        }

        public static void RegisterUserRoles(string userName, string[] roles)
        {
            userName = userName.ToUpper();
            RemoveUserRoles(userName);

            foreach (string role in roles)
            {
                if (!Roles.RoleExists(role))
                    Roles.CreateRole(role);
                Roles.AddUserToRole(userName, role);
            }
        }

        public static void RemoveUserRoles(string userName)
        {
            userName = userName.ToUpper();
            if (Roles.GetRolesForUser(userName.ToUpper()).Length > 0)
                Roles.RemoveUserFromRoles(userName.ToUpper(), Roles.GetRolesForUser(userName.ToUpper()));
        }

        public static FormsAuthenticationTicket CreateTicket(string userName, string[] roles, DateTime expiration, bool isPersistent )
        {
            userName = userName.ToUpper();

            //if (Roles.GetRolesForUser(userName.ToUpper()).Length > 0)
            //    Roles.RemoveUserFromRoles(userName.ToUpper(), Roles.GetRolesForUser(userName.ToUpper()));

            string roleString = string.Empty;
            foreach (string role in roles)
            {
                //if (!Roles.RoleExists(role))
                //    Roles.CreateRole(role);
                //Roles.AddUserToRole(userName, role);
                roleString += role + "|";
            }

            FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(1,
                                                userName,
                                                DateTime.Now,
                                                expiration,
                                                isPersistent,
                                                roleString);
            return authTicket;
        }


        public static HttpCookie CreateFormsAutheticationCookie(string userName, string[] roles, bool registerUserRoles) { return CreateFormsAutheticationCookie(userName, roles, registerUserRoles, DateTime.Now.AddMinutes(60), false); }
        public static HttpCookie CreateFormsAutheticationCookie(string userName, string[] roles, bool registerUserRoles, DateTime expiration, bool isPersistent)
        {
            if (registerUserRoles)
                RegisterUserRoles(userName, roles);
            FormsAuthenticationTicket authTicket = CreateTicket(userName, roles, expiration, isPersistent);
            return CreateFormsAutheticationCookie(authTicket); 
        }
        public static HttpCookie CreateFormsAutheticationCookie(FormsAuthenticationTicket authTicket)
        {
            string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
            HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
            //authCookie.Expires = authTicket.Expiration;
            //response.Cookies.Add(authCookie);

            FormsIdentity identity = new FormsIdentity(authTicket);
            GenericPrincipal principal = new GenericPrincipal(identity, Roles.GetRolesForUser(authTicket.Name));
            HttpContext.Current.User = principal;
            Thread.CurrentPrincipal = HttpContext.Current.User;
            return authCookie;
        }


        public static FormsAuthenticationTicket GetTicketFromCookie(HttpRequest request)
        {
            HttpCookie cookie = request.Cookies[FormsAuthentication.FormsCookieName];
            if (cookie == null)
                throw new Exception("Could not find forms authentication cookie '" + FormsAuthentication.FormsCookieName + "'");

            return FormsAuthentication.Decrypt(cookie.Value);
        }

        public static bool IsAuthenticated(HttpRequest request)
        {
            HttpCookie cookie = request.Cookies[FormsAuthentication.FormsCookieName];
            if (cookie == null)
                return false;
            FormsAuthenticationTicket decryptedTicket = FormsAuthentication.Decrypt(cookie.Value);
            return !decryptedTicket.Expired;
        }


        /// <summary>
        /// Retuns null if no cookie found to renew
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static HttpCookie RenewFormsAuthenticationCookie(HttpRequest request)
        {
            HttpCookie cookie = request.Cookies[FormsAuthentication.FormsCookieName];
            if (cookie == null)
                return null;
            FormsAuthenticationTicket decryptedTicket = FormsAuthentication.Decrypt(cookie.Value);
            FormsAuthenticationTicket renewedTicket = FormsAuthentication.RenewTicketIfOld(decryptedTicket);
            return CreateFormsAutheticationCookie(renewedTicket);
        }



    }
}