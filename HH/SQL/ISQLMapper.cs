﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace HH.SQL
{
    /// <summary>
    /// Use this if any custom mapping in a DAL class needs to take place
    /// </summary>
    public interface ISQLMapper
    {
        object ConvertPropertyToSQLValue(object parentObject, string propertyName);
        void ConvertSQLValueToProperty(object parentObject, string propertyName, object sqlVal);
    }
}
