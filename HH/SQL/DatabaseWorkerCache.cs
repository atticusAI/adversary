﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq.Mapping;
using System.Data.SqlClient;
using Microsoft.SqlServer.Management.Common;
using SMO = Microsoft.SqlServer.Management.Smo;
using System.Reflection;
using HH.Utils;

namespace HH.SQL
{
    /// <summary>
    /// Used for caching SQL server metadata discovered by SMO and data discovered during reflection on the DAL
    /// </summary>
    internal class DatabaseWorkerCache
    {
        private static object _lock = new object();
        private static Dictionary<string, DatabaseWorkerCache> _cache = new Dictionary<string, DatabaseWorkerCache>();

        private Dictionary<string, Dictionary<string, string>> _spCache = new Dictionary<string, Dictionary<string, string>>();
        private Dictionary<Type, Dictionary<string, AssociationAttribute>> _objEntityCache = new Dictionary<Type, Dictionary<string, AssociationAttribute>>();
        private Dictionary<Type, Dictionary<string, string>> _objPropertyCache = new Dictionary<Type, Dictionary<string, string>>();


        protected internal static DatabaseWorkerCache GetInstance(string key)
        {
            if (!_cache.ContainsKey(key))
            {
                lock (_lock)
                {
                    if (!_cache.ContainsKey(key))
                        _cache.Add(key, new DatabaseWorkerCache());
                }
            }
            return _cache[key];
        }

        public bool ContainsSP(string cacheKey)
        {
            return _spCache.ContainsKey(cacheKey);
        }

        public bool ContainsType(Type type)
        {
            return _objEntityCache.ContainsKey(type) && _objPropertyCache.ContainsKey(type);
        }

        public Dictionary<string, string> GetSPMappings(string cacheKey)
        {
            return _spCache[cacheKey];
        }

        public Dictionary<string, AssociationAttribute> GetEntityList(Type type)
        {
            return _objEntityCache[type];
        }

        public Dictionary<string, string> GetPropertyList(Type type)
        {
            return _objPropertyCache[type];
        }


        #region CACHE METHODS

        /// <summary>
        /// Caches the parameters in a stored procedure
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="procName"></param>
        public void CacheSP(string cacheKey, string procName, object obj, SqlConnection conn)
        {
            lock (_lock)
            {
                if (ContainsSP(cacheKey))
                    return;

                //SqlConnection conn = GetConnection();
                ServerConnection sc = new ServerConnection(conn);
                SMO.Server server = new SMO.Server(sc);
                SMO.Database db = server.Databases[conn.Database];
                SMO.StoredProcedure sp = db.StoredProcedures[procName];

                if (sp == null)
                    throw new Exception("Stored procedure " + procName + " does not exist");

                Dictionary<string, string> paramMappings = new Dictionary<string, string>();
                foreach (SMO.StoredProcedureParameter p in sp.Parameters)
                {
                    string paramName = p.Name;
                    if (paramName.StartsWith("@")) //strip the leading @
                        paramName = paramName.Substring(1);

                    if (obj == null)
                    {
                        paramMappings.Add(paramName, paramName);
                        continue;
                    }

                    PropertyInfo info = obj.GetType().GetProperty(paramName);
                    if (info != null)
                    {
                        paramMappings.Add(info.Name, paramName);
                        continue;
                    }

                    //if the property was not found, search the object to see if one of the properties has a different name then it's DB column
                    foreach (PropertyInfo i in obj.GetType().GetProperties())
                    {
                        ColumnAttribute c = ReflectionUtils.GetAttribute<ColumnAttribute>(i);
                        if (c != null && c.Name == paramName)
                        {
                            paramMappings.Add(i.Name, paramName);
                            break;
                        }
                    }
                }

                _spCache.Add(cacheKey, paramMappings);
                sc.Cancel();
            }
        }

        /// <summary>
        /// Caches reflection data
        /// </summary>
        /// <param name="type"></param>
        public void CacheType(Type type)
        {
            lock (_lock)
            {
                if (ContainsType(type))
                    return;

                Dictionary<string, AssociationAttribute> entityList = new Dictionary<string, AssociationAttribute>();
                Dictionary<string, string> propList = new Dictionary<string, string>();

                foreach (PropertyInfo info in type.GetProperties())
                {
                    ColumnAttribute col = ReflectionUtils.GetAttribute<ColumnAttribute>(info);
                    if (col != null)
                    {
                        string colName;
                        if (!string.IsNullOrEmpty(col.Name))
                            colName = col.Name;
                        else
                            colName = info.Name;
                        propList.Add(info.Name, colName);
                        continue;
                    }

                    AssociationAttribute assoc = ReflectionUtils.GetAttribute<AssociationAttribute>(info);
                    if (assoc != null &&
                        !string.IsNullOrEmpty(assoc.OtherKey))
                        entityList.Add(info.Name, assoc);
                }

                if (!_objEntityCache.ContainsKey(type))
                    _objEntityCache.Add(type, entityList);
                if (!_objPropertyCache.ContainsKey(type))
                    _objPropertyCache.Add(type, propList);
            }
        }
        #endregion

    }

}
