﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.Linq.Mapping;
using SMO = Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;
using System.Data;
using System.Reflection;
using HH.Utils;
using System.Collections;
using System.Linq;

namespace HH.SQL
{
    /// <summary>
    /// Stored procedure type for the worker to call
    /// </summary>
    public enum SPType
    {
        Get,
        Set,
        Search,
        Delete,
        //A Reimer added 12/01/2016 for Associated Memo project
        Copy
    }

    /// <summary>
    /// ORM tool for interfacing between DAL and DB
    /// </summary>
    public class DatabaseWorker
    {
        
       
        private DatabaseWorker() { }


        /// <summary>
        /// ORM tool for interfacing between DAL and DB
        /// </summary>
        /// <param name="conn"></param>
        public DatabaseWorker(SqlConnection conn)
        {
            _conn = conn;
        }

        private SqlConnection _conn;
        public SqlConnection Connection
        {
            get
            {
                return _conn;
            }
        }

        #region MAIN WORKER ENTRY POINT METHODS
        
        /// <summary>
        /// Retrieve a single DAL object of type T via an SP Get
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="lookupCriteria">Set the primary key on the lookup object and pass to the stored proc</param>
        /// <returns></returns>
        public T RetrieveSingle<T>(T lookupCriteria) { return RetrieveSingle<T>(lookupCriteria, GetDefaultStoredProcNameFromType<T>(SPType.Get)); }
        /// <summary>
        /// Retrieve a single DAL object of type T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="lookupCriteria">Set property values on the lookup object to pass to the stored proc</param>
        /// <param name="storedProcName">Override the default stored proc</param>
        /// <returns></returns>
        public T RetrieveSingle<T>(T lookupCriteria, string storedProcName)
        {
            SqlConnection conn = _conn;
            T single = RetrieveSingle<T>(lookupCriteria, storedProcName, conn);
            
            return single;
        }
        /// <summary>
        /// Retrieve a single DAL object of type T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="lookupCriteria">Set property values on the lookup object to pass to the stored proc</param>
        /// <param name="storedProcName">Override the default stored proc that gets called</param>
        /// <param name="conn">Pass a local connection</param>
        /// <returns></returns>
        public T RetrieveSingle<T>(T lookupCriteria, string storedProcName, SqlConnection conn)
        {
            SqlCommand cmd = BuildStoredProcedure(lookupCriteria, storedProcName, conn);
            DataTable dt = ExecuteCommand(cmd);
            if (dt.Rows.Count == 0)
                return default(T);
            //if (dt.Rows.Count > 1)
            //    throw new Exception("More than one record was retrieved with the given criteria");
            Populate(lookupCriteria, dt.Rows[(dt.Rows.Count-1)], storedProcName, conn);
            return lookupCriteria;
        }

        /// <summary>
        /// Retrieve a single DAL object of type T based on the PK ID (keyId) specfieid
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="keyId">The PK ID</param>
        /// <param name="storedProcName">Override the default stored proc that gets called</param>
        /// <returns></returns>
        public T RetrieveSingle<T>(int keyId, string storedProcName)
        {
            SqlConnection conn = _conn;
            T single = RetrieveSingle<T>(keyId, storedProcName, conn);
            
            return single;
        }
        /// <summary>
        /// Retrieve a single DAL object of type T based on the PK ID (keyId) specfieid
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="keyId">The PK ID</param>
        /// <param name="storedProcName">Override the default stored proc that gets called</param>
        /// <param name="conn">Pass a local connection</param>
        /// <returns></returns>
        public T RetrieveSingle<T>(int keyId, string storedProcName, SqlConnection conn)
        {
            object obj = ReflectionUtils.CreateNewInstanceOfType(typeof(T));
            ReflectionUtils.SetEntityKey(ref obj, keyId);
            SqlCommand cmd = BuildStoredProcedure(obj, storedProcName, conn);
            DataTable dt = ExecuteCommand(cmd);
            Populate(obj, dt.Rows[(dt.Rows.Count - 1)], storedProcName, conn);
            return (T)obj;
        }

        /// <summary>
        /// Calls a standard ADO.Net DataTable fill based on records returned from the storedProc specified
        /// </summary>
        /// <param name="storedProcName"></param>
        /// <returns></returns>
        public DataTable RetrieveToDataTable(string storedProcName) { return RetrieveToDataTable(storedProcName, null); }
        /// <summary>
        /// Calls a standard ADO.Net DataTable fill based on records returned from the storedProc specified
        /// </summary>
        /// <param name="storedProcName"></param>
        /// <param name="values">Any values that need to be passed to the stored proc</param>
        /// <returns></returns>
        public DataTable RetrieveToDataTable(string storedProcName, SqlParameter[] values)
        {
            SqlConnection conn = _conn;
            DataTable dt = RetrieveToDataTable(storedProcName, values, conn);
            
            return dt;
        }
        /// <summary>
        /// Calls a standard ADO.Net DataTable fill based on records returned from the storedProc specified
        /// </summary>
        /// <param name="storedProcName"></param>
        /// <param name="values">Any values that need to be passed to the stored proc</param>
        /// <param name="conn"></param>
        /// <returns></returns>
        public DataTable RetrieveToDataTable(string storedProcName, SqlParameter[] values, SqlConnection conn)
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();
            SqlCommand cmd = new SqlCommand(storedProcName);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = conn;
            if (values != null)
                cmd.Parameters.AddRange(values);
            DataTable dt = ExecuteCommand(cmd);
            return dt;
        }

        /// <summary>
        /// Creates a standard ADO.Net SqlDataReader  based on records returned from the storedProc specified
        /// </summary>
        /// <param name="storedProcName"></param>
        /// <returns></returns>
        public SqlDataReader RetrieveToSqlDataReader(string storedProcName) { return RetrieveToSqlDataReader(storedProcName, null); }
        /// <summary>
        /// Creates a standard ADO.Net SqlDataReader based on records returned from the storedProc specified
        /// </summary>
        /// <param name="storedProcName"></param>
        /// <param name="values">Any values that need to be passed to the stored proc</param>
        /// <returns></returns>
        public SqlDataReader RetrieveToSqlDataReader(string storedProcName, SqlParameter[] values)
        {
            SqlConnection conn = _conn;
            SqlDataReader reader = RetrieveToSqlDataReader(storedProcName, values, conn);
            return reader;
        }
        /// <summary>
        /// Creates a standard ADO.Net SqlDataReader based on records returned from the storedProc specified
        /// </summary>
        /// <param name="storedProcName"></param>
        /// <param name="values">Any values that need to be passed to the stored proc</param>
        /// <param name="conn"></param>
        /// <returns></returns>
        public SqlDataReader RetrieveToSqlDataReader(string storedProcName, SqlParameter[] values, SqlConnection conn)
        {
            SqlCommand cmd = new SqlCommand(storedProcName);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = conn;
            if (conn.State != ConnectionState.Open)
                conn.Open();
            if (values != null)
                cmd.Parameters.AddRange(values);
            return cmd.ExecuteReader();
        }

        /// <summary>
        /// Returns every record from the Table associated to the generic DAL type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public List<T> RetrieveToList<T>() { return RetrieveToList<T>(GetDefaultStoredProcNameFromType<T>(SPType.Search), null); }
        /// <summary>
        /// Calls the SP specified and returns it to a List collection
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="storedProcName"></param>
        /// <returns></returns>
        public List<T> RetrieveToList<T>(string storedProcName) { return RetrieveToList<T>(storedProcName, null); }
        /// <summary>
        /// Calls the SP specified and returns it to a List collection of the generic DAL type specified
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="storedProcName"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public List<T> RetrieveToList<T>(string storedProcName, SqlParameter[] values)
        {
            SqlConnection conn = _conn;
            return RetrieveToList<T>(storedProcName, values, conn);
        }
        /// <summary>
        /// Calls the SP specified and returns it to a List collection of the generic DAL type specified
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="storedProcName"></param>
        /// <param name="values"></param>
        /// <param name="conn"></param>
        /// <returns></returns>
        public List<T> RetrieveToList<T>(string storedProcName, SqlParameter[] values, SqlConnection conn)
        {
            DataTable dt = RetrieveToDataTable(storedProcName, values, conn);
            return ConvertToList<T>(dt, storedProcName, conn);
        }

        /// <summary>
        /// Calls the default Set SP and commits the DAL object specified
        /// </summary>
        /// <param name="obj"></param>
        public void Commit(object obj) { Commit(obj, GetDefaultStoredProcName(obj.GetType(), SPType.Set)); }
        /// <summary>
        /// Commits the DAL object and uses the SP specified
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="storedProcName"></param>
        public void Commit(object obj, string storedProcName)
        {
            SqlConnection conn = _conn;
            Commit(obj, storedProcName, conn);
        }
        /// <summary>
        /// Commits the DAL object and uses the SP and connection specified
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="storedProcName"></param>
        /// <param name="conn"></param>
        public void Commit(object obj, string storedProcName, SqlConnection conn)
        {
            bool mustTransferProperties = false;
            string oldScrMemID = "";
            string enteredScrMemID = "";
            object objEnteredScrMemID;
            PropertyInfo propEnteredAssociatedID = null;
            if (obj.GetType().ToString() == "Memo")
            {
                if (obj.GetType().GetProperty("ScrMemType").GetValue(obj, null).ToString() == "6")
                {
                    propEnteredAssociatedID = obj.GetType().GetProperty("AssociatedScrMemID");
                    if (propEnteredAssociatedID != null)
                    {
                        objEnteredScrMemID = obj.GetType()
                            .GetProperty("AssociatedScrMemID")
                            .GetValue(obj, null);

                        if (objEnteredScrMemID != null)
                        {
                            enteredScrMemID = objEnteredScrMemID.ToString();
                        }
                        DataTable dtOld = ExecuteCommand(BuildStoredProcedure(obj, "PR_GET_MEMO"));
                        oldScrMemID = (dtOld.Rows.Count > 0 ? dtOld.Rows[0]["AssociatedScrMemID"].ToString() : "");
                    }
                }
                
            }
            


            SqlCommand cmd = BuildStoredProcedure(obj, storedProcName, conn);
            DataTable dt = ExecuteCommand(cmd);
            if (dt.Rows.Count > 1)
                throw new Exception("The database record was committed with errors. More than one record was retrieved with the given data '" + obj.GetType().Name + "'");
            if (dt.Rows.Count == 1)
            {
                /*addition 11/24/2016 for Rejected Matter Associated Memo change request*/
                //if the memo is a "rejected memo" type, and there is a reference made to the original memo,
                //populate the rejected memo's information with anything pertinent from the original memo

                //check to see if this is a Memo object, and then if there is an association
                if (obj.GetType().ToString() == "Memo")
                {

                    if (enteredScrMemID != "")
                    {
                        var propScrmemType = obj.GetType().GetProperty("ScrMemType");
                        var propTransferred = obj.GetType().GetProperty("AssociatedTransferred");
                        var propAssociatedID = obj.GetType().GetProperty("AssociatedScrMemID");
                        if (propAssociatedID.GetValue(obj, null) != null)
                        {
                            //oldScrMemID = obj.GetType().GetProperty("AssociatedScrMemID").GetValue(obj, null).ToString();
                            if (oldScrMemID != enteredScrMemID) propTransferred.SetValue(obj, false, null);
                        }

                        if (propTransferred != null && propAssociatedID != null && propScrmemType != null)
                        {
                            if (propScrmemType.GetValue(obj, null).ToString() == "6")
                            {
                                if (propTransferred.GetValue(obj, null) == null)
                                {
                                    mustTransferProperties = true;
                                }
                                else
                                {
                                    mustTransferProperties = !Convert.ToBoolean(propTransferred.GetValue(obj, null));
                                }

                                var newScrMemID = //(int)obj.GetType().GetProperty("ScrMemID").GetValue(obj, null);
                                dt.Rows[0]["ScrMemID"];

                                int _enteredScrMemID;

                                if (!Int32.TryParse(enteredScrMemID, out _enteredScrMemID)) throw new Exception("The associated screening memo was not entered correctly");

                                if (mustTransferProperties)
                                {
                                    List<SqlParameter> sqlParams = new List<SqlParameter>();
                                    sqlParams.Add(new SqlParameter("@CopyFromScrMemID", enteredScrMemID));
                                    sqlParams.Add(new SqlParameter("@CopyToScrMemID", newScrMemID));
                                    string sp = "pr_COPY_ASSOCIATED_MEMO";
                                    ExecuteNonQuery(sp, sqlParams.ToArray(), conn);
                                    propTransferred.SetValue(obj, true, null);
                                    //run the GET command again to refresh the local datatable object
                                    obj.GetType().GetProperty("ScrMemID").SetValue(obj, newScrMemID, null);
                                    List<SqlParameter> getParams = new List<SqlParameter>();
                                    SqlParameter getprmScrMemId = new SqlParameter();
                                    getprmScrMemId.Value = newScrMemID.ToString();
                                    getprmScrMemId.SqlDbType = SqlDbType.NVarChar;
                                    getprmScrMemId.ParameterName = "ScrMemID";
                                    SqlCommand getCmd = BuildStoredProcedure(obj, "pr_GET_Memo");
                                    dt = ExecuteCommand(getCmd);

                                }
                            }
                        }
                    }
                    
                }
                

                
                Populate(obj, dt.Rows[0], storedProcName, conn);
            }
            
        }

        /// <summary>
        /// Calls the default search SP and passes any non-null property values in the searchCriteria to the SP, fills a DataTable with the returned records
        /// </summary>
        /// <param name="searchCriteria"></param>
        /// <returns></returns>
        public DataTable SearchToDataTable(object searchCriteria) 
        {
            SqlConnection conn = _conn;
            return SearchToDataTable(searchCriteria, conn);
        }
        /// <summary>
        /// Calls the default search SP and passes any non-null property values in the searchCriteria to the SP, fills a DataTable with the returned records
        /// </summary>
        /// <param name="searchCriteria"></param>
        /// <param name="conn">Override the default connection</param>
        /// <returns></returns>
        public DataTable SearchToDataTable(object searchCriteria, SqlConnection conn)
        {
            string storedProcName = GetDefaultStoredProcName(searchCriteria.GetType(), SPType.Search);
            SqlCommand cmd = BuildStoredProcedure(searchCriteria, storedProcName, conn);
            return ExecuteCommand(cmd);            
        }

        /// <summary>
        /// Calls the default search SP and passes any non-null property values in the searchCriteria to the SP, returns records found as a SqlDataReader
        /// </summary>
        /// <param name="searchCriteria">passes any non-null property values in the searchCriteria to the SP</param>
        /// <returns></returns>
        public SqlDataReader SearchToSqlDataReader(object searchCriteria)
        {
            return SearchToSqlDataReader(searchCriteria, _conn);
        }
        /// <summary>
        /// Calls the default search SP and passes any non-null property values in the searchCriteria to the SP, returns records found as a SqlDataReader
        /// </summary>
        /// <param name="searchCriteria">passes any non-null property values in the searchCriteria to the SP</param>
        /// <returns></returns>
        public SqlDataReader SearchToSqlDataReader(object searchCriteria, SqlConnection conn)
        {
            string storedProcName = GetDefaultStoredProcName(searchCriteria.GetType(), SPType.Search);
            SqlCommand cmd = BuildStoredProcedure(searchCriteria, storedProcName, conn);
            return cmd.ExecuteReader();
        }

        /// <summary>
        /// Calls the default search SP and passes any non-null property values in the searchCriteria to the SP, returns records found as a List
        /// </summary>
        /// <param name="searchCriteria">passes any non-null property values in the searchCriteria to the SP</param>
        /// <returns></returns>
        public List<T> SearchToList<T>(T searchCriteria) 
        { 
            SqlConnection conn = _conn;
            return SearchToList<T>(searchCriteria, _conn); 
        }
        /// <summary>
        /// Calls the default search SP and passes any non-null property values in the searchCriteria to the SP, returns records found as a List
        /// </summary>
        /// <param name="searchCriteria">passes any non-null property values in the searchCriteria to the SP</param>
        /// <returns></returns>
        public List<T> SearchToList<T>(T searchCriteria, SqlConnection conn)
        {
            string storedProcName = GetDefaultStoredProcNameFromType<T>(SPType.Search);
            DataTable dt = SearchToDataTable(searchCriteria, conn);
            return ConvertToList<T>(dt, storedProcName, conn);
        }

        /// <summary>
        /// Calls the default delete stored proc and deletes the database record for the DAL entity passed
        /// </summary>
        /// <param name="objToDelete"></param>
        public void Delete(object objToDelete) { Delete(objToDelete, _conn); }
        /// <summary>
        /// Calls the default delete stored proc and deletes the database record for the DAL entity passed
        /// </summary>
        /// <param name="objToDelete"></param>
        /// <param name="conn"></param>
        public void Delete(object objToDelete, SqlConnection conn)
        {
            string storedProcName = GetDefaultStoredProcName(objToDelete.GetType(), SPType.Delete);
            SqlCommand cmd = BuildStoredProcedure(objToDelete, storedProcName, conn);
            cmd.ExecuteNonQuery();
        }

        /// <summary>
        /// Calls a standard ExecuteNonQuery() command
        /// </summary>
        /// <param name="storedProcName"></param>
        /// <returns></returns>
        public int ExecuteNonQuery(string storedProcName) { return ExecuteNonQuery(storedProcName, null); }
        /// <summary>
        /// Calls a standard ExecuteNonQuery() command
        /// </summary>
        /// <param name="storedProcName"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public int ExecuteNonQuery(string storedProcName, SqlParameter[] values)
        {
            SqlConnection conn = _conn;
            return ExecuteNonQuery(storedProcName, values, conn);
        }
        /// <summary>
        /// Calls a standard ExecuteNonQuery() command
        /// </summary>
        /// <param name="storedProcName"></param>
        /// <param name="values"></param>
        /// <param name="conn"></param>
        /// <returns></returns>
        public int ExecuteNonQuery(string storedProcName, SqlParameter[] values, SqlConnection conn)
        {
            SqlCommand cmd = new SqlCommand(storedProcName);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = conn;
            if (conn.State != ConnectionState.Open)
                conn.Open();
            if (values != null)
                cmd.Parameters.AddRange(values);
            return cmd.ExecuteNonQuery();
        }
        #endregion

        /// <summary>
        /// Builds a SqlCommand object based on the DAL object passed and the metadata discovered from the default stored proc type specified
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="spType"></param>
        /// <returns></returns>
        public SqlCommand BuildStoredProcedure(object obj, SPType spType)
        {
            string procName = GetDefaultStoredProcName(obj.GetType(), spType);
            return BuildStoredProcedure(obj, procName, _conn);
        }
        /// <summary>
        /// Builds a SqlCommand object based on the DAL object passed and the metadata discovered from the stored proc specified
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="procName"></param>
        /// <returns></returns>
        public SqlCommand BuildStoredProcedure(object obj, string procName) { return BuildStoredProcedure(obj, procName, _conn); }
        /// <summary>
        /// Builds a SqlCommand object based on the DAL object passed and the metadata discovered from the stored proc specified
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="procName"></param>
        /// <param name="conn"></param>
        /// <returns></returns>
        public SqlCommand BuildStoredProcedure(object obj, string procName, SqlConnection conn)
        {
            if (obj == null)
                throw new ArgumentException("Could not build stored procedure " + procName  + " , object was null");

            if (conn.State != ConnectionState.Open)
                conn.Open();

            string cacheKey = (procName + "-" + obj.GetType().Name).ToUpper();
            DatabaseWorkerCache cache = DatabaseWorkerCache.GetInstance(conn.Database);
            if (!cache.ContainsSP(cacheKey))
                cache.CacheSP(cacheKey, procName, obj, conn);

            SqlCommand cmd = new SqlCommand(procName);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = conn;
            Dictionary<string, string> paramMappings = cache.GetSPMappings(cacheKey);

            foreach (string propName in paramMappings.Keys)
            {
                string paramName = paramMappings[propName];
                object value;
                if (obj is ISQLMapper)
                {
                    value = ((ISQLMapper)obj).ConvertPropertyToSQLValue(obj, propName);
                    if (value == null)
                        value = DBNull.Value;
                }
                else
                {
                    PropertyInfo info = obj.GetType().GetProperty(propName);
                    if (info == null)
                        value = DBNull.Value;
                    else
                    {
                        SQLDefaultMapper mapper = new SQLDefaultMapper();
                        value = mapper.ConvertPropertyToSQLValue(obj, propName);
                    }
                }
                cmd.Parameters.AddWithValue(paramName, value);
            }

            return cmd;
        }

        /// <summary>
        /// Standard DataTable fill pattern
        /// </summary>
        /// <param name="cmd"></param>
        /// <returns></returns>
        public DataTable ExecuteCommand(SqlCommand cmd)
        {
            DataTable dt = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            return dt;
        }

        /// <summary>
        /// Where most of the SMO work is done
        /// </summary>
        /// <param name="obj">The DAL object to populate</param>
        /// <param name="dr">The DataRow to use to populate obj</param>
        /// <param name="storedProcName">The stored proc to call</param>
        /// <param name="conn"></param>
        private void Populate(object obj, DataRow dr, string storedProcName, SqlConnection conn)
        {
            Type type = obj.GetType();
            
            //cache all properties and entities for the type
            DatabaseWorkerCache cache = DatabaseWorkerCache.GetInstance(conn.Database);
            if (!cache.ContainsType(type))
                cache.CacheType(type);

            Dictionary<string, string> propList = cache.GetPropertyList(type);
            Dictionary<string, AssociationAttribute> entityList = cache.GetEntityList(type);
                        
            //set each property with the correct value
            foreach (KeyValuePair<string, string> item in propList)
            {
                string propName = item.Key;
                string colName = item.Value;
                object val = dr[colName];

                ISQLMapper mapper = (obj is ISQLMapper) ? (ISQLMapper)obj : new SQLDefaultMapper();
                mapper.ConvertSQLValueToProperty(obj, propName, val);
            }






            //loop through each entity and set as necessary
            foreach (KeyValuePair<string, AssociationAttribute> item in entityList)
            {
                string entityName = item.Key;                                                                                //get the entity name
                AssociationAttribute assoc = item.Value;                                                                     //get the association
                PropertyInfo entityInfo = type.GetProperty(entityName);                                                      //get the entity property info

                object keyValue = obj.GetType().GetProperty(assoc.OtherKey).GetValue(obj, null);                             //get the PK/FK value from the parent object
                string keyName = assoc.OtherKey;                                                                             //get the property name for the PK/FK from the parent object

                object newInstance = ReflectionUtils.CreateNewInstanceOfType(entityInfo.PropertyType);                               //create a new instance of the entity, this is the search criteria for use in DB retrieval
                PropertyInfo keyInfo = newInstance.GetType().GetProperty(keyName);                                           //get the PK/FK property info for the entity
                keyInfo.SetValue(newInstance, keyValue, null);                                                               //set the PK/FK property with the parent's value

                bool isList = (entityInfo.GetValue(obj, null) is IList);                                                     //determine whether the parent property is a list
                MethodInfo m = (isList) ? GetType().GetMethod("InvokeSearch", BindingFlags.NonPublic | BindingFlags.Instance) :
                                          GetType().GetMethod("InvokeRetrieveSingle", BindingFlags.NonPublic | BindingFlags.Instance);  
                MethodInfo method = m.MakeGenericMethod(new Type[] { newInstance.GetType() });                               //Create an instance of the DB method to call.

                if (isList)
                {
                    IList retrievedValues = (IList)method.Invoke(this, new object[] { newInstance, conn });                   //invoke the method and get the list retrieved by the DB search
                    IList entitySet = (IList)entityInfo.GetValue(obj, null);                                                  //property is a list so cast it
                    entitySet.Clear();
                    foreach (object retrievedValue in retrievedValues)                                                        //add retrieved values
                        entitySet.Add(retrievedValue);
                }
                else
                {
                    object retrievedValue = method.Invoke(this, new object[] { newInstance, conn });                          //invoke the method and get the value retrieved by the DB retrieval
                    if (retrievedValue != null)
                        entityInfo.SetValue(obj, retrievedValue, null);                                                       //set it and forget it (tm)
                }
            }
        }

        /// <summary>
        /// Converts a DataTable to the list specified by type T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dt"></param>
        /// <param name="storedProcName"></param>
        /// <param name="conn"></param>
        /// <returns></returns>
        private List<T> ConvertToList<T>(DataTable dt, string storedProcName, SqlConnection conn)
        {
            List<T> list = new List<T>();
            foreach (DataRow dr in dt.Rows)
            {
                T instance = (T)ReflectionUtils.CreateNewInstanceOfType(typeof(T));
                Populate(instance, dr, storedProcName, conn);
                list.Add(instance);
            }
            return list;
        }

        
        #region INOVKE METHODS
        /// <summary>
        /// Issues with calling overriden methods with reflection. Call this method to search via reflection within the worker
        /// </summary>
        private List<T> InvokeSearch<T>(T searchCriteria, SqlConnection conn)
        {
             return SearchToList<T>(searchCriteria, conn);
        }

        /// <summary>
        /// Issues with calling overriden methods with reflection. Call this method to search via reflection within the worker
        /// </summary>
        private T InvokeRetrieveSingle<T>(T searchCriteria, SqlConnection conn)
        {
            string storedProcName = GetDefaultStoredProcNameFromType<T>(SPType.Get);
            return RetrieveSingle<T>(searchCriteria, storedProcName, conn);
        }
        #endregion

        #region UTIL Methods
        /// <summary>
        /// Gets the default stored proc's name based on the generic and SPtype specified
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="spType"></param>
        /// <returns></returns>
        private static string GetDefaultStoredProcNameFromType<T>(SPType spType) { return GetDefaultStoredProcName(typeof(T), spType); }
        /// <summary>
        /// Gets the default stored proc's name based on the DAL type and SPtype specified
        /// </summary>
        /// <param name="type"></param>
        /// <param name="spType"></param>
        /// <returns></returns>
        public static string GetDefaultStoredProcName(Type type, SPType spType) 
        {
            TableAttribute t = ReflectionUtils.GetAttribute<TableAttribute>((Attribute[])type.GetCustomAttributes(typeof(TableAttribute), false));
            if (t == null)
                throw new Exception("Could not get the default " + spType + " stored procedure name for type " + type.Name + ". No TableAttribute was found.");

            string procName = "PR_" + spType.ToString().ToUpper() + "_" + t.Name;
            procName = procName.ToUpper().Replace("[DBO].", "");
            procName = procName.ToUpper().Replace("]", "");
            return procName;
        }
        #endregion
    }
}
