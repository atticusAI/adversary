﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Web.UI.WebControls;

using SMO = Microsoft.SqlServer.Management.Smo;


namespace HH.SQL.Tools
{
    /// <summary>
    /// Uses SMO to creates DAL class files based on metadata discovered in a SQL database
    /// </summary>
    public static class DataObjectBuilder
    {
        private static string _dbName = string.Empty;
        private static string _tableName = string.Empty;
        private static string _className = string.Empty;
        private static bool _addScreenBuilderAttributes = false;
        private static bool _verboseSummary = false;
        private static bool _appendToExisting = false;
        private static string _existingTxt = string.Empty;
        private static List<string> _constructorTxt = new List<string>();
        private static List<string> _bodyTxt = new List<string>();

        private static Microsoft.SqlServer.Management.Common.ServerConnection _conn = null;
        private static SMO.Database _db = null;
        private static SMO.Database _Database
        {
            get
            {
                if (_db == null)
                {
                    SMO.Server server = new SMO.Server(_conn);
                    _db = server.Databases[_dbName];
                }
                return _db;
            }
        }

        /// <summary>
        /// Reads a database and creates DAL files based on the DB metadata
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="directoryPath"></param>
        /// <param name="dbName"></param>
        /// <param name="addScreenBuilderAttributes">True to add ScreenBuilder attributes to all properties</param>
        /// <param name="verboseSummary">Adds extra info in property summary tags</param>
        /// <param name="appendToExisting">Append to the file if it already exists (warning, setting to true is a bit buggy)</param>
        public static void BuildAllClassFiles(Microsoft.SqlServer.Management.Common.ServerConnection conn, string directoryPath, string dbName, bool addScreenBuilderAttributes, bool verboseSummary, bool appendToExisting)
        {
            if (!Directory.Exists(directoryPath))
                throw new ArgumentException("The directory " + directoryPath + " does not exist");
            
            _conn = conn;
            _dbName = dbName.Replace(" ", "");
            foreach (SMO.Table table in _Database.Tables)
            {
                string className = table.Name;
                string filePath = @directoryPath + className + ".cs";
                BuildClassFile(conn, className, filePath, _dbName, className, addScreenBuilderAttributes, verboseSummary, appendToExisting);
            }
        }

        /// <summary>
        /// Reads a database and creates DAL files based on the DB metadata
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="filePath"></param>
        /// <param name="dbName"></param>
        /// <param name="tableName"></param>
        /// <param name="addScreenBuilderAttributes">True to add ScreenBuilder attributes to all properties</param>
        /// <param name="verboseSummary">Adds extra info in property summary tags</param>
        /// <param name="appendToExisting">Append to the file if it already exists (warning, setting to true is a bit buggy)</param>
        public static void BuildClassFile(Microsoft.SqlServer.Management.Common.ServerConnection conn, string filePath, string dbName, string tableName, bool addScreenBuilderAttributes, bool verboseSummary, bool appendToExisting)
        {
            if (string.IsNullOrEmpty(tableName))
                throw new ArgumentException("Table name not specified");
            string className = filePath.Trim().Substring(filePath.LastIndexOf('\\')+1);
            if (className.EndsWith(".cs", true, null))
                className = className.Remove(className.IndexOf("."));
            className = className.Replace(" ", "");
            BuildClassFile(conn, className, filePath, dbName, tableName, addScreenBuilderAttributes, verboseSummary, appendToExisting);
        }
        public static void BuildClassFile(Microsoft.SqlServer.Management.Common.ServerConnection conn, string className, string filePath, string dbName, string tableName, bool addScreenBuilderAttributes, bool verboseSummary, bool appendToExisting)
        {
            _appendToExisting = appendToExisting && File.Exists(filePath);
            string text = string.Empty;
            if (_appendToExisting)
            {
                TextReader reader = File.OpenText(filePath);
                text = reader.ReadToEnd();
                _existingTxt = text.ToLower().Trim();
                reader.Close();
                string appendText = BuildClassText(conn, className, dbName, tableName, addScreenBuilderAttributes, verboseSummary);
                if (appendText.Trim().Length == 0)
                    return;
                text = text.Remove(text.LastIndexOf('}'),1);
                text += "\r\n";
                text += appendText + "\r\n";
                text += "}";
            }
            else
            {
                text = BuildClassText(conn, className, dbName, tableName, addScreenBuilderAttributes, verboseSummary);
            }
            TextWriter writer = new StreamWriter(filePath);
            writer.Write(text);
            writer.Close();
        }

        /// <summary>
        /// Returns the text of a DAL class file
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="className"></param>
        /// <param name="dbName"></param>
        /// <param name="tableName"></param>
        /// <param name="addScreenBuilderAttributes"></param>
        /// <param name="verboseSummary"></param>
        /// <returns></returns>
        public static string BuildClassText(Microsoft.SqlServer.Management.Common.ServerConnection conn, string className, string dbName, string tableName, bool addScreenBuilderAttributes, bool verboseSummary)
        {
            _conn = conn;
            _dbName = dbName.Replace(" ","");
            _tableName = tableName.Replace(" ","");
            _addScreenBuilderAttributes = addScreenBuilderAttributes;
            _verboseSummary = verboseSummary;
            _className = className.Replace(" ", "");
            _constructorTxt.Clear();
            _bodyTxt.Clear();

            if (!_Database.Tables.Contains(tableName))
                throw new Exception("Could not find table " + tableName);
            SMO.Table table = _Database.Tables[tableName];
            StringBuilder sb = new StringBuilder();

            if (!_appendToExisting)
            {
                sb.AppendLine();
                sb.AppendLine("using System;");
                sb.AppendLine("using System.Data;");
                sb.AppendLine("using System.Data.Linq;");
                sb.AppendLine("using System.Configuration;");
                sb.AppendLine("using System.Linq;");
                sb.AppendLine("using System.Data.Linq.Mapping;");
                sb.AppendLine("using System.Web;");
                sb.AppendLine("using System.Web.Security;");
                sb.AppendLine("using System.Web.UI;");
                sb.AppendLine("using System.Web.UI.HtmlControls;");
                sb.AppendLine("using System.Web.UI.WebControls;");
                sb.AppendLine("using System.Web.UI.WebControls.WebParts;");
                sb.AppendLine("using System.Xml.Linq;");
                if (_addScreenBuilderAttributes)
                    sb.AppendLine("using HHControls.Screen;");

                sb.AppendLine("/// <summary>");
                sb.AppendLine("/// Summary description for ScreeningMemo");
                sb.AppendLine("/// This class file was created using the HH DataObjectBuilder");
                sb.AppendLine("/// </summary>");
                sb.AppendLine("[Table(Name=\"" + table.Name + "\")]");
                sb.AppendLine("public class " + _className);
                sb.AppendLine("{");
                sb.AppendLine();
            }
            
            foreach (SMO.Column col in table.Columns)
            {
                BuildProperty(ref sb, col);
            }

            if (!_appendToExisting)
            {
                sb.AppendLine();
                sb.AppendLine();
                if (_constructorTxt.Count == 0)
                {
                    sb.AppendLine("\tpublic " + _className + "() { }");
                }
                else
                {
                    sb.AppendLine("\tpublic " + _className + "()");
                    sb.AppendLine("\t{");
                    foreach (string txt in _constructorTxt)
                    {
                        sb.AppendLine("\t" + txt);
                    }
                    sb.AppendLine("\t}");
                }
                sb.AppendLine();
                foreach (string txt in _bodyTxt)
                {
                    sb.AppendLine(txt);
                    sb.AppendLine();
                }
                sb.AppendLine("}");
            }
            return sb.ToString();
        }

        private static void BuildProperty(ref StringBuilder sb, SMO.Column col)
        {
            string colName = col.Name.Replace(" ", "");
            string dataType = col.DataType.ToString().ToLower();
            string controlType = "Auto";
            string paramType = "string";
            bool isNullable = false;
            
            if (dataType.Contains("char") ||
                dataType.Contains("time") ||
                dataType.Contains("text"))
            {
                if (col.DataType.MaximumLength > 50)
                    controlType = "TextArea";
                else
                    controlType = "TextBox";

                if (dataType.Contains("time"))
                {
                    paramType = "DateTime";
                    isNullable = col.Nullable;
                }
                
            }
            else if (dataType.Contains("int"))
            {
                if (dataType.ToLower() == "tinyint")
                {
                    paramType = "byte";
                    isNullable = col.Nullable;
                }
                else
                {
                    paramType = "int";
                    isNullable = col.Nullable;
                }
            }
            else if (dataType.Contains("decimal"))
            {
                paramType = "decimal";
                isNullable = col.Nullable;
            }
            else if (dataType.Contains("float"))
            {
                paramType = "float";
                isNullable = col.Nullable;
            }
            else if (dataType.Contains("real"))
            {
                paramType = "float";
                isNullable = col.Nullable;
            }
            else if (dataType.Contains("money"))
            {
                paramType = "decimal";
            }
            else if (dataType.Contains("numeric"))
            {
                paramType = "decimal";
                isNullable = col.Nullable;
            }
            else if (dataType.Contains("bit"))
            {
                controlType = "BooleanRadioControl";
                paramType = "bool";
                isNullable = col.Nullable;
            }
            else if (dataType.Contains("binary"))
            {
                controlType = "None";
                paramType = "byte[]";
                isNullable = false;
            }
            else
            {
                paramType = "object";
            }

            string paramTxt = "public " + paramType;
            if (isNullable)
                paramTxt += "?";

            bool addMapping = false;
            if (colName == _className)
            {
                colName += "Value";
                addMapping = true;
            }
            paramTxt += " " + colName + " { get; set; }";
            
            if (_appendToExisting && _existingTxt.Contains(paramTxt.ToLower()))
                return;

            sb.AppendLine("\t/// <summary>");
            sb.AppendLine("\t/// DB Column: [" + _dbName + "].[" + _tableName + "].[" + col.Name + "] (" + col.DataType.SqlDataType + ")");
            if (_verboseSummary)
            {
                sb.AppendLine("\t/// Nullable: " + col.Nullable);
                sb.AppendLine("\t/// In Primary Key: " + col.InPrimaryKey);
                sb.AppendLine("\t/// Is Foreign Key: " + col.IsForeignKey);
                sb.AppendLine("\t/// Identity: " + col.Identity);
                //if (_addScreenBuilderAttributes)
                //    sb.AppendLine("\t/// Screen Builder Control Type: " + controlType);
            }
            sb.AppendLine("\t/// </summary>");
            if (_addScreenBuilderAttributes)
            {
                sb.AppendLine("\t[ScreenBuilderStandardControl(");
                sb.AppendLine("\t Caption = \"" + colName + "\",");
                sb.AppendLine("\t ControlType = ScreenBuilderStandardControlType." + controlType + ",");
                sb.AppendLine("\t ScreenIDs = new string[] { })]");
            }
            sb.Append("\t[Column(Storage=\"" + colName + "\", DbType=\"" + col.DataType.SqlDataType);
            if (col.DataType.MaximumLength > 1)
                sb.Append("(" + col.DataType.MaximumLength + ")");
            if (!col.Nullable)
                sb.Append(" NOT NULL");
            if (col.Identity)
                sb.Append(" IDENTITY");
            sb.Append("\"");
            if (addMapping)
                sb.Append(", Name=\"" + col.Name + "\"");
            if (col.IdentityIncrement > 0)
                sb.Append(", IsDbGenerated=true");
            if (col.Identity)
                sb.Append(", AutoSync=AutoSync.Always");
            if (col.InPrimaryKey)
                sb.Append(", IsPrimaryKey=true");
            sb.AppendLine(")]");
            sb.AppendLine("\t" + paramTxt);
            sb.AppendLine();
            sb.AppendLine();
            if (col.InPrimaryKey)
                BuildPKAssociation(ref sb, col);
            if (col.IsForeignKey)
                BuildFKAssociation(ref sb, col);
        }

        private static void BuildPKAssociation(ref StringBuilder sb, Microsoft.SqlServer.Management.Smo.Column col)
        {
            foreach (SMO.Table table in _Database.Tables)
            {
                if (table.Name == _tableName)
                    continue;

                foreach (SMO.ForeignKey fk in table.ForeignKeys)
                {
                    if (fk.ReferencedTable.ToLower() != _tableName.ToLower())
                        continue;

                    foreach (SMO.ForeignKeyColumn fkc in fk.Columns)
                    {
                        if (fkc.Name.ToLower() != col.Name.ToLower())
                            continue;

                        string refClassName = table.Name.Replace(" ", "");
                        string paramName = refClassName;
                        if (paramName.EndsWith("s", true, System.Globalization.CultureInfo.CurrentCulture))
                            paramName += "es";
                        else
                            paramName += "s";
                        string varName = "_" + paramName.ToLower();
                        string ctorTxt = "this." + varName + " = new EntitySet<" + refClassName + ">(";
                        ctorTxt += "new Action<" + refClassName + ">(this.attach_" + paramName + "), ";
                        ctorTxt += "new Action<" + refClassName + ">(this.detach_" + paramName + "));";
                        _constructorTxt.Add(ctorTxt);
                        sb.AppendLine("\tprivate EntitySet<" + refClassName + "> " + varName + ";");
                        
                        sb.AppendLine("\t[Association(Name=\"" + fk.Name + "\", Storage=\"" + varName + "\", OtherKey=\"" + col.Name + "\")]");
                        sb.AppendLine("\tpublic EntitySet<" + refClassName + "> " + paramName);
                        sb.AppendLine("\t{");
                        sb.AppendLine("\t\tget");
                        sb.AppendLine("\t\t{");
                        sb.AppendLine("\t\t\treturn this." + varName + ";");
                        sb.AppendLine("\t\t}");
                        sb.AppendLine("\t\tset");
                        sb.AppendLine("\t\t{");
                        sb.AppendLine("\t\t\tthis." + varName + ".Assign(value);");
                        sb.AppendLine("\t\t}");
                        sb.AppendLine("\t}");
                        sb.AppendLine();
                        sb.AppendLine();

                        StringBuilder sbBody = new StringBuilder();
                        sbBody.AppendLine("\tprivate void attach_" + paramName + "(" + refClassName + " entity)");
                        sbBody.AppendLine("\t{");
                        //sbBody.AppendLine("\t\tthis.SendPropertyChanging();");
                        sbBody.AppendLine("\t\tentity." + _className + " = this;");
                        sbBody.AppendLine("\t}");
                        sbBody.AppendLine("\tprivate void detach_" + paramName + "(" + refClassName + " entity)");
                        sbBody.AppendLine("\t{");
                        //sbBody.AppendLine("\t\tthis.SendPropertyChanging();");
                        sbBody.AppendLine("\t\tentity." + _className + " = null;");
                        sbBody.AppendLine("\t}");
                        _bodyTxt.Add(sbBody.ToString());
                    }
                }
            }
        }

        private static void BuildFKAssociation(ref StringBuilder sb, Microsoft.SqlServer.Management.Smo.Column col)
        {
            SMO.Table table = _Database.Tables[_tableName];
            SMO.ForeignKey fk = null;
            foreach (SMO.ForeignKey key in table.ForeignKeys)
            {
                if (key.Columns.Contains(col.Name))
                {
                    fk = key;
                    break;
                }
            }
            if (fk == null)
                return;

            string refClassName = fk.ReferencedTable.Replace(" ", "");
            string varName = "_" + refClassName.ToLower();
            string paramName = _tableName;
            if (paramName.EndsWith("s", true, System.Globalization.CultureInfo.CurrentCulture))
                paramName += "es";
            else
                paramName += "s"; 
            sb.AppendLine("\tprivate EntityRef<" + refClassName + "> " + varName + " = default(EntityRef<" + refClassName + ">);");
            
            sb.Append("\t[Association(Name=\"" + fk.Name + "\"");
            sb.Append(", Storage=\"" + varName + "\"");
            sb.Append(", ThisKey=\"" + col.Name + "\"");
            sb.AppendLine(", IsForeignKey=true)]");
            sb.AppendLine("\tpublic " + refClassName + " " + refClassName);
		    sb.AppendLine("\t{");
			sb.AppendLine("\t\tget");
			sb.AppendLine("\t\t{");
            sb.AppendLine("\t\t\treturn this." + varName + ".Entity;");
            sb.AppendLine("\t\t}");
            sb.AppendLine("\t\tset");
			sb.AppendLine("\t\t{");
            sb.AppendLine("\t\t\t" + refClassName + " previousValue = this." + varName + ".Entity;");
			sb.AppendLine("\t\t\tif (((previousValue != value)");
            sb.AppendLine("\t\t\t	|| (this." + varName + ".HasLoadedOrAssignedValue == false)))");
			sb.AppendLine("\t\t\t{");
			//sb.AppendLine("\t\t\t\tthis.SendPropertyChanging();");
			sb.AppendLine("\t\t\t\tif ((previousValue != null))");
			sb.AppendLine("\t\t\t\t{");
            sb.AppendLine("\t\t\t\t\tthis." + varName + ".Entity = null;");
            sb.AppendLine("\t\t\t\t\tpreviousValue." + paramName + ".Remove(this);");
			sb.AppendLine("\t\t\t\t}");
            sb.AppendLine("\t\t\t\tthis." + varName + ".Entity = value;");
			sb.AppendLine("\t\t\t\tif ((value != null))");
			sb.AppendLine("\t\t\t\t{");
            sb.AppendLine("\t\t\t\t\tvalue." + paramName + ".Add(this);");
			sb.AppendLine("\t\t\t\t\tthis." + col.Name + " = value." + col.Name + ";");
			sb.AppendLine("\t\t\t\t}");
			sb.AppendLine("\t\t\t\telse");
			sb.AppendLine("\t\t\t\t{");
            sb.AppendLine("\t\t\t\t\tthis." + col.Name + " = default(Nullable<int>);");
			sb.AppendLine("\t\t\t\t}");
			//sb.AppendLine("\t\t\t\tthis.SendPropertyChanged(\"" + fkClass + "\");");
			sb.AppendLine("\t\t\t}");
            sb.AppendLine("\t\t}");
            sb.AppendLine("\t}");
			sb.AppendLine();
            sb.AppendLine();
        }

    }
}
