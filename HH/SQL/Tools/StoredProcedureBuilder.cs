﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Data.Linq.Mapping;
using System.Text;
using System.Web.UI.WebControls;

using SMO = Microsoft.SqlServer.Management.Smo;
using HH.Utils;

namespace HH.SQL.Tools
{
    public enum StoredProcedureBuildMode
    {
        Get,
        Set,
        Delete,
        Search,
        All
    }

    /// <summary>
    /// Builds Get,Set,Delete and Search stored procs from DAL classes
    /// </summary>
    public static class StoredProcedureBuilder
    {
        private static string _dbName = string.Empty;
        private static bool _drop = false;
        private static object _topLevelObject = null;
        private static Assembly _parentAssembly = null;

        private static Microsoft.SqlServer.Management.Common.ServerConnection _conn = null;
        private static SMO.Database _db = null;
        private static SMO.Database _Database
        {
            get
            {
                if (_db == null)
                {
                    SMO.Server server = new SMO.Server(_conn);
                    _db = server.Databases[_dbName];
                }
                return _db;
            }
        }

        /// <summary>
        /// Builds default Stored Procs via SMO based on the topLevelObject specified, the proc builder will traverse the hierarchy of DAL objects and create stored procs for all sub objects found
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="dbName"></param>
        /// <param name="mode">All generates SET,GET,SEARCH and DELETE Procs</param>
        /// <param name="dropPrevious">If the proc exists, drop it first</param>
        /// <param name="topLevelObject">The Top parent DAL object to inspect, the proc builder will traverse the hierarchy of DAL objects and create stored procs for all sub objects found</param>
        public static void BuildAll(Microsoft.SqlServer.Management.Common.ServerConnection conn, string dbName, StoredProcedureBuildMode mode, bool dropPrevious, object topLevelObject)
        {
            _conn = conn;
            _dbName = dbName;
            _drop = dropPrevious;
            _topLevelObject = topLevelObject;
            _parentAssembly = topLevelObject.GetType().Assembly;
            BuildAll(mode, topLevelObject);
        }
        private static void BuildAll(StoredProcedureBuildMode mode, object topLevelObject)
        {
            TableAttribute t = ReflectionUtils.GetAttribute<TableAttribute>(topLevelObject.GetType());
            if (t != null)
                Add(mode, t, topLevelObject);

            foreach (PropertyInfo info in topLevelObject.GetType().GetProperties())
            {
                object obj = info.GetValue(topLevelObject, null);
                if (CanInspect(obj, _topLevelObject, _parentAssembly))
                    BuildAll(mode, obj);
            }
        }

        /// <summary>
        /// Builds stored procs based on the DAL object (tableObject) specified. Does not traverse the DAL object hierarchy
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="dbName"></param>
        /// <param name="mode"></param>
        /// <param name="dropPrevious"></param>
        /// <param name="tableObject"></param>
        public static void Build(Microsoft.SqlServer.Management.Common.ServerConnection conn, string dbName, StoredProcedureBuildMode mode, bool dropPrevious, object tableObject)
        {
            _conn = conn;
            _dbName = dbName;
            _drop = dropPrevious;
            TableAttribute t = ReflectionUtils.GetAttribute<TableAttribute>(tableObject.GetType());
            if (t != null)
                Add(mode, t, tableObject);
            else
                throw new Exception("Could not build stored procedure from object " + tableObject.GetType().Name);
        }

        private static void Add(StoredProcedureBuildMode mode, TableAttribute t, object obj)
        {
            SMO.StoredProcedure setProc = new SMO.StoredProcedure(_Database, "PR_SET_" + t.Name);
            SMO.StoredProcedure searchProc = new SMO.StoredProcedure(_Database, "PR_SEARCH_" + t.Name);
            SMO.Table table = _Database.Tables[t.Name];
            setProc.TextMode = false;
            searchProc.TextMode = false;

            PropertyInfo[] props = obj.GetType().GetProperties();
            string pk = string.Empty;
            List<string> fks = new List<string>();
            string insertColumns = string.Empty;
            string insertValues = string.Empty;
            string updateValues = string.Empty;
            StringBuilder searchTxt = new StringBuilder();
            bool searchAppendAnd = false;

            for (int i=0; i<props.Length; i++)
            {
                PropertyInfo info = props[i];
                ColumnAttribute c = ReflectionUtils.GetAttribute<ColumnAttribute>(info);
                AssociationAttribute a = ReflectionUtils.GetAttribute<AssociationAttribute>(info);
                
                if (c != null)
                {
                    //column name may differ from property name so check for it
                    string columnName = (!string.IsNullOrEmpty(c.Name)) ? c.Name : info.Name;
                    
                    if (c.IsPrimaryKey)
                    {
                        pk = columnName;
                    }
                    else if (mode == StoredProcedureBuildMode.All ||
                             mode == StoredProcedureBuildMode.Set)
                    {
                        if (insertColumns != string.Empty)
                        {
                            insertColumns += ", ";
                            insertValues += ", ";
                            updateValues += ", ";
                        }
                        insertColumns += "[" + columnName + "]";
                        insertValues += "@" + columnName;
                        updateValues += "[" + columnName + "]=@" + columnName;
                    }

                    if (mode == StoredProcedureBuildMode.All ||
                        mode == StoredProcedureBuildMode.Set)
                    {
                        SMO.Column col = table.Columns[columnName];
                        if (col == null)
                            throw new Exception("Column " + columnName + " was not found in the database");
                        
                        SMO.StoredProcedureParameter param = new SMO.StoredProcedureParameter(setProc, "@" + columnName);
                        param.DataType = col.DataType;
                        setProc.Parameters.Add(param);
                    }

                    if (mode == StoredProcedureBuildMode.All ||
                        mode == StoredProcedureBuildMode.Search)
                    {
                        if (!searchAppendAnd)
                            searchTxt.AppendLine("SELECT * FROM [dbo].[" + t.Name + "] WHERE");
                        BuildGetOrSearchParam(ref searchProc, ref searchTxt, table, columnName, searchAppendAnd);
                        searchAppendAnd = true;
                    }
                }
                else if (a != null &&
                         a.IsForeignKey)
                {
                    fks.Add(a.ThisKey);
                }
            }

            if (mode == StoredProcedureBuildMode.All ||
                mode == StoredProcedureBuildMode.Get)
            {
                SMO.StoredProcedure getProc = new SMO.StoredProcedure(_Database, "PR_GET_" + t.Name);
                getProc.TextMode = false;

                if (_drop)
                {
                    try
                    {
                        if (_Database.StoredProcedures.Contains(getProc.Name))
                            _Database.StoredProcedures[getProc.Name].Drop();
                    }
                    catch { }
                }

                StringBuilder selTxt = new StringBuilder();
                selTxt.Append("SELECT * FROM [dbo].[" + t.Name + "]");
                if (string.IsNullOrEmpty(pk) && fks.Count == 0)
                    selTxt.AppendLine();
                else
                    selTxt.AppendLine(" WHERE");
                bool appendAnd = false;
                if (!string.IsNullOrEmpty(pk))
                {
                    BuildGetOrSearchParam(ref getProc, ref selTxt, table, pk, appendAnd);
                    appendAnd = true;
                }
                foreach (string fk in fks)
                {
                    BuildGetOrSearchParam(ref getProc, ref selTxt, table, fk, appendAnd);
                    appendAnd = true;
                }
                selTxt.AppendLine();
                getProc.TextBody = selTxt.ToString();
                if (!_Database.StoredProcedures.Contains(getProc.Name))
                    _Database.StoredProcedures.Add(getProc);
                getProc.Create();
            }
            if ((mode == StoredProcedureBuildMode.All ||
                 mode == StoredProcedureBuildMode.Delete) &&
                (!string.IsNullOrEmpty(pk) || 
                 fks.Count > 0))
            {
                SMO.StoredProcedure delProc = new SMO.StoredProcedure(_Database, "PR_DELETE_" + t.Name);
                delProc.TextMode = false;

                if (_drop)
                {
                    try
                    {
                        if (_Database.StoredProcedures.Contains(delProc.Name))
                            _Database.StoredProcedures[delProc.Name].Drop();
                    }
                    catch { }
                }
                string txt = "DELETE FROM [dbo].[" + t.Name + "] WHERE ";
                if (!string.IsNullOrEmpty(pk))
                {
                    txt += pk + "=@" + pk;
                    SMO.StoredProcedureParameter param = new SMO.StoredProcedureParameter(delProc, "@" + pk);
                    param.DataType = table.Columns[pk].DataType;
                    delProc.ImplementationType = SMO.ImplementationType.TransactSql;
                    delProc.Parameters.Add(param);
                }
                else
                {
                    for (int i = 0; i < fks.Count; i++)
                    {
                        string fk = fks[i];
                        if (i != 0)
                            txt += " AND ";

                        txt += fk + "=@" + fk;
                        SMO.StoredProcedureParameter param = new SMO.StoredProcedureParameter(delProc, "@" + fk);
                        param.DataType = SMO.DataType.Int;
                        delProc.ImplementationType = SMO.ImplementationType.TransactSql;
                        delProc.Parameters.Add(param);
                    }
                }

                delProc.TextBody = txt;
                if (!_Database.StoredProcedures.Contains(delProc.Name))
                    _Database.StoredProcedures.Add(delProc);
                delProc.Create();
            }
            if ((mode == StoredProcedureBuildMode.All ||
                 mode == StoredProcedureBuildMode.Set) &&
                !string.IsNullOrEmpty(pk))
            {
                if (_drop)
                {
                    try
                    {
                        if (_Database.StoredProcedures.Contains(setProc.Name))
                            _Database.StoredProcedures[setProc.Name].Drop();
                    }
                    catch { }
                }
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("IF @" + pk + " < 1");
                sb.AppendLine("BEGIN");
                sb.AppendLine(" INSERT INTO [dbo].[" + t.Name + "] (");
                sb.AppendLine(" " + insertColumns);
                sb.AppendLine("  )");
                sb.AppendLine("  VALUES (");
                sb.AppendLine(" " + insertValues);
                sb.AppendLine("  );");
                sb.AppendLine("SELECT @" + pk + "=@@IDENTITY;");
                sb.AppendLine("END");
                sb.AppendLine("ELSE");
                sb.AppendLine(" UPDATE [dbo].[" + t.Name + "]");
                sb.AppendLine(" SET ");
                sb.AppendLine("  " + updateValues);
                sb.AppendLine(" WHERE " + pk + "=@" + pk + ";");
                sb.AppendLine();
                sb.AppendLine("SELECT * FROM [dbo].[" + t.Name + "] WHERE " + pk + "=@" + pk + ";");
                sb.AppendLine();
                setProc.TextBody = sb.ToString();
                if (_Database.StoredProcedures.Contains(setProc.Name))
                    _Database.StoredProcedures.Add(setProc);
                setProc.Create();
            }
            if ((mode == StoredProcedureBuildMode.All ||
                 mode == StoredProcedureBuildMode.Search))
            {
                if (_drop)
                {
                    try
                    {
                        if (_Database.StoredProcedures.Contains(searchProc.Name))
                            _Database.StoredProcedures[searchProc.Name].Drop();
                    }
                    catch { }
                }

                searchTxt.AppendLine();
                searchProc.TextBody = searchTxt.ToString();
                if (!_Database.StoredProcedures.Contains(searchProc.Name))
                    _Database.StoredProcedures.Add(searchProc);
                searchProc.Create();
            }
        }

        private static void BuildGetOrSearchParam(ref SMO.StoredProcedure proc, ref StringBuilder sb, SMO.Table table, string column, bool appendAnd)
        {
            SMO.StoredProcedureParameter param = new SMO.StoredProcedureParameter(proc, "@" + column);
            param.DataType = table.Columns[column].DataType;
            param.DefaultValue = "null";
            proc.ImplementationType = SMO.ImplementationType.TransactSql;
            proc.Parameters.Add(param);
            if (appendAnd)
                sb.AppendLine(" AND ");
            sb.Append(" ((@" + column + " is null) OR (" + column + " = @" + column + "))");
        }

        /// <summary>
        /// True to traverse the hierarchy from parent to child
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="topLevelObject"></param>
        /// <param name="parentAssembly"></param>
        /// <returns></returns>
        private static bool CanInspect(object obj, object topLevelObject, Assembly parentAssembly)
        {
            if (obj != null)
            {
                if (obj.GetType().IsArray)
                    return false;
                if (obj == topLevelObject)
                    return false;
                return (parentAssembly.ManifestModule.GetType(obj.GetType().Name, false, true) != null);
            }
            return false;
        }
    }
}
