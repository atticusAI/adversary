﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace HH.SQL
{
    /// <summary>
    /// Thrown when the SQLDefaultMapper can't map somethin' or throw when an ISQLMapper can't map
    /// </summary>
    public class DataObjectMappingException : Exception
    {
        public PropertyInfo Info { get; set; }
        public object ParentObject { get; set; }
        public object Value { get; set; }

        public DataObjectMappingException(Exception innerException, PropertyInfo info, object parentObject, object value) :
            base("A " + innerException.GetType() + " exception was thrown while attempting to map value '" + value + "' to parent " +
                 "object '" + ((parentObject == null) ? "null" : parentObject.GetType().ToString()) + "' " +
                 "parameter '" + ((info == null) ? "null" : info.Name + "'"), innerException)
        {
            Info = info;
            ParentObject = parentObject;
            Value = value;
        }
    }
}
