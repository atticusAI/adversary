﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using System.Reflection;
using System.ComponentModel;
using System.Collections;
using System.Text.RegularExpressions;
using HH.Utils;

namespace HH.SQL
{
    /// <summary>
    /// Implements a default mapping of .NET types to SQL values and vice-versa
    /// </summary>
    public class SQLDefaultMapper : ISQLMapper
    {
        /// <summary>
        /// Gets the value of the property in the parent object and converts it to a corresponding SQL value if necessary
        /// </summary>
        /// <param name="parentObject"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public object ConvertPropertyToSQLValue(object parentObject, string propertyName)
        {
            PropertyInfo info = GetPropertyInfo(parentObject, propertyName);
            if (!info.CanRead)
                throw new Exception("Could not get value from property " + info.Name + ". Property cannot be read.");

            object value = null;

            try
            {
                value = info.GetValue(parentObject, null);

                if (value == null)
                    return DBNull.Value;

                object returnValue = null;
                if (info.PropertyType.IsGenericType && info.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                {
                    Type nullableType = Nullable.GetUnderlyingType(info.PropertyType);
                    returnValue = (nullableType.IsEnum) ? (int)value : value;
                }
                else
                {
                    returnValue = value;
                }

                if (returnValue is string)
                {
                    string str = returnValue as string;
                    if (string.IsNullOrEmpty(str))
                        return DBNull.Value;
                }
                if (returnValue is DateTime)
                {
                    DateTime? dt = returnValue as DateTime?;
                    if (dt == DateTime.MinValue || dt == DateTime.MaxValue)
                        return DBNull.Value;
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                throw new DataObjectMappingException(ex, info, parentObject, value);
            }
        }

        /// <summary>
        /// Takes the value specified and sets the parent object's property converting from SQL value to the property type if necessary
        /// </summary>
        /// <param name="parentObject"></param>
        /// <param name="propertyName"></param>
        /// <param name="value"></param>
        public void ConvertSQLValueToProperty(object parentObject, string propertyName, object sqlVal)
        {

            PropertyInfo info = GetPropertyInfo(parentObject, propertyName);
            if (!info.CanWrite)
                return; 

            object obj = info.GetValue(parentObject, null);

            try
            {
                if (obj != null && obj.Equals(sqlVal))
                    return;

                if (sqlVal == DBNull.Value)
                    sqlVal = null;

                if (info.PropertyType.IsGenericType && info.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    ReflectionUtils.CastAndSetProperty(parentObject, info.Name, sqlVal);
                else
                    info.SetValue(parentObject, Convert.ChangeType(sqlVal, info.PropertyType), null);
            }
            catch (Exception ex)
            {
                throw new DataObjectMappingException(ex, info, parentObject, sqlVal);
            }
            
        }

        private static PropertyInfo GetPropertyInfo(object parentObject, string propertyName)
        {
            if (parentObject == null)
                throw new ArgumentException("Parent object was null");

            PropertyInfo info = parentObject.GetType().GetProperty(propertyName);
            if (info == null)
                throw new ArgumentException("Property " + propertyName + " not found in parent object");
            return info;
        }
    }
}
