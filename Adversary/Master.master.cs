﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using HH.Branding;
using HH.Screen;
using System.Text;
using System.Web.UI.HtmlControls;
using HH.Utils;
using HH.Extensions;
using System.Drawing;
using HH.UI;

namespace HH.Branding
{
    public partial class Master2 : System.Web.UI.MasterPage, IMaster
    {
        public bool HeaderVisble { get; set; }
        public string HeaderText { get; set; }
        public string Title { get; set; }
        public bool BreadCrumbVisble { get; set; }
        public string BreadCrumbText { get; set; }
        public string CopyrightText { get; set; }
        //public string ErrorMessage { get; set; }

        public string FeedbackMessage { get; set; }
        public MasterPageFeedbackMode FeedbackMode { get; set; }

        //public MasterPagePopupDialogMode PopupDialogMode { get; set; }
        //public string PopupDialogText { get; set; }

        public bool TabMenuEnabled { get; set; }
        public bool TabMenuVisible { get; set; }
        public bool SideBarEnabled { get; set; }
        public bool SideBarVisible { get; set; }
        public bool PageContentVisible { get; set; }
        public bool FooterVisible { get; set; }
        public bool FooterNavVisible { get; set; }
        public bool NextButtonVisible { get; set; }
        public bool NextButtonEnabled { get; set; }
        public bool PreviousButtonVisible { get; set; }
        public bool PreviousButtonEnabled { get; set; }

        public event EventHandler PopupYesClick;
        public string PopupYesCommandArgument { get; set; }
        public event EventHandler PopupNoClick;
        public string PopupNoCommandArgument { get; set; }
        public event MasterPagePostBackEventHandler PostBackEvent;
        public bool HasRegisteredPostBackEvent 
        {
            get
            {
                return (PostBackEvent != null);
            }
        }
        private bool _popupVisible = false; 
        public bool PopupDialogVisible
        {
            get
            {
                return _popupVisible;
            }
        }
        //public bool IsMasterPagePostback { get; }

        public ScriptManager ScriptManager 
        {
            get
            {
                return this._scriptManager;
            }
        }
        public ContentPlaceHolder HeaderContent
        {
            get
            {
                return this.HeaderContentPlaceHolder;
            }
        }
        public ContentPlaceHolder UpperContent
        {
            get
            {
                return this.UpperContentPlaceHolder;
            }
        }
        public IScreen Screen 
        {
            get
            {
                return _screen;
            }
        }
        public ContentPlaceHolder LowerContent
        {
            get
            {
                return this.LowerContentPlaceHolder;
            }
        }

        public bool RoleSecurityEnabled { get; set; }
        public RoleProvider CustomRoleProvider { get; set; }
        public MasterPageMenuMode MenuMode { get; set; }
        private SiteMapProvider _smProvider = null;
        public SiteMapProvider SiteMapProvider 
        {
            get
            {
                if (_smProvider == null)
                    _smProvider = SiteMap.Provider;
                return _smProvider;
            }
            set
            {
                _smProvider = value;
            }
        }
        private MasterPageTabCollection _tabs = new MasterPageTabCollection();
        public MasterPageTabCollection Tabs 
        {
            get
            {
                return _tabs;
            }
        }
        public MasterPageTab SelectedTab
        {
            get
            {
                foreach (MasterPageTab tab in _tabs)
                {
                    if (tab.Selected)
                        return tab;
                }
                return null;
            }
        }

        private int _selectedItemIndex = -1;
        private MasterPageNavControl _selectedItem = null;
        public MasterPageNavControl SelectedMenuItem
        {
            get
            {
                return _selectedItem;
            }
        }

        public string SelectedTabTitle
        {
            get
            {
                MasterPageTab tab = SelectedTab;
                if (tab == null)
                    return null;
                return tab.Title;
            }
        }
        private List<MasterPageSideBar> _sideBarMenu = new List<MasterPageSideBar>();
        public List<MasterPageSideBar> SideBarMenu
        {
            get
            {
                return _sideBarMenu;
            }
        }
        public MasterPageSideBar SelectedSideBar
        {
            get
            {
                foreach (MasterPageSideBar menu in _sideBarMenu)
                {
                    if (menu.Selected)
                        return menu;
                }
                return null;
            }
        }
        public string SelectedSideBarTitle
        {
            get
            {
                MasterPageSideBar sideBar = SelectedSideBar;
                if (sideBar == null)
                    return null;
                return sideBar.Title;
            }
        }
        private List<Control> _footerControls = new List<Control>();
        public List<Control> FooterControls
        {
            get
            {
                return _footerControls;
            }
        }

        public string LogoutUrl { get; set; }

        private Dictionary<string, string> _urlParams = new Dictionary<string, string>();
        public Dictionary<string, string> UrlParams 
        {
            get
            {
                return _urlParams;
            }
        }


        public Master2()
        {
            //defaults
            BreadCrumbVisble = true;
            BreadCrumbText = string.Empty;
            HeaderVisble = true;
            HeaderText = string.Empty;
            //ErrorMessage = string.Empty;
            Title = "Holland & Hart";
            FeedbackMode = MasterPageFeedbackMode.Information;
            MenuMode = MasterPageMenuMode.Tabs;
            //PopupDialogMode = MasterPagePopupDialogMode.Popup;
            //PopupDialogText = "You have clicked a link that will take you away from this page without saving your work. Are you sure you would like to do this?";
            PageContentVisible = true;
            TabMenuEnabled = true;
            TabMenuVisible = true;
            SideBarEnabled = true;
            SideBarVisible = true;
            FooterVisible = true;
            PreviousButtonVisible = true;
            PreviousButtonEnabled = true;
            NextButtonVisible = true;
            NextButtonEnabled = true;
            FooterNavVisible = true;
            RoleSecurityEnabled = true;
            LogoutUrl = string.Empty;
        }


        
        #region BIND METHODS

        public override void DataBind()
        {
            if (SiteMapProvider == null || SiteMapProvider.RootNode == null)
                throw new Exception("Attempt to populate menu failed. Could not find any SiteMapProvider data.");

            switch (MenuMode)
            {
                case MasterPageMenuMode.Tabs:
                    BindTabs();
                    break;
                case MasterPageMenuMode.SideBar:
                    BindSideBarMenus();
                    break;
            }

            //Handle the main content here
            SiteMapNode currentNode = SiteMapProvider.CurrentNode;
            if (currentNode == null)
            {
                _btnPrev.Enabled = false;
                _btnNext.Enabled = false;
                return;
            }

            SiteMapNode prevNode = currentNode.PreviousSibling;
            while (prevNode != null &&
                   (!SiteMaps.IsSiteMapNodeVisible(prevNode) ||
                    !SiteMaps.IsSiteMapNodeEnabled(prevNode)))
                prevNode = prevNode.PreviousSibling;
            if (prevNode != null)
                _btnPrev.SetDataItem(prevNode);

            SiteMapNode nextNode = currentNode.NextSibling;
            while (nextNode != null &&
                   (!SiteMaps.IsSiteMapNodeVisible(nextNode) ||
                    !SiteMaps.IsSiteMapNodeEnabled(nextNode)))
                nextNode = nextNode.NextSibling;
            if (nextNode != null)
                _btnNext.SetDataItem(nextNode);

            //browser title
            if (string.IsNullOrEmpty(Title))
            {
                if (!string.IsNullOrEmpty(currentNode.Title))
                    Title = currentNode.Title;
                else if (!string.IsNullOrEmpty(currentNode.Description))
                    Title = currentNode.Description;
                else
                    Title = currentNode.Url;
            }

            if (!string.IsNullOrEmpty(BreadCrumbText))
                return;

            if (!string.IsNullOrEmpty(currentNode.Title))
                BreadCrumbText = currentNode.Title;
            else if (currentNode.Url.ToLower().Contains("default.aspx"))
                BreadCrumbText = "Home";
            SiteMapNode parentNode = currentNode.ParentNode;
            while (parentNode != null)
            {
                if (!string.IsNullOrEmpty(parentNode.Title))
                    BreadCrumbText = parentNode.Title + " > " + BreadCrumbText;
                parentNode = parentNode.ParentNode;
            }
        }

        #region TAB BIND METHODS
        private void BindTabs()
        {
            Tabs.Clear();

            SiteMapNodeCollection nodes = SiteMapProvider.RootNode.ChildNodes;
            SiteMapNode currentNode = SiteMapProvider.CurrentNode;
            for (int x=0; x<nodes.Count; x++)
            {
                SiteMapNode n = nodes[x];
                MasterPageTab tab = new MasterPageTab(n.Title);
                LoadNavControl(tab, n, currentNode);

                for (int y=0; y<n.ChildNodes.Count; y++)
                {
                    SiteMapNode c = n.ChildNodes[y];
                    int safetyCount = 0;
                    MasterPageTabItem subMenuItem = BindSubMenuItem(c, currentNode, tab, ref safetyCount);
                    if (subMenuItem.Selected && _selectedItem == null)
                    {
                        _selectedItem = subMenuItem;
                        _selectedItemIndex = y;
                    }
                    tab.Items.Add(subMenuItem);
                }

                if (!string.IsNullOrEmpty(LogoutUrl))
                {
                    MasterPageTabItem logoutItem = new MasterPageTabItem(tab, "Logout");
                    logoutItem.Url = LogoutUrl;
                    logoutItem.Description = "Logout of the application";
                    tab.Items.Add(logoutItem);
                }
                Tabs.Add(tab);
            }
        }

        private MasterPageTabItem BindSubMenuItem(SiteMapNode n, SiteMapNode currentNode, MasterPageNavControl parent, ref int safetyCount)
        {
            MasterPageTabItem item = new MasterPageTabItem(parent, n.Title);
            LoadNavControl(item, n, currentNode);
            safetyCount++;
            if (safetyCount >= 500)
                return item;
            for (int x=0; x < n.ChildNodes.Count; x++)
            {
                SiteMapNode c = n.ChildNodes[x];
                MasterPageTabItem subMenuItem = BindSubMenuItem(c, currentNode, item, ref safetyCount);
                if (subMenuItem.Selected)
                {
                    _selectedItem = subMenuItem;
                    _selectedItemIndex = x;
                }
                item.Items.Add(subMenuItem);
            }

            return item;
        }
        #endregion

        #region SIDE BAR BIND METHODS
        private void BindSideBarMenus()
        {
            SiteMapNodeCollection nodes = SiteMapProvider.RootNode.ChildNodes;
            SiteMapNode currentNode = SiteMapProvider.CurrentNode;
            foreach (SiteMapNode n in nodes)
            {
                MasterPageSideBar bar = new MasterPageSideBar(n.Title);
                LoadNavControl(bar, n, currentNode);
                foreach (SiteMapNode c in n.ChildNodes)
                    bar.Items.Add(BindSideBarMenuItem(c, currentNode));
                SideBarMenu.Add(bar);
            }
        }

        private MasterPageSideBarItem BindSideBarMenuItem(SiteMapNode n, SiteMapNode currentNode)
        {
            MasterPageSideBarItem item = new MasterPageSideBarItem(n.Title);
            LoadNavControl(item, n, currentNode);
            foreach (SiteMapNode c in n.ChildNodes)
                item.Items.Add(BindSideBarMenuItem(c, currentNode));
            return item;
        }
        #endregion

        #region MISC BIND METHODS

        private void LoadNavControl(MasterPageNavControl c, SiteMapNode n, SiteMapNode currentNode)
        {
            c.Title = n.Title;
            c.Url = n.Url;
            c.Description = n.Description;
            c.Selected = (n == currentNode);
            c.Enabled = SiteMaps.IsSiteMapNodeEnabled(n);
            c.Visible = SiteMaps.IsSiteMapNodeVisible(n);
            c.Locked = !CheckRoleSecurity(n);
            c.Target = SiteMaps.GetTarget(n);
            //c.ConfirmRedirect = PopupDialogOnTabClick || SiteMaps.IsConfirmRedirectEnabled(n);

            //SiteMapNode roleNode = n;
            int count = 0;
            while (n != null)
            {
                c.AddRoleList(n.Roles);
                n = n.ParentNode;
                count++;
                if (count >= 200)
                {
                    //sanity check, make sure this doesn't get caught looping
                    n = null;
                    break;
                }
            }
        }

        /// <summary>
        /// Returns true if role security is off or if the user belongs to a role specified in the sitemap node
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        private bool CheckRoleSecurity(SiteMapNode n)
        {
            if (!RoleSecurityEnabled)
                return true;
            //if (CustomRoleProvider != null)
                //return CustomRoleProvider.IsUserAuthorized(n);
                //return SiteMapProvider.IsAccessibleToUser(Context, n);
            return SiteMaps.IsUserAuthorized(n);
        }
        
        #endregion

        #endregion


        #region PAGE LOAD METHODS

        protected void Page_Load(object sender, EventArgs e)
        {
            if (TabMenuVisible)
            {
                CreateTabs();
            }
            else
            {
                TableCell td = new TableCell();
                td.Text = "&nbsp;";
                td.CssClass = "tabOff";
                _trTabs.Cells.Add(td);
            }

            if (SideBarVisible)
                CreateSideBar();

            if (FooterVisible)
                CreateFooter();

            if (SiteMapProvider != null && 
                SiteMapProvider.CurrentNode != null && 
                !CheckRoleSecurity(SiteMapProvider.CurrentNode))
            {
                //_divLocalPageContent.Visible = false;
                //_phFooterControls.Visible = false;
                //UpperContent.Controls.Clear();
                //Screen.Form.Rows.Clear();
                //LowerContent.Controls.Clear();
                UpperContent.Visible = false;
                Screen.Visible = false;
                LowerContent.Visible = false;
                FeedbackMessage = "<br/><br/>You do not have permission to view this page";
                FeedbackMode = MasterPageFeedbackMode.Warning;
            }

            //if (PopupYesClick != null)
            //{
            //    _btnPopupOK.Click += new EventHandler(PopupDialog_Click);
            //    _modalPopupDialog.OkControlID = null;
            //}
            //if (PopupCancelClick != null)
            //{
            //    _btnPopupCancel.Click += new EventHandler(PopupDialog_Click);
            //    _modalPopupDialog.CancelControlID = null;
            //}
        }

        protected override void OnPreRender(EventArgs e)
        {
            _divTabMenu.Visible = TabMenuVisible;
            _divBreadCrumbs.Visible = BreadCrumbVisble && !string.IsNullOrEmpty(BreadCrumbText);
            _divLocalPageContent.Visible = PageContentVisible;
            _tableHeader.Visible = (HeaderVisble && !string.IsNullOrEmpty(HeaderText)) || HeaderContentPlaceHolder.Controls.Count > 0;
            _lblFeedback.Text = FeedbackMessage;

            switch (FeedbackMode)
            {
                case MasterPageFeedbackMode.Information:
                    _lblFeedback.ForeColor = Color.Black;
                    break;
                case MasterPageFeedbackMode.Success:
                    _lblFeedback.ForeColor = Color.Blue;
                    break;
                case MasterPageFeedbackMode.Error:
                    _lblFeedback.ForeColor = Color.Red;
                    break;
                case MasterPageFeedbackMode.Warning:
                    _lblFeedback.ForeColor = Color.Orange;
                    break;
                case MasterPageFeedbackMode.Confirm:
                    _lblFeedback.ForeColor = Color.Green;
                    break;
                default:
                    _lblFeedback.ForeColor = Color.Black;
                    break;
            }

            if (!SideBarVisible)
            {
                _divContentMain.Attributes["class"] = "contentMainNoSidebar";
                _divContentSidebar.Visible = false;
                _divContentSpacer.Visible = false;
            }
            else
            {
                _divContentSidebarMenu.Visible = SideBarMenu.Count > 0;
            }
            
            _divFooter.Visible = FooterVisible;
            _btnPrev.Visible = FooterNavVisible && PreviousButtonVisible;
            _btnPrev.Enabled = PreviousButtonEnabled && (_btnPrev.GetDataItem() != null);
            _btnNext.Visible = FooterNavVisible && NextButtonVisible;
            _btnNext.Enabled = NextButtonEnabled && (_btnNext.GetDataItem() != null);
            _divCopyright.Visible = !string.IsNullOrEmpty(CopyrightText);
            //_btnPopupOK.CommandName = PopupConfirmCommandName;
            _btnPopupYes.CommandArgument = PopupYesCommandArgument;
            //_btnPopupCancel.CommandName = PopupConfirmCommandName;
            _btnPopupNo.CommandArgument = PopupNoCommandArgument;
        }

        
        #region TAB CREATE METHODS
        private void CreateTabs()
        {
            if (Tabs.Count == 0)
                return;

            for (int i = 0; i < Tabs.Count; i++)
            {
                MasterPageTab tab = Tabs[i];
                if (!tab.Visible)
                    continue;

                TableCell td = new TableCell();
                bool locked = tab.Locked; //RoleSecurityEnabled && !Utils.IsUserAuthorized(tab.Roles.ToArray());
                if (locked)
                    td.Controls.Add(BuildLockIcon());
                td.ID = "Tab_" + i;
                td.Enabled = TabMenuEnabled && tab.Enabled;
                td.CssClass = (tab.Selected && tab.Enabled && !locked) ? "tabActive" : (!TabMenuEnabled || locked || !tab.Enabled) ? "tabInactiveDisabled" : "tabInactive";
                td.ToolTip = tab.Description + ((locked) ? " (locked)" : (!TabMenuEnabled || !tab.Enabled) ? " (disabled)" : string.Empty);
                bool buildLinkBtn = false;
                if (TabMenuEnabled && !locked && tab.Enabled)
                {
                    if (string.IsNullOrEmpty(tab.Url))
                    {
                        td.Attributes.Add("onclick", "handleTabClick(this, " + i + ");");
                    }
                    else
                    {
                        if (HasRegisteredPostBackEvent)
                        {
                            //if (tab.Enabled && !tab.Selected && !locked)
                            if (tab.Enabled && !locked)
                            {
                                td.Attributes.Add("onclick", "handleTabClickPostBack(this, " + i + ", " + tab.ConfirmRedirect.ToString().ToLower() + ");");
                                buildLinkBtn = true;
                            }
                        }
                        else
                        {
                            td.Attributes.Add("onclick", "window.location.href='" + tab.Url + "'");
                        }
                    }
                }
                if (buildLinkBtn)
                {
                    LinkButton link = new LinkButton();
                    link.Text = tab.Title;
                    link.ID = "TabLink_" + i;
                    link.CommandName = "TabClick";
                    link.CommandArgument = tab.Url;
                    link.Click += new EventHandler(this.TabMenu_Click);
                    td.Controls.Add(link);
                    //if (tab.ConfirmRedirect)
                    //    AddConfirmExtender(link);
                }
                else
                {
                    td.Controls.Add(new LiteralControl(tab.Title));
                }
                _trTabs.Cells.Add(td);

                if (i != (Tabs.Count - 1))
                {
                    td = new TableCell();
                    td.CssClass = "tabSpacerCell";
                    _trTabs.Cells.Add(td);
                }

                if (!locked)
                    CreateTabSubMenu(tab, i);
            }

            //calculate width for tab cells
            int spacerCount = (_trTabs.Cells.Count - 1) / 2;
            int tabCount = _trTabs.Cells.Count - spacerCount;
            double spacerWidth = 1;
            double tabWidthTotal = 100 - (spacerWidth * spacerCount);
            double tabWidth = tabWidthTotal / tabCount;
            foreach (TableCell td in _trTabs.Cells)
            {
                if (td.CssClass != "tabSpacerCell")
                    td.Width = Unit.Percentage(tabWidth);
                else
                    td.Width = Unit.Percentage(spacerWidth);
            }
        }
        private void CreateTabSubMenu(MasterPageTab tab, int index)
        {
            TableRow tr = new TableRow();
            tr.ID = "SubMenu_" + index;
            tr.Attributes.Add("name", "SubMenu_" + index);
            tr.Width = Unit.Percentage(100);

            int subIndex = 0;
            foreach (MasterPageTabItem item in tab.Items)
            {
                if (!item.Visible)
                    continue;
                
                TableCell td = new TableCell();
                td.HorizontalAlign = HorizontalAlign.Center;
                Control menu = BuildFlyoutMenu(item, false);
                if (string.IsNullOrEmpty(menu.ID))
                    menu.ID = tr.ID + "___Menu_" + subIndex;
                td.Controls.Add(menu);
                tr.Cells.Add(td);

                subIndex++;
            }

            //add spacers and apply width to submenu cells
            //int spacerCount = (tr.Cells.Count - 1) / 2;
            int spacerCount = 0;
            for (int i = tr.Cells.Count-1; i > 0; i--)
            {
                TableCell td = new TableCell();
                td.HorizontalAlign = HorizontalAlign.Center;
                td.CssClass = "subMenuSpacer";
                td.Text = "|";
                tr.Cells.AddAt(i, td);
                spacerCount++;
            }
            int menuCount = tr.Cells.Count - spacerCount;
            double spacerWidth = 2;
            double menuWidthTotal = 100 - (spacerWidth * spacerCount);
            double menuWidth = menuWidthTotal / menuCount;
            foreach (TableCell td in tr.Cells)
            {
                if (td.Text != "|")
                    td.Width = Unit.Percentage(menuWidth);
                else
                    td.Width = Unit.Percentage(spacerWidth);
            }


            string display = (tab.Selected) ? "" : "none";
            tr.Style.Add("display", display);
            _tableTabSubMenu.Rows.Add(tr);
        }

        #endregion


        #region SIDE BAR METHODS
        private void CreateSideBar()
        {
            if (SideBarMenu.Count == 0)
                return;

            _phSidebar.Controls.Add(new LiteralControl("<dl>"));
            foreach (MasterPageSideBar item in SideBarMenu)
            {
                if (!item.Visible)
                    continue;

                Control menu = BuildFlyoutMenu(item, true);
                
                //if (item.Selected || selected)
                    //menu.ForeColor = Color.Yellow;

                _phSidebar.Controls.Add(new LiteralControl("<dt>"));
                _phSidebar.Controls.Add(menu);
                _phSidebar.Controls.Add(new LiteralControl("</dt>"));
            }
            _phSidebar.Controls.Add(new LiteralControl("</dl>"));
        }

        #endregion


        #region MENU BUILDING METHODS

        private Menu BuildMenu(bool isSubMenuFlyout)
        {
            Menu menu = new Menu();
            if (isSubMenuFlyout)
            {
                menu.DynamicHorizontalOffset = -3;
                menu.DynamicVerticalOffset = 0;
                menu.StaticMenuItemStyle.CssClass = "subMenuFlyoutLink";
                menu.DynamicMenuStyle.CssClass = "subMenuFlyout";
                menu.DynamicMenuItemStyle.CssClass = "subMenuFlyoutItem";
                menu.DynamicHoverStyle.CssClass = "subMenuFlyoutHover";
                menu.DynamicSelectedStyle.CssClass = "subMenuFlyoutItemSelected";
            }
            else
            {
                menu.DynamicHorizontalOffset = -3;
                menu.DynamicVerticalOffset = -10;
                menu.StaticMenuItemStyle.CssClass = "sideBarMenuLink";
                menu.DynamicMenuStyle.CssClass = "sideBarMenuFlyout";
                menu.DynamicMenuItemStyle.CssClass = "sideBarMenuFlyoutItem";
                menu.DynamicHoverStyle.CssClass = "sideBarMenuFlyoutHover";
                menu.DynamicSelectedStyle.CssClass = "sideBarMenuFlyoutSelected";
            }
            return menu;
        }

        private Control BuildFlyoutMenu(MasterPageNavControl nav, bool isSideBarMenu)
        {
            if (!nav.Visible)
                return BuildMenu(isSideBarMenu);
            if (nav.Locked || !nav.Enabled)
                return BuildDisabledControl(nav, nav.Locked, isSideBarMenu);
            if (!string.IsNullOrEmpty(nav.Target))
            {
                HtmlAnchor link = new HtmlAnchor();
                link.InnerHtml = nav.Title;
                if (!string.IsNullOrEmpty(nav.ID))
                    link.ID = nav.ID;
                link.Target = nav.Target;
                link.HRef = nav.Url;
                link.Attributes.Add("class", (isSideBarMenu) ? "sideBarMenuLink" : "subMenuFlyoutLink");
                return link;
            }

            Menu menu = BuildMenu(!isSideBarMenu);
            menu.ItemWrap = !isSideBarMenu;
            if (HasRegisteredPostBackEvent)
                menu.MenuItemClick += new MenuEventHandler(MenuItem_Click);
            MenuItem item = BuildFlyoutMenuItem(nav);
            if (!isSideBarMenu && item.Selected)
                item.Text = "<span style='color: yellow;'>" + item.Text + "</span>";
            menu.Items.Add(item);
            if (!string.IsNullOrEmpty(nav.ID))
                menu.ID = nav.ID;
            return menu;

            //SiteMapDataSourceView dv = node.GetDataSourceView(new SiteMapDataSource(), node.Title + "_View");
            //menu.DataSource = dv.Select(new DataSourceSelectArguments());
            //bool enabled = (_SiteMapMenuMode == MenuMode.SideBar) ? _SideBarMenuEnabled : _TabMenuEnabled;
            //menu.Enabled = enabled;
            //menu.DynamicEnableDefaultPopOutImage = enabled;
            //menu.StaticEnableDefaultPopOutImage = enabled;
            //menu.DataBind();
            //return menu;
        }

        private MenuItem BuildFlyoutMenuItem(MasterPageNavControl nav)
        {
            MenuItem item = new MenuItem();
            item.Text = nav.Title;
            item.ToolTip = nav.Description;
            item.Selected = nav.Selected;
            
            if (nav.Locked)
            {
                item.NavigateUrl = "#";
                item.Text = "&nbsp;&nbsp;" + item.Text;
                item.ImageUrl = Request.ApplicationPath + "/_css/lock.png";
                item.ToolTip += " (Locked)";
                item.Enabled = false;
                item.Selected = false;
                return item;
            }
            if (!nav.Enabled)
            {
                item.NavigateUrl = "#";
                item.ToolTip += " (Disabled)";
                item.Enabled = false;
                item.Selected = false;
                return item;
            }
            
            if (HasRegisteredPostBackEvent && string.IsNullOrEmpty(nav.Target))
                item.Value = BuildUrl(nav);
            else
                item.NavigateUrl = BuildUrl(nav);
            if (!string.IsNullOrEmpty(nav.Target))
                item.Target = nav.Target;

            IEnumerable<MasterPageNavControl> items;
            if (nav is MasterPageTabItem)
                items = ((MasterPageTabItem)nav).Items.Cast<MasterPageNavControl>();
            else if (nav is MasterPageSideBar)
                items = ((MasterPageSideBar)nav).Items.Cast<MasterPageNavControl>();
            else
                items = ((MasterPageSideBarItem)nav).Items.Cast<MasterPageNavControl>();

            foreach (MasterPageNavControl childNav in items)
            {
                if (childNav.Visible)
                {
                    MenuItem childItem = BuildFlyoutMenuItem(childNav);
                    item.ChildItems.Add(childItem);
                    item.Selected = item.Selected || childItem.Selected;
                }
            }
            
            
            return item;
        }
        #endregion



        #region MISC BUILD METHODS

        private void CreateFooter()
        {
            foreach (Control c in _footerControls)
            {
                //if (c is Button)
                //    ((Button)c).Click += new EventHandler(FooterNav_Click);
                //if (item.ConfirmRedirect)
                //    AddConfirmExtender(item.Control);
                _phFooterControls.Controls.Add(c);
            }

        }

        private HtmlImage BuildLockIcon()
        {
            HtmlImage img = new HtmlImage();
            img.Attributes.Add("class", "tabLockIcon");
            img.Src = Request.ApplicationPath + "/_css/lock.png";
            img.Alt = "Your user permissions do not allow access to this menu item";
            return img;
        }

        private WebControl BuildDisabledControl(MasterPageNavControl nav, bool locked, bool isSideBarMenu)
        {
            Panel pnl = new Panel();
            Label lbl = new Label();
            lbl.Text = nav.Title;
            lbl.ToolTip = nav.Description + ((locked) ? " (locked)" : " (disabled)");
            //lbl.CssClass = (isSideBarMenu) ? "sideBarMenuDisabled" : "subMenuDisabled";
            lbl.Enabled = false;

            if (locked)
            {
                if (isSideBarMenu)
                {
                    lbl.Text += "&nbsp;&nbsp;";
                    pnl.Controls.Add(lbl);
                    pnl.Controls.Add(BuildLockIcon());
                    return pnl;
                }
                pnl.Controls.Add(BuildLockIcon());
            }
            pnl.Controls.Add(lbl);
            return pnl;
        }


        private string BuildUrl(MasterPageNavControl nav)
        {
            string url = string.Empty; //"#";

            if (string.IsNullOrEmpty(nav.Url))
            {
                string requestUrl = "#";
                string param = string.Empty;
                //if (node["param"] != null)
                //{
                //    requestUrl = Request.Url.LocalPath;
                //    param = node["param"].Trim();
                //}
                //else if (node["appendparam"] != null)
                //{
                //    requestUrl = Request.Url.PathAndQuery;
                //    param = node["appendparam"].Trim();
                //}

                if (!string.IsNullOrEmpty(param))
                {
                    param = param.Replace("?", "");
                    param = (param.StartsWith("&")) ? param.Remove(0, 1) : param;
                    if (requestUrl.Contains("?"))
                        url = requestUrl + "&" + param;
                    else
                        url = requestUrl + "?" + param;
                }
            }
            else
            {
                url = nav.Url;
                url = AddUrlParams(url);
                    
            }


            return url;
        }

        private string AddUrlParams(string url)
        {
            if (UrlParams.Count > 0)
            {
                bool containsParams = url.Contains("?");
                foreach (string key in UrlParams.Keys)
                {
                    if (!containsParams)
                    {
                        url += "?" + key + "=" + UrlParams[key];
                        containsParams = true;
                    }
                    else
                        url += "&" + key + "=" + UrlParams[key];
                }
            }
            return url;
        }

        private bool IsSelectedLink(SiteMapNode node)
        {
            return (Request.RawUrl.ToLower().Contains(node.Url.ToLower())); /*&&
                    (string.IsNullOrEmpty(LogoutUrl) ||
                     !string.IsNullOrEmpty(LogoutUrl) &&
                     !node.Url.ToLower().Contains(LogoutUrl.ToLower())));*/
        }

        private HyperLink BuildLogoutLink(bool isSideBarMenu)
        {
            HyperLink link = new HyperLink();
            string url = LogoutUrl.Trim();
            if (url.StartsWith("?"))
                url = Request.Url.LocalPath + url;
            link.CssClass = (isSideBarMenu) ? "sideBarMenuLink" : "subMenuLink";
            link.NavigateUrl = url;
            link.Text = "Logout";
            link.ToolTip = "Logout of the application";
            return link;
        }

        #endregion

        #endregion


        #region MODAL DIALOG METHODS
        //private void AddConfirmExtender(Control c)
        //{
        //    if (c is HyperLink)
        //        return;
        //    if (c is Panel)
        //        return;

        //    string targetControlId = c.ID;
        //    AjaxControlToolkit.ConfirmButtonExtender confirm = new AjaxControlToolkit.ConfirmButtonExtender();
        //    confirm.ID = "confirm___" + targetControlId;
        //    confirm.ConfirmText = PopupDialogText;
        //    confirm.TargetControlID = targetControlId;

        //    if (PopupDialogMode != MasterPagePopupDialogMode.Popup)
        //    {
        //        _upPopupDialog.Visible = true;
        //        AjaxControlToolkit.ModalPopupExtender modal = new AjaxControlToolkit.ModalPopupExtender();
        //        modal.ID = "modal___" + targetControlId;
        //        modal.TargetControlID = targetControlId;
        //        modal.PopupControlID = _pnlPopupDialog.ID;
        //        modal.BackgroundCssClass = "modalFormDisablePane";
        //        modal.Y = 150;

        //        if (PopupYesClick == null)
        //            modal.OkControlID = _btnPopupOK.ID;
        //        if (PopupCancelClick == null)
        //            modal.CancelControlID = _btnPopupCancel.ID;
        //        this._scriptManager.Controls.Add(modal);
        //        confirm.DisplayModalPopupID = modal.ID;
        //    }
        //    this._scriptManager.Controls.Add(confirm);
        //}


        public void ShowPopupDialog(string message, MasterPagePopupDialogMode mode) { ShowPopupDialog(message, mode, Unit.Percentage(50), null, false); }
        public void ShowPopupDialog(string message, MasterPagePopupDialogMode mode, Unit width, object dataItem) { ShowPopupDialog(message, mode, width, dataItem, false); }
        /// <summary>
        /// Show the popup dialog
        /// </summary>
        /// <param name="message">Message to show in dialog box</param>
        /// <param name="mode">The button mode to use</param>
        /// <param name="dataItem">Optional data item to embed in the button</param>
        public void ShowPopupDialog(string message, MasterPagePopupDialogMode mode, Unit width, object dataItem, bool causesValidation)
        {
            if (mode == MasterPagePopupDialogMode.Popup)
            {
                _popupVisible = true;
                ShowJavaScriptAlert(message);
                return;
            }

            _btnPopupYes.Style.Add("display", "");
            _btnPopupNo.Style.Add("display", "");
            _btnPopupCancel.Style.Add("display", "none");

            switch (mode)
            {
                case MasterPagePopupDialogMode.ModalYesNo:
                    _btnPopupYes.Text = "Yes";
                    _btnPopupNo.Text = "No";
                    break;
                case MasterPagePopupDialogMode.ModalYesNoCancel:
                    _btnPopupCancel.Style.Add("display", "");
                    break;
                case MasterPagePopupDialogMode.ModalContinueCancel:
                    _btnPopupYes.Text = "Continue";
                    _btnPopupNo.Text = "Cancel";
                    break;
                case MasterPagePopupDialogMode.ModalOK:
                    _btnPopupYes.Text = "OK";
                    _btnPopupNo.Style.Add("display", "none");
                    break;
                case MasterPagePopupDialogMode.ModalCancel:
                    _btnPopupYes.Style.Add("display", "none");
                    _btnPopupNo.Text = "Cancel";
                    break;
                case MasterPagePopupDialogMode.ModalOkCancel:
                    _btnPopupYes.Text = "OK";
                    _btnPopupNo.Text = "Cancel";
                    break;

                //case MasterPagePopupDialogMode.ModalNoButtons:
                //    btnPopupOK.Style.Add("display", "none");
                //    btnPopupCancel.Style.Add("display", "none");
                //    break;
            }

            if (dataItem != null)
            {
                _btnPopupYes.SetDataItem(dataItem);
                _btnPopupNo.SetDataItem(dataItem);
            }

            _btnPopupYes.CausesValidation = causesValidation;
            _btnPopupNo.CausesValidation = causesValidation;
            _btnPopupCancel.CausesValidation = causesValidation;

            _lblPopupDialog.Text = message.Trim();
            //_upPopupDialog.Visible = true;
            //_upPopupDialog.Update();
            //_modalPopupDialog.Show();
            _modalPopupDialog.Width = width;
            _modalPopupDialog.Show();
            _popupVisible = true;
        }

        public void HidePopupDialog()
        {
            _modalPopupDialog.Hide();
        }

        public void ShowError(Exception ex)
        {
            ShowError(UIUtils.BuildErrorMessage(ex));
        }
        public void ShowError(string errorMessage)
        {
            _lblErrorBox.Text = errorMessage.Trim();
            _upErrorBox.Visible = true;
            _divErrorBoxContainer.Style["visibility"] = "visible";
        }


        public void ShowJavaScriptAlert(string popupMsg)
        {
            _upFeedback.ContentTemplateContainer.Controls.Add(new LiteralControl("<script type='text/javascript'>alert('" + popupMsg + "');</script>"));
        }
        
        #endregion

        #region EVENT HANDLING
        private void MenuItem_Click(object sender, MenuEventArgs e)
        {
            Menu menu = (Menu)sender;
            string url = menu.SelectedValue;
            MasterPagePostBackSource s = (menu.StaticMenuItemStyle.CssClass == "sideBarMenuLink") ? MasterPagePostBackSource.SideBar : MasterPagePostBackSource.SubMenu;
            if (HandlePostbackEvent(sender, e, s, url))
                Response.Redirect(url, false);
        }
        private void TabMenu_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            MasterPagePostBackSource s = (btn.CommandName == "TabClick") ? MasterPagePostBackSource.Tab : MasterPagePostBackSource.SubMenu;
            if (HandlePostbackEvent(sender, e, s, btn.CommandArgument))
                Response.Redirect(btn.CommandArgument, false);
        }
        protected void FooterNav_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            bool isFooterNav = (btn.CommandName == "PreviousClick" || btn.CommandName == "NextClick");
            string url = "#";
            if (isFooterNav)
            {
                try
                {
                    bool movePrevious = (btn.CommandName == "PreviousClick");

                    if (_selectedItemIndex > -1 && _selectedItem is MasterPageTabItem)
                    {
                        try
                        {
                            MasterPageTabItem selected = (MasterPageTabItem)_selectedItem;
                            MasterPageTabItem[] menuItems;
                            if (selected.Parent is MasterPageTab)
                                menuItems = ((MasterPageTab)selected.Parent).Items.ToArray();
                            else
                                menuItems = ((MasterPageTabItem)selected.Parent).Items.ToArray();

                            if (movePrevious)
                            {
                                for (int i = _selectedItemIndex - 1; i >= 0; i--)
                                {
                                    MasterPageTabItem item = menuItems[i];
                                    if (item.Enabled && item.Visible)
                                    {
                                        url = item.Url;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                for (int i = _selectedItemIndex + 1; i < menuItems.Count(); i++)
                                {
                                    MasterPageTabItem item = menuItems[i];
                                    if (item.Enabled && item.Visible)
                                    {
                                        url = item.Url;
                                        break;
                                    }
                                }
                            }
                        }
                        catch { }
                    }
                    
                    if (url == "#")
                    {
                        SiteMapNode node = null;
                        if (movePrevious)
                            node = SiteMapProvider.CurrentNode.PreviousSibling;
                        else
                            node = SiteMapProvider.CurrentNode.NextSibling;
                        if (node != null)
                            url = node.Url;
                    }

                    if (url == "#")
                    {
                        ShowError("Could not handle command " + btn.CommandName + ". No url specified to redirect to.");
                        return;
                    }
                    
                    url = AddUrlParams(url);
                }
                catch { }
            }

            bool redir = HandlePostbackEvent(sender, e, MasterPagePostBackSource.NavButton, url);

            if (isFooterNav && redir)
                Response.Redirect(url, false);
        }
        protected void PopupDialog_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            MasterPagePostBackSource s = (btn == _btnPopupYes) ? MasterPagePostBackSource.PopupYes : MasterPagePostBackSource.PopupNo;
            if (s == MasterPagePostBackSource.PopupYes && PopupYesClick != null)
                PopupYesClick(sender, new EventArgs());
            else if (s == MasterPagePostBackSource.PopupNo && PopupNoClick != null)
                PopupNoClick(sender, new EventArgs());
            
            _modalPopupDialog.Hide();

            bool redir = HandlePostbackEvent(sender, e, s, "#");
        }
        private bool HandlePostbackEvent(object sender, EventArgs e, MasterPagePostBackSource s, string redirUrl)
        {
            //_masterPagePostback = true;

            try
            {
                if (PostBackEvent != null)
                    return PostBackEvent(sender, new MasterPagePostBackEventArgs(redirUrl, e, s));
                else
                    return true;
            }
            catch (Exception ex)
            {
                ShowError(ex);
                return false;
            }
        }
        #endregion


    }
    
}