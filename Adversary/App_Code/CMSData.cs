﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using HH.Utils;
using System.Configuration;

/// <summary>
/// Handles all calls to CMS
/// </summary>
public class CMSData
{
    public const string PARTY_STATUS_DESCRIPTION = "description";
    public const string PARTY_STATUS_CODE = "status_code";
    public const string PARTY_STATUS_PLUS_CODE = "status_plus";

    public const string OFFICE_DESCRIPTION = "offc_desc";
    public const string OFFICE_CODE = "offc_code";

    public const string PRACTICE_TYPES_DESCRIPTION = "matt_cat_desc";
    public const string PRACTICE_TYPES_CODE = "matt_cat_code";
    public const string PRACTICE_TYPES_DESCRIPTION_PLUS_CODE = "desc_plus";

    public const string CLIENT_NAME = "CLIENT_NAME";
    public const string CLIENT_CODE = "CLIENT_CODE";

    public const string EMP_NAME = "employee_name";
    public const string EMP_CODE = "employee_code";
    public const string EMP_LOGIN = "login";

    private static string _connectionString;
    public static string CMSConnectionString
    {
        get
        {
            //return Config.GetConnectionString("HHCMSDB");
            return Config.GetConnectionString(ConfigurationManager.AppSettings["CMSDBKey"]);
        }
    }
    
    private static DataTable _partyStatus = null;
    public DataTable PartyStatus
    {
        get
        {
            if (_partyStatus == null)
            {
                DataTable dt = LoadData("USP_ADV_GET_PARTY_STATUS");
                dt.Columns.Add(new DataColumn(PARTY_STATUS_PLUS_CODE));
                foreach (DataRow dr in dt.Rows)
                    dr[PARTY_STATUS_PLUS_CODE] = dr[PARTY_STATUS_CODE] + " " + dr[PARTY_STATUS_DESCRIPTION];
                
                _partyStatus = dt;
            }
            return _partyStatus;
        }
    }

    private static DataTable _offices = null;
    public DataTable AllOffices
    {
        get
        {
            if (_offices == null)
            {
                _offices = LoadData("USP_ADV_GET_ORIG_OFFICE");
                //there is a trailing space at the end of each office code in CMS, get rid of it here.
                foreach (DataRow row in _offices.Rows)
                    row[OFFICE_CODE] = row[OFFICE_CODE].ToString().Trim();
            }
            return _offices;
        }
    }

    private static DataTable _pracTypes = null;
    public DataTable PracticeTypes
    {
        get
        {
            if (_pracTypes == null)
            {
                DataTable dt = LoadData("USP_ADV_GET_PRACTICE_CODES");

                //add extra description and code combo column
                dt.Columns.Add(new DataColumn(PRACTICE_TYPES_DESCRIPTION_PLUS_CODE));
                foreach (DataRow dr in dt.Rows)
                    dr[PRACTICE_TYPES_DESCRIPTION_PLUS_CODE] = dr[PRACTICE_TYPES_CODE] + " " + dr[PRACTICE_TYPES_DESCRIPTION];
                _pracTypes = dt;
            }
            return _pracTypes;
        }
    }

    
    private CMSData() { }

    public static CMSData GetInstance()
    {
        _connectionString = CMSConnectionString;
        return new CMSData();
    }

    public DataTable DoEmployeeSearch(string searchCriteria)
    {
        SqlParameter p = new SqlParameter("search", searchCriteria);
        return LoadData("USP_ADV_EMP_SEARCH", new SqlParameter[] { p });
    }

    public List<Address> DoClientAddressLookup(string clientCode)
    {
        List<Address> addresses = new List<Address>();
        SqlParameter p1 = new SqlParameter(CLIENT_CODE, clientCode.Trim());
        DataTable dt = LoadData("USP_ADV_GET_CLIENT_ADDRESSES", new SqlParameter[] { p1 });
        foreach (DataRow dr in dt.Rows)
            addresses.Add(BuildAddress(dr));
        return addresses;
    }
    private Address BuildAddress(DataRow addressRow)
    {
        Address address = new Address();
        //address.AddressID = 0;

        if (addressRow["ADDRESS1"] != null)
            address.Address1 = addressRow["ADDRESS1"].ToString().Trim();
        if (addressRow["ADDRESS2"] != null)
            address.Address2 = addressRow["ADDRESS2"].ToString().Trim();
        if (addressRow["ADDRESS3"] != null)
            address.Address3 = addressRow["ADDRESS3"].ToString().Trim();
        //if (addressRow["ADDRESS4"] != null)
        //    address.Address4 = addressRow["ADDRESS4"].ToString().Trim();
        if (addressRow["CITY"] != null)
            address.City = addressRow["CITY"].ToString().Trim();
        if (addressRow["STATE_CODE"] != null)
            address.State = addressRow["STATE_CODE"].ToString().Trim();
        if (addressRow["COUNTRY_CODE"] != null)
            address.Country = addressRow["COUNTRY_CODE"].ToString().Trim();
        if (addressRow["POST_CODE"] != null)
            address.Zip = addressRow["POST_CODE"].ToString().Trim();
        if (addressRow["PHONE_NUMBER"] != null)
            address.Phone = addressRow["PHONE_NUMBER"].ToString().Trim();
        if (addressRow["PHONE_EXT_NUM"] != null && !string.IsNullOrEmpty(addressRow["PHONE_EXT_NUM"].ToString().Trim()))
            address.Phone += " ext. " + addressRow["PHONE_EXT_NUM"].ToString().Trim();

        return address;
    }

    public Address GetAddress(string addressUno)
    {
        SqlParameter p1 = new SqlParameter("address_uno", addressUno.Trim());
        DataTable dt = LoadData("USP_ADV_GET_SINGLE_ADDRESS", new SqlParameter[] { p1 });

        if (dt.Rows.Count > 0)
            throw new Exception("More than one address found for address_uno " + addressUno);

        return BuildAddress(dt.Rows[0]);
    }
    

    public DataTable DoClientSearch(string clientName, string clientCode)
    {
        object val1 = clientName;
        object val2 = clientCode;

        if (string.IsNullOrEmpty(clientName))
            val1 = DBNull.Value;
        if (string.IsNullOrEmpty(clientCode))
            val2 = DBNull.Value;

        SqlParameter p1 = new SqlParameter("CLIENT_NAME", val1);
        SqlParameter p2 = new SqlParameter("CLIENT_CODE", val2);
        return LoadData("USP_ADV_CLIENT_SEARCH", new SqlParameter[] { p1, p2 });
    }

    private static Dictionary<string, string> _clientCache = new Dictionary<string, string>();
    public string GetClientName(string clientCode)
    {
        if (_clientCache.ContainsKey(clientCode))
            return _clientCache[clientCode];

        DataTable clients = DoClientSearch(null, clientCode.Trim());
        var query = from c in clients.AsEnumerable()
                    where c.Field<string>(CLIENT_CODE).Trim() == clientCode.Trim()
                    select c.Field<string>(CLIENT_NAME);
        if (query.Count() == 0)
            throw new Exception("A client with " + CLIENT_CODE + " " + clientCode + " could not be found");

        string client = query.Single<string>();
        _clientCache.Add(clientCode, client);
        return client;
    }

    public DataTable DoUserSearch(string searchCriteria)
    {
        SqlParameter p1 = new SqlParameter("search", searchCriteria);
        //return LoadData("USP_ADV_GET_USER_BY_LOGIN", new SqlParameter[] { p1 });
        return LoadData("USP_ADV_SEARCH_USERS", new SqlParameter[] { p1 });
    }

    public DataTable DoBillerLookup(string employeeId)
    {
        SqlParameter p1 = new SqlParameter("code", employeeId);
        return LoadData("USP_ADV_BILLER_LOOKUP", new SqlParameter[] { p1 });
    }
    public string DoBillerEmailLookup(string employeeId)
    {
        DataTable dt = DoBillerLookup(employeeId);
        if (dt.Rows.Count == 0)
            return string.Empty;
        string empCode = dt.Rows[0][EMP_CODE].ToString();
        FirmDirectory fd = new FirmDirectory();
        return fd.GetEmployeeEmailAddress(empCode);
    }

    public DataTable GetUserDataByLogin(string login)
    {
        SqlParameter p1 = new SqlParameter("LOGIN", login.Trim());
        return LoadData("USP_ADV_GET_USER_BY_LOGIN", new SqlParameter[] { p1 });
    }

    public DataTable GetUserDataByCode(string employeeId)
    {
        if (string.IsNullOrEmpty(employeeId))
            throw new ArgumentException("The employee ID was null");

        SqlParameter p1 = new SqlParameter("CODE", employeeId.Trim());
        return LoadData("USP_ADV_GET_USER_BY_EMPLOYEE_CODE", new SqlParameter[] { p1 });
    }

    private static Dictionary<string, string> _userNameCache = new Dictionary<string, string>();
    public string GetUserNameByCode(string employeeId)
    {
        if (_userNameCache.ContainsKey(employeeId))
            return _userNameCache[employeeId];

        DataTable dt = GetUserDataByCode(employeeId);

        if (dt.Rows.Count != 1)
            throw new Exception("Zero or more than one matching record found for user with employee ID " + employeeId);

        string userName = dt.Rows[0][CMSData.EMP_NAME].ToString();
        _userNameCache.Add(employeeId, userName);
        return userName;
    }

    private static Dictionary<string, string> _userCodeCache = new Dictionary<string, string>();
    public string GetUserCodeByLogin(string login)
    {
        if (_userCodeCache.ContainsKey(login))
            return _userCodeCache[login];

        DataTable dt = GetUserDataByLogin(login);

        if (dt.Rows.Count != 1)
            throw new Exception("Zero or more than one matching record found for user with login " + login);

        string code = dt.Rows[0][CMSData.EMP_CODE].ToString();
        _userCodeCache.Add(login, code);
        return code;
    }

    public string GetPartyStatusName(string statusCode)
    {
        var query = from p in PartyStatus.AsEnumerable()
                    where p.Field<string>(PARTY_STATUS_CODE) == statusCode.Trim()
                    select p.Field<string>(PARTY_STATUS_DESCRIPTION);
        if (query.Count() == 0)
            throw new Exception("A party status with " + PARTY_STATUS_CODE + " " + statusCode + " could not be found");

        return query.Single<string>();
    }

    public string GetOfficeName(string officeCode)
    {
        if (string.IsNullOrEmpty(officeCode))
            throw new ArgumentException("Could not retrieve office name, office code was not specified");

        var query = from o in AllOffices.AsEnumerable()
                    where o.Field<string>(OFFICE_CODE).ToLower() == officeCode.Trim().ToLower()
                    select o;
        if (query.Count() == 0)
            throw new Exception("An office with " + OFFICE_CODE + " " + officeCode + " could not be found");

        return query.FirstOrDefault().Field<string>(OFFICE_DESCRIPTION);
    }

    public string GetOfficeCode(string officeDesc)
    {
        if (officeDesc == "DTC")
            officeDesc = "Tech Center";

        //added catch for Washington DC
        else if (officeDesc == "Washington DC")
            officeDesc = "Washington";

        var query = from o in AllOffices.AsEnumerable()
                    where o.Field<string>(OFFICE_DESCRIPTION).ToLower() == officeDesc.Trim().ToLower()
                    select o;
        if (query.Count() == 0)
            throw new Exception("An office with " + OFFICE_DESCRIPTION + " " + officeDesc + " could not be found");

        return query.FirstOrDefault().Field<string>(OFFICE_CODE);
    }

    public string GetPracticeName(string pracCode)
    {
        if (string.IsNullOrEmpty(pracCode))
            throw new ArgumentException("Could not get practice name, the practice code that was supplied was null");

        var query = from p in PracticeTypes.AsEnumerable()
                    where p.Field<string>(PRACTICE_TYPES_CODE).ToLower().Trim() == pracCode.ToLower().Trim()
                    select p;
        if (query.Count() == 0)
            return "**NO PRACTICE CODE DESCRIPTION AVAILABLE";
            //throw new Exception("A practice code with " + PRACTICE_TYPES_CODE + " " + pracCode + " could not be found");
        else
            return query.FirstOrDefault().Field<string>(PRACTICE_TYPES_DESCRIPTION);
    }


    private static DataTable LoadData(string procName) { return LoadData(procName, null); }
    private static DataTable LoadData(string procName, SqlParameter[] parameters)
    {
        DataTable dt = new DataTable();
        using (SqlConnection conn = new SqlConnection(_connectionString))
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(procName, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            if (parameters != null)
                cmd.Parameters.AddRange(parameters);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
        }
        return dt;
    }


    public decimal GetTotalAR(string clientNumber)
    {
        CMSDataWS.CMSData cms = new CMSDataWS.CMSData();
        DataSet ds = cms.AR_GetSumAR(clientNumber.Trim());

        decimal total = 0;
        if (ds != null)
            total = Decimal.Parse(ds.Tables[0].Rows[0]["TotalAR"].ToString());
        return total;
    }

    public DataTable GetAROverTime(string clientNumber)
    {
        CMSDataWS.CMSData cms = new CMSDataWS.CMSData();
        DataSet ds = cms.AR_GetBillInfo(clientNumber.Trim());
        if (ds == null)
            return null;

        return ds.Tables[0];
    }

    public ARBalance GetClientBill(string clientNumber)
    {
        ARBalance balance = new ARBalance();
        balance.TotalAR = GetTotalAR(clientNumber.Trim());
        DataTable dt = GetAROverTime(clientNumber.Trim());
        if (dt == null)
            return balance;

        foreach (DataRow dr in dt.Rows)
        {
            //set the total AR field.
            //strTotalAR = String.Format("{0:###,###,###,##0.00}", dr["TOTAL_AR"]);
            decimal ar = Decimal.Parse(dr["TOTAL_AR"].ToString());

            
            //fetch number of days difference between the bill and the current date
            TimeSpan span = DateTime.Now - DateTime.Parse(dr["BILL_DATE"].ToString()); //Utility.DaysDifference(dr["BILL_DATE"].ToString(), strNow);

            if (span.Days < 30)
                balance.Current += ar;
            else if ((31 <= span.Days) && (span.Days <= 60))
                balance.Days_31_to_60 += ar;
            else if ((61 <= span.Days) && (span.Days <= 90))
                balance.Days_61_to_90 += ar;
            else if ((91 <= span.Days) && (span.Days <= 120))
                balance.Days_91_to_120 += ar;
            else if (121 <= span.Days)
                balance.Days_121_Plus += ar;
            
        }

        return balance;
    }

}

public class ARBalance
{
    public decimal TotalAR { get; set; }
    public decimal Current { get; set; }
    public decimal Days_31_to_60 { get; set; }
    public decimal Days_61_to_90 { get; set; }
    public decimal Days_91_to_120 { get; set; }
    public decimal Days_121_Plus { get; set; }

    protected internal ARBalance()
    {
        TotalAR = 0;
        Current = 0;
        Days_31_to_60 = 0;
        Days_61_to_90 = 0;
        Days_91_to_120 = 0;
        Days_121_Plus = 0;
    }

    public bool Has90DayBalance()
    {
        return (Days_91_to_120 > 0 || Days_121_Plus > 0);
    }
}
