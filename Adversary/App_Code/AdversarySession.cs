﻿
/// <summary>
/// All session data encapsulated here
/// </summary>
public class AdversarySession
{
    public bool LoggedIn { get; set; }
    public string LoginEmpName { get; set; }
    public string LoginEmpCode { get; set; }

    private Requests _requests = null;
    public Requests AdversaryRequest
    {
        get
        {
            if (_requests == null)
                _requests = new Requests();
            return _requests;
        }
        set
        {
            _requests = value;
        }
    }

    private Memo _memo = null;
    public Memo MemoData
    {
        get
        {
            if (_memo == null)
                _memo = new Memo();
            return _memo;
        }
        set
        {
            _memo = value;
        }
    }

    public bool IsLoaded { get; set; }

    public AdminLogins AdminLogin { get; set; }

    public AdversarySession()
    {
        IsLoaded = false;
        LoggedIn = false;

    }
}


