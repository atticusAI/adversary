﻿using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using HH;
using HH.Branding;
using HH.SQL;
using System.Data.SqlClient;

/// <summary>
/// page class inherited by all screening aspx pages
/// </summary>
public class ScreeningBasePage : AjaxBasePage
{
    
    public ScreeningBasePage() { }

    /// <summary>
    /// Called prior to the page's Page_Init
    /// </summary>
    /// <param name="e"></param>
    protected override void OnInit(EventArgs e)
    {
        try
        {
            LoadPage = DoBaseInit();
            if (!LoadPage)
                return;


            LoadPage = HandleScreeningPageInit();
            if (LoadPage)
                base.OnInit(e);
        }
        catch (Exception ex)
        {
            HandleError(ex);
        }
    }

    /// <summary>
    /// Called prior to the page's Page_Load
    /// </summary>
    /// <param name="e"></param>
    protected override void OnLoad(EventArgs e)
    {
        if (LoadPage)
            base.OnLoad(e);
    }

    /// <summary>
    /// Handles masterpage config, specifies the correct sitemapprovider, and binds the DAL to the page's Screen control
    /// TODO: too much logic needed to be added to this method over time as the site grew, figure out a way to clean up this mess
    /// </summary>
    /// <returns>False if the page's Page_init method should not be called</returns>
    private bool HandleScreeningPageInit()
    {
        _MasterPage.Screen.ShowInformationToolTips = false;
        bool isPrintPage = Request.FilePath.ToLower().Contains("printview.aspx");

        //if print page turn some controls off and get out of here
        if (isPrintPage)
        {
            _MasterPage.TabMenuVisible = false;
            _MasterPage.SideBarVisible = false;
            _MasterPage.FooterVisible = false;
            if (!_SessionData.MemoLoaded) //send the user back to the screening memo home if the session is dead
            {
                JSAlertAndRedirect("A memo has not been selected for edit. Please create a new memo or search for an existing one to continue.", "/Screening/Default.aspx");
                return false;
            }
            return true;
        }

        string screenId = ParseURLForScreenID(Request.Path);
        bool isHomeTab = _MasterPage.SelectedTabTitle == "Screening Home";
        
        //TODO: Need a better way to handle setting the confirm dialog flag
        if (_MasterPage.SiteMapProvider.Name == "Screening")
        {
            foreach (MasterPageTab tab in _MasterPage.Tabs)
            {
                try
                {
                    if (tab.Title == "Screening Home")
                    {
                        tab.Enabled = true;
                        tab.ConfirmRedirect = true;
                        foreach (MasterPageTabItem item in tab.Items)
                            item.ConfirmRedirect = !isHomeTab;
                    }
                    else
                    {
                        tab.Enabled = (_MasterPage.SelectedTabTitle == tab.Title);
                    }
                }
                catch { }

                try
                {
                    ///commented by A Reimer 03/21/12 for 'Rejected to Modification Required' change
                    //if (tab.Selected && _SessionData.MemoLoaded && _SessionData.MemoData.DocketingInfoRequired() && tab.Title != "Screening Home" && tab.Title != "Rejected Matter")

                    // blmcgee@hollandhart.com 2014-01-13
                    // if (tab.Selected && _SessionData.MemoLoaded && _SessionData.MemoData.DocketingInfoRequired() && tab.Title != "Screening Home" && tab.Title != "Matter Requiring Modification")

                    if (tab.Selected && _SessionData.MemoLoaded && _SessionData.MemoData.DocketingInfoRequired() && tab.Title != "Screening Home" && tab.Title != "Rejected Matter")
                        tab.Items["Docketing"].Visible = true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        else if (!_SessionData.MemoLoaded)
        {
            try
            {
                _MasterPage.Tabs[3].Items[2].Enabled = false;
                _MasterPage.Tabs[3].Items[3].Enabled = false;
                _MasterPage.Tabs[3].Items[4].Enabled = false;
            }
            catch { }
        }

        bool isSummaryPage = Request.FilePath.ToLower().Contains("summary.aspx");
        bool isDefault = Request.FilePath.ToLower().Contains("/screening/default.aspx");
        bool isDownloadPage = Request.FilePath.ToLower().Contains("/screening/download.aspx");
        bool isSearch = Request.FilePath.ToLower().Contains("/screening/search");
        bool isSubmitPage = Request.FilePath.ToLower().Contains("/screening/submit.aspx");
        
        if (_SessionData.MemoLoaded && !isSearch && !isSubmitPage && !isDownloadPage && !isDefault)
            CheckAddApprovalPageLink();

        //If on the home tab, we're done
        if (isHomeTab || _MasterPage.SelectedTabTitle == "Approval")
        {
            //_SessionData.MemoData = null;
            //_SessionData.MemoLoaded = false;
            if (_SessionData.MemoData.Tracking.StatusTypeID.HasValue &&
                _SessionData.MemoData.Tracking.StatusTypeID == TrackingStatusType.Completed &&
                isSummaryPage)
            {
                _MasterPage.FeedbackMessage = "This memo has been completed";
                _MasterPage.FeedbackMode = MasterPageFeedbackMode.Confirm;
            }
            return true;
        }


        if (!_SessionData.MemoLoaded && !isHomeTab && !isDefault && !isSearch) //send the user back to the screening memo home if the session is dead
        {
            JSAlertAndRedirect("A memo has not been selected for edit. Please create a new memo or search for an existing one to continue.", "/Screening/Default.aspx");
            return false;
        }

        if (isDownloadPage)
            return true;

        if (_SessionData.MemoLoaded && 
            !isDefault &&
            !isSearch &&
            ((Request.Params["ScrMemType"] == null) || 
             (Request.Params["ScrMemType"] != null && 
              Request.Params["ScrMemType"] != _SessionData.MemoData.ScrMemType.ToString())))
        {
            Response.Redirect(Request.Url.LocalPath + "?ScrMemID=" + _SessionData.MemoData.ScrMemID + "&ScrMemType=" + _SessionData.MemoData.ScrMemType, false);
            return false;
        }

        if (_SessionData.MemoLoaded && 
            _SessionData.MemoData.Tracking.StatusTypeID.HasValue &&
            _SessionData.MemoData.Tracking.StatusTypeID != TrackingStatusType.None && 
            !isSubmitPage &&
            !isSearch)
            ShowStatus();

        _MasterPage.PostBackEvent += new MasterPagePostBackEventHandler(Screening_PostBackEvent);
        
        TrackingStatusType? status = _SessionData.MemoData.Tracking.StatusTypeID;
        bool locked = _SessionData.MemoData.IsLocked();
        if ((status == TrackingStatusType.Completed) || locked)
        {
            Label lblLocked = null;

            bool cmNumsSent = (_SessionData.MemoData.Tracking.CMNumbersSentDate.HasValue && _SessionData.MemoData.Tracking.CMNumbersSentDate.Value > DateTime.MinValue);
            if (locked && status != TrackingStatusType.Completed)
            {
                lblLocked = new Label();
                if (cmNumsSent)
                    lblLocked.Text = "Client matter numbers have been issued for this memo, the memo is locked and cannot be edited.<br/><br/>";
                else if (_SessionData.MemoData.ScrMemID < 100000 && _SessionData.MemoData.ScrMemID != 0)
                    lblLocked.Text = "This memo is archived and cannot be edited";
                else
                    lblLocked.Text = "This screening memo is currently locked and cannot be edited.";
                lblLocked.Font.Bold = true;
                lblLocked.ForeColor = Color.Blue;
            }

            if (!isPrintPage && _MasterPage.SiteMapProvider.Name == "Screening" && _MasterPage.SelectedTab != null)
            {
                MasterPageTab tab = _MasterPage.SelectedTab;
                for (int i = 1; i < tab.Items.Count; i++)
                    tab.Items[i].Enabled = false;
            }

            if (!isSummaryPage &&   
                !isSubmitPage &&
                !isSearch)
            {
                _MasterPage.UpperContent.Controls.Clear();
                _MasterPage.LowerContent.Controls.Clear();
                if (lblLocked != null)
                    _MasterPage.UpperContent.Controls.Add(lblLocked);
                return false;
            }
            else
            {
                if (lblLocked != null)
                    _MasterPage.UpperContent.Controls.AddAt(0, lblLocked);
                return true;
            }
        }

        if (_SessionData.MemoLoaded)
        {
            try
            {
                if (!isSearch)
                    _MasterPage.BreadCrumbText = _SessionData.MemoData.GetMemoTypeDescription();
                if (_MasterPage.SiteMapProvider.CurrentNode != null)
                    _MasterPage.BreadCrumbText += " > " + _MasterPage.SiteMapProvider.CurrentNode.Description;
            }
            catch { }
        }

        if (_SessionData.MemoLoaded && !isSummaryPage)
        {
            
            _MasterPage.Screen.ShowInformationToolTips = true;
            _MasterPage.FooterNavVisible = true;

            //Add pages that do not bind the screen control here
            string filePath = Request.FilePath.ToLower();
            if (filePath.Contains("legalwork.aspx") ||
                filePath.Contains("docketing.aspx"))
                return true;

            _MasterPage.Screen.ConnectionString = AdversaryData.ConnectionString;
            _MasterPage.Screen.ScreenID = screenId;
            _MasterPage.Screen.DataSource = _SessionData.MemoData;
            _MasterPage.Screen.ValidationEnabled = true;
            _MasterPage.Screen.DataBind();
            _MasterPage.PopupYesClick += new EventHandler(_MasterPage_PopupYesClick);
            return true;
        }

        return isSearch || isDefault || _SessionData.MemoLoaded;
    }

    /// <summary>
    /// Adds a link to the approval screen at the top right corner of the page if the user has AP, PGl or Adversary roles
    /// </summary>
    private void CheckAddApprovalPageLink()
    {
        try
        {
            if ((_SessionData.MemoLoaded) &&
                (_SessionData.MemoData.Tracking.TrackingID.HasValue && _SessionData.MemoData.Tracking.TrackingID > 0) &&
                ((_SessionData.AdminLogin.IsAP.HasValue && _SessionData.AdminLogin.IsAP.Value) ||
                 (_SessionData.AdminLogin.IsPGM.HasValue && _SessionData.AdminLogin.IsPGM.Value) ||
                 (_SessionData.AdminLogin.IsAdversary.HasValue && _SessionData.AdminLogin.IsAdversary.Value)))
            {
                string url = GetBaseUrl() + "Admin/Approval.aspx";
                LinkButton lbApprovalRedirectLink = new LinkButton();
                lbApprovalRedirectLink.ID = "lbApprovalRedirectLink";
                lbApprovalRedirectLink.Text = "Go to the Approval Page for this Memo";
                lbApprovalRedirectLink.Font.Bold = true;
                lbApprovalRedirectLink.Font.Size = FontUnit.Larger;
                lbApprovalRedirectLink.Click += new EventHandler(lbApprovalRedirectLink_Click);
                lbApprovalRedirectLink.CommandArgument = url;
                _MasterPage.HeaderContent.Controls.Add(lbApprovalRedirectLink);
                _MasterPage.HeaderContent.Controls.Add(new LiteralControl("<br/>"));

                if (_MasterPage.SelectedTabTitle == "Approval")
                    return;

                MasterPageTabItem item = new MasterPageTabItem(_MasterPage.Tabs["Screening Home"], "Approval Page");
                item.Url = url;
                _MasterPage.Tabs["Screening Home"].Items.Insert(2, item);
            }
        }
        catch { }
    }
    private void lbApprovalRedirectLink_Click(object sender, EventArgs e)
    {
        LinkButton lb = (LinkButton)sender;
        Response.Redirect(lb.CommandArgument);
    }

    /// <summary>
    /// Checks if a valid attorney and entering employee was entered on the screening memo
    /// </summary>
    /// <returns></returns>
    public bool CheckBasicInfoValid()
    {
        ValidationException vex = new ValidationException("Please correct the following issues to continue");

        if (string.IsNullOrEmpty(_SessionData.MemoData.AttEntering))
            vex.AddInvalidReason("Attorney Employee Number");
        if (string.IsNullOrEmpty(_SessionData.MemoData.PersonnelID))
            vex.AddInvalidReason("Entering Employee Number");
        if (vex.HasInvalidReasons())
        {
            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Warning;
            _MasterPage.FeedbackMessage = vex.GetInvalidMessage();
            if (!Request.Path.ToLower().Contains("basicinfo.aspx"))
                Response.Redirect("BasicInfo.aspx?ScrMemType=" + _SessionData.MemoData.ScrMemType, false);
            return false;
        }
        return true;
    }

    /// <summary>
    /// Handles the masterpage's postback, typically will commit the memo to the DB
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// <returns></returns>
    public bool Screening_PostBackEvent(object sender, MasterPagePostBackEventArgs e)
    {
        
        try
        {
            if (e.PostBackSource == MasterPagePostBackSource.Tab)
                return true;
            if (!_SessionData.MemoLoaded)
                return true;
            if (_SessionData.MemoData.IsLocked())
                return true;
            if (_MasterPage.Screen.DataSource == null)
                return true;

            _SessionData.MemoData = (Memo)_MasterPage.Screen.DataSource;
            //error validation
            if (_SessionData.MemoData.ScrMemID == null)
            {
                JSAlertAndRedirect("An unexpected error occurred", "/Screening/Default.aspx");
                return false;
            }

            if (!_MasterPage.Screen.IsValid)
            {
                _MasterPage.FeedbackMode = MasterPageFeedbackMode.Error;
                _MasterPage.FeedbackMessage = "One or more entries was invalid, please correct to continue";
                return false;
            }

            if (_SessionData.MemoData.ScrMemID == 0)
            {
                //do not validate if redirecting to these pages
                if (e.RedirectUrl.ToLower().Contains("summary.aspx") ||
                    e.RedirectUrl.ToLower().Contains("default.aspx"))
                    return true;

                if (!CheckBasicInfoValid())
                    return false;
            }

            string screenId = _MasterPage.Screen.ScreenID;
            using (SqlConnection conn = AdversaryData.GetConnection())
            {
                DatabaseWorker worker = new DatabaseWorker(conn);
                //passed validation so do the save
                ///TODO: Fix this save and move it to the db worker. Should be one call to DoDataSave()
                if (_SessionData.MemoData.ScrMemID == 0)
                {
                    DoDataSave(_SessionData.MemoData, worker);
                    return true;
                }

                if (_SessionData.MemoData.Address.AddressID != null && !string.IsNullOrEmpty(screenId) && screenId.ToLower().Contains("clientaddress.aspx"))
                    DoDataSave(_SessionData.MemoData.Address, worker);
                if (_SessionData.MemoData.Billing.BillingID != null && !string.IsNullOrEmpty(screenId) && screenId.ToLower().Contains("billinginfo.aspx"))
                    DoDataSave(_SessionData.MemoData.Billing, worker);
                try
                {
                    //error occurring here
                    if (_SessionData.MemoData.NewClient.NwClientID != null && !string.IsNullOrEmpty(screenId) && (screenId.ToLower().Contains("referringparty.aspx?scrmemtype=1") || screenId.ToLower().Contains("clientdetails.aspx")))
                        DoDataSave(_SessionData.MemoData.NewClient, worker);
                }
                catch (Exception ex)
                {
                    _MasterPage.FeedbackMessage = "An error occurred when saving the client information. IT has been notified.";
                    _MasterPage.FeedbackMode = MasterPageFeedbackMode.Error;
                    HandleError(ex);
                }

                if (!string.IsNullOrEmpty(screenId) && screenId.ToLower().Contains("contact.aspx"))
                {
                    foreach (ClientContact contact in _SessionData.MemoData.ClientContacts)
                        DoDataSave(contact, worker);
                }

                if (_SessionData.MemoData.ReferringParty.PartyID != null && !string.IsNullOrEmpty(screenId) && screenId.ToLower().Contains("referringparty.aspx"))
                {
                    Party party = _SessionData.MemoData.ReferringParty;
                    PartyAddressInfo address = party.PartyAddressInfo;
                    party.ScrMemID = _SessionData.MemoData.ScrMemID;
                    DoDataSave(party, worker);

                    if (party.PartyID != null)
                    {
                        if (address.PartyAddressID == null)
                            address.PartyAddressID = 0;
                        address.PartyID = party.PartyID;
                        DoDataSave(address, worker);
                        party.PartyAddressInfo = address;
                    }
                }


                if (!string.IsNullOrEmpty(screenId) && screenId.ToLower().Contains("opposing.aspx"))
                {
                    foreach (OpposingCounsel counsel in _SessionData.MemoData.OpposingCounsels)
                        DoDataSave(counsel, worker);
                }

                DoDataSave(_SessionData.MemoData, worker);
            }

            if ((!_SessionData.MemoData.Tracking.StatusTypeID.HasValue || _SessionData.MemoData.Tracking.StatusTypeID < TrackingStatusType.Submitted) &&
                (e.RedirectUrl.ToLower().Contains("search.aspx") || e.RedirectUrl.ToLower().Contains("default.aspx")))
            {
                _MasterPage.ShowPopupDialog("You are about to be redirected away from the current screening memo. Would you like to continue?", HH.Branding.MasterPagePopupDialogMode.ModalContinueCancel);
                ViewState["redirectUrl"] = e.RedirectUrl;
                return false;
            }
        }
        catch (Exception ex)
        {
            if (_DebugMode)
            {
                _MasterPage.ShowError(ex);
            }
            else
            {
                _MasterPage.FeedbackMessage = "Sorry, an error occurred while attempting to save your form data to the database.";
                _MasterPage.FeedbackMode = MasterPageFeedbackMode.Error;
                HandleError(ex);
            }
            return false;
        }

        return true;
    }

    void _MasterPage_PopupYesClick(object sender, EventArgs e)
    {
        if (ViewState["redirectUrl"] != null)
            Response.Redirect(ViewState["redirectUrl"].ToString(), true);
    }

}
