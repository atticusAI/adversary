
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq.Mapping;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI.WebControls;
using HH.Screen;
using HH.Utils;

using HH.SQL;
using System.Configuration;

public enum TrackingStatusType
{
    None=0,
    Submitted=1,
    //Acknowledged = 2,
    //APSubmitted=1,
    //APAcknowledged=2,
    //APApproved = 3,
    //PGMSubmitted = 3,
    //PGMAcknowledged = 4,
    //PGMApproved = 5,
    //AdversaryGroupSubmitted = 2,
    //AdversaryGroupAcknowledged = 6,
    AdversaryGroupProcessing = 3,
    //AdversaryGroupProcessing = 8,
    Completed = 100,
    //commented out by A Reimer 03/22/12 for "Rejected to Modification Requested" project
    //Rejected = 200
    Rejected = 600,
    ModificationRequested = 200
    //PGMRejected = 201,
    //AdversaryGroupRejected = 201
}

/// <summary>
/// Summary description for ScreeningMemo
/// This class file was created using the HH DataObjectBuilder
/// </summary>
[Table(Name="Tracking")]
public class Tracking
{
    public static readonly string STATUS_ID = "StatusTypeID";
    public static readonly string STATUS_DESCRIPTION = "Status";

    private static DataTable _statusTypes = null;
    public static DataTable StatusTypes 
    {
        get
        {
            if (_statusTypes == null)
            {
                //changed by A Reimer 12/09/2016 for Associated Memo - piggybacking the change to use the cmgr service
                //using (SqlConnection conn = new SqlConnection(Config.GetConnectionString("AdversaryDB")))
                using (SqlConnection conn = new SqlConnection(Config.GetConnectionString(ConfigurationManager.AppSettings["AdversaryDBKey"])))
                {
                    DatabaseWorker worker = new DatabaseWorker(conn);
                    _statusTypes = worker.RetrieveToDataTable("PR_GET_TrackingStatusTypes");
                    conn.Close();
                }
            }
            return _statusTypes;
        }
    }

    

	/// <summary>
	/// DB Column: [Adversary].[Tracking].[TrackingID] (Int)
	/// Nullable: False
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: True
	/// </summary>
	[Column(Storage = "TrackingID", DbType = "Int(4) NOT NULL IDENTITY", IsDbGenerated = true, AutoSync = AutoSync.Always, IsPrimaryKey = true)]
	public int? TrackingID { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Tracking].[ScrMemID] (Int)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: True
	/// Identity: False
	/// </summary>
	[Column(Storage="ScrMemID", DbType="Int(4)")]
	public int? ScrMemID { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Tracking].[Status] (Int)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// </summary>
	[Column(Storage = "StatusTypeID", DbType = "Int(4)")]
    public TrackingStatusType? StatusTypeID { get; set; }

    [Column(Storage="Locked", DbType="Bit")]
	public bool? Locked { get; set; }

    [Column(Storage = "RejectionHold", DbType = "Bit")]
    public bool? RejectionHold { get; set; }

    [Column(Storage = "SubmittedOn", DbType = "SmallDateTime(4)")]
    public DateTime? SubmittedOn { get; set; }
    
    [Column(Storage = "Notes", DbType = "VarChar(MAX)")]
    public string Notes { get; set; }


    [Column(Storage = "RetainerAcknowledged", DbType = "Bit")]
    public bool? RetainerAcknowledged { get; set; }

    [Column(Storage = "APAcknowledged", DbType = "Bit")]
    public bool? APAcknowledged { get; set; }

    [Column(Storage = "APApproved", DbType = "Bit")]
    public bool? APApproved { get; set; }

	/// <summary>
	/// DB Column: [Adversary].[Tracking].[APSignature] (VarChar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// </summary>
	[ScreenBuilder(
     Caption = "Approved By",
     FieldOrder = 1,
	 BuilderType = BuilderType.Label,
	 ScreenIDs = new string[] { "AP" })]
	[Column(Storage="APSignature", DbType="VarChar(50)")]
	public string APSignature { get; set; }


    /// <summary>
    /// DB Column: [Adversary].[Tracking].[APDate] (SmallDateTime)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// </summary>
    [ScreenBuilder(
     Caption = "Approval Date",
     FieldOrder = 2,
     BuilderType = BuilderType.Label,
     ScreenIDs = new string[] { "AP" })]
    [Column(Storage = "APDate", DbType = "SmallDateTime(4)")]
    public DateTime? APDate { get; set; }


    /// <summary>
    /// DB Column: [Adversary].[Tracking].[APARSignature] (VarChar)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// </summary>
    [ScreenBuilder(
     Caption = "Approval Signature for A/R over 90 Days",
     FieldOrder=3,
     BuilderType = BuilderType.TextBox,
     ScreenIDs = new string[] { "AP" })]
    [Column(Storage = "APARSignature", DbType = "VarChar(50)")]
    public string APARSignature { get; set; }

    /// <summary>
    /// DB Column: [Adversary].[Tracking].[APExceptionSignature] (VarChar)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// </summary>
    [ScreenBuilder(
     Caption = "Exception approval for signed engagement letter",
     FieldOrder=4,
     BuilderType = BuilderType.TextBox,
     ScreenIDs = new string[] { "AP" })]
    [Column(Storage = "APExceptionSignature", DbType = "VarChar(50)")]
    public string APExceptionSignature { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Tracking].[APNotes] (VarChar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// </summary>
	[ScreenBuilder(
	 Caption = "Additional Notes",
	 BuilderType = BuilderType.TextArea,
     FieldOrder=5,
     ScreenIDs = new string[] { "AP" })]
	[Column(Storage="APNotes", DbType="VarChar(MAX)")]
	public string APNotes { get; set; }


    [Column(Storage = "APRejectionReason", DbType = "VarChar(MAX)")]
    public string APRejectionReason { get; set; }


    [Column(Storage = "PGMAcknowledged", DbType = "Bit")]
    public bool? PGMAcknowledged { get; set; }

    [Column(Storage = "PGMApproved", DbType = "Bit")]
    public bool? PGMApproved { get; set; }

	/// <summary>
	/// DB Column: [Adversary].[Tracking].[PGMSignature] (VarChar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// </summary>
	[ScreenBuilder(
     Caption = "Approved By",
     FieldOrder = 1,
	 BuilderType = BuilderType.Label,
     ScreenIDs = new string[] { "PGM" })]
	[Column(Storage="PGMSignature", DbType="VarChar(50)")]
	public string PGMSignature { get; set; }

    /// <summary>
    /// DB Column: [Adversary].[Tracking].[PGMDate] (SmallDateTime)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// </summary>
    [ScreenBuilder(
     Caption = "Approval Date",
     FieldOrder = 2,
     BuilderType = BuilderType.Label,
     ScreenIDs = new string[] { "PGM" })]
    [Column(Storage = "PGMDate", DbType = "SmallDateTime(4)")]
    public DateTime? PGMDate { get; set; }

    /// <summary>
    /// DB Column: [Adversary].[Tracking].[PGMARSignature] (VarChar)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// </summary>
    [ScreenBuilder(
     Caption = "Approval Signature for A/R over 90 Days",
     FieldOrder=3,
     BuilderType = BuilderType.TextBox,
     ScreenIDs = new string[] { "PGM" })]
    [Column(Storage = "PGMARSignature", DbType = "VarChar(50)")]
    public string PGMARSignature { get; set; }


    /// <summary>
    /// DB Column: [Adversary].[Tracking].[PGMExceptionSignature] (VarChar)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// </summary>
    [ScreenBuilder(
     Caption = "Exception approval for signed engagement letter",
     BuilderType = BuilderType.TextBox,
     FieldOrder = 4,
     ScreenIDs = new string[] { "PGM" })]
    [Column(Storage = "PGMExceptionSignature", DbType = "VarChar(50)")]
    public string PGMExceptionSignature { get; set; }



	/// <summary>
	/// DB Column: [Adversary].[Tracking].[PGMNotes] (VarChar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// </summary>
	[ScreenBuilder(
     Caption = "Additional Notes",
     BuilderType = BuilderType.TextArea,
     FieldOrder = 5,
     ScreenIDs = new string[] { "PGM" })]
	[Column(Storage="PGMNotes", DbType="VarChar(MAX)")]
	public string PGMNotes { get; set; }


    [Column(Storage = "PGMRejectionReason", DbType = "VarChar(MAX)")]
    public string PGMRejectionReason { get; set; }



    [Column(Storage = "AdversaryAdminAcknowledged", DbType = "Bit")]
    public bool? AdversaryAdminAcknowledged { get; set; }

    [Column(Storage = "ARApprovalReceived", DbType = "Bit")]
    public bool? ARApprovalReceived { get; set; }

    [Column(Storage = "AdversaryAdminApproved", DbType = "Bit")]
    public bool? AdversaryAdminApproved { get; set; }

    [Column(Storage = "AdversarySignature", DbType = "VarChar(50)", Name = "AdversarySignature")]
    public string AdversaryAdminSignature { get; set; }

    [Column(Storage = "AdversaryRejectionReason", DbType = "VarChar(MAX)")]
    public string AdversaryRejectionReason { get; set; }

    [Column(Storage = "AdversaryAcknowledged", DbType = "Bit")]
    public bool? AdversaryAcknowledged { get; set; }

    //[ScreenBuilder(
    // Caption = "Approval Date",
    // BuilderType = BuilderType.Label,
    // ScreenIDs = new string[] { "Adversary" })]
    [Column(Storage = "AdversaryAdminDate", DbType = "SmallDateTime(4)")]
    public DateTime? AdversaryAdminDate { get; set; }

    [Column(Storage = "CMNumbersSentDate", DbType = "SmallDateTime(4)")]
    public DateTime? CMNumbersSentDate { get; set; }

	/// <summary>
	/// DB Column: [Adversary].[Tracking].[ClientMatterNumber] (VarChar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// </summary>
    //[ScreenBuilder(
    // Caption = "Client/Matter",
    // BuilderType = BuilderType.TextBox,
    // ScreenIDs = new string[] { "Adversary" })]
    //[Column(Storage="ClientMatterNumber", DbType="VarChar(50)")]
    //public string ClientMatterNumber { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Tracking].[Opened] (DateTime)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// </summary>
	[ScreenBuilder(
	 Caption = "Opened",
	 BuilderType = BuilderType.TextBox,
     ScreenIDs = new string[] { "Adversary" })]
	[Column(Storage="Opened", DbType="varchar(MAX)")]
	public string Opened { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Tracking].[Conflicts] (VarChar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// </summary>
	[ScreenBuilder(
	 Caption = "Conflicts",
	 BuilderType = BuilderType.TextArea,
     ScreenIDs = new string[] { "Adversary" })]
	[Column(Storage="Conflicts", DbType="VarChar(MAX)")]
	public string Conflicts { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Tracking].[FeeSplits] (VarChar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// </summary>
	[ScreenBuilder(
	 Caption = "Fee Splits",
	 BuilderType = BuilderType.TextArea,
     ScreenIDs = new string[] { "Adversary" })]
	[Column(Storage="FeeSplits", DbType="VarChar(MAX)")]
	public string FeeSplits { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Tracking].[FinalCheck] (VarChar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// </summary>
	[ScreenBuilder(
	 Caption = "FinalCheck",
	 BuilderType = BuilderType.TextArea,
     ScreenIDs = new string[] { "Adversary" })]
	[Column(Storage="Final Check", DbType="VarChar(MAX)")]
	public string FinalCheck { get; set; }



	/// <summary>
	/// DB Column: [Adversary].[Tracking].[APUserID] (Int)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: True
	/// Identity: False
	/// </summary>
	[Column(Storage="APUserID", DbType="Int(4)")]
	public int? APUserID { get; set; }

	/// <summary>
	/// DB Column: [Adversary].[Tracking].[PGMUserID] (Int)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: True
	/// Identity: False
	/// </summary>
	[Column(Storage="PGMUserID", DbType="Int(4)")]
	public int? PGMUserID { get; set; }


	[Column(Storage="AdversaryUserID", DbType="Int(4)")]
	public int? AdversaryUserID { get; set; }


    public Tracking()
    {
        //Locked = false;
        //StatusTypeID = TrackingStatusTypes.None;
    }


    public string GetStatus()
    {
        if (StatusTypeID == null || StatusTypeID == TrackingStatusType.None)
            return "Not Submitted";

        foreach (DataRow dr in StatusTypes.Rows)
        {
            if ((int)dr[STATUS_ID] == (int)StatusTypeID)
                return dr[STATUS_DESCRIPTION].ToString();
        }

        throw new Exception("Could not find a status for status code " + StatusTypeID);
    }

    public string GetAPLoginName()
    {
        if (APUserID == null)
            return "AP Unassigned";
        return AdminLogins.GetAdminLoginNameFromID(APUserID);
    }

    public string GetPGMLoginName()
    {
        if (PGMUserID == null)
            return "PGM Unassigned";
        return AdminLogins.GetAdminLoginNameFromID(PGMUserID);
    }

    public string GetAdversaryLoginName()
    {
        if (AdversaryUserID == null)
            return "Adversary Group Processing Unassigned";
        return AdminLogins.GetAdminLoginNameFromID(AdversaryUserID);
    }

    //public static DataTable GetQueue() { return GetOpenMemos(null, null, true); }
    public static List<Memo> GetQueue(SqlConnection conn, int? userId, TrackingStatusType? status, bool sortNewestFirst)
    {
        //DatabaseWorker worker = DatabaseWorker.GetInstance(Config.GetConnectionString("AdversaryDB"));
        //DatabaseWorker worker = AdversaryData.GetInstance();
        List<SqlParameter> parameters = new List<SqlParameter>();
        //if (office != null)
        //    parameters.Add(new SqlParameter("OrigOffice", office));
        //if (!string.IsNullOrEmpty(pracCode))
        //    parameters.Add(new SqlParameter("PracCode", pracCode));
        if (status != null)
            parameters.Add(new SqlParameter("StatusTypeID", (int)status));
        if (userId != null)
            parameters.Add(new SqlParameter("UserID", userId));
        parameters.Add(new SqlParameter("SortNewestFirst", sortNewestFirst));
        //return worker.RetrieveToDataTable("PR_GET_OpenMemos", parameters.ToArray());

        ///TODO: TEMP FIX
        return GetList(conn, parameters);
    }

    private static List<Memo> GetList(SqlConnection conn, List<SqlParameter> parameters)
    {
        List<Memo> list = new List<Memo>();
        DatabaseWorker worker = new DatabaseWorker(conn);
        SqlDataReader reader = worker.RetrieveToSqlDataReader("PR_GET_OpenMemos", parameters.ToArray(), conn);
        while (reader.Read())
        {
            Memo memo = new Memo();
            memo.ScrMemID = (int?)GetReaderValue(reader["ScrMemID"]);
            memo.ScrMemType = (int?)GetReaderValue(reader["ScrMemType"]);
            memo.MatterName = (string)GetReaderValue(reader["MatterName"]);
            memo.AttEntering = (string)GetReaderValue(reader["AttEntering"]);
            memo.PersonnelID = (string)GetReaderValue(reader["PersonnelID"]);
            memo.RespAttID = (string)GetReaderValue(reader["RespAttID"]);
            memo.PracCode = (string)GetReaderValue(reader["PracCode"]);
            memo.ClientNumber = (string)GetReaderValue(reader["ClientNumber"]);
            memo.Company = (bool?)GetReaderValue(reader["Company"]);
            memo.WorkDesc = (string)GetReaderValue(reader["WorkDesc"]);
            list.Add(memo);
        }

        reader.Close();

        return list;
    }
    private static object GetReaderValue(object value)
    {
        return (value == DBNull.Value) ? null : value;
    }

    public static List<Memo> GetAdversaryQueue()
    {
        using (SqlConnection conn = AdversaryData.GetConnection())
        {
            DatabaseWorker worker = new DatabaseWorker(conn);
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("StatusTypeID", (int)TrackingStatusType.AdversaryGroupProcessing));
            parameters.Add(new SqlParameter("SortNewestFirst", false));
            return worker.RetrieveToList<Memo>("PR_GET_OpenMemos", parameters.ToArray());
        }
    }

}
