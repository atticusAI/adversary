
using System;
using System.Data;
using System.Data.Linq;
using System.Configuration;
using System.Linq;
using System.Data.Linq.Mapping;
using System.Security.Policy;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using HH.Screen;
using HH.UI.FileList;

/// <summary>
/// Summary description for ScreeningMemo
/// This class file was created using the HH DataObjectBuilder
/// </summary>
[Table(Name="Attachments")]
public class Attachments : IFileListDataItem
{

	/// <summary>
	/// DB Column: [Adversary].[Attachments].[AttachmentID] (Int)
	/// Nullable: False
	/// In Primary Key: True
	/// Is Foreign Key: False
	/// Identity: True
	/// </summary>
	[ScreenBuilder(
	 Caption = "AttachmentID",
	 BuilderType = BuilderType.Auto,
	 ScreenIDs = new string[] { })]
	[Column(Storage="AttachmentID", DbType="Int(4) NOT NULL IDENTITY", IsDbGenerated=true, AutoSync=AutoSync.Always, IsPrimaryKey=true)]
	public int? AttachmentID { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Attachments].[ScrMemID] (Int)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// </summary>
	[ScreenBuilder(
	 Caption = "ScrMemID",
	 BuilderType = BuilderType.Auto,
	 ScreenIDs = new string[] { })]
	[Column(Storage="ScrMemID", DbType="Int(4)")]
	public int? ScrMemID { get; set; }

    /// <summary>
    /// DB Column: [Adversary].[Attachments].[FileName] (VarChar)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// </summary>
    //[ScreenBuilder(
    // Caption = "Title",
    // BuilderType = BuilderType.TextArea,
    // ScreenIDs = new string[] { })]
    [Column(Storage = "FileDescription", DbType = "VarChar(400)", Name = "Description")]
    public string FileDescription { get; set; }

	/// <summary>
	/// DB Column: [Adversary].[Attachments].[FileName] (VarChar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// </summary>
    //[ScreenBuilder(
    // Caption = "FileName",
    // BuilderType = BuilderType.TextArea,
    // ScreenIDs = new string[] { })]
	[Column(Storage="FileName", DbType="VarChar(200)")]
	public string FileName { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Attachments].[FileData] (VarBinaryMax)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// </summary>
    //[ScreenBuilder(
    // Caption = "FileData",
    // BuilderType = BuilderType.FileUploadTextBox,
    // ScreenIDs = new string[] { })]
    //[Column(Storage="FileData", DbType="VarBinaryMax")]
    //public byte[] FileData { get; set; }

    public string FileLocalPath { get; set; }

    public Uri FileURI { get; set; }

    private EntityRef<Memo> _memo = default(EntityRef<Memo>);
    [Association(Name = "Attachments", Storage = "_memo", ThisKey = "ScrMemID", IsForeignKey = true)]
    public Memo Memo
    {
        get
        {
            return this._memo.Entity;
        }
        set
        {
            Memo previousValue = this._memo.Entity;
            if (((previousValue != value)
                || (this._memo.HasLoadedOrAssignedValue == false)))
            {
                if ((previousValue != null))
                {
                    this._memo.Entity = null;
                    previousValue.Attachments.Remove(this);
                }
                this._memo.Entity = value;
                if ((value != null))
                {
                    value.Attachments.Add(this);
                    this.ScrMemID = value.ScrMemID;
                }
                else
                {
                    this.ScrMemID = default(Nullable<int>);
                }
            }
        }
    }


	public Attachments() { }

}
