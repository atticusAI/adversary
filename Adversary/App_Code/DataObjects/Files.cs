
using System;
using System.Data;
using System.Data.Linq;
using System.Configuration;
using System.Linq;
using System.Data.Linq.Mapping;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using HH.Screen;
/// <summary>
/// Summary description for ScreeningMemo
/// This class file was created using the HH DataObjectBuilder
/// </summary>
[Table(Name="Files")]
public class Files
{

	/// <summary>
	/// DB Column: [Adversary].[Files].[FileID] (int)
	/// Nullable: False
	/// In Primary Key: True
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: AutoDetermine
	/// </summary>
	[ScreenBuilder(
	 Caption = "FileID",
	 BuilderType = BuilderType.Auto,
	 ScreenIDs = new string[] {})]
	[Column(Storage="FileID", DbType="Int(4) NOT NULL", IsPrimaryKey=true)]
	public int? FileID { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Files].[ScrMemID] (int)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: True
	/// Identity: False
	/// Screen Builder Control Type: AutoDetermine
	/// </summary>
	[ScreenBuilder(
	 Caption = "ScrMemID",
	 BuilderType = BuilderType.Auto,
	 ScreenIDs = new string[] {})]
	[Column(Storage="ScrMemID", DbType="Int(4)")]
	public int? ScrMemID { get; set; }


	private EntityRef<Memo> _memo = default(EntityRef<Memo>);
	[Association(Name="FK_Files_Memo", Storage="_memo", ThisKey="ScrMemID", IsForeignKey=true)]
	public Memo Memo
	{
		get
		{
			return this._memo.Entity;
		}
		set
		{
			Memo previousValue = this._memo.Entity;
			if (((previousValue != value)
				|| (this._memo.HasLoadedOrAssignedValue == false)))
			{
				if ((previousValue != null))
				{
					this._memo.Entity = null;
					previousValue.Files.Remove(this);
				}
				this._memo.Entity = value;
				if ((value != null))
				{
					value.Files.Add(this);
					this.ScrMemID = value.ScrMemID;
				}
				else
				{
					this.ScrMemID = default(Nullable<int>);
				}
			}
		}
	}


	/// <summary>
	/// DB Column: [Adversary].[Files].[FileType] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "FileType",
	 BuilderType = BuilderType.TextBox,
	 ScreenIDs = new string[] {})]
	[Column(Storage="FileType", DbType="NVarChar(50)")]
	public string FileType { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Files].[FileLabel] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "FileLabel",
	 BuilderType = BuilderType.TextBox,
	 ScreenIDs = new string[] {})]
	[Column(Storage="FileLabel", DbType="NVarCharMax")]
	public string FileLabel { get; set; }



    

	public Files() { }

}
