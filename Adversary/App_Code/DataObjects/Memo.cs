
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Web.UI.WebControls;
using HH;
using HH.Extensions;
using HH.Screen;
using HH.SQL;
using System.Reflection;

/// <summary>
/// Summary description for ScreeningMemo
/// This class file was created using the HH DataObjectBuilder
/// </summary>
[Table(Name="Memo")]
public class Memo 
{

	/// <summary>
	/// DB Column: [Adversary].[Memo].[ScrMemID] (int)
	/// Nullable: False
	/// In Primary Key: True
	/// Is Foreign Key: False
	/// Identity: True
	/// Screen Builder Control Type: AutoDetermine
	/// </summary>
    [ScreenBuilder(
     Caption = "Screening Memo Number",
     BuilderType = BuilderType.Label,
     ScreenIDs = new string[] {"BasicInfo.aspx?ScrMemType=1",
                               "BasicInfo.aspx?ScrMemType=2",
                               "BasicInfo.aspx?ScrMemType=3",
                               "BasicInfo.aspx?ScrMemType=5",
                               "BasicInfo.aspx?ScrMemType=6"},
     CssClass = "boldLabel",
     FieldOrder=0)]
	[Column(Storage="ScrMemID", DbType="Int(4) NOT NULL IDENTITY", IsDbGenerated=true, AutoSync=AutoSync.Always, IsPrimaryKey=true)]
	public int? ScrMemID { get; set; }


    private Address _address = new Address();
    [Association(Name = "FK_Address_Memo1", Storage = "_address", OtherKey = "ScrMemID")]
    [ScreenBuilder(BuilderType = BuilderType.EntityAssociation,
        ScreenIDs = new string[] {"ClientAddress.aspx?ScrMemType=1",
                                  "ClientAddress.aspx?ScrMemType=2",
                                  "ClientAddress.aspx?ScrMemType=3",
                                  "ClientAddress.aspx?ScrMemType=6"})]
    public Address Address
    {
        get
        {
            this._address.ScrMemID = this.ScrMemID;
            return this._address;
        }
        set
        {
            this._address = value;
            this._address.ScrMemID = this.ScrMemID;
        }
    }
    //private EntitySet<Address> _addresses;
    //[Association(Name="FK_Address_Memo1", Storage="_addresses", OtherKey="ScrMemID")]
    //public EntitySet<Address> Addresses
    //{
    //    get
    //    {
    //        return this._addresses;
    //    }
    //    set
    //    {
    //        this._addresses.Assign(value);
    //    }
    //}

    private Billing _billing = new Billing();
    [Association(Name = "FK_Billing_Memo", Storage = "_billing", OtherKey = "ScrMemID")]
    [ScreenBuilder(BuilderType = BuilderType.EntityAssociation,
                    ScreenIDs = new string[] {"BillingInfo.aspx?ScrMemType=1",
                                              "BillingInfo.aspx?ScrMemType=2",
                                              "BillingInfo.aspx?ScrMemType=5"})]
    public Billing Billing
    {
        get
        {
            this._billing.ScrMemID = this.ScrMemID;
            return this._billing;
        }
        set
        {
            this._billing = value;
            this._billing.ScrMemID = this.ScrMemID;
        }
    }
    //private EntitySet<Billing> _billings;
    //[Association(Name="FK_Billing_Memo", Storage="_billings", OtherKey="ScrMemID")]
    //public EntitySet<Billing> Billings
    //{
    //    get
    //    {
    //        return this._billings;
    //    }
    //    set
    //    {
    //        this._billings.Assign(value);
    //    }
    //}


	private EntitySet<ClientContact> _clientcontacts;
    [ScreenBuilder(BuilderType=BuilderType.EntityControl, 
     EntityControlID="Contacts",
     Caption = "Contacts",
     ScreenIDs = new string[] {"Contact.aspx?ScrMemType=1",
                               "Contact.aspx?ScrMemType=2",
                               "Contact.aspx?ScrMemType=3",
                               "Contact.aspx?ScrMemType=6"},
     AllowInsert=true,
     AllowUpdate=true,
     AllowDelete=true)]
	[Association(Name="FK_ClientContact_Memo", Storage="_clientcontacts", OtherKey="ScrMemID")]
	public EntitySet<ClientContact> ClientContacts
	{
		get
		{
			return this._clientcontacts;
		}
		set
		{
			this._clientcontacts.Assign(value);
		}
	}


	private EntitySet<FeeSplitStaff> _feesplitstaffs;
    //[ScreenBuilderEntityControl("Fees",
    // ScreenIDs = new string[] {"Fees.aspx?ScrMemType=1",
    //                           "Fees.aspx?ScrMemType=2"},
    // Caption="<strong>Associate Origination:</strong><br/>" + 
    //         "<em>Employee numbers of person(s) claiming origination</em>",
    // InformationToolTip="Click the Add a new record link to add an associate's name to the database. " +
    //                    "Identify multiple associates if applicable. ",
    // AllowInsert = true,
    // AllowUpdate = false,
    // AllowDelete = true)]
	[Association(Name="FK_FeeSplitStaff_Memo", Storage="_feesplitstaffs", OtherKey="ScrMemID")]
	public EntitySet<FeeSplitStaff> FeeSplitStaffs
	{
		get
		{
			return this._feesplitstaffs;
		}
		set
		{
			this._feesplitstaffs.Assign(value);
		}
	}


	private EntitySet<Files> _files;
	[Association(Name="FK_Files_Memo", Storage="_files", OtherKey="ScrMemID")]
	public EntitySet<Files> Files
	{
		get
		{
			return this._files;
		}
		set
		{
			this._files.Assign(value);
		}
	}

    private EntitySet<Attachments> _attachments;
    [Association(Name = "FK_Files_Memo", Storage = "_attachments", OtherKey = "ScrMemID")]
    public EntitySet<Attachments> Attachments
    {
        get
        {
            return this._attachments;
        }
        set
        {
            this._attachments.Assign(value);
        }
    }

    public bool AttachedFileExists(string fileName)
    {
        fileName = fileName.ToLower();

        var query = from a in Attachments
                    where a.FileName.ToLower() == fileName
                    select a;

        return (query.Count() > 0);
    }

    public List<Attachments> ConflictReportFiles
    {
        get
        {
            var query = from a in Attachments
                        where a.FileDescription == "Conflict Report"
                        select a;

            return query.ToList<Attachments>();
        }
    }

    public List<Attachments> EngagementLetterFiles
    {
        get
        {
            var query = from a in Attachments
                        where a.FileDescription == "Engagement Letter"
                        select a;

            return query.ToList<Attachments>();
        }
    }

    public List<Attachments> WrittenPolicyFiles
    {
        get
        {
            var query = from a in Attachments
                        where a.FileDescription == "Written Policy"
                        select a;

            return query.ToList<Attachments>();
        }
    }

    public List<Attachments> RejectionLetterFiles
    {
        get
        {
            var query = from a in Attachments
                        ///commented by A Reimer 03/21/12 for 'Rejected to Modification Required' change 
                        //where a.FileDescription == "Rejection Letter"
                        where (a.FileDescription == "Rejection Letter" || a.FileDescription == "Request for Modification")
                        select a;

            return query.ToList<Attachments>();
        }
    }

    

    /// <summary>
    /// DB Column: [Adversary].[Memo].[Company] (bit)
    /// Nullable: False
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: BooleanRadioControl
    /// </summary>
    private bool? _company = null;
    [ScreenBuilder(BuilderType = BuilderType.AccordianControl,
     AccordianControlID = "Company",
     Caption = "Client Type",
     ScreenIDs = new string[] { "ClientDetails.aspx?ScrMemType=1",
                                "ClientDetails.aspx?ScrMemType=3",
                                "ClientDetails.aspx?ScrMemType=6"},
     PaneItemText = new string[] { "Organization", "Individual" },
     PaneItemValues = new string[] { "True", "False" },
     ListRepeatDirection = RepeatDirection.Horizontal)]
    [Column(Storage = "Company", DbType = "Bit NOT NULL")]
    public bool? Company
    {
        get
        {
            if (_company != null)
                Individual = !_company;
            return _company;
        }
        set
        {
            if (value == null)
            {
                Individual = null;
                _company = null;
            }
            else
            {
                Individual = !value;
                _company = value;
            }
        }
    }

    /// <summary>
    /// DB Column: [Adversary].[Memo].[Individual] (bit)
    /// Nullable: False
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// 
    /// This property has the opposite flag of the Company property, 
    /// unfortunately necessary for backwards compatability with the access DB
    /// </summary>
    [Column(Storage = "Individual", DbType = "Bit NOT NULL")]
    public bool? Individual { get; set; }

    //private EntitySet<NewClient> _newclients;
    ////[ScreenBuilderEntityControl(
    //// EntityControlID = "NewClients",
    //// Caption = "Client's Information",
    //// ScreenID = "ClientDetails",
    //// AllowInsert = true,
    //// AllowUpdate = true,
    //// AllowDelete = true)]
    //[Association(Name = "FK_NewClient_Memo", Storage = "_newclients", OtherKey = "ScrMemID")]
    //protected internal EntitySet<NewClient> NewClients
    //{
    //    get
    //    {
    //        return this._newclients;
    //    }
    //    set
    //    {
    //        this._newclients.Assign(value);
    //    }
    //}

    private NewClient _newClient = new NewClient();
    //[ScreenBuilderEntityControl(
    // Caption = "Client List",
    // ScreenID = "ClientType",
    // AllowInsert = true,
    // AllowUpdate = true,
    // AllowDelete = true)]
    [Association(Name = "FK_NewClient_Memo", Storage = "_newclient", OtherKey = "ScrMemID")]
    [ScreenBuilder(BuilderType=BuilderType.EntityAssociation,
        ScreenIDs = new string[] {"ReferringParty.aspx?ScrMemType=1",
                                  /* "ReferringParty.aspx?ScrMemType=3",*/
                                  "ClientDetails.aspx?ScrMemType=1",
                                  "ClientDetails.aspx?ScrMemType=3",
                                  "ClientDetails.aspx?ScrMemType=6"})]
    public NewClient NewClient
    {
        get
        {
            if (this._newClient.NwClientID == null &&
                this.ScrMemID != null)
            {
                this._newClient.NwClientID = 0;
                this._newClient.ScrMemID = this.ScrMemID;
            }
            return _newClient;
        }
        set
        {
            this._newClient = value;
            if (this._newClient.NwClientID == null &&
                this.ScrMemID != null)
            {
                this._newClient.NwClientID = 0;
                this._newClient.ScrMemID = this.ScrMemID;
            }
        }
    }

	private EntitySet<OpposingCounsel> _opposingcounsels;
	[Association(Name="FK_OpposingCounsel_Memo", Storage="_opposingcounsels", OtherKey="ScrMemID")]
    //[ScreenBuilderEntityAssociation(new string[] {"RelatedParties.aspx?ScrMemType=1",
    //                                              "AdverseParties.aspx?ScrMemType=1",
    //                                              "RelatedParties.aspx?ScrMemType=2",
    //                                              "AdverseParties.aspx?ScrMemType=2",
    //                                              "Opposing.aspx?ScrMemType=3",
    //                                              "RelatedParties.aspx?ScrMemType=5",
    //                                              "AdverseParties.aspx?ScrMemType=5",
    //                                              "RelatedParties.aspx?ScrMemType=6",
    //                                              "AdverseParties.aspx?ScrMemType=6"})]
    [ScreenBuilder(BuilderType=BuilderType.EntityControl, 
     EntityControlID="OpposingCounsel",
     ScreenIDs = new string[] { "Opposing.aspx?ScrMemType=3" },
     Caption = "Opposing Counsel",
     InformationToolTip = "If \"Yes\", click the add a new record link to add opposing counsel.",
     AllowInsert = true,
     AllowUpdate = true,
     AllowDelete = true)]
	public EntitySet<OpposingCounsel> OpposingCounsels
	{
		get
		{
			return this._opposingcounsels;
		}
		set
		{
			this._opposingcounsels.Assign(value);
		}
	}


	private EntitySet<Party> _parties;
    [Association(Name="FK_Party_Memo", Storage="_parties", OtherKey="ScrMemID")]
    //[ScreenBuilderEntityAssociation(new string[] {"RelatedParties.aspx?ScrMemType=1",
    //                                              "AdverseParties.aspx?ScrMemType=1",
    //                                              "RelatedParties.aspx?ScrMemType=2",
    //                                              "AdverseParties.aspx?ScrMemType=2",
    //                                              "RelatedParties.aspx?ScrMemType=3",
    //                                              "AdverseParties.aspx?ScrMemType=3",
    //                                              "RelatedParties.aspx?ScrMemType=5",
    //                                              "AdverseParties.aspx?ScrMemType=5",
    //                                              "RelatedParties.aspx?ScrMemType=6",
    //                                              "AdverseParties.aspx?ScrMemType=6"})]
	public EntitySet<Party> Parties
	{
		get
		{
			return this._parties;
		}
		set
		{
			this._parties.Assign(value);
		}
	}

    //[ScreenBuilderControl(
    // Caption = "Referring Party",
    // ScreenID = "ReferringParty.aspx?ScrMemType=1"},
    // ControlType = ScreenBuilderControlType.None)]
    /*[Association(Name = "FK_Party_Memo", Storage = "_parties", OtherKey = "ScrMemID")]*/
    //[ScreenBuilderEntityControl(
    // "ReferringParty",
    // Caption = "Referring Parties",
    // ScreenIDs = new string[] { "ReferringParty.aspx?ScrMemType=1",
    //                            "ReferringParty.aspx?ScrMemType=3" },
    // AllowInsert = false,
    // AllowUpdate = true,
    // AllowDelete = false,
    // DataTarget = "Parties")]
    [ScreenBuilder(BuilderType=BuilderType.EntityAssociation,
        ScreenIDs = new string[] {  "ReferringParty.aspx?ScrMemType=1",
                                    "ReferringParty.aspx?ScrMemType=3"})]
    public Party ReferringParty
    {
        get
        {
            
            var query = from p in Parties
                        where p.PartyRelationshipCode == "902"
                        select p;
            
            if (query.Count() > 0)
                return query.LastOrDefault();

            Party party = new Party();
            party.PartyID = 0;
            party.PartyRelationshipCode = "902";
            party.PartyRelationshipName = "Referring Party";
            party.PartyAddressInfo.PartyAddressID = 0;
            
            //if (party.PartyAddressInfo.Count == 0)
            //{
            //    PartyAddressInfo info = new PartyAddressInfo();
            //    info.PartyAddressID = 0;
            //    party.PartyAddressInfo.Add(info);    
            //}
            Parties.Add(party);
            return party;
        }
        //set
        //{
        //    bool found = false;
        //    for (int i=0; i<Parties.Count(); i++)
        //    {
        //        if (Parties[i].PartyRelationshipCode == "902")
        //        {
        //            Parties[i] = value;
        //            Parties[i].PartyRelationshipName = "Referring Party";
        //            found = true;
        //            break;
        //        }
        //    }

        //    if (!found)
        //    {
        //        value.PartyRelationshipCode = "902";
        //        value.PartyRelationshipName = "Referring Party";
        //        Parties.Add(value);
        //    }
        //}
    }

    //[ScreenBuilderEntityControl(
    // "RelatedParties",
    // Caption="Related Parties",
    // ScreenIDs = new string[] {"RelatedParties.aspx?ScrMemType=1",
    //                           "RelatedParties.aspx?ScrMemType=2",
    //                           "RelatedParties.aspx?ScrMemType=3",
    //                           "RelatedParties.aspx?ScrMemType=5",
    //                           "RelatedParties.aspx?ScrMemType=6"},
    // AllowInsert = true,
    // AllowUpdate = true,
    // AllowDelete = true,
    // DataTarget="Parties")]
    public List<Party> RelatedParties
    {
        get
        {

            DataTable dt = new DataTable();
            Parties.FillDataTable(dt);
            DataTable related = Party.SelectRelatedParties(dt, "PartyRelationshipCode");
            return GetPartyList(related);
            
            //var query = from p in Parties
            //            where (p.PC == 0 ||
            //                   p.PC == null) &&
            //                  p.PartyRelationshipCode != "902"
            //            select p;
            //return query.ToList<Party>();
        }
        //set
        //{
        //    this._parties.Assign(value);
        //}
    }

    //TODO: Need a better option than this
    private List<Party> GetPartyList(DataTable parties)
    {
        List<Party> list = new List<Party>(parties.Rows.Count);
        foreach (DataRow dr in parties.Rows)
        {
            Party party = new Party();
            foreach (PropertyInfo info in typeof(Party).GetProperties())
            {
                if (info.Name == "Memo")
                    continue;
                if (!parties.Columns.Contains(info.Name))
                    continue;
                object value = dr[info.Name];
                if (value == DBNull.Value)
                    continue;

                typeof(Party).GetProperty(info.Name).SetValue(party, value, null);
            }
            list.Add(party);
        }
        return list;
    }

    //[ScreenBuilderEntityControl(
    // "AdverseParties",
    // Caption = "Adverse Parties",
    // ScreenIDs = new string[] {"AdverseParties.aspx?ScrMemType=1",
    //                           "AdverseParties.aspx?ScrMemType=2",
    //                           "AdverseParties.aspx?ScrMemType=3",
    //                           "AdverseParties.aspx?ScrMemType=5",
    //                           "AdverseParties.aspx?ScrMemType=6"},
    // AllowInsert = true,
    // AllowUpdate = true,
    // AllowDelete = true,
    // DataTarget = "Parties")]
    public List<Party> AdverseParties
    {
        get
        {
            DataTable dt = new DataTable();
            Parties.FillDataTable(dt);
            DataTable adverse = Party.SelectAdverseParties(dt, "PartyRelationshipCode");
            //List<Party> list = new List<Party>(adverse.Rows.Count);
            //adverse.FillIList(list);
            //return list;

            return GetPartyList(adverse);

            //var query = from p in Parties
            //            where p.PC == 1 &&
            //                  p.PartyRelationshipCode != "902"
            //            select p;
            //return query.ToList<Party>();
        }
        //set
        //{
        //    this._parties.Assign(value);
        //}
    }

    
	private EntitySet<Staffing> _staffing;
    //[ScreenBuilderEntityControl(
    // "Staff",
    // ScreenIDs = new string[] {"Staffing.aspx?ScrMemType=1",
    //                           "Staffing.aspx?ScrMemType=2",
    //                           "Staffing.aspx?ScrMemType=3",
    //                           "Staffing.aspx?ScrMemType=5"},
    // Caption="Proposed Initial Staffing",
    // InformationToolTip = "<a href=\"http://h2o.hollandhart.com/C7/FS%20Inquiries/Document%20Library/TKIDLookup.aspx\" target=\"_blank\">Click</a> to search for staff",
    // AllowInsert = true,
    // AllowUpdate = false,
    // AllowDelete = true,
    // FieldOrder=0)]
	[Association(Name="FK_Staffing_Memo", Storage="_staffing", OtherKey="ScrMemID")]
	public EntitySet<Staffing> Staffing
	{
		get
		{
			return this._staffing;
		}
		set
		{
			this._staffing.Assign(value);
		}
	}

    private EntitySet<NDriveAccess> _nDriveAccess;
    [Association(Name = "FK_NDriveAccess_Memo", Storage = "_nDriveAccess", OtherKey = "ScrMemID")]
    public EntitySet<NDriveAccess> NDriveAccess
    {
        get
        {
            return this._nDriveAccess;
        }
        set
        {
            this._nDriveAccess.Assign(value);
        }
    }

    /// <summary>
    /// DB Column: [Adversary].[Memo].[DocketingAtty] (nvarchar)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: AutoDetermine
    /// </summary>
    //[ScreenBuilder(
    // Caption = "Docketing Attorney",
    // BuilderType = BuilderType.Auto,
    // ScreenIDs = new string[] {"Docketing.aspx?ScrMemType=1",
    //                           "Docketing.aspx?ScrMemType=2",
    //                           "Docketing.aspx?ScrMemType=3",
    //                           "Docketing.aspx?ScrMemType=5"})]
    [Column(Storage = "DocketingAtty", DbType = "nvarchar(50)")]
    public string DocketingAtty { get; set; }


    /// <summary>
    /// DB Column: [Adversary].[Memo].[DocketingStaff] (nvarchar)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: AutoDetermine
    /// </summary>
    //[ScreenBuilder(
    // Caption = "Docketing Staff",
    // BuilderType = BuilderType.Auto,
    // ScreenIDs = new string[] {"Docketing.aspx?ScrMemType=1",
    //                           "Docketing.aspx?ScrMemType=2",
    //                           "Docketing.aspx?ScrMemType=3",
    //                           "Docketing.aspx?ScrMemType=5"})]
    [Column(Storage = "DocketingStaff", DbType = "nvarchar(50)")]
    public string DocketingStaff { get; set; }


    private EntitySet<DocketingTeam> _docketingTeam;
    [Association(Name = "FK_DocketingTeam_Memo", Storage = "_docketingTeam", OtherKey = "ScrMemID")]
    public EntitySet<DocketingTeam> DocketingTeam
    {
        get
        {
            return this._docketingTeam;
        }
        set
        {
            this._docketingTeam.Assign(value);
        }
    }

    private EntitySet<ClientMatterNumber> _clientMatterNumbers;
    [Association(Name = "FK_ClientMatterNumbers_Memo", Storage = "_clientMatterNumbers", OtherKey = "ScrMemID")]
    public EntitySet<ClientMatterNumber> ClientMatterNumbers
    {
        get
        {
            return this._clientMatterNumbers;
        }
        set
        {
            this._clientMatterNumbers.Assign(value);
        }
    }


	/// <summary>
	/// DB Column: [Adversary].[Memo].[ScrMemType] (int)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: AutoDetermine
	/// </summary>
	[ScreenBuilder(
	 Caption = "ScrMemType",
	 BuilderType = BuilderType.Auto,
	 ScreenIDs = new string[] {})]
	[Column(Storage="ScrMemType", DbType="Int(4)")]
	public int? ScrMemType { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Memo].[RefDate] (datetime)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
    [ScreenBuilder(
     Caption = "Date",
     BuilderType = BuilderType.TextBox,
     ScreenIDs = new string[] {"BasicInfo.aspx?ScrMemType=1",
                               "BasicInfo.aspx?ScrMemType=2",
                               "BasicInfo.aspx?ScrMemType=3",
                               "BasicInfo.aspx?ScrMemType=5",
                               "BasicInfo.aspx?ScrMemType=6"})]
    [Column(Storage = "RefDate", DbType = "DateTime(20)")]
    //[Column(Storage = "RefDate", DbType = "DateTime(8)")]
    public DateTime? RefDate { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Memo].[InputDate] (money)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: AutoDetermine
	/// </summary>
	[ScreenBuilder(
	 Caption = "InputDate",
	 BuilderType = BuilderType.Auto,
	 ScreenIDs = new string[] {})]
	[Column(Storage="InputDate", DbType="Money(8)")]
	public decimal? InputDate { get; set; }


    private string _attEntering = null;
	/// <summary>
	/// DB Column: [Adversary].[Memo].[AttEntering] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
    //[ScreenBuilder(
    // Caption = "Attorney Preparing Memo Employee #",
    // BuilderType = BuilderType.TextBox,
    // ScreenIDs = new string[] {"BasicInfo.aspx?ScrMemType=1",
    //                           "BasicInfo.aspx?ScrMemType=2",
    //                           "BasicInfo.aspx?ScrMemType=3",
    //                           "BasicInfo.aspx?ScrMemType=5",
    //                           "BasicInfo.aspx?ScrMemType=6"},
    // ValidateHasValue=true)]
    [Column(Storage = "AttEntering", DbType = "NVarChar(50)")]
    public string AttEntering
    {
        get
        {
            return _attEntering;
        }
        set
        {
            _attEntering = value;
            //_attCity = null;
        }
    }

	/// <summary>
	/// DB Column: [Adversary].[Memo].[PersonnelID] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
    //[ScreenBuilder(
    // Caption = "Input By Employee #",
    // BuilderType = BuilderType.TextBox,
    // ScreenIDs = new string[] {"BasicInfo.aspx?ScrMemType=1",
    //                           "BasicInfo.aspx?ScrMemType=2",
    //                           "BasicInfo.aspx?ScrMemType=3",
    //                           "BasicInfo.aspx?ScrMemType=5",
    //                           "BasicInfo.aspx?ScrMemType=6"},
    // ValidateHasValue=true)]
	[Column(Storage="PersonnelID", DbType="NVarChar(50)")]
	public string PersonnelID { get; set; }


    private string _respAttId = null;
	/// <summary>
	/// DB Column: [Adversary].[Memo].[RespAttID] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
    //[ScreenBuilder(
    // Caption = "Billing Attorney",
    // InformationToolTip = "<a href=\"http://h2o.hollandhart.com/C7/FS%20Inquiries/Document%20Library/TKIDLookup.aspx\" target=\"_blank\">Click</a> to search for staff",
    // BuilderType = BuilderType.TextBox,
    // ScreenIDs = new string[] { "Staffing.aspx?ScrMemType=1", 
    //                            "Staffing.aspx?ScrMemType=2",
    //                            "Staffing.aspx?ScrMemType=3",
    //                            "Staffing.aspx?ScrMemType=5"})]
	[Column(Storage="RespAttID", DbType="NVarChar(4)")]
	public string RespAttID 
    {
        get
        {
            return _respAttId;
        }
        set
        {
            _respAttId = value;
            _respOfficeName = null;
            _respOfficeCode = null;
        }
    }



    private string _respOfficeName = null;
    public string RespAttOfficeName
    {
        get
        {
            if (string.IsNullOrEmpty(RespAttID))
                return null;
            if (_respOfficeName == null)
            {
                try
                {
                    FirmDirectory fd = new FirmDirectory();
                    _respOfficeName = fd.GetEmployeeCity(RespAttID);
                }
                catch
                {
                    _respOfficeName = string.Empty;
                }
            }
            return _respOfficeName;
        }
    }

    private string _respOfficeCode;
    [Column(Storage = "RespAttOfficeCode", DbType = "NVarChar(50)")]
    public string RespAttOfficeCode
    {
        get
        {
            if (string.IsNullOrEmpty(RespAttID))
                return null;
            if (RespAttOfficeName == string.Empty)
                return string.Empty;
            if (_respOfficeCode == null)
            {
                try
                {
                    CMSData cms = CMSData.GetInstance();
                    _respOfficeCode = cms.GetOfficeCode(RespAttOfficeName);
                }
                catch
                {
                    _respOfficeCode = string.Empty;
                }
            }
            return _respOfficeCode;
        }
    }


	/// <summary>
	/// DB Column: [Adversary].[Memo].[PreScreen] (bit)
	/// Nullable: False
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: BooleanRadioControl
	/// </summary>
	[ScreenBuilder(
	 Caption = "PreScreen",
	 BuilderType = BuilderType.BooleanRadioControl,
	 ScreenIDs = new string[] {})]
	[Column(Storage="PreScreen", DbType="Bit NOT NULL")]
	public bool? PreScreen { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Memo].[PreScreenDesc] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "PreScreenDesc",
	 BuilderType = BuilderType.TextBox,
	 ScreenIDs = new string[] {})]
	[Column(Storage="PreScreenDesc", DbType="NVarCharMax")]
	public string PreScreenDesc { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Memo].[PreScreenNames] (tinyint)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: AutoDetermine
	/// </summary>
	[ScreenBuilder(
	 Caption = "PreScreenNames",
	 BuilderType = BuilderType.Auto,
	 ScreenIDs = new string[] {})]
	[Column(Storage="PreScreenNames", DbType="TinyInt")]
	public byte? PreScreenNames { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Memo].[PreScreenNameList] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "PreScreenNameList",
	 BuilderType = BuilderType.TextBox,
	 ScreenIDs = new string[] {})]
	[Column(Storage="PreScreenNameList", DbType="NVarCharMax")]
	public string PreScreenNameList { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Memo].[ClearedConflicts] (bit)
	/// Nullable: False
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: BooleanRadioControl
	/// </summary>
	[ScreenBuilder(
	 Caption = "ClearedConflicts",
	 BuilderType = BuilderType.BooleanRadioControl,
	 ScreenIDs = new string[] {})]
	[Column(Storage="ClearedConflicts", DbType="Bit NOT NULL")]
	public bool? ClearedConflicts { get; set; }




	/// <summary>
	/// DB Column: [Adversary].[Memo].[WorkDesc] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextArea
	/// </summary>
    [ScreenBuilder(
     Caption = "Work Description",
     BuilderType = BuilderType.TextArea,
     ScreenIDs = new string[] {"CM.aspx?ScrMemType=1",
                               "CM.aspx?ScrMemType=2",
                               "CM.aspx?ScrMemType=3",
                               "CM.aspx?ScrMemType=5"},
     TextAreaRows = 3,
     FieldOrder=1,
     ValidateAsRequiredField=true)]
	[Column(Storage="WorkDesc", DbType="NVarChar(500)")]
	public string WorkDesc { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Memo].[FeeSplit] (bit)
	/// Nullable: False
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: BooleanRadioControl
	/// </summary>
    //[ScreenBuilderControl(
    // Caption = "FeeSplit",
    // ControlType = ScreenBuilderControlType.BooleanRadioControl,
    // ScreenID = "Fees.aspx?ScrMemType=1"})]
	[Column(Storage="FeeSplit", DbType="Bit NOT NULL")]
	public bool? FeeSplit { get; set; }








	/// <summary>
	/// DB Column: [Adversary].[Memo].[FeeSplitDesc] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
     AccordianTargetIDs = new string[] { "NewMatterOnly" },
     AccordianPaneIDs = new string[] { "no" },
     BuilderType = BuilderType.TextArea,
     /*ScreenIDs = new string[] { "Fees.aspx?ScrMemType=1" },*/
     Caption = "Explanation")]
	[Column(Storage="FeeSplitDesc", DbType="NVarCharMax")]
	public string FeeSplitDesc { get; set; }


    /// <summary>
    /// Data source for the offices drop down list
    /// </summary>
    public static DataTable OrigOffices
    {
        get
        {
            CMSData cms = CMSData.GetInstance();
            return cms.AllOffices;
        }
    }
	/// <summary>
	/// DB Column: [Adversary].[Memo].[OrigOffice] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "Originating Office",
	 BuilderType = BuilderType.DropDownList,
	 ScreenIDs = new string[] {"Office.aspx?ScrMemType=1",
                               "Office.aspx?ScrMemType=2",
                               "Office.aspx?ScrMemType=3",
                               "Office.aspx?ScrMemType=5"},
     DataSource = "OrigOffices",
     DataTextField = CMSData.OFFICE_DESCRIPTION,
     DataValueField = CMSData.OFFICE_CODE,
     FieldOrder=4)]
	[Column(Storage="OrigOffice", DbType="NVarChar(50)")]
	public string OrigOffice { get; set; }



    /// <summary>
    /// Data source for the Practice Codes drop down list
    /// </summary>
    public DataTable PracticeTypes
    {
        get
        {
            CMSData cms = CMSData.GetInstance();
            DataTable _returnTable = cms.PracticeTypes;
            DataTable _newTable = _returnTable.Clone();
            if (new List<int>() { 1, 2 }.Contains(Convert.ToInt32(this.ScrMemType)))
            {
                //_returnTable.AsEnumerable().Where(I => !new List<string>() { "105X", "107X", "105Z" }.Contains(I.Field<string>(CMSData.PRACTICE_TYPES_CODE))).CopyToDataTable(_newTable,System.Data.LoadOption.OverwriteChanges);
                //added 102X 10-31-2012 per request from Vanessa Becker
                _returnTable.AsEnumerable().Where(I => !new List<string>() { "105X", "107X", "105Z", "102X" }.Contains(I.Field<string>(CMSData.PRACTICE_TYPES_CODE))).CopyToDataTable(_newTable, System.Data.LoadOption.OverwriteChanges);
                return _newTable;
            }
            else
            {
                return _returnTable;
            }
            
        }
    }
    /// <summary>
    /// DB Column: [Adversary].[Memo].[PracCode] (nvarchar)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: TextBox
    /// </summary>
    [ScreenBuilder(
     Caption = "Practice Type Code",
     BuilderType = BuilderType.DropDownList,
     ScreenIDs = new string[] {"CM.aspx?ScrMemType=1",
                               "CM.aspx?ScrMemType=2",
                               "CM.aspx?ScrMemType=3",
                               "CM.aspx?ScrMemType=5"},
     DataSource = "PracticeTypes",
     DataTextField = CMSData.PRACTICE_TYPES_DESCRIPTION_PLUS_CODE,
     DataValueField = CMSData.PRACTICE_TYPES_CODE,
     ValidateAsRequiredField=true,
     FieldOrder=2)]
    [Column(Storage = "PracCode", DbType = "NVarChar(5)")]
    public string PracCode { get; set; }



    /// <summary>
    /// DB Column: [Adversary].[Memo].[EstFees] (nvarchar)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: TextBox
    /// </summary>
    [ScreenBuilder(
     Caption = "Estimate of Fees",
     BuilderType = BuilderType.DropDownList,
     ScreenIDs = new string[] {"CM.aspx?ScrMemType=1",
                               "CM.aspx?ScrMemType=2",
                               "CM.aspx?ScrMemType=5"},
     ListItemText = new string[] { "Under $5000", "$5000 - $10,000", "Over $10,000" },
     ListItemValues = new string[] { "Under_$5000", "$5000_$10,000", "Over_$10,000" },
     FieldOrder = 3)]
    [Column(Storage = "EstFees", DbType = "NVarChar(50)")]
    public string EstFees { get; set; }



    /// <summary>
    /// DB Column: [Adversary].[Memo].[ContingFee] (bit)
    /// Nullable: False
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: BooleanRadioControl
    /// </summary>
    [ScreenBuilder(
     Caption = "Contingency Fee?",
     BuilderType = BuilderType.BooleanRadioControl,
     ScreenIDs = new string[] {"CM.aspx?ScrMemType=1",
                               "CM.aspx?ScrMemType=2",
                               "CM.aspx?ScrMemType=5"},
     FieldOrder = 4)]
    [Column(Storage = "ContingFee", DbType = "Bit NOT NULL")]
    public bool? ContingFee { get; set; }

    [ScreenBuilder(Caption = "Create an N Drive folder for this client?",
     BuilderType = BuilderType.BooleanRadioControl,
     ScreenIDs = new string[] {"CM.aspx?ScrMemType=1",
                               "CM.aspx?ScrMemType=2",
                               "CM.aspx?ScrMemType=3",
                               "CM.aspx?ScrMemType=5"},
     FieldOrder = 5)]
    [Column(Storage = "NDrive", DbType = "Bit NOT NULL")]
    public bool? NDrive { get; set; }

    [ScreenBuilder(Caption = "N Drive Folder Type",
     InformationToolTip = //"Patent Working Files - Full Access to Patent Admins<br/><br/>" +  //per PS request on 7/28/2011, Removed Patent Working Files and Patent Files options and added "Patent Record" option 
                          //"Patent Files - Restricted Access to Patent Admins<br/><br/>" +
                          "Trademark Working Files - Full Access to Trademark Admins<br/><br/>" +
                          "Matter Folder - Name to match C/M lookup (requires user list)<br/><br/>" + 
                          "Patent Record - Requires team selection, Docketing members auto-included.<br/><br/>",
     BuilderType = BuilderType.DropDownList,
     ListItemText = new string[] { "(Select Type)", "Patent Working Files", "Patent Files", "Trademark Working Files", "Matter Folder", "Patent Record" },
     ListItemValues = new string[] { "0", "1", "2", "3", "4", "5" },
     ScreenIDs = new string[] {"CM.aspx?ScrMemType=1",
                               "CM.aspx?ScrMemType=2",
                               "CM.aspx?ScrMemType=3",
                               "CM.aspx?ScrMemType=5"},
     FieldOrder = 6)]
    [Column(Storage = "NDriveType", DbType = "int")]
    public int? NDriveType { get; set; }


    /// <summary>
    /// This is the N Drive "Does This Apply" dropdown feature that was requested 06/25/2013
    /// DB Column: [Adversary].[Memo].[NDriveDoesApply] (int)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: DropDownList
    /// </summary>
    [ScreenBuilder(Caption = "Do any of these apply?<br/><br/><strong><font color='red'>N: Drive folders are generally not secured!</font></strong>",
        InformationToolTip = "Please choose if any of the following situations apply to this N-Drive",
        BuilderType = BuilderType.DropDownList,
        ListItemText = new string[] { "(Select)", "None", "Court Ordered/Protective Order", "Client Policy/HIPAA/ITAR (etc)", "Patent" },
        ListItemValues = new string[] { "-1", "0", "1", "2", "3" },
        ScreenIDs = new string[] {"CM.aspx?ScrMemType=1",
                                    "CM.aspx?ScrMemType=2",
                                    "CM.aspx?ScrMemType=3",
                                    "CM.aspx?ScrMemType=5"},
        FieldOrder = 7)]
    [Column(Storage = "NDriveDoesApply", DbType = "int")]
    public int? NDriveDoesApply { get; set; }
    

	[Column(Storage="EngageLetter", DbType="Bit NOT NULL")]
	public bool? EngageLetter { get; set; }

	[Column(Storage="EngageDesc", DbType="NVarCharMax")]
	public string EngageDesc { get; set; }

    [Column(Storage = "ConflictReport", DbType = "Bit NOT NULL")]
    public bool? ConflictReport { get; set; }

    [Column(Storage = "ConflictDesc", DbType = "NVarCharMax")]
    public string ConflictDesc { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Memo].[DocumentID] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "DocumentID",
	 BuilderType = BuilderType.TextBox,
	 ScreenIDs = new string[] {})]
	[Column(Storage="DocumentID", DbType="NVarChar(50)")]
	public string DocumentID { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Memo].[Imanage] (bit)
	/// Nullable: False
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: BooleanRadioControl
	/// </summary>
	[ScreenBuilder(
	 Caption = "Imanage",
	 BuilderType = BuilderType.BooleanRadioControl,
	 ScreenIDs = new string[] {})]
	[Column(Storage="Imanage", DbType="Bit NOT NULL")]
	public bool? Imanage { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Memo].[DocumentOffice] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "DocumentOffice",
	 BuilderType = BuilderType.TextBox,
	 ScreenIDs = new string[] {})]
	[Column(Storage="DocumentOffice", DbType="NVarChar(50)")]
	public string DocumentOffice { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Memo].[ContigDocumentID] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "ContigDocumentID",
	 BuilderType = BuilderType.TextBox,
	 ScreenIDs = new string[] {})]
	[Column(Storage="ContigDocumentID", DbType="NVarChar(50)")]
	public string ContigDocumentID { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Memo].[ContigDocumentOffice] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "ContigDocumentOffice",
	 BuilderType = BuilderType.TextBox,
	 ScreenIDs = new string[] {})]
	[Column(Storage="ContigDocumentOffice", DbType="NVarChar(50)")]
	public string ContigDocumentOffice { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Memo].[Retainer] (bit)
	/// Nullable: False
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: BooleanRadioControl
	/// </summary>
    //[ScreenBuilderAccordianControl(
    // "Retainer",
    // Caption = "Is there a retainer for this engagement?",
    // InformationToolTip="If no retainer is requested or if retainer is less than $5000 PGM or AP signature is required.",
    // PaneItemText = new string[] {"Yes","No"},
    // PaneItemValues = new string[] { "True", "False" },
    // ScreenIDs = new string[] {"Retainer.aspx?ScrMemType=1",
    //                           "Retainer.aspx?ScrMemType=2",
    //                           "Retainer.aspx?ScrMemType=3"})]
	[Column(Storage="Retainer", DbType="Bit NOT NULL")]
	public bool? Retainer { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Memo].[RetainerDescNo] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
    //[ScreenBuilder(
    // Caption = "Why Not?",
    // BuilderType = BuilderType.TextArea,
    // AccordianTargetIDs=new string[] {"Retainer"},
    // AccordianPaneIDs=new string[] {"False"}
    // /*ScreenIDs = new string[] {"Retainer.aspx?ScrMemType=1"}*/)]
	[Column(Storage="RetainerDescNo", DbType="NVarCharMax")]
	public string RetainerDescNo { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Memo].[RetainerDescYes] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
    //[ScreenBuilder(
    // Caption = "Why?",
    // BuilderType = BuilderType.TextArea,
    // AccordianTargetIDs = new string[] {"Retainer"},
    // AccordianPaneIDs = new string[] { "True" }
    // /*ScreenIDs = new string[] { "Retainer.aspx?ScrMemType=1" }*/)]
	[Column(Storage="RetainerDescYes", DbType="NVarCharMax")]
	public string RetainerDescYes { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Memo].[Confidential] (bit)
	/// Nullable: False
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: BooleanRadioControl
	/// </summary>
    [ScreenBuilder(
     Caption = "Confidential Memo?",
     //RowRepeatDirection = RepeatDirection.Horizontal,
     ListRepeatDirection=RepeatDirection.Horizontal,
     BuilderType = BuilderType.BooleanRadioControl,
     ScreenIDs = new string[] {"BasicInfo.aspx?ScrMemType=1",
                               "BasicInfo.aspx?ScrMemType=2",
                               "BasicInfo.aspx?ScrMemType=3",
                               "BasicInfo.aspx?ScrMemType=5",
                               "BasicInfo.aspx?ScrMemType=6"})]
	[Column(Storage="Confidential", DbType="Bit NOT NULL")]
	public bool? Confidential { get; set; }


	
    

	/// <summary>
	/// DB Column: [Adversary].[Memo].[ClientNumber] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
    //[ScreenBuilder(
    // Caption = "Client Number",
    // BuilderType = BuilderType.TextBox,
    // ScreenIDs = new string[] { /* "CM.aspx?ScrMemType=2", */
    //                           "ClientDetails.aspx?ScrMemType=3",
    //                           "ClientDetails.aspx?ScrMemType=6"},
    // InformationToolTip = "<a href=\"http://h2o.hollandhart.com/C12/CMLookup/default.aspx\" target=\"_blank\">Lookup a client number</a>",
    // FieldOrder=0)]
    private string _clientNumber;
    [Column(Storage = "ClientNumber", DbType = "NVarChar(10)")]
    public string ClientNumber
    {
        get
        {
            return _clientNumber;
        }
        set
        {
            if (_clientNumber != value)
                _clientName = null;
            _clientNumber = value;
        }
    }

    private string _clientName;
    public string ClientName
    {
        get
        {
            if (_clientName == null)
            {
                if (string.IsNullOrEmpty(ClientNumber))
                    return null;

                try
                {
                    CMSData cms = CMSData.GetInstance();
                    _clientName = cms.GetClientName(ClientNumber);

                }
                catch 
                {
                    _clientName = string.Empty;
                }
            }

            return _clientName;
        }
        set
        {
            _clientName = value;
        }
    }


	/// <summary>
	/// DB Column: [Adversary].[Memo].[EmpEntering] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "EmpEntering",
	 BuilderType = BuilderType.TextBox,
     ScreenIDs = new string[] {})]
	[Column(Storage="EmpEntering", DbType="NVarChar(50)")]
	public string EmpEntering { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Memo].[EmpClient] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "EmpClient",
	 BuilderType = BuilderType.TextBox,
	 ScreenIDs = new string[] {})]
	[Column(Storage="EmpClient", DbType="NVarChar(50)")]
	public string EmpClient { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Memo].[EstHours] (int)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: AutoDetermine
	/// </summary>
	[ScreenBuilder(
	 Caption = "EstHours",
	 BuilderType = BuilderType.Auto,
	 ScreenIDs = new string[] {})]
	[Column(Storage="EstHours", DbType="Int(4)")]
	public int? EstHours { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Memo].[RetainerDesc] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "RetainerDesc",
	 BuilderType = BuilderType.TextBox,
	 ScreenIDs = new string[] {})]
	[Column(Storage="RetainerDesc", DbType="NVarChar(50)")]
	public string RetainerDesc { get; set; }

    public enum ProBonoType
    {
        LegalWork=1,
        CivicWork=2,
        ProfessionalGroups=3
    }
	/// <summary>
	/// DB Column: [Adversary].[Memo].[ProBonoTypeID] (int)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: AutoDetermine
	/// </summary>
	[ScreenBuilder(
	 Caption = "Pro Bono Type",
	 BuilderType = BuilderType.DropDownList,
     ListItemText= new string[] {"Legal Work", "Civic Work", "Professional Groups"},
     ListItemValues= new string[] {"1", "2", "3"},
     ScreenIDs = new string[] { "BasicInfo.aspx?ScrMemType=3" },
     FieldOrder=1)]
	[Column(Storage="ProBonoTypeID", DbType="Int(4)")]
	public ProBonoType? ProBonoTypeID { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Memo].[HHLegalWorkID] (int)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: AutoDetermine
	/// </summary>
    //[ScreenBuilder(
    // Caption = "4-digit Payroll Number, if Employee",
    // BuilderType = BuilderType.TextBox,
    // ScreenIDs = new string[] { "LegalWorkType.aspx?ScrMemType=5" },
    // FieldOrder=1)]
	[Column(Storage="HHLegalWorkID", DbType="Int(4)")]
	public int? HHLegalWorkID { get; set; }

    //[ScreenBuilderAccordianControl("HHLegalWorkIsForFirm",
    //    ScreenIDs = new string[] { "LegalWorkType.aspx?ScrMemType=5" },
    //    PaneItemText = new string[] { "Work for H&H As A Firm", "Work for an H&H Employee or Partner " },
    //    PaneItemValues = new string[] { "True", "False" })]
    public bool? HHLegalWorkIsForFirm
    {
        get
        {
            if (HHLegalWorkID == null)
                return null;
            return (HHLegalWorkID == 1);
        }
    }


	/// <summary>
	/// DB Column: [Adversary].[Memo].[MatterName] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextArea
	/// </summary>
    [ScreenBuilder(
     Caption = "Matter Name",
     BuilderType = BuilderType.TextArea,
     ScreenIDs = new string[] {"CM.aspx?ScrMemType=1",
                               "CM.aspx?ScrMemType=2",
                               "CM.aspx?ScrMemType=3",
                               "CM.aspx?ScrMemType=5"},
     FieldOrder=0,
     //Width="300px",
     TextAreaRows=3,
     ValidateAsRequiredField=true)]
	[Column(Storage="MatterName", DbType="NVarChar(255)")]
	public string MatterName { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Memo].[RejectDesc] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
     ///commented by A Reimer 03/21/12 for 'Rejected to Modification Required' change 
     //Caption = "Reason for the Rejection",
     // blmcgee@hollandhart.com 2014-01-10 
     // Caption="Reason for the Modification Request",
     Caption = "Reason for the Rejection",
	 BuilderType = BuilderType.TextArea,
     TextAreaRows=10,
     ScreenIDs = new string[] { "RejectionReason.aspx?ScrMemType=6" })]
	[Column(Storage="RejectDesc", DbType="NVarCharMax")]
	public string RejectDesc { get; set; }


    [Column(Storage = "NoRejectionLetterReason", DbType = "NVarCharMax")]
    public string NoRejectionLetterReason { get; set; }

    [Column(Storage = "NoRejectionLetter", DbType = "bit")]
    public bool? NoRejectionLetter { get; set; }


    /// <summary>
    /// DB Column: [Adversary].[Memo].[RejectDesc] (nvarchar)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: TextBox
    /// </summary>
    [ScreenBuilder(
     Caption = "Please state why you feel that this memo is Pro Bono?",
     BuilderType = BuilderType.TextArea,
     TextAreaRows = 10,
     ScreenIDs = new string[] { "ProBonoReason.aspx?ScrMemType=3" })]
    [Column(Storage = "ProBonoReason", DbType = "NVarCharMax")]
    public string ProBonoReason { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Memo].[NewMatterOnly] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
    [ScreenBuilder(BuilderType = BuilderType.AccordianControl,
     AccordianControlID = "NewMatterOnly",
     Caption = "* If claiming an origination, provide justification including the percentage " +
               "of origination claimed by the associate(s)",
     PaneItemText = new string[] {"New matter for client for which I have previously received " + 
                                  "origination credit",
                                  "Other"},
     PaneItemValues = new string[] {"yes","no"},
     ListRepeatDirection = RepeatDirection.Vertical,
     ScreenIDs = new string[] {"Fees.aspx?ScrMemType=1",
                               "Fees.aspx?ScrMemType=2"})]
	[Column(Storage="NewMatterOnly", DbType="NVarChar(50)")]
	public string NewMatterOnly { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Memo].[OppQuestion] (bit)
	/// Nullable: False
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: BooleanRadioControl
	/// </summary>
	[ScreenBuilder(
	 Caption = "Is there opposing counsel on this matter?",
	 BuilderType = BuilderType.BooleanRadioControl,
	 ScreenIDs = new string[] { "Opposing.aspx?ScrMemType=3" },
     FieldOrder=0)]
	[Column(Storage="OppQuestion", DbType="Bit NOT NULL")]
	public bool? OppQuestion { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Memo].[PatentMatter] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextArea
	/// </summary>
	[ScreenBuilder(
	 Caption = "PatentMatter",
	 BuilderType = BuilderType.TextArea,
	 ScreenIDs = new string[] {})]
	[Column(Storage="PatentMatter", DbType="NVarChar(255)")]
	public string PatentMatter { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Memo].[TreasuryList] (bit)
	/// Nullable: False
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: BooleanRadioControl
	/// </summary>
	[ScreenBuilder(
	 Caption = "TreasuryList",
	 BuilderType = BuilderType.BooleanRadioControl,
	 ScreenIDs = new string[] {})]
	[Column(Storage="TreasuryList", DbType="Bit NOT NULL")]
	public bool? TreasuryList { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Memo].[CommerceList] (bit)
	/// Nullable: False
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: BooleanRadioControl
	/// </summary>
	[ScreenBuilder(
	 Caption = "CommerceList",
	 BuilderType = BuilderType.BooleanRadioControl,
	 ScreenIDs = new string[] {})]
	[Column(Storage="CommerceList", DbType="Bit NOT NULL")]
	public bool? CommerceList { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Memo].[WrittenPolicies] (bit)
	/// Nullable: False
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: BooleanRadioControl
	/// </summary>
    //[ScreenBuilder(
    // Caption = "Does this client or potential client require outside counsel to follow " +
    //           "written policies or procedures relating to billing or other matters?",
    // BuilderType = BuilderType.BooleanRadioControl,
    // ScreenIDs = new string[] {"EngagementLetter.aspx?ScrMemType=1",
    //                           "EngagementLetter.aspx?ScrMemType=2",
    //                           "EngagementLetter.aspx?ScrMemType=3",
    //                           "EngagementLetter.aspx?ScrMemType=5"},
    // FieldOrder=2)]
	[Column(Storage="WrittenPolicies", DbType="Bit NOT NULL")]
	public bool? WrittenPolicies { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Memo].[PendingBankruptcy] (bit)
	/// Nullable: False
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: BooleanRadioControl
	/// </summary>
	[ScreenBuilder(
	 Caption = "Is this client a debtor in a pending bankruptcy?<br/>" +
               "<em>If you do not know, please ask the client.</em>",
	 BuilderType = BuilderType.BooleanRadioControl,
     ScreenIDs = new string[] {"ClientDetails.aspx?ScrMemType=1",
                               "ClientDetails.aspx?ScrMemType=3",
                               "ClientDetails.aspx?ScrMemType=6"})]
	[Column(Storage="PendingBankruptcy", DbType="Bit NOT NULL")]
	public bool? PendingBankruptcy { get; set; }


    private Tracking _tracking = new Tracking();
    [Association(Name = "FK_Tracking_Memo", Storage = "_tracking", OtherKey = "ScrMemID")]
    public Tracking Tracking
    {
        get
        {
            if (this._tracking.TrackingID == null &&
                this.ScrMemID != null)
            {
                this._tracking.TrackingID = 0;
                this._tracking.StatusTypeID = TrackingStatusType.None;
                this._tracking.ScrMemID = this.ScrMemID;
            }
            return _tracking;
        }
        set
        {
            this._tracking = value;
            if (this._tracking.TrackingID == null &&
                this.ScrMemID != null)
            {
                this._tracking.TrackingID = 0;
                this._tracking.StatusTypeID = TrackingStatusType.None;
                this._tracking.ScrMemID = this.ScrMemID;
            }
        }
        //get
        //{
        //    this._tracking.ScrMemID = this.ScrMemID;
        //    return this._tracking;
        //}
        //set
        //{
        //    this._tracking = value;
        //    this._tracking.ScrMemID = this.ScrMemID;
        //}
    }

    /// <summary>
    /// Adding a new field to rejected memos only, a way to associate the new
    /// rejected matter memo to a previously filled out memo
    /// </summary>
    [ScreenBuilder(
        Caption = "Associated Screening Memo ID<br/>" + 
        "<em>Caution! Adding an associated screening memo will overwrite all existing data you've entered for this rejected memo</em>",
        BuilderType = BuilderType.TextBox,
        ScreenIDs = new string[] { "BasicInfo.aspx?ScrMemType=6" },
        FieldOrder = 1
        )]
    [Column(Storage = "AssociatedScrMemID", DbType = "NVarChar(20)")]
    public string AssociatedScrMemID { get; set; }

    [ScreenBuilder(BuilderType = BuilderType.Hidden,
        ScreenIDs = new string[] {"BasicInfo.aspx?ScrMemType=6" })]
    [Column(Storage = "AssociatedTransferred", DbType = "Bit NULL")]
    public bool? AssociatedTransferred { get; set; }


	public Memo()
	{
        //this._addresses = new EntitySet<Address>(new Action<Address>(this.attach_Addresses), new Action<Address>(this.detach_Addresses));
        //this._billings = new EntitySet<Billing>(new Action<Billing>(this.attach_Billings), new Action<Billing>(this.detach_Billings));
        this._clientcontacts = new EntitySet<ClientContact>(new Action<ClientContact>(this.attach_ClientContacts), new Action<ClientContact>(this.detach_ClientContacts));
	    this._feesplitstaffs = new EntitySet<FeeSplitStaff>(new Action<FeeSplitStaff>(this.attach_FeeSplitStaffs), new Action<FeeSplitStaff>(this.detach_FeeSplitStaffs));
        this._attachments = new EntitySet<Attachments>(new Action<Attachments>(this.attach_Attachments), new Action<Attachments>(this.detach_Attachments));
        this._files = new EntitySet<Files>(new Action<Files>(this.attach_Files), new Action<Files>(this.detach_Files));
	    //this._newclients = new EntitySet<NewClient>(new Action<NewClient>(this.attach_NewClients), new Action<NewClient>(this.detach_NewClients));
	    this._opposingcounsels = new EntitySet<OpposingCounsel>(new Action<OpposingCounsel>(this.attach_OpposingCounsels), new Action<OpposingCounsel>(this.detach_OpposingCounsels));
	    this._parties = new EntitySet<Party>(new Action<Party>(this.attach_Parties), new Action<Party>(this.detach_Parties));
	    this._staffing = new EntitySet<Staffing>(new Action<Staffing>(this.attach_Staffings), new Action<Staffing>(this.detach_Staffings));
        this._nDriveAccess = new EntitySet<NDriveAccess>(new Action<NDriveAccess>(this.attach_NDriveAccess), new Action<NDriveAccess>(this.detach_NDriveAccess));
        this._docketingTeam = new EntitySet<DocketingTeam>(new Action<DocketingTeam>(this.attach_DocketingTeam), new Action<DocketingTeam>(this.detach_DocketingTeam));
        this._clientMatterNumbers = new EntitySet<ClientMatterNumber>(new Action<ClientMatterNumber>(this.attach_ClientMatterNumbers), new Action<ClientMatterNumber>(this.detach_ClientMatterNumbers));

        //Default confidential memo to No
        //Confidential = false;
	}

    //private void attach_Addresses(Address entity)
    //{
    //    entity.Memo = this;
    //}
    //private void detach_Addresses(Address entity)
    //{
    //    entity.Memo = null;
    //}


    //private void attach_Billings(Billing entity)
    //{
    //    entity.Memo = this;
    //}
    //private void detach_Billings(Billing entity)
    //{
    //    entity.Memo = null;
    //}


	private void attach_ClientContacts(ClientContact entity)
	{
		entity.Memo = this;
	}
	private void detach_ClientContacts(ClientContact entity)
	{
		entity.Memo = null;
	}


	private void attach_FeeSplitStaffs(FeeSplitStaff entity)
	{
		entity.Memo = this;
	}
	private void detach_FeeSplitStaffs(FeeSplitStaff entity)
	{
		entity.Memo = null;
	}

    private void attach_Attachments(Attachments entity)
    {
        entity.Memo = this;
    }
    private void detach_Attachments(Attachments entity)
    {
        entity.Memo = null;
    }

	private void attach_Files(Files entity)
	{
		entity.Memo = this;
	}
	private void detach_Files(Files entity)
	{
		entity.Memo = null;
	}


    //private void attach_NewClients(NewClient entity)
    //{
    //    entity.Memo = this;
    //}
    //private void detach_NewClients(NewClient entity)
    //{
    //    entity.Memo = null;
    //}


	private void attach_OpposingCounsels(OpposingCounsel entity)
	{
		entity.Memo = this;
	}
	private void detach_OpposingCounsels(OpposingCounsel entity)
	{
		entity.Memo = null;
	}


	private void attach_Parties(Party entity)
	{
		entity.Memo = this;
	}
	private void detach_Parties(Party entity)
	{
		entity.Memo = null;
	}


	private void attach_Staffings(Staffing entity)
	{
		entity.Memo = this;
	}
	private void detach_Staffings(Staffing entity)
	{
		entity.Memo = null;
	}

    private void attach_NDriveAccess(NDriveAccess entity)
    {
        entity.Memo = this;
    }
    private void detach_NDriveAccess(NDriveAccess entity)
    {
        entity.Memo = null;
    }

    private void attach_DocketingTeam(DocketingTeam entity)
    {
        entity.Memo = this;
    }
    private void detach_DocketingTeam(DocketingTeam entity)
    {
        entity.Memo = null;
    }

    private void attach_ClientMatterNumbers(ClientMatterNumber entity)
    {
        entity.Memo = this;
    }
    private void detach_ClientMatterNumbers(ClientMatterNumber entity)
    {
        entity.Memo = null;
    }


    /// <summary>
    /// Throws a ValidationException if the memo does not have valid information
    /// </summary>
    public ValidationException Validate()
    {
        ValidationException ex = new ValidationException("This screening memo is not valid");
        
        //if (this.ScrMemID == null ||
        //    this.ScrMemID == 0)
        //    ex.AddInvalidReason("The screening memo does not have an ID. It has not been saved to the database");

        if (string.IsNullOrEmpty(this.AttEntering))
            ex.AddInvalidReason("Specify an attorney for this memo");
        else
        {
            CMSData cms = CMSData.GetInstance();
            if (cms.DoEmployeeSearch(this.AttEntering).Rows.Count == 0)
                ex.AddInvalidReason("The attorney listed for this memo with employee number " + this.AttEntering + " did not resolve to a valid H&H employee");
        }


        if (string.IsNullOrEmpty(this.PersonnelID))
            ex.AddInvalidReason("Specify the person entering this memo");
        else
        {
            CMSData cms = CMSData.GetInstance();
            if (cms.DoEmployeeSearch(this.PersonnelID).Rows.Count == 0)
                ex.AddInvalidReason("The person listed in the 'input by' field for this memo with employee number " + this.PersonnelID + " did not resolve to a valid H&H employee");
        }

        //check for a client number if the memo type is 2
        if ((this.ScrMemType == 2) && 
             string.IsNullOrEmpty(this.ClientNumber))
            ex.AddInvalidReason("Provide a valid client name or number");

        if (!this.ConflictReport.HasValue)
            ex.AddInvalidReason("Specify whether a conflict report will be sent.");
        else if ((bool)this.ConflictReport &&
                 this.ConflictReportFiles.Count == 0)
            ex.AddInvalidReason("A conflict report has not been attached to this screening memo.");
        else if (!(bool)this.ConflictReport &&
                 string.IsNullOrEmpty(this.ConflictDesc))
            ex.AddInvalidReason("Specify a reason why a conflict report will not be attached.");

        //check for new or existing client info with memos 1,3 and 6
        if (this.ScrMemType == 1 || this.ScrMemType == 3 || this.ScrMemType == 6)
        {
            if (string.IsNullOrEmpty(ClientNumber) && !Company.HasValue)
                ex.AddInvalidReason("Specify whether this is a new or existing client");
            else if (Company.HasValue && (bool)Company && string.IsNullOrEmpty(NewClient.CName))
                ex.AddInvalidReason("Provide the client organization's name");
            else if (Company.HasValue && (bool)Individual && string.IsNullOrEmpty(NewClient.FName) && string.IsNullOrEmpty(NewClient.MName) && string.IsNullOrEmpty(NewClient.LName))
                ex.AddInvalidReason("Provide the client individual's name");
        }


        //this rule added 11/01/2011 by A Reimer to check that if there are affiliates,
        //they must be specified in the parties.
        //if (this.ScrMemType == 1)
        //{
        //    bool hasProperAffiliates = false;
        //    if ((bool)this.NewClient.ClientHasAffiliates)
        //    {
        //        List<string> PartyCodes = new List<string> { "201", "202", "203" };
        //        foreach (Party p in this.Parties)
        //        {
        //            if (PartyCodes.Contains(p.PartyRelationshipCode)) hasProperAffiliates = true;
        //        }
        //    }
        //    if (!hasProperAffiliates)
        //        ex.AddInvalidReason("You have selected that this client has affiliates, but haven't specified any related parties as affiliates.");
        //}

        //check the address fields
        if (this.ScrMemType == 1 || this.ScrMemType == 2 || this.ScrMemType == 3 || this.ScrMemType == 6)
        {
            if (string.IsNullOrEmpty(this.Address.Address1))
                ex.AddInvalidReason("The client address is missing a street");

            if (string.IsNullOrEmpty(this.Address.City))
                ex.AddInvalidReason("The client address is missing a city");

            if (string.IsNullOrEmpty(this.Address.State))
                ex.AddInvalidReason("The client address is missing a state");

            if (string.IsNullOrEmpty(this.Address.Country))
                ex.AddInvalidReason("The client address is missing a country");

            if (string.IsNullOrEmpty(this.Address.Phone))
                ex.AddInvalidReason("The client address is missing a phone number");
        }

        //perform rest of the validation for rejected matter memos and return
        if (this.ScrMemType == 6)
        {
            if (string.IsNullOrEmpty(this.RejectDesc))
                ///commented by A Reimer 03/21/12 for 'Rejected to Modification Required' change
                //ex.AddInvalidReason("Provide a reason why this matter is being rejected");
                // blmcgee@hollandhart.com 2014-01-10
                // ex.AddInvalidReason("Provide a reason why this matter requires modification");
                ex.AddInvalidReason("Provide a reason why this matter is being rejected");
            if (!this.NoRejectionLetter.HasValue)
                ///commented by A Reimer 03/21/12 for 'Rejected to Modification Required' change
                //ex.AddInvalidReason("Specify whether a rejection letter will be attached to this memo");
                // blmcgee@hollandhart.com 2014-01-10
                // ex.AddInvalidReason("Specify whether a request for modification letter will be attached to this memo");
                ex.AddInvalidReason("Specify whether a rejection letter will be attached to this memo");
            else if (this.NoRejectionLetter.Value && string.IsNullOrEmpty(this.NoRejectionLetterReason))
                ///commented by A Reimer 03/21/12 for 'Rejected to Modification Required' change
                //ex.AddInvalidReason("Specify a reason why a rejection letter will not be attached to this memo");
                // blmcgee@hollandhart.com 2014-01-10
                // ex.AddInvalidReason("Specify a reason why a request for modification letter will not be attached to this memo");
                ex.AddInvalidReason("Specify a reason why a rejection letter will not be attached to this memo");
            else if (!this.NoRejectionLetter.Value && this.RejectionLetterFiles.Count == 0)
                ///commented by A Reimer 03/21/12 for 'Rejected to Modification Required' change
                //ex.AddInvalidReason("A rejection letter was not attached to this memo");
                // blmcgee@hollandhart.com 2014-01-10
                // ex.AddInvalidReason("A request for modification letter was not attached to this memo");
                ex.AddInvalidReason("A rejection letter was not attached to this memo");
            return ex;
        }


        if (!this.EngageLetter.HasValue)
            ex.AddInvalidReason("Specify whether an engagement letter will be sent.");
        else if ((bool)this.EngageLetter &&
                 this.EngagementLetterFiles.Count == 0)
            ex.AddInvalidReason("An engagement letter has not been attached to this screening memo.");
        else if (!(bool)this.EngageLetter &&
                 string.IsNullOrEmpty(this.EngageDesc))
            ex.AddInvalidReason("Specify a reason why an engagement letter will not be attached.");

        if (string.IsNullOrEmpty(this.OrigOffice))
            ex.AddInvalidReason("No originating office was selected");

        if (string.IsNullOrEmpty(this.PracCode))
            ex.AddInvalidReason("No practice group was selected for this memo");
        else if (this.PracCode == "101X" ||
                 this.PracCode == "180L" ||
                 this.PracCode == "181L" ||
                 this.PracCode == "182L")
            ex.AddInvalidReason("Practice group codes 101X, 180L, 181L and 182L are no longer valid");

        if (this.ScrMemType == 1 || this.ScrMemType == 2 || this.ScrMemType == 3 || this.ScrMemType == 5)
        {
            CMSData cms = CMSData.GetInstance();
            if (string.IsNullOrEmpty(this.MatterName))
                ex.AddInvalidReason("Specify a matter name");
            if (string.IsNullOrEmpty(this.WorkDesc))
                ex.AddInvalidReason("Provide a description of the Work");

            if (this.NDrive.HasValue && this.NDrive.Value)
            {
                if (!this.NDriveType.HasValue || this.NDriveType.Value < 1)
                    ex.AddInvalidReason("Select the type of N Drive folder required");
                else if (((this.NDriveType.Value == 4 || this.NDriveType.Value == 5) && this.NDriveAccess.Count == 0) && (this.NDriveDoesApply.HasValue && 
                    !(this.NDriveDoesApply.Value == 0 || this.NDriveDoesApply.Value == -1)))
                    ex.AddInvalidReason("Specify users requiring access to the N Drive folder");
            }

            if (DocketingInfoRequired() && (this.ScrMemType != 3 && this.ScrMemType != 5))
            {
                if (string.IsNullOrEmpty(this.DocketingAtty))
                    ex.AddInvalidReason("Specify the Docketing Attorney");
                if (string.IsNullOrEmpty(this.DocketingStaff))
                    ex.AddInvalidReason("Specify the Docketing Staff Member");
            }

            if (string.IsNullOrEmpty(this.RespAttID))
                ex.AddInvalidReason("A billing attorney was not entered");
            else if (cms.GetUserDataByCode(this.RespAttID).Rows.Count != 1)
                ex.AddInvalidReason("Billing attorney '" + this.RespAttID + "' did not resolve to a valid H&H employee");

            //foreach (Staffing staff in this.Staffing)
            //{
            //    DataTable dt = cms.GetUserDataByCode(staff.PersonnelID);
            //    if (dt.Rows.Count != 1)
            //    {
            //        ex.AddInvalidReason("One or more proposed initial staffing entries did not resolve to a valid H&H employee");
            //        break;
            //    }
            //}
        }

        if (this.ScrMemType == 1 || this.ScrMemType == 2)
        {
            if (!this.Retainer.HasValue)
                ex.AddInvalidReason("Specify whether a retainer will be required for this memo.");
            //if (this.Retainer.HasValue && !this.Retainer.Value && string.IsNullOrEmpty(this.RetainerDescNo))
            //    ex.AddInvalidReason("Explain why no retainer will be required for this memo.");

            //CMSData cms = CMSData.GetInstance();
            //foreach (FeeSplitStaff staff in this.FeeSplitStaffs)
            //{
            //    DataTable dt = cms.GetUserDataByCode(staff.PersonnelID);
            //    if (dt.Rows.Count != 1)
            //    {
            //        ex.AddInvalidReason("One or more fee split staff entries did not resolve to a valid H&H employee");
            //        break;
            //    }
            //}
        }

        if (this.ScrMemType == 1  && this.FeeSplitStaffs.Count > 0)
        {
            if (string.IsNullOrEmpty(this.NewMatterOnly))
                ex.AddInvalidReason("Select whether you have previously received origination credit");
            else if (this.NewMatterOnly == "no" && string.IsNullOrEmpty(this.FeeSplitDesc))
                ex.AddInvalidReason("Please give justification for receiving the origination credit");
        }

        if (this.ScrMemType == 3 && string.IsNullOrEmpty(this.ProBonoReason))
            ex.AddInvalidReason("Specify a reason for providing Pro Bono work");

        if (!this.WrittenPolicies.HasValue)
            ex.AddInvalidReason("Specify whether this client requires outside counsel to follow written policies.");
        //else if ((bool)this.WrittenPolicies &&
        //         this.WrittenPolicyFiles.Count == 0)
        //    ex.AddInvalidReason("A written policy has not been attached to this screening memo.");

        return ex;
    }

    public bool IsLocked()
    {
        return (Tracking.Locked.HasValue && Tracking.Locked.Value) || ((ScrMemID != 0) && (ScrMemID < 100000));
    }

    public bool CanSubmit()
    {
        return (!IsLocked() &&
                !Validate().HasInvalidReasons() &&
                ((Tracking.StatusTypeID == TrackingStatusType.None) ||
                 ((Tracking.RejectionHold.HasValue && 
                   Tracking.RejectionHold.Value &&
                   (int)Tracking.StatusTypeID >= 200))));
    }

    public string GetMemoTypeDescription()
    {
        switch (ScrMemType)
        {
            case 1:
                return "New Matter for New Client";
            case 2:
                return "New Matter for Existing Client";
            case 3:
                return "Pro Bono";
            case 5:
                return "Legal Work for H&H or an Employee";
            case 6:
                ///commented by A Reimer 03/21/12 for 'Rejected to Modification Required' change
                //return "Rejected Matter";

                // blmcgee@hollandhart.com 2014-01-10
                // return "Matter Requires Modification";
                return "Rejected Matter";
            default:
                throw new Exception("Screening memo type ID " + ScrMemType + " was not found");
        }
    }

    public bool DocketingInfoRequired()
    {
        //return (!string.IsNullOrEmpty(PracCode) && !PracCode.Contains("E") && !PracCode.Contains("T"));

        if (string.IsNullOrEmpty(PracCode))
            return false;
        if (PracCode.Contains("E") || PracCode.Contains("T"))
            return false;
        if (PracCode == "501L" || PracCode == "502L")
            return false;

        return true;
    }
}
