
using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Web.UI.WebControls;
using HH.Screen;

/// <summary>
/// Summary description for ScreeningMemo
/// This class file was created using the HH DataObjectBuilder
/// </summary>
[Table(Name="FeeSplitStaff")]
public class FeeSplitStaff : IStaff
{

	[Column(Storage = "ScrMemID", DbType = "Int(4)", IsPrimaryKey = true)]
	public int? ScrMemID { get; set; }


	private EntityRef<Memo> _memo = default(EntityRef<Memo>);
	[Association(Name="FK_FeeSplitStaff_Memo", Storage="_memo", ThisKey="ScrMemID", IsForeignKey=true)]
	public Memo Memo
	{
		get
		{
			return this._memo.Entity;
		}
		set
		{
			Memo previousValue = this._memo.Entity;
			if (((previousValue != value)
				|| (this._memo.HasLoadedOrAssignedValue == false)))
			{
				if ((previousValue != null))
				{
					this._memo.Entity = null;
					previousValue.FeeSplitStaffs.Remove(this);
				}
				this._memo.Entity = value;
				if ((value != null))
				{
					value.FeeSplitStaffs.Add(this);
					this.ScrMemID = value.ScrMemID;
				}
				else
				{
					this.ScrMemID = default(Nullable<int>);
				}
			}
		}
	}


	/// <summary>
	/// DB Column: [Adversary].[FeeSplitStaff].[PersonnelID] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
    [ScreenBuilder(
     Caption = "Personnel",
     BuilderType = BuilderType.TextBox,
     EntityControlTargetIDs = new string[] { "Fees" })]
    [Column(Storage = "PersonnelID", DbType = "NVarChar(50)")]
	public string PersonnelID { get; set; }




	public FeeSplitStaff() { }

}
