
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Web.UI.WebControls;
using HH.Screen;
using HH.SQL;


public enum PartyTypes
{
    Individual = 1,
    Organization = 2
}

public enum PartyCodes
{
    Related = 0,
    Adverse = 1
}

/// <summary>
/// Summary description for ScreeningMemo
/// This class file was created using the HH DataObjectBuilder
/// </summary>
[Table(Name="Party")]
public class Party : ISQLMapper
{
    public static DataTable PartyStatus
    {
        get
        {
            CMSData cms = CMSData.GetInstance();
            return cms.PartyStatus;
        }
    }

    private static DataTable _partyStatusRelated = null;
    public static DataTable PartyStatusRelated
    {
        get
        {
            if (_partyStatusRelated == null)
                _partyStatusRelated = SelectRelatedParties(PartyStatus, CMSData.PARTY_STATUS_CODE);
            return _partyStatusRelated;
        }
    }
    public static DataTable SelectRelatedParties(DataTable parties, string columnName)
    {
        //Changed by A Reimer 03/25/12 for new Related Parties change request (ID #4) - added code 102
        IEnumerable<DataRow> query = from p in parties.AsEnumerable()
                    where p.Field<string>(columnName) == "101" ||
                          p.Field<string>(columnName) == "102" || //<--newly added code
                          p.Field<string>(columnName) == "104" ||
                          p.Field<string>(columnName) == "900" ||
                          p.Field<string>(columnName) == "211" ||
                          p.Field<string>(columnName) == "205" ||
                          p.Field<string>(columnName) == "206" ||
                          p.Field<string>(columnName) == "207" ||
                          p.Field<string>(columnName) == "204" ||
                          p.Field<string>(columnName) == "212" ||
                          p.Field<string>(columnName) == "219" ||
                          p.Field<string>(columnName) == "208" ||
                          p.Field<string>(columnName) == "222" ||
                          p.Field<string>(columnName) == "202" ||
                          p.Field<string>(columnName) == "210" ||
                          p.Field<string>(columnName) == "203" ||
                          p.Field<string>(columnName) == "201"
                    select p;
        if (query.Count() == 0)
            return new DataTable();

        return query.CopyToDataTable<DataRow>();
    }

    private static DataTable _partyStatusAdverse = null;
    public static DataTable PartyStatusAdverse
    {
        get
        {
            if (_partyStatusAdverse == null)
                _partyStatusAdverse = SelectAdverseParties(PartyStatus, CMSData.PARTY_STATUS_CODE);
            return _partyStatusAdverse;
        }
    }
    public static DataTable SelectAdverseParties(DataTable parties, string columnName)
    {
        IEnumerable<DataRow> query = from p in parties.AsEnumerable()
                    where p.Field<string>(columnName) == "300" ||
                          p.Field<string>(columnName) == "301" ||
                          p.Field<string>(columnName) == "306" ||
                          p.Field<string>(columnName) == "307" ||
                          p.Field<string>(columnName) == "308" ||
                          p.Field<string>(columnName) == "311" ||
                          p.Field<string>(columnName) == "303" ||
                          p.Field<string>(columnName) == "999"
                    select p;
        if (query.Count() == 0)
            return new DataTable();

        return query.CopyToDataTable<DataRow>();
    }

    /// <summary>
    /// DB Column: [Adversary].[Party].[PartyID] (int)
    /// Nullable: False
    /// In Primary Key: True
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: AutoDetermine
    /// </summary>
    //[ScreenBuilder(
    // Caption = "Party ID #",
    // BuilderType = BuilderType.None,
    // EntityControlTargetIDs = new string[] {"ReferringParty","RelatedParties","AdverseParties"},
    // ScreenIDs = new string[] { "ReferringParty.aspx?ScrMemType=1",
    //                            "ReferringParty.aspx?ScrMemType=3"},
    // CssClass = "boldLabel")]
    [Column(Storage = "PartyID", DbType = "Int(4) NOT NULL", IsPrimaryKey = true)]
    public int? PartyID { get; set; }

	/// <summary>
	/// DB Column: [Adversary].[Party].[ScrMemID] (int)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: True
	/// Identity: False
	/// Screen Builder Control Type: AutoDetermine
	/// </summary>
    //[ScreenBuilder(
    // Caption = "ScrMemID",
    // BuilderType = BuilderType.None,
    // EntityControlTargetIDs = new string[] {"ReferringParty","RelatedParties","AdverseParties"},
    // ScreenIDs = new string[] { "ReferringParty.aspx?ScrMemType=1",
    //                            "ReferringParty.aspx?ScrMemType=3"})]
	[Column(Storage="ScrMemID", DbType="Int(4)")]
	public int? ScrMemID { get; set; }


	private EntityRef<Memo> _memo = default(EntityRef<Memo>);
    [Association(Name="FK_Party_Memo", Storage="_memo", ThisKey="ScrMemID", IsForeignKey=true)]
	public Memo Memo
	{
		get
		{
			return this._memo.Entity;
		}
		set
		{
			Memo previousValue = this._memo.Entity;
			if (((previousValue != value)
				|| (this._memo.HasLoadedOrAssignedValue == false)))
			{
				if ((previousValue != null))
				{
					this._memo.Entity = null;
					previousValue.Parties.Remove(this);
				}
				this._memo.Entity = value;
				if ((value != null))
				{
					value.Parties.Add(this);
					this.ScrMemID = value.ScrMemID;
				}
				else
				{
					this.ScrMemID = default(Nullable<int>);
				}
			}
		}
	}

	/// <summary>
	/// DB Column: [Adversary].[Party].[PartyType] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
    [ScreenBuilder(BuilderType = BuilderType.AccordianControl,
     AccordianControlID = "PartyType",
     Caption = "Party Type",
     EntityControlTargetIDs = new string[] { "ReferringParty", "RelatedParties", "AdverseParties" },
     PaneEnumDataSource = typeof(PartyTypes),
     ScreenIDs = new string[] { "ReferringParty.aspx?ScrMemType=1",
                                "ReferringParty.aspx?ScrMemType=3" })]
	[Column(Storage="PartyType", DbType="NVarChar(50)")]
    public string PartyType { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Party].[PartyFName] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
    [ScreenBuilder(
     AccordianTargetIDs = new string[] {"PartyType"},
     AccordianPaneIDs = new string[] {"Individual"},
	 Caption = "First Name",
     BuilderType = BuilderType.TextBox)]
	[Column(Storage="PartyFName", DbType="NVarChar(50)")]
	public string PartyFName { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Party].[PartyMName] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "Middle Name",
     BuilderType = BuilderType.TextBox,
     AccordianTargetIDs = new string[] {"PartyType"},
     AccordianPaneIDs = new string[] {"Individual"})]
	[Column(Storage="PartyMName", DbType="NVarChar(50)")]
	public string PartyMName { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Party].[PartyLName] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "Last Name",
     BuilderType = BuilderType.TextBox,
     AccordianTargetIDs = new string[] { "PartyType" },
     AccordianPaneIDs = new string[] { "Individual" })]
	[Column(Storage="PartyLName", DbType="NVarChar(50)")]
	public string PartyLName { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Party].[PartyOrganization] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "Organization Name",
	 BuilderType = BuilderType.TextBox,
     AccordianTargetIDs = new string[] {"PartyType" },
     AccordianPaneIDs = new string[] {"Organization" })]
	[Column(Storage="PartyOrganization", DbType="NVarChar(50)")]
	public string PartyOrganization { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Party].[PartyRelationshipName] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// </summary>
    //[ScreenBuilder(
    // BuilderType=BuilderType.Label,
    // Caption="Relationship Type")]
	[Column(Storage="PartyRelationshipName", DbType="NVarChar(50)")]
	public string PartyRelationshipName { get; set; }

    //[ScreenBuilder(
    // BuilderType=BuilderType.Label,
    // Caption="Relationship Code")]
	[Column(Storage="PartyRelationshipCode", DbType="NVarChar(50)")]
    public string PartyRelationshipCode { get; set; }


    /// <summary>
    /// DB Column: [Adversary].[Party].[PartyRelationshipCode] (nvarchar)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: TextBox
    /// </summary>
    [ScreenBuilder(
     Caption = "Party Relationship",
     BuilderType = BuilderType.DropDownList,
     EntityControlTargetIDs = new string[] { "RelatedParties" },
     DataSource = "PartyStatusRelated",
     DataTextField = CMSData.PARTY_STATUS_PLUS_CODE,
     DataValueField = CMSData.PARTY_STATUS_CODE)]
    public string PartyStatusRelatedCode
    {
        get
        {
            return PartyRelationshipCode;
        }
        set
        {
            PartyRelationshipCode = value;
        }
    }

    /// <summary>
    /// DB Column: [Adversary].[Party].[PartyRelationshipCode] (nvarchar)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: TextBox
    /// </summary>
    [ScreenBuilder(
     Caption = "Party Relationship",
     BuilderType = BuilderType.DropDownList,
     EntityControlTargetIDs = new string[] { "AdverseParties" },
     DataSource = "PartyStatusAdverse",
     DataTextField = CMSData.PARTY_STATUS_PLUS_CODE,
     DataValueField = CMSData.PARTY_STATUS_CODE)]
    public string PartyStatusAdverseCode
    {
        get
        {
            return PartyRelationshipCode;
        }
        set
        {
            PartyRelationshipCode = value;
        }
    }

	//private EntitySet<PartyAddressInfo> _partyaddressinfos;
    private PartyAddressInfo _partyaddressinfo = new PartyAddressInfo();
    [ScreenBuilder(BuilderType=BuilderType.EntityAssociation,
        ScreenIDs = new string[] {  "ReferringParty.aspx?ScrMemType=1",
                                    "ReferringParty.aspx?ScrMemType=3" })]
	[Association(Name="FK_PartyAddressInfo_Party", Storage="_partyaddressinfos", OtherKey="PartyID")]
    public PartyAddressInfo PartyAddressInfo //EntitySet<PartyAddressInfo> PartyAddressInfos
	{
		get
		{
            this._partyaddressinfo.PartyID = this.PartyID;
            return this._partyaddressinfo;
		}
		set
		{
            this._partyaddressinfo = value;
            this._partyaddressinfo.PartyID = this.PartyID;
		}
	}


	/// <summary>
	/// DB Column: [Adversary].[Party].[PC] (int)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// </summary>
	[Column(Storage="PC", DbType="Int(4)")]
	public int? PC { get; set; }

    ///// <summary>
    ///// DB Column: [Adversary].[Party].[PartyAffiliates] (int)
    ///// Nullable: True
    ///// In Primary Key: False
    ///// Is Foreign Key: False
    ///// Identity: False
    ///// </summary>
    //[ScreenBuilder(
    // Caption = "Any Affiliates?",
    // BuilderType = BuilderType.TextBox,
    // AccordianTargetIDs = new string[] {"PartyType" },
    // AccordianPaneIDs = new string[] {"Organization" })]
    //[Column(Storage = "PartyAffiliates", DbType = "nvarchar (MAX)")]
    //public string PartyAffiliates { get; set; }





	public Party()
	{
	    //this._partyaddressinfos = new EntitySet<PartyAddressInfo>(new Action<PartyAddressInfo>(this.attach_PartyAddressInfos), new Action<PartyAddressInfo>(this.detach_PartyAddressInfos));
	}

    //private void attach_PartyAddressInfos(PartyAddressInfo entity)
    //{
    //    entity.Party = this;
    //}
    //private void detach_PartyAddressInfos(PartyAddressInfo entity)
    //{
    //    entity.Party = null;
    //}


    public object ConvertPropertyToSQLValue(object parent, string propertyName)
    {
        try
        {
            if (propertyName == "PartyRelationshipName" &&
                PartyRelationshipCode != null &&
                PartyStatus != null)
            {
                CMSData cms = CMSData.GetInstance();
                return cms.GetPartyStatusName(PartyRelationshipCode);
            }
        }
        catch { }
        SQLDefaultMapper mapper = new SQLDefaultMapper();
        return mapper.ConvertPropertyToSQLValue(this, propertyName);
    }

    public void ConvertSQLValueToProperty(object parnet, string propertyName, object value)
    {
        try
        {
            if (propertyName == "PartyRelationshipName" &&
                PartyRelationshipCode != null &&
                PartyStatus != null)
            {
                CMSData cms = CMSData.GetInstance();
                value = cms.GetPartyStatusName(PartyRelationshipCode);
            }
        }
        catch { }
        SQLDefaultMapper mapper = new SQLDefaultMapper();
        mapper.ConvertSQLValueToProperty(this, propertyName, value);
    }

}
