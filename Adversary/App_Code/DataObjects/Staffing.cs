
using System;
using System.Data;
using System.Data.Linq;
using System.Configuration;
using System.Linq;
using System.Data.Linq.Mapping;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using HH.Screen;
/// <summary>
/// Summary description for ScreeningMemo
/// This class file was created using the HH DataObjectBuilder
/// </summary>
[Table(Name="Staffing")]
public class Staffing : IStaff
{

	/// <summary>
	/// DB Column: [Adversary].[Staffing].[ScrMemID] (int)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: True
	/// Identity: False
	/// Screen Builder Control Type: AutoDetermine
	/// </summary>
	[Column(Storage="ScrMemID", DbType="Int(4)", IsPrimaryKey=true )]
	public int? ScrMemID { get; set; }


	private EntityRef<Memo> _memo = default(EntityRef<Memo>);
	[Association(Name="FK_Staffing_Memo", Storage="_memo", ThisKey="ScrMemID", IsForeignKey=true)]
	public Memo Memo
	{
		get
		{
			return this._memo.Entity;
		}
		set
		{
			Memo previousValue = this._memo.Entity;
			if (((previousValue != value)
				|| (this._memo.HasLoadedOrAssignedValue == false)))
			{
				if ((previousValue != null))
				{
					this._memo.Entity = null;
					previousValue.Staffing.Remove(this);
				}
				this._memo.Entity = value;
				if ((value != null))
				{
					value.Staffing.Add(this);
					this.ScrMemID = value.ScrMemID;
				}
				else
				{
					this.ScrMemID = default(Nullable<int>);
				}
			}
		}
	}


	/// <summary>
	/// DB Column: [Adversary].[Staffing].[PersonnelID] (nvarchar)
	/// Nullable: False
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "Personnel",
	 BuilderType = BuilderType.TextBox,
     EntityControlTargetIDs = new string[] { "Staff" })]
	[Column(Storage = "PersonnelID", DbType = "NVarChar(50) NOT NULL IDENTITY", IsDbGenerated = false, AutoSync = AutoSync.Always, IsPrimaryKey = false)]
	public string PersonnelID { get; set; }




	public Staffing() { }

}
