
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Web.UI.WebControls;
using HH.Screen;
using HH.Utils;
using HH;
using System.Reflection;


public enum AdversaryRequestClientCategory
{
    //a reimer 08/09/11 removed "New" at request of Adversary.
    //New, 
    Potential,
    Existing
}


/// <summary>
/// Summary description for ScreeningMemo
/// This class file was created using the HH DataObjectBuilder
/// </summary>
[Table(Name="Requests")]
public class Requests
{


    public IList<AdminLogins> AdversaryGroup
    {
        get
        {
            return AdminLogins.GetAdversaryGroup();
        }
    }


    /// <summary>
    /// Data source for the Practice Codes drop down list
    /// </summary>
    public DataTable PracticeTypes
    {
        get
        {
            CMSData cms = CMSData.GetInstance();
            return cms.PracticeTypes;
        }
    }

    public DataTable ClientTypes
    {
        get
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("CLIENT_TYPE_DESCRIPTION", typeof(String)));
            dt.Columns.Add(new DataColumn("CLIENT_TYPE_VALUE", typeof(String)));
            DataRow dr = dt.NewRow();
            dr["CLIENT_TYPE_DESCRIPTION"] = "Please Select a Client Type...";
            dr["CLIENT_TYPE_VALUE"] = "";
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["CLIENT_TYPE_DESCRIPTION"] = "Potential";
            dr["CLIENT_TYPE_VALUE"] = "Potential";
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["CLIENT_TYPE_DESCRIPTION"] = "Existing";
            dr["CLIENT_TYPE_VALUE"] = "Existing";
            dt.Rows.Add(dr);
            return dt;
        }
    }



    /// <summary>
	/// DB Column: [Adversary].[Requests].[ReqID] (Int)
	/// Nullable: False
	/// In Primary Key: True
	/// Is Foreign Key: False
	/// Identity: True
	/// Screen Builder Control Type: Auto
	/// </summary>
	[ScreenBuilder(
	 Caption = "Request ID #",
	 BuilderType = BuilderType.Label,
     ScreenIDs = new string[] { "BasicInfo.aspx",
                                "EditAdversaryRequest.aspx" },
     FieldOrder=100,
     SummaryViewFieldOrder=100)]
	[Column(Storage="ReqID", DbType="Int(4) NOT NULL IDENTITY", IsDbGenerated=true, AutoSync=AutoSync.Always, IsPrimaryKey=true)]
	public int? ReqID { get; set; }




    /// <summary>
    /// DB Column: [Adversary].[Requests].[SubmittalDate] (DateTime)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: TextBox
    /// </summary>
    [ScreenBuilder(
     Caption = "Submitted On",
     BuilderType = BuilderType.Label,
     ScreenIDs = new string[] { "BasicInfo.aspx",
                                "EditAdversaryRequest.aspx" },
     FieldOrder=101,
     SummaryViewFieldOrder=101)]
    [Column(Storage = "SubmittalDate", DbType = "DateTime(8)")]
    public DateTime? SubmittalDate { get; set; }


    /// <summary>
    /// DB Column: [Adversary].[Requests].[DeadlineDate] (DateTime)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: TextBox
    /// </summary>
    [ScreenBuilder(
     Caption = "Deadline Date",
     BuilderType = BuilderType.Label,
     ScreenIDs = new string[] { "BasicInfo.aspx",
                                "EditAdversaryRequest.aspx" },
    FieldOrder=102,
    SummaryViewFieldOrder=102)]
    [Column(Storage = "DeadlineDate", DbType = "DateTime(8)")]
    public DateTime? DeadlineDate { get; set; }



    /// <summary>
    /// DB Column: [Adversary].[Requests].[AttyLogin] (NVarChar)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: TextBox
    /// </summary>
    [ScreenBuilder(
     Caption = "Attorney Payroll ID",
     BuilderType = BuilderType.TextBox,
     ScreenIDs = new string[] { "EditAdversaryRequest.aspx" },
     FieldOrder = 103,
     SummaryViewFieldOrder=103)]
    [Column(Storage = "AttyLogin", DbType = "NVarChar(50)")]
    public string AttyLogin { get; set; }

    /// <summary>
    /// DB Column: [Adversary].[Requests].[TypistLogin] (NVarChar)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: TextBox
    /// </summary>
    [ScreenBuilder(
     Caption = "Submitted By (Payroll ID)",
     BuilderType = BuilderType.TextBox,
     ScreenIDs = new string[] { "EditAdversaryRequest.aspx" },
     FieldOrder = 104,
     SummaryViewFieldOrder=104)]
    [Column(Storage = "TypistLogin", DbType = "NVarChar(50)")]
    public string TypistLogin { get; set; }


    /// <summary>
    /// DB Column: [Adversary].[Requests].[ClientName] (NVarChar)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: TextArea
    /// </summary>
    [ScreenBuilder(
     Caption = "Client Name",
     BuilderType = BuilderType.TextArea,
     ScreenIDs = new string[] { "BasicInfo.aspx",
                                "EditAdversaryRequest.aspx" },
    FieldOrder = 105,
    SummaryViewFieldOrder=105)]
    [Column(Storage = "ClientName", DbType = "NVarChar(255)")]
    public string ClientName { get; set; }


    /// <summary>
    /// DB Column: [Adversary].[Requests].[ClientCategory] (NVarChar)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: Dropdown List
    /// </summary>
    //[ScreenBuilder(
    // Caption = "Client Status Category",
    // BuilderType = BuilderType.DropDownList,
    // EnumDataSource = typeof(AdversaryRequestClientCategory),
    // ScreenIDs = new string[] { "BasicInfo.aspx",
    //                            "EditAdversaryRequest.aspx" },
    //FieldOrder = 6,
    //ValidateAsRequiredField = true,
    //InformationToolTip = "Please select whether the client is an existing or potential client.",
    //SummaryViewFieldOrder = 6)]
    //[Column(Storage = "ClientCategory", DbType = "NVarChar(50)")]
    //public AdversaryRequestClientCategory? ClientCategory { get; set; }

    [ScreenBuilder(
    Caption = "Client Status Category",
    BuilderType = BuilderType.DropDownList,
    DataSource = "ClientTypes",
    DataTextField="CLIENT_TYPE_DESCRIPTION",
    DataValueField="CLIENT_TYPE_VALUE",
    ScreenIDs = new string[] { "BasicInfo.aspx",
                                "EditAdversaryRequest.aspx" },
   FieldOrder = 106,
   ValidateAsRequiredField = true,
   InformationToolTip = "Please select whether the client is an existing or potential client.",
   SummaryViewFieldOrder = 106)]
    [Column(Storage = "ClientCategory", DbType = "NVarChar(50)")]
    public String ClientCategory { get; set; }





    /// <summary>
    /// DB Column: [Adversary].[Requests].[ClientMatter] (NVarChar)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: TextBox
    /// </summary>
    [ScreenBuilder(
     Caption = "Client Matter Number (if existing client)",
     BuilderType = BuilderType.TextBox,
     ScreenIDs = new string[] { "BasicInfo.aspx",
                                "EditAdversaryRequest.aspx" },
    FieldOrder = 107,
    SummaryViewFieldOrder=107)]
    [Column(Storage = "ClientMatter", DbType = "NVarChar(50)")]
    public string ClientMatter { get; set; }





    /// <summary>
    /// DB Column: [Adversary].[Requests].[PracticeCode] (NVarChar)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: TextBox
    /// </summary>
    [ScreenBuilder(
     Caption = "Practice Type/Specialty Code",
     BuilderType = BuilderType.DropDownList,
     DataSource = "PracticeTypes",
     DataTextField = CMSData.PRACTICE_TYPES_DESCRIPTION_PLUS_CODE,
     DataValueField = CMSData.PRACTICE_TYPES_CODE,
     ScreenIDs = new string[] { "BasicInfo.aspx",
                                "EditAdversaryRequest.aspx" },
     ValidateAsRequiredField = true,
     FieldOrder = 108,
     SummaryViewFieldOrder=108)]
    [Column(Storage = "PracticeCode", DbType = "NVarChar(50)")]
    public string PracticeCode { get; set; }



    private EntitySet<RequestParties> _requestparties;
    [ScreenBuilder(BuilderType = BuilderType.EntityControl,
     EntityControlID = "RequestParties",
     //ScreenIDs = new string[] {"Parties.aspx",
     //                           "EditAdversaryRequest.aspx"},
     Caption = "Parties Related/Adverse To Client",
     AllowInsert = true,
     AllowUpdate = true,
     AllowDelete = true,
     FieldOrder = 109,
     SummaryViewFieldOrder=109)]
    [Association(Name = "FK_RequestParties_Requests", Storage = "_requestparties", OtherKey = "ReqID")]
    public EntitySet<RequestParties> RequestParties
    {
        get
        {
            return this._requestparties;
        }
        set
        {
            this._requestparties.Assign(value);
        }
    }



    /// <summary>
    /// DB Column: [Adversary].[Requests].[OpposingCounsel] (NVarChar)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: TextArea
    /// </summary>
    [ScreenBuilder(
     Caption = "Opposing Counsel",
     BuilderType = BuilderType.TextArea,
     ScreenIDs = new string[] { "Parties.aspx",
                                "EditAdversaryRequest.aspx" },
     FieldOrder=206,
     SummaryViewFieldOrder=206)]
    [Column(Storage = "OpposingCounsel", DbType = "NVarChar(150)")]
    public string OpposingCounsel { get; set; }


    /// <summary>
    /// DB Column: [Adversary].[Requests].[OKforNewMatter] (Bit)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: CheckBox
    /// additional info: if the user selects "Yes" that means "Hide this from the new memo" and
    /// keep the adversary request confidential.  Selecting "No" (leaving the box unchecked) means
    /// that this memo will appear in the published request list.
    /// </summary>
    [ScreenBuilder(
     Caption = "Keep this adversary request confidential?",
     BuilderType = BuilderType.CheckBox,
     ScreenIDs = new string[] { "BasicInfo.aspx",
                                "EditAdversaryRequest.aspx" },
    FieldOrder = 109,
    SummaryViewFieldOrder=109)]
    [Column(Name = "OKforNewMatter", Storage = "DailyMemo", DbType = "Bit")]
    public bool? DailyMemo { get; set; }

	/// <summary>
	/// DB Column: [Adversary].[Requests].[ClientAddress] (NVarChar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextArea
	/// </summary>
	[ScreenBuilder(
	 Caption = "ClientAddress",
	 BuilderType = BuilderType.TextArea,
	 ScreenIDs = new string[] { })]
	[Column(Storage="ClientAddress", DbType="NVarChar(255)")]
	public string ClientAddress { get; set; }




    /// <summary>
    /// DB Column: [Adversary].[Requests].[Priority] (NVarChar)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: TextBox
    /// </summary>
    [ScreenBuilder(
     Caption = "Priority",
     BuilderType = BuilderType.TextBox,
     ScreenIDs = new string[] { })]
    [Column(Storage = "Priority", DbType = "NVarChar(50)")]
    public string Priority { get; set; }



    /// <summary>
    /// DB Column: [Adversary].[Requests].[RelatedParties] (NVarCharMax)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: TextBox
    /// </summary>
    [ScreenBuilder(
     Caption = "Related Parties (not including co-defendants)",
     BuilderType = BuilderType.TextArea,
     ScreenIDs = new string[] { "Parties.aspx", "EditAdversaryRequest.aspx" },
     FieldOrder=203,
     SummaryViewFieldOrder=203)]
    [Column(Storage = "RelatedParties", DbType = "NVarCharMax")]
    public string RelatedParties { get; set; }


    /// <summary>
    /// DB Column: [Adversary].[Requests].[AffiliatesOfAdverseParties] (NVarCharMax)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: TextBox
    /// </summary>
    //[ScreenBuilder(
    // Caption = "Affiliates of Adverse Parties",
    // BuilderType = BuilderType.TextArea,
    // ScreenIDs = new string[] { "Parties.aspx", "EditAdversaryRequest.aspx" },
    // FieldOrder = 201,
    // SummaryViewFieldOrder = 201)]
    //[Column(Storage = "AffiliatesOfAdverseParties", DbType = "NVarCharMax")]
    //public string AffiliatesOfAdverseParties { get; set; }


    /// <summary>
    /// DB Column: [Adversary].[Requests].[OtherClientAffiliates] (NVarCharMax)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: TextBox
    /// </summary>
    //[ScreenBuilder(
    // Caption = "Other Client Affiliates",
    // BuilderType = BuilderType.TextArea,
    // ScreenIDs = new string[] { "Parties.aspx", "EditAdversaryRequest.aspx" },
    // FieldOrder = 204,
    // SummaryViewFieldOrder = 204)]
    //[Column(Storage = "OtherClientAffiliates", DbType = "NVarCharMax")]
    //public string OtherClientAffiliates { get; set; }







    /// <summary>
    /// DB Column: [Adversary].[Requests].[PartiesToMediationArbitration] (NVarCharMax)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: TextBox 
    /// </summary>
    [ScreenBuilder(
        Caption = "Parties to Mediation and Arbitration",
        BuilderType = BuilderType.TextArea,
        ScreenIDs = new string[] { "Parties.aspx", "EditAdversaryRequest.aspx" },
        FieldOrder = 205,
        SummaryViewFieldOrder = 205)]
    [Column(Storage = "PartiesToMediationArbitration", DbType = "NVarCharMax")]
    public string PartiesToMediationArbitration { get; set; }

    /// <summary>
    /// DB Column: [Adversary].[Requests].[AdverseParties] (NVarCharMax)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: TextBox
    /// </summary>
    [ScreenBuilder(
     Caption = "Adverse Parties",
     BuilderType = BuilderType.TextArea,
     ScreenIDs = new string[] { "Parties.aspx", "EditAdversaryRequest.aspx" },
     FieldOrder=200,
     SummaryViewFieldOrder=200)]
    [Column(Storage = "AdverseParties", DbType = "NVarCharMax")]
    public string AdverseParties { get; set; }

    /// <summary>
    /// DB Column: [Adversary].[Requests].[RelatedCoDefendants] (NVarCharMax)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: TextBox
    /// </summary>
    [ScreenBuilder(
     Caption = "Related Co-Defendants",
     BuilderType = BuilderType.TextArea,
     ScreenIDs = new string[] { "Parties.aspx", "EditAdversaryRequest.aspx" },
     FieldOrder = 202,
     SummaryViewFieldOrder = 202)]
    [Column(Storage = "RelatedCoDefendants", DbType = "NVarCharMax")]
    public string RelatedCoDefendants { get; set; }


	



    /// <summary>
    /// DB Column: [Adversary].[Requests].[Communications] (Bit)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: CheckBox
    /// </summary>
    [ScreenBuilder(
     Caption = "This is a telecommunications client or matter.",
     BuilderType = BuilderType.CheckBox,
     ScreenIDs = new string[] { "AdditionalInfo.aspx",
                                "EditAdversaryRequest.aspx" },
     FieldOrder = 301,
     SummaryViewFieldOrder=301)]
    [Column(Storage = "Communications", DbType = "Bit")]
    public bool? Communications { get; set; }

    /// <summary>
    /// DB Column: [Adversary].[Requests].[Insurance] (Bit)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: CheckBox
    /// </summary>
    [ScreenBuilder(
     Caption = "This client is an insurance company.",
     BuilderType = BuilderType.CheckBox,
     ScreenIDs = new string[] { "AdditionalInfo.aspx",
                                "EditAdversaryRequest.aspx" },
     FieldOrder = 302,
     SummaryViewFieldOrder=302)]
    [Column(Storage = "Insurance", DbType = "Bit")]
    public bool? Insurance { get; set; }


    /// <summary>
    /// DB Column: [Adversary].[Requests].[LegalFees] (Bit)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: CheckBox
    /// </summary>
    [ScreenBuilder(
     Caption = "This client's legal fees will be paid by an insurance company <em>or</em> other third party.",
     BuilderType = BuilderType.CheckBox,
     ScreenIDs = new string[] { "AdditionalInfo.aspx",
                                "EditAdversaryRequest.aspx" },
     FieldOrder = 305,
     SummaryViewFieldOrder=305)]
    [Column(Storage = "LegalFees", DbType = "Bit")]
    public bool? LegalFees { get; set; }

	/// <summary>
	/// DB Column: [Adversary].[Requests].[LawFirm] (Bit)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: CheckBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "This client <em>or</em> adversary is a lawyer or a law firm.",
	 BuilderType = BuilderType.CheckBox,
     ScreenIDs = new string[] { "AdditionalInfo.aspx",
                                "EditAdversaryRequest.aspx" },
     FieldOrder = 306,
     SummaryViewFieldOrder=306)]
	[Column(Storage="LawFirm", DbType="Bit")]
	public bool? LawFirm { get; set; }

    /// <summary>
    /// DB Column: [Adversary].[Requests].[SkiResort] (Bit)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: CheckBox
    /// </summary>
    [ScreenBuilder(
     Caption = "This client <em>or</em> adversary is a ski resort.",
     BuilderType = BuilderType.CheckBox,
     ScreenIDs = new string[] { "AdditionalInfo.aspx",
                                "EditAdversaryRequest.aspx" },
     FieldOrder = 307,
     SummaryViewFieldOrder=307)]
    [Column(Storage = "SkiResort", DbType = "Bit")]
    public bool? SkiResort { get; set; }

    /// <summary>
    /// DB Column: [Adversary].[Requests].[Bankruptcy] (Bit)
    /// Nullable: True
    /// Is Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: CheckBox
    /// </summary>
    [ScreenBuilder(
        Caption = "This is a bankruptcy matter.",
        BuilderType = BuilderType.CheckBox,
        ScreenIDs = new string[] { "AdditionalInfo.aspx", "EditAdversaryRequest.aspx" },
        FieldOrder = 300,
        SummaryViewFieldOrder = 300)]
    [Column(Storage = "Bankruptcy", DbType = "Bit")]
    public bool? Bankruptcy { get; set; }

    /// <summary>
    /// DB Column: [Adversary].[Requests].[IndianTribe] (Bit)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: CheckBox
    /// </summary>
    [ScreenBuilder(
     Caption = "This client <em>or</em> adversary is an Indian tribe.",
     BuilderType = BuilderType.CheckBox,
     ScreenIDs = new string[] { "AdditionalInfo.aspx",
                                "EditAdversaryRequest.aspx"},
     FieldOrder = 308,
     SummaryViewFieldOrder=308)]
    [Column(Storage = "IndianTribe", DbType = "Bit")]
    public bool? IndianTribe { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Requests].[Government] (Bit)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: CheckBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "This client is a governmental entity.",
	 BuilderType = BuilderType.CheckBox,
     ScreenIDs = new string[] { "AdditionalInfo.aspx",
                                "EditAdversaryRequest.aspx" },
     FieldOrder = 303,
     SummaryViewFieldOrder=303)]
	[Column(Storage="Government", DbType="Bit")]
	public bool? Government { get; set; }
    

	/// <summary>
	/// DB Column: [Adversary].[Requests].[AccountingFirm] (Bit)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: CheckBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "This adversary is an accounting firm.",
	 BuilderType = BuilderType.CheckBox,
     ScreenIDs = new string[] { "AdditionalInfo.aspx",
                                "EditAdversaryRequest.aspx" },
     FieldOrder = 310,
     SummaryViewFieldOrder=310)]
	[Column(Storage="AccountingFirm", DbType="Bit")]
	public bool? AccountingFirm { get; set; }


    /// <summary>
    /// DB Column: [Adversary].[Requests].[ElectricUtility] (Bit)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: CheckBox
    /// </summary>
    [ScreenBuilder(
     Caption = "This client is an electric utility.",
     BuilderType = BuilderType.CheckBox,
     ScreenIDs = new string[] { "AdditionalInfo.aspx",
                                "EditAdversaryRequest.aspx" },
     FieldOrder = 304,
     SummaryViewFieldOrder=304)]
    [Column(Storage = "ElectricUtility", DbType = "Bit")]
    public bool? ElectricUtility { get; set; }

	/// <summary>
	/// DB Column: [Adversary].[Requests].[OilGas] (Bit)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: CheckBox
	/// </summary>
	[ScreenBuilder(
     Caption = "This client <em>or</em> adversary is in any segment of the oil and gas industry.",
	 BuilderType = BuilderType.CheckBox,
     ScreenIDs = new string[] { "AdditionalInfo.aspx",
                                "EditAdversaryRequest.aspx" },
     FieldOrder = 309,
     SummaryViewFieldOrder=309)]
	[Column(Storage="OilGas", DbType="Bit")]
	public bool? OilGas { get; set; }

    /// <summary>
    /// None of the additional criteria is checked
    /// </summary>
    [ScreenBuilder(
     Caption = "None of the above",
     BuilderType = BuilderType.CheckBox,
     ScreenIDs = new string[] { "AdditionalInfo.aspx",
                                "EditAdversaryRequest.aspx" },
     FieldOrder = 311,
     SummaryViewFieldOrder=311)]
    [Column(Storage = "NoneOfTheAbove", DbType = "Bit")]
    public bool? NoneOfTheAbove { get; set; }


    /// <summary>
    /// DB Column: [Adversary].[Requests].[WorkBegunBy] (NVarChar)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: TextBox
    /// </summary>
    [ScreenBuilder(
     Caption = "Assigned To",
     BuilderType = BuilderType.DropDownList,
     ScreenIDs = new string[] { "EditAdversaryRequest.aspx" },
     DataSource = "AdversaryGroup",
     DataTextField = "Login",
     DataValueField = "Login",
     FieldOrder = 400,
     SummaryViewFieldOrder=400)]
    [Column(Storage = "WorkBegunBy", DbType = "NVarChar(50)")]
    public string WorkBegunBy { get; set; }


    /// <summary>
    /// DB Column: [Adversary].[Requests].[WorkBegunDate] (DateTime)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: TextBox
    /// </summary>
    [ScreenBuilder(
     Caption = "Work Begun Date",
     BuilderType = BuilderType.Label,
     ScreenIDs = new string[] { "EditAdversaryRequest.aspx" },
     FieldOrder = 401,
     SummaryViewFieldOrder=401)]
    [Column(Storage = "WorkBegunDate", DbType = "DateTime(8)")]
    public DateTime? WorkBegunDate { get; set; }


    /// <summary>
    /// DB Column: [Adversary].[Requests].[WorkCompletedBy] (NVarChar)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: TextBox
    /// </summary>
    [ScreenBuilder(
     Caption = "Work Completed By",
     BuilderType = BuilderType.DropDownList,
     ScreenIDs = new string[] { "EditAdversaryRequest.aspx" },
     DataSource = "AdversaryGroup",
     DataTextField = "Login",
     DataValueField = "Login",
     FieldOrder = 402,
     SummaryViewFieldOrder=402)]
    [Column(Storage = "WorkCompletedBy", DbType = "NVarChar(50)")]
    public string WorkCompletedBy { get; set; }



    //[ScreenBuilder(
    // Caption = "Status",
    // BuilderType = BuilderType.Label,
    // ListItemText = new string[] { "Completed", "Pending" },
    // ListItemValues = new string[] { "True", "False" },
    // ScreenIDs = new string[] { "EditAdversaryRequest.aspx" },
    // InformationToolTip = "An employee login must be selected in the Work Completed By drop down " +
    //                      "list to mark the request as complete. Select an employee and then click " +
    //                      "the Mark Complete button complete the request. The status can be switched " +
    //                      "back to pending by clicking the Click to remove Completed Status link when visible.",
    // FieldOrder=13)]
    public bool IsComplete
    {
        get
        {
            return (CompletionDate != null) && !string.IsNullOrEmpty(WorkCompletedBy);
        }
        //set
        //{
        //    if (value)
        //        CompletionDate = DateTime.Now;
        //    else
        //        CompletionDate = null;
        //}
    }

    /// <summary>
    /// DB Column: [Adversary].[Requests].[CompletionDate] (DateTime)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: TextBox
    /// </summary>
    //[ScreenBuilder(
    //    Caption = "Completed On",
    //    BuilderType = BuilderType.Label,
    //    ScreenIDs = new string[] { "EditAdversaryRequest.aspx" },
    //    FieldOrder=14)]
    [Column(Storage = "CompletionDate", DbType = "DateTime(8)")]
    public DateTime? CompletionDate { get; set; }


    /// <summary>
    /// DB Column: [Adversary].[Requests].[Pharm] (Bit)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: CheckBox
    /// </summary>
    [ScreenBuilder(
     Caption = "Pharm",
     BuilderType = BuilderType.CheckBox,
     ScreenIDs = new string[] { })]
    [Column(Storage = "Pharm", DbType = "Bit")]
    public bool? Pharm { get; set; }



	public Requests()
	{
	    this._requestparties = new EntitySet<RequestParties>(new Action<RequestParties>(this.attach_RequestParties), new Action<RequestParties>(this.detach_RequestParties));
        
        //a reimer 08/11/11 - changed to false to accommodate new request
        this.DailyMemo = false;
        //this.DailyMemo = true; //default this checkbox to true for now

	}

	private void attach_RequestParties(RequestParties entity)
	{
		entity.Requests = this;
	}
	private void detach_RequestParties(RequestParties entity)
	{
		entity.Requests = null;
	}

    public ValidationException Validate()
    {
        ValidationException ex = new ValidationException("This adversary request is not valid");

        if (this.DeadlineDate == null ||
            this.DeadlineDate <= DateTime.Now)
            ex.AddInvalidReason("The deadline date for this request is either invalid or has not been entered");

        if (string.IsNullOrEmpty(this.ClientName))
            ex.AddInvalidReason("A client name was not entered");

        if (string.IsNullOrEmpty(this.TypistLogin))
            ex.AddInvalidReason("A name or payroll ID was not specified for this request");

        if (string.IsNullOrEmpty(this.AttyLogin))
            ex.AddInvalidReason("An attorney name or payroll ID was not specified for this request");

        //bool criteriaChecked = ((this.AccountingFirm.HasValue && (bool)this.AccountingFirm) ||
        //                        (this.Communications.HasValue && (bool)this.Communications) ||
        //                        (this.ElectricUtility.HasValue && (bool)this.ElectricUtility) ||
        //                        (this.Government.HasValue && (bool)this.Government) ||
        //                        (this.IndianTribe.HasValue && (bool)this.IndianTribe) ||
        //                        (this.Insurance.HasValue && (bool)this.Insurance) ||
        //                        (this.LawFirm.HasValue && (bool)this.LawFirm) ||
        //                        (this.LegalFees.HasValue && (bool)this.LegalFees) ||
        //                        (this.OilGas.HasValue && (bool)this.OilGas) ||
        //                        (this.Pharm.HasValue && (bool)this.Pharm) ||
        //                        (this.SkiResort.HasValue && (bool)this.SkiResort) ||
        //                        (this.NoneOfTheAbove.HasValue && (bool)this.NoneOfTheAbove));

        bool criteriaChecked = (from bools in this.GetType().GetProperties()
                                where bools.PropertyType == typeof(bool?) && 
                                bools.Name != "DailyMemo" && 
                                (bool?)bools.GetValue(this, null) == true
                                select bools).Count() > 0;
        
        if (!criteriaChecked)
            ex.AddInvalidReason("Please choose a valid criteria for the client or adverse party on the Additional Information page.");

        return ex;
    }

    public bool CanSubmit()
    {
        return  (!Validate().HasInvalidReasons() &&
                 string.IsNullOrEmpty(WorkCompletedBy) &&
                 ((!SubmittalDate.HasValue) ||
                  (SubmittalDate.HasValue && !string.IsNullOrEmpty(WorkBegunBy)))) ;

    }

}
