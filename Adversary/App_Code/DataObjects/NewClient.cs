
using System;
using System.Data;
using System.Data.Linq;
using System.Configuration;
using System.Linq;
using System.Data.Linq.Mapping;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using HH.Screen;
/// <summary>
/// Summary description for ScreeningMemo
/// This class file was created using the HH DataObjectBuilder
/// </summary>
[Table(Name="NewClient")]
public class NewClient
{

	/// <summary>
	/// DB Column: [Adversary].[NewClient].[ScrMemID] (int)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: True
	/// Identity: False
	/// Screen Builder Control Type: AutoDetermine
	/// </summary>
	[Column(Storage="ScrMemID", DbType="Int(4)")]
	public int? ScrMemID { get; set; }


    //private EntityRef<Memo> _memo = default(EntityRef<Memo>);
    //[Association(Name="FK_NewClient_Memo", Storage="_memo", ThisKey="ScrMemID", IsForeignKey=true)]
    //public Memo Memo
    //{
    //    get
    //    {
    //        return this._memo.Entity;
    //    }
    //    set
    //    {
    //        Memo previousValue = this._memo.Entity;
    //        if (((previousValue != value)
    //            || (this._memo.HasLoadedOrAssignedValue == false)))
    //        {
    //            if ((previousValue != null))
    //            {
    //                this._memo.Entity = null;
    //                previousValue.NewClients.Remove(this);
    //            }
    //            this._memo.Entity = value;
    //            if ((value != null))
    //            {
    //                value.NewClients.Add(this);
    //                this.ScrMemID = value.ScrMemID;
    //            }
    //            else
    //            {
    //                this.ScrMemID = default(Nullable<int>);
    //            }
    //        }
    //    }
    //}


	/// <summary>
	/// DB Column: [Adversary].[NewClient].[NwClientID] (int)
	/// Nullable: False
	/// In Primary Key: True
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: AutoDetermine
	/// </summary>
	[Column(Storage="NwClientID", DbType="Int(4) NOT NULL", IsPrimaryKey=true)]
	public int? NwClientID { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[NewClient].[CName] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextArea
	/// </summary>
	[ScreenBuilder(
	 Caption = "Organization Name",
	 BuilderType = BuilderType.TextArea,
     /*ScreenIDs = new string[] {"ClientDetails.aspx?ScrMemType=1"},*/
     AccordianTargetIDs = new string[] {"Company", "HHLegalWorkIsForFirm"},
     AccordianPaneIDs = new string[] { "True", "True" })]
	[Column(Storage="CName", DbType="NVarChar(255)")]
	public string CName { get; set; }


    //added by A Reimer 10/24/2011
    ///<summary>
    /// DBColumn [Adversary].[NewClient].[ClientHasAffiliates] (bit)
    /// Nullable: False
    /// Primary Key: False
    /// Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: Checkbox
    /// </summary>
    [ScreenBuilder(Caption = "Are we representing this client ONLY?",
        BuilderType = BuilderType.BooleanRadioControl,
        ScreenIDs = new string[] { },
        AccordianTargetIDs = new string[] { "Company" },
        AccordianPaneIDs = new string[] { "True", "True" })]
    [Column(Storage = "ClientHasAffiliates", DbType = "Bit NULL")]
    public bool? ClientHasAffiliates
    {
        get;
        set;
    }



	/// <summary>
	/// DB Column: [Adversary].[NewClient].[FName] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "First Name",
	 BuilderType = BuilderType.TextBox,
     /*ScreenIDs = new string[] {"ClientDetails.aspx?ScrMemType=1"},*/
     AccordianTargetIDs = new string[] { "Company", "HHLegalWorkIsForFirm" },
     AccordianPaneIDs = new string[] { "False", "False" })]
	[Column(Storage="FName", DbType="NVarChar(30)")]
	public string FName { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[NewClient].[MName] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "Middle Name",
	 BuilderType = BuilderType.TextBox,
     /*ScreenIDs = new string[] {"ClientDetails.aspx?ScrMemType=1"},*/
     AccordianTargetIDs = new string[] { "Company", "HHLegalWorkIsForFirm" },
     AccordianPaneIDs = new string[] { "False", "False" })]
	[Column(Storage="MName", DbType="NVarChar(50)")]
	public string MName { get; set; }


    /// <summary>
    /// DB Column: [Adversary].[NewClient].[LName] (nvarchar)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// Screen Builder Control Type: TextBox
    /// </summary>
    [ScreenBuilder(
     Caption = "Last Name",
     BuilderType = BuilderType.TextBox,
        /*ScreenIDs = new string[] {"ClientDetails.aspx?ScrMemType=1"},*/
     AccordianTargetIDs = new string[] { "Company", "HHLegalWorkIsForFirm" },
     AccordianPaneIDs = new string[] { "False", "False" })]
    [Column(Storage = "LName", DbType = "NVarChar(30)")]
    public string LName { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[NewClient].[ContactName] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[Column(Storage="ContactName", DbType="NVarChar(50)")]
	public string ContactName { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[NewClient].[ContactTitle] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[Column(Storage="ContactTitle", DbType="NVarChar(50)")]
	public string ContactTitle { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[NewClient].[OriginationComments] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
    [ScreenBuilder(
     Caption = "Please Enter How This Client Came to Select H&H for this work",
     BuilderType = BuilderType.TextArea,
     TextAreaRows=5,
     ScreenIDs = new string[] {"ReferringParty.aspx?ScrMemType=1"})]
	[Column(Storage="OriginationComments", DbType="NVarCharMax")]
	public string OriginationComments { get; set; }




	public NewClient() { }

}
