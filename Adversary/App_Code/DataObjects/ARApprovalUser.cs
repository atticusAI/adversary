﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.Linq.Mapping;
using HH.Screen;
using HH.SQL;
using System.Collections.Generic;
using System.Data.SqlClient;

/// <summary>
/// Summary description for ARApprovalUser
/// </summary>
[Table(Name = "ARApprovalUser")]
public class ARApprovalUser
{
    [ScreenBuilder(
     Caption = "ID #",
     BuilderType = BuilderType.Label)]
    [Column(Storage = "ApprovalUserID", DbType = "Int(4) NOT NULL", IsPrimaryKey = true)]
    public int? ApprovalUserID { get; set; }

    [ScreenBuilder(Caption = "User Name")]
    [Column(Storage = "UserName", DbType = "NVarChar(150) NULL")]
    public string UserName { get; set; }

    [ScreenBuilder(Caption = "Email Address")]
    [Column(Storage = "EmailAddress", DbType = "NVarChar(100) NULL")]
    public string EmailAddress { get; set; }

    public ARApprovalUser() { }

    public static List<ARApprovalUser> GetAllARApprovalRecipients()
    {
        using (SqlConnection conn = AdversaryData.GetConnection())
        {
            DatabaseWorker worker = new DatabaseWorker(conn);
            return worker.RetrieveToList<ARApprovalUser>();
        }
    }
}
