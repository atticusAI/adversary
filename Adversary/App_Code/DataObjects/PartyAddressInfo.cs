
using System;
using System.Data;
using System.Data.Linq;
using System.Configuration;
using System.Linq;
using System.Data.Linq.Mapping;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using HH.Screen;
/// <summary>
/// Summary description for ScreeningMemo
/// This class file was created using the HH DataObjectBuilder
/// </summary>
[Table(Name="PartyAddressInfo")]
public class PartyAddressInfo
{

	/// <summary>
	/// DB Column: [Adversary].[PartyAddressInfo].[PartyAddressID] (int)
	/// Nullable: False
	/// In Primary Key: True
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: AutoDetermine
	/// </summary>
	[Column(Storage="PartyAddressID", DbType="Int(4) NOT NULL", IsPrimaryKey=true)]
	public int? PartyAddressID { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[PartyAddressInfo].[PartyID] (int)
	/// Nullable: False
	/// In Primary Key: False
	/// Is Foreign Key: True
	/// Identity: False
	/// Screen Builder Control Type: AutoDetermine
	/// </summary>
	[Column(Storage="PartyID", DbType="Int(4) NOT NULL")]
	public int? PartyID { get; set; }


    //private EntityRef<Party> _party = default(EntityRef<Party>);
    //[Association(Name="FK_PartyAddressInfo_Party", Storage="_party", ThisKey="PartyID", IsForeignKey=true)]
    //public Party Party
    //{
    //    get
    //    {
    //        return this._party.Entity;
    //    }
    //    set
    //    {
    //        Party previousValue = this._party.Entity;
    //        if (((previousValue != value)
    //            || (this._party.HasLoadedOrAssignedValue == false)))
    //        {
    //            if ((previousValue != null))
    //            {
    //                this._party.Entity = null;
    //                previousValue.PartyAddressInfos.Remove(this);
    //            }
    //            this._party.Entity = value;
    //            if ((value != null))
    //            {
    //                value.PartyAddressInfos.Add(this);
    //                this.PartyID = value.ScrMemID;
    //            }
    //            else
    //            {
    //                this.PartyID = default(Nullable<int>);
    //            }
    //        }
    //    }
    //}


	/// <summary>
	/// DB Column: [Adversary].[PartyAddressInfo].[Address_Line_1] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "Address 1",
	 BuilderType = BuilderType.TextBox,
     ScreenIDs = new string[] { "ReferringParty.aspx?ScrMemType=1",
                                "ReferringParty.aspx?ScrMemType=2"})]
	[Column(Storage="Address_Line_1", DbType="NVarChar(50)")]
	public string Address_Line_1 { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[PartyAddressInfo].[Address_Line_2] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "Address 2",
	 BuilderType = BuilderType.TextBox,
     ScreenIDs = new string[] { "ReferringParty.aspx?ScrMemType=1",
                                "ReferringParty.aspx?ScrMemType=2"})]
	[Column(Storage="Address_Line_2", DbType="NVarChar(50)")]
	public string Address_Line_2 { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[PartyAddressInfo].[Address_Line_3] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "Address 3",
	 BuilderType = BuilderType.TextBox,
     ScreenIDs = new string[] { "ReferringParty.aspx?ScrMemType=1",
                                "ReferringParty.aspx?ScrMemType=2"})]
	[Column(Storage="Address_Line_3", DbType="NVarChar(50)")]
	public string Address_Line_3 { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[PartyAddressInfo].[City] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "City",
	 BuilderType = BuilderType.TextBox,
     ScreenIDs = new string[] { "ReferringParty.aspx?ScrMemType=1",
                                "ReferringParty.aspx?ScrMemType=2"})]
	[Column(Storage="City", DbType="NVarChar(50)")]
	public string City { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[PartyAddressInfo].[State] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "State",
	 BuilderType = BuilderType.TextBox,
     ScreenIDs = new string[] { "ReferringParty.aspx?ScrMemType=1",
                                "ReferringParty.aspx?ScrMemType=2"})]
	[Column(Storage="State", DbType="NVarChar(50)")]
	public string State { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[PartyAddressInfo].[Zip] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "Zip",
	 BuilderType = BuilderType.TextBox,
     ScreenIDs = new string[] { "ReferringParty.aspx?ScrMemType=1",
                                "ReferringParty.aspx?ScrMemType=2"})]
	[Column(Storage="Zip", DbType="NVarChar(50)")]
	public string Zip { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[PartyAddressInfo].[Country] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "Country",
	 BuilderType = BuilderType.TextBox,
     ScreenIDs = new string[] { "ReferringParty.aspx?ScrMemType=1",
                                "ReferringParty.aspx?ScrMemType=2"})]
	[Column(Storage="Country", DbType="NVarChar(50)")]
	public string Country { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[PartyAddressInfo].[Phone] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "Phone",
	 BuilderType = BuilderType.TextBox,
     ScreenIDs = new string[] { "ReferringParty.aspx?ScrMemType=1",
                                "ReferringParty.aspx?ScrMemType=2"})]
	[Column(Storage="Phone", DbType="NVarChar(50)")]
	public string Phone { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[PartyAddressInfo].[Fax] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "Fax",
	 BuilderType = BuilderType.TextBox,
     ScreenIDs = new string[] { "ReferringParty.aspx?ScrMemType=1",
                                "ReferringParty.aspx?ScrMemType=2"})]
	[Column(Storage="Fax", DbType="NVarChar(50)")]
	public string Fax { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[PartyAddressInfo].[EMail] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "Email",
	 BuilderType = BuilderType.TextBox,
     ValidationType=ValidationType.Email,
     ScreenIDs = new string[] { "ReferringParty.aspx?ScrMemType=1",
                                "ReferringParty.aspx?ScrMemType=2"})]
	[Column(Storage="EMail", DbType="NVarChar(50)")]
	public string EMail { get; set; }




	public PartyAddressInfo() { }

}
