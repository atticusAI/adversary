
using System;
using System.Data;
using System.Data.Linq;
using System.Configuration;
using System.Linq;
using System.Data.Linq.Mapping;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using HH.Screen;

/// <summary>
/// Summary description for ScreeningMemo
/// This class file was created using the HH DataObjectBuilder
/// </summary>
[Table(Name="StatusTypes")]
public class StatusTypes
{

	/// <summary>
	/// DB Column: [Adversary].[StatusTypes].[StatusTypeID] (Int)
	/// Nullable: False
	/// In Primary Key: True
	/// Is Foreign Key: False
	/// Identity: False
	/// </summary>
	[ScreenBuilder(
	 Caption = "StatusTypeID",
	 BuilderType = BuilderType.Auto,
	 ScreenIDs = new string[] { })]
	[Column(Storage="StatusTypeID", DbType="Int(4) NOT NULL", IsPrimaryKey=true)]
	public int StatusTypeID { get; set; }


    //private EntitySet<Tracking> _tracking;
    //[Association(Name="FK_Tracking_StatusTypes", Storage="_trackings", OtherKey="StatusTypeID")]
    //public EntitySet<Tracking> Tracking
    //{
    //    get
    //    {
    //        return this._tracking;
    //    }
    //    set
    //    {
    //        this._tracking.Assign(value);
    //    }
    //}


	/// <summary>
	/// DB Column: [Adversary].[StatusTypes].[Status] (Char)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// </summary>
	[ScreenBuilder(
	 Caption = "Status",
	 BuilderType = BuilderType.TextBox,
	 ScreenIDs = new string[] { })]
	[Column(Storage="Status", DbType="Char(20)")]
	public string Status { get; set; }




	public StatusTypes()
	{
	    //this._trackings = new EntitySet<Tracking>(new Action<Tracking>(this.attach_Trackings), new Action<Tracking>(this.detach_Trackings));
	}

	private void attach_Trackings(Tracking entity)
	{
		//entity.StatusTypes = this;
	}

	private void detach_Trackings(Tracking entity)
	{
		//entity.StatusTypes = null;
	}


}
