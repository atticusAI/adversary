
using System;
using System.Data;
using System.Data.Linq;
using System.Configuration;
using System.Linq;
using System.Data.Linq.Mapping;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using System.Data.SqlClient;

/// <summary>
/// Summary description for ScreeningMemo
/// This class file was created using the HH DataObjectBuilder
/// </summary>
[Table(Name="AdminPracCodes")]
public class AdminPracCodes
{

	/// <summary>
	/// DB Column: [Adversary].[AdminPracCodes].[UserID] (Int)
	/// Nullable: False
	/// In Primary Key: False
	/// Is Foreign Key: True
	/// Identity: False
	/// </summary>
	[Column(Storage="UserID", DbType="Int(4) NOT NULL")]
	public int? UserID { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[AdminPracCodes].[PracCode] (NVarChar)
	/// Nullable: False
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// </summary>
	[Column(Storage="PracCode", DbType="NVarChar(5) NOT NULL")]
	public string PracCode { get; set; }


    /// <summary>
    /// DB Column: [Adversary].[AdminPracCodes].[IsPrimary] (Bit)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// </summary>
    [Column(Storage = "IsPrimary", DbType = "Bit")]
    public bool? IsPrimary { get; set; }



    private EntityRef<AdminLogins> _adminlogins = default(EntityRef<AdminLogins>);
    [Association(Name = "FK_AdminPracCodes_AdminLogins", Storage = "_adminlogins", ThisKey = "UserID", IsForeignKey = true)]
    public AdminLogins AdminLogins
    {
        get
        {
            return this._adminlogins.Entity;
        }
        set
        {
            AdminLogins previousValue = this._adminlogins.Entity;
            if (((previousValue != value)
                || (this._adminlogins.HasLoadedOrAssignedValue == false)))
            {
                if ((previousValue != null))
                {
                    this._adminlogins.Entity = null;
                    previousValue.AdminPracCodes.Remove(this);
                }
                this._adminlogins.Entity = value;
                if ((value != null))
                {
                    value.AdminPracCodes.Add(this);
                    this.UserID = value.UserID;
                }
                else
                {
                    this.UserID = default(Nullable<int>);
                }
            }
        }
    }

	public AdminPracCodes() { }


}
