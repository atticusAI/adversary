﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.Linq.Mapping;
using HH.Screen;
using HH.SQL;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.SqlClient;

/// <summary>
/// Summary description for ARApprovalUser
/// </summary>
[Table(Name = "ClientMatterNumbers")]
public class ClientMatterNumber
{
    
    //[ScreenBuilder(
    // Caption = "ID #",
    // BuilderType = BuilderType.Label)]
    [Column(Storage = "ClientMatterID", DbType = "Int(4) NOT NULL IDENTITY", IsDbGenerated = true, AutoSync = AutoSync.Always, IsPrimaryKey = true)]
    public int? ClientMatterID { get; set; }

    //[ScreenBuilder(
    // Caption = "Screening Memo #",
    // BuilderType = BuilderType.Label)]
    [Column(Storage = "ScrMemID", DbType = "Int(4)")]
    public int? ScrMemID { get; set; }

    [ScreenBuilder(Caption = "Client Matter Number")]
    [Column(Storage = "Number", DbType = "NVarChar(150) NULL", Name="ClientMatterNumber")]
    public string Number { get; set; }


    public ClientMatterNumber() { }

    public static List<ARApprovalUser> GetAllARApprovalRecipients()
    {
        using (SqlConnection conn = AdversaryData.GetConnection())
        {
            DatabaseWorker worker = new DatabaseWorker(conn);
            return worker.RetrieveToList<ARApprovalUser>();
        }
    }



    private EntityRef<Memo> _memo = default(EntityRef<Memo>);
    [Association(Name = "FK_ClientMatterNumbers_Memo", Storage = "_memo", ThisKey = "ScrMemID", IsForeignKey = true)]
    public Memo Memo
    {
        get
        {
            return this._memo.Entity;
        }
        set
        {
            Memo previousValue = this._memo.Entity;
            if (((previousValue != value)
                || (this._memo.HasLoadedOrAssignedValue == false)))
            {
                if ((previousValue != null))
                {
                    this._memo.Entity = null;
                    previousValue.ClientMatterNumbers.Remove(this);
                }
                this._memo.Entity = value;
                if ((value != null))
                {
                    value.ClientMatterNumbers.Add(this);
                    this.ScrMemID = value.ScrMemID;
                }
                else
                {
                    this.ScrMemID = default(Nullable<int>);
                }
            }
        }
    }
}
