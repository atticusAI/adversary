
using System;
using System.Data;
using System.Data.Linq;
using System.Configuration;
using System.Linq;
using System.Data.Linq.Mapping;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using HH.Screen;
/// <summary>
/// Summary description for ScreeningMemo
/// This class file was created using the HH DataObjectBuilder
/// </summary>
[Table(Name="OpposingCounsel")]
public class OpposingCounsel
{

	/// <summary>
	/// DB Column: [Adversary].[OpposingCounsel].[opposingID] (int)
	/// Nullable: False
	/// In Primary Key: True
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: AutoDetermine
	/// </summary>
    //[ScreenBuilder(
    // Caption = "opposingID",
    // BuilderType = BuilderType.None,
    // ScreenIDs = new string[] {})]
	[Column(Storage="opposingID", DbType="Int(4) NOT NULL", IsPrimaryKey=true)]
	public int? opposingID { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[OpposingCounsel].[LawfirmName] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextArea
	/// </summary>
	[ScreenBuilder(
	 Caption = "Law Firm Name",
	 BuilderType = BuilderType.TextArea,
     EntityControlTargetIDs = new string[] { "OpposingCounsel" })]
	[Column(Storage="LawfirmName", DbType="NVarChar(150)")]
	public string LawfirmName { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[OpposingCounsel].[LawyerName] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "Lawyer Name",
     BuilderType = BuilderType.TextBox,
     EntityControlTargetIDs = new string[] { "OpposingCounsel" })]
	[Column(Storage="LawyerName", DbType="NVarChar(50)")]
	public string LawyerName { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[OpposingCounsel].[ScrMemID] (int)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: True
	/// Identity: False
	/// Screen Builder Control Type: AutoDetermine
	/// </summary>
    //[ScreenBuilder(
    // Caption = "ScrMemID",
    // BuilderType = BuilderType.None,
    // ScreenIDs = new string[] {})]
	[Column(Storage="ScrMemID", DbType="Int(4)")]
	public int? ScrMemID { get; set; }


	private EntityRef<Memo> _memo = default(EntityRef<Memo>);
	[Association(Name="FK_OpposingCounsel_Memo", Storage="_memo", ThisKey="ScrMemID", IsForeignKey=true)]
	public Memo Memo
	{
		get
		{
			return this._memo.Entity;
		}
		set
		{
			Memo previousValue = this._memo.Entity;
			if (((previousValue != value)
				|| (this._memo.HasLoadedOrAssignedValue == false)))
			{
				if ((previousValue != null))
				{
					this._memo.Entity = null;
					previousValue.OpposingCounsels.Remove(this);
				}
				this._memo.Entity = value;
				if ((value != null))
				{
					value.OpposingCounsels.Add(this);
					this.ScrMemID = value.ScrMemID;
				}
				else
				{
					this.ScrMemID = default(Nullable<int>);
				}
			}
		}
	}




	public OpposingCounsel() { }

}
