
using System;
using System.Data;
using System.Data.Linq;
using System.Configuration;
using System.Linq;
using System.Data.Linq.Mapping;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using HH.Screen;
/// <summary>
/// Summary description for ScreeningMemo
/// This class file was created using the HH DataObjectBuilder
/// </summary>
[Table(Name="Billing")]
public class Billing
{

	/// <summary>
	/// DB Column: [Adversary].[Billing].[ScrMemID] (int)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: True
	/// Identity: False
	/// Screen Builder Control Type: AutoDetermine
	/// </summary>
	[ScreenBuilder(
	 Caption = "ScrMemID",
	 BuilderType = BuilderType.Auto,
	 ScreenIDs = new string[] {})]
	[Column(Storage="ScrMemID", DbType="Int(4)")]
	public int? ScrMemID { get; set; }


    //private EntityRef<Memo> _memo = default(EntityRef<Memo>);
    //[Association(Name="FK_Billing_Memo", Storage="_memo", ThisKey="ScrMemID", IsForeignKey=true)]
    //public Memo Memo
    //{
    //    get
    //    {
    //        return this._memo.Entity;
    //    }
    //    set
    //    {
    //        Memo previousValue = this._memo.Entity;
    //        if (((previousValue != value)
    //            || (this._memo.HasLoadedOrAssignedValue == false)))
    //        {
    //            if ((previousValue != null))
    //            {
    //                this._memo.Entity = null;
    //                previousValue.Billings.Remove(this);
    //            }
    //            this._memo.Entity = value;
    //            if ((value != null))
    //            {
    //                value.Billings.Add(this);
    //                this.ScrMemID = value.ScrMemID;
    //            }
    //            else
    //            {
    //                this.ScrMemID = default(Nullable<int>);
    //            }
    //        }
    //    }
    //}


	/// <summary>
	/// DB Column: [Adversary].[Billing].[BillingID] (int)
	/// Nullable: False
	/// In Primary Key: True
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: AutoDetermine
	/// </summary>
	[Column(Storage="BillingID", DbType="Int(4) NOT NULL", IsPrimaryKey=true)]
	public int? BillingID { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Billing].[SpecialFee] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
     Caption = "Please describe any special fee arrangements",
	 BuilderType = BuilderType.TextArea,
	 ScreenIDs = new string[] {"BillingInfo.aspx?ScrMemType=1",
                               "BillingInfo.aspx?ScrMemType=2",
                               "BillingInfo.aspx?ScrMemType=5"})]
	[Column(Storage="SpecialFee", DbType="NVarCharMax")]
	public string SpecialFee { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Billing].[BillInstructions] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
     Caption = "Special billing instructions",
     BuilderType = BuilderType.TextArea,
     ScreenIDs = new string[] {"BillingInfo.aspx?ScrMemType=1",
                               "BillingInfo.aspx?ScrMemType=2",
                               "BillingInfo.aspx?ScrMemType=5"})]
	[Column(Storage="BillInstructions", DbType="NVarCharMax")]
	public string BillInstructions { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Billing].[BillFormat] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
     Caption = "Name of special bill format",
	 BuilderType = BuilderType.TextBox,
     InformationToolTip="Contact a billing specialist in accounting if you need help with a bill format.",
     ScreenIDs = new string[] {"BillingInfo.aspx?ScrMemType=1",
                               "BillingInfo.aspx?ScrMemType=2",
                               "BillingInfo.aspx?ScrMemType=5"})]
	[Column(Storage="BillFormat", DbType="NVarChar(50)")]
	public string BillFormat { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Billing].[ClientLevelBill] (bit)
	/// Nullable: False
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: BooleanRadioControl
	/// </summary>
	[ScreenBuilder(
     Caption = "Client level billing?",
	 BuilderType = BuilderType.BooleanRadioControl,
     ScreenIDs = new string[] {"BillingInfo.aspx?ScrMemType=1",
                               "BillingInfo.aspx?ScrMemType=2",
                               "BillingInfo.aspx?ScrMemType=5"})]
	[Column(Storage="ClientLevelBill", DbType="Bit NOT NULL")]
	public bool? ClientLevelBill { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Billing].[LateInterest] (bit)
	/// Nullable: False
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: BooleanRadioControl
	/// </summary>
	[ScreenBuilder(
     Caption = "Interest on late payments?",
     BuilderType = BuilderType.BooleanRadioControl,
     ScreenIDs = new string[] {"BillingInfo.aspx?ScrMemType=1",
                               "BillingInfo.aspx?ScrMemType=2",
                               "BillingInfo.aspx?ScrMemType=5"})]
	[Column(Storage="LateInterest", DbType="Bit NOT NULL")]
	public bool? LateInterest { get; set; }




	public Billing() { }

}
