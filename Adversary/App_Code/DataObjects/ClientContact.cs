
using System;
using System.Data;
using System.Data.Linq;
using System.Configuration;
using System.Linq;
using System.Data.Linq.Mapping;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using HH.Screen;
/// <summary>
/// Summary description for ScreeningMemo
/// This class file was created using the HH DataObjectBuilder
/// </summary>
[Table(Name="ClientContact")]
public class ClientContact
{

	/// <summary>
	/// DB Column: [Adversary].[ClientContact].[ContactID] (int)
	/// Nullable: False
	/// In Primary Key: True
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: AutoDetermine
	/// </summary>
	[ScreenBuilder(
	 Caption = "Contact ID",
	 BuilderType = BuilderType.Label,
	 EntityControlTargetIDs = new string[] {"Contacts"},
     FieldOrder=0,
     CssClass = "boldLabel")]
	[Column(Storage="ContactID", DbType="Int(4) NOT NULL", IsPrimaryKey=true)]
	public int? ContactID { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[ClientContact].[ContactFName] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "First Name",
	 BuilderType = BuilderType.TextBox,
     EntityControlTargetIDs = new string[] {"Contacts"})]
	[Column(Storage="ContactFName", DbType="NVarChar(50)")]
	public string ContactFName { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[ClientContact].[ContactMName] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "Middle Name",
	 BuilderType = BuilderType.TextBox,
     EntityControlTargetIDs = new string[] {"Contacts"})]
	[Column(Storage="ContactMName", DbType="NVarChar(50)")]
	public string ContactMName { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[ClientContact].[ContactLName] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "Last Name",
	 BuilderType = BuilderType.TextBox,
     EntityControlTargetIDs = new string[] {"Contacts"})]
	[Column(Storage="ContactLName", DbType="NVarChar(50)")]
	public string ContactLName { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[ClientContact].[ContactTitle] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "Contact's Title",
	 BuilderType = BuilderType.TextBox,
     EntityControlTargetIDs = new string[] { "Contacts" })]
	[Column(Storage="ContactTitle", DbType="NVarChar(50)")]
	public string ContactTitle { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[ClientContact].[ScrMemID] (int)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: True
	/// Identity: False
	/// Screen Builder Control Type: AutoDetermine
	/// </summary>
    //[ScreenBuilderControl(
    // Caption = "ScrMemID",
    // ControlType = ScreenBuilderControlType.Label,
    // EntityControlTargetID = "Contacts",
    // FieldOrder=0)]
	[Column(Storage="ScrMemID", DbType="Int(4)")]
	public int? ScrMemID { get; set; }


	private EntityRef<Memo> _memo = default(EntityRef<Memo>);
	[Association(Name="FK_ClientContact_Memo", Storage="_memo", ThisKey="ScrMemID", IsForeignKey=true)]
	public Memo Memo
	{
		get
		{
			return this._memo.Entity;
		}
		set
		{
			Memo previousValue = this._memo.Entity;
			if (((previousValue != value)
				|| (this._memo.HasLoadedOrAssignedValue == false)))
			{
				if ((previousValue != null))
				{
					this._memo.Entity = null;
					previousValue.ClientContacts.Remove(this);
				}
				this._memo.Entity = value;
				if ((value != null))
				{
					value.ClientContacts.Add(this);
					this.ScrMemID = value.ScrMemID;
				}
				else
				{
					this.ScrMemID = default(Nullable<int>);
				}
			}
		}
	}

	public ClientContact() 
    {
        //ContactID = 0;
    }

}
