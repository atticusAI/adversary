
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Web.UI.WebControls;
using HH.Screen;
using HH.Utils;

using System.Data.SqlClient;
using HH.SQL;


/// <summary>
/// Summary description for ScreeningMemo
/// This class file was created using the HH DataObjectBuilder
/// </summary>
[Table(Name="AdminLogins")]
public class AdminLogins
{
    //private static bool _adminsLoaded = false;
    //private static EntitySet<AdminLogins> _allAdmins = new EntitySet<AdminLogins>();
    private static AdminLogins[] _admins = null;
    //[ScreenBuilderEntityControl("Users",
    // Caption = "Users",
    // ScreenIDs = new string[] {"Users"},
    // AllowInsert = true,
    // AllowUpdate = true,
    // AllowDelete = true)]
    public static AdminLogins[] AllAdmins
    {
        get
        {
            if (_admins == null)
                RefreshAdminList();
            
            return _admins;
        }
    }    
    
	/// <summary>
	/// DB Column: [Adversary].[AdminLogins].[UserID] (Int)
	/// Nullable: False
	/// In Primary Key: True
	/// Is Foreign Key: False
	/// Identity: True
	/// </summary>
	[Column(Storage="UserID", DbType="Int(4) NOT NULL IDENTITY", IsDbGenerated=true, AutoSync=AutoSync.Always, IsPrimaryKey=true)]
	public int? UserID { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[AdminLogins].[Login] (VarChar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// </summary>
	[Column(Storage="Login", DbType="VarChar(50)")]
	public string Login { get; set; }


    /// <summary>
    /// DB Column: [Adversary].[AdminLogins].[Email] (VarChar)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// </summary>
    [Column(Storage = "Email", DbType = "VarChar(50)")]
    public string Email { get; set; }


    /// <summary>
    /// Data source for the offices drop down list
    /// </summary>
    public static DataTable Offices
    {
        get
        {
            CMSData cms = CMSData.GetInstance();
            return cms.AllOffices;
        }
    }

    /// <summary>
    /// DB Column: [Adversary].[AdminLogins].[Office] (VarChar)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// </summary>
    [Column(Storage = "Office", DbType = "int")]
    public int? Office { get; set; }



	/// <summary>
	/// DB Column: [Adversary].[AdminLogins].[IsAP] (Bit)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// </summary>
	[Column(Storage="IsAP", DbType="Bit")]
	public bool? IsAP { get; set; }


    /// <summary>
    /// DB Column: [Adversary].[AdminLogins].[IsPrimaryAP] (Bit)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// </summary>
    [Column(Storage = "IsPrimaryAP", DbType = "Bit")]
    public bool? IsPrimaryAP { get; set; }

	/// <summary>
	/// DB Column: [Adversary].[AdminLogins].[IsPGM] (Bit)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// </summary>
	[Column(Storage="IsPGM", DbType="Bit")]
	public bool? IsPGM { get; set; }

    /// <summary>
    /// DB Column: [Adversary].[AdminLogins].[IsAdversary] (Bit)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// </summary>
    [Column(Storage = "IsAdversary", DbType = "Bit")]
    public bool? IsAdversary { get; set; }

    /// <summary>
    /// DB Column: [Adversary].[AdminLogins].[IsAdversaryAdmin] (Bit)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// </summary>
    [Column(Storage = "IsAdversaryAdmin", DbType = "Bit")]
    public bool? IsAdversaryAdmin { get; set; }


    /// <summary>
    /// DB Column: [Adversary].[AdminLogins].[IsPGM] (Bit)
    /// Nullable: True
    /// In Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// </summary>
    [Column(Storage = "SortOrder", DbType = "Int(4)")]
    public int? SortOrder { get; set; }

    /// <summary>
    /// DB Column: [Adversary].[dbo].[IsFinancialAdmin] (bit)
    /// Nullable: True
    /// Is Primary Key: False
    /// Is Foreign Key: False
    /// Identity: False
    /// </summary>
    //[Column(Storage = "IsFinancialAdmin", DbType = "Bit")]
    //public bool? IsFinancialAdmin { get; set; }




    private string _name;
    public string Name
    {
        get
        {
            if (_name == null)
                SetEmployeeData();

            return _name;
        }
    }

    private string _empId;
    public string EmployeeID
    {
        get
        {
            if (_empId == null)
                SetEmployeeData();

            return _empId;
        }
    }


    private EntitySet<AdminPracCodes> _pracCodes;
    [Association(Name = "FK_AdminPracCodes_AdminLogins", Storage = "_pracCodes", OtherKey = "UserID")]
    public EntitySet<AdminPracCodes> AdminPracCodes
    {
        get
        {
            return this._pracCodes;
        }
        set
        {
            this._pracCodes.Assign(value);
        }
    }

    public AdminLogins() 
    {
        this._pracCodes = new EntitySet<AdminPracCodes>(new Action<AdminPracCodes>(this.attach_AdminPracCodes), new Action<AdminPracCodes>(this.detach_AdminPracCodes));
    }


    private void attach_AdminPracCodes(AdminPracCodes entity)
    {
        entity.AdminLogins = this;
    }
    private void detach_AdminPracCodes(AdminPracCodes entity)
    {
        entity.AdminLogins = null;
    }

    private void SetEmployeeData()
    {
        if (string.IsNullOrEmpty(Login))
            return;

        try
        {
            CMSData cms = CMSData.GetInstance();
            DataTable dt = cms.GetUserDataByLogin(Login);
            DataRow dr = dt.Rows[0];
            _name = dr[CMSData.EMP_NAME].ToString();
            _empId = dr[CMSData.EMP_CODE].ToString();
        }
        catch
        {
            _name = string.Empty;
            _empId = string.Empty;
        }
    }

    public bool IsAPForOffice(string officeName)
    {
        if (string.IsNullOrEmpty(officeName))
            throw new ArgumentException("Could not determine if " + Login + " is AP. Office name was not specified");
        if (!IsAP.HasValue || !IsAP.Value)
            return false;

        CMSData cms = CMSData.GetInstance();
        string officeCode = cms.GetOfficeCode(officeName);
        return (Int32.Parse(officeCode) == Office);
    }

    public bool IsMemberOfPracticeGroup(string pracCode)
    {
        if (!IsPGM.HasValue || !IsPGM.Value)
            return false;

        var query = from p in this.AdminPracCodes
                    where p.PracCode == pracCode
                    select p;

        return (query.Count() > 0);
    }

    public void RemovePracticeGroupMembership()
    {
        //using (SqlConnection conn = AdversaryData.GetConnection())
        //using (DatabaseWorker worker = DatabaseWorker.GetInstance(conn))
        //{
        //    //foreach (AdminPracCodes code in this.AdminPracCodes)
        //    //    worker.Delete(code);

            
        //}

        using (SqlConnection conn = AdversaryData.GetConnection())
        {
            //DatabaseWorker worker = AdversaryData.GetInstance();
            DatabaseWorker worker = new DatabaseWorker(conn);
            AdminPracCodes codes = new AdminPracCodes();
            codes.UserID = this.UserID;
            worker.Delete(codes);
            this.AdminPracCodes.Clear();

        }
        

        //using (SqlConnection conn = new SqlConnection(AdversaryData.ConnectionString))
        //{
        //    conn.Open();
        //    SqlCommand cmd = worker.BuildStoredProcedure(new AdminPracCodes(), SPType.Delete);
        //    cmd.Parameters.AddWithValue("UserID", this.UserID);
        //    //cmd.Parameters.AddWithValue("PracCode", code.PracCode);
        //    cmd.Connection = conn;
        //    cmd.ExecuteNonQuery();
        //}
    }




    #region STATIC METHODS
    public static AdminLogins GetAdminLoginFromLoginName(string login)
    {
        var query = from a in AllAdmins
                    where a.Login.ToLower() == login.ToLower()
                    select a;

        if (query.Count() == 0)
            throw new ArgumentException("Could not lookup data for login " + login);

        return query.FirstOrDefault();
    }

    public static AdminLogins[] GetAPsByOffice(string officeCode, bool primaryOnly)
    {
        var query = from a in AllAdmins
                    where (a.Office == Int32.Parse(officeCode)) &&
                          (a.IsAP.HasValue && a.IsAP.Value) &&
                          (!primaryOnly || (a.IsPrimaryAP.HasValue && a.IsPrimaryAP.Value))
                    select a;

        return query.ToArray();
    }

    public static AdminLogins[] GetAPsByOfficeName(string officeName, bool primaryOnly)
    {
        if (string.IsNullOrEmpty(officeName))
            throw new ArgumentException("Could not get APs by office name. Office name was not specified");

        CMSData cms = CMSData.GetInstance();
        string officeCode = cms.GetOfficeCode(officeName);
        var query = from a in AllAdmins
                    where (a.Office == Int32.Parse(officeCode)) &&
                          (a.IsAP.HasValue && a.IsAP.Value) &&
                          (!primaryOnly || (a.IsPrimaryAP.HasValue && a.IsPrimaryAP.Value))
                    select a;

        return query.ToArray();
    }

    public static AdminLogins[] GetPGMsByPracticeCode(string pracCode, bool primaryOnly)
    {
        var query = from a in AllAdmins
                    from p in a.AdminPracCodes
                    where (p.PracCode == pracCode) &&
                          (a.IsPGM.HasValue && a.IsPGM.Value) &&
                          (!primaryOnly || (p.IsPrimary.HasValue && p.IsPrimary.Value))
                    select a;

        return query.ToArray();
    }

    //public static AdminLogins[] GetAPsAndPGMs(Memo memo, bool primaryOnly) { return GetAPsAndPGMs(Int32.Parse(memo.OrigOffice), memo.PracCode, primaryOnly); }
    //public static AdminLogins[] GetAPsAndPGMs(int origOffice, string pracCode, bool primaryOnly)
    //{

    //    var query = from a in AllAdmins
    //                from p in a.AdminPracCodes
    //                where ( 
    //                        (a.Office == origOffice) && 
    //                        (a.IsAP.HasValue && a.IsAP.Value) && 
    //                        (!primaryOnly || (primaryOnly && a.IsPrimaryAP.HasValue && a.IsPrimaryAP.Value))
    //                      ) 
    //                      ||
    //                      ( 
    //                        (p.PracCode == pracCode) &&
    //                        (a.IsPGM.HasValue && a.IsPGM.Value) &&
    //                        (!primaryOnly || (primaryOnly && p.IsPrimary.HasValue && p.IsPrimary.Value))
    //                      )

    //                select a;

    //    return query.ToArray();
    //}

    public static AdminLogins[] GetAllAPs()
    {
        var query = from a in AllAdmins
                    where a.IsAP.HasValue && a.IsAP.Value
                    select a;

        return query.ToArray();
    }

    public static AdminLogins[] GetAllPGMs()
    {
        var query = from a in AllAdmins
                    where a.IsPGM.HasValue && a.IsPGM.Value
                    select a;

        return query.ToArray();
    }

    public static AdminLogins[] GetAdversaryGroup()
    {
        var query = from a in AllAdmins
                    where a.IsAdversary.HasValue && a.IsAdversary.Value
                    select a;

        return query.ToArray();
    }

    public static AdminLogins[] GetAdversaryGroupAdmins()
    {
        var query = from a in AllAdmins
                    where (a.IsAdversary.HasValue && a.IsAdversary.Value) &&
                          (a.IsAdversaryAdmin.HasValue && a.IsAdversaryAdmin.Value)
                    select a;

        return query.ToArray();
    }

    public static AdminLogins GetAdminLoginFromID(int? userId)
    {
        var query = from a in AllAdmins
                    where a.UserID == userId
                    select a;

        if (query.Count() == 0)
            throw new ArgumentException("Could not lookup data for User ID " + userId);

        return query.FirstOrDefault();
    }

    public static string GetAdminLoginNameFromID(int? userId)
    {
        return GetAdminLoginFromID(userId).Login;
    }

    public static void RefreshAdminList()
    {
        using (SqlConnection conn = AdversaryData.GetConnection())
        {
            DatabaseWorker worker = new DatabaseWorker(conn);
            _admins = worker.RetrieveToList<AdminLogins>("PR_GET_AdminLogins").ToArray(); //worker.Search<AdminLogins>(new AdminLogins());
        }
    }
    #endregion
}
