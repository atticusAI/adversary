
using System;
using System.Data;
using System.Data.Linq;
using System.Configuration;
using System.Linq;
using System.Data.Linq.Mapping;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using HH.Screen;

/// <summary>
/// Summary description for ScreeningMemo
/// This class file was created using the HH DataObjectBuilder
/// </summary>
[Table(Name="Address")]
public class Address
{

	/// <summary>
	/// DB Column: [Adversary].[Address].[ScrMemID] (int)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: True
	/// Identity: False
	/// Screen Builder Control Type: AutoDetermine
	/// </summary>
	[ScreenBuilder(
	 Caption = "ScrMemID",
	 BuilderType = BuilderType.Auto,
	 ScreenIDs = new string[] {},
     CssClass="boldLabel")]
	[Column(Storage="ScrMemID", DbType="Int(4)")]
	public int? ScrMemID { get; set; }


    //private EntityRef<Memo> _memo = default(EntityRef<Memo>);
    //[Association(Name="FK_Address_Memo1", Storage="_memo", ThisKey="ScrMemID", IsForeignKey=true)]
    //public Memo Memo
    //{
    //    get
    //    {
    //        return this._memo.Entity;
    //    }
    //    set
    //    {
    //        Memo previousValue = this._memo.Entity;
    //        if (((previousValue != value)
    //            || (this._memo.HasLoadedOrAssignedValue == false)))
    //        {
    //            if ((previousValue != null))
    //            {
    //                this._memo.Entity = null;
    //                previousValue.Addresses.Remove(this);
    //            }
    //            this._memo.Entity = value;
    //            if ((value != null))
    //            {
    //                value.Addresses.Add(this);
    //                this.ScrMemID = value.ScrMemID;
    //            }
    //            else
    //            {
    //                this.ScrMemID = default(Nullable<int>);
    //            }
    //        }
    //    }
    //}


	/// <summary>
	/// DB Column: [Adversary].[Address].[AddressID] (int)
	/// Nullable: False
	/// In Primary Key: True
	/// Is Foreign Key: False
	/// Identity: True
	/// Screen Builder Control Type: AutoDetermine
	/// </summary>
    //[ScreenBuilder(
    // Caption = "Address #",
    // BuilderType = BuilderType.Label,
    // ScreenIDs = new string[] {"ClientAddress.aspx?ScrMemType=1",
    //                           "ClientAddress.aspx?ScrMemType=2",
    //                           "ClientAddress.aspx?ScrMemType=3",
    //                           "ClientAddress.aspx?ScrMemType=6"},
    // CssClass="boldLabel")]
	[Column(Storage="AddressID", DbType="Int(4) NOT NULL IDENTITY", IsDbGenerated=true, AutoSync=AutoSync.Always, IsPrimaryKey=true)]
	public int? AddressID { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Address].[Address1] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "Address",
	 BuilderType = BuilderType.TextBox,
	 ScreenIDs = new string[] {"ClientAddress.aspx?ScrMemType=1",
                               "ClientAddress.aspx?ScrMemType=2",
                               "ClientAddress.aspx?ScrMemType=3",
                               "ClientAddress.aspx?ScrMemType=6"},
     ValidateAsRequiredField = true)]
	[Column(Storage="Address1", DbType="NVarChar(50)")]
	public string Address1 { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Address].[Address2] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "",
	 BuilderType = BuilderType.TextBox,
     ScreenIDs = new string[] {"ClientAddress.aspx?ScrMemType=1",
                               "ClientAddress.aspx?ScrMemType=2",
                               "ClientAddress.aspx?ScrMemType=3",
                               "ClientAddress.aspx?ScrMemType=6"})]
	[Column(Storage="Address2", DbType="NVarChar(50)")]
	public string Address2 { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Address].[Address3] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "",
	 BuilderType = BuilderType.TextBox,
     ScreenIDs = new string[] {"ClientAddress.aspx?ScrMemType=1",
                               "ClientAddress.aspx?ScrMemType=2",
                               "ClientAddress.aspx?ScrMemType=3",
                               "ClientAddress.aspx?ScrMemType=6"})]
	[Column(Storage="Address3", DbType="NVarChar(50)")]
	public string Address3 { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Address].[City] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
     Caption = "City",
	 BuilderType = BuilderType.TextBox,
     ScreenIDs = new string[] {"ClientAddress.aspx?ScrMemType=1",
                               "ClientAddress.aspx?ScrMemType=2",
                               "ClientAddress.aspx?ScrMemType=3",
                               "ClientAddress.aspx?ScrMemType=6"},
     ValidateAsRequiredField = true)]
	[Column(Storage="City", DbType="NVarChar(50)")]
	public string City { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Address].[State] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
     Caption = "State/Province",
	 BuilderType = BuilderType.TextBox,
     ScreenIDs = new string[] {"ClientAddress.aspx?ScrMemType=1",
                               "ClientAddress.aspx?ScrMemType=2",
                               "ClientAddress.aspx?ScrMemType=3",
                               "ClientAddress.aspx?ScrMemType=6"},
     ValidateMaxLength = 2,
     ValidateAsRequiredField = true)]
	[Column(Storage="State", DbType="NVarChar(2)")]
	public string State { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Address].[Zip] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "Zip",
	 BuilderType = BuilderType.TextBox,
     ValidateMaxLength=12,
     ScreenIDs = new string[] {"ClientAddress.aspx?ScrMemType=1",
                               "ClientAddress.aspx?ScrMemType=2",
                               "ClientAddress.aspx?ScrMemType=3",
                               "ClientAddress.aspx?ScrMemType=6"})]
	[Column(Storage="Zip", DbType="NVarChar(12)")]
	public string Zip { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Address].[Country] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
     Caption = "Country",
	 BuilderType = BuilderType.TextBox,
     ScreenIDs = new string[] {"ClientAddress.aspx?ScrMemType=1",
                               "ClientAddress.aspx?ScrMemType=2",
                               "ClientAddress.aspx?ScrMemType=3",
                               "ClientAddress.aspx?ScrMemType=6"},
     ValidateAsRequiredField = true)]
	[Column(Storage="Country", DbType="NVarChar(50)")]
	public string Country { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Address].[Phone] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
     Caption = "Phone",
	 BuilderType = BuilderType.TextBox,
     ScreenIDs = new string[] {"ClientAddress.aspx?ScrMemType=1",
                               "ClientAddress.aspx?ScrMemType=2",
                               "ClientAddress.aspx?ScrMemType=3",
                               "ClientAddress.aspx?ScrMemType=6"},
     ValidateAsRequiredField = true)]
	[Column(Storage="Phone", DbType="NVarChar(50)")]
	public string Phone { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Address].[AltPhone] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "Alt Phone",
	 BuilderType = BuilderType.TextBox,
     ScreenIDs = new string[] {"ClientAddress.aspx?ScrMemType=1",
                               "ClientAddress.aspx?ScrMemType=2",
                               "ClientAddress.aspx?ScrMemType=3",
                               "ClientAddress.aspx?ScrMemType=6"})]
	[Column(Storage="AltPhone", DbType="NVarChar(50)")]
	public string AltPhone { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Address].[email] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "Email",
	 BuilderType = BuilderType.TextBox,
     /*ValidationType=ValidationType.Email,*/
     ScreenIDs = new string[] {"ClientAddress.aspx?ScrMemType=1",
                               "ClientAddress.aspx?ScrMemType=2",
                               "ClientAddress.aspx?ScrMemType=3",
                               "ClientAddress.aspx?ScrMemType=6"})]
	[Column(Storage="email", DbType="NVarChar(50)")]
	public string email { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[Address].[Fax] (nvarchar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextBox
	/// </summary>
	[ScreenBuilder(
	 Caption = "Fax",
	 BuilderType = BuilderType.TextBox,
     ScreenIDs = new string[] {"ClientAddress.aspx?ScrMemType=1",
                               "ClientAddress.aspx?ScrMemType=2",
                               "ClientAddress.aspx?ScrMemType=3",
                               "ClientAddress.aspx?ScrMemType=6"})]
	[Column(Storage="Fax", DbType="NVarChar(50)")]
	public string Fax { get; set; }




	public Address() { }

}
