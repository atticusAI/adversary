
using System;
using System.Data;
using System.Data.Linq;
using System.Configuration;
using System.Linq;
using System.Data.Linq.Mapping;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using HH.Screen;

/// <summary>
/// Summary description for ScreeningMemo
/// This class file was created using the HH DataObjectBuilder
/// </summary>
[Table(Name="RequestParties")]
public class RequestParties
{

	/// <summary>
	/// DB Column: [Adversary].[RequestParties].[PartyID] (Int)
	/// Nullable: False
	/// In Primary Key: True
	/// Is Foreign Key: False
	/// Identity: True
	/// Screen Builder Control Type: Auto
	/// </summary>
    //[ScreenBuilder(
    // Caption = "PartyID",
    // BuilderType = BuilderType.None,
    // EntityControlTargetIDs = new string[] { "RequestParties" })]
	[Column(Storage="PartyID", DbType="Int(4) NOT NULL IDENTITY", IsDbGenerated=true, AutoSync=AutoSync.Always, IsPrimaryKey=true)]
	public int? PartyID { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[RequestParties].[ReqID] (Int)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: True
	/// Identity: False
	/// Screen Builder Control Type: Auto
	/// </summary>
	[Column(Storage="ReqID", DbType="Int(4)")]
	public int? ReqID { get; set; }


	private EntityRef<Requests> _requests = default(EntityRef<Requests>);
	[Association(Name="FK_RequestParties_Requests", Storage="_requests", ThisKey="ReqID", IsForeignKey=true)]
	public Requests Requests
	{
		get
		{
			return this._requests.Entity;
		}
		set
		{
			Requests previousValue = this._requests.Entity;
			if (((previousValue != value)
				|| (this._requests.HasLoadedOrAssignedValue == false)))
			{
				if ((previousValue != null))
				{
					this._requests.Entity = null;
					previousValue.RequestParties.Remove(this);
				}
				this._requests.Entity = value;
				if ((value != null))
				{
					value.RequestParties.Add(this);
					this.ReqID = value.ReqID;
				}
				else
				{
					this.ReqID = default(Nullable<int>);
				}
			}
		}
	}


	/// <summary>
	/// DB Column: [Adversary].[RequestParties].[PartyName] (NVarChar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextArea
	/// </summary>
	[ScreenBuilder(
	 Caption = "Party Name",
	 BuilderType = BuilderType.TextArea,
     EntityControlTargetIDs = new string[] { "RequestParties" },
     SummaryViewFieldOrder=11)]
	[Column(Storage="PartyName", DbType="NVarChar(255)")]
	public string PartyName { get; set; }


	/// <summary>
	/// DB Column: [Adversary].[RequestParties].[PartyRelationship] (NVarChar)
	/// Nullable: True
	/// In Primary Key: False
	/// Is Foreign Key: False
	/// Identity: False
	/// Screen Builder Control Type: TextArea
	/// </summary>
	[ScreenBuilder(
	 Caption = "Party Relationship",
	 BuilderType = BuilderType.DropDownList,
     ListItemText = new string[] { "RELATED to client", 
                                   "ADVERSE to client" },
     InformationToolTip = "Include contacts under related parties, and parties related to adverse parties under adverse parties.",
     ListItemValues = new string[] { "RELATED", "ADVERSE" },
     EntityControlTargetIDs = new string[] { "RequestParties" },
     SummaryViewFieldOrder=11)]
	[Column(Storage="PartyRelationship", DbType="NVarChar(255)")]
	public string PartyRelationship { get; set; }

	public RequestParties() { }

}
