﻿using System;
using System.Collections.Generic;

/// <summary>
/// Custom role provider for this site
/// </summary>
public class AdveraryRoles : System.Web.Security.RoleProvider /*, ICustomRoleProvider */
{
    Dictionary<string, List<string>> _userRoles = new Dictionary<string, List<string>>();
    List<string> _masterRoleList = new List<string>();

    public AdveraryRoles() { }


    public override string[] GetAllRoles()
    {
        return _masterRoleList.ToArray();
    }

    public override void AddUsersToRoles(string[] usernames, string[] roleNames)
    {
        foreach (string user in usernames)
            AddUserToRoles(user, roleNames);
    }

    public void AddUserToRoles(string userName, string[] roleNames)
    {
        if (!_userRoles.ContainsKey(userName))
            _userRoles.Add(userName, new List<string>());

        List<string> roles = _userRoles[userName];
        foreach (string role in roleNames)
        {
            if (!_masterRoleList.Contains(role))
                _masterRoleList.Add(role);
            if (!roles.Contains(role))
                roles.Add(role);
        }

        _userRoles[userName] = roles;
    }

    public override string ApplicationName { get; set; }

    public override void CreateRole(string roleName)
    {
        if (!_masterRoleList.Contains(roleName))
            _masterRoleList.Add(roleName);
    }

    public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
    {
        if (!_masterRoleList.Contains(roleName))
            return false; //throw new Exception("Could not delete role " + roleName + " because it doesn't exist.");

        string[] users = GetUsersInRole(roleName);
        if (throwOnPopulatedRole && users.Length > 0)
            throw new Exception("Could not delete role " + roleName + ", " + users.Length + " users belong to this role.");

        bool removed = false;
        foreach (string user in users)
            removed = removed || _userRoles[user].Remove(roleName);

        return removed && _masterRoleList.Remove(roleName);
    }

    public override string[] FindUsersInRole(string roleName, string usernameToMatch)
    {
        List<string> retList = new List<string>();
        string[] users = GetUsersInRole(roleName);
        foreach (string user in users)
        {
            if (user == usernameToMatch)
                retList.Add(user);
        }
        return retList.ToArray();
    }

    public override string[] GetRolesForUser(string username)
    {
        if (!_userRoles.ContainsKey(username))
            return new string[0]; //throw new ArgumentException("Could not get roles for user name " + username + ". User does not exist in the role dictionary");

        return _userRoles[username].ToArray();
    }

    public override string[] GetUsersInRole(string roleName)
    {
        List<string> users = new List<string>();
        foreach (string user in _userRoles.Keys)
        {
            List<string> roles = _userRoles[user];
            if (roles.Contains(roleName))
                users.Add(user);
        }
        return users.ToArray();
    }

    public override bool IsUserInRole(string username, string roleName)
    {
        if (!_userRoles.ContainsKey(username))
            return false; //throw new ArgumentException("Could not get roles for user name " + username + ". User does not exist in the role dictionary");
        return _userRoles[username].Contains(roleName);
    }

    public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
    {
        foreach (string user in usernames)
        {
            try
            {
                List<string> roles = _userRoles[user];
                foreach (string role in roleNames)
                    roles.Remove(role);
                _userRoles[user] = roles;
            }
            catch{}
        }
    }

    public override bool RoleExists(string roleName)
    {
        return _masterRoleList.Contains(roleName);
    }


    public bool RemoveUser(string userName)
    {
        return _userRoles.Remove(userName);
    }
}