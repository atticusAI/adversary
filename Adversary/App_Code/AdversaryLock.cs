﻿using System;
using System.Collections.Generic;


/// <summary>
/// Adversary requested a method for "locking" screening memos from being edited by multiple people when they were being processed, this class holds the lock info
/// </summary>
public class AdversaryLock
{
    private int? _id;
    public int? ScrMemId
    {
        get
        {
            return _id;
        }
    }

    private string _login;
    public string Login
    {
        get
        {
            return _login;
        }
    }

    public DateTime Expiration { get; set; }

    public bool Acknowledged { get; set; }

    private bool _locked = false;
    public bool Locked 
    {
        get
        {
            return _locked;
        }
    }

    private string _unlockedBy;
    public string UnlockedBy
    {
        get
        {
            return _unlockedBy;
        }
    }

    public bool Expired
    {
        get
        {
            return (Expiration <= DateTime.Now);
        }
    }

    public AdversaryLock(int? scrMemId, string login)
    {
        _id = scrMemId;
        _login = login;
        Acknowledged = false;
        _locked = true;
        RenewExpiration();
    }

    public void Unlock(string login)
    {
        RenewExpiration();

        if (!_locked)
            return;

        _locked = false;
        _unlockedBy = login;
    }

    public void RenewExpiration()
    {
        Expiration = DateTime.Now.AddMinutes(5);
    }
} 

/// <summary>
/// Adversary requested a method for "locking" screening memos from being edited by multiple people when they were being processed, this class handles the locking
/// </summary>
public class AdversaryLocking
{
    private static AdversaryLocking _instance = null;
    private static readonly object _syncRoot = new object();

    private Dictionary<int?, AdversaryLock> _locks = new Dictionary<int?, AdversaryLock>();
    private static DateTime _cacheExpiration = DateTime.Now.AddMinutes(5);
    
    public static AdversaryLocking Instance
    {
        get
        {
            if (_instance == null)
            {
                lock (_syncRoot)
                {
                    if (_instance == null)
                        _instance = new AdversaryLocking();
                }
            }
            else
                CheckCache();

            return _instance;
        }
    }

    private static void CheckCache()
    {
        if (_cacheExpiration <= DateTime.Now)
        {
            lock (_syncRoot)
            {
                if (_cacheExpiration <= DateTime.Now)
                {
                    List<int?> remove = new List<int?>();
                    foreach (AdversaryLock al in _instance._locks.Values)
                    {
                        if (al.Expired)
                            remove.Add(al.ScrMemId);
                    }
                    foreach (int? scrMemId in remove)
                        _instance._locks.Remove(scrMemId);

                    _cacheExpiration = DateTime.Now.AddMinutes(5);
                }
            }
        }
    }

    private AdversaryLocking() { }

    public void AddLock(int? scrMemId, AdversaryLock al)
    {
        lock (_syncRoot)
        {
            if (!HasLock(scrMemId))
                _locks.Add(scrMemId, al);
        }
    }

    public bool HasLock(int? scrMemId)
    {
        lock (_syncRoot)
        {
            if (!_locks.ContainsKey(scrMemId))
                return false;
            if (_locks[scrMemId].Expired)
            {
                _locks.Remove(scrMemId);
                return false;
            }
            return true;
        }
    }

    public bool RemoveLock(int? scrMemId)
    {
        lock (_syncRoot)
        {
            return _locks.Remove(scrMemId);
        }
    }

    public AdversaryLock GetLock(int? scrMemId)
    {
        lock (_syncRoot)
        {
            if (!HasLock(scrMemId))
                return null;
            return _locks[scrMemId];
        }
    }
}
