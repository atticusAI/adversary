﻿using System;
using System.Collections.Generic;
using System.Data;

/// <summary>
/// Handles all calls to the FirmDirectory
/// </summary>
public class FirmDirectory
{
    private static object _lockObject = new object();

    public const string FIRM_DIR_GUID = "PersonGUID";
    public const string FIRM_DIR_EMAIL = "CompanyEmailAddress";
    public const string FIRM_DIR_CITY = "City";

    public FirmDirectory() { }


    public string GetEmployeeGUID(string employeeId)
    {
    //    DataRow dr = GetFirmDirEmployeeData(employeeId);
    //    return dr[FIRM_DIR_GUID].ToString();
        return GetEmployee(employeeId).PersonGUID;
    }

    public string GetEmployeeEmailAddress(string employeeId)
    {
        ///TODO: This ain't a permanent solution!
        if (employeeId == "8139")
            return "sesoares@hollandhart.com";
        if (employeeId == "8167")
            return "rljohnson@hollandhart.com";

        //DataRow dr = GetFirmDirEmployeeData(employeeId);
        //return dr[FIRM_DIR_EMAIL].ToString();
        return GetEmployee(employeeId).CompanyEmailAddress;
    }

    public string GetEmployeeCity(string employeeId)
    {
        //DataRow dr = GetFirmDirEmployeeData(employeeId);
        //return dr[FIRM_DIR_CITY].ToString();
        return GetEmployee(employeeId).City;
    }

    public HRDataService.Employee GetEmployee(string employeeID)
    {
        HRDataService.Employee employee = new HRDataService.Employee();
        HRDataService.PeopleDataContractClient peopleData = new HRDataService.PeopleDataContractClient();
        employee = peopleData.GetEmployeeByPersonId(employeeID);
        return employee;
    }

    //public EmployeeWS.Employee GetEmployee(string employeeId)
    //{
    //    EmployeeWS.EmployeeWS ws = new EmployeeWS.EmployeeWS();
    //    string guid = GetEmployeeGUID(employeeId);
    //    return ws.GetEmployeeByPersonGUID(guid);
    //}

    //private static Dictionary<string, DataRow> _empCache = new Dictionary<string, DataRow>();
    //public DataRow GetFirmDirEmployeeData(string employeeId)
    //{
    //    if (!_empCache.ContainsKey(employeeId))
    //    {
    //        lock (_lockObject)
    //        {
    //            if (!_empCache.ContainsKey(employeeId))
    //            {
    //                EmployeeWS.EmployeeWS ws = new EmployeeWS.EmployeeWS();
    //                DataSet ds = ws.GetEmployees(string.Empty, employeeId.Trim(), string.Empty, string.Empty);
    //                DataTable dt = ds.Tables[0];
    //                if (dt.Rows.Count == 0)
    //                    throw new ArgumentException("Could not find a firm directory match for employee ID " + employeeId);
    //                //if (dt.Rows.Count > 1)
    //                //    throw new ArgumentException("More than one match found in firm directory for employee ID " + employeeId);
    //                _empCache.Add(employeeId, dt.Rows[0]);
    //                return dt.Rows[0];
    //            }
    //        }
    //    }

    //    return _empCache[employeeId];
    //}
}
