﻿
/// <summary>
/// All session data encapsulated here
/// </summary>
public class SessionData
{
    public bool LoggedIn { get; set; }
    public string LoginEmpName { get; set; }
    public string LoginEmpCode { get; set; }
    public string LoginEmpEmail { get; set; }

    //private AdveraryRoles _userRoles;
    //public AdveraryRoles UserRoles
    //{
    //    get
    //    {
    //        if (_userRoles == null)
    //            _userRoles = new AdveraryRoles();
    //        return _userRoles;
    //    }
    //}

    private Requests _requests = null;
    public Requests AdversaryRequest
    {
        get
        {
            if (_requests == null)
                _requests = new Requests();
            return _requests;
        }
        set
        {
            _requests = value;
        }
    }

    private Memo _memo = null;
    public Memo MemoData
    {
        get
        {
            if (_memo == null)
                _memo = new Memo();
            return _memo;
        }
        set
        {
            _memo = value;
        }
    }

    public bool MemoLoaded { get; set; }

    public bool AdversaryRequestLoaded { get; set; }

    private AdminLogins _admin = null;
    public AdminLogins AdminLogin
    {
        get
        {
            if (_admin == null)
                _admin = new AdminLogins();
            return _admin;
        }
        set
        {
            _admin = value;
        }
    }

    public SessionData()
    {
        LoggedIn = false;
        MemoLoaded = false;
        AdversaryRequestLoaded = false;
    }
}


