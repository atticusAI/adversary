﻿using System.Data.SqlClient;
using HH.SQL;
using HH.Utils;
using System.Configuration;

/// <summary>
/// SQL connection class for the adversary DB
/// </summary>
public class AdversaryData
{
    /// <summary>
    /// changed 12/09/2016 for Associated Memo, this change to use the
    /// service is piggybacking.
    /// </summary>
    public static string ConnectionString
    {
        get
        {
            //return Config.GetConnectionString("AdversaryDB");
            return Config.GetConnectionString(ConfigurationManager.AppSettings["AdversaryDBKey"]); 
        }
    }

    private AdversaryData() { }

    
    public static SqlConnection GetConnection()
    {
        return new SqlConnection(AdversaryData.ConnectionString);
    }

}
