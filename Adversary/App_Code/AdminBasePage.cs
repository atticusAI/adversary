﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using HH;
using HH.Branding;
using HH.Screen;
using HH.SQL;

/// <summary>
/// Base functionality for all pages related to the administration section of this site
/// </summary>
public class AdminBasePage : AjaxBasePage
{
    public static readonly string _ADVERSARY_LOCK_KEY = "AdversaryLock";

    protected internal Memo _memo;
    protected internal Tracking _tracking;
    protected internal AdminLogins _login;

    private bool _getARBalance = true;
    private ARBalance _balance = null;
    public ARBalance Balance
    {
        get
        {
            if (_getARBalance)
            {
                if (!string.IsNullOrEmpty(_SessionData.MemoData.ClientNumber))
                    _balance = GetARBalance(_SessionData.MemoData.ClientNumber);
                _getARBalance = false;
            }
            return _balance;
        }
    }

    /// <summary>
    /// Called prior to the .aspx page's Page_Init
    /// </summary>
    /// <param name="e"></param>
    protected override void OnInit(EventArgs e)
    {
        try
        {
            LoadPage = DoBaseInit();
            if (!LoadPage)
                return;

            if (!User.IsInRole("Admin") &&
                !User.IsInRole("AP") &&
                !User.IsInRole("PGM"))
                return;


            base.OnInit(e);
        }
        catch (Exception ex)
        {
            HandleError(ex);
        }
    }

    /// <summary>
    /// Called prior to the aspx page's Page_Load
    /// </summary>
    /// <param name="e"></param>
    protected override void OnLoad(EventArgs e)
    {
        _tracking = _SessionData.MemoData.Tracking;
        _login = _SessionData.AdminLogin;
        _memo = _SessionData.MemoData;

        if (LoadPage)
            base.OnLoad(e);
    }

    /// <summary>
    /// Checks if the memo in session is loaded and valid, displays a message in the master page's feedback secion if invalid
    /// </summary>
    /// <returns></returns>
    public bool ValidateMemo()
    {
        string msg = null;

        if (!_SessionData.MemoLoaded)
            msg = "A memo has not been selected. Please select a memo from the queue or search page to continue.";
        else if (!_SessionData.MemoData.Tracking.TrackingID.HasValue || _SessionData.MemoData.Tracking.TrackingID == 0)
            msg = "The approval screen is not available for this memo, the memo has not been submitted.";

        if (msg != null)
        {
            _MasterPage.ShowPopupDialog(msg, MasterPagePopupDialogMode.Popup);
            _MasterPage.PopupYesCommandArgument = "redirectToQueue";
            return false;
        }

        ValidationException vex = _SessionData.MemoData.Validate();
        if (vex.HasInvalidReasons())
        {
            _MasterPage.FeedbackMessage = vex.GetInvalidMessage();
            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Error;
        }
        return true;
    }

    /// <summary>
    /// True if the memo requires the approver to acknowlege a lack of retainer
    /// </summary>
    /// <returns></returns>
    public bool RequiresRetainerApproval()
    {
        return (_memo.ScrMemType == 1 || _memo.ScrMemType == 2) &&
                (!_memo.Retainer.HasValue || !_memo.Retainer.Value || !string.IsNullOrEmpty(_memo.RetainerDescYes));
    }

    /// <summary>
    /// True if the memo requires the approver to acknowlege the client's A/R balance
    /// </summary>
    /// <returns></returns>
    public bool RequiresARApproval()
    {
        return (this.Balance != null && this.Balance.Has90DayBalance());
    }

    /// <summary>
    /// Commits Tracking data to the DB and session
    /// </summary>
    /// <param name="tracking"></param>
    public void SaveTrackingData(Tracking tracking)
    {
        DoDataSave(tracking);

        _SessionData.MemoData.Tracking = tracking;
        _memo = _SessionData.MemoData;
        _tracking = _memo.Tracking;
    }

    /// <summary>
    /// Commits the CM number to the database and sets other tracking properties
    /// </summary>
    /// <param name="cmNum"></param>
    /// <param name="type"></param>
    public void HandleCMEntitySubmit(ClientMatterNumber cmNum, EntitySubmitType type)
    {
        cmNum.ScrMemID = _memo.ScrMemID;

        if (type == EntitySubmitType.Create && string.IsNullOrEmpty(cmNum.Number))
        {
            _MasterPage.FeedbackMessage = "Client matter number cannot be blank";
            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Error;
            return;
        }

        using (SqlConnection conn = AdversaryData.GetConnection())
        {
            DatabaseWorker worker = new DatabaseWorker(conn);
            switch (type)
            {
                case EntitySubmitType.Create:
                    worker.Commit(cmNum);
                    break;
                case EntitySubmitType.Delete:
                    worker.Delete(cmNum);
                    break;
            }

            //List<ClientMatterNumber> cmNumbers = worker.RetrieveToList<ClientMatterNumber>();
            ClientMatterNumber searchCriteria = new ClientMatterNumber();
            searchCriteria.ScrMemID = _memo.ScrMemID;
            List<ClientMatterNumber> cmNumbers = worker.SearchToList<ClientMatterNumber>(searchCriteria);
            _memo.ClientMatterNumbers.Clear();
            _memo.ClientMatterNumbers.AddRange(cmNumbers);
            worker.Commit(_memo);
            worker.Commit(_tracking);

            _SessionData.MemoData = _memo;
            _SessionData.MemoData.Tracking = _tracking;

        }
    }


    /// <summary>
    /// Emails originator,biller and prac support as necessary. Updates UI status.
    /// </summary>
    /// <param name="memo"></param>
    /// <returns>the addresses the message was sent to</returns>
    protected void HandleStatusNotification(Memo memo)
    {
        try
        {
            MasterPageFeedbackMode feedbackMode = MasterPageFeedbackMode.Success;
            CMSData cms = CMSData.GetInstance();
            FirmDirectory fd = new FirmDirectory();

            string feedback = string.Empty;

            try
            {
                SendStatusEmail(memo);
                feedback += "Email notification sent to originating party.<br/>";
            }
            catch
            {
                feedback += "<span style='color: red;'> * An error occurred while sending the status notification to the originating party, one or more of the intended recipients may not have been notified.</span>";
                feedbackMode = MasterPageFeedbackMode.Error;
            }

            if (memo.Tracking.StatusTypeID == TrackingStatusType.AdversaryGroupProcessing ||
                memo.Tracking.StatusTypeID == TrackingStatusType.Completed)
            {
                MailAddressCollection to = new MailAddressCollection();
                StringBuilder msg = new StringBuilder();
                msg.AppendLine("A new matter screening issue requires your attention:");
                msg.AppendLine();
                msg.AppendLine();

                if (!string.IsNullOrEmpty(_memo.RespAttID))
                {
                    try
                    {
                        string billerEmail = null;
                        DataTable dt = cms.DoBillerLookup(_memo.RespAttID);
                        if (dt.Rows.Count > 0)
                            billerEmail = fd.GetEmployeeEmailAddress(dt.Rows[0][CMSData.EMP_CODE].ToString());
                        else
                        {
                            feedback += "<br/><br/><span style='color: red;'> * A biller was not notified, no biller could be found for billing attorney " + _memo.RespAttID + "</span>";
                            feedbackMode = MasterPageFeedbackMode.Error;
                        }

                        if (!string.IsNullOrEmpty(billerEmail))
                        {
                            to.Add(billerEmail);
                            msg.AppendLine("* Biller Action Required");
                            msg.AppendLine();
                            msg.AppendLine();
                            feedback += "<br/><br/> - Biller " + dt.Rows[0][CMSData.EMP_NAME] + "(" + billerEmail + ") notified.";
                        }
                    }
                    catch
                    {
                        feedback += "<br/><br/><span style='color: red;'> * An error occurred while attempting to lookup biller information, the biller was not notified.</span>";
                        feedbackMode = MasterPageFeedbackMode.Error;
                    }
                }
                else if (_memo.ScrMemType != 6)
                {
                    feedback += "<br/><br/><span style='color: red;'> * An error occurred while attempting to lookup biller information, no billing attorney was specified.</span>";
                    feedbackMode = MasterPageFeedbackMode.Error;
                }

                bool nDrive = memo.NDrive.HasValue && memo.NDrive.Value;
                bool docketing = memo.DocketingInfoRequired();
                if (nDrive || docketing)
                {
                    string pracSupportEmails = ConfigurationManager.AppSettings["EmailPracticeSupport"];
                    string[] list = pracSupportEmails.Split(new char[] { ';' });
                    foreach (string email in list)
                        to.Add(email);

                    if (nDrive)
                    {
                        msg.AppendLine("* Please create an N Drive folder for this matter");
                        msg.AppendLine();

                        try
                        {
                            if (!memo.NDriveType.HasValue || memo.NDriveType == 0)
                                msg.AppendLine("NO N DRIVE TYPE SPECIFIED FOR THIS MATTER, PLEASE CONTACT THE MEMO ORIGINATOR FOR INSTRUCTIONS");
                            else
                            {
                                ScreenBuilder sb = HH.Utils.ReflectionUtils.GetAttribute<ScreenBuilder>(memo, "NDriveType");
                                msg.AppendLine("Folder Type:\t\t\t\t" + sb.ListItemText[memo.NDriveType.Value]);
                                msg.AppendLine();

                                //added by A Reimer 06/28/2013 for N Drive Does Apply change request
                                if(memo.NDriveDoesApply.HasValue && memo.NDriveDoesApply > -1)
                                {
                                    sb = new ScreenBuilder();
                                    sb = HH.Utils.ReflectionUtils.GetAttribute<ScreenBuilder>(memo, "NDriveDoesApply");
                                    msg.AppendLine("The following situation applies to this N Drive: " + sb.ListItemText[memo.NDriveDoesApply.Value]);
                                    msg.AppendLine();
                                }



                                if (memo.NDriveType == 4 || memo.NDriveType == 5) //per PS request on 7/28/2011, added "Patent Record" NDrive option at index 5 and enabled NDrive access list if option is selected 
                                {
                                    if (memo.NDriveAccess.Count == 0)
                                        msg.AppendLine("NO ACCESS LIST SPECIFIED FOR MATTER FOLDER, PLEASE CONTACT THE MEMO ORIGINATOR FOR INSTRUCTIONS");
                                    else
                                    {
                                        msg.AppendLine("Folder access required for:");
                                        msg.AppendLine();
                                        foreach (IStaff s in memo.NDriveAccess)
                                            msg.AppendLine(" - " + GetEmployeeDisplayName(s.PersonnelID));
                                    }
                                }
                            }
                        }
                        catch
                        {
                            msg.AppendLine("AN ERROR OCCURRED WHILE ATTEMPTING TO RETRIEVE N DRIVE INFORMATION, PLEASE CONTACT THE MEMO ORIGINATOR FOR INSTRUCTIONS");
                        }
                        
                        msg.AppendLine();
                    }

                    if (docketing)
                    {
                        if (nDrive)
                            msg.AppendLine();

                        msg.AppendLine("* Docketing information supplied for this matter");
                        msg.AppendLine();

                        if (string.IsNullOrEmpty(memo.DocketingAtty))
                            msg.AppendLine("NO DOCKETING ATTORNEY SPECIFIED FOR THIS MATTER, PLEASE CONTACT THE MEMO ORIGINATOR FOR INSTRUCTIONS");
                        else
                            msg.AppendLine("Docketing Attorney:\t\t\t" + cms.GetUserNameByCode(memo.DocketingAtty));
                        msg.AppendLine();

                        if (string.IsNullOrEmpty(memo.DocketingStaff))
                            msg.AppendLine("NO DOCKETING STAFF SPECIFIED FOR THIS MATTER, PLEASE CONTACT THE MEMO ORIGINATOR FOR INSTRUCTIONS");
                        else
                            msg.AppendLine("Docketing Staff Member:\t\t\t" + cms.GetUserNameByCode(memo.DocketingStaff));
                        msg.AppendLine();

                        if (memo.DocketingTeam.Count == 0)
                        {
                            msg.AppendLine("No docketing team was specified");
                            msg.AppendLine();
                        }
                        else
                        {
                            msg.AppendLine("Docketing Team:");
                            msg.AppendLine();
                            foreach (IStaff s in memo.DocketingTeam)
                                msg.AppendLine(" - " + GetEmployeeDisplayName(s.PersonnelID));
                            msg.AppendLine();
                        }

                        msg.AppendLine();
                    }

                    //msg.AppendLine("Notifying:");
                    //foreach (MailAddress email in to)
                    //    msg.AppendLine(email.Address);
                    //msg.AppendLine();
                    feedback += "<br/><br/> - Practice Support Notified";
                }

                if (to.Count > 0)
                {
                    msg.AppendLine();
                    msg.AppendLine(BuildMemoStatusEmail(memo));
                    msg.AppendLine();
                    msg.AppendLine(BuildMemoApproverEmail(memo));
                    msg.AppendLine();
                    msg.AppendLine("Click the link below to view the print version of this memo:");
                    msg.AppendLine();
                    msg.AppendLine(GetBaseUrl() + "Screening/PrintView.aspx?ScrMemID=" + memo.ScrMemID);
                    msg.AppendLine();
                    msg.AppendLine();
                    msg.AppendLine("Click the link below to view the summary of this memo:");
                    msg.AppendLine();
                    msg.AppendLine(GetBaseUrl() + "Screening/Summary.aspx?ScrMemID=" + memo.ScrMemID);

                    string subject = "Screening Memo #" + memo.ScrMemID + " - ACTION REQUIRED";
                    SmtpClient client = new SmtpClient(ConfigurationManager.AppSettings["EmailHost"]);
                    
                    MailAddress from = new MailAddress(ConfigurationManager.AppSettings["EmailSendAddress"]);
                    Email.SendEmail(client, to, from, subject, msg.ToString(), false);
                }

                if (memo.ClientMatterNumbers.Count > 0)
                {
                    memo.Tracking.Locked = true;
                    memo.Tracking.CMNumbersSentDate = DateTime.Now;
                    DoDataSave(memo.Tracking);
                    feedback += "<br/><br/> - Client Matter Numbers Sent";
                }
            }

            _MasterPage.FeedbackMode = feedbackMode;
            _MasterPage.FeedbackMessage = feedback;
            _MasterPage.ShowPopupDialog(feedback, MasterPagePopupDialogMode.ModalOK, Unit.Percentage(40), null);
        }
        catch
        {
            _MasterPage.FeedbackMessage = "An error occurred while attempting to send the notification, one or more of the intended recipients may not have been notified.";
            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Error;
        }
    }

    /// <summary>
    /// Approver combo boxes come in three flavors
    /// </summary>
    protected internal enum ApproverComboBoxType
    {
        AP,
        PGL,
        Adv
    }
    
    /// <summary>
    /// Binds the combobox to a list of approvers
    /// </summary>
    /// <param name="combo"></param>
    /// <param name="dataSource"></param>
    /// <param name="selectedApprover"></param>
    /// <param name="cbType"></param>
    /// <param name="memo"></param>
    protected internal void BindApproverComboBox(ComboBox combo, AdminLogins[] dataSource, int? selectedApprover, ApproverComboBoxType cbType, Memo memo)
    {
        foreach (AdminLogins login in dataSource)
        {
            string name = login.Name;
            switch (cbType)
            {
                case ApproverComboBoxType.AP:
                    name += (login.IsPrimaryAP.HasValue && login.IsPrimaryAP.Value) ? " (P)" : " (S)";
                    break;
                case ApproverComboBoxType.PGL:
                    var query = from c in login.AdminPracCodes
                                where (c.PracCode == memo.PracCode)
                                select c;
                    AdminPracCodes code = query.FirstOrDefault();
                    name += (code.IsPrimary.HasValue && code.IsPrimary.Value) ? " (P)" : " (S)";
                    break;
                case ApproverComboBoxType.Adv:
                    name += (login.IsAdversaryAdmin.HasValue && login.IsAdversaryAdmin.Value) ? " (P)" : " (S)";
                    break;
            }
            combo.Items.Add(new ListItem(name, login.UserID.ToString()));
        }

        combo.Items.Insert(0, new ListItem("(Unassigned)", "unassigned"));

        try
        {
            if (selectedApprover.HasValue)
                combo.SelectedValue = selectedApprover.Value.ToString();
            else
                combo.SelectedIndex = 0;
        }
        catch
        {
            combo.SelectedIndex = 0;
        }
    }


   

}
