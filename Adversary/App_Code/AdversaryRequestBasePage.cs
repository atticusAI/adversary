﻿using System;
using System.Web;
using HH;
using HH.Branding;
using HH.SQL;
using System.Data.SqlClient;

/// <summary>
/// Class inherited by all Adversary Request .aspx pages
/// </summary>
public class AdversaryRequestBasePage : AjaxBasePage
{
    public AdversaryRequestBasePage() { }

    /// <summary>
    /// Called before the page's Page_Init() method
    /// </summary>
    /// <param name="e"></param>
    protected override void OnInit(EventArgs e)
    {
        
        try
        {
            LoadPage = DoBaseInit();
            if (!LoadPage)
                return;

            //_MasterPage.CustomRoleProvider = _SessionData.UserRoles;
            //_MasterPage.CustomRoleProvider = AdveraryRoles.Instance;
            _MasterPage.SiteMapProvider = SiteMap.Providers["Main"];
            _MasterPage.SiteMapProvider.SiteMapResolve += new SiteMapResolveEventHandler(SiteMapProvider_SiteMapResolve);
            _MasterPage.DataBind();

            LoadPage = HandleAdminRequestPageInit();
            if (LoadPage)
                base.OnInit(e);
        }
        catch (Exception ex)
        {
            HandleError(ex);
        }
    }

    /// <summary>
    /// Called before the page's Page_Load() method
    /// </summary>
    /// <param name="e"></param>
    protected override void OnLoad(EventArgs e)
    {
        if (LoadPage)
            base.OnLoad(e);
    }

    /// <summary>
    /// Handles some master page config, session var setting and event wirte up
    /// </summary>
    /// <returns>false if an adv request is not loaded</returns>
    private bool HandleAdminRequestPageInit()
    {
        string screenId = ParseURLForScreenID(Request.Path);


        if (_MasterPage.SelectedTab == null) return false;



        //If on the home tab, we're done
        if (_MasterPage.SelectedTab.Title == "Home")
        {
            _SessionData.AdversaryRequest = null;
            _SessionData.AdversaryRequestLoaded = false;
            return true;
        }

        if (Request.FilePath.ToLower().Contains("default.aspx") ||
            Request.FilePath.ToLower().Contains("select.aspx") ||
            Request.FilePath.ToLower().Contains("viewrequests.aspx"))
        {
            _SessionData.AdversaryRequest = null;
            _SessionData.AdversaryRequestLoaded = false;
            _MasterPage.FooterNavVisible = false;

            try
            {
                _MasterPage.Tabs[2].Items[2].Enabled = false;
                _MasterPage.Tabs[2].Items[3].Enabled = false;
                _MasterPage.Tabs[2].Items[3].Enabled = false;
                _MasterPage.Tabs[2].Items[4].Enabled = false;
                _MasterPage.Tabs[2].Items[5].Enabled = false;
                _MasterPage.Tabs[2].Items[6].Enabled = false;

                _MasterPage.Tabs[4].Items[2].Items[2].Enabled = false;
                _MasterPage.Tabs[4].Items[2].Items[3].Enabled = false;
            }
            catch { }

            return true;
        }
        else if (!_SessionData.AdversaryRequestLoaded)
        {
            string type = (_SessionData.AdminLogin.IsAdversary.HasValue &&
                           _SessionData.AdminLogin.IsAdversary.Value) ? "all" : "submitted";

            JSAlertAndRedirect("This page is unviewable because you have not selected an adversary request", "/Request/ViewRequests.aspx?type=" + type);
            return false;
        }

        CheckStatus();

        _MasterPage.FooterNavVisible = true;
        _MasterPage.PostBackEvent += new MasterPagePostBackEventHandler(Request_PostBackEvent);
        return true;
    }

    /// <summary>
    /// Handles MasterPage post back events, under most conditions, commits the Adversary Request data to the DB
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// <returns></returns>
    private bool Request_PostBackEvent(object sender, MasterPagePostBackEventArgs e)
    {
        try
        {
            if (_MasterPage.Screen.DataSource == null)
                return true;


            if (!_SessionData.AdversaryRequestLoaded)
            {
                JSAlertAndRedirect("An unexpected error occurred", "/Request/Default.aspx");
                return false;
            }

            _SessionData.AdversaryRequest = (Requests)_MasterPage.Screen.DataSource;
            if (_SessionData.AdversaryRequest.ReqID != null &&
                !string.IsNullOrEmpty(_SessionData.AdversaryRequest.AttyLogin) &&
                !string.IsNullOrEmpty(_SessionData.AdversaryRequest.TypistLogin) &&
                !string.IsNullOrEmpty(_SessionData.AdversaryRequest.ClientName))
            {
                RequestParties[] parties = new RequestParties[_SessionData.AdversaryRequest.RequestParties.Count];
                _SessionData.AdversaryRequest.RequestParties.CopyTo(parties, 0);
                using (SqlConnection conn = AdversaryData.GetConnection())
                {
                    DatabaseWorker worker = new DatabaseWorker(conn);
                    DoDataSave(_SessionData.AdversaryRequest, worker);
                    foreach (RequestParties party in parties)
                    {
                        party.ReqID = _SessionData.AdversaryRequest.ReqID;
                        DoDataSave(party, worker);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            if (_DebugMode)
            {
                _MasterPage.ShowError(ex);
            }
            else
            {
                _MasterPage.FeedbackMessage = "Sorry, an error occurred while attempting to save your form data to the database.<br/>" +
                    "Information for technical support: " + ex.Message;
                _MasterPage.FeedbackMode = MasterPageFeedbackMode.Error;
            }
            return false;
        }

        return true;
    }

    /// <summary>
    /// Checks and displays the submission date and validation info in the master page feedback section
    /// </summary>
    public void CheckStatus()
    {
        //if (_SessionData.AdversaryRequest.ReqID > 0)
        if (_SessionData.AdversaryRequest.SubmittalDate != null)
        {
            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Confirm;
            _MasterPage.FeedbackMessage = "This adversary request has been submitted";
            if (!string.IsNullOrEmpty(_SessionData.AdversaryRequest.WorkCompletedBy))
                _MasterPage.FeedbackMessage += " and was completed by " + _SessionData.AdversaryRequest.WorkCompletedBy + " on " + _SessionData.AdversaryRequest.CompletionDate;
            else if (!string.IsNullOrEmpty(_SessionData.AdversaryRequest.WorkBegunBy))
                _MasterPage.FeedbackMessage += " and was acknowledged by " + _SessionData.AdversaryRequest.WorkBegunBy + " on " + _SessionData.AdversaryRequest.WorkBegunDate;

            _MasterPage.FeedbackMessage += "<br/><br/>";
            return;
        }

        if (Request.FilePath.Contains("Submit.aspx"))
        {
            ValidationException vex = _SessionData.AdversaryRequest.Validate();
            if (vex.HasInvalidReasons())
            {
                _MasterPage.FeedbackMessage = "<img src=\"..\\_css\\red_x.gif\" alt=\"failed\" />&nbsp;&nbsp;&nbsp;" + vex.GetInvalidMessage();
                _MasterPage.FeedbackMode = MasterPageFeedbackMode.Warning;
            }
            else
            {
                _MasterPage.FeedbackMessage = "<img src=\"..\\_css\\green_check.jpg\" alt=\"passed\" /> This adversary request passes validation and can be submitted";
                _MasterPage.FeedbackMode = MasterPageFeedbackMode.Confirm;
            }
        }
    }
}
