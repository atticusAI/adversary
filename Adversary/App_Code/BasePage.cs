﻿#define DEBUG

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using HH;
using HH.Authentication;
using HH.Branding;
using HH.SQL;
using HH.UI;
using HH.UI.FileList;
using HH.Utils;



/// <summary>
/// Summary description for BasePage
/// </summary>
public class BasePage : System.Web.UI.Page
{
    private static Dictionary<string, string> _empList = new Dictionary<string, string>();
    private static Dictionary<string, string> _clientList = new Dictionary<string, string>();


    #region PROPERTIES
    public IMaster _MasterPage
    {
        get
        {
            return (IMaster)Master;
        }
    }

    public SessionData _SessionData
    {
        get
        {
            if (Session["adversarySession"] == null)
                Session["adversarySession"] = new SessionData();

            return (SessionData)Session["adversarySession"];
        }
    }
    /// <summary>
    /// Removes the screening memo from the session
    /// </summary>
    public void ClearMemo()
    {
        //Session["adversarySession"] = null;
        _SessionData.MemoData = new Memo();
        _SessionData.MemoLoaded = false;
    }

    /// <summary>
    /// Clears the session from memory
    /// </summary>
    public void ClearSession()
    {
        try
        {
            string userName = _SessionData.AdminLogin.Login;
            Session["adversarySession"] = null;
            if (!SecurityUtils.IsAuthenticated(Request))
                return;

            FormsAuthentication.SignOut();
            SecurityUtils.RemoveUserRoles(userName);
        }
        catch { }
    }

    /// <summary>
    /// True if Web.config debug flag is true
    /// </summary>
    public static bool _DebugMode
    {
        get
        {
            return Config.GetDebugStatus();
        }
    }

    /// <summary>
    /// False will not call the inherting page's Page_Load method
    /// </summary>
    public bool LoadPage { get; set; }
    #endregion

    public BasePage()
    {
        LoadPage = true;
    }

    #region BASE OnInit HANDLER
    /// <summary>
    /// Handles masterpage and sitemap init, as well as login form presentation and session loading if necessary
    /// </summary>
    /// <returns>False if the user is not logged in</returns>
    protected internal bool DoBaseInit()
    {
        _MasterPage.MenuMode = MasterPageMenuMode.Tabs;
        _MasterPage.RoleSecurityEnabled = true;
        _MasterPage.Screen.CssClass = "screenTable";
        _MasterPage.Screen.CaptionCssClass = "formBuilderCapionText";
        _MasterPage.FooterNavVisible = false;
        

        string url = Request.Url.ToString().ToLower();
        if ((Page is ScreeningBasePage) && !url.Contains("type=approve"))
        {
            _MasterPage.SiteMapProvider = SiteMap.Providers["Screening"];
            _MasterPage.SiteMapProvider.SiteMapResolve += new SiteMapResolveEventHandler(SiteMapProvider_SiteMapResolve);
            if (_SessionData.MemoLoaded && _SessionData.MemoData.ScrMemID.HasValue && _SessionData.MemoData.ScrMemID > 0)
                _MasterPage.UrlParams.Add("ScrMemID", _SessionData.MemoData.ScrMemID.ToString());
            else if (!string.IsNullOrEmpty(Request.Params["ScrMemID"]))
                _MasterPage.UrlParams.Add("ScrMemID", Request.Params["ScrMemID"].ToString());
            _MasterPage.DataBind(); 
        }
        else
        {
            _MasterPage.SiteMapProvider = SiteMap.Providers["Main"];
            _MasterPage.SiteMapProvider.SiteMapResolve += new SiteMapResolveEventHandler(SiteMapProvider_SiteMapResolve);
            _MasterPage.DataBind();

            if (!_SessionData.MemoLoaded)
                ToggleMenuEnabled(false);
        }

        if (!_SessionData.LoggedIn)
        {
            if (SecurityUtils.IsAuthenticated(Request))
            {
                try
                {
                    FormsAuthenticationTicket ticket = SecurityUtils.GetTicketFromCookie(Request);
                    if (!ticket.IsPersistent)
                    {
                        ShowLoginForm();
                        return false;
                    }

                    HandleLogin(true, ticket.Name);
                }
                catch
                {
                    ShowLoginForm();
                    return false;
                }
            }
            else
            {
                ShowLoginForm();
                return false;
            }
        }

        CheckDataLoad();
        return true;
    }



    //hooking into the SiteMapResolve event is necessary to clean the url of any extra url params
    protected internal SiteMapNode SiteMapProvider_SiteMapResolve(object sender, SiteMapResolveEventArgs e)
    {
        string url = e.Context.Request.AppRelativeCurrentExecutionFilePath;
        SiteMapNode resolvedNode = e.Provider.FindSiteMapNode(e.Context.Request.RawUrl);
        if (resolvedNode == null)
            resolvedNode = e.Provider.FindSiteMapNode(url);
        if (resolvedNode == null && e.Context.Request.QueryString["ScrMemType"] != null)
            //url += "?ScrMemType=" + e.Context.Request.QueryString["ScrMemType"];
            resolvedNode = e.Provider.FindSiteMapNode(url + "?ScrMemType=" + e.Context.Request.QueryString["ScrMemType"]);
        if (resolvedNode == null && e.Context.Request.QueryString["type"] != null)
            //url += "?type=" + e.Context.Request.QueryString["type"];
            resolvedNode = e.Provider.FindSiteMapNode(url + "?type=" + e.Context.Request.QueryString["type"]);

        return resolvedNode;
    }

    /// <summary>
    /// Sets Approval and Admin tab links
    /// </summary>
    /// <param name="enabled"></param>
    public void ToggleMenuEnabled(bool enabled)
    {
        try
        {
            MasterPageTab approval = _MasterPage.Tabs["Approval"];
            approval.Items["Approve Selected Memo"].Enabled = enabled;
            approval.Items["Summary View"].Enabled = enabled;
            approval.Items["Print View"].Enabled = enabled;
            approval.Items["Attachment View"].Enabled = enabled;

            MasterPageTab admin = _MasterPage.Tabs["Admin"];
            MasterPageTabItem menu = admin.Items["Screening Memo Approval"];
            menu.Items["Process Selected Memo"].Enabled = enabled;
            menu.Items["Print View"].Enabled = enabled;
            menu.Items["Attachment View"].Enabled = enabled;
            menu.Items["Summary View"].Enabled = enabled;
        }
        catch { }
    }

    /// <summary>
    /// Displays/configures the modal login form
    /// </summary>
    private void ShowLoginForm()
    {
        ILoginPopup login = (ILoginPopup)Page.LoadControl("~/_usercontrols/LoginPopup.ascx");
        login.LoggedIn += new LoggedInEventHandler(Login_LoggedIn);
        login.ActiveDirectoryServer = ConfigurationManager.AppSettings[(_DebugMode) ? "ADServerDebug" : "ADServer"];
        login.Domain = ConfigurationManager.AppSettings[(_DebugMode) ? "ADDomainDebug" : "ADDomain"];
        login.AllowDomainSelect = false;
        login.AllowLoginCancel = false;
        login.AuthenticationMode = LoginAuthenticationMode.ActiveDirectory;
        login.AllowSaveLogin = true;
        login.Width = Unit.Pixel(500);

        _MasterPage.ScriptManager.Controls.Add((UserControl)login);
        _MasterPage.TabMenuEnabled = false;
        _MasterPage.FooterNavVisible = false;
        _MasterPage.UpperContent.Visible = false;
        _MasterPage.LowerContent.Visible = false;

        login.Show();
    }

    /// <summary>
    /// Global error handling method, displays error message and emails exception if Debug flag is false
    /// </summary>
    /// <param name="ex"></param>
    public void HandleError(Exception ex)
    {
        string error = UIUtils.BuildErrorMessage(ex);

        if (_DebugMode)
        {
            _MasterPage.ShowError("Sorry, an error occurred.<br/><br/>Error Details<br/><br/>" + error);
            return;
        }

        try
        {
            if (Page != null)
                error += "<br/><br/>File Path: " + Page.Request.CurrentExecutionFilePath + "<br/><br/>";
            try
            {
                if (!_SessionData.MemoLoaded)
                    error += "<br/><br/>No Memo Loaded";
                else
                {
                    try
                    {
                        error += "<br/><br/>" + BuildMemoStatusEmail(_SessionData.MemoData);
                    }
                    catch (Exception emailEx)
                    {
                        error += "<br/><br/>An error occurred while attempting to build the memo status portion of this error report:<br/><br/>" + UIUtils.BuildErrorMessage(emailEx);
                        error += "<br/><br/>Screening Memo #:" + _SessionData.MemoData.ScrMemID + "<br/>";
                    }
                }   
            }
            catch { }

            SendErrorEmail(error.Replace("<br/>", "\r\n"));
        }
        catch { }
    }

    //TODO: Need some better error handling methods
    public void SendErrorEmail(Exception ex) { SendErrorEmail(UIUtils.BuildErrorMessage(ex).Replace("<br/>", "\r\n")); }
    public void SendErrorEmail(string errorMsg)
    {
        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["EmailErrorAddress"]))
        {
            try
            {
                MailAddressCollection to = new MailAddressCollection();
                string[] recipients = ConfigurationManager.AppSettings["EmailErrorAddress"].Trim().Split(new Char[] { ';' }, StringSplitOptions.None);
                foreach (string recipient in recipients)
                    if (recipient != null && recipient.Trim() != string.Empty)
                        to.Add(new MailAddress(recipient.Trim()));
                MailAddress from = new MailAddress(ConfigurationManager.AppSettings["EmailSendAddress"]);
                SendEmail(to, from, "Adversary Site Error", errorMsg, false);
            }
            catch { }
        }
    }

    #endregion

    #region PAGE POSTBACK AND DB CALLS

    #region LOGIN HANDLERS
    /// <summary>
    /// Handles login events
    /// </summary>
    /// <param name="sender">The login control sending the event</param>
    /// <param name="userName">User name entered in the form</param>
    /// <param name="password">Password entered in the form</param>
    private void Login_LoggedIn(ILoginPopup sender, string userName, string password)
    {
        sender.SetStatusMessage("Login successful!", System.Drawing.Color.Blue);
        HandleLogin(sender.SaveLoginChecked, userName);
    }

    /// <summary>
    /// If a login event occurrs, this method would be the one that handles it. Sets some user specific session vars and the user's roles
    /// </summary>
    /// <param name="cacheLogin">Caches the login in a cookie so the user is not prompted next time they open the site in a browser</param>
    /// <param name="userName">The user's login</param>
    private void HandleLogin(bool cacheLogin, string userName)
    {
        Roles.DeleteCookie();
        Response.Cookies.Remove(FormsAuthentication.FormsCookieName);
        ((AdveraryRoles)Roles.Provider).RemoveUser(userName);

        CMSData cms = CMSData.GetInstance();
        DataTable dt = cms.GetUserDataByLogin(userName);
        if (dt.Rows.Count == 1)
        {
            _SessionData.LoginEmpCode = dt.Rows[0][CMSData.EMP_CODE].ToString();
            _SessionData.LoginEmpName = userName;//dt.Rows[0][CMSData.EMP_NAME].ToString();
            try
            {
                //if the WS call fails, create email address from userName and hope for the best
                FirmDirectory fd = new FirmDirectory();
                _SessionData.LoginEmpEmail = fd.GetEmployeeEmailAddress(_SessionData.LoginEmpCode);
            }
            catch (Exception ex)
            {
                _SessionData.LoginEmpEmail = userName + "@hollandhart.com";
                HandleError(ex);
            }
        }
        else
        {
            _MasterPage.ShowPopupDialog("A login error occurred that may prevent you from having full access to this site", MasterPagePopupDialogMode.Popup);
        }

        try
        {
            AdminLogins.RefreshAdminList();
        }
        catch { }

        List<string> roles = new List<string>();
        var query = from a in AdminLogins.AllAdmins //admins
                    where !string.IsNullOrEmpty(a.Login) && a.Login.ToUpper() == userName.ToUpper()
                    select a;
        if (query.Count() == 1)
        {
            AdminLogins login = query.FirstOrDefault();
            if (login.IsAP.HasValue && login.IsAP.Value)
                roles.Add("AP");
            if (login.IsPGM.HasValue && login.IsPGM.Value)
                roles.Add("PGM");
            if (login.IsAdversary.HasValue && login.IsAdversary.Value)
                roles.Add("Admin");

            _SessionData.AdminLogin = query.FirstOrDefault();
            //_SessionData.UserRoles.AddRoles(roles);
            ((AdveraryRoles)Roles.Provider).AddUserToRoles(userName, roles.ToArray());
        }

        HttpCookie cookie;
        if (cacheLogin)
        {
            cookie = SecurityUtils.CreateFormsAutheticationCookie(userName, Roles.GetRolesForUser(userName), true, DateTime.Now.AddDays(30), true);
            cookie.Expires = DateTime.Now.AddDays(30);
        }
        else
            cookie = SecurityUtils.CreateFormsAutheticationCookie(userName, Roles.GetRolesForUser(userName), true);

        Response.Cookies.Add(cookie);
        _SessionData.LoggedIn = true;
        _MasterPage.TabMenuEnabled = false;
        _MasterPage.FooterNavVisible = false;
        _MasterPage.UpperContent.Visible = true;
        _MasterPage.LowerContent.Visible = true;
        _MasterPage.DataBind();
        Response.Redirect(Request.RawUrl, false);
    }
    #endregion

    /// <summary>
    /// Checks Screening Memo and adversary session vars, if either do not exist or do not match the URL's parameter then this method will load the memo/adversary request into memory
    /// </summary>
    public void CheckDataLoad()
    {
        //load screening memo if necessary
        string scrMemID = Request.QueryString["ScrMemID"];
        if (!string.IsNullOrEmpty(scrMemID) &&
            !IsPostBack)
        {
            int id = Convert.ToInt32(scrMemID.Trim());
            if (_SessionData.MemoData.ScrMemID != id)
            {
                Memo memo = new Memo();
                memo.ScrMemID = id;
                DoDataLoad(memo); //AdversaryData.GetInstance());
                _SessionData.MemoData = memo;
                _SessionData.MemoLoaded = true;
            }
            else
            {
                _SessionData.MemoLoaded = true;
            }

            if (_MasterPage.SiteMapProvider.Name == "Main")
                ToggleMenuEnabled(true);
        }
        if (!_SessionData.MemoLoaded && !string.IsNullOrEmpty(Request.Params["ScrMemID"]))
        {
            try
            {
                Memo memo = new Memo();
                memo.ScrMemID = Int32.Parse(Request.Params["ScrMemID"]);
                DoDataLoad(memo);
                _SessionData.MemoLoaded = true;

                if (_MasterPage.SiteMapProvider.Name == "Main")
                    ToggleMenuEnabled(true);

                _MasterPage.ShowPopupDialog("Your session expired, and was renewed. Please retry the last action", MasterPagePopupDialogMode.Popup);
            }
            catch { }
        }

        string reqId = Request.QueryString["ReqID"];
        if (!string.IsNullOrEmpty(reqId) &&
            !IsPostBack)
        {
            int id = Convert.ToInt32(reqId.Trim());
            if (_SessionData.AdversaryRequest.ReqID != id)
            {
                Requests request = new Requests();
                request.ReqID = id;
                DoDataLoad(request); //AdversaryData.GetInstance());
                _SessionData.AdversaryRequest = request;
                _SessionData.AdversaryRequestLoaded = true;
            }
            else
            {
                _SessionData.AdversaryRequestLoaded = true;
            }
        }
    }

    /// <summary>
    /// Calls the database worker and loads data into the specified DAL object
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="obj"></param>
    public void DoDataLoad<T>(T obj)
    {
        using (SqlConnection conn = AdversaryData.GetConnection())
            DoDataLoad(obj, new DatabaseWorker(conn));
    }
    /// <summary>
    /// Calls the database worker and loads data into the specified DAL object, use this for multiple DB calls
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="obj"></param>
    /// <param name="worker"></param>
    public void DoDataLoad<T>(T obj, DatabaseWorker worker)
    {
        //AdversaryData.Get(obj);
        worker.RetrieveSingle(obj);

        if (obj is Memo)
        {
            _SessionData.MemoData = obj as Memo;
            _SessionData.MemoLoaded = true;
            if (_MasterPage.SiteMapProvider.Name == "Main")
                ToggleMenuEnabled(true);
        }
    }

    /// <summary>
    /// Saves the DAL object to the adversary database
    /// </summary>
    /// <param name="obj"></param>
    public void DoDataSave(object obj)
    {
        using (SqlConnection conn = AdversaryData.GetConnection())
            DoDataSave(obj, new DatabaseWorker(conn));
    }
    /// <summary>
    /// Saves the DAL obj to the adversary DB, use this method for multiple DB transactions
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="worker"></param>
    public void DoDataSave(object obj, DatabaseWorker worker)
    {
        //AdversaryData.Set(obj);
        worker.Commit(obj);

        if (obj is Memo)
        {
            _SessionData.MemoData = (Memo)obj;
            _SessionData.MemoLoaded = true;

            if (_MasterPage.SiteMapProvider.Name == "Main")
                ToggleMenuEnabled(true);
        }
    }

    #endregion

    #region MISC

    #region STRING FORMATTING
    /// <summary>
    /// Returns "Client Name (Client Number)" string
    /// </summary>
    /// <param name="memo"></param>
    /// <returns></returns>
    public static string GetClientDisplayName(Memo memo)
    {

        try
        {
            string name = string.Empty;
            if (memo.ScrMemType == 2 ||
                ((memo.ScrMemType == 3 || memo.ScrMemType == 6) && !string.IsNullOrEmpty(memo.ClientNumber)))
                name = string.IsNullOrEmpty(memo.ClientNumber) ? string.Empty : memo.ClientName + " (" + memo.ClientNumber + ")";
            else
            {
                if (memo.Company.HasValue && memo.Company.Value)
                    name = memo.NewClient.CName;
                else
                    name = memo.NewClient.FName + " " + memo.NewClient.MName + " " + memo.NewClient.LName;
            }

            name = name.Trim();
            name = name.Replace("  ", " ");
            return (string.IsNullOrEmpty(name)) ? "<em>unavailable</em>" : name;
        }
        catch
        {
            return "<span style='color: red;'>Client Name Not Found</span>";
        }
    }
    /// <summary>
    /// Returns "Client Name (Client Number)" string
    /// </summary>
    /// <param name="clientNum"></param>
    /// <returns></returns>
    public static string GetClientDisplayName(object clientNum)
    {
        if (clientNum == null || clientNum == DBNull.Value)
            return string.Empty;

        CMSData cms = CMSData.GetInstance();
        string cn = clientNum.ToString().Trim();
        if (!_clientList.ContainsKey(cn))
            _clientList.Add(cn, cms.GetClientName(cn));
        return _clientList[cn] + " (" + cn + ")";
    }

    /// <summary>
    /// Returns "Last, First (Emp Number)" string
    /// </summary>
    /// <param name="employeeNumber"></param>
    /// <returns></returns>
    public static string GetEmployeeDisplayName(object employeeNumber)
    {
        if (employeeNumber == null || employeeNumber == DBNull.Value)
            return string.Empty;

        CMSData cms = CMSData.GetInstance();
        string empNumber = employeeNumber.ToString();
        if (!_empList.ContainsKey(empNumber))
            _empList.Add(empNumber, cms.GetUserNameByCode(empNumber));
        return _empList[empNumber] + " (" + empNumber + ")";
    }
    
    #endregion

    /// <summary>
    /// Inspects URL parameters for the screenId to pass to the Screen control (screenId is typically PageName.aspx?ScreMemType=# for screening memo pages, PageName.aspx for all others
    /// </summary>
    /// <param name="url"></param>
    /// <returns></returns>
    public string ParseURLForScreenID(string url)
    {
        string screenId = url.Substring(url.LastIndexOf('/'));
        screenId = screenId.Replace("/", "");
        screenId = screenId.Substring(0, screenId.IndexOf(".aspx") + 5);
        //screenId = screenId.Replace(".aspx", "");
        if (!string.IsNullOrEmpty(Request.QueryString["ScrMemType"]))
            screenId += "?ScrMemType=" + Request.QueryString["ScrMemType"];
        return screenId;
    }


    /// <summary>
    /// JavaScript popup that the user will be redirected, also handles JS redirection
    /// </summary>
    /// <param name="popupMsg">The message to display in the popup</param>
    /// <param name="url">The url to redirect to after popup is closed</param>
    public void JSAlertAndRedirect(string popupMsg, string url)
    {
        Page.ClientScript.RegisterClientScriptBlock(
                this.GetType(),
                "redirWarning",
                "alert('" + popupMsg + ". You are being redirected.');" +
                "window.location.href = '" + Request.ApplicationPath + url + "';",
                true);
        _MasterPage.TabMenuVisible = false;
        _MasterPage.FooterNavVisible = false;
    }

    /// <summary>
    /// Opens a Print View page in a new window
    /// </summary>
    public void OpenPrintView()
    {
        if (!_SessionData.MemoLoaded)
        {
            _MasterPage.FeedbackMessage = "Could not show memo in print view. No memo has been loaded";
            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Error;
            return;
        }

        Page.ClientScript.RegisterClientScriptBlock(
                this.GetType(),
                "newPrintWindow",
                "openExternalWindow('" + Request.ApplicationPath + "/Screening/PrintView.aspx?ScrMemType=" + _SessionData.MemoData.ScrMemType + "&ScrMemID=" + _SessionData.MemoData.ScrMemType + "');",
                true);
    }


    /// <summary>
    /// Strips the current URL down to request scheme, port (if not 80) and application path
    /// </summary>
    /// <returns></returns>
    public string GetBaseUrl()
    {
        StringBuilder url = new StringBuilder();
        url.Append(Request.Url.Scheme);
        url.Append("://");
        url.Append(Request.Url.Host);
        if (Request.Url.Port != 80)
        {
            url.Append(":");
            url.Append(Request.Url.Port);
        }
        url.Append(Request.ApplicationPath);
        url.Append("/");
        return url.ToString();
    }

    #region A/R Balance
    /// <summary>
    /// Calls CMS for A/R balance
    /// </summary>
    /// <param name="clientNumber"></param>
    /// <returns></returns>
    public ARBalance GetARBalance(string clientNumber)
    {
        CMSData cms = CMSData.GetInstance();
        return cms.GetClientBill(clientNumber);
    }
    /// <summary>
    /// Builds a panel that contains a HTML table of A/R data
    /// </summary>
    /// <param name="clientNumber"></param>
    /// <returns></returns>
    public Panel BuildARTable(string clientNumber) { return BuildARTable(GetARBalance(clientNumber)); }
    /// <summary>
    /// Builds a panel that contains a HTML table of A/R data
    /// </summary>
    /// <param name="balance"></param>
    /// <returns></returns>
    public Panel BuildARTable(ARBalance balance)
    {
        Panel pnl = new Panel();

        try
        {
            if (balance == null)
            {
                pnl.Controls.Add(new LiteralControl("<span style='color: orange; font-weight: bold;'>Could not retrieve A/R Balance for this client</span><br/><br/>"));
            }
            else if (balance.TotalAR > 0)
            {
                Table table = new Table();
                table.Width = Unit.Percentage(50);
                table.Style.Add("border", "outset 1px #68849F");
                table.Style.Add("border-collapse", "collapse");
                table.Style.Add("background-color", "white");

                TableRow tr = new TableRow();
                TableCell td = BuildARCell();
                td.Text = "Current";
                td.Width = Unit.Percentage(20);
                td.Font.Bold = true;
                tr.Cells.Add(td);

                td = BuildARCell();
                td.Text = "31 to 60 Day";
                td.Width = Unit.Percentage(20);
                td.Font.Bold = true;
                tr.Cells.Add(td);

                td = BuildARCell();
                td.Text = "61 to 90 Day";
                td.Width = Unit.Percentage(20);
                td.Font.Bold = true;
                tr.Cells.Add(td);

                td = BuildARCell();
                td.Text = "91 to 120 Day";
                td.Width = Unit.Percentage(20);
                td.Font.Bold = true;
                tr.Cells.Add(td);

                td = BuildARCell();
                td.Text = "121+ Days";
                td.Width = Unit.Percentage(20);
                td.Font.Bold = true;
                tr.Cells.Add(td);
                table.Rows.Add(tr);

                tr = new TableRow();
                td = BuildARCell();
                td.Font.Bold = (balance.Current > 0);
                td.Text = "$" + balance.Current.ToString("0.00");
                td.ForeColor = (td.Font.Bold) ? Color.Red : Color.Black;
                tr.Cells.Add(td);

                td = BuildARCell();
                td.Text = "$" + balance.Days_31_to_60.ToString("0.00");
                td.Font.Bold = (balance.Days_31_to_60 > 0);
                td.ForeColor = (td.Font.Bold) ? Color.Red : Color.Black;
                tr.Cells.Add(td);

                td = BuildARCell();
                td.Text = "$" + balance.Days_61_to_90.ToString("0.00");
                td.Font.Bold = (balance.Days_61_to_90 > 0);
                td.Font.Bold = (balance.Days_61_to_90 > 0);
                td.ForeColor = (td.Font.Bold) ? Color.Red : Color.Black;
                tr.Cells.Add(td);

                td = BuildARCell();
                td.Text = "$" + balance.Days_91_to_120.ToString("0.00");
                td.Font.Bold = (balance.Days_91_to_120 > 0);
                td.ForeColor = (td.Font.Bold) ? Color.Red : Color.Black;
                tr.Cells.Add(td);

                td = BuildARCell();
                td.Text = "$" + balance.Days_121_Plus.ToString("0.00");
                td.Font.Bold = (balance.Days_121_Plus > 0);
                td.ForeColor = (td.Font.Bold) ? Color.Red : Color.Black;
                tr.Cells.Add(td);
                table.Rows.Add(tr);


                pnl.Controls.Add(new LiteralControl("<strong>This client has a current A/R balance</strong><br/><br/>"));
                pnl.Controls.Add(table);
                pnl.Controls.Add(new LiteralControl("<br/><strong>Total amount owed by this client is <span style='color: red;'>$" + balance.TotalAR.ToString("0.00") + "</span></strong><br/><br/>"));
                return pnl;
            }
            else
            {
                pnl.Controls.Add(new LiteralControl("<span style='color: blue; font-weight: bold;'>This client does not have a current A/R balance</span><br/><br/>"));
            }
        }
        catch (Exception ex)
        {
            HandleError(ex);
        }

        return pnl;
    }
    private TableCell BuildARCell()
    {
        TableCell td = new TableCell();
        td.Style.Add("border", "solid 1px #68849F");
        //td.Style.Add("border-width: 1px 1px 1px 1px;");
        td.Style.Add("padding", "5px 5px 5px 5px");
        //td.Style.Add("border-style: solid solid solid solid;");
        //td.BackColor = Color.White;
        return td;
    }
    #endregion

    /// <summary>
    /// Shows the current screening memo status in the master page feedback section
    /// </summary>
    public void ShowStatus()
    {
        //validate first
        ValidationException ex = _SessionData.MemoData.Validate();
        if (ex.HasInvalidReasons())
        {
            _MasterPage.FeedbackMessage = "<br/><br/><img src=\"..\\_css\\Red_X_Lrg.png\" height=\"29px\" width=\"29px\" alt=\"failed\" />&nbsp;&nbsp;&nbsp;" + ex.GetInvalidMessage();
            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Error;
            return;
        }

        //show status if valid
        if (_SessionData.MemoData.Tracking.StatusTypeID == TrackingStatusType.None)
        {
            _MasterPage.FeedbackMessage = "<br/><br/><img src=\"..\\_css\\green_check.jpg\" alt=\"passed\" /> This screening memo passes validation and can be submitted";
            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Confirm;
            return;
        }
        //commented out by A Reimer 03/22/12 for "Rejected to Modification Requested" project
        //if (_SessionData.MemoData.Tracking.StatusTypeID == TrackingStatusType.Rejected)
        if(_SessionData.MemoData.Tracking.StatusTypeID == TrackingStatusType.Rejected || 
            _SessionData.MemoData.Tracking.StatusTypeID == TrackingStatusType.ModificationRequested)
        {
            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Error;
            //commented out by A Reimer 03/22/12 for "Rejected to Modification Requested" project
            //_MasterPage.FeedbackMessage = "This memo has been " + _SessionData.MemoData.Tracking.GetStatus();
            _MasterPage.FeedbackMessage = _SessionData.MemoData.Tracking.GetStatus().Contains("Modification") ?
                "This memo requires modification" : "This memo has been " + _SessionData.MemoData.Tracking.GetStatus();
                
                

            if (_SessionData.MemoData.Tracking.RejectionHold.HasValue && _SessionData.MemoData.Tracking.RejectionHold.Value)
            {
                //commented out by A Reimer 03/22/12 for "Rejected to Modification Requested" project
                //_MasterPage.FeedbackMessage += ". Please make amendments to this memo and resubmit. Rejection Reason(s):<ul>";
                _MasterPage.FeedbackMessage += ". Please make amendments to this memo and resubmit. Reason(s) for Modification Request:<ul>";
                if (!string.IsNullOrEmpty(_SessionData.MemoData.Tracking.APRejectionReason))
                    _MasterPage.FeedbackMessage += "<li>" + _SessionData.MemoData.Tracking.APRejectionReason + "</li>";
                if (!string.IsNullOrEmpty(_SessionData.MemoData.Tracking.PGMRejectionReason))
                    _MasterPage.FeedbackMessage += "<li>" + _SessionData.MemoData.Tracking.PGMRejectionReason + "</li>";
                if (!string.IsNullOrEmpty(_SessionData.MemoData.Tracking.AdversaryRejectionReason))
                    _MasterPage.FeedbackMessage += "<li>" + _SessionData.MemoData.Tracking.AdversaryRejectionReason + "</li>";
                _MasterPage.FeedbackMessage += "</ul>";
            }
        }
        else
        {
            switch (_SessionData.MemoData.Tracking.StatusTypeID)
            {
                case TrackingStatusType.Submitted:
                    _MasterPage.FeedbackMessage = "This screening memo is awaiting approval.";
                    break;
                case TrackingStatusType.AdversaryGroupProcessing:
                    _MasterPage.FeedbackMessage = "This screening memo has been approved and is being processed.";
                    break;
                case TrackingStatusType.Completed:
                    _MasterPage.FeedbackMessage = "This screening memo has been processed and completed.";
                    break;
            }
            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Confirm;
        }
    }


    //public bool AllowNDrive(string pracCode)
    //{
    //    return true;
    //    //if (string.IsNullOrEmpty(pracCode))
    //    //    return false;
    //    //return pracCode.Contains("231") ||
    //    //       pracCode.Contains("232") ||
    //    //       pracCode.Contains("233");
    //}

    #region EMAIL METHODS

    /// <summary>
    /// Sends an email to all necessary memo approvers
    /// </summary>
    /// <param name="memo"></param>
    public void SendApproversEmail(Memo memo) { SendApproversEmail(memo, false); }
    /// <summary>
    /// Sends an email to all necessary memo approvers
    /// </summary>
    /// <param name="memo"></param>
    /// <param name="isResubmit"></param>
    public void SendApproversEmail(Memo memo, bool isResubmit)
    {
        StringBuilder msg = new StringBuilder();
        if (memo.Tracking.StatusTypeID == TrackingStatusType.AdversaryGroupProcessing)
        {
            msg.Append("Screening Memo #" + memo.ScrMemID + " has been approved for processing. Please click the link below to access your queue of new matters that require your review.");
            msg.AppendLine();
            msg.AppendLine(GetBaseUrl() + "Admin/AdminQueue.aspx");
        }
        else
        {
            msg.AppendLine("A new matter screening issue requires your attention. Please click the link below to access your queue of new matters that require your review.");
            msg.AppendLine();
            msg.AppendLine(GetBaseUrl() + "Admin/Queue.aspx");
        }
        msg.AppendLine();
        msg.AppendLine();
        //msg.AppendLine("The approval page for this memo can be found here:");
        //msg.AppendLine();
        //msg.AppendLine(urlRoot + "Admin/Approval.aspx?ScrMemID=" + memo.ScrMemID);
        //msg.AppendLine();
        //msg.AppendLine();

        MailAddressCollection adminRecipients = new MailAddressCollection();
        MailAddress from = new MailAddress(ConfigurationManager.AppSettings["EmailSendAddress"]);
        List<AdminLogins> admins = new List<AdminLogins>();
        string adminMsg = string.Empty;
        switch (memo.Tracking.StatusTypeID)
        {
            case TrackingStatusType.Submitted:
                
                //set the AP recipients for non-Denver office memos
                if (memo.RespAttOfficeName != "Denver" && memo.ScrMemType != 6)
                {
                    //AdminLogins[] aps = AdminLogins.GetAPsByOffice(memo.OrigOffice, true);
                    try
                    {
                        AdminLogins[] aps = AdminLogins.GetAPsByOfficeName(memo.RespAttOfficeName, true);
                        if (aps.Count() == 0)
                        {
                            admins.AddRange(AdminLogins.GetAdversaryGroupAdmins());
                            CMSData cms = CMSData.GetInstance();
                            adminMsg += "* A corresponding AP was not found for the " + memo.RespAttOfficeName + " office.\r\n\r\n";
                            //adminMsg += "Forwarding approval request to adversary group manager.\r\n\r\n";
                        }
                        else
                        {
                            admins.AddRange(aps);
                        }
                    }
                    catch (Exception ex)
                    {
                        admins.AddRange(AdminLogins.GetAdversaryGroupAdmins());
                        CMSData cms = CMSData.GetInstance();
                        adminMsg += "* An error occurred when attempting a AP for office '" + memo.RespAttOfficeName + ". ";
                        adminMsg += "Forwarding approval request to adversary group manager. The error message has been sent to development\r\n\r\n";
                        SendErrorEmail(ex);
                    }
                }

                //set the PGM recipients
                AdminLogins[] pgms = AdminLogins.GetPGMsByPracticeCode(memo.PracCode, true);
                if (pgms.Count() == 0 && memo.ScrMemType != 6)
                {
                    admins.AddRange(AdminLogins.GetAdversaryGroupAdmins());
                    CMSData cms = CMSData.GetInstance();
                    adminMsg += "* A corresponding PGL was not found for practice code " + cms.GetPracticeName(memo.PracCode) + ".\r\n\r\n";
                    //adminMsg += "Forwarding approval request to adversary group manager.\r\n\r\n";
                }
                else
                {
                    admins.AddRange(pgms);
                }

                //add adversary admin
                admins.AddRange(AdminLogins.GetAdversaryGroupAdmins());

                //per Mark's request, use. NoReply
                //try
                //{
                //    //if the WS fails just use NoReply as From address
                //    from = new MailAddress(GetEmployeeEmailAddress(memo.AttEntering));
                //}
                //catch { }
                break;
            //case TrackingStatusType.AdversaryGroupSubmitted:
            //    admins.AddRange(AdminLogins.GetAdversaryGroupAdmins());
            //break;
            case TrackingStatusType.AdversaryGroupProcessing:
                admins.AddRange(AdminLogins.GetAdversaryGroup());
                break;
            default:
                admins.AddRange(AdminLogins.GetAdversaryGroupAdmins());
                break;
            //throw new ArgumentException("Could not send tracking email. Status " + memo.Tracking.StatusTypeID + " is not a valid status");
        }

        if (admins.Count == 0)
            throw new Exception("Could not send tracking email, no valid admin recipients found to send to");

        foreach (AdminLogins admin in admins)
            adminRecipients.Add(admin.Email);

        if (!string.IsNullOrEmpty(adminMsg))
            msg.AppendLine(adminMsg + "\r\n\r\n");
        msg.AppendLine(BuildMemoStatusEmail(memo));
        msg.AppendLine();
        msg.AppendLine(BuildMemoApproverEmail(memo));


        string subject = "Screening Memo #" + memo.ScrMemID;
        if (memo.Tracking.StatusTypeID == TrackingStatusType.AdversaryGroupProcessing)
            subject += " - APPROVED";
        else if (isResubmit)
            subject += " - RESUBMITTED";
        else if (memo.Tracking.StatusTypeID == TrackingStatusType.Submitted)
            subject += " - SUBMITTED";
        SendEmail(adminRecipients, subject, msg.ToString());
    }
    /// <summary>
    /// Generates body text for an approval email
    /// </summary>
    /// <param name="memo"></param>
    /// <returns></returns>
    public string BuildMemoApproverEmail(Memo memo)
    {
        StringBuilder msg = new StringBuilder();

        if (!string.IsNullOrEmpty(memo.Tracking.APSignature))
        {
            msg.Append("AP Signature: " + memo.Tracking.APSignature);
            msg.AppendLine((memo.Tracking.APDate.HasValue) ? " (" + memo.Tracking.APDate.Value + ")" : string.Empty);
            msg.AppendLine();
        }
        if (!string.IsNullOrEmpty(memo.Tracking.PGMSignature))
        {
            msg.Append("PGL Signature: " + memo.Tracking.PGMSignature);
            msg.AppendLine((memo.Tracking.PGMDate.HasValue) ? " (" + memo.Tracking.PGMDate.Value + ")" : string.Empty);
            msg.AppendLine();
        }

        if (!string.IsNullOrEmpty(memo.Tracking.APARSignature))
            msg.AppendLine("AP Approval for A/R over 90 days: " + memo.Tracking.APARSignature + "\r\n");
        if (!string.IsNullOrEmpty(memo.Tracking.PGMARSignature))
            msg.AppendLine("PGM Approval for A/R over 90 days: " + memo.Tracking.PGMARSignature + "\r\n");
        if (!string.IsNullOrEmpty(memo.Tracking.APExceptionSignature))
            msg.AppendLine("AP Exception approval for signed engagement letter: " + memo.Tracking.APExceptionSignature + "\r\n");
        if (!string.IsNullOrEmpty(memo.Tracking.PGMExceptionSignature))
            msg.AppendLine("PGL Exception approval for signed engagement letter: " + memo.Tracking.PGMExceptionSignature + "\r\n");

        if (!string.IsNullOrEmpty(memo.Tracking.Opened))
            msg.AppendLine("Opened: " + memo.Tracking.Opened + "\r\n");
        if (!string.IsNullOrEmpty(memo.Tracking.Conflicts))
            msg.AppendLine("Conflicts: " + memo.Tracking.Conflicts + "\r\n");
        if (!string.IsNullOrEmpty(memo.Tracking.FeeSplits))
            msg.AppendLine("Fee Splits: " + memo.Tracking.FeeSplits + "\r\n");
        if (!string.IsNullOrEmpty(memo.Tracking.FinalCheck))
            msg.AppendLine("Final Check: " + memo.Tracking.FinalCheck + "\r\n");

        if (!string.IsNullOrEmpty(memo.Tracking.Notes))
        {
            msg.AppendLine("Notes:");
            msg.AppendLine();
            msg.AppendLine(memo.Tracking.Notes);
            msg.AppendLine();
            msg.AppendLine();
        }
        if (!string.IsNullOrEmpty(memo.Tracking.APNotes))
        {
            msg.AppendLine("Additional AP Notes:");
            msg.AppendLine();
            msg.AppendLine(memo.Tracking.APNotes);
            msg.AppendLine();
            msg.AppendLine();
        }
        if (!string.IsNullOrEmpty(memo.Tracking.PGMNotes))
        {
            msg.AppendLine("Additional PGL Notes:");
            msg.AppendLine();
            msg.AppendLine(memo.Tracking.PGMNotes);
            msg.AppendLine();
            msg.AppendLine();
        }

        return msg.ToString();
    }

    /// <summary>
    /// Sends a status notification to the memo's originating party
    /// </summary>
    /// <param name="memo"></param>
    public void SendStatusEmail(Memo memo) { SendStatusEmail(memo, false); }
    /// <summary>
    /// Sends a status notification to the memo's originating party
    /// </summary>
    /// <param name="memo"></param>
    /// <param name="isResubmit"></param>
    public void SendStatusEmail(Memo memo, bool isResubmit)
    {
        string url = GetBaseUrl() + "Screening/Summary.aspx?ScrMemID=" + memo.ScrMemID;
        string msg = string.Empty;
        string subject = "Screening Memo #" + memo.ScrMemID;
        bool cmNumSent = false;
        //commented out by A Reimer 03/22/12 for "Rejected to Modification Requested" project
        //if (memo.Tracking.StatusTypeID == TrackingStatusType.Rejected)
        if(memo.Tracking.StatusTypeID == TrackingStatusType.Rejected || memo.Tracking.StatusTypeID == TrackingStatusType.ModificationRequested)
        {
            StringBuilder rejectionMsg = new StringBuilder();
            //commented out by A Reimer 03/22/12 for "Rejected to Modification Requested" project
            //rejectionMsg.Append("This memo has been rejected. ");
            rejectionMsg.Append("This memo requires modification. ");

            if (memo.Tracking.RejectionHold.HasValue && memo.Tracking.RejectionHold.Value)
                rejectionMsg.Append("Please make amendments to this memo and resubmit. ");

            rejectionMsg.AppendLine();
            rejectionMsg.AppendLine();
            rejectionMsg.AppendLine("You can login to the New Business Intake Website and amend this memo at:\r\n\r\n" + url);
            rejectionMsg.AppendLine();
            //commented out by A Reimer 03/22/12 for "Rejected to Modification Requested" project
            //rejectionMsg.AppendLine("Rejection Reason(s):");
            rejectionMsg.AppendLine("Reason(s) for Modification Request:");
            rejectionMsg.AppendLine();
            if (!string.IsNullOrEmpty(memo.Tracking.APRejectionReason))
            {
                //rejectionMsg.AppendLine("From " + AdminLogins.GetAdminLoginNameFromID(memo.Tracking.APUserID));
                rejectionMsg.AppendLine("+ " + _SessionData.MemoData.Tracking.APRejectionReason);
                rejectionMsg.AppendLine();
            }
            if (!string.IsNullOrEmpty(memo.Tracking.PGMRejectionReason))
            {
                rejectionMsg.AppendLine("+ " + _SessionData.MemoData.Tracking.PGMRejectionReason);
                rejectionMsg.AppendLine();
            }
            if (!string.IsNullOrEmpty(memo.Tracking.AdversaryRejectionReason))
            {
                rejectionMsg.AppendLine("+ " + _SessionData.MemoData.Tracking.AdversaryRejectionReason);
                rejectionMsg.AppendLine();
            }
            rejectionMsg.AppendLine();

            msg = BuildMemoStatusEmail(memo);
            msg += rejectionMsg.ToString();
            //commented out by A Reimer 03/22/12 for "Rejected to Modification Requested" project
            //subject += " - REJECTED";
            subject += " - MODIFICATION REQUESTED";
        }
        else
        {
            msg = BuildMemoStatusEmail(memo);
            msg += "\r\n\r\nYou can login to the New Business Intake Website and view this memo at:\r\n\r\n" + url + "\r\n\r\n";
            if (isResubmit)
            {
                subject += " - RESUBMITTED";
            }
            else if ((memo.Tracking.StatusTypeID == TrackingStatusType.AdversaryGroupProcessing ||
                      memo.Tracking.StatusTypeID == TrackingStatusType.Completed) &&
                     memo.ClientMatterNumbers.Count > 0)
            {
                subject += " - CLIENT MATTER NUMBERS";
                cmNumSent = true;
            }
            else if (memo.Tracking.StatusTypeID == TrackingStatusType.Submitted)
                msg = "Your screening memo has been submitted for approval.\r\n\r\n" + msg;
        }

        const string ERROR = "Could not send status email because one or more recipients were not valid.";
        MailAddressCollection acknowledgeRecipients = new MailAddressCollection();

        if (string.IsNullOrEmpty(memo.AttEntering) || string.IsNullOrEmpty(memo.PersonnelID))
            throw new Exception(ERROR);

        FirmDirectory fd = new FirmDirectory();
        try
        {
            acknowledgeRecipients.Add(fd.GetEmployeeEmailAddress(memo.AttEntering));
            acknowledgeRecipients.Add(fd.GetEmployeeEmailAddress(memo.PersonnelID));
            if (!string.IsNullOrEmpty(memo.RespAttID))
                acknowledgeRecipients.Add(fd.GetEmployeeEmailAddress(memo.RespAttID));

            if (cmNumSent && !string.IsNullOrEmpty(ConfigurationManager.AppSettings["EmailOnCMNumIssued"]))
            {
                try
                {
                    string[] recipients = ConfigurationManager.AppSettings["EmailOnCMNumIssued"].Trim().Split(new Char[] { ';' }, StringSplitOptions.None);
                    foreach (string recipient in recipients)
                        if (recipient != null && recipient.Trim() != string.Empty)
                            acknowledgeRecipients.Add(new MailAddress(recipient.Trim()));
                }
                catch { }
            }
        }
        catch
        {
            throw new Exception(ERROR);
        }

        SendEmail(acknowledgeRecipients, subject, msg);
    }
    public static string BuildMemoStatusEmail(Memo memo)
    {
        StringBuilder memoSummary = new StringBuilder();
        CMSData cms = CMSData.GetInstance();
        memoSummary.AppendLine();
        memoSummary.AppendLine("Screening Memo #:\t\t\t" + memo.ScrMemID);
        memoSummary.AppendLine();
        memoSummary.AppendLine("Memo Type:\t\t\t\t" + memo.GetMemoTypeDescription());
        memoSummary.AppendLine();
        if (memo.ScrMemType != 6)
        {
            memoSummary.AppendLine("Practice Code:\t\t\t" + cms.GetPracticeName(memo.PracCode) + " (" + memo.PracCode + ")");
            memoSummary.AppendLine();
        }
        memoSummary.AppendLine("Status:\t\t\t\t" + memo.Tracking.GetStatus());
        memoSummary.AppendLine();
        if (!string.IsNullOrEmpty(memo.AttEntering))
        {
            memoSummary.AppendLine("Attorney Preparing Memo:\t" + cms.GetUserNameByCode(memo.AttEntering) + " (" + memo.AttEntering + ")");
            memoSummary.AppendLine();
        }
        if (!string.IsNullOrEmpty(memo.PersonnelID))
        {
            memoSummary.AppendLine("Memo Input By:\t\t\t" + cms.GetUserNameByCode(memo.PersonnelID) + " (" + memo.PersonnelID + ")");
            memoSummary.AppendLine();
        }
        if (!string.IsNullOrEmpty(memo.RespAttID))
        {
            memoSummary.AppendLine("Billing Attorney:\t\t\t" + cms.GetUserNameByCode(memo.RespAttID) + " (" + memo.RespAttID + ")");
            memoSummary.AppendLine();
        }
        memoSummary.AppendLine();
        string clientName = GetClientDisplayName(memo);
        if (!string.IsNullOrEmpty(clientName))
        {
            memoSummary.AppendLine("Client:");
            memoSummary.AppendLine();
            memoSummary.AppendLine(clientName);
            memoSummary.AppendLine();
        }
        if (memo.ClientMatterNumbers.Count > 0)
        {
            memoSummary.AppendLine("Client Matter Numbers:");
            memoSummary.AppendLine();
            foreach (ClientMatterNumber cmNum in memo.ClientMatterNumbers)
                memoSummary.AppendLine(cmNum.Number);
            memoSummary.AppendLine();
        }
        if (!string.IsNullOrEmpty(memo.MatterName))
        {
            memoSummary.AppendLine("Matter Name:");
            memoSummary.AppendLine();
            memoSummary.AppendLine(memo.MatterName);
            memoSummary.AppendLine();
        }
        if (!string.IsNullOrEmpty(memo.WorkDesc))
        {
            memoSummary.AppendLine("Work Description:");
            memoSummary.AppendLine();
            memoSummary.AppendLine(memo.WorkDesc);
            memoSummary.AppendLine();
        }
        

        return memoSummary.ToString();
    }

    /// <summary>
    /// Sends adversary request notifications
    /// </summary>
    /// <param name="request"></param>
    public void SendAdversaryRequestEmail(Requests request)
    {
        FirmDirectory fd = new FirmDirectory();
        CMSData cms = CMSData.GetInstance();
        bool complete = request.IsComplete;
        MailAddressCollection to = new MailAddressCollection();

        if (!complete)
        {
            foreach (AdminLogins adv in AdminLogins.GetAdversaryGroup())
                to.Add(adv.Email);
        }

        try
        {
            if (!string.IsNullOrEmpty(request.TypistLogin))
                to.Add(fd.GetEmployeeEmailAddress(request.TypistLogin));
            if (!string.IsNullOrEmpty(request.AttyLogin))
                to.Add(fd.GetEmployeeEmailAddress(request.AttyLogin));
            //to.Add(fd.GetUserDataByCode(request.TypistLogin).Rows[0]["LOGIN"] + "@hollandhart.com");
            //to.Add(fd.GetUserDataByCode(request.AttyLogin).Rows[0]["LOGIN"] + "@hollandhart.com");
        }
        catch { }

        StringBuilder msg = new StringBuilder();
        bool existing = !string.IsNullOrEmpty(request.WorkBegunBy);
        MailAddress from = new MailAddress("AdversaryRequest@hollandhart.com");
        string subject;
        string header;
        if (complete)
        {
            subject = "Adversary Request Processed and Completed";
            header = "Adversary Request Completed By " + request.WorkCompletedBy;
        }
        else
        {
            subject = (!existing) ? "New Adversary Request" : "Adversary Request Updated";
            header = "Adversary Request Submitted";
        }

        if (existing && !complete)
            msg.AppendLine("EDIT NOTICE: A REQUEST ASSIGNED TO " + request.WorkBegunBy + " HAS BEEN UPDATED BY USER." + "\r\n\r\n");
        msg.AppendLine(header);
        msg.AppendLine();
        msg.AppendLine();
        msg.AppendLine("Request # " + request.ReqID);
        msg.AppendLine();
        msg.AppendLine("Submitted On: " + request.SubmittalDate);
        msg.AppendLine();
        msg.AppendLine("Client Name: " + request.ClientName);
        msg.AppendLine();
        msg.AppendLine("Client Matter Number: " + request.ClientMatter);
        msg.AppendLine();
        msg.AppendLine("Attorney: " + cms.GetUserNameByCode(request.AttyLogin));
        msg.AppendLine();
        msg.AppendLine("Submitted By: " + cms.GetUserNameByCode(request.TypistLogin));
        msg.AppendLine();

        SendEmail(to, from, subject, msg.ToString(), false);
    }

    public void SendEmail(MailAddressCollection to, string subject, string msg) { SendEmail(to, null, subject, msg, false, null); }
    public void SendEmail(MailAddressCollection to, MailAddress from, string subject, string msg, bool isBodyHtml) { SendEmail(to, from, subject, msg, false, null); }
    public void SendEmail(MailAddressCollection to, MailAddress from, string subject, string msg, bool isBodyHtml, NetworkCredential credentials)
    {
        
        SmtpClient client = new SmtpClient(ConfigurationManager.AppSettings["EmailHost"]);
        if (credentials != null)
        {
            client.UseDefaultCredentials = false;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.Credentials = credentials;
        }
        if (from == null)
            from = new MailAddress(ConfigurationManager.AppSettings["EmailSendAddress"]);
        
        #region Test Mode addition 11/21/2012
        string _IsTestMode = "1";
        try
        {
            _IsTestMode = ConfigurationManager.AppSettings["TestMode"].ToString();
        }
        catch 
        {
            _IsTestMode = "1";
        }

        if (_IsTestMode == "1")
        {
            to.Clear();
            to.Add(new MailAddress("amreimer@hollandhart.com"));
            to.Add(new MailAddress("mnayak@hollandhart.com"));
            from = new MailAddress("newbusinesstest@hollandhart.com");
        }
        #endregion

        
        Email.SendEmail(client, to, from, subject, msg, isBodyHtml);
        
    }
    #endregion


    #endregion

    
    #region FILE UPLOAD CONTROL METHODS

    //public void SaveFile(HttpPostedFile file, Attachments attachment, string fileName)
    //{
    //    string dir = GetUploadDirectoryPath(attachment);
    //    if (!Directory.Exists(dir))
    //        Directory.CreateDirectory(dir);
    //    string path = dir + "\\" + fileName;
    //    file.SaveAs(path);
    //}

    //public void DeleteFile(Attachments attachment)
    //{
    //    string path = GetUploadDirectoryPath(attachment);
    //    path = path + "\\" + attachment.FileName;
    //    if (File.Exists(path))
    //        File.Delete(path);
    //}

    //public void RenameFile(Attachments attachment, string newName)
    //{
    //    string path = GetUploadDirectoryPath(attachment);
    //    string oldFile = path + "\\" + attachment.FileName;
    //    string newFile = path + "\\" + newName;
    //    if (File.Exists(oldFile))
    //        File.Move(oldFile, newFile);
    //}

    /// <summary>
    /// Returns the complete local directory path for any screening memo attachment files
    /// </summary>
    /// <param name="attachment"></param>
    /// <returns></returns>
    public static string GetAttachmentDirectoryPath(Attachments attachment) { return GetAttachmentDirectoryPath(attachment.ScrMemID.ToString()); }
    /// <summary>
    /// Returns the complete local directory path for any screening memo attachment files
    /// </summary>
    /// <param name="scrMemId"></param>
    /// <returns></returns>
    public static string GetAttachmentDirectoryPath(string scrMemId)
    {
        //return Server.MapPath("~\\" + ConfigurationManager.AppSettings["FileUploadVirtualDirectory"]) + "\\" + scrMemId;
        return ConfigurationManager.AppSettings["FileUploadDirectory"] + "\\" + scrMemId;
    }

    /// <summary>
    /// Returns the URL to access a particular screening memo file
    /// </summary>
    /// <param name="attachment"></param>
    /// <returns></returns>
    public Uri GetAttachmentDownloadURL(Attachments attachment)
    {
        //Uri uri = new Uri(GetBaseUrl() + ConfigurationManager.AppSettings["FileUploadVirtualDirectory"] + "/" + scrMemId);
        string fileName = HttpUtility.UrlEncode(attachment.FileName);
        return new Uri(GetBaseUrl() + "Screening/Download.aspx?ScrMemID=" + attachment.ScrMemID + "&FileName=" + fileName);
    }

    /// <summary>
    /// OnFileListItemDataBound in FileListControl, this handles adding a link (FlieListItemControl) to the FileListControl repeater item when the FileListControl is bound to a list of attachments
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public void FileList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Attachments attachment = (Attachments)e.Item.DataItem;
        FileListItemControl c = new FileListItemControl(attachment);
        c.DownloadPage = GetAttachmentDownloadURL(attachment);
        e.Item.Controls.Add(c);
    }

    /// <summary>
    /// OnFileUploaded, this handle the upload
    /// </summary>
    /// <param name="sender">the control that received the upload event</param>
    /// <param name="file">The file stream/name from the client</param>
    /// <param name="description">The file's description</param>
    /// <returns></returns>
    public bool FileListControl_OnFileUploaded(IFileListControl sender, FileUpload file, string description)
    {
        if (!_SessionData.MemoLoaded)
            return false;

        string fileName = file.PostedFile.FileName;

        //trim to the filename if given the whole path 
        if (fileName.Contains("\\"))
            fileName = fileName.Substring(fileName.LastIndexOf("\\") + 1);

        if (_SessionData.MemoData.AttachedFileExists(fileName))
        {
            sender.SetStatusMessage("File upload failed. File " + fileName + " already exists.", Color.Red);
            return false;
        }

        Attachments attachment = new Attachments();
        attachment.ScrMemID = _SessionData.MemoData.ScrMemID;
        attachment.AttachmentID = 0;
        attachment.FileDescription = description;
        attachment.FileName = fileName;

        DoDataSave(attachment); //SaveData(attachment, AdversaryData.GetInstance());
        //SaveFile(file.PostedFile, attachment, fileName);
        FileUtils.UploadFile(file.PostedFile, GetAttachmentDirectoryPath(attachment));
        _SessionData.MemoData.Attachments.Add(attachment);

        return true;
    }

    /// <summary>
    /// When the edit file info link is clicked in a FileListControl, this handles it
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="dataItem"></param>
    /// <returns></returns>
    public bool FileListControl_OnFileEditClick(IFileListControl sender, object dataItem)
    {
        Attachments attachment = (Attachments)dataItem;
        sender.FileName = attachment.FileName;
        sender.FileDescription = attachment.FileDescription;
        return true;
    }

    /// <summary>
    /// When file info is deleted in a FileListControl, this handles it
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="dataItem"></param>
    /// <returns></returns>
    public bool FileListControl_OnFileDeleteClick(IFileListControl sender, object dataItem)
    {
        Attachments attachment = (Attachments)dataItem;
        using (SqlConnection conn = AdversaryData.GetConnection())
        {
            //DatabaseWorker worker = AdversaryData.GetInstance();
            DatabaseWorker worker = new DatabaseWorker(conn);
            //DeleteFile(attachment);
            string path = GetAttachmentDirectoryPath(attachment) + "\\" + attachment.FileName;
            FileUtils.DeleteFile(path);
            worker.Delete(attachment);
            _SessionData.MemoData.Attachments.Remove(attachment);
            return true;
        }
    }

    /// <summary>
    /// When file info is edited in a FileListControl, this handles it
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="dataItem"></param>
    /// <param name="newFileName"></param>
    /// <param name="description"></param>
    /// <returns></returns>
    public bool FileListControl_OnFileEditSubmit(IFileListControl sender, object dataItem, string newFileName, string description)
    {
        Attachments attachment = (Attachments)dataItem;
        bool renamed = (newFileName.ToLower() != attachment.FileName.ToLower());

        //if the file has been renamed, check if another file with the same name already exists
        if (renamed && _SessionData.MemoData.AttachedFileExists(newFileName))
        {
            sender.SetStatusMessage("File rename failed. File " + newFileName + " already exists.", Color.Red);
            return false;
        }

        //rename the physical file
        if (renamed)
        {
            //RenameFile(attachment, newFileName);
            string path = GetAttachmentDirectoryPath(attachment);
            string oldPath = path + "\\" + attachment.FileName;
            string newPath = path + "\\" + newFileName;
            FileUtils.MoveFile(oldPath, newPath);
        }

        //upate the DB
        attachment.FileDescription = description;
        attachment.FileName = newFileName;
        //AdversaryData.Set(attachment);
        //SaveData(attachment, AdversaryData.GetInstance());
        DoDataSave(attachment);

        //update the session
        _SessionData.MemoData.Attachments.Remove(attachment);
        _SessionData.MemoData.Attachments.Add(attachment);

        return true;
    }
    #endregion

}
