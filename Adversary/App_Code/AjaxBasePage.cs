﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using HH.UI;
using System.Text.RegularExpressions;
using HH.Utils;
using System.Data.SqlClient;
using HH.SQL;
using HH.Branding;
using HH.UI.AutoComplete;

/// <summary>
/// Handles all of the calls from AJAX controls, inherits from BasePage
/// </summary>
public class AjaxBasePage : BasePage
{
    public AjaxBasePage() { }


    #region AUTOCOMPLETE METHODS
    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod]
    public static string[] GetMemoList(string prefixText, int count, string contextKey)
    {
        try
        {
            DataTable dt = HandleAutoCompleteSearch<Memo>(prefixText, count, contextKey);
            List<string> results = new List<string>((dt.Rows.Count > count) ? count : dt.Rows.Count);
            foreach (DataRow dr in dt.Rows)
                results.Add(dr[contextKey].ToString());
            return results.ToArray();
        }
        catch (Exception ex)
        {
            if (_DebugMode)
                return new string[] { "<span style=\"font-color: red\">Could not return values: " + ex.Message + "</span>" };
            else
                return new string[0];
        }
    }
    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod]
    public static string[] GetClientMatterNumberList(string prefixText, int count, string contextKey)
    {
        try
        {
            DataTable dt = HandleAutoCompleteSearch<ClientMatterNumber>(prefixText, count, "Number");
            List<string> results = new List<string>((dt.Rows.Count > count) ? count : dt.Rows.Count);
            foreach (DataRow dr in dt.Rows)
                results.Add(dr[contextKey].ToString());
            return results.ToArray();
        }
        catch (Exception ex)
        {
            if (_DebugMode)
                return new string[] { "<span style=\"font-color: red\">GetClientMatterNumberList returned an error: " + ex.Message + "</span>" };
            else
                return new string[0];
        }
    }
    public static DataTable HandleAutoCompleteSearch<DataType>(string prefixText, int count, string propertyName)
    {
        Regex hasAlpha = new Regex("[^0-9]");
        if (propertyName == "ScrMemID" && hasAlpha.IsMatch(prefixText))
            return new DataTable();

        DataType data = (DataType)ReflectionUtils.CreateNewInstanceOfType(typeof(DataType));
        //DataType data = (DataType)typeof(DataType).Assembly.CreateInstance(typeof(DataType).Name);
        ReflectionUtils.CastAndSetProperty(data, propertyName, prefixText);

        //DatabaseWorker worker = AdversaryData.GetInstance();
        DataTable dt = null;
        using (SqlConnection conn = new SqlConnection(AdversaryData.ConnectionString))
        {
            DatabaseWorker worker = new DatabaseWorker(conn);
            dt = worker.SearchToDataTable(data);
            conn.Close();
        }
        return dt;
    }

    #region EMPLOYEE SEARCH AUTOCOMPLETE METHODS
    private static readonly string TB_EMP_SEARCH_MSG = " (Type employee name or number to start searching) ";

    public AutoCompleteField BuildEmployeeSearchAutoComplete(string id, string contextKey) { return BuildEmployeeSearchAutoComplete(id, contextKey, null); }
    public AutoCompleteField BuildEmployeeSearchAutoComplete(string id, string contextKey, string initialValue)
    {
        AutoCompleteField searchBox = new AutoCompleteField(id, "GetEmployeeList", contextKey);
        searchBox.TextBoxSearchMessage = TB_EMP_SEARCH_MSG;
        searchBox.ClearButtonText = "Clear Employee";

        if (!string.IsNullOrEmpty(initialValue))
        {
            string[] results = GetEmployeeList(initialValue, 1, contextKey);
            if (results.Count() != 1)
            {
                _MasterPage.FeedbackMode = MasterPageFeedbackMode.Warning;
                _MasterPage.FeedbackMessage = "Could not find a matching employee with ID " + initialValue + " in the database. Please double check that you have entered it correctly.";
                searchBox.Text = initialValue;
                searchBox.Mode = AutoCompleteFieldMode.ShowSearchBox;
            }
            else
            {
                searchBox.Text = results[0];
                searchBox.Mode = AutoCompleteFieldMode.ShowResults;
            }
        }

        return searchBox;
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod]
    public static string[] GetEmployeeList(string prefixText, int count, string contextKey)
    {
        if (string.IsNullOrEmpty(prefixText) ||
            ValidEmployeeSelection(prefixText))
            return new string[0];

        CMSData cms = CMSData.GetInstance();
        DataTable dt = cms.DoEmployeeSearch(prefixText);
        List<string> results = new List<string>((dt.Rows.Count > count) ? count : dt.Rows.Count);
        foreach (DataRow dr in dt.Rows)
        {
            //a reimer 08/09/11 - remove "AMEX" names
            if (dr[CMSData.EMP_NAME].ToString().StartsWith("AMEX")) continue;
            results.Add(dr[CMSData.EMP_NAME] + " (" + dr[CMSData.EMP_CODE] + ")");
            if (results.Count == count)
                break;
        }
        return results.ToArray();
    }

    public string ParseSearchRequestTextForNumber(string text)
    {
        text = text.Trim();

        if (string.IsNullOrEmpty(text))
            return string.Empty;

        Regex hasAplha = new Regex("[A-Za-z]");
        if (!hasAplha.IsMatch(text))
        {
            try
            {
                Regex hasNumeric = new Regex("^[0-9]+$");
                text = text.Trim();
                if (hasNumeric.IsMatch(text))
                    return text;
            }
            catch { }
            return string.Empty;
        }
        else
        {
            if (!text.Contains("(") || !text.Contains(")"))
                return string.Empty;
            text = text.Substring(text.LastIndexOf("(") + 1);
            text = text.Replace(")", "");
            try
            {
                Regex hasNumeric = new Regex("^[0-9]+$");
                text = text.Trim();
                if (hasNumeric.IsMatch(text))
                    return text;
            }
            catch { }
            return string.Empty;
        }
    }

    public static bool ValidEmployeeSelection(string text)
    {
        return (text.Contains("(") &&
                text.Contains(")") &&
                !text.Contains(TB_EMP_SEARCH_MSG));
    }
    #endregion

    #region CLIENT SEARCH AUTOCOMPLETE METHODS
    private static readonly string TB_CLIENT_SEARCH_MSG = " (Type client name or number to start searching) ";

    public AutoCompleteField BuildClientSearchAutoComplete(string id, string contextKey, string clientNumber)
    {
        AutoCompleteField searchBox = new AutoCompleteField(id, "GetClientList", contextKey);
        searchBox.TextBoxSearchMessage = TB_CLIENT_SEARCH_MSG;
        searchBox.ClearButtonText = "Clear Client";

        if (!string.IsNullOrEmpty(clientNumber))
        {
            string[] results = DoClientSearch(string.Empty, clientNumber, 50);
            if (results.Count() != 1)
            {
                _MasterPage.FeedbackMode = MasterPageFeedbackMode.Warning;
                _MasterPage.FeedbackMessage = "Could not find a matching client with ID " + clientNumber + " in the database. Please double check that you have entered it correctly.";
                searchBox.Text = clientNumber;
                searchBox.Mode = AutoCompleteFieldMode.ShowSearchBox;
            }
            else
            {
                searchBox.Text = results[0];
                searchBox.Mode = AutoCompleteFieldMode.ShowResults;
            }
        }

        return searchBox;
    }



    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod]
    public static string[] GetClientList(string prefixText, int count, string contextKey)
    {
        if (ValidClientSelection(prefixText.Trim()))
            return new string[0];

        string clientName = string.Empty;
        string clientCode = string.Empty;

        //parse the input text to determine whether it is a client name (alpha numeric) or client code
        Regex hasAlpha = new Regex("[^0-9]");
        if (hasAlpha.IsMatch(prefixText))
            clientName = prefixText;
        else
            clientCode = prefixText;

        return DoClientSearch(clientName, clientCode, count);
    }
    private static string[] DoClientSearch(string clientName, string clientCode, int count)
    {
        CMSData cms = CMSData.GetInstance();
        DataTable dt = cms.DoClientSearch(clientName.Trim(), clientCode.Trim());
        List<string> results = new List<string>((count > dt.Rows.Count) ? count : dt.Rows.Count);
        foreach (DataRow dr in dt.Rows)
        {
            if (results.Count >= count)
                break;

            try
            {
                string result = dr[CMSData.CLIENT_NAME].ToString() + " (" + dr[CMSData.CLIENT_CODE].ToString().Trim() + ")";
                results.Add(result);
            }
            catch { }
        }

        return results.ToArray();
    }

    public static bool ValidClientSelection(string text)
    {
        return (text.Contains("(") &&
                text.Contains(")") &&
                !text.Contains(TB_CLIENT_SEARCH_MSG));
    }

    #endregion

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod]
    public static string[] GetAdminList(string prefixText, int count, string contextKey)
    {
        try
        {
            var query = from a in AdminLogins.AllAdmins
                        where (!string.IsNullOrEmpty(a.Login) && a.Login.ToLower().Contains(prefixText.ToLower())) ||
                              (a.Name.ToLower().Contains(prefixText.ToLower())) ||
                              (a.EmployeeID.StartsWith(prefixText))
                        select a;

            List<string> list = new List<string>();
            foreach (AdminLogins admin in query)
                list.Add(admin.Name + " (" + admin.EmployeeID + ") - " + admin.Login.ToUpper());

            return list.ToArray();
        }
        catch (Exception ex)
        {
            if (_DebugMode)
                return new string[] { "<span style=\"font-color: red\">Could not return values: " + ex.Message + "</span>" };
            else
                return new string[0];
        }
    }


    #endregion

}
