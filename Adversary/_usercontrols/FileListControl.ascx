﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FileListControl.ascx.cs" Inherits="FileListControl" %>


<asp:UpdatePanel ID="upFileList" runat="server">
    <ContentTemplate>
    
        <script type="text/javascript">
        
        var frameRefreshCounter = 0;
        
        function keepAlive()
        {
            $get('<%=btnUpload.ClientID%>').disabled = true;
            $get('divUploadStatus').style.visibility = 'visible';
            
            if (frameRefreshCounter >= 5)
            {
                frameRefreshCounter = 0;
                $get('refreshFrame').contentWindow.location.reload();
            }
            else
                frameRefreshCounter++;
            
            var lbl = $get('divUploadStatusLbl').innerHTML = "Uploading your file, please wait. Large files may take a while to complete.";
        }
        
        </script>
        
        <asp:Label ID="lblHeader" runat="server" Font-Bold="true" Visible="false"></asp:Label><br />
        <asp:Label ID="lblStatus" runat="server" Font-Bold="true"></asp:Label><br />
        <asp:Repeater ID="repFileList" runat="server"></asp:Repeater>
        <asp:Panel ID="pnlFileListEmpty" runat="server" Visible="false">
            <br />
            <span style="color: Blue; font-weight: bold;">No files have been uploaded</span>
        </asp:Panel>
        <asp:Panel ID="pnlForm" runat="server">
        
            <br />
            <br />
            <table>
                
                <tr>
                    <td>File Description</td>
                    <td><asp:TextBox ID="tbDesc" runat="server" Width="250px" ></asp:TextBox></td>
                </tr>
                
                <asp:Panel ID="pnlUpload" runat="server">
                
                    <tr>
                        <td>File to Upload</td>
                        <td><asp:FileUpload ID="fuFile" runat="server" Width="250px"  /></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: left;">
                            <br />
                            <asp:Button ID="btnUpload" runat="server" Text="Upload File" OnClick="btnUpload_Click" OnClientClick="setInterval('keepAlive()', 5000);" /><br />
                            <br />
                            <div id="divUploadStatus" style="width: 500px; visibility: hidden;">
                                <table>
                                    <tr>
                                        <td>
                                            <img id="imgUploadStatus" src="<%=Request.ApplicationPath%>/_css/loading_bars.gif" />
                                            <iframe id="refreshFrame" src="<%=Request.ApplicationPath%>/_usercontrols/FileListControlFrame.aspx" width="0px" height="0px" frameborder="yes"></iframe>
                                        </td>
                                        <td><div style="color: Blue;" id="divUploadStatusLbl" /></td>        
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                
                </asp:Panel>
                
                <asp:Panel ID="pnlEdit" runat="server" Visible="false">
                
                    
                    <tr>
                        <td>File Name</td>
                        <td><asp:TextBox ID="tbFileName" runat="server" Width="250px" ></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: left;">
                            <br />
                            <asp:Button ID="btnEdit" runat="server" Text="Update" OnClick="btnUpdate_Click" />&nbsp;&nbsp;
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                        </td>
                    </tr>
                    
                </asp:Panel>
                
            </table>
        
        </asp:Panel>
    
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnUpload" />
        <asp:AsyncPostBackTrigger ControlID="btnEdit" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="repFileList" EventName="ItemCommand" />
    </Triggers>
    
</asp:UpdatePanel>
