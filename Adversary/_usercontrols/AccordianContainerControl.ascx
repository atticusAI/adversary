﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AccordianContainerControl.ascx.cs" Inherits="AccordianContainerControl" %>
<%@ Register TagPrefix="UC" TagName="screen2" Src="~/_usercontrols/Screen.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<table>
    <tr>
        <td width="90%">
            <asp:Label ID="lblCaption" runat="server"></asp:Label>
        </td>
        <td>
            <asp:PlaceHolder ID="phToolTip" runat="server"></asp:PlaceHolder>
        </td>
    </tr>
</table>

<asp:RadioButtonList ID="rblList" runat="server"></asp:RadioButtonList>
<ajax:Accordion ID="accordian" runat="server"
                AutoSize="None"
                FadeTransitions=true
                TransitionDuration="220"
                FramesPerSecond="100"
                RequireOpenedPane=true
                SuppressHeaderPostbacks=true>
</ajax:Accordion>

