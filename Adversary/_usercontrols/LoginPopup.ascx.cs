﻿using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using HH;
using HH.Authentication;
using HH.Utils;
using System.Web.Security;
using HH.UI;

public partial class LoginPopup : System.Web.UI.UserControl, ILoginPopup
{
    //private AjaxControlToolkit.ModalPopupExtender _modal;
    private static readonly bool _debugMode = Config.GetDebugStatus();

    public event LoggingInEventHandler LoggingIn;
    public event LoggedInEventHandler LoggedIn;
    public event LoginErrorEventHandler LoginError;

    public UserControl ParentUserControl
    {
        get
        {
            return this;
        }
    }
    public string UserName
    {
        get
        {
            return tbLogin.Text;
        }
        set
        {
            tbLogin.Text = value;
        }
    }
    public string Password
    {
        get
        {
            return tbPwd.Text;
        }
        set
        {
            tbPwd.Text = value;
        }
    }
    public string Domain
    {
        get
        {
            return tbDom.Text;
        }
        set
        {
            tbDom.Text = value;
        }
    }
    public bool SaveLoginChecked
    {
        get
        {
            return cbPersistentLogin.Checked;
        }
        set
        {
            cbPersistentLogin.Checked = value;
        }
    }
    public bool AllowLoginCancel
    {
        get
        {
            return (btnCancel.Style["display"] == "none");
        }
        set
        {
            if (value)
                btnCancel.Style.Remove("display");
            else
                btnCancel.Style.Add("display", "none");
        }
    }
    private LoginAuthenticationMode _authMode;
    public LoginAuthenticationMode AuthenticationMode
    {
        get
        {
            return _authMode;
        }
        set
        {
            _authMode = value;
            if (_authMode == LoginAuthenticationMode.Off)
                pnlLogin.Enabled = false;
            else
                pnlLogin.Enabled = true;
        }
    }
    public string StatusMessage
    {
        get
        {
            return lblStatus.Text;
        }
    }
    public string LoginHeader { get; set; }
    public string ActiveDirectoryServer { get; set; }
    public bool AllowDomainSelect { get; set; }
    public bool AllowSaveLogin { get; set; }
    public Unit Width { get; set; }


    /// <summary>
    /// Modal login form, will also handle basic AD login and user role assignment if configured in Web.config
    /// </summary>
    public LoginPopup()
    {
        LoginHeader = "Please login to access this site";
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        //_modal = UIUtils.BuildModalPopupExtender(ref pnlLogin, Unit.Empty, btnHidden.ID, btnHidden.ID, btnCancel.ID);
        //phAjax.Controls.Add(_modal);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        lblStatus.Text = "<br/>";
        //if (!IsPostBack)
        //    Roles.DeleteCookie();
    }

    protected override void OnPreRender(EventArgs e)
    {
        pnlDomain.Enabled = AllowDomainSelect;
        pnlDomain.Visible = (AllowDomainSelect) || (!AllowDomainSelect && !string.IsNullOrEmpty(Domain));
        trPersistentLogin.Visible = AllowSaveLogin;

        base.OnPreRender(e);
    }

    protected void login_Click(object sender, EventArgs e)
    {
        try
        {
            string login = tbLogin.Text.Trim();
            string password = tbPwd.Text;

            if (LoggingIn != null)
                LoggingIn(this, login, password);

            //if (_debugMode)
            //{
            //    LoggedIn(this, login, password);
            //    return;
            //}

            if (AuthenticationMode == LoginAuthenticationMode.ActiveDirectory)
            {
                if (BasePage._DebugMode)
                {
                }
                else 


                ///TODO: IMPORTANT - REMOVE THIS FOR PRODUCTION!!!!
                //if (BasePage._DebugMode && 
                //    (login.ToUpper().Contains("V_VAHLING") || 
                //     login.ToUpper().Contains("M_ROHLENA") || 
                //     login.ToUpper().Contains("T_PETERSON") ||
                //     login.ToUpper().Contains("B_STEINER")))
                //{
                //    //do nothing
                //}
                //else
                if (!SecurityUtils.AuthenticateAgainstActiveDirectory(ActiveDirectoryServer, login, password, Domain))
                    throw new ValidationException("Your login was invalid");
            }

            if (LoggedIn != null)
                LoggedIn(this, login, password);
            else if (AuthenticationMode != LoginAuthenticationMode.Custom)
            {
                Hide();
                Response.Redirect(Request.RawUrl);
            }
        }
        catch (Exception ex)
        {
            if (ex is ValidationException)
            {
                SetStatusMessage(((ValidationException)ex).GetInvalidMessage(), Color.Red);
            }
            else
            {
                string msg = UIUtils.BuildErrorMessage(ex);

                if (_debugMode)
                    SetStatusMessage(msg, Color.Red);
                else
                    SetStatusMessage("<strong>Sorry, a system error occurred</strong><br/><br/>" + msg, Color.Red);
            }

            if (LoginError != null)
                LoginError(this, lblStatus.Text, ex);


            Page.ClientScript.RegisterClientScriptBlock(
                this.GetType(),
                "changeButton",
                btnLogin.ClientID + ".value = 'Login';",
                true);
        }
        finally
        {
            if (!_debugMode)
            {
                tbLogin.Text = string.Empty;
                tbPwd.Text = string.Empty;
            }
        }
    }

    protected void cancel_Click(object sender, EventArgs e)
    {
        lblStatus.Text = "<br/>";
        tbLogin.Text = string.Empty;
        tbPwd.Text = string.Empty;
        btnLogin.Text = "Login";
        Hide();
    }

    public void Show()
    {
        lblLoginHeader.Text = LoginHeader;
        modalLoginForm.Show();
    }

    public void Hide()
    {
        modalLoginForm.Hide();
    }

    public void SetStatusMessage(string msg, Color color)
    {
        lblStatus.Text = msg;
        lblStatus.ForeColor = color;
    }
}

