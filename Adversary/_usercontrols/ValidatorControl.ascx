﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ValidatorControl.ascx.cs" Inherits="ValidatorControl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<!-- Date Validation Calendar Controls -->
<asp:ImageButton ID="imgCal" runat="server"
                ToolTip = "Click to display calendar"
                ImageUrl = "~/_css/calendar.png"
                style="margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px;"
                Visible="false" />
                
<ajax:CalendarExtender ID="cal" runat="server"                
                PopupButtonID="imgCal"
                Format="d"
                CssClass="popupCalendarExtender"
                Enabled="false"
                TargetControlID="tbHidden" />
                
<asp:TextBox ID="tbHidden" runat="server" Visible="false" />
<!-- End Calendar Controls -->
                
<asp:PlaceHolder ID="phHint" runat="server" Visible="false" />                

<ajax:ValidatorCalloutExtender ID="validatorCallout" runat="server" />