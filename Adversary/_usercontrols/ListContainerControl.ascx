﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ListContainerControl.ascx.cs" Inherits="ListContainerControl" %>
<%@ Register TagPrefix="UC" TagName="screen2" Src="~/_usercontrols/Screen.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register TagPrefix="UC" Assembly="HH" Namespace="HH.UI.ModalPopupForm" %>



<asp:UpdatePanel ID="upPanel" runat="server" UpdateMode="Conditional" RenderMode="Inline" >
    <ContentTemplate>
        <table>
            <tr>
                <td width="90%" style="white-space: nowrap;">
                    <strong><asp:Label ID="lblCaption" runat="server" Font-Bold="true"></asp:Label></strong>
                </td>
                <td>
                    <asp:PlaceHolder ID="phToolTip" runat="server"></asp:PlaceHolder>
                </td>
            </tr>
        </table>
        <asp:GridView ID="gridView" runat="server" 
                        AutoGenerateColumns="false"
                        GridLines="None"
                        BorderStyle="solid" 
                        
                        BorderWidth="1"
                        BorderColor="#F0F0F0"
                        RowStyle-BackColor="#F0F0F0" 
                        AlternatingRowStyle-BackColor="White"
                        RowStyle-Height="30px"
                        Width="95%" />
        <br />
        <asp:Panel ID="pnlAdd" runat="server"></asp:Panel>
        
        <UC:ModalForm ID="modalForm" runat="server">
            <asp:UpdatePanel ID="pnlForm" runat="server">
                
                <ContentTemplate>
                
                    <asp:Label ID="lblForm" runat="server" Font-Bold="true"></asp:Label><br />
                    <br />
                    <UC:screen2 ID="listContainerScreen" runat="server" />
                    <br />                            
                    <br />
                    <div style="text-align: center;">
                        <asp:LinkButton ID="lbSubmit" runat="server" Text="Submit" CommandName="Create" OnClick="lbSubmit_Click" />&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:LinkButton ID="lbClose" runat="server" Text="Close" CommandName="Cancel" OnClick="lbSubmit_Click" />
                    </div>
                    
                </ContentTemplate>
                
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="gridView" EventName="RowCommand" />
                    <asp:AsyncPostBackTrigger ControlID="lbClose" EventName="Click" />
                </Triggers>
                
            </asp:UpdatePanel>
        </UC:ModalForm>
        
        
    </ContentTemplate>

</asp:UpdatePanel>
