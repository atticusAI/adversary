﻿using System;
using System.Collections;
using System.Reflection;
using System.Web.UI.WebControls;
using HH.Screen;
using HH.Utils;
using System.Collections.Generic;
using System.Web.UI;
using System.Linq;

public partial class ReportBuilder : System.Web.UI.UserControl, IReportBuilder
{
    private SortedList<int, TableRow> _orderedRows;
    private Table _table;

    List<Table> _tables = new List<Table>();
    public List<Table> Tables 
    {
        get
        {
            return _tables;
        }
    }

    public string ScreenID { get; set; }
    public object DataSource { get; set; }

    public string CssClass { get; set; }
    
    public string RowCssClass { get; set; }
    public string RowValueCssClass { get; set; }
    
    public Unit Width { get; set; }
    
    public bool Visible
    {
        get
        {
            //return _report.Visible;
            return pnlReport.Visible;
        }
        set
        {
            //_report.Visible = value;
            pnlReport.Visible = value;
        }
    }

    public bool ShowNullValues { get; set; }

    /// <summary>
    /// Uses reflection to present the specified ScreenBuilder enabled DataSource in a consistent form
    /// </summary>
    public ReportBuilder()
    {
        CssClass = "gridView";
        RowCssClass = "gridViewRow";
        RowValueCssClass = "reportItemValue";
        ShowNullValues = true;
        Width = Unit.Pixel(800);
    }


    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public override void DataBind()
    {
        if (DataSource == null)
            throw new NullReferenceException("Could not perform data bind, the data source is null");
        
        //_report.Rows.Clear();
        pnlReport.Controls.Clear();
        _table = null;
        _tables.Clear();

        if (DataSource is IList)
        {
            IList entities = (IList)DataSource;
            for (int i = 0; i < entities.Count; i++)
            {
                _table = BuildReport();
                _orderedRows = new SortedList<int, TableRow>();
                BuildReport(entities[i], ScreenID);
                AddOrderedRows();
                if (_table.Rows.Count == 0)
                    continue;
                pnlReport.Controls.Add(_table);
                if (!_tables.Contains(_table))
                    _tables.Add(_table);
                //if (i != entities.Count - 1)
                //    pnlReport.Controls.Add(new LiteralControl("<br/>"));
            }
        }
        else
        {
            if (_table == null)
                _table = BuildReport();
            _orderedRows = new SortedList<int, TableRow>();
            BuildReport(DataSource, ScreenID);
            AddOrderedRows();
            //pnlReport.Controls.Add(_table);
            if (!_tables.Contains(_table))
                _tables.Add(_table);
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        int count = _tables.Count;
        for (int i = 0; i < count; i++)
        {
            pnlReport.Controls.Add(_tables[i]);
            if (i != (count - 1))
                pnlReport.Controls.Add(new LiteralControl("<br/>"));
        }

        base.OnPreRender(e);

    }

    private Table BuildReport()
    {
        Table table = new Table();
        table.CssClass = CssClass;
        table.Width = Width;
        return table;
    }

    private void AddOrderedRows()
    {
        foreach (KeyValuePair<int, TableRow> pair in _orderedRows)
        {
            int fieldOrder = pair.Key;
            TableRow tr = pair.Value;

            try
            {
                if (fieldOrder <= _table.Rows.Count)
                    _table.Rows.AddAt(fieldOrder, tr);
                else if (fieldOrder > _table.Rows.Count)
                    _table.Rows.AddAt(_table.Rows.Count, tr);
                else
                    _table.Rows.Add(tr);
            }
            catch
            {
                _table.Rows.Add(tr);
            }
        }
    }

    private void BuildReport(object dataSource, string screenId)
    {
        Type t = dataSource.GetType();
        List<PropertyInfo> properties = t.GetProperties().ToList();
        
        foreach (PropertyInfo info in properties)
        {
            object val = info.GetValue(dataSource, null);
            
            if (!ShowNullValues && val == null)
                continue;

            ScreenBuilder sb = ReflectionUtils.GetAttribute<ScreenBuilder>(info);
            if (sb == null)
                continue;

            switch (sb.BuilderType)
            {
                case BuilderType.AccordianControl:
                    HandleReportForAccordianControl(sb, val, screenId, dataSource);
                    break;
                case BuilderType.EntityControl:
                    HandleReportForEntityControl(sb, val, screenId);
                    break;
                case BuilderType.EntityAssociation:
                    HandleReportForEntityAssociation(sb, val, screenId);
                    break;
                default:
                    HandleReportForStandardControl(sb, val, screenId);
                    break;
            }
        }
    }

    private void HandleReportForStandardControl(ScreenBuilder sb, object val, string screenId)
    {
        if (!IsVisible(sb.ScreenIDs, screenId) &&
            !IsVisible(sb.EntityControlTargetIDs, screenId))
            return;
        if (screenId == null && sb.AccordianTargetIDs != null) //filter out accordian panes for now, they will get added when an accordian control is found
            return;

        AddStandardControl(sb, val);
    }
    private void AddStandardControl(ScreenBuilder sb, object val)
    {
        if (val != null &&
            sb.ListItemText != null)
        {
            //map the value to readable text
            for (int i = 0; i < sb.ListItemValues.Length; i++)
            {
                string listVal = sb.ListItemValues[i];
                if (val.ToString() == listVal)
                {
                    //val = sb.ListItemText[i];
                    val = sb.ListItemText[i].Replace("\r\n", "<br/>");
                    break;
                }
            }
        }

        //_report.Rows.Add(BuildReportRow(sb.Caption, val));
        TableRow tr = BuildReportRow(sb.Caption, val);
        AddRow(tr, sb);
    }

    private void HandleReportForAccordianControl(ScreenBuilder sb, object val, string screenId, object dataSource)
    {
        if (!IsVisible(sb.ScreenIDs, screenId) &&
            !IsVisible(sb.EntityControlTargetIDs, screenId))
            return;

        string rowValue = null;
        if (val != null)
            rowValue = val.ToString();
        if (val != null && sb.PaneItemText != null)
        {
            for (int i = 0; i < sb.PaneItemValues.Length; i++)
            {
                if (sb.PaneItemValues[i] == val.ToString())
                {
                    rowValue = sb.PaneItemText[i];
                    break;
                }
            }
        }

        //_report.Rows.Add(BuildReportRow(sb.Caption, val));
        TableRow tr = BuildReportRow(sb.Caption, rowValue);
        AddRow(tr, sb);
        
        if (val != null)
            HandleAccoridanPanes(val, screenId, sb.AccordianControlID, dataSource);
    }
    private void HandleAccoridanPanes(object val, string screenId, string accordianId, object dataSource)
    {
        Type t;
        if (dataSource is IList && dataSource.GetType().IsGenericType)
            t = dataSource.GetType().GetGenericArguments()[0];
        else
            t = dataSource.GetType();

        foreach (PropertyInfo info in t.GetProperties())
        {
            ScreenBuilder sb = ReflectionUtils.GetAttribute<ScreenBuilder>(info);
            if (sb == null)
                continue;

            if (sb.BuilderType == BuilderType.EntityAssociation)
            {
                if (!IsVisible(sb.ScreenIDs, screenId))
                    continue;

                object ds = info.GetValue(dataSource, null);
                HandleAccoridanPanes(val, screenId, accordianId, ds);
            }
            else
            {
                if (!IsVisible(sb.AccordianTargetIDs, accordianId))
                    continue;
                if (!IsVisible(sb.AccordianPaneIDs, val.ToString()))
                    continue;

                object paneVal = info.GetValue(dataSource, null);
                if (!ShowNullValues && paneVal == null)
                    continue;

                AddStandardControl(sb, paneVal);
            }
        }
    }

    private void HandleReportForEntityControl(ScreenBuilder sb, object val, string screenId)
    {
        const string NO_RECORDS = "<em>No Records Found</em>";

        if (!IsVisible(sb.ScreenIDs, screenId))
            return;

        if (val == null)
        {
            //_report.Rows.Add(BuildReportRow(sb.Caption, NO_RECORDS));
            TableRow tr = BuildReportRow(sb.Caption, NO_RECORDS);
            AddRow(tr, sb);
            return;
        }

        if (!(val is IList))
        {
            //_report.Rows.Add(BuildReportRow(sb.Caption, val));
            TableRow tr = BuildReportRow(sb.Caption, val);
            AddRow(tr, sb);
            return;
        }

        IList entities = (IList)val;
        if (entities.Count == 0)
        {
            //_report.Rows.Add(BuildReportRow(sb.Caption, NO_RECORDS));
            TableRow tr = BuildReportRow(sb.Caption, NO_RECORDS);
            AddRow(tr, sb);
            return;
        }

        for (int i = 0; i < entities.Count; i++)
        {
            BuildReport(entities[i], sb.EntityControlID);
            if (i != entities.Count - 1)
                //_report.Rows.Add(BuildSpacer());
                _table.Rows.Add(BuildSpacer());
        }

    }

    private void HandleReportForEntityAssociation(ScreenBuilder sb, object val, string screenId)
    {
        if (!IsVisible(sb.ScreenIDs, screenId))
            return;

        BuildReport(val, screenId);
    }

    /// <summary>
    /// Builds a two cell row for a report table. Should be used to display the contents of a data object's property.
    /// </summary>
    /// <param name="caption">The caption to show next to the value</param>
    /// <param name="val">The value to display</param>
    /// <param name="rowCssClass">The CSS class to apply to the row</param>
    /// <returns></returns>
    private TableRow BuildReportRow(string caption, object val)
    {
        TableRow tr = new TableRow();
        TableCell td = new TableCell();

        tr.CssClass = RowCssClass;
        td.Text = "&nbsp;" + caption;
        tr.Cells.Add(td);

        td = new TableCell();
        td.CssClass = RowValueCssClass;

        //if a binary control create a checkbox
        if (val is bool || val is bool?)
        {
            bool isChecked = (val != null && Boolean.Parse(val.ToString()));
            td.Text = (isChecked) ? "Yes" : "No";
        }
        else
        {
            //td.Text = (val == null) ? "&nbsp;" : val.ToString();
            //a reimer 08/10/11 - add replace to do line breaks
            td.Text = (val == null) ? "&nbsp;" : val.ToString().Replace("\r\n","<br/>");
        }

        tr.Cells.Add(td);
        return tr;
    }

    private void AddRow(TableRow tr, ScreenBuilder sb)
    {
        //if (sb.FieldOrder >= 0)
        if(sb.SummaryViewFieldOrder >= 0 && sb.SummaryViewFieldOrder < int.MaxValue)
        {
            try
            {
                //int fieldOrder = sb.FieldOrder;
                int fieldOrder = sb.SummaryViewFieldOrder;
                while (_orderedRows.ContainsKey(fieldOrder))
                    fieldOrder++;
                _orderedRows.Add(fieldOrder, tr);
            }
            catch
            {
                _table.Rows.Add(tr);
            }
        }
        else
        {
            if (sb.FieldOrder >= 0)
            {
                int fieldOrder = sb.FieldOrder;
                while (_orderedRows.ContainsKey(fieldOrder))
                    fieldOrder++;
                _orderedRows.Add(fieldOrder, tr);
            }
            else
            {
                _table.Rows.Add(tr);
            }
            
        }
        
    }

    private TableRow BuildSpacer()
    {
        TableRow tr = new TableRow();
        tr.CssClass = RowCssClass;
        TableCell td = new TableCell();
        td.HorizontalAlign = HorizontalAlign.Center;
        td.Text = "<br/>"; //"<hr style='color: #68849F solid 1px; width: 95%; margin: 3px 0px 3px 0px; height: 1px;' />";
        td.ColumnSpan = 2;
        tr.Cells.Add(td);
        return tr;
    }

    private bool IsVisible(string[] screenIds, string screenId)
    {
        if (string.IsNullOrEmpty(screenId))
            return true;

        if (screenIds == null)
            return false;

        foreach (string sid in screenIds)
        {
            if (sid == screenId)
                return true;
        }

        return false;
    }

    public void ClearReport()
    {
        //_report.Rows.Clear();
        pnlReport.Controls.Clear();
    }

    public void AddRow(string caption, object value)
    {
        if (_table == null)
        {
            _table = BuildReport();
            _tables.Add(_table);
        }

        _table.Rows.Add(BuildReportRow(caption, value));

    }

    public void AddRowAt(int index, string caption, object value)
    {
        if (_table == null)
        {
            _table = BuildReport();
            _tables.Add(_table);
        }

        _table.Rows.AddAt(index, BuildReportRow(caption, value));

    }
}
