﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using HH;
using HH.Extensions;
using HH.Screen;
using HH.SQL;
using HH.UI;
using HH.Utils;
using HH.UI.ModalPopupForm;
using System.Data.SqlClient;



public partial class ListContainerControl : System.Web.UI.UserControl, IListContainerControl
{
    public static readonly string CACHE_KEY = "listContainerControlDataSource";

    private bool _eventsSubscribed = false;
    private LinkButton _lbAdd = null;
    private List<LinkButton> _wiredLinks = new List<LinkButton>();

    public event ScreenRowControlCreatingEventHandler ScreenRowControlCreating;
    public event ScreenRowControlCreatedEventHandler ScreenRowControlCreated;
    public event ScreenRowCreatedEventHandler ScreenRowCreated;

    public event GridFieldsAdding FieldsAdding; 
    public event GridViewRowEventHandler RowDataBound;
    public event GridViewRowEventHandler RowCreated;
    public event GridViewCommandEventHandler RowCommand;
    public event EntitySubmitEventHandler OnEntityPreSubmit;
    public event EntitySubmitEventHandler OnEntitySubmitted;
    public event EntitySubmitEventHandler OnEntitySubmitComplete;

    public ListContainerFormMode FormMode { get; set; }
    public IModalForm CustomForm { get; set; }

    public string ConnectionString { get; set; }

    public string EntityID { get; set; }
    public string ScreenID { get; set; }
    public object DataSource { get; set; }

    public string Caption
    {
        get
        {
            return lblCaption.Text;
        }
        set
        {
            lblCaption.Text = value;
        }
    }

    public string AddButtonText { get; set; }

    public bool AutoGenerateColumns { get; set; }
    public bool AllowAdd { get; set; }
    public bool AllowModify { get; set; }
    public bool AllowDelete{ get; set; }
    public bool IsChildOfScreenControl { get; set; }
    public Unit GridWidth
    {
        get
        {
            return gridView.Width;
        }
        set
        {
            gridView.Width = value;
        }
    }
    public Unit FormWidth
    {
        get
        {
            return listContainerScreen.Width;
        }
        set
        {
            listContainerScreen.Width = value;
        }
    }
    public string CssClass
    {
        get
        {
            return listContainerScreen.CssClass;
        }
        set
        {
            listContainerScreen.CssClass = value;
        }
    }
    public string CaptionCssClass
    {
        get
        {
            return listContainerScreen.CaptionCssClass;
        }
        set
        {
            listContainerScreen.CaptionCssClass = value;
        }
    }

    public string InformationToolTip { get; set; }
    
    /// <summary>
    /// Allows for consistent look and feel when presenting lists, a ScreenBuilder enabled control that will automatically handle entity inserts, edits and deletes in the DAL
    /// </summary>
    public ListContainerControl()
    { 
        AllowAdd = true;
        AllowModify = true;
        AllowDelete = true;
        AddButtonText = "Add New Record";
        IsChildOfScreenControl = false;
        AutoGenerateColumns = true;
        FormMode = ListContainerFormMode.AutoGenerate;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (DataSource == null)
            return;

        WireForClickEvent(lbSubmit);
        WireForClickEvent(lbClose);

        if (AllowAdd)
        {
            object entity = ReflectionUtils.CreateNewInstanceOfType(DataSource.GetType());
            ReflectionUtils.SetEntityKey(ref entity, 0);
            _lbAdd = new LinkButton();
            _lbAdd.ID = "lbAdd___" + this.ID;
            _lbAdd.SetDataItem(entity);
            _lbAdd.Font.Bold = true;
            _lbAdd.Text = AddButtonText;
            _lbAdd.Click += new EventHandler(lbRecordCommand_Click);
            _lbAdd.CommandName = EntitySubmitType.Create.ToString();

            pnlAdd.Controls.Add(_lbAdd);

            AsyncPostBackTrigger trigger = new AsyncPostBackTrigger();
            trigger.ControlID = _lbAdd.ID;
            trigger.EventName = "Click";
            upPanel.Triggers.Add(trigger);

            WireForClickEvent(_lbAdd);
        }
        

        if (IsPostBack)
        {
            if (Session[CACHE_KEY] != null && FormMode == ListContainerFormMode.AutoGenerate)
            {
                listContainerScreen.ScreenID = EntityID;//ScreenID;
                listContainerScreen.DataSource = Session[CACHE_KEY];
                listContainerScreen.DataBind();
            }
        }
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        if (!string.IsNullOrEmpty(this.InformationToolTip))
            phToolTip.Controls.Add(new InformationTooltip(this.InformationToolTip));
        HandleRaiseEvent();
    }

    
    /// <summary>
    ///unfortunately we have to handle our own click events if this control is embedded in another user control
    /// </summary>
    private void HandleRaiseEvent()
    {
        if (!IsPostBack)
            return;
        if (!IsChildOfScreenControl)
            return;

        foreach (LinkButton lb in _wiredLinks)
        {
            if (lb != null &&
                Request.Form["__EVENTTARGET"] == lb.UniqueID)
            {
                if (lb == lbSubmit ||
                    lb == lbClose)
                    lbSubmit_Click(lb, new EventArgs());
                else
                    lbRecordCommand_Click(lb, new EventArgs());
                break;
            }
        }
    }

    private void WireForClickEvent(LinkButton lb)
    {
        if (!IsPostBack)
            return;
        if (!IsChildOfScreenControl)
            return;

        _wiredLinks.Add(lb);
    }

    public override void DataBind()
    {
        if (DataSource == null)
            throw new Exception("Could not bind data to the control. No data source specified");

        //base.DataBind();

        if (!_eventsSubscribed)
        {
            if (ScreenRowControlCreating != null)
                listContainerScreen.ScreenRowControlCreating += ScreenRowControlCreating;
            if (ScreenRowControlCreated != null)
                listContainerScreen.ScreenRowControlCreated += ScreenRowControlCreated;
            if (ScreenRowCreated != null)
                listContainerScreen.ScreenRowCreated += ScreenRowCreated;

            if (RowDataBound != null)
                gridView.RowDataBound += RowDataBound;

            gridView.RowCreated += new GridViewRowEventHandler(gridView_RowCreated);
            //if (RowCreated != null)
            //    gridView.RowCreated += RowCreated;
            
            if (RowCommand != null)
                gridView.RowCommand += RowCommand;
            gridView.RowDeleting += new GridViewDeleteEventHandler(gridView_RowDeleting); //not hooking into this event caused JavaScript error
            //gridView.RowDeleted += new GridViewDeletedEventHandler(gridView_RowDeleted);
            _eventsSubscribed = true;
        }

        AddRows();

        gridView.DataSource = DataSource; 
        gridView.DataBind();
    }

    void gridView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //throw new NotImplementedException();
    }


    private void AddRows()
    {
        gridView.Columns.Clear();

        if (AutoGenerateColumns)
        {
            Type t;
            if (DataSource.GetType().IsGenericType)
                t = DataSource.GetType().GetGenericArguments()[0];
            else if (DataSource.GetType().IsArray)
                t = DataSource.GetType().GetElementType();
            else
                t = DataSource.GetType();

            foreach (PropertyInfo info in t.GetProperties())
            {
                ScreenBuilder sb = ReflectionUtils.GetAttribute<ScreenBuilder>(info);
                if (sb == null)
                    continue;

                BoundField bf = new BoundField();
                bf.DataField = info.Name;
                bf.SortExpression = info.Name;
                bf.HeaderText = sb.Caption;
                gridView.Columns.Add(bf);
            }
        }

        if (FieldsAdding != null)
            FieldsAdding(gridView, DataSource);
    }

    private void ShowForm(object entity, EntitySubmitType type, int? index)
    {
        switch (FormMode)
        {
            case ListContainerFormMode.AutoGenerate:

                modalForm.Visible = true;
                lblForm.Text = (type == EntitySubmitType.Create) ? "Add New Record" : "Modify this Record";
                listContainerScreen.ScreenID = EntityID; //ScreenID;
                listContainerScreen.DataSource = entity;
                listContainerScreen.DataBind();
                lbSubmit.CommandName = type.ToString();
                if (index != null)
                    lbSubmit.CommandArgument = index.ToString();

                //AjaxControlToolkit.ModalPopupExtender modal = (AjaxControlToolkit.ModalPopupExtender)phAjaxContainer.Controls[0];
                //modal.Show();
                modalForm.Show();

                Session[CACHE_KEY] = entity;
                
                break;

            case ListContainerFormMode.Custom:

                modalForm.Visible = false;
                CustomForm.Show();
                break;
        }

        
    }

    private void gridView_RowCreated(object sender, GridViewRowEventArgs e)
    {
        TableCell td = new TableCell();

        if (!AllowModify && !AllowDelete)
        {
            if (RowCreated != null)
                RowCreated(sender, e);
            return;
        }

        if (e.Row.RowType != DataControlRowType.DataRow)
        {
            td.Text = "&nbsp;";
            td.Width = Unit.Pixel(150);
            e.Row.Cells.AddAt(0, td);
        }
        else
        {
            if (AllowModify)
            {
                LinkButton lbMod = new LinkButton();
                lbMod.ID = "lbMod___" + e.Row.DataItemIndex;
                lbMod.CommandArgument = e.Row.DataItemIndex.ToString();
                lbMod.SetDataItem(e.Row.DataItem);
                lbMod.Text = "Modify";
                lbMod.Click += new EventHandler(lbRecordCommand_Click);
                lbMod.CommandName = EntitySubmitType.Modify.ToString();
                td.Controls.Add(lbMod);
                td.Controls.Add(new LiteralControl("&nbsp;&nbsp;&nbsp;&nbsp;"));
                WireForClickEvent(lbMod);
            }

            if (AllowDelete)
            {
                LinkButton lbDel = new LinkButton();
                lbDel.ID = "lbDel___" + e.Row.DataItemIndex;
                lbDel.CommandArgument = e.Row.DataItemIndex.ToString();
                lbDel.SetDataItem(e.Row.DataItem);
                lbDel.Text = "Delete";
                lbDel.Attributes.Add("onclick", "return confirm('Are you sure that you want to delete this record?');");
                lbDel.Click += new EventHandler(lbRecordCommand_Click);
                lbDel.CommandName = EntitySubmitType.Delete.ToString();
                td.Controls.Add(lbDel);
                WireForClickEvent(lbDel);
            }
        }
        
        e.Row.Cells.AddAt(0, td);

        if (RowCreated != null)
            RowCreated(sender, e);
    }


    private void lbRecordCommand_Click(object sender, EventArgs e)
    {
        LinkButton lb = (LinkButton)sender;
        EntitySubmitType type = (EntitySubmitType)Enum.Parse(typeof(EntitySubmitType), lb.CommandName, true);
        object entity = lb.GetDataItem();
        int? index = null;
        if (type == EntitySubmitType.Delete || type == EntitySubmitType.Modify)
            index = Int16.Parse(lb.CommandArgument);

        if (OnEntityPreSubmit != null)
            OnEntityPreSubmit(this, DataSource, entity, type);

        switch (type)
        {
            case EntitySubmitType.Create:
                ShowForm(entity, type, index);
                if (FormMode != ListContainerFormMode.AutoGenerate && OnEntitySubmitted != null)
                    OnEntitySubmitted(this, DataSource, entity, type);
                if (FormMode != ListContainerFormMode.AutoGenerate && OnEntitySubmitComplete != null)
                    OnEntitySubmitComplete(this, DataSource, entity, type);
                break;

            case EntitySubmitType.Modify:
                ShowForm(entity, type, index);
                if (FormMode != ListContainerFormMode.AutoGenerate && OnEntitySubmitted != null)
                    OnEntitySubmitted(this, DataSource, entity, type);
                if (FormMode != ListContainerFormMode.AutoGenerate && OnEntitySubmitComplete != null)
                    OnEntitySubmitComplete(this, DataSource, entity, type);
                break;

            case EntitySubmitType.Delete:
                if (OnEntitySubmitted != null)
                    OnEntitySubmitted(this, DataSource, entity, type);
                else
                    HandleDelete(entity, index.Value);

                if (OnEntitySubmitComplete != null)
                    OnEntitySubmitComplete(this, DataSource, entity, type);

                ((Screen)listContainerScreen).ClearForm();
                break;
        }
    }

    protected void lbSubmit_Click(object sender, EventArgs e)
    {
        if (listContainerScreen.DataSource == null)
            return; //data has already been submitted, probably re-submitting old form data

        LinkButton lb = (LinkButton)sender;
        object entity = listContainerScreen.DataSource;
        EntitySubmitType type = (EntitySubmitType)Enum.Parse(typeof(EntitySubmitType), ((LinkButton)sender).CommandName, true);
        int? index = null;
        if (type == EntitySubmitType.Delete || type == EntitySubmitType.Modify)
            index = Int16.Parse(lb.CommandArgument);

        if (type == EntitySubmitType.Cancel)
        {
            if (OnEntitySubmitted != null)
                OnEntitySubmitted(this, DataSource, entity, type);
        }
        else
        {
            if (OnEntitySubmitted != null)
                OnEntitySubmitted(this, DataSource, entity, type);
            else
                HandleSet(entity, type, index);
        }

        Session.Remove(CACHE_KEY);

        if (OnEntitySubmitComplete != null)
            OnEntitySubmitComplete(this, DataSource, entity, type);

        upPanel.Update();
        listContainerScreen.ClearForm();
        modalForm.Hide();
    }

    private void HandleDelete(object entity, int index)
    {
        //((IList)DataSource).Remove(entity);
        ((IList)DataSource).RemoveAt(index);

        if (!string.IsNullOrEmpty(ConnectionString))
        {
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                DatabaseWorker worker = new DatabaseWorker(conn);
                worker.Delete(entity);
                conn.Close();
            }
        }

        DataBind();
        upPanel.Update();
    }

    private void HandleSet(object entity, EntitySubmitType type, int? index)
    {
        if (type == EntitySubmitType.Create)
        {
            ((IList)DataSource).Add(entity);
        }
        else
        {
            //int index = ((IList)DataSource).IndexOf(entity);
            ((IList)DataSource).RemoveAt(index.Value);
            ((IList)DataSource).Insert(index.Value, entity);
        }

        if (!string.IsNullOrEmpty(ConnectionString))
        {

            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                DatabaseWorker worker = new DatabaseWorker(conn);
                worker.Commit(entity);
                conn.Close();
            }
        }

        DataBind();
        upPanel.Update();
    }

    public void Update()
    {
        upPanel.Update();
    }
}
