﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using HH.Screen;
using HH.SQL;
using HH.UI;
using HH.Utils;
using HH;


/// <summary>
/// Dynamically generates a web form if the DataSource specified is ScreenBuilder enabled
/// </summary>
public partial class Screen : System.Web.UI.UserControl, IScreen
{
    private static object _syncRoot = new object();

    private Dictionary<IList, KeyValuePair<PropertyInfo, object>> _entityList = new Dictionary<IList, KeyValuePair<PropertyInfo, object>>();
    private List<IValidatorControl> _validators = new List<IValidatorControl>();
    private SortedList<int, TableRow> _orderedRows;
    private bool _isBound = false;
    private SQLDefaultMapper _mapper = new SQLDefaultMapper();
    
    public event ScreenRowControlCreatingEventHandler ScreenRowControlCreating;
    public event ScreenRowControlCreatedEventHandler ScreenRowControlCreated;
    public event ScreenRowCreatedEventHandler ScreenRowCreated;

    public event GridFieldsAdding FieldsAdding;
    public event GridViewRowEventHandler RowDataBound;
    public event GridViewRowEventHandler RowCreated;
    public event GridViewCommandEventHandler RowCommand;
    public event EntitySubmitEventHandler OnEntityPreSubmit;
    public event EntitySubmitEventHandler OnEntitySubmitted;
    public event EntitySubmitEventHandler OnEntitySubmitComplete;

    public Table Form
    {
        get
        {
            return this._screen;
        }
    }

    public string ConnectionString { get; set; }

    public string ScreenID { get; set; }
    public object DataSource { get; set; }
    
    public string CssClass
    {
        get
        {
            return _screen.CssClass;
        }
        set
        {
            _screen.CssClass = value;
        }
    }
    public string CaptionCssClass { get; set; }
    public bool ShowInformationToolTips { get; set; }
    public Unit Width 
    {
        get
        {
            return _screen.Width;
        }
        set
        {
            _screen.Width = value;
        }
    }
    public bool Visible
    {
        get
        {
            return _screen.Visible;
        }
        set
        {
            _screen.Visible = value;
            upScreen.Update();
        }
    }
    public bool Enabled
    {
        get
        {
            return this.Enabled;
        }
        set
        {
            this.Enabled = value;
        }
    }
    public bool ValidationEnabled { get; set; }

    public bool IsValid
    {
        get
        {
            if (!ValidationEnabled)
                return true;

            bool isValid = true;
            foreach (IValidatorControl validator in _validators)
            {
                if (!validator.IsValid)
                    isValid = false;
            }
            return isValid;
        }
    }

    public UpdatePanelTriggerCollection Triggers
    {
        get
        {
            return this.upScreen.Triggers;
        }
    }

    public Screen()
    {
        ShowInformationToolTips = true;
        ValidationEnabled = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    public override void DataBind()
    {
        if (DataSource == null)
            throw new NullReferenceException("Could not perform data bind, the data source is null");

        _screen.Rows.Clear();
        phValidation.Controls.Clear();
        _validators.Clear();
        _orderedRows = new SortedList<int, TableRow>();
        BuildScreen(DataSource);
        foreach (KeyValuePair<int, TableRow> pair in _orderedRows)
        {
            int fieldOrder = pair.Key;
            TableRow tr = pair.Value;
                
            try
            {
                if (fieldOrder <= _screen.Rows.Count)
                    _screen.Rows.AddAt(fieldOrder, tr);
                else if (fieldOrder > _screen.Rows.Count)
                    _screen.Rows.AddAt(_screen.Rows.Count, tr);
                else
                    _screen.Rows.Add(tr);
            }
            catch
            {
                _screen.Rows.Add(tr);
            }
        }
        upScreen.Update();
        _isBound = true;
    }

    private void BuildScreen(object dataSource)
    {
        string key = ScreenID + "-" + ((dataSource != null) ? dataSource.GetType().Name : string.Empty);
        List<KeyValuePair<ScreenBuilder, PropertyInfo>> screenData;
        if (Cache[key] == null)
            screenData = CacheScreenData(key, dataSource);
        else
            screenData = Cache[key] as List<KeyValuePair<ScreenBuilder, PropertyInfo>>;

        foreach (KeyValuePair<ScreenBuilder, PropertyInfo> data in screenData)
        {
            ScreenBuilder sb = data.Key;
            PropertyInfo info = data.Value;
            switch (sb.BuilderType)
            {
                case BuilderType.EntityAssociation:
                    HandleEntityAssociation(sb, info, dataSource);
                    break;
                case BuilderType.AccordianControl:
                    HandleAccordianControl(sb, info, dataSource);
                    break;
                case BuilderType.EntityControl:
                    HandleEntityControl(sb, info, dataSource);
                    break;
                default:
                    HandleStandardControl(sb, info, dataSource);
                    break;
            }

            
        }
    }

    private List<KeyValuePair<ScreenBuilder, PropertyInfo>> CacheScreenData(string key, object dataSource)
    {
        lock (_syncRoot)
        {
            if (Cache[key] != null)
                return Cache[key] as List<KeyValuePair<ScreenBuilder, PropertyInfo>>;

            List<KeyValuePair<ScreenBuilder, PropertyInfo>> screenData = new List<KeyValuePair<ScreenBuilder, PropertyInfo>>();
            foreach (PropertyInfo info in dataSource.GetType().GetProperties())
            {
                ScreenBuilder sb = ReflectionUtils.GetAttribute<ScreenBuilder>(info);
                if (sb == null)
                    continue;

                switch (sb.BuilderType)
                {
                    case BuilderType.EntityAssociation:
                        if (IsVisible(sb.ScreenIDs))
                            screenData.Add(new KeyValuePair<ScreenBuilder, PropertyInfo>(sb, info));
                        break;
                    case BuilderType.AccordianControl:
                        if (IsVisible(sb.ScreenIDs) || IsVisible(sb.EntityControlTargetIDs))
                            screenData.Add(new KeyValuePair<ScreenBuilder, PropertyInfo>(sb, info));
                        break;
                    case BuilderType.EntityControl:
                        if (IsVisible(sb.ScreenIDs))
                            screenData.Add(new KeyValuePair<ScreenBuilder, PropertyInfo>(sb, info));
                        break;
                    default:
                        if (IsVisible(sb.ScreenIDs) || IsVisible(sb.EntityControlTargetIDs))
                            screenData.Add(new KeyValuePair<ScreenBuilder, PropertyInfo>(sb, info));
                        break;
                }
            }
            Cache[key] = screenData;
            return screenData;
        }
    }

    private void HandleStandardControl(ScreenBuilder sb, PropertyInfo info,object dataSource)
    {
        Control control;
        object value = GetControlValue(info, dataSource, sb);
        if (ScreenRowControlCreating != null)
            control = ScreenRowControlCreating(sb, info, dataSource);
        else
            control = ScreenControlBuilder.Build(sb, info, value, dataSource);

        if (ScreenRowControlCreated != null)
            ScreenRowControlCreated(sb, info, dataSource, value, control);

        AddControlToForm(sb, info, dataSource, value, control);
    }

    private void HandleAccordianControl(ScreenBuilder sb, PropertyInfo info, object dataSource)
    {
        Control control;
        object value = GetControlValue(info, dataSource);
        if (ScreenRowControlCreating != null)
        {
            control = ScreenRowControlCreating(sb, info, dataSource);
        }
        else
        {
            IAccordianContainerControl ac = (IAccordianContainerControl)LoadControl("~/_usercontrols/AccordianContainerControl.ascx");
            ac.ID = sb.AccordianControlID;
            ac.Caption = sb.Caption;
            ac.ScreenID = ScreenID;
            ac.RepeatDirection = sb.ListRepeatDirection;
            ac.Width = this.Width;
            ac.CssClass = CssClass;
            if (!string.IsNullOrEmpty(CaptionCssClass))
                ac.CaptionCssClass = CaptionCssClass;
            if (!string.IsNullOrEmpty(sb.InformationToolTip))
                ac.InformationToolTip = sb.InformationToolTip;

            if (ScreenRowControlCreating != null)
                ac.ScreenRowControlCreating += ScreenRowControlCreating;
            if (ScreenRowControlCreated != null)
                ac.ScreenRowControlCreated += ScreenRowControlCreated;
            if (ScreenRowCreated != null)
                ac.ScreenRowCreated += ScreenRowCreated;

            ac.DataSource = dataSource;
            ac.DataBind();
            control = (Control)ac;
        }

        if (ScreenRowControlCreated != null)
            ScreenRowControlCreated(sb, info, dataSource, value, control);

        AddControlToForm(sb, info, dataSource, value, control);
    }

    private void HandleEntityControl(ScreenBuilder sb, PropertyInfo info, object dataSource)
    {
        object value = info.GetValue(dataSource, null);
        IListContainerControl lc = (IListContainerControl)LoadControl("~/_usercontrols/ListContainerControl.ascx");
        lc.ID = "lc___" + sb.EntityControlID;
        lc.EntityID = sb.EntityControlID;
        //lc.Caption = sb.Caption;
        lc.ScreenID = ScreenID; //sb.EntityControlID;
        lc.IsChildOfScreenControl = true;
        lc.FormWidth = this.Width;
        lc.CssClass = CssClass;
        if (!string.IsNullOrEmpty(CaptionCssClass))
            lc.CaptionCssClass = CaptionCssClass;
        if (!string.IsNullOrEmpty(ConnectionString))
            lc.ConnectionString = ConnectionString;
        if (!string.IsNullOrEmpty(sb.InformationToolTip))
            lc.InformationToolTip = sb.InformationToolTip;

        //wire the events
        lc.OnEntitySubmitComplete += new EntitySubmitEventHandler(lc_OnEntitySubmitComplete);
        if (FieldsAdding != null)
            lc.FieldsAdding += this.FieldsAdding;
        if (OnEntityPreSubmit != null)
            lc.OnEntityPreSubmit += this.OnEntityPreSubmit;
        if (OnEntitySubmitted != null)
            lc.OnEntitySubmitted += this.OnEntitySubmitted;
        if (OnEntitySubmitComplete != null)
            lc.OnEntitySubmitComplete += this.OnEntitySubmitComplete;
        if (RowCommand != null)
            lc.RowCommand += this.RowCommand;
        if (RowCreated != null)
            lc.RowCreated += this.RowCreated;
        if (RowDataBound != null)
            lc.RowDataBound += this.RowDataBound;
        if (ScreenRowControlCreating != null)
            lc.ScreenRowControlCreating += this.ScreenRowControlCreating;
        if (ScreenRowControlCreated != null)
            lc.ScreenRowControlCreated += this.ScreenRowControlCreated;
        if (ScreenRowCreated != null)
            lc.ScreenRowCreated += this.ScreenRowCreated;

        lc.DataSource = value;
        lc.DataBind();

        if (ScreenRowControlCreated != null)
            ScreenRowControlCreated(sb, info, dataSource, value, (Control)lc);

        AddControlToForm(sb, info, dataSource, value, (Control)lc);

        PropertyInfo entityInfo = info;
        if (!string.IsNullOrEmpty(sb.DataTarget))
            entityInfo = dataSource.GetType().GetProperty(sb.DataTarget);
        if (!_entityList.ContainsKey((IList)value))
            _entityList.Add((IList)value, new KeyValuePair<PropertyInfo, object>(entityInfo, dataSource));
    }

    private void HandleEntityAssociation(ScreenBuilder sb, PropertyInfo info, object dataSource)
    {
        object value = info.GetValue(dataSource, null);
        BuildScreen(value);
    }

    private void lc_OnEntitySubmitComplete(IListContainerControl sender, object dataSource, object entity, EntitySubmitType type)
    {
        KeyValuePair<PropertyInfo, object> entityRef = _entityList[(IList)dataSource];
        PropertyInfo entityInfo = entityRef.Key;
        object entityDataSource = entityRef.Value;

        switch (type)
        {
            case EntitySubmitType.Create:
                if (!((IList)entityInfo.GetValue(entityDataSource, null)).Contains(entity))
                    ((IList)entityInfo.GetValue(entityDataSource, null)).Add(entity);
                break;
            case EntitySubmitType.Modify:
                int index = ((IList)entityInfo.GetValue(entityDataSource, null)).IndexOf(entity);
                if (index != -1)
                {
                    ((IList)entityInfo.GetValue(entityDataSource, null)).RemoveAt(index);
                    ((IList)entityInfo.GetValue(entityDataSource, null)).Insert(index, entity);
                }
                break;
            case EntitySubmitType.Delete:
                if (((IList)entityInfo.GetValue(entityDataSource, null)).Contains(entity))
                    ((IList)entityInfo.GetValue(entityDataSource, null)).Remove(entity);
                break;
        }
    }


    private object GetControlValue(PropertyInfo info, object dataSource) { return GetControlValue(info, dataSource, null); }
    private object GetControlValue(PropertyInfo info, object dataSource, ScreenBuilder sb)
    {
        if (!_isBound && IsPostBack)
        {
            foreach (string key in Request.Form.Keys)
            {
                if (key != null &&
                    //key.ToUpper().Contains("$" + info.Name.ToUpper()))
                    key.ToUpper().EndsWith("$" + info.Name.ToUpper()))
                {
                    string formValue = Request.Form[key];
                    if ((info.PropertyType == typeof(bool?) || info.PropertyType == typeof(bool)) &&
                        formValue == "on")
                        formValue = "true";
                    {
                        SQLDefaultMapper mapper = new SQLDefaultMapper();
                        mapper.ConvertSQLValueToProperty(dataSource, info.Name, formValue);
                    }
                    return formValue;
                }
            }

            //checkboxes that are unchecked do not show up in the form postback, check if the control is a CB and return false if yes
            if (sb != null)
            {
                if (sb.BuilderType == BuilderType.CheckBox)
                {
                    _mapper.ConvertSQLValueToProperty(dataSource, info.Name, false);
                    return false;
                }

                Control c = ScreenControlBuilder.Build(sb, info, null, dataSource);
                if (c is CheckBox)
                {
                    _mapper.ConvertSQLValueToProperty(dataSource, info.Name, false);
                    return false;
                }
            }
        }

        object value = _mapper.ConvertPropertyToSQLValue(dataSource, info.Name);
        if (value == DBNull.Value)
            value = null;
        return value;
    }

    public void AddControlToForm(Control control) { AddControlToForm(control, null, null); }
    public void AddControlToForm(Control control, string caption) { AddControlToForm(control, caption, null); }
    public void AddControlToForm(Control control, string caption, string toolTip)
    {
        if (string.IsNullOrEmpty(caption))
            _screen.Rows.Add(BuildOneCellTableRow(control, caption, toolTip));
        else
            _screen.Rows.Add(BuildTwoCellTableRow(control, caption, toolTip));
    }

    private void AddControlToForm(ScreenBuilder sb, PropertyInfo info, object dataSource, object value, Control control)
    {
        if (control == null)
            return;

        TableRow tr;
        bool singleCell = (control is IAccordianContainerControl || control is IListContainerControl);
        if (singleCell)
        {
            string caption = string.Empty;
            CheckRequiredField(sb, ref caption);
            tr = BuildOneCellTableRow(control, caption, sb.InformationToolTip);
        }
        else
        {
            string caption = sb.Caption;
            CheckRequiredField(sb, ref caption);
            tr = BuildTwoCellTableRow(control, caption, sb.InformationToolTip);
        }

        if (ValidationEnabled)
            HandleValidation(sb, info, value, control, tr.Cells[(singleCell) ? 1 : 2]);

        if (sb.FieldOrder >= 0)
        {
            try
            {
                
                int fieldOrder = sb.FieldOrder;
                while (_orderedRows.ContainsKey(fieldOrder))
                    fieldOrder++;
                _orderedRows.Add(fieldOrder, tr);
            }
            catch 
            {
                _screen.Rows.Add(tr);
            }
        }
        else
        {
            _screen.Rows.Add(tr);
        }

        if (ScreenRowCreated != null)
            ScreenRowCreated(info, dataSource, value, tr);
    }
    private void CheckRequiredField(ScreenBuilder sb, ref string caption)
    {
        if (!sb.ValidateAsRequiredField)
            return;

        caption += " <span style='color: red; font-weight: bold; font-size: larger;'>*</span>";
        lblFeedback.Text = "<span style='color: red; font-weight: bold;'>*</span> Indicates Required Field<br/>";
        lblFeedback.Visible = true;
    }

    public void AddControlToFormAt(int index, Control control) { AddControlToFormAt(index, control, null, null); }
    public void AddControlToFormAt(int index, Control control, string caption) { AddControlToFormAt(index, control, caption, null); }
    public void AddControlToFormAt(int index, Control control, string caption, string toolTip)
    {
        if (string.IsNullOrEmpty(caption))
            _screen.Rows.AddAt(index, BuildOneCellTableRow(control, caption, toolTip));
        else
            _screen.Rows.AddAt(index, BuildTwoCellTableRow(control, caption, toolTip));
    }

    private TableRow BuildOneCellTableRow(Control control, string caption, string toolTip)
    {
        TableRow tr = new TableRow();
        TableCell td = new TableCell();
        bool hasToolTip = !string.IsNullOrEmpty(toolTip);

        //if (!string.IsNullOrEmpty(CaptionCssClass))
        //    td.CssClass = CaptionCssClass;

        tr.ID = "tr___" + control.ID;
        //td.Text = caption;
        if (!string.IsNullOrEmpty(caption))
            td.Controls.Add(new LiteralControl(caption + "<br/>"));
        td.ColumnSpan = 2;
        td.Controls.Add(control);
        tr.Cells.Add(td);

        if (ValidationEnabled || (ShowInformationToolTips && hasToolTip))
        {
            td = new TableCell();
            td.VerticalAlign = VerticalAlign.Middle;
            td.HorizontalAlign = HorizontalAlign.Left;
            td.Style.Add("padding-left", "10px");
            if (hasToolTip)
                td.Controls.Add(new InformationTooltip(toolTip)); 
            tr.Cells.Add(td);
        }

        return tr;
    }

    private TableRow BuildTwoCellTableRow(Control control, string caption, string toolTip)
    {
        TableRow tr = new TableRow();
        TableCell td = new TableCell();
        bool hasToolTip = !string.IsNullOrEmpty(toolTip);

        tr.ID = "tr___" + control.ID;
        if (!string.IsNullOrEmpty(CaptionCssClass))
            td.CssClass = CaptionCssClass;
        td.Text = caption;
        td.Width = Unit.Percentage(20);
        td.VerticalAlign = VerticalAlign.Top;
        td.HorizontalAlign = HorizontalAlign.Left;
        tr.Cells.Add(td);

        td = new TableCell();
        td.Width = Unit.Percentage(40);
        td.Controls.Add(control);
        td.VerticalAlign = VerticalAlign.Top;
        td.HorizontalAlign = HorizontalAlign.Left;
        tr.Cells.Add(td);

        if (ValidationEnabled || (ShowInformationToolTips && hasToolTip))
        {
            td = new TableCell();
            td.VerticalAlign = VerticalAlign.Top;
            td.HorizontalAlign = HorizontalAlign.Left;
            td.Style.Add("padding-left", "10px");
            if (hasToolTip)
            {
                InformationTooltip tooltip = new InformationTooltip(toolTip);
                //tooltip.Width = Unit.Percentage(10);
                td.Controls.Add(tooltip); //td.Controls.Add(BuildInformationToolTip(screen, row.Data));
            }
            tr.Cells.Add(td);
        }

        return tr;
    }

    private bool IsVisible(string[] screenIds)
    {
        if (string.IsNullOrEmpty(ScreenID))
            return true;

        if (screenIds == null)
            return false;

        foreach (string screenId in screenIds)
        {
            if (screenId == ScreenID)
                return true;
        }

        return false;
    }

    public void ClearForm()
    {
        _screen.Rows.Clear();
    }

    public void Update()
    {
        upScreen.Update();
    }

    public TableRow FindControlRow(string controlId)
    {
        Control c = FindControl(controlId);
        if (c == null)
            return null;
        return (TableRow)c.Parent.Parent;
    }

    #region VALIDATION METHODS
    public void Validate()
    {
        foreach (IValidatorControl validator in _validators)
            validator.Validate();
    }


    private void HandleValidation(ScreenBuilder sb, PropertyInfo info, object value, Control control, TableCell td)
    {
        if (sb.ValidationType == ValidationType.None)
            return;
        if (control is Label)
            return;

        if (sb.ValidateAsRequiredField)
            AddValidation(sb, control, td, ValidationMode.RequiredField);
        if (sb.ValidateMinLength > 0 || sb.ValidateMaxLength > 1)
            AddValidation(sb, control, td, ValidationMode.Length);

        switch (sb.ValidationType)
        {
            case ValidationType.Auto:
                DetermineValidation(sb, info, value, control, td);
                break;
            case ValidationType.BooleanIsFalse:
                break;
            case ValidationType.BooleanIsTrue:
                break;
            case ValidationType.ValidateAlphaOnly:
                AddValidation(sb, control, td, ValidationMode.Alpha);
                break;
            case ValidationType.ValidateNumericOnly:
                AddValidation(sb, control, td, ValidationMode.Numeric);
                break;
            case ValidationType.ValidateAlphaNumeric:
                AddValidation(sb, control, td, ValidationMode.AlphaNumeric);
                break;
            case ValidationType.Email:
                AddValidation(sb, control, td, ValidationMode.Email);
                break;
            case ValidationType.ValidateDate:
                AddDateValidation(sb, value, control, td);
                break;
        }
    }

    private void DetermineValidation(ScreenBuilder sb, PropertyInfo info, object value, Control control, TableCell td)
    {
        if (info.PropertyType == typeof(DateTime) || (Nullable.GetUnderlyingType(info.PropertyType) == typeof(DateTime)))
            AddDateValidation(sb, value, control, td);

    }

    private void AddValidation(ScreenBuilder sb, Control control, TableCell td, ValidationMode mode)
    {
        IValidatorControl vc = (IValidatorControl)LoadControl("~/_usercontrols/ValidatorControl.ascx");
        vc.ID = "vc" + mode + "___" + control.ID;
        vc.Mode = mode;
        vc.TargetControlID = control.ID;
        vc.ValidatorContainer = phValidation;
        vc.HintsVisible = (mode != ValidationMode.RequiredField) && (mode != ValidationMode.Length);
        if (mode == ValidationMode.Length)
        {
            if (sb.ValidateMinLength > 0)
                vc.MinLength = sb.ValidateMinLength;
            if (sb.ValidateMaxLength > 0)
                vc.MaxLength = sb.ValidateMaxLength;
        }
        td.Controls.AddAt(0, (UserControl)vc);
        _validators.Add(vc);
    }

    private void AddDateValidation(ScreenBuilder sb, object value, Control control, TableCell td)
    {
        IValidatorControl vc = (IValidatorControl)LoadControl("~/_usercontrols/ValidatorControl.ascx");
        vc.ID = "vcDate___" + control.ID;
        vc.Mode = ValidationMode.Date;
        vc.TargetControlID = control.ID;
        vc.MaskVisible = sb.ValidationMaskVisible;
        vc.Mask = sb.ValidationMask;
        vc.CalendarVisible = sb.ValidationCalendarVisible;
        vc.HintsVisible = false;
        if (value != null)
        {
            try
            {
                vc.SelectedDate = DateTime.Parse(value.ToString());
            }
            catch
            {
                vc.SelectedDate = DateTime.Now;
            }
        }
        vc.ValidatorContainer = phValidation;
        td.Controls.AddAt(0, (UserControl)vc);
        _validators.Add(vc);
    }

    

    #endregion
}
