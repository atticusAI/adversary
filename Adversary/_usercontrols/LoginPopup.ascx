﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LoginPopup.ascx.cs" Inherits="LoginPopup" %>
<%@ Register TagPrefix="UC" Assembly="HH" Namespace="HH.UI.ModalPopupForm" %>


    <div class="modalFormDisablePane" />
    

    <UC:ModalForm ID="modalLoginForm" runat="server" Y="100">
    
    
    <asp:Panel ID="pnlLogin" runat="server">
        <br />
        <asp:Label ID="lblLoginHeader" runat="server" Font-Bold="true"></asp:Label><br />
        <br />
        <asp:UpdatePanel ID="upStatus" runat="server" >
            <ContentTemplate>
                <asp:Label ID="lblStatus" runat="server" Font-Bold="true" ForeColor="Red"></asp:Label>
                <br />
                

                <div style="text-align: center; padding-left: 45px; padding-right: 45px;">
                    <table style="text-align: left;">
                        <tr>
                            <td>Login</td>
                            <td><asp:TextBox ID="tbLogin" runat="server" autocomplete="off" Width="200px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>Password</td>
                            <td><asp:TextBox ID="tbPwd" runat="server" TextMode="Password" Width="200px"></asp:TextBox></td>
                        </tr>
                        <asp:Panel ID="pnlDomain" runat="server">
                            <tr>
                                <td>Domain</td>
                                <td><asp:TextBox ID="tbDom" runat="server" autocomplete="off" Width="200px"></asp:TextBox></td>
                            </tr>
                        </asp:Panel>
                        <tr ID="trPersistentLogin" runat="server">
                            <td colspan="2" style="padding-left: 5px; padding-top: 2px;">
                                <asp:CheckBox ID="cbPersistentLogin" runat="server" Text="Remember my login and password?" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" >
                                <br />
                                <div style="text-align: center;">
                                    <asp:Button ID="btnHidden" runat="server" Text="Please wait..." style="display: none;" Enabled="false" Width="120px" />
                                    <asp:Button ID="btnLogin" runat="server" Text="Login" Width="120px" OnClick="login_Click" OnClientClick="doLoginAnimation();" />
                                    <span style="vertical-align: bottom;"><img id="resultsImg" runat="server" alt="loading" src="~/_css/ajax-loader.gif" width="18" height="18" style="visibility: hidden;" /></span>
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="120px" OnClick="cancel_Click" />
                                    
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <br />
                <script type="text/javascript">
                function doLoginAnimation()
                {
                    $get('<%=btnLogin.ClientID%>').style.display = 'none';
                    $get('<%=btnHidden.ClientID%>').style.display = '';
                    $get('<%=resultsImg.ClientID%>').style.visibility = 'visible';
                }
                </script>
                
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnLogin" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>

    </UC:ModalForm>
    
