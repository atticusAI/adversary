﻿using System;
using System.Drawing;
using System.Security.Policy;
using System.Web.UI;
using System.Web.UI.WebControls;
using HH.Extensions;
using HH.UI.FileList;

public partial class FileListControl : System.Web.UI.UserControl, IFileListControl //, IEmbeddedControl
{
    private static readonly string CACHE_KEY = "FileListControlEditObject";
    private bool subscribeToRepeater = true;

    public event FileUploaded OnFileUploaded;
    public event FileEditClick OnFileEditClick;
    public event FileDeleteClick OnFileDeleteClick;
    public event FileEditSubmit OnFileEditSubmit;
    public event RepeaterItemEventHandler FileListItemDataBound;

    public bool AllowFileEdit { get; set; }
    public bool AllowFileDelete { get; set; }
    public bool EnableDescription 
    {
        get
        {
            return tbDesc.Enabled;
        }
        set
        {
            tbDesc.Enabled = value;
        }
    }
        
    public string FileListHeader 
    {
        get
        {
            return lblHeader.Text;
        }
        set
        {
            lblHeader.Visible = !string.IsNullOrEmpty(value);
            if (lblHeader.Visible)
                lblHeader.Text = "<div style=\"margin: 0px 0px 4px 0px;\">" + value + "</div>";
            else
                lblHeader.Text = "<br/><br/>"; //string.Empty;
        }
    }

    public object DataSource
    {
        get
        {
            return repFileList.DataSource;
        }
        set
        {
            repFileList.DataSource = value;
        }
    }

    public Repeater FileList
    {
        get
        {
            return repFileList;
        }
    }

    public string FileDescription
    {
        get
        {
            return tbDesc.Text.Trim();
        }
        set
        {
            tbDesc.Text = value;
        }
    }

    public string FileName
    {
        get
        {
            return tbFileName.Text.Trim();
        }
        set
        {
            tbFileName.Text = value;
        }
    }

    public Uri FileDirectory { get; set; }
    public Uri DownloadPage { get; set; }

    //public UpdatePanel UpdatePanel
    //{
    //    get
    //    {
    //        return null;// upFileListControl;
    //    }
    //}

    private FileListControlMode _mode = FileListControlMode.Upload;
    public FileListControlMode Mode 
    {
        get
        {
            return _mode;
        }
        set
        {
            _mode = value;

            switch (_mode)
            {
                case FileListControlMode.Upload:
                    pnlForm.Visible = true;
                    pnlEdit.Visible = false;
                    pnlUpload.Visible = true;
                    break;
                case FileListControlMode.Edit:
                    pnlForm.Visible = true;
                    pnlEdit.Visible = true;
                    pnlUpload.Visible = false;
                    break;
                case FileListControlMode.FileListOnly:
                    pnlForm.Visible = false;
                    break;
            }
        }
    }

    private class SeperatorTemplate : ITemplate
    {
        #region ITemplate Members

        public void InstantiateIn(Control container)
        {
            container.Controls.Add(new LiteralControl("<br/>"));
        }

        #endregion
    }

    /// <summary>
    /// A glorified file upload textbox
    /// </summary>
    public FileListControl() 
    {
        AllowFileEdit = true;
        AllowFileDelete = true;
    }



    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
            Session.Remove(CACHE_KEY);
    }

    public override void DataBind()
    {
        if (subscribeToRepeater)
        {
            if (repFileList.SeparatorTemplate == null)
                repFileList.SeparatorTemplate = new SeperatorTemplate();
            repFileList.ItemDataBound += new RepeaterItemEventHandler(repFileList_ItemDataBound);
            subscribeToRepeater = false;
        }

        base.DataBind();
        repFileList.DataBind();

        if (repFileList.Items.Count == 0)
        {
            pnlFileListEmpty.Visible = true;
            repFileList.Visible = false;
        }
        else
        {
            pnlFileListEmpty.Visible = false;
            repFileList.Visible = true;
        }
    }

    private void repFileList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        object obj = e.Item.DataItem;
        if (obj == null)
            return;

        if (AllowFileEdit)
        {
            LinkButton btnEdit = new LinkButton();
            btnEdit.ID = "btnEdit___" + e.Item.ClientID;
            btnEdit.SetDataItem(obj);
            btnEdit.Text = "Edit";
            btnEdit.Click += new EventHandler(btnEdit_Click);
            e.Item.Controls.Add(btnEdit);
            e.Item.Controls.Add(new LiteralControl("&nbsp;&nbsp;"));
        }

        if (AllowFileDelete)
        {
            LinkButton btnDel = new LinkButton();
            btnDel.ID = "btnDel___" + e.Item.ClientID;
            btnDel.SetDataItem(obj);
            btnDel.Text = "Delete";
            btnDel.Click += new EventHandler(btnDel_Click);
            btnDel.Attributes.Add("onclick", "return confirm('Are you sure that you want to delete this file?');");
            e.Item.Controls.Add(btnDel);
            e.Item.Controls.Add(new LiteralControl("&nbsp;&nbsp;"));
        }

        if (FileListItemDataBound != null)
        {
            FileListItemDataBound(this, e);
        }
        else if (obj is IFileListDataItem)
        {
            FileListItemControl c = new FileListItemControl((IFileListDataItem)obj);
            c.DownloadPage = DownloadPage;
            c.FileDirectory = FileDirectory;
            e.Item.Controls.Add(c);
        }
    }

    protected void btnEdit_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)sender;
        object obj = btn.GetDataItem();
        Mode = FileListControlMode.Edit;
        Session[CACHE_KEY] = obj;
        ClearStatus();

        bool success = true;
        if (OnFileEditClick != null)
            success = OnFileEditClick(this, obj);

        if (!success)// && lblStatus.Text.Length != 0)
            SetStatusMessage("Unable to edit this file", Color.Red);
    }
    private void btnDel_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)sender;
        object obj = btn.GetDataItem();
        ClearStatus();

        bool success = true;
        if (OnFileDeleteClick != null)
            OnFileDeleteClick(this, obj);

        if (success)
            SetStatusMessage("File delete successful", Color.Blue);
        //else //if (lblStatus.Text == string.Empty)
        //    SetStatusMessage("File delete was not successful", Color.Red);
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        ClearStatus();
        string desc = tbDesc.Text.Trim();
        string fileName = tbFileName.Text.Trim();
        if (!Validate(desc, fileName))
            return;
        
        object obj = Session[CACHE_KEY];
        if (obj == null)
        {
            SetStatusMessage("An internal error occurred, the file could not be updated", Color.Red);
            return;
        }

        bool success = true;
        if (OnFileEditSubmit != null)
            success = OnFileEditSubmit(this, obj, fileName, desc);

        if (success)
        {
            Mode = FileListControlMode.Upload;
            Session.Remove(CACHE_KEY);
            SetStatusMessage("File " + fileName + " update successful", Color.Blue);
            tbDesc.Text = string.Empty;
        }
        //else //if (lblStatus.Text == string.Empty)
        //{
        //    SetStatusMessage("File " + fileName + " was not updated successfully", Color.Red);
        //}
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Mode = FileListControlMode.Upload;
        Session.Remove(CACHE_KEY);
        ClearStatus();
        tbDesc.Text = string.Empty;
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        ClearStatus();
        string desc = tbDesc.Text.Trim();
        string fileName = fuFile.PostedFile.FileName.Trim();
        if (!Validate(desc, fileName))
            return;
        if (fuFile.PostedFile.ContentLength == 0)
        {
            SetStatusMessage("The file you are trying to upload is corrupt or has no data", Color.Red);
            return;
        }

        bool success = true;
        if (OnFileUploaded != null)
            success = OnFileUploaded(this, fuFile, desc);

        if (success)
        {
            SetStatusMessage("File " + fileName + " uploaded successfully", Color.Blue);
            tbDesc.Text = string.Empty;
        }
        //else if (lblStatus.Text == string.Empty)
        //{
        //    SetStatusMessage("File " + fileName + " was not uploaded successfully", Color.Red);
        //}
    }

    private bool Validate(string desc, string fileName)
    {
        if (string.IsNullOrEmpty(desc))
        {
            SetStatusMessage("The file description cannot be blank", Color.Red);
            return false;
        }
        if (string.IsNullOrEmpty(fileName))
        {
            SetStatusMessage("The file name was invalid", Color.Red);
            return false;
        }

        return true;
    }

    public void ClearStatus()
    {
        lblStatus.Text = "<br/>";// string.Empty;
    }

    public void SetStatusMessage(string msg, Color color)
    {
        lblStatus.Text = msg + "<br />";
        lblStatus.ForeColor = color;
    }
}
