﻿using System;
using System.Collections;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using HH.Screen;
using HH.SQL;
using HH.UI;
using HH.Utils;


public enum HeaderControlMode
{
    RadioButtons,
    DropDownList
}

/// <summary>
/// Wraps and extends the functionality of the AjaxControlToolkit.Accordion control, will show/hide sections of a Screen if the DataSource is enabled with ScreenBuilder attributes
/// </summary>
public partial class AccordianContainerControl : System.Web.UI.UserControl, IAccordianContainerControl 
{
    public static readonly string CACHE_KEY = "accordianContainerControlDataSource";

    private bool _eventsSubscribed = false;
    private ScreenBuilder _sb = null;
    private SQLDefaultMapper _mapper = new SQLDefaultMapper();

    public event ScreenRowControlCreatingEventHandler ScreenRowControlCreating;
    public event ScreenRowControlCreatedEventHandler ScreenRowControlCreated;
    public event ScreenRowCreatedEventHandler ScreenRowCreated;

    //public HeaderControlMode HeaderControlMode { get; set; }
    public string ScreenID { get; set; }
    public object DataSource { get; set; }

    public Unit Width 
    {
        get
        {
            return accordian.Width;
        }
        set
        {
            accordian.Width = value;
        }
    }

    public RepeatDirection RepeatDirection { get; set; }

    public AjaxControlToolkit.Accordion AccordianControl
    {
        get
        {
            return accordian;
        }
    }

    public string Caption
    {
        get
        {
            return lblCaption.Text;
        }
        set
        {
            lblCaption.Text = value;
        }
    }

    public string CssClass 
    {
        get
        {
            return accordian.CssClass;
        }
        set
        {
            accordian.CssClass = value;
        }
    }
    public string CaptionCssClass { get; set; }
    public string InformationToolTip { get; set; }

    public AccordianContainerControl()
    {
        this.RepeatDirection = RepeatDirection.Horizontal;
        //this.HeaderControlMode = HeaderControlMode.RadioButtons;
    }

    
    protected void Page_Load(object sender, EventArgs e)
    {
        //rblList.Visible = (HeaderControlMode == HeaderControlMode.RadioButtons);
        //ddlList.Visible = (HeaderControlMode == HeaderControlMode.DropDownList);

        if (DataSource == null)
            return;

        if (IsPostBack &&
            Session[CACHE_KEY] != null)
        {
            DataSource = Session[CACHE_KEY];
            DataBind();
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        rblList.RepeatDirection = this.RepeatDirection;
        if (!string.IsNullOrEmpty(this.InformationToolTip))
            phToolTip.Controls.Add(new InformationTooltip(this.InformationToolTip));
        base.OnPreRender(e);
    }

    public override void DataBind()
    {
        if (DataSource == null)
            throw new Exception("Could not bind data to the control. No data source specified");

        if (string.IsNullOrEmpty(this.ID))
            throw new Exception("Could not bind data. No ID was given to the control.");

        //base.DataBind();

        if (!_eventsSubscribed)
        {
            //if (ScreenRowControlCreating != null)
            //    screen.ScreenRowControlCreating += ScreenRowControlCreating;
            //if (ScreenRowControlCreated != null)
            //    screen.ScreenRowControlCreated += ScreenRowControlCreated;
            //if (ScreenRowCreated != null)
            //    screen.ScreenRowCreated += ScreenRowCreated;

            _eventsSubscribed = true;
        }

        //accordian.ID = "acc___" + this.ID;
        AddPanes(DataSource);
    }

    private void AddPanes(object dataSource)
    {
        Type t;
        if (dataSource is IList)
            t = dataSource.GetType().GetGenericArguments()[0];
        else
            t = dataSource.GetType();

        bool selectedPaneSet = false;
        foreach (PropertyInfo info in t.GetProperties())
        {
            ScreenBuilder sb = ReflectionUtils.GetAttribute<ScreenBuilder>(info);
            if (sb == null)
                continue;
            if (sb.BuilderType != BuilderType.AccordianControl)
                continue;
            if (!IsVisible(sb.ScreenIDs) && !IsVisible(sb.EntityControlTargetIDs))
                continue;
            if (sb.PaneItemValues == null && sb.PaneEnumDataSource == null)
                throw new Exception("Could not bind accordian control for property " + info.Name);
            
            _sb = sb;
            if (string.IsNullOrEmpty(Caption))
                Caption = sb.Caption; 
            object value = GetHeaderControlValue(info, dataSource);
            
            //if the accordian attribute is found process it and determine the panes that need to be added to the accordian control
            if (sb.PaneItemValues != null) //check for values specified in PaneItemValues array
            {
                for (int i = 0; i < sb.PaneItemValues.Length; i++)
                {
                    string paneId = sb.PaneItemValues[i];
                    string text;
                    if (sb.PaneItemText != null)
                        text = sb.PaneItemText[i];
                    else
                        text = paneId;
                    
                    bool selected = (value == null) ? false : (value.ToString().ToLower() == paneId.ToLower());
                    selectedPaneSet = selectedPaneSet || selected;
                    AddPane(paneId, text, selected);
                }
                break;
            }
            else if (sb.PaneEnumDataSource != null) //if that doesn't work see if values are specified in an enum
            {
                string[] paneItemValues = Enum.GetNames(sb.PaneEnumDataSource);
                foreach (string paneId in paneItemValues)
                {
                    bool selected = (value == null) ? false : (value.ToString().ToLower() == paneId.ToLower());
                    selectedPaneSet = selectedPaneSet || selected;
                    AddPane(paneId, paneId, selected);
                }
            }
            break;
        }

        if (accordian.Panes.Count == 0)
            throw new Exception("Could not create accordian panes for control " + this.ID);
        if (!selectedPaneSet)
            SetSelectedPane(0);

        AddControls(dataSource);
    }

    private void AddPane(string paneId, string text, bool selected)
    {
        //add to the header radio button control
        ListItem item = new ListItem(text, paneId);
        item.Selected = selected;
        rblList.Items.Add(item);
        
        //add javascript to toggle panes to the radio button
        int index = rblList.Items.Count - 1;
        rblList.Items[index].Attributes.Add("onclick", "$get('" + accordian.ClientID + "').AccordionBehavior.set_SelectedIndex(" + index + ");");
        
        //create the table that will hold the form controls in the pane
        AjaxControlToolkit.AccordionPane pane = new AjaxControlToolkit.AccordionPane();
        pane.ID = paneId;
        Table table = new Table();
        table.ID = "tbl___" + paneId;
        table.Width = Width;
        table.CssClass = CssClass;
        pane.ContentContainer.Controls.Add(table);
        accordian.Panes.Add(pane);

        //set the pane to open if it is selected
        if (selected)
            SetSelectedPane(index);
    }

    private void AddControls(object dataSource)
    {
        Type t;
        if (dataSource is IList && dataSource.GetType().IsGenericType)
            t = dataSource.GetType().GetGenericArguments()[0];
        else
            t = dataSource.GetType();

        foreach (PropertyInfo info in t.GetProperties())
        {
            ScreenBuilder sb = ReflectionUtils.GetAttribute<ScreenBuilder>(info);
            if (sb == null)
                continue;

            if (sb.BuilderType == BuilderType.EntityAssociation)
            {
                if (!IsVisible(sb.ScreenIDs))
                    continue;

                object value = info.GetValue(dataSource, null);
                AddControls(value);
            }
            else
            {
                if (sb.BuilderType == BuilderType.AccordianControl)
                    continue; //no child accordian controls in an accordian control

                int index = IndexOfId(this.ID, sb.AccordianTargetIDs);
                if (index == -1)
                    continue;

                Control control;
                object value = GetControlValue(info, dataSource);
                if (ScreenRowControlCreating != null)
                    control = ScreenRowControlCreating(sb, info, dataSource);
                else
                    control = ScreenControlBuilder.Build(sb, info, value, dataSource);

                if (ScreenRowControlCreated != null)
                    ScreenRowControlCreated(sb, info, dataSource, value, control);

                AddControlToPane(info, dataSource, value, sb.AccordianPaneIDs[index], sb.Caption, sb.InformationToolTip, control);
            }
        }
    }

    private void AddControlToPane(PropertyInfo info, object dataSource, object value, string paneId, string caption, string informationToolTip, Control control)
    {
        if (control == null)
            return;

        AjaxControlToolkit.AccordionPane pane = accordian.Panes[paneId];
        TableRow tr = BuildTableRow(control, caption, informationToolTip);
        ((Table)pane.ContentContainer.FindControl("tbl___" + paneId)).Rows.Add(tr);
        
        if (ScreenRowCreated != null)
            ScreenRowCreated(info, dataSource, value, tr);
    }

    private TableRow BuildTableRow(Control control, string caption, string toolTip)
    {
        TableRow tr = new TableRow();
        TableCell td = new TableCell();

        if (!string.IsNullOrEmpty(CaptionCssClass))
            td.CssClass = CaptionCssClass;
        td.Text = caption;
        td.Width = Unit.Percentage(20);
        tr.Cells.Add(td);

        td = new TableCell();
        td.Controls.Add(control);
        tr.Cells.Add(td);

        return tr;
    }

    public void SetSelectedPane(int index)
    {
        try
        {
            accordian.SelectedIndex = index;
            rblList.SelectedIndex = index;
        }
        catch { }
    }

    private object GetControlValue(PropertyInfo info, object dataSource)
    {
        if (IsPostBack)
        {
            foreach (string key in Request.Form.Keys)
            {
                if (key != null &&
                    (key.ToUpper().Contains("$" + info.Name.ToUpper()) ||
                     key.ToUpper().Contains(info.Name.ToUpper() + "$")))
                {
                    string value = Request.Form[key];
                    //info.SetValue(dataSource, value, null);
                    _mapper.ConvertSQLValueToProperty(dataSource, info.Name, value);
                    return value;
                }
            }

            return _mapper.ConvertPropertyToSQLValue(dataSource, info.Name);
        }

        return _mapper.ConvertPropertyToSQLValue(dataSource, info.Name);
    }

    private object GetHeaderControlValue(PropertyInfo info, object dataSource)
    {
        if (IsPostBack)
        {
            foreach (string key in Request.Form.Keys)
            {
                if (key != null &&
                    key.Contains("rblList"))
                {
                    string value = Request.Form[key];
                    //info.SetValue(dataSource, value, null);
                    _mapper.ConvertSQLValueToProperty(dataSource, info.Name, value);
                    return value;
                }
            }

            return _mapper.ConvertPropertyToSQLValue(dataSource, info.Name);
        }

        return _mapper.ConvertPropertyToSQLValue(dataSource, info.Name);
    }

    private bool IsVisible(string[] screenIds)
    {
        if (string.IsNullOrEmpty(ScreenID))
            return true;

        if (screenIds == null)
            return false;

        foreach (string screenId in screenIds)
        {
            if (screenId == ScreenID)
                return true;
        }

        return false;
    }

    private int IndexOfId(string primaryId, string[] idList)
    {
        if (string.IsNullOrEmpty(primaryId))
            return -1;

        if (idList == null)
            return -1;

        for (int i=0; i<idList.Length; i++)
        {
            if (idList[i] == primaryId)
                return i;
        }

        return -1;
    }
}