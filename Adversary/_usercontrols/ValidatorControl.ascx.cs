﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using HH.UI;

/// <summary>
/// Handles validation for ScreenBuilder dynamically generated web controls
/// </summary>
public partial class ValidatorControl : System.Web.UI.UserControl, IValidatorControl
{
    public ValidationMode Mode { get; set; }
    public string TargetControlID { get; set; }
    public string ErrorMessage { get; set; }
    public string Hint { get; set; }
    public bool HintsVisible { get; set; }
    public string RegEx { get; set; }
    public bool SetFocusOnError { get; set; }

    public int? MinLength { get; set; }
    public int? MaxLength { get; set; }

    public DateTime? SelectedDate { get; set; }
    public bool CalendarVisible { get; set; }
    public DateTime? MinDate { get; set; }
    public DateTime? MaxDate { get; set; }

    public bool MaskVisible { get; set; }
    public string Mask { get; set; }
    public Control ValidatorContainer { get; set; }

    private IValidator _validator;
    public bool IsValid
    {
        get
        {
            if (_validator == null)
                throw new Exception("Could not validate, validator control has not been created yet.");
            return _validator.IsValid;
        }
    }

    public ValidatorControl()
    {
        Mode = ValidationMode.RequiredField;
        SetFocusOnError = false;
        CalendarVisible = true;
        MinDate = DateTime.MinValue;
        MaxDate = DateTime.MaxValue;
        MaskVisible = false;
        HintsVisible = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.UICulture = "auto";
        Page.Culture = "auto";

        //cal.TargetControlID = TargetControlID;

        switch (Mode)
        {
            case ValidationMode.RequiredField:
                BindRequiredFieldValidation();
                break;

            case ValidationMode.Date:
                BindDateValidation();
                break;

            case ValidationMode.Length:
                if (!MinLength.HasValue && !MaxLength.HasValue)
                    throw new ArgumentException("Length mode selected but a min and/or maximum length was not specified");
                int min = 0;
                int max = 99999;
                if (MinLength.HasValue)
                    min = MinLength.Value;
                if (MaxLength.HasValue)
                    max = MaxLength.Value;
                if (min < 0)
                    throw new ArgumentException("Minimum value must not be less than zero. Value specified was " + min);
                if (max < 1)
                    throw new ArgumentException("Max value must not be less than one. Value specified was " + max);
                if (max < min)
                    throw new ArgumentException("Max value must not be less than minimum");
                RegEx = @"^[\s\S]{" + min + "," + max + "}$";
                string msg = "Value entered for this field must be between " + min + " and " + max + " characters in length";
                if (string.IsNullOrEmpty(ErrorMessage))
                    ErrorMessage = msg;
                if (string.IsNullOrEmpty(Hint))
                    Hint = msg;
                BindRegExValidation();
                break;

            case ValidationMode.Numeric:
                RegEx = @"^(\d|-)?(\d|,)*\.?\d*$"; /*"[0-9]+" [0-9][.]+ */
                if (string.IsNullOrEmpty(ErrorMessage))
                    ErrorMessage = "This field accepts numeric charcters (0-9) only";
                if (string.IsNullOrEmpty(Hint))
                    Hint = "Field excepts number (0-9), negative sign and decimal point only";
                BindRegExValidation();
                break;

            case ValidationMode.Alpha:
                RegEx = "[a-zA-Z]+";
                if (string.IsNullOrEmpty(ErrorMessage))
                    ErrorMessage = "This field accepts alpha characters (A-Z) only";
                if (string.IsNullOrEmpty(Hint))
                    Hint = "Field excepts upper and lower case alphabet characters only";
                BindRegExValidation();
                break;

            case ValidationMode.AlphaNumeric:
                RegEx = "[a-zA-Z0-9]+";
                if (string.IsNullOrEmpty(ErrorMessage))
                    ErrorMessage = "This field accepts alpha numeric characters (A-Z and 0-9) only";
                if (string.IsNullOrEmpty(Hint))
                    Hint = "Field excepts upper and lower case alphabet and numeric (0-9) charcaters only";
                BindRegExValidation();
                break;

            case ValidationMode.Email:
                RegEx = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
                if (string.IsNullOrEmpty(ErrorMessage))
                    ErrorMessage = "This email address is not valid";
                if (string.IsNullOrEmpty(Hint))
                    Hint = "This field excepts valid email addresses only";
                BindRegExValidation();
                break;

            case ValidationMode.RegEx:
                if (string.IsNullOrEmpty(ErrorMessage))
                    ErrorMessage = "This field is not valid";
                BindRegExValidation();
                break;
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        if (SelectedDate != null)
            cal.SelectedDate = SelectedDate;

        base.OnPreRender(e);
    }

    
    private void BindRequiredFieldValidation()
    {
        RequiredFieldValidator v = new RequiredFieldValidator();
        v.ID = "requiredFieldValidator___" + TargetControlID;
        v.ControlToValidate = TargetControlID;
        v.SetFocusOnError = SetFocusOnError;
        if (!string.IsNullOrEmpty(ErrorMessage))
            v.ErrorMessage = ErrorMessage;
        else
            v.ErrorMessage = "This field cannot be left blank or unselected";
        v.Display = ValidatorDisplay.None;
        v.EnableClientScript = true;
        if (string.IsNullOrEmpty(Hint))
            Hint = "Required Field";
        if (HintsVisible)
            AddHint(true);
        AddValidator(v, v.ID);
        cal.Enabled = false;
    }

    private void BindDateValidation()
    {
        const string MSG = "Enter a valid date in the format MM/DD/YYYY";
        imgCal.Visible = CalendarVisible;
        cal.Enabled = CalendarVisible;
        cal.TargetControlID = TargetControlID;
        if (string.IsNullOrEmpty(Hint))
            Hint = MSG;
        if (HintsVisible)
            AddHint(false);

        if (MaskVisible)
        {
            AjaxControlToolkit.MaskedEditExtender mask = new AjaxControlToolkit.MaskedEditExtender();
            mask.ID = "Mask_" + TargetControlID;
            mask.TargetControlID = TargetControlID;
            mask.MaskType = AjaxControlToolkit.MaskedEditType.Date;
            if (Mask != null)
                mask.Mask = Mask;
            else
                mask.Mask = "99/99/9999";
            //mask.AutoComplete = false;
            //mask.AutoCompleteValue = "0";
            mask.AcceptAMPM = false;
            mask.AcceptNegative = AjaxControlToolkit.MaskedEditShowSymbol.None;
            mask.ClipboardEnabled = true;
            this.Controls.Add(mask);
        }

        imgCal.Visible = CalendarVisible;
        cal.Enabled = CalendarVisible;
        cal.TargetControlID = TargetControlID;

        if (MaskVisible)
        {
            AjaxControlToolkit.MaskedEditExtender mask = new AjaxControlToolkit.MaskedEditExtender();
            mask.ID = "Mask_" + TargetControlID;
            mask.TargetControlID = TargetControlID;
            mask.MaskType = AjaxControlToolkit.MaskedEditType.Date;
            if (Mask != null)
                mask.Mask = Mask;
            else
                mask.Mask = "99/99/9999";
            //mask.AutoComplete = false;
            //mask.AutoCompleteValue = "0";
            mask.AcceptAMPM = false;
            mask.AcceptNegative = AjaxControlToolkit.MaskedEditShowSymbol.None;
            mask.ClipboardEnabled = true;
            this.Controls.Add(mask);
        }

        RangeValidator v = new RangeValidator();
        v.Type = ValidationDataType.Date;
        v.ID = "rangeValidatorDate___" + TargetControlID;
        v.ControlToValidate = TargetControlID;
        v.SetFocusOnError = SetFocusOnError;
        if (MinDate.HasValue)
            v.MinimumValue = MinDate.Value.ToShortDateString();
        else
            v.MinimumValue = "01/01/1500";
        if (MaxDate.HasValue)
            v.MaximumValue = MaxDate.Value.ToShortDateString();
        else
            v.MaximumValue = "12/31/2500";
        if (!string.IsNullOrEmpty(ErrorMessage))
            v.ErrorMessage = ErrorMessage;
        else
            v.ErrorMessage = MSG;
        v.Display = ValidatorDisplay.None;
        v.EnableClientScript = true;
        AddValidator(v, v.ID);
    }

    private void BindRegExValidation()
    {
        //add the validation
        RegularExpressionValidator v = new RegularExpressionValidator();
        v.ID = "regExValidator" + Mode + "___" + TargetControlID;
        v.ControlToValidate = TargetControlID;
        v.ValidationExpression = RegEx;
        v.ErrorMessage = ErrorMessage;
        v.Display = ValidatorDisplay.None;
        v.EnableClientScript = true;
        if (HintsVisible && 
            ((Mode != ValidationMode.RegEx) || 
             (Mode == ValidationMode.RegEx && !string.IsNullOrEmpty(Hint))))
            AddHint(false);
        AddValidator(v, v.ID);
        cal.Enabled = false;
        //tbToValidate.CausesValidation = true;
    }

    private void AddValidator(IValidator v, string validatorId)
    {
        //range validator cannot be added to this UserControl
        if (ValidatorContainer != null)
        {
            ValidatorContainer.Controls.Add((Control)v);
        }
        else
        {
            try
            {
                Control c = Parent.FindControl(TargetControlID);
                if (c is UpdatePanel)
                    ((UpdatePanel)c).ContentTemplateContainer.Controls.Add((Control)v);
                else
                    c.Controls.Add((Control)v);
            }
            catch
            {
                throw new Exception("Could not add validator to control tree. Please specify a container with the ValidatorContainer to add a control to");
            }
        }

        validatorCallout.TargetControlID = validatorId;
        _validator = v;
    }

    private void AddHint(bool requiredField)
    {
        //InformationTooltip toolTip = new InformationTooltip(Hint);
        //if (requiredField)
        //    toolTip.ImgSrc = Page.Request.ApplicationPath + "/_css/asterisk.png";
        //phHint.Controls.Add(toolTip);
        //phHint.Visible = true;
    }

    public void Validate()
    {
        if (_validator == null)
            throw new Exception("Could not validate, validator control has not been created yet.");
        _validator.Validate();
    }
}
