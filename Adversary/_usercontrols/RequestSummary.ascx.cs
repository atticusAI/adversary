﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using HH.Screen;
using HH.Utils;

public partial class RequestSummary : System.Web.UI.UserControl, IReportBuilder
{
    public object DataSource { get; set; }

    private List<ReportItem> _ReportItems = new List<ReportItem>();
    private Table _tblSummaryView;
    private List<Table> _tables = new List<Table>();

    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    public RequestSummary()
    {
        CssClass = "gridView";
        RowCssClass = "gridViewRow";
        RowValueCssClass = "reportItemValue";
        ShowNullValues = true;
        Width = Unit.Pixel(800);
    }


    private class ReportItem
    {
        private Int32 _SummaryViewFieldOrder;
        private string _Caption;
        private string _DisplayValue;
        private bool _ValueIsNull = false;
        private bool _IsBoolTypeValue = false;

        public Int32 SummaryViewFieldOrder { get { return _SummaryViewFieldOrder; } set { _SummaryViewFieldOrder = value; } }
        public string Caption { get { return _Caption; } set { _Caption = value; } }
        public string DisplayValue { get { return _DisplayValue; } set { _DisplayValue = value; } }
        public bool ValueIsNull { get { return _ValueIsNull; } set { _ValueIsNull = value; } }
        public bool IsBoolTypeValue { get { return _IsBoolTypeValue; } set { _IsBoolTypeValue = value; } }
    }

    private ReportItem ConvertRequestPropertiesToReportItems(ScreenBuilder sbuilder, object val)
    {
        ReportItem reportItem = new ReportItem();
        reportItem.Caption = sbuilder.Caption;

        if (val == null)
        {
            reportItem.ValueIsNull = true;

            //exception for checkboxes
            if (sbuilder.BuilderType == BuilderType.CheckBox)
            {
                reportItem.DisplayValue = "No";
            }
            else
            {
                reportItem.DisplayValue = string.Empty;
            }
        }
        else
        {
            if (val is bool)
            {
                reportItem.DisplayValue = ((bool)val ? "Yes" : "No");
                reportItem.IsBoolTypeValue = true;
            }
            else if (val is DateTime)
                reportItem.DisplayValue = String.Format("{0:d}", val);
            else
                reportItem.DisplayValue = val.ToString().Replace("\r\n", "<br/>");
        }
        reportItem.SummaryViewFieldOrder = sbuilder.SummaryViewFieldOrder;
        return reportItem;
    }

    public override void DataBind()
    {
        if (DataSource == null)
            throw new NullReferenceException("There was no data to present.");
        else
        {
            //data source not null - it is an adversary request
            //loop through properties, assign them to a ReportItem
            List<PropertyInfo> propertyInfo = DataSource.GetType().GetProperties().ToList();
            foreach (PropertyInfo property in propertyInfo)
            {
                object value = property.GetValue(DataSource, null);
                if (!ShowNullValues && value == null) continue;

                ScreenBuilder sb = ReflectionUtils.GetAttribute<ScreenBuilder>(property);
                if (sb == null) continue;
                if (sb.ScreenIDs == null) continue;
                if (sb.ScreenIDs.Count() <= 0) continue;
                
                
                if (sb.BuilderType.Equals(BuilderType.CheckBox))
                {
                    if (((bool)value).Equals(false)) continue;
                }
                
                
                _ReportItems.Add(ConvertRequestPropertiesToReportItems(sb, value));

            }

        }
        if (_ReportItems.Count > 0)
        {
            _ReportItems.Sort((x, y) => x.SummaryViewFieldOrder.CompareTo(y.SummaryViewFieldOrder));
            BuildReportTable(_ReportItems);
        }

        pnlRequestSummary.Controls.AddAt(0, this._tblSummaryView);
    }

    private void BuildReportTable(List<ReportItem> _ReportItems)
    {
        this._tblSummaryView = new Table();
        _tblSummaryView.CssClass = CssClass;
        _tblSummaryView.Width = Width;

        foreach (ReportItem _reportItem in _ReportItems)
        {
            if (!ShowNullValues && _reportItem.ValueIsNull) continue;

            TableRow tr = new TableRow();
            tr.CssClass = this.RowCssClass;

            TableCell td = new TableCell();
            td.Text = _reportItem.Caption;
            td.CssClass = this.RowValueCssClass;
            tr.Cells.Add(td);

            td = new TableCell();
            td.Text = _reportItem.DisplayValue.ToString();
            td.CssClass = this.RowValueCssClass;
            if (_reportItem.IsBoolTypeValue && _reportItem.DisplayValue.Equals("Yes") && !_reportItem.Caption.ToLower().Contains("none of the above"))
            {
                td.ForeColor = System.Drawing.Color.Red;
            }
            tr.Cells.Add(td);

            _tblSummaryView.Rows.Add(tr);

        }
        
    }

    public List<Table> Tables
    {
        get { return _tables; }
    }

    public string ScreenID
    {
        get;
        set;
    }

    public string CssClass
    {
        get;set;
    }

    public string RowCssClass
    {
        get;set;
    }

    public string RowValueCssClass
    {
        get;set;
    }

    public Unit Width
    {
        get;set;
    }

    public bool ShowNullValues
    {
        get;
        set;
    }

    public void ClearReport()
    {
        throw new NotImplementedException();
    }

    public void AddRow(string caption, object value)
    {
        throw new NotImplementedException();
    }

    public void AddRowAt(int index, string caption, object value)
    {
        throw new NotImplementedException();
    }
}