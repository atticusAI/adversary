﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Screen.ascx.cs" Inherits="Screen" %>


<asp:UpdatePanel ID="upScreen" runat="server" RenderMode="Inline" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:Table ID="_screen" runat="server"></asp:Table>
        <asp:Label ID="lblFeedback" runat="server"></asp:Label>
        <asp:PlaceHolder ID="phValidation" runat="server"></asp:PlaceHolder>
    </ContentTemplate>
</asp:UpdatePanel>

