﻿using System;
using System.Linq;
using System.Collections.Generic;

public partial class Default : BasePage
{
    protected override void OnInit(EventArgs e)
    {
        _MasterPage.SideBarVisible = true;
        if (!string.IsNullOrEmpty(Request["logout"]))
        {
            ClearSession();
            Response.Redirect("Default.aspx");
        }

        if (!DoBaseInit())
            return;
        
        if (_SessionData.LoggedIn)
            _MasterPage.BreadCrumbText = "You are logged in as " + _SessionData.LoginEmpName;
        _MasterPage.HeaderText = "Holland & Hart New Business Intake Website";

        dtApproval.Visible = (_SessionData.AdminLogin.IsAP.HasValue && _SessionData.AdminLogin.IsAP.Value) ||
                             (_SessionData.AdminLogin.IsPGM.HasValue && _SessionData.AdminLogin.IsPGM.Value) ||
                             (_SessionData.AdminLogin.IsAdversary.HasValue && _SessionData.AdminLogin.IsAdversary.Value);

        try
        {
            string userName = _SessionData.AdminLogin.Login;
            List<string> roles = new List<string>();
            var query = from a in AdminLogins.AllAdmins //admins
                        where !string.IsNullOrEmpty(a.Login) && a.Login.ToUpper() == userName.ToUpper()
                        select a;
            if (query.Count() == 1)
            {
                AdminLogins login = query.FirstOrDefault();
                if (login.IsAP.HasValue && login.IsAP.Value)
                    roles.Add("AP");
                if (login.IsPGM.HasValue && login.IsPGM.Value)
                    roles.Add("PGM");
                if (login.IsAdversary.HasValue && login.IsAdversary.Value)
                    roles.Add("Admin");

                _SessionData.AdminLogin = query.FirstOrDefault();
            }


            foreach (string role in roles)
            {
                if (!System.Web.Security.Roles.IsUserInRole(role))
                {
                    System.Web.HttpCookie cookie;
                    try
                    {
                        System.Web.Security.FormsAuthenticationTicket ticket = HH.Authentication.SecurityUtils.GetTicketFromCookie(Request);
                        cookie = HH.Authentication.SecurityUtils.CreateFormsAutheticationCookie(userName, roles.ToArray(), true, ticket.Expiration, (ticket.Expiration > DateTime.Now.AddDays(1)));
                    }
                    catch
                    {
                        cookie = HH.Authentication.SecurityUtils.CreateFormsAutheticationCookie(userName, roles.ToArray(), true);
                    }
                    


                    Response.Cookies.Add(cookie);
                    _SessionData.LoggedIn = true;
                    _MasterPage.TabMenuEnabled = false;
                    _MasterPage.FooterNavVisible = false;
                    _MasterPage.UpperContent.Visible = true;
                    _MasterPage.LowerContent.Visible = true;
                    //_MasterPage.CustomRoleProvider = _SessionData.UserRoles;
                    //_MasterPage.CustomRoleProvider = AdveraryRoles.Instance;
                    _MasterPage.DataBind();
                }
            }
        }
        catch { }
    }
}
