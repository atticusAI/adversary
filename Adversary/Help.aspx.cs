﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using HH.Branding;

public partial class Help : BasePage
{
    protected override void OnInit(EventArgs e)
    {
        if (!DoBaseInit())
            return;

        _MasterPage.MenuMode = MasterPageMenuMode.None;
        _MasterPage.BreadCrumbText = "Main Menu";
        _MasterPage.HeaderText = "Holland & Hart New Business Intake Help Page";
    }
}
