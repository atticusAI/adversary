﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Help.aspx.cs" Inherits="Help" MasterPageFile="~/Master.master" %>

<asp:Content ID="Content" ContentPlaceHolderID="UpperContentPlaceHolder" runat="server">

Submit a new screening memo request, check the latest client matter numbers, or send a new adversary request.<br />
<dl>
    <dt><strong><a href="Screening/Default.aspx">Screening Memo</a></strong></dt>
    <dt><strong><a href="CMList/Default.aspx">New Client Matter List</a></strong></dt>
	<%-- blmcgee@hollandhart.com 11/4/2013 remove all Adversary Request links --%>
	<%--
    <dt><strong><a href="Request/Default.aspx">Adversary Request</a></strong></dt>
	--%>
    <dt><strong><a href="Help.aspx">Help</a></strong></dt>
</dl>

</asp:Content>