﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="AdminProcessing.aspx.cs" Inherits="AdminProcessing" MasterPageFile="~/Master.master" %>
<%@ Register TagPrefix="UC" Assembly="HH" Namespace="HH.UI.ModalPopupForm" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register TagName="Screen" TagPrefix="UC" Src="~/_usercontrols/Screen.ascx" %>
<%@ Register TagPrefix="UC" TagName="ListContainerControl" Src="~/_usercontrols/ListContainerControl.ascx" %>
<%@ Register TagPrefix="UC" TagName="ApprTable" Src="~/Screening/_ApprovalTable.ascx" %>


<asp:Content ID="Content" ContentPlaceHolderID="UpperContentPlaceHolder" runat="server">

<asp:UpdatePanel ID="_upLocking" runat="server" UpdateMode="Conditional" >
    <ContentTemplate>
        <br />
        <asp:Timer ID="_timerLocking" runat="server" OnTick="timer_Tick" Interval="10000" ></asp:Timer>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="_timerLocking" EventName="Tick" />
    </Triggers>
</asp:UpdatePanel>

<asp:UpdatePanel ID="_upMain" runat="server" UpdateMode="Conditional">
<ContentTemplate>
    
    <div style="text-align:left; width: 80%;">
        <table runat="server" id="tableSubmittedOn" style="padding: 0 0 20px 0; text-align: left; width: 100%;" >
            <tr id="_trAR" runat="server" visible="false">
                <td id="_tdAR" runat="server" colspan="2"></td>
            </tr>
            <tr>
                <td colspan="2" style="padding: 0 0 15px 0;">
                    <div style="width: 77%;">
                        <UC:ApprTable ID="_apprTable" runat="server" />
                    </div>
                </td>
            </tr>
            <tr>    
                <td style="font-weight: bold; padding: 5px 0 5px 0;">Confidential Memo?</td>
                <td>
                    <asp:Label ID="_lblConfidential" runat="server" Font-Bold="true" />
                </td>
            </tr>
            <tr>    
                <td style="font-weight: bold; width: 40%; padding: 5px 0 5px 0;">Client</td>
                <td style="width: 60%;">
                    <asp:Label ID="_lblClient" runat="server" Font-Bold="true" />
                </td>
            </tr>
            <tr>    
                <td style="font-weight: bold; padding: 5px 0 5px 0;">Matter Name</td>
                <td>
                    <asp:Label ID="_lblMatter" runat="server" Font-Bold="true" />
                </td>
            </tr>
            <tr>    
                <td style="font-weight: bold; padding: 5px 0 5px 0;">Attorney</td>
                <td>
                    <asp:Label ID="_lblAtty" runat="server" Font-Bold="true" />
                </td>
            </tr>
            <tr>
                <td colspan="2" style=" padding: 5px 0 5px 0;">
                    <div style="font-weight:bold;">Notes:</div><br />
                    <asp:TextBox ID="_tbNotes" runat="server" TextMode=MultiLine CssClass="sbControl" Width="62%" Rows="6" /><br />
                    <br />
                </td>
            </tr>
            <tr>
                <td style=" padding: 5px 0 5px 0;">
                    <strong>Adversary Group Processing - Processed By: </strong>
                </td>
                <td>
                    <ajax:ComboBox ID="_comboAdversary" runat="server"
                                CssClass="CBAquaStyle"
                                AutoPostBack="true"
                                AutoCompleteMode="Suggest"
                                CaseSensitive="false"
                                DropDownStyle="DropDownList"
                                DataTextField="Login"
                                DataValueField="UserID"
                                OnSelectedIndexChanged="combo_SelectedIndexChanged" />
                </td>
            </tr>
            <tr>
                <td style=" padding: 5px 0 5px 0;">
                    <strong>Adversary Approved for Processing</strong>
                </td>
                <td>
                    <asp:Label ID="_lblAdvAdminApproval" runat="server" Font-Bold="true"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" style=" padding: 5px 0 5px 0;">
                <table>
                    <tr>
                        <td style="width: 20%;">Opened</td>
                        <td><asp:TextBox ID="_tbOpened" runat="server" CssClass="sbControl"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>Conflicts</td>
                        <td><asp:TextBox ID="_tbConflicts" runat="server" CssClass="sbControl" TextMode="MultiLine" Rows="4"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>Fee Splits</td>
                        <td><asp:TextBox ID="_tbFeeSplits" runat="server" CssClass="sbControl" TextMode="MultiLine" Rows="4"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>Final Check</td>
                        <td><asp:TextBox ID="_tbFinalCheck" runat="server" CssClass="sbControl" TextMode="MultiLine" Rows="4"></asp:TextBox></td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding-left: 20%; padding-top: 10px; padding-bottom: 25px;">
                    <asp:Button ID="_btnComplete" runat="server" Width="200px" Text="Mark Complete" CommandArgument="Complete" OnClick="btnComplete_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="_lblCMTimeStamp" runat="server" Visible="false" Font-Bold="true"></asp:Label>
                    <UC:ListContainerControl ID="_cmList" runat="server" AllowAdd="false" AllowModify="false" Width="750px" OnOnEntitySubmitComplete="cmList_OnEntitySubmitComplete"></UC:ListContainerControl><br />
                    <asp:LinkButton ID="_lbCMForm" runat="server" Text="Add Client Matter Numbers" Font-Bold="true" Font-Size="10pt" OnClick="lbCMForm_Click"></asp:LinkButton><br />
                    <br />
                    <!-- Bottom form controls -->
                    <a href="../Screening/PrintView.aspx" style="font-weight: bold; font-size: 10pt;" target="_blank">View Memo in Print View</a><br />  
                    <asp:LinkButton ID="_lbSend" runat="server" OnClick="lbSend_Click" CommandName="edit" Font-Bold="true" Font-Size="10pt" OnClientClick="this.innerHTML += '.....(Sending, please wait)';" />
                </td>
            </tr>
        </table>
    </div>
    
    <UC:ModalForm runat="server" ID="_modalCMForm" Width="50%" CancelControlID="_lbCMCancel" >
        <asp:Panel ID="_pnlCMForm" runat="server">
            <strong>Add C/M Number</strong><br />
            <br />
            <asp:Label ID="_lblCMFormError" runat="server" Font-Bold="true" ForeColor="Red"></asp:Label><br />
            <br />
            <table width="100%">
                <tr>
                    <td style="padding: 0 15px 0 0;">Client Matter Number</td>
                    <td><asp:TextBox ID="_tbCM" runat="server" CssClass="sbControl"></asp:TextBox></td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: center;">
                    <br />
                    <br />
                    <asp:LinkButton ID="_lbCMSubmit" runat="server" Text="Submit" Font-Bold="true" OnClick="lbCMSubmit_Click"></asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:LinkButton ID="_lbCMCancel" runat="server" Text="Cancel" Font-Bold="true"></asp:LinkButton>
                    </td>
                </tr>
            </table>
            <br />
        </asp:Panel>
       
    </UC:ModalForm>
    
    
</ContentTemplate>
<Triggers>
    <asp:AsyncPostBackTrigger ControlID="_comboAdversary" EventName="SelectedIndexChanged" />
    <asp:AsyncPostBackTrigger ControlID="_btnComplete" EventName="Click" />
    <asp:AsyncPostBackTrigger ControlID="_lbSend" EventName="Click" />
</Triggers>
</asp:UpdatePanel>

</asp:Content>
