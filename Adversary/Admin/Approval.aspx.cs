﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Net.Mail;
using System.Text;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using HH.Branding;
using HH.Screen;

public partial class Approval : AdminBasePage
{
    private string _office;
    private bool _isRejectedMatter = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        
        _MasterPage.PostBackEvent += new MasterPagePostBackEventHandler(MasterPage_PostBackEvent);
        _MasterPage.PopupYesClick += new EventHandler(MasterPage_PopupConfirm);
        //_MasterPage.PopupDialogText = "Are you sure you would like to set this memo's status to complete and remove it from the queue?";
        //_MasterPage.PopupDialogMode = MasterPagePopupDialogMode.ModalOkCancel;
        
        if (!ValidateMemo())
        {
            tableSubmittedOn.Visible = false;
            pnlAP.Visible = false;
            pnlPGM.Visible = false;
            pnlAdversary.Visible = false;
            return;
        }

        #region THIS NEEDS TO BE MOVED TO THE MEMO VALIDATION METHOD
        string error = null;
        if (string.IsNullOrEmpty(_memo.AttEntering))
            error = "Specify an attorney for this memo";
        else
        {
            CMSData cms = CMSData.GetInstance();
            if (cms.DoEmployeeSearch(_memo.AttEntering).Rows.Count == 0)
                error = "The attorney listed for this memo with employee number " + _memo.AttEntering + " did not resolve to a valid H&H employee.";
        }


        if (string.IsNullOrEmpty(_memo.PersonnelID))
            error = "Specify the person entering this memo";
        else
        {
            if (CMSData.GetInstance().DoEmployeeSearch(_memo.PersonnelID).Rows.Count == 0)
                error = "The person listed in the 'input by' field for this memo with employee number " + _memo.PersonnelID + " did not resolve to a valid H&H employee.";
            else if (error != null)
            {
                try
                {
                    FirmDirectory fd = new FirmDirectory();
                    string email = fd.GetEmployeeEmailAddress(_memo.PersonnelID);
                    error += " Please contact <a href='mailto:" + email + "'>" + email + "</a>";
                }
                catch { }
            }
        }

        if (_memo.ScrMemType != 6)
        {
            if (string.IsNullOrEmpty(_memo.RespAttID))
                error = "Specify a billing attorney for this memo";
            else
            {
                CMSData cms = CMSData.GetInstance();
                if (cms.DoEmployeeSearch(_memo.RespAttID).Rows.Count == 0)
                    error = "The attorney listed for this memo with employee number " + _memo.RespAttID + " did not resolve to a valid H&H employee.";
            }
        }

        if (error != null)
        {
            _MasterPage.FeedbackMessage = error;
            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Warning;
            tableSubmittedOn.Visible = false;
            pnlAP.Visible = false;
            pnlPGM.Visible = false;
            pnlAdversary.Visible = false;
            return;
        }
        #endregion

        BindControls();
        SetControlVisibility();
    }

    
    private void BindControls()
    {
        //bind the AP screen
        scrAP.DataSource = _tracking;
        scrAP.DataBind();

        //bind the PGM screen
        scrPGM.DataSource = _tracking;
        scrPGM.DataBind();

        //bind the Adv screen
        scrAdversary.DataSource = _tracking;
        scrAdversary.DataBind();

        cmList.DataSource = _memo.ClientMatterNumbers;
        cmList.DataBind();

        lblLocked.Visible = cbLocked.Checked;

        lbSend.Text = (_memo.ClientMatterNumbers.Count > 0) ? "Send Client Matter Numbers to Originating Party" : "Send Status Update to Orignating Party";

        _isRejectedMatter = (_memo.ScrMemType == 6);
        _office = _memo.RespAttOfficeName;
        if (!_isRejectedMatter && string.IsNullOrEmpty(_office))
        {
            CMSData cms = CMSData.GetInstance();
            //_office = cms.GetOfficeName(_memo.OrigOffice);
            _office = cms.GetOfficeName(_memo.OrigOffice);

            _MasterPage.FeedbackMessage = "Could not determine office for billing attorney with employee ID " + _memo.RespAttID + ", using originating office of " + _office;
            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Error;
        }
        //TODO: temp fix for Carson City, remove this when multiple APs allowed
        if (!_isRejectedMatter && !string.IsNullOrEmpty(_office) && _office.ToLower().Contains("carson"))
            _office = "Reno";

        if (IsPostBack) //following controls only bound on initial page load
            return;

        //set the A/R Balance if applicable
        if (!string.IsNullOrEmpty(_SessionData.MemoData.ClientNumber))
            phARBalance.Controls.Add(BuildARTable(this.Balance));

        //set the top form controls
        _MasterPage.BreadCrumbText = "Screening Memo Approval > " + _memo.GetMemoTypeDescription() + " (Memo #" + _memo.ScrMemID + ")";
        
        lblClient.Text = GetClientDisplayName(_memo);
        lblMatter.Text = (_memo.ScrMemType == 6) ? "REJECTED MATTER" : _memo.MatterName;
        
        if (_tracking.SubmittedOn.HasValue)
            lblMatter.Text += "&nbsp;&nbsp;&nbsp;&nbsp;( Submitted:&nbsp;&nbsp;" + _tracking.SubmittedOn.Value.ToString() + " )";
        tbNotes.Text = _tracking.Notes;

        //bind combo boxes
        if (!_isRejectedMatter)
        {
            //AdminLogins[] aps = AdminLogins.GetAPsByOffice(_memo.OrigOffice, false);
            AdminLogins[] aps = AdminLogins.GetAPsByOfficeName(_office, false);
            BindApproverComboBox(comboAP, aps, _tracking.APUserID, ApproverComboBoxType.AP, _memo);
            AdminLogins[] pgms = AdminLogins.GetPGMsByPracticeCode(_memo.PracCode, false);
            BindApproverComboBox(comboPGM, pgms, _tracking.PGMUserID, ApproverComboBoxType.PGL, _memo);
        }
        AdminLogins[] advs = AdminLogins.GetAdversaryGroup();
        BindApproverComboBox(comboAdversary, advs, _tracking.AdversaryUserID, ApproverComboBoxType.Adv, _memo);

        //set the retainer checkboxes
        lblAPRetainerAcknowledged.Text = (!string.IsNullOrEmpty(_memo.RetainerDescYes)) ? "Approval for retainer less than $5000" : "Approval for no retainer fee";
        cbAPRetainerAcknowledged.Checked = _tracking.RetainerAcknowledged.HasValue && _tracking.RetainerAcknowledged.Value;
        lblPGMRetainerAcknowledged.Text = lblAPRetainerAcknowledged.Text;
        cbPGMRetainerAcknowledged.Checked = _tracking.RetainerAcknowledged.HasValue && _tracking.RetainerAcknowledged.Value;

        //set AP, PGM and Adv status labels
        SetIndividualStatuses();

        //bind the Adv Admin controls
        //if (!string.IsNullOrEmpty(_tracking.AdversaryAdminSignature))
        //    lblAdvAdminApproval.Text = _tracking.AdversaryAdminSignature + " (" + _tracking.AdversaryAdminDate + ")";
        cbARApproval.Checked = _tracking.ARApprovalReceived.HasValue && _tracking.ARApprovalReceived.Value;
        
        //bind status drop down
        ddlStatus.DataSource = Tracking.StatusTypes;
        ddlStatus.DataTextField = "Status";
        ddlStatus.DataValueField = "StatusTypeID";
        ddlStatus.DataBind();
        SetSelectedStatus();
        
        //bind memu controls
        cbRejectionHold.Checked = _tracking.RejectionHold.HasValue && _tracking.RejectionHold.Value;
        cbLocked.Checked = _tracking.Locked.HasValue && _tracking.Locked.Value;
        lblLocked.Visible = cbLocked.Checked;

        //bind controls for rejection form
        lblAPRejection.Text = _tracking.APRejectionReason;
        tbAPRejectionReason.Text = _tracking.APRejectionReason;
        lblPGMRejection.Text = _tracking.PGMRejectionReason;
        tbPGMRejectionReason.Text = _tracking.PGMRejectionReason;
        lblAdvRejection.Text = _tracking.AdversaryRejectionReason;
        tbAdvRejectionReason.Text = _tracking.AdversaryRejectionReason;
        cbModalRejectionHold.Checked = cbRejectionHold.Checked;
        
        //bind controls for A/R form
        if (this.Balance != null)
        {
            lblFrom.Text = _SessionData.LoginEmpEmail; //_login.Email;
            tbTo.Text = string.Empty;
            List<ARApprovalUser> recipients = ARApprovalUser.GetAllARApprovalRecipients();
            foreach (ARApprovalUser recipient in recipients)
                tbTo.Text += recipient.EmailAddress + "; ";
            tbSubject.Text = "A/R Approval Required";

            StringBuilder sb = new StringBuilder();
            sb.AppendLine();
            sb.AppendLine();
            sb.AppendLine("Additional approval required for client with A/R past 90 days:");
            sb.AppendLine();
            sb.AppendLine("Client: " + _memo.ClientName);
            sb.AppendLine();
            sb.AppendLine("Current:            $" + Balance.Current.ToString("0.00"));
            sb.AppendLine("31  to 60 Days:     $" + Balance.Days_31_to_60.ToString("0.00"));
            sb.AppendLine("61 to 90 Days:      $" + Balance.Days_61_to_90.ToString("0.00"));
            sb.AppendLine("91 to 120 Days:     $" + Balance.Days_91_to_120.ToString("0.00"));
            sb.AppendLine("Over 121 Days:      $" + Balance.Days_121_Plus.ToString("0.00"));
            sb.AppendLine("Total A/R:          $" + Balance.TotalAR.ToString("0.00"));
            sb.AppendLine();
            if (tbNotes.Text.Trim() != string.Empty)
            {
                sb.AppendLine("Notes:");
                sb.AppendLine();
                sb.AppendLine(tbNotes.Text);
                sb.AppendLine();
            }
            tbARApprovalEmail.Text = sb.ToString();
        }
    }

    private void SetIndividualStatuses()
    {
        string text;
        Color color;

        text = "No response by AP";
        color = Color.Orange;
        btnAPApprove.Text = "Approve";
        btnAPApprove.CommandName = "add";
        btnAPApprove.OnClientClick = "this.value='Approving, please wait';";
        if (_tracking.APApproved.HasValue && _tracking.APApproved.Value)
        {
            text = "Approved by AP";
            color = Color.Green;
            btnAPApprove.Text = "Remove AP Approval";
            btnAPApprove.CommandName = "remove";
            btnAPApprove.OnClientClick = "this.value='Removing approval, please wait';";
        }
        else if (_tracking.APAcknowledged.HasValue && _tracking.APAcknowledged.Value)
        {
            text = "Acknowledged by AP";
            color = Color.Blue;
        }
        lblAPStatus.Text = text;
        lblAPStatus.ForeColor = color;

        text = "No response by PGL";
        color = Color.Orange;
        btnPGMApprove.Text = "Approve";
        btnPGMApprove.CommandName = "add";
        btnPGMApprove.OnClientClick = "this.value='Approving, please wait';";
        if (_tracking.PGMApproved.HasValue && _tracking.PGMApproved.Value)
        {
            text = "Approved by PGL";
            color = Color.Green;
            btnPGMApprove.Text = "Remove PGL Approval";
            btnPGMApprove.CommandName = "remove";
            btnPGMApprove.OnClientClick = "this.value='Removing approval, please wait';";
        }
        else if (_tracking.PGMAcknowledged.HasValue && _tracking.PGMAcknowledged.Value)
        {
            text = "Acknowledged by PGL";
            color = Color.Blue;
        }
        lblPGMStatus.Text = text;
        lblPGMStatus.ForeColor = color;

        text = "No response by Adversary Group";
        color = Color.Orange;
        btnAdvAdminApprove.Text = "Approve for Processing";
        btnAdvAdminApprove.CommandName = "add";
        btnAdvAdminApprove.OnClientClick = "this.value='Approving, please wait';";

        if (_memo.Tracking.StatusTypeID == TrackingStatusType.Completed)
        {
            text = "Processing Completed";
            color = Color.Green;
        }
        else if (_memo.Tracking.AdversaryAdminApproved.HasValue && _memo.Tracking.AdversaryAdminApproved.Value)
        {
            if (_memo.Tracking.AdversaryAcknowledged.HasValue && _memo.Tracking.AdversaryAcknowledged.Value)
            {
                text = "Adversary Group Processing";
                color = Color.Blue;
            }
            else
            {
                text = "Approved for Processing";
                color = Color.Green;
            }

            btnAdvAdminApprove.Text = "Remove Adversary Approval";
            btnAdvAdminApprove.CommandName = "remove";
            btnAdvAdminApprove.OnClientClick = "this.value='Removing approval, please wait';";
        }
        else if (_memo.Tracking.AdversaryAdminAcknowledged.HasValue && _memo.Tracking.AdversaryAdminAcknowledged.Value)
        {
            text = "Acknowledged by Adversary";
            color = Color.Blue;
        }

        trAdvAdminApproval.Visible = !string.IsNullOrEmpty(_tracking.AdversaryAdminSignature);
        if (trAdvAdminApproval.Visible)
            lblAdvAdminApproval.Text = _tracking.AdversaryAdminSignature + " (" + _tracking.AdversaryAdminDate + ")";
        lblAdvStatus.Text = text;
        lblAdvStatus.ForeColor = color;
    }

    private void SetSelectedStatus()
    {
        ddlStatus.SelectedValue = ((int)_tracking.StatusTypeID).ToString();
    }

    

    private void SetControlVisibility()
    {
        //set some bools
        bool isSubmittedToAdv = (_tracking.StatusTypeID == TrackingStatusType.AdversaryGroupProcessing) /*||
                                (_tracking.StatusTypeID == TrackingStatusType.AdversaryGroupSubmitted)*/;

        //commented out by A Reimer 03/22/12 for "Rejected to Modification Requested" project
        //bool isComplete = ((_tracking.StatusTypeID == TrackingStatusType.Completed) || 
        //                   ((_tracking.StatusTypeID == TrackingStatusType.Rejected /*|| 
        //                     _tracking.StatusTypeID == TrackingStatusType.AdversaryGroupRejected*/) && 
        //                    (!_tracking.RejectionHold.HasValue || 
        //                    (_tracking.RejectionHold.HasValue && !_tracking.RejectionHold.Value))));

        bool isComplete = ((_tracking.StatusTypeID == TrackingStatusType.Completed) ||
                           (((_tracking.StatusTypeID == TrackingStatusType.Rejected || _tracking.StatusTypeID == TrackingStatusType.ModificationRequested) &&
                            (!_tracking.RejectionHold.HasValue ||
                            (_tracking.RejectionHold.HasValue && !_tracking.RejectionHold.Value)))));



        //TODO: temp fix for Carson City, remove this when multiple APs allowed
        bool isAP = !string.IsNullOrEmpty(_office) && _login.IsAPForOffice(_office); //!string.IsNullOrEmpty(_memo.RespAttOfficeName) && _login.IsAPForOffice(_memo.RespAttOfficeName);
        bool isPGM = !string.IsNullOrEmpty(_memo.PracCode) && _login.IsMemberOfPracticeGroup(_memo.PracCode);
        bool isAdv = _login.IsAdversary.HasValue && _login.IsAdversary.Value;
        bool isAdvAdmin = _login.IsAdversaryAdmin.HasValue && _login.IsAdversaryAdmin.Value;
        
        //set the AP and PGM screen visibility
        pnlAP.Visible = (!_isRejectedMatter && _office != "Denver");
        pnlPGM.Visible = (!_isRejectedMatter);

        //set AP form controls
        if (pnlAP.Visible)
        {
            bool isAPEnabled = !isSubmittedToAdv && !isComplete && (isAP || isAdv);
            comboAP.Enabled = !isComplete && (isAPEnabled || isAdvAdmin);
            trAPRetainerAcknowledged.Visible = RequiresRetainerApproval();
            cbAPRetainerAcknowledged.Enabled = !isComplete && (isAPEnabled || isAdvAdmin);
            btnAPApprove.Visible = !isComplete && (isAPEnabled || isAdvAdmin);
            btnAPReject.Visible = !isComplete && (isAPEnabled || isAdvAdmin);
            SetScreenVisibility(scrAP, isAPEnabled);
        }

        //set PGM form controls
        if (pnlPGM.Visible)
        {
            bool isPGMEnabled = !isSubmittedToAdv && !isComplete && (isPGM || isAdv);
            comboPGM.Enabled = !isComplete && (isPGMEnabled || isAdvAdmin);
            trPGMRetainerAcknowledged.Visible = RequiresRetainerApproval();
            cbPGMRetainerAcknowledged.Enabled = !isComplete && (isPGMEnabled || isAdvAdmin);
            btnPGMApprove.Visible = !isComplete && (isPGMEnabled || isAdvAdmin);
            btnPGMReject.Visible = !isComplete && (isPGMEnabled || isAdvAdmin);
            SetScreenVisibility(scrPGM, isPGMEnabled);
        }

        //set Adversary Admin Controls
        //tbAdversarySignature.Enabled = isAdvAdmin && !isComplete;
        trARApproval.Visible = (_tracking.ARApprovalReceived.HasValue && _tracking.ARApprovalReceived.Value) || RequiresARApproval();
        cbARApproval.Enabled = isAdvAdmin && !isComplete;
        lbAR.Visible = !isComplete && isAdvAdmin;
        btnAdvAdminApprove.Visible = isAdv && (_tracking.StatusTypeID != TrackingStatusType.AdversaryGroupProcessing) && !isComplete;
        btnAdvAdminApprove.Enabled = isAdvAdmin;
        btnAdvAdminReject.Visible = !isComplete && isAdv;
        btnAdvAdminReject.Enabled = isAdvAdmin;

        //set adversary controls
        comboAdversary.Enabled = isAdv && !isComplete;
        SetScreenVisibility(scrAdversary, isAdv && !isComplete);
        lbSend.Visible = isAdv;
        btnComplete.Visible = !btnAdvAdminApprove.Visible && isAdv && !isComplete;

        //set lower menu controls
        //commented out by A Reimer 03/22/12 for "Rejected to Modification Requested" project
        //trRejectionHold.Visible = _tracking.StatusTypeID == TrackingStatusType.Rejected; // ||
        //                          //_tracking.StatusTypeID == TrackingStatusType.AdversaryGroupRejected;
        trRejectionHold.Visible = (_tracking.StatusTypeID == TrackingStatusType.Rejected)
                                    || (_tracking.StatusTypeID == TrackingStatusType.ModificationRequested);

        //set rejection modal form controls
        trAPRejection.Visible = !string.IsNullOrEmpty(_tracking.APRejectionReason);
        pnlAPRejectionReason.Visible = (isAP || !string.IsNullOrEmpty(_tracking.APRejectionReason)) && pnlAP.Visible;
        tbAPRejectionReason.Enabled = isAP;

        trPGMRejection.Visible = !string.IsNullOrEmpty(_tracking.PGMRejectionReason);
        pnlPGMRejectionReason.Visible = (isPGM || !string.IsNullOrEmpty(_tracking.PGMRejectionReason)) && pnlPGM.Visible;
        tbPGMRejectionReason.Enabled = isPGM;

        trAdvRejection.Visible = !string.IsNullOrEmpty(_tracking.AdversaryRejectionReason);
        pnlAdvRejectionReason.Visible = isAdv || !string.IsNullOrEmpty(_tracking.AdversaryRejectionReason);
        tbAdvRejectionReason.Enabled = isAdv;
    }
    private void SetScreenVisibility(Screen screen, bool enabled)
    {
        foreach (TableRow tr in screen.Form.Rows)
            tr.Enabled = enabled;
    }

    private bool RefreshGlobals()
    {
        if (!_SessionData.MemoLoaded)
            return false;

        Tracking tracking = null;

        if (_login.IsAP.HasValue && _login.IsAP.Value)
            tracking = (Tracking)scrAP.DataSource;
        if (_login.IsPGM.HasValue && _login.IsPGM.Value)
            tracking = (Tracking)scrPGM.DataSource;
        if ((_login.IsAdversary.HasValue && _login.IsAdversary.Value) ||
            (_login.IsAdversaryAdmin.HasValue && _login.IsAdversaryAdmin.Value))
            tracking = (Tracking)scrAdversary.DataSource;

        if (tracking == null)
            return false;

        tracking.Notes = tbNotes.Text.Trim();
        tracking.RetainerAcknowledged = cbAPRetainerAcknowledged.Checked || cbPGMRetainerAcknowledged.Checked;
        //tracking.AdversaryAdminSignature = tbAdversarySignature.Text.Trim();
        trARApproval.Visible = !string.IsNullOrEmpty(_tracking.AdversaryAdminSignature);
        if (trARApproval.Visible)
            lblAdvAdminApproval.Text = _tracking.AdversaryAdminSignature + " (" + _tracking.AdversaryAdminDate + ")";
        tracking.ARApprovalReceived = cbARApproval.Checked;
        
        _tracking = tracking;
        _login = _SessionData.AdminLogin;
        _memo = _SessionData.MemoData;
        return true;
    }

    #region EVENT HANDLERS

    void MasterPage_PopupConfirm(object sender, EventArgs e)
    {
        Button button = (Button)sender;
        switch (button.CommandArgument)
        {
            case "complete":
                CompleteMemo();
                Response.Redirect("Queue.aspx");
                break;
            case "redirectToQueue":
                Response.Redirect("Queue.aspx");
                break;
        }
    }

    
    bool MasterPage_PostBackEvent(object sender, MasterPagePostBackEventArgs e)
    {
        try
        {
            if (!RefreshGlobals())
                return true;

            SaveTrackingData(_tracking);
            return true;
        }
        catch (Exception ex)
        {
            _MasterPage.ShowError(ex);
            return false;
        }
    }

    protected void combo_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!RefreshGlobals())
            return;

        ComboBox combo = (ComboBox)sender;

        string msg = string.Empty;
        bool unassigned = combo.SelectedValue == "unassigned";
        int? val = (unassigned) ? null : (int?)Int32.Parse(combo.SelectedValue);

        switch (combo.ID)
        {
            case "comboAP":
                _tracking.APUserID = val;
                _tracking.APAcknowledged = !unassigned;
                if (unassigned)
                    _tracking.APApproved = false;
                msg = (unassigned) ? "Administrative Partner Assignment Removed" : "Assigned to Administrative Partner " + combo.SelectedItem.Text;
                break;
            case "comboPGM":
                _tracking.PGMUserID = val;
                _tracking.PGMAcknowledged = !unassigned;
                if (unassigned)
                    _tracking.PGMApproved = false;
                msg = (unassigned) ? "Practice Group Leader Assignment Removed" : "Assigned to Practice Group Leader " + combo.SelectedItem.Text;
                break;
            case "comboAdversary":
                _tracking.AdversaryUserID = val;
                _tracking.AdversaryAcknowledged = !unassigned;
                _tracking.AdversaryAdminAcknowledged = !unassigned && _login.IsAdversaryAdmin.HasValue && _login.IsAdversaryAdmin.Value;
                msg = (unassigned) ? "Adversary Group Assignment Removed" : "Assigned to Adversary Group Member " + combo.SelectedItem.Text;
                break;
        }

        SaveTrackingData(_tracking);
        SetSelectedStatus();
        SetIndividualStatuses();
        SetControlVisibility();

        _MasterPage.FeedbackMode = MasterPageFeedbackMode.Success;
        _MasterPage.FeedbackMessage = msg;
        upMain.Update();
    }

    #region APPROVE/COMPLETE/REJECT BUTTONS
    protected void btnApproval_Click(object sender, EventArgs e)
    {
        if (!RefreshGlobals())
            return;

        Button btn = (Button)sender;
        bool approved = (btn.CommandName == "add");
 
        //exit the approval process if retainer has not been acknowledged
        if (approved && RequiresRetainerApproval() && (btn.CommandArgument == "AP" || btn.CommandArgument == "PGM") &&
            (!_tracking.RetainerAcknowledged.HasValue || !_tracking.RetainerAcknowledged.Value))
        {
            _MasterPage.ShowPopupDialog(_MasterPage.FeedbackMessage + " You have not approved the retainer for this memo. Please review all retainer information on the memo summary " + 
                                                                      "page and check the " + lblAPRetainerAcknowledged.Text + " box when finished", 
                                                                      MasterPagePopupDialogMode.ModalOK, Unit.Percentage(80), null);
            _MasterPage.PopupYesCommandArgument = "retainerNotice";
            return;
        }

        switch (btn.CommandArgument)
        {
            case "AP":
                if (_tracking.APUserID == null && comboAP.Items.FindByValue(_login.UserID.ToString()) != null)
                {
                    _tracking.APUserID = _login.UserID;
                    comboAP.SelectedValue = _login.UserID.ToString();
                }
                _tracking.APAcknowledged = true;
                _tracking.APApproved = approved;
                _tracking.APDate = (approved) ? DateTime.Now : (DateTime?)null;
                _tracking.APSignature = (approved) ? _login.Login.ToUpper() : null;
                break;

            case "PGM":
                if (_tracking.PGMUserID == null && comboPGM.Items.FindByValue(_login.UserID.ToString()) != null)
                {
                    _tracking.PGMUserID = _login.UserID;
                    comboPGM.SelectedValue = _login.UserID.ToString();
                }
                _tracking.PGMAcknowledged = true;
                _tracking.PGMApproved = approved;
                _tracking.PGMDate = (approved) ? DateTime.Now : (DateTime?)null;
                _tracking.PGMSignature = (approved) ? _login.Login.ToUpper() : null;
                break;

            case "AdvAdmin":
                _tracking.RejectionHold = null;
                _tracking.AdversaryAdminAcknowledged = true;
                _tracking.AdversaryAdminApproved = approved;
                _tracking.AdversaryAdminDate = (approved) ? DateTime.Now : (DateTime?)null;
                _tracking.AdversaryAdminSignature = (approved) ? _login.Login.ToUpper() : null;
                break;
        }

        //bool isDenver = _memo.OrigOffice == "10";
        bool isDenver = _office == "Denver";
        //bool isRejectedMemoType = _memo.ScrMemType == 6;
        bool apApproved = _tracking.APApproved.HasValue && _tracking.APApproved.Value;
        bool pgmApproved = _tracking.PGMApproved.HasValue && _tracking.PGMApproved.Value;
        bool advAdminApproved = _tracking.AdversaryAdminApproved.HasValue && _tracking.AdversaryAdminApproved.Value;

        //set the correct status
        if (approved)
        {
            if (_isRejectedMatter && advAdminApproved)
                _tracking.StatusTypeID = TrackingStatusType.AdversaryGroupProcessing;
            else if (isDenver && pgmApproved && advAdminApproved)
                _tracking.StatusTypeID = TrackingStatusType.AdversaryGroupProcessing;
            else if (!isDenver && apApproved && pgmApproved && advAdminApproved)
                _tracking.StatusTypeID = TrackingStatusType.AdversaryGroupProcessing;
            //else if (isDenver && pgmApproved)
            //    _tracking.StatusTypeID = TrackingStatusType.AdversaryGroupSubmitted;
            //else if (!isDenver && apApproved && pgmApproved)
            //    _tracking.StatusTypeID = TrackingStatusType.AdversaryGroupSubmitted;
        }
        else
        {
            if (_isRejectedMatter)
                _tracking.StatusTypeID = TrackingStatusType.AdversaryGroupProcessing;
            else if (isDenver && !pgmApproved)
                _tracking.StatusTypeID = TrackingStatusType.Submitted;
            else if (!isDenver && (!apApproved || !pgmApproved))
                _tracking.StatusTypeID = TrackingStatusType.Submitted;
            else
                _tracking.StatusTypeID = TrackingStatusType.Submitted; //_tracking.StatusTypeID = TrackingStatusType.AdversaryGroupSubmitted;
        }

        SaveTrackingData(_tracking);
        BindControls();
        SetSelectedStatus();
        SetIndividualStatuses();
        SetControlVisibility();
        

        //send email notifications to adv group as necessary
        bool messageSent = false;
        if (approved &&
            (_tracking.StatusTypeID == TrackingStatusType.AdversaryGroupProcessing))
        {
            try
            {
                //SendTrackingEmail(_memo, true, false);
                SendApproversEmail(_memo);
                messageSent = true;
            }
            catch (Exception ex)
            {
                HandleError(ex);
            }
        }

        _MasterPage.FeedbackMode = (approved) ? MasterPageFeedbackMode.Success : MasterPageFeedbackMode.Warning;
        _MasterPage.FeedbackMessage = "You have " + ((approved) ? "added" : "removed") + " your approval of this memo.";
        if (messageSent)
            _MasterPage.FeedbackMessage += " The adversary group has been notified that this memo is ready for processing.";
        if (approved)
        {
            _MasterPage.ShowPopupDialog(_MasterPage.FeedbackMessage + " Click Continue to be redirected back to your queue.", MasterPagePopupDialogMode.ModalContinueCancel, Unit.Percentage(80), null);
            _MasterPage.PopupYesCommandArgument = "redirectToQueue";
        }
    }

    protected void btnComplete_Click(object sender, EventArgs e)
    {
        _MasterPage.ShowPopupDialog("Are you sure you would like to set this memo's status to complete and remove it from the queue?", MasterPagePopupDialogMode.ModalYesNo);
        _MasterPage.PopupYesCommandArgument = "complete";
    }

    private void CompleteMemo()
    {
        if (!RefreshGlobals())
            return;

        //_tracking.Opened = DateTime.Now;
        _tracking.StatusTypeID = TrackingStatusType.Completed;

        SaveTrackingData(_tracking);
        SetSelectedStatus();
        SetIndividualStatuses();
        SetControlVisibility();
        upMain.Update();

        try
        {
            //SendTrackingEmail(_memo, false, true);

            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Success;
            _MasterPage.FeedbackMessage = "This memo has been marked as completed.";
        }
        catch (Exception ex)
        {
            HandleError(ex);
        }
    }

    protected void btnReject_Click(object sender, EventArgs e)
    {
        cbModalRejectionHold.Checked = true;
        modalRejection.Show();
    }

    protected void cbRetainer_CheckedChanged(object sender, EventArgs e)
    {
        if (!RefreshGlobals())
            return;

        CheckBox cb = (CheckBox)sender;
        _tracking.RetainerAcknowledged = cb.Checked;
        cbAPRetainerAcknowledged.Checked = cb.Checked;
        cbPGMRetainerAcknowledged.Checked = cb.Checked;
        SaveTrackingData(_tracking);

        _MasterPage.FeedbackMode = MasterPageFeedbackMode.Success;
        _MasterPage.FeedbackMessage = "You have " + ((cb.Checked) ? "added approval" : "removed approval") + " for this memo's retainer";
    }
    #endregion

    #region LOWER MENU BUTTONS
    protected void cbLocked_CheckedChanged(object sender, EventArgs e)
    {
        if (!RefreshGlobals())
            return;

        _tracking.Locked = cbLocked.Checked;
        lblLocked.Visible = cbLocked.Checked;

        SaveTrackingData(_tracking);

        _MasterPage.FeedbackMode = MasterPageFeedbackMode.Success;
        _MasterPage.FeedbackMessage = "This memo has been " + ((cbLocked.Checked) ? "Locked" : "Unlocked");
    }

    protected void cbRejectionHold_CheckedChanged(object sender, EventArgs e)
    {
        if (!RefreshGlobals())
            return;

        _tracking.RejectionHold = cbRejectionHold.Checked;
        cbModalRejectionHold.Checked = cbRejectionHold.Checked;

        SaveTrackingData(_tracking);
        SetControlVisibility();

        _MasterPage.FeedbackMode = MasterPageFeedbackMode.Success;
        ///commented by A Reimer 03/21/12 for 'Rejected to Modification Required' change 
        //_MasterPage.FeedbackMessage = "A rejection hold for this memo has been " + ((cbRejectionHold.Checked) ? "applied" : "removed");
        _MasterPage.FeedbackMessage = "A request for modification hold for this memo has been " + ((cbRejectionHold.Checked) ? "applied" : "removed");
    }

    protected void btnStatus_Click(object sender, EventArgs e)
    {
        if (!RefreshGlobals())
            return;

        _tracking.StatusTypeID = (TrackingStatusType)Int32.Parse(ddlStatus.SelectedValue);

        //commented out by A Reimer 03/22/12 for "Rejected to Modification Requested" project
        //if (_tracking.StatusTypeID != TrackingStatusType.Rejected)// &&
        //    //_tracking.StatusTypeID != TrackingStatusType.AdversaryGroupRejected)
        if (_tracking.StatusTypeID != TrackingStatusType.Rejected || _tracking.StatusTypeID != TrackingStatusType.ModificationRequested)
            _tracking.RejectionHold = false;

        SaveTrackingData(_tracking);
        cbRejectionHold.Checked = (_tracking.RejectionHold.HasValue && _tracking.RejectionHold.Value);
        cbModalRejectionHold.Checked = cbRejectionHold.Checked;
        SetIndividualStatuses();
        SetControlVisibility();

        _MasterPage.FeedbackMode = MasterPageFeedbackMode.Success;
        _MasterPage.FeedbackMessage = "Status updated to " + _tracking.GetStatus();
    }

    protected void lbSend_Click(object sender, EventArgs e)
    {
        if (!RefreshGlobals())
            return;
        SaveTrackingData(_tracking);
        //SendStatusUpdate();
        //SendStatusEmail(_memo);
        HandleStatusNotification(_memo);
    }
    #endregion

    #region MODAL FORM BUTTONS


    protected void cmList_OnEntitySubmitComplete(IListContainerControl sender, object dataSource, object entity, EntitySubmitType type)
    {
        if (!RefreshGlobals())
            return;

        HandleCMEntitySubmit((ClientMatterNumber)entity, type);

        BindControls();
        SetSelectedStatus();
        SetIndividualStatuses();
        SetControlVisibility();
        upMain.Update();
    }


    protected void btnRejectionSubmit_Click(object sender, EventArgs e)
    {
        if (!RefreshGlobals())
            return;

        bool hasText = false;
        string apReason = tbAPRejectionReason.Text.Trim();
        string pgmReason = tbPGMRejectionReason.Text.Trim();
        string advReason = tbAdvRejectionReason.Text.Trim();
        StringBuilder reason = new StringBuilder();

        if (!string.IsNullOrEmpty(apReason))
        {
            ///commented by A Reimer 03/21/12 for 'Rejected to Modification Required' change 
            //reason.AppendLine("AP Rejection Reason");
            reason.AppendLine("AP Request for Modification Reason");
            reason.AppendLine();
            reason.AppendLine(apReason);
            reason.AppendLine();
            reason.AppendLine();

            hasText = true;
            ///commented out by A Reimer 03/22/12 for "Rejected to Modification Requested" project
            //tracking.StatusTypeID = TrackingStatusType.Rejected;
            _tracking.StatusTypeID = TrackingStatusType.ModificationRequested;
        }
        if (!string.IsNullOrEmpty(pgmReason))
        {
            ///commented by A Reimer 03/21/12 for 'Rejected to Modification Required' change 
            //reason.AppendLine("PGL Rejection Reason");
            reason.AppendLine("PGL Request for Modification Reason");
            reason.AppendLine();
            reason.AppendLine(pgmReason);
            reason.AppendLine();
            reason.AppendLine();

            hasText = true;
            //commented out by A Reimer 03/22/12 for "Rejected to Modification Requested" project
            //_tracking.StatusTypeID = TrackingStatusType.Rejected;
            _tracking.StatusTypeID = TrackingStatusType.ModificationRequested;
        }
        if (!string.IsNullOrEmpty(advReason))
        {
            ///commented by A Reimer 03/21/12 for 'Rejected to Modification Required' change 
            //reason.AppendLine("Adversary Rejection Reason");
            reason.AppendLine("Adversary Request for Modification Reason");
            reason.AppendLine();
            reason.AppendLine(advReason);
            reason.AppendLine();
            reason.AppendLine();

            hasText = true;
            //_tracking.StatusTypeID = TrackingStatusType.AdversaryGroupRejected; 
            //commented out by A Reimer 03/22/12 for "Rejected to Modification Requested" project
            //_tracking.StatusTypeID = TrackingStatusType.Rejected;
            _tracking.StatusTypeID = TrackingStatusType.ModificationRequested;
        }

        if (!hasText)
        {
            ///commented by A Reimer 03/21/12 for 'Rejected to Modification Required' change 
            //lblRejectionFeedback.Text = "<br/>Could not send this message. A reason for the rejection was not specified.";
            lblRejectionFeedback.Text = "<br/>Could not send this message. A reason for the modification request was not specified.";
            return;
        }

        cbRejectionHold.Checked = cbModalRejectionHold.Checked;
        trRejectionHold.Visible = true;

        _tracking.APRejectionReason = apReason;
        _tracking.PGMRejectionReason = pgmReason;
        _tracking.AdversaryRejectionReason = advReason;
        _tracking.RejectionHold = cbModalRejectionHold.Checked;

        try
        {
            //SendTrackingEmail(_memo, reason.ToString());
            SendStatusEmail(_memo);

            SaveTrackingData(_tracking);
            SetSelectedStatus();
            SetControlVisibility();

            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Success;
            ///commented by A Reimer 03/21/12 for 'Rejected to Modification Required' change
            //_MasterPage.FeedbackMessage = "This memo has been rejected and notification has been sent. Status updated to " + ddlStatus.SelectedItem.Text + ".";
            _MasterPage.FeedbackMessage = "This memo requires modification and notification has been sent. Status updated to " +
                (ddlStatus.SelectedItem.Text.Contains("rejected") ? "Modification Required" : ddlStatus.SelectedItem.Text);
            modalRejection.Hide();
            upMain.Update();
        }
        catch (Exception ex)
        {
            HandleError(ex);
        }
    }

    protected void btnRejectionCancel_Click(object sender, EventArgs e)
    {
        modalRejection.Hide();
    }

    protected void lbAR_Click(object sender, EventArgs e)
    {
        modalAR.Show();
    }

    protected void btnARSubmit_Click(object sender, EventArgs e)
    {
        lblARFeedback.Text = string.Empty;

        string to = tbTo.Text.Trim();
        MailAddress from = new MailAddress(lblFrom.Text);
        string subj = tbSubject.Text.Trim();
        string msg = tbARApprovalEmail.Text.Trim();

        MailAddressCollection toAddresses = new MailAddressCollection();
        string[] recipients = to.Split(new char[] { ';' });
        foreach (string recipient in recipients)
        {
            try
            {
                if (string.IsNullOrEmpty(recipient.Trim()))
                    continue;
                MailAddress addr = new MailAddress(recipient.Trim());
                toAddresses.Add(addr);
            }
            catch
            {
                lblARFeedback.Text = "<br/>" + recipient + " is not a valid email address<br/><br/>";
                return;
            }
        }

        try
        {
            SendEmail(toAddresses, from, subj, msg, false);
            _MasterPage.FeedbackMessage = "A/R Approval Request Sent";
            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Success;
            modalAR.Hide();
            upMain.Update();
        }
        catch
        {
            lblARFeedback.Text = "<br/>Due to issues contacting the email server, your A/R approval request may not have reached all of its intended recipients. Please contact them to verify.<br/><br/>";
        }
    }

    protected void btnARCancel_Click(object sender, EventArgs e)
    {
        modalAR.Hide();
    }
    #endregion

    #region BOTTOM PAGE LINK BUTTONS
    protected void btnEdit_Click(object sender, EventArgs e)
    {
        if (!RefreshGlobals())
            return;
        SaveTrackingData(_tracking);
        Response.Redirect("~/Screening/Summary.aspx?type=admin&ScrMemType=" + _memo, false);
    }
    #endregion

    #endregion
}
