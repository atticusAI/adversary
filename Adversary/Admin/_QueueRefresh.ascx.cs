﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using HH.Branding;

public partial class QueueRefresh : System.Web.UI.UserControl
{
    public int RefreshTime { get; set; }
    public bool Enabled { get; set; }

    public QueueRefresh()
    {
        RefreshTime = 10000;
        Enabled = true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected override void  OnPreRender(EventArgs e)
    {
 	    base.OnPreRender(e);
        if (!Enabled)
            upRefresh.Visible = false;
    }

    protected void btnRefresh_Click(object sender, EventArgs e)
    {
        IMaster masterPage = ((BasePage)Page)._MasterPage;
        if (string.IsNullOrEmpty(masterPage.FeedbackMessage))
        {
            masterPage.FeedbackMessage = "Your queue was last refreshed " + DateTime.Now.ToLongTimeString();
            masterPage.FeedbackMode = MasterPageFeedbackMode.Success;
        }
    }
}
