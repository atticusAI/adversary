﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Approval.aspx.cs" Inherits="Approval" MasterPageFile="~/Master.master" %>
<%@ Register TagPrefix="UC" Assembly="HH" Namespace="HH.UI.ModalPopupForm" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register TagName="Screen" TagPrefix="UC" Src="~/_usercontrols/Screen.ascx" %>
<%@ Register TagPrefix="UC" TagName="ListContainerControl" Src="~/_usercontrols/ListContainerControl.ascx" %>



<asp:Content ID="Content" ContentPlaceHolderID="UpperContentPlaceHolder" runat="server">

<asp:UpdatePanel ID="upMain" runat="server" RenderMode="Inline" UpdateMode="Conditional">
<ContentTemplate>
    
    <asp:PlaceHolder ID="phARBalance" runat="server"></asp:PlaceHolder>
    <table runat="server" id="tableSubmittedOn" style="padding: 0 0 20px 0;" >
        <tr>    
            <td style="font-weight: bold; width: 425px; padding: 5px 0 5px 0;">Client</td>
            <td>
                <asp:Label ID="lblClient" runat="server" Font-Bold="true" />
            </td>
        </tr>
        <tr>    
            <td style="font-weight: bold; padding: 5px 0 5px 0;">Matter</td>
            <td>
                <asp:Label ID="lblMatter" runat="server" Font-Bold="true" />
            </td>
        </tr>
        <tr>    
            <td style="font-weight: bold;  padding: 5px 0 5px 0;">Current Status</td>
            <td>
                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="sbControl" Width="210px"></asp:DropDownList>&nbsp;&nbsp;
                <asp:Button ID="btnStatus" runat="server"  Text="Change" OnClick="btnStatus_Click" />
            </td>
        </tr>
        <tr id="trRejectionHold" runat="server">
            <%--commented out by A Reimer 03/22/12 for "Rejected to Modification Requested" project --%>
            <%--<td style="font-weight: bold; padding: 5px 0 5px 0;">This rejected screening memo can be resubmitted</td>--%>
            <td style="font-weight:bold; padding:5px 0 5px 0;">This matter requiring modification can be resubmitted</td>
            <td >
                <asp:CheckBox ID="cbRejectionHold" runat="server" Text="" AutoPostBack="true"  OnCheckedChanged="cbRejectionHold_CheckedChanged" /><br />
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; padding: 5px 0 5px 0;">Lock this screening memo from editing</td>
            <td >
                <asp:CheckBox ID="cbLocked" runat="server" Text="" AutoPostBack="true"  OnCheckedChanged="cbLocked_CheckedChanged" />
                <asp:Label ID="lblLocked" runat="server" ForeColor="Red" Text="(Locked)" Font-Bold="true" ></asp:Label>
                
            </td>
        </tr>
        <tr>
            <td colspan="2" style="font-weight:bold; padding: 5px 0 7px 0;">
                Notes:<br />
                <br />
                <asp:TextBox ID="tbNotes" runat="server" TextMode=MultiLine Rows="8" CssClass="sbControl" Width="90%"></asp:TextBox>
            </td>
        </tr>
    </table>
    
    
    <asp:Panel ID="pnlAP" runat="server"   >
    
        <br />
        <table style="width: 825px; line-height: 24px; margin: 50px 0 50px 0;">
            <tr>
                <td style="width: 40%;">
                    <strong>Administrative Partner Approval - Acknowledged By: </strong>
                </td>
                <td style="width: 60%;">
                    <ajax:ComboBox ID="comboAP" runat="server"
                                CssClass="CBAquaStyle"
                                AutoPostBack="true"
                                AutoCompleteMode="Suggest"
                                CaseSensitive="false"
                                DropDownStyle="DropDownList"
                                DataTextField="Login"
                                DataValueField="UserID"
                                OnSelectedIndexChanged="combo_SelectedIndexChanged" />
                </td>
            </tr>
            <tr id="trAPRetainerAcknowledged" runat="server">
                <td><strong><asp:Label ID="lblAPRetainerAcknowledged" runat="server"></asp:Label></strong></td>
                <td><asp:CheckBox ID="cbAPRetainerAcknowledged" runat="server" OnCheckedChanged="cbRetainer_CheckedChanged" AutoPostBack="true" /></td>
            </tr>
            <tr>
                <td><strong>AP Approval Status</strong></td>
                <td><asp:Label ID="lblAPStatus" runat="server" Font-Bold="true"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="2"><UC:Screen ID="scrAP" runat="server" ScreenID="AP" /></td>
            </tr>
            <tr id="trAPRejection" runat="server" visible="false">
                <td colspan="2">
                    <%--commented out by A Reimer 03/22/12 for "Rejection to Modification Request" request --%>
                    <%--<strong>AP Rejection Notes</strong><br />--%>
                    <strong>AP Request for Modification Notes</strong><br />
                    <br />
                    <asp:Label ID="lblAPRejection" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="text-align: right; padding-right: 5px; padding-top: 10px; padding-bottom: 25px;">
                    <asp:Button ID="btnAPApprove" runat="server" Width="210px" CommandArgument="AP"  AutoPostBack="true" OnClick="btnApproval_Click" OnClientClick="this.value+=' (Processing)'; this.enabled = false;" />
                </td>
                <td style="padding-left: 5px; padding-top: 10px; padding-bottom: 25px;">
                    <%--commented out by A Reimer 03/22/12 for "Rejection to Modification Request" request --%>
                    <%--<asp:Button ID="btnAPReject" runat="server" Width="210px" Text="Reject Memo" OnClick="btnReject_Click" />--%>
                    <asp:Button ID="btnAPReject" runat="server" Width="210px" Text="Request for Modification" OnClick="btnReject_Click" />
                </td>
            </tr>
        </table>
        
    </asp:Panel>


    <asp:Panel ID="pnlPGM" runat="server"   >

        <table style="width: 825px; line-height: 24px; margin: 50px 0 50px 0;">
            <tr>
                <td style="width: 40%;">
                    <strong>Practice Group Leader Approval - Acknowledged By: </strong>
                </td>
                <td style="width: 60%;">
                    <ajax:ComboBox ID="comboPGM" runat="server"
                                CssClass="CBAquaStyle"
                                AutoPostBack="true"
                                AutoCompleteMode="Suggest"
                                CaseSensitive="false"
                                DropDownStyle="DropDownList"
                                DataTextField="Login"
                                DataValueField="UserID"
                                OnSelectedIndexChanged="combo_SelectedIndexChanged" />
                </td>
            </tr>
            <tr id="trPGMRetainerAcknowledged" runat="server">
                <td><strong><asp:Label ID="lblPGMRetainerAcknowledged" runat="server"></asp:Label></strong></td>
                <td><asp:CheckBox ID="cbPGMRetainerAcknowledged" runat="server" OnCheckedChanged="cbRetainer_CheckedChanged" AutoPostBack="true" /></td>
            </tr>
            <tr>
                <td><strong>PGL Approval Status</strong></td>
                <td><asp:Label ID="lblPGMStatus" runat="server" Font-Bold="true"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="2"><UC:Screen ID="scrPGM" runat="server" ScreenID="PGM" /></td>
            </tr>
            <tr id="trPGMRejection" runat="server" visible="false">
                <td colspan="2">
                    <%--commented out by A Reimer 03/22/12 for "Rejection to Modification Request" request --%>
                    <%--<strong>PGL Rejection Notes</strong><br />--%>
                    <strong>PGL Request for Modification Notes</strong><br />
                    <br />
                    <asp:Label ID="lblPGMRejection" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="text-align: right; padding-right: 5px; padding-top: 10px; padding-bottom: 25px;">
                    <asp:Button ID="btnPGMApprove" runat="server" Width="210px" CommandArgument="PGM" OnClick="btnApproval_Click" />
                </td>
                <td style="padding-left: 5px; padding-top: 10px; padding-bottom: 25px;">
                    <%--commented out by A Reimer 03/22/12 for "Rejection to Modification Request" request --%>
                    <%--<asp:Button ID="btnPGMReject" runat="server" Width="210px" Text="Reject Memo" OnClick="btnReject_Click" />--%>
                    <asp:Button ID="btnPGMReject" runat="server" Width="210px" Text="Request for Modification" OnClick="btnReject_Click" />
                </td>
            </tr>
        </table>
        
    </asp:Panel>

    <asp:Panel ID="pnlAdversary" runat="server"   >
    
        <table style="width: 825px; line-height: 24px; margin: 50px 0 50px 0;">
            
            <tr ID="trARApproval" runat="server">
                <td><strong>Approval received for A/R over 90 Days</strong></td>
                <td>
                    <asp:CheckBox ID="cbARApproval" runat="server"  />
                    <asp:LinkButton ID="lbAR" runat="server" Text="Send request for approval" Font-Bold="true" OnClick="lbAR_Click"></asp:LinkButton>
                </td>
            </tr>
            
            <tr>
                <td style="width: 40%">
                    <strong>Adversary Group Processing - Processed By: </strong>
                </td>
                <td style="width: 60%">
                    <ajax:ComboBox ID="comboAdversary" runat="server"
                                CssClass="CBAquaStyle"
                                AutoPostBack="true"
                                AutoCompleteMode="Suggest"
                                CaseSensitive="false"
                                DropDownStyle="DropDownList"
                                DataTextField="Login"
                                DataValueField="UserID"
                                OnSelectedIndexChanged="combo_SelectedIndexChanged" />
                </td>
            </tr>
            <tr>
                <td><strong>Adversary Approval Status</strong></td>
                <td><asp:Label ID="lblAdvStatus" runat="server" Font-Bold="true"></asp:Label></td>
            </tr>
            
            <tr id="trAdvAdminApproval" runat="server">
                <td>
                    <strong>Adversary Approved</strong>
                </td>
                <td>
                    <asp:Label ID="lblAdvAdminApproval" runat="server" Font-Bold="true"></asp:Label>
                </td>
            </tr>
                
            <tr>
                <td colspan="2"><UC:Screen ID="scrAdversary" runat="server" ScreenID="Adversary" /></td>
            </tr>
            <tr id="trAdvRejection" runat="server" visible="false">
                <td colspan="2">
                    <%--commented out by A Reimer 03/22/12 for "Rejection to Modification Request" request --%>
                    <%--<strong>Adversary Rejection Notes</strong><br />--%>
                    <strong>Adversary Request for Modification Notes</strong>
                    <br />
                    <asp:Label ID="lblAdvRejection" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="text-align: right; padding-right: 5px; padding-top: 10px; padding-bottom: 25px;">
                    <asp:Button ID="btnAdvAdminApprove" runat="server" Width="210px" CommandArgument="AdvAdmin"  OnClick="btnApproval_Click" />
                    <asp:Button ID="btnComplete" runat="server" Width="210px" Text="Mark Complete" CommandArgument="Complete" OnClick="btnComplete_Click" OnClientClick="this.value+='...(Working)';" />
                </td>
                <td style="padding-left: 5px; padding-top: 10px; padding-bottom: 25px;">
                    <%--commented out by A Reimer 03/22/12 for "Rejection to Modification Request" request --%>
                    <%--<asp:Button ID="btnAdvAdminReject" runat="server" Width="210px" Text="Reject Memo" OnClick="btnReject_Click" />--%>
                    <asp:Button ID="btnAdvAdminReject" runat="server" Width="210px" Text="Request for Modification" OnClick="btnReject_Click" />
                </td>
            </tr>
             <tr>
                <td colspan="2">
                    <UC:ListContainerControl ID="cmList" runat="server" AllowModify="false" AddButtonText="<font style='font-weight: bold; font-size: 10pt;'>Add Client Matter Number</font>" Width="750px" OnOnEntitySubmitComplete="cmList_OnEntitySubmitComplete"></UC:ListContainerControl><br />
                    <!-- Bottom form controls -->
                    <a href="../Screening/PrintView.aspx" style="font-weight: bold; font-size: 10pt;" target="_blank">View Memo in Print View</a><br />  
                    <asp:LinkButton ID="lbSend" runat="server" OnClick="lbSend_Click" CommandName="edit" Font-Bold="true" Font-Size="10pt" OnClientClick="this.innerHTML += '.....(Sending, please wait)';" />
                </td>
            </tr>
        </table>

        
    </asp:Panel>
    
    <!-- Modal Rejection Popup Pane -->
    <UC:ModalForm ID="modalRejection" runat="server" Width="50%">
    
    <asp:UpdatePanel ID="upRejection" runat="server" >
        <ContentTemplate>
            <%--commented out by A Reimer 03/22/12 for "Rejection to Modification Request" request --%>            
            <%--<strong>Please specify a reason why this memo is being rejected (this message will be included in the message to the memo originator and originating attorney).</strong><br />--%>
            <strong>Please specify a reason why this memo requires modification (this message will be included in the message to the memo originator and originating attorney).</strong><br />
            <asp:Label ID="lblRejectionFeedback" runat="server" ForeColor="Red"></asp:Label><br />
            <asp:Panel ID="pnlAPRejectionReason" runat="server">
                <%--commented out by A Reimer 03/22/12 for "Rejection to Modification Request" request --%>            
                <%--<span style=" font-weight: lighter;">AP Rejection Reason:</span>--%>
                <span style=" font-weight: lighter;">AP Modification Request Reason:</span>
                <asp:TextBox ID="tbAPRejectionReason" runat="server" TextMode="MultiLine" Rows="4" CssClass="sbControl" Width="95%"></asp:TextBox>
            </asp:Panel>
            
            <asp:Panel ID="pnlPGMRejectionReason" runat="server">
                <%--commented out by A Reimer 03/22/12 for "Rejection to Modification Request" request --%>
                <%--<span style=" font-weight: lighter;">PGL Rejection Reason:</span>--%>
                <span style=" font-weight: lighter;">PGL Modification Request Reason</span>
                <asp:TextBox ID="tbPGMRejectionReason" runat="server" TextMode="MultiLine" Rows="4" CssClass="sbControl" Width="95%"></asp:TextBox>
            </asp:Panel>
            
            <asp:Panel ID="pnlAdvRejectionReason" runat="server">
                <%--commented out by A Reimer 03/22/12 for "Rejection to Modification Request" request --%>            
                <%--<span style=" font-weight: lighter;">Adversary Rejection Reason:</span>--%>
                <span style=" font-weight:lighter;">Adversary Modification Request Reason</span>
                <asp:TextBox ID="tbAdvRejectionReason" runat="server" TextMode="MultiLine" Rows="4" CssClass="sbControl" Width="95%"></asp:TextBox>
            </asp:Panel>
            <br />
            <br />
            <asp:CheckBox ID="cbModalRejectionHold" runat="server" Font-Bold="true" ForeColor="Red" Text="This memo can be resubmitted by the orignator after amendments have been made" Checked="true" /><br />
            <br />
            <br />
            <div style="text-align: center;">
                <%--commented out by A Reimer 03/22/12 for "Rejection to Modification Request" request --%>            
                <%--<asp:Button ID="btnRejectionSubmit" runat="server" Text="Reject and Send" OnClick="btnRejectionSubmit_Click" Width="210px" OnClientClick="this.value='Sending, please wait';" />&nbsp;&nbsp;&nbsp;--%>
                <asp:Button ID="btnRejectionSubmit" runat="server" Text="Request Modification and Send" OnClick="btnRejectionSubmit_Click" Width="210px" OnClientClick="this.value='Sending, please wait';" />&nbsp;&nbsp;&nbsp;

                <asp:Button ID="btnRejectionCancel" runat="server" Text="Cancel" OnClick="btnRejectionCancel_Click" Width="210px" />
            </div>
            <br />
        
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnRejectionSubmit" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnRejectionCancel" EventName="Click" />
        </Triggers>
        
    </asp:UpdatePanel>
    
    </UC:ModalForm>
    <!-- End Rejection Modal Popup -->
    
    
    <!-- Modal A/R Popup Pane -->
    <UC:ModalForm ID="modalAR" runat="server" Width="45%">
    
    <asp:UpdatePanel ID="upAR" runat="server" >
        <ContentTemplate>
            <strong>Send Email Notification that Approval is required for A/R Balance over 90 Days</strong><br />
            <asp:Label ID="lblARFeedback" runat="server" ForeColor="Red"></asp:Label>
            <table style="width: 95%;">
                <tr>
                    <td style="width: 15%">From</td>
                    <td style="width: 85%"><asp:Label ID="lblFrom" runat="server" Font-Bold="true"></asp:Label>
                </tr>
                <tr>
                    <td>To</td>
                    <td><asp:TextBox ID="tbTo" runat="server" CssClass="sbControl" Width="100%"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Subject</td>
                    <td><asp:TextBox ID="tbSubject" runat="server" CssClass="sbControl" Width="100%"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td><span style="color: Red;">(Seperate multiple email addresses with a semi-colon)</span></td>
                </tr>
            </table>
            
            <asp:TextBox ID="tbARApprovalEmail" runat="server" TextMode="MultiLine" Rows="15" CssClass="sbControl" Width="95%"></asp:TextBox><br />
            <br />
            <div style="text-align: center;">
                <asp:Button ID="btnARSubmit" runat="server" Text="Send Message" OnClick="btnARSubmit_Click" Width="210px" OnClientClick="this.value='Sending, please wait';" />&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnARCancel" runat="server" Text="Cancel" OnClick="btnARCancel_Click"  Width="210px" />
            </div>
            <br />
    
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnARSubmit" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnARCancel" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    
    </UC:ModalForm>
    
    <!-- End Modal A/R Popup Form -->
            
</ContentTemplate>
<Triggers>
    <asp:AsyncPostBackTrigger ControlID="comboAP" EventName="SelectedIndexChanged" />
    <asp:AsyncPostBackTrigger ControlID="cbAPRetainerAcknowledged" EventName="CheckedChanged" />
    <asp:AsyncPostBackTrigger ControlID="btnAPApprove" EventName="Click" />
    <asp:AsyncPostBackTrigger ControlID="comboPGM" EventName="SelectedIndexChanged" />
    <asp:AsyncPostBackTrigger ControlID="cbPGMRetainerAcknowledged" EventName="CheckedChanged" />
    <asp:AsyncPostBackTrigger ControlID="btnPGMApprove" EventName="Click" />
    <asp:AsyncPostBackTrigger ControlID="comboAdversary" EventName="SelectedIndexChanged" />
    <asp:AsyncPostBackTrigger ControlID="btnAdvAdminApprove" EventName="Click" />
    
    <asp:AsyncPostBackTrigger ControlID="cbRejectionHold" EventName="CheckedChanged" />
    <asp:AsyncPostBackTrigger ControlID="cbLocked" EventName="CheckedChanged" />
    
    <asp:AsyncPostBackTrigger ControlID="lbAR" EventName="Click" />
    <asp:AsyncPostBackTrigger ControlID="lbSend" EventName="Click" />
</Triggers>
</asp:UpdatePanel>

</asp:Content>
