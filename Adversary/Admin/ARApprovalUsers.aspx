﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ARApprovalUsers.aspx.cs" Inherits="ARApprovalUsers" MasterPageFile="~/Master.master" %>
<%@ Register TagName="ListContainerControl" TagPrefix="UC" Src="~/_usercontrols/ListContainerControl.ascx" %>


<asp:Content ID="Content" runat="server" ContentPlaceHolderID="UpperContentPlaceHolder">


If additional A/R approval is needed, email notification will be sent to the following default users:<br />
<br />
<UC:ListContainerControl ID="lcUsers" runat="server" />



</asp:Content>
