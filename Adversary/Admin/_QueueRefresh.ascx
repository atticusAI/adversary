﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_QueueRefresh.ascx.cs" Inherits="QueueRefresh" %>
<%@ Register TagPrefix="UC" Assembly="HH" Namespace="HH.UI.ModalPopupForm" %>

<asp:UpdatePanel ID="upRefresh" runat="server">
    <ContentTemplate>
    
    <UC:ModalForm ID="modalRefresh" runat="server" BehaviorID="refreshMsg" Width="40%">
        <asp:Panel ID="pnlModalRefresh" runat="server">
            <br />
            <br />
            <table>
                <tr>
                    <td style="vertical-align: middle;">&nbsp;&nbsp;<img src="../_css/ajax-loader.gif" id="imgRefresh" /></td>
                    <td style="vertical-align: middle; font-size: larger; white-space: nowrap;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Refreshing your queue, please wait</strong></td>
                </tr>
            </table>
            <br />
            <br />
            <br />
        </asp:Panel>
    </UC:ModalForm>
    
    <asp:Button ID="btnRefresh" runat="server" style="visibility: hidden;" OnClick="btnRefresh_Click" />

    <script type="text/javascript">
    
    setInterval('setRefresh()', <%=RefreshTime%>);
    function setRefresh()
    {
        $find('refreshMsg').show();
        if (!document.documentMode) $get('imgRefresh').style.visibility = 'hidden';
        $get('<%=btnRefresh.ClientID %>').click();
    }
    
    </script>

    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btnRefresh" EventName="Click" />
    </Triggers>
    
</asp:UpdatePanel>