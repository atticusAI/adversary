﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FilesAdmin.aspx.cs" Inherits="FilesAdmin" MasterPageFile="~/Master.master" %>
<%@ Register TagName="FileListControl" TagPrefix="UC" Src="~/_usercontrols/FileListControl.ascx" %>


<asp:Content ID="Content" runat="server" ContentPlaceHolderID="UpperContentPlaceHolder">

<table runat="server" id="tableSubmittedOn" style="padding: 35px 0 10px 0; width: 550px;" >
    <tr>    
        <td style="vertical-align: bottom; width: 225px; font-weight: bold;">Screening Memo #</td>
        <td style="vertical-align: bottom; text-align: right;">
            <asp:Label ID="lblScrMemID" runat="server" Font-Bold="true" />
        </td>
    </tr>
    <tr>    
        <td style="vertical-align: bottom; width: 225px; font-weight: bold;">Submitted On</td>
        <td style="vertical-align: bottom; text-align: right;">
            <asp:Label ID="lblSubmittedOn" runat="server" Font-Bold="true" />
        </td>
    </tr>
</table><br />

Attach additional files to this Screening Memo<br />
<br />
<UC:FileListControl ID="fcFiles" runat="server" />



</asp:Content>
