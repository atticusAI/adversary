﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Linq.Expressions;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_DailyList : AdminBasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected override void Render(HtmlTextWriter writer)
    {
        if (!Page.IsPostBack)
        {
            tbStartDate.Text = DateTime.Today.AddDays(-1).ToShortDateString();
            tbEndDate.Text = DateTime.Today.ToShortDateString();
        }
        base.Render(writer);
    }
   
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        GetDailyListData();
    }
    private void GetDailyListData()
    {
        int days = 7;
        using (CMSOpenDataContext dataContext = new CMSOpenDataContext(CMSData.CMSConnectionString))
        {
            //List<Request> _Requests = (from R in dataContext.Requests
            //                           where (R.SubmittalDate >= DateTime.Parse(tbStartDate.Text)
            //                                    && R.SubmittalDate <= DateTime.Parse(tbEndDate.Text).Add(new TimeSpan(1, 0, 0, 0)).Subtract(new TimeSpan(0, 0, 1)))
            //                           orderby R.ClientCategory descending, R.SubmittalDate
            //                           select R).ToList<Request>();
            List<DailyListItem> _Requests = dataContext.USP_ADV_GETLATESTOPENMATTERS(days).ToList<DailyListItem>();

            //if (rblAllIndividual.SelectedValue.Equals("Me"))
            //{
            //    _Requests = _Requests.Where(R => R.e.Equals(_SessionData.AdminLogin.Login, StringComparison.CurrentCultureIgnoreCase)).ToList();
            //}
            //if (!cbAllMemos.Checked)
            //{
            //    _Requests = _Requests.Where(R => R.OKforNewMatter == true).ToList();
            //}

            TableRow tr = new TableRow();
            TableCell tc;
            bool isExisting = false;

            /*<TABLE border=0 WIDTH="552" CELLPADDING=0 CELLSPACING=0>

<TR>
	<TD BGCOLOR="Silver">
		<CFOUTPUT>
			New matters opened in the last <B>#openDays#</B> days (sorted by client date):
		</CFOUTPUT>
	</TD>
</TR>
<TR>
	<TD>
		<TABLE WIDTH="100%">
			<TR BGCOLOR="Teal">
				<TD><FONT COLOR="White"><B>Client Matter</B></FONT></TD>
				<TD><FONT COLOR="White"><B>Open</B></FONT></TD>
				<TD><FONT COLOR="White"><B>Employee</B></FONT></TD>
				<TD><FONT COLOR="White"><B>Lookup</B></FONT></TD>
			</TR>
			<CFOUTPUT QUERY="getHot100">
			<TR BGCOLOR="Silver">
				<TD>
					<A HREF="http://neo.hollandhart.com/samedi/website/htdocs/acct/cms/qlook/cm/cmsGetMatter.cfm?CLIENT_UNO=#CLIENT_UNO#">#CLIENT_NAME#</A>
					<BR>
					#CLIENT_CODE#.#Left(MATTER_CODE,4)# &nbsp; #MATT_CAT_CODE#
					<BR>
					#MATTER_NAME#
				</TD>
				<TD>
					#DateFormat(C_OPEN_DATE, "mm/dd/yy")#
					<BR>
					<BR>
					#DateFormat(M_OPEN_DATE, "mm/dd/yy")#
				</TD>
				<TD VALIGN="TOP">#EMPLOYEE_CODE# : #EMPLOYEE_NAME#</TD>
				<TD VALIGN="TOP"><A HREF="advNewParties.cfm?MATTER_UNO=#MATTER_UNO#">Parties</A></TD>

			</TR>
			</CFOUTPUT>
		</TABLE>

	</TD>
</TR>
</TABLE>*/
            tr = new TableRow();
            tc = new TableCell();
            tc.Text = "New matters opened in the last " + days.ToString() + " days (sorted by client date):";
            tc.ColumnSpan = 4;
            tblParties.Rows.Add(tr);

            foreach (DailyListItem _request in _Requests)
            {
                //if (_request.ClientCategory.Equals("Existing") && !isExisting)
                //{
                //    tc = new TableCell();
                //    tr = new TableRow();
                //    tc.Text = "SEARCH DONE ON EXISTING CLIENTS";
                //    tr.Cells.Add(tc);
                //    this.tblParties.Rows.Add(tr);
                //    isExisting = true;
                //}

                tr = new TableRow();
                tc = new TableCell();
                tc.Text = "Client Matter";
                tc.Font.Bold = true;
                tr.Cells.Add(tc);
                tc = new TableCell();
                tc.Text = "Open";
                tc.Font.Bold = true;
                tr.Cells.Add(tc);

                if (!String.IsNullOrEmpty(_request.ClientName))
                {
                    tc = new TableCell();
                    tr = new TableRow();
                    tc.Text = CreatePartiesString(_request.ClientName);
                    tr.Cells.Add(tc);
                    this.tblParties.Rows.Add(tr);
                }
                //if (!String.IsNullOrEmpty(_request.RelatedParties))
                //{
                //    tc = new TableCell();
                //    tr = new TableRow();
                //    tc.Text = CreatePartiesString(_request.RelatedParties);
                //    tr.Cells.Add(tc);
                //    this.tblParties.Rows.Add(tr);
                //}
                //if (!String.IsNullOrEmpty(_request.AdverseParties))
                //{
                //    tc = new TableCell();
                //    tr = new TableRow();
                //    tc.Text = "Adverse: " + CreatePartiesString(_request.AdverseParties);
                //    tr.Cells.Add(tc);
                //    this.tblParties.Rows.Add(tr);
                //}
                //if (!String.IsNullOrEmpty(_request.AttyLogin))
                //{
                //    tc = new TableCell();
                //    tr = new TableRow();
                //    try
                //    {
                //        tc.Text = "(" + GetAttorneyName(_request.AttyLogin).ToUpper() + ")";
                //    }
                //    catch
                //    {
                //        tc.Text = "Attorney " + _request.AttyLogin + " was not found";
                //    }
                    
                //    tr.Cells.Add(tc);
                //    this.tblParties.Rows.Add(tr);
                //}
                tr = new TableRow();
                tc = new TableCell();
                tc.Text = "&nbsp;";
                tr.Cells.Add(tc);
                this.tblParties.Rows.Add(tr);
            }
        }
        
    }
    protected string CreatePartiesString(string targetString)
    {
        if (String.IsNullOrEmpty(targetString)) return "<br/>";
        System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(@"(\r\n|\r|\n)+");
        string newText = RepeatString("&nbsp;", 9) + regex.Replace(targetString, "<br/>");
        return newText;
    }
    private string RepeatString(string stringToRepeat, int numberOfTimes)
    {
        string strReturn = stringToRepeat;
        for (int i = 0; i < numberOfTimes - 1; i++)
        {
            strReturn += stringToRepeat;
        }
        return stringToRepeat;
    }
    protected string GetAttorneyName(string payrollID)
    {
        //FirmDirectory fDir = new FirmDirectory();
        //System.Data.DataRow dr = fDir.GetFirmDirEmployeeData(payrollID);
        //return dr["SortLastName"].ToString() + ", " + dr["SortFirstName"].ToString() + 
        //    (dr["PreferredMiddleName"] ?? "");
        FirmDirectory fDir = new FirmDirectory();
        HRDataService.Employee employee = fDir.GetEmployee(payrollID);
        return employee.SortLastName + ", " + employee.SortFirstName;
    }

}
