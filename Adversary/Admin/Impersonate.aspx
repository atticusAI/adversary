﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Impersonate.aspx.cs" Inherits="Impersonate" MasterPageFile="~/Master.master" %>
<%@ Register TagPrefix="UC" Assembly="HH" Namespace="HH.UI.AutoComplete" %>

<asp:Content ID="Content" runat="server" ContentPlaceHolderID="UpperContentPlaceHolder">

Type the name of the approver that you would like to view:<br />
<br />
<table>
    <tr>
        <td style=" vertical-align:top"><UC:AutoCompleteField ID="_acApprover" runat="server" TextBoxSearchMessage="(Type name, employee ID or login to search for an approver)" ServiceMethod="GetAdminList" ContextKey="Admin" Mode="ShowSearchBox" MinimumCharacters="1" TextChangeCausesPostBack="false"  /></td>
        <td style=" vertical-align:top"><asp:Button ID="_btnView" runat="server" Text="Show Queue" OnClick="btnView_Click" /></td>
    </tr>
</table>
<br />
<br />

</asp:Content>
