﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using HH;
using HH.Branding;
using HH.Extensions;
using System.Data;
using System.Data.SqlClient;
using HH.SQL;

public partial class Queue : AdminBasePage
{
    private const string CACHE_KEY = "queueControlState";

    private CMSData _cms = CMSData.GetInstance();
    private AdminLogins _login;
    private bool _setForPgm = false;
    private bool _setForDualRole = false;
    private bool _impersonationMode = false;

    private class QueueState
    {
        public int APOffice { get; set; }
        public int PracCode { get; set; }
        public int Status { get; set; }
        //public int Open { get; set; }
        public int AssignedTo { get; set; }
        public int Sort { get; set; }
    }

    protected override void OnInit(EventArgs e)
    {
        _MasterPage.SideBarVisible = false;
        if (!IsPostBack)
            ClearMemo(); //clear any current memo from memory when hitting the queue for the first time

        base.OnInit(e);
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!_SessionData.LoggedIn)
                return;
            if (!IsPostBack && !HH.Utils.SiteMaps.IsUserAuthorized(SiteMap.Providers["Main"].CurrentNode))
                return;
        }
        catch { }

        if (!string.IsNullOrEmpty(Request.QueryString["impersonate"]))
        {
            _impersonationMode = true;

            try
            {
                _login = AdminLogins.GetAdminLoginFromLoginName(Request.QueryString["impersonate"].Trim());
            }
            catch
            {
                _MasterPage.FeedbackMessage = "Could not show queue for " + Request.QueryString["impersonate"];
                _MasterPage.FeedbackMode = MasterPageFeedbackMode.Warning;
                _login = _SessionData.AdminLogin;
            }
            refreshControl.Enabled = false;
        }
        else
        {
            _login = _SessionData.AdminLogin;
            refreshControl.RefreshTime = (_SessionData.AdminLogin.IsAdversary.HasValue && _SessionData.AdminLogin.IsAdversary.Value) ? 300000 : 900000;
        }

        aboutClickRegion.Visible = !(_login.IsAdversary.HasValue && _login.IsAdversary.Value);
        SetDropDownLists(!IsPostBack);
        LoadQueueState();
        BindRepeater();
    }

    private void SetDropDownLists(bool bindLists)
    {
        bool isAP = _login.IsAP.HasValue && _login.IsAP.Value;
        bool isPGM = _login.IsPGM.HasValue && _login.IsPGM.Value;
        bool isAdversary = _login.IsAdversary.HasValue && _login.IsAdversary.Value;
        bool isAdversaryAdmin = _login.IsAdversaryAdmin.HasValue && _login.IsAdversaryAdmin.Value;
        _setForPgm = isPGM && !isAP && !isAdversary;
        _setForDualRole = isPGM && isAP && !isAdversary;

        if (!bindLists)
            return;

        //Office
        ddlAPOffice.Items.Clear();
        ddlAPOffice.DataTextField = CMSData.OFFICE_DESCRIPTION;
        ddlAPOffice.DataValueField = CMSData.OFFICE_CODE;
        ddlAPOffice.DataSource = _cms.AllOffices;
        ddlAPOffice.DataBind();
        ddlAPOffice.Items.Insert(0, new ListItem("All Offices", string.Empty));
        ddlAPOffice.SelectedIndex = 0;
        if ((isAP && !isPGM && !isAdversary) || _setForDualRole)
        {
            ddlAPOffice.Enabled = _setForDualRole;
            ddlAPOffice.SelectedValue = _login.Office.ToString();
        }
        //TODO: Get rid of this loop, need to change Tech Center to DTC
        foreach (ListItem item in ddlAPOffice.Items)
        {
            if (item.Text == "Tech Center")
            {
                item.Text = "DTC";
                break;
            }
        }

        //Prac Code
        if (_setForPgm)
        {
            int selectedIndex = -1;
            if (IsPostBack)
                selectedIndex = ddlPracGroup.SelectedIndex;
            ddlPracGroup.Items.Clear();

            foreach (AdminPracCodes code in _login.AdminPracCodes)
                ddlPracGroup.Items.Add(new ListItem(_cms.GetPracticeName(code.PracCode), code.PracCode));

            if (_login.AdminPracCodes.Count <= 1)
                ddlPracGroup.Enabled = false;
            else
                ddlPracGroup.Items.Insert(0, new ListItem("My Practice Groups", "my"));
            ddlPracGroup.SelectedIndex = 0;
            try
            {
                if (IsPostBack && selectedIndex != -1)
                    ddlPracGroup.SelectedIndex = selectedIndex;
            }
            catch { }
        }
        else
        {
            int selectedIndex = -1;
            if (IsPostBack)
                selectedIndex = ddlPracGroup.SelectedIndex;
            ddlPracGroup.Items.Clear();

            ddlPracGroup.DataTextField = CMSData.PRACTICE_TYPES_DESCRIPTION_PLUS_CODE;
            ddlPracGroup.DataValueField = CMSData.PRACTICE_TYPES_CODE;
            ddlPracGroup.DataSource = _cms.PracticeTypes;
            ddlPracGroup.DataBind();
            ddlPracGroup.Items.Insert(0, new ListItem("All Practice Groups", string.Empty));
            if (_setForDualRole) ddlPracGroup.Items.Insert(0, new ListItem("My Practice Groups", "my"));
            ddlPracGroup.SelectedIndex = selectedIndex;
        
        }

        //Status
        ddlStatus.Items.Clear();
        ddlStatus.DataTextField = Tracking.STATUS_DESCRIPTION;
        ddlStatus.DataValueField = Tracking.STATUS_ID;
        ddlStatus.DataSource = Tracking.StatusTypes;
        ddlStatus.DataBind();
        ddlStatus.Items.Remove(ddlStatus.Items.FindByValue(((int)TrackingStatusType.None).ToString()));
        ddlStatus.Items.Insert(0, new ListItem("All Open Memos", string.Empty));
        ddlStatus.SelectedIndex = 0;
        //if (isAdversary && !isAdversaryAdmin)
        //    ddlStatus.SelectedValue = ((int)TrackingStatusType.AdversaryGroupProcessing).ToString();

        //approved (disabled for now, might turn into enhancement request needs the DB hook to complete)
        //if (ddlOpen.Items.Count == 0)
        //{
        //    ddlOpen.Items.Add(new ListItem("Show open memos that have been Approved", "show"));
        //    ddlOpen.Items.Add(new ListItem("Hide open memos that have been Approved", "hide"));
        //}
        //ddlOpen.Enabled = (isAP || isPGM || isAdversaryAdmin);
        //ddlOpen.ClearSelection();

        //assigned to
        if (ddlAssignedTo.Items.Count == 0)
        {
            ddlAssignedTo.Enabled = true;
            ddlAssignedTo.Items.Add(new ListItem("Anyone", string.Empty));
            ddlAssignedTo.Items.Add(new ListItem("Me", _login.UserID.ToString()));
        }
        ddlAssignedTo.SelectedIndex = 0;

        //sort
        if (ddlSort.Items.Count == 0)
        {
            ddlSort.Items.Add(new ListItem("Oldest First", "true"));
            ddlSort.Items.Add(new ListItem("Newest First", "false"));
        }
        ddlSort.SelectedIndex = 0;

        btnRemove.Enabled = false;
    }

    private void LoadQueueState()
    {
        if (IsPostBack)
            return;

        try
        {
            QueueState qs = (QueueState)Session[CACHE_KEY];
            if (qs == null)
                return;

            if (ddlAPOffice.Enabled)
                ddlAPOffice.SelectedIndex = qs.APOffice;
            ddlPracGroup.SelectedIndex = qs.PracCode;
            ddlStatus.SelectedIndex = qs.Status;
            //ddlOpen.SelectedIndex = qs.Open;
            ddlAssignedTo.SelectedIndex = qs.AssignedTo;
            ddlSort.SelectedIndex = qs.Sort;
            btnRemove.Enabled = true;
        }
        catch { }
    }

    private void SaveQueueState()
    {
        try
        {
            QueueState qs = new QueueState();
            if (ddlAPOffice.Enabled)
                qs.APOffice = ddlAPOffice.SelectedIndex;
            qs.PracCode = ddlPracGroup.SelectedIndex;
            qs.Status = ddlStatus.SelectedIndex;
            //qs.Open = ddlOpen.SelectedIndex;
            qs.AssignedTo = ddlAssignedTo.SelectedIndex;
            qs.Sort = ddlSort.SelectedIndex;
            Session[CACHE_KEY] = qs;
        }
        catch { }

    }

    private class SubmittedOnComparer : IComparer<Memo>
    {
        bool _desc = false;
        protected internal SubmittedOnComparer(bool sortOldestFirst)
        {
            _desc = sortOldestFirst;
        }
        int IComparer<Memo>.Compare(Memo x, Memo y)
        {
            try
            {
                if (_desc)
                    return DateTime.Compare(x.Tracking.SubmittedOn.Value, y.Tracking.SubmittedOn.Value);
                else
                    return DateTime.Compare(y.Tracking.SubmittedOn.Value, x.Tracking.SubmittedOn.Value);
            }
            catch
            {
                return 0;
            }
        }
    }

    private void BindRepeater()
    {
        int? office = null;
        string pracCode = null;
        TrackingStatusType? status = null;
        bool sortOldestFirst = true;
        int? userId = null;

        bool isAdversary = (_login.IsAdversary.HasValue && _login.IsAdversary.Value);

        if (ddlAPOffice.SelectedValue != string.Empty)
            office = Int32.Parse(ddlAPOffice.SelectedValue);

        if (ddlPracGroup.SelectedValue != string.Empty)
            pracCode = ddlPracGroup.SelectedValue;

        ddlAssignedTo.Enabled = true;
        if (ddlStatus.SelectedValue != string.Empty)
        {
            status = (TrackingStatusType?)Int32.Parse(ddlStatus.SelectedValue);
            //commented out by A Reimer 03/22/12 for "Rejected to Modification Requested" project
            //if (status == TrackingStatusType.Completed ||
            //    status == TrackingStatusType.Rejected)// ||
            //    //status == TrackingStatusType.AdversaryGroupRejected)
            if(status == TrackingStatusType.Completed || status == TrackingStatusType.Rejected || status == TrackingStatusType.ModificationRequested)
            {
                ddlAssignedTo.SelectedValue = _login.UserID.ToString();
                ddlAssignedTo.Enabled = false;
            }
        }

        sortOldestFirst = !string.IsNullOrEmpty(ddlSort.SelectedValue) && Boolean.Parse(ddlSort.SelectedValue);

        if (ddlAssignedTo.SelectedValue != string.Empty)
            userId = Int32.Parse(ddlAssignedTo.SelectedValue);


        bool bindDualRole = (_setForDualRole && ddlAPOffice.SelectedValue == _login.Office.ToString() && ddlPracGroup.SelectedValue == "my");
        List<Memo> memoList;
        using (SqlConnection conn = AdversaryData.GetConnection())
        {
            
            //List<Memo> memoList = Tracking.GetOpenMemos(userId, status, sortOldestFirst);
            //DataTable dt = Tracking.GetQueue(userId, status, sortOldestFirst);
            //List<Memo> memoList = new List<Memo>();
            //dt.FillIList(memoList);
            memoList = Tracking.GetQueue(conn, userId, status, sortOldestFirst);

            try
            {

                //filter out rejected memo for non-adversary
                if (!isAdversary)
                    memoList = memoList.Where(memo => memo.ScrMemType != 6).ToList();

                //filter by office
                if (ddlAPOffice.SelectedValue != string.Empty && !bindDualRole)
                {
                    //TODO: temp fix for Carson City, remove this when multiple APs allowed
                    if (ddlAPOffice.SelectedItem.Text == "Reno")
                    {
                        
                        memoList = memoList.Where(memo => memo.RespAttOfficeName.ToLower() == "reno" || memo.RespAttOfficeName.ToLower().Contains("carson")).ToList();
                    }
                    else if (ddlAPOffice.SelectedItem.Text.Contains("Washington")) //added by A Reimer after change to use HHESB services.
                    {
                        memoList = memoList.Where(memo => memo.RespAttOfficeName.ToLower().Contains("washington")).ToList();
                    }
                    else
                        memoList = memoList.Where(memo => (memo.RespAttOfficeName == null) || (memo.RespAttOfficeName.ToLower() == ddlAPOffice.SelectedItem.Text.ToLower())).ToList();
                }

            }
            catch (Exception ex)
            {
                HandleError(ex);
            }

            try
            {
                //filter by prac code
                if (bindDualRole)
                {
                    List<Memo> query = new List<Memo>();
                    //add a catch to make sure that reno and carson memos are combined
                    if (ddlAPOffice.SelectedItem.Text == "Reno")
                    {
                        query = (from m in memoList
                                 from p in _login.AdminPracCodes
                                 where (m.PracCode == p.PracCode) ||
                                       (m.RespAttOfficeName.ToLower() == "reno" || m.RespAttOfficeName.ToLower().Contains("carson"))
                                 select m).Distinct().ToList();
                    }
                    else
                    {
                        query = (from m in memoList
                                 from p in _login.AdminPracCodes
                                 where (m.PracCode == p.PracCode) ||
                                       (m.RespAttOfficeName.ToLower() == ddlAPOffice.SelectedItem.Text.ToLower())
                                 select m).Distinct().ToList();
                    }


                    memoList = query;
                }
                else if (_setForPgm && ddlPracGroup.SelectedValue == "my")
                {
                    var query = from m in memoList
                                from p in _login.AdminPracCodes
                                where m.PracCode == p.PracCode
                                select m;
                    memoList = query.ToList();
                }
                else if (ddlPracGroup.SelectedValue != string.Empty)
                    memoList = memoList.Where(memo => memo.PracCode == ddlPracGroup.SelectedValue).ToList();

            }
            catch (Exception ex)
            {
                HandleError(ex);
            }

            DatabaseWorker worker = new DatabaseWorker(conn);
            foreach (Memo memo in memoList)
            {
                try
                {
                    Tracking tracking = new Tracking();
                    tracking.ScrMemID = memo.ScrMemID;
                    memo.Tracking = worker.RetrieveSingle<Tracking>(tracking);

                    if (memo.ScrMemType != 1 && memo.ScrMemType != 5 && !string.IsNullOrEmpty(memo.ClientNumber))
                        continue;

                    NewClient client = new NewClient();
                    client.ScrMemID = memo.ScrMemID;
                    NewClient nc = worker.RetrieveSingle<NewClient>(client);
                    if (nc != null)
                        memo.NewClient = nc;
                }
                catch { }
            }

            conn.Close();
        }

        if (bindDualRole)
            memoList = memoList.OrderBy(m => m, new SubmittedOnComparer(sortOldestFirst)).ToList();


        if (isAdversary)
        {
            List<Memo> processingList = (from m in memoList
                                         where m.Tracking.StatusTypeID == TrackingStatusType.AdversaryGroupProcessing
                                         select m).Distinct().ToList();

            memoList = memoList.Except(processingList).ToList();

            if (processingList.Count > 0)
            {
                pnlProcessing.Visible = true;
                lbProcessing.InnerText = processingList.Count + " " + ((processingList.Count == 1) ? "item" : "items") + " found in the Adversary Processing Queue";
                repProcessing.Login = _login;
                repProcessing.DataSource = processingList;
                repProcessing.DataBind();
            }

            pnlPrimary.Visible = (memoList.Count > 0);
            repPrimary.Login = _login;
            repPrimary.DataSource = memoList;
            repPrimary.DataBind();
            lbPrimary.InnerText = memoList.Count + " " + ((memoList.Count == 1) ? "item" : "items") + " found in the Adversary Approval Queue";
            pnlBackup.Visible = false;

            if (!IsPostBack && _login.IsAdversaryAdmin.HasValue && _login.IsAdversaryAdmin.Value)
            {
                cpPrimary.Collapsed = false;
                cpProcessing.Collapsed = true;
                lbProcessing.InnerText += " (click to expand list)";
            }
            else if (!IsPostBack)
            {
                cpPrimary.Collapsed = true;
                cpProcessing.Collapsed = false;
                lbPrimary.InnerText += " (click to expand list)";
            }
        }
        else
        {
            List<Memo> processingList;

            bool isAP = _login.IsAP.HasValue && _login.IsAP.Value; //&& !_setForDualRole;
            bool isPGM = _login.IsPGM.HasValue && _login.IsPGM.Value;// && !_setForDualRole;

            if (_setForDualRole)
            {
                processingList = (from m in memoList
                                  where (m.Tracking.StatusTypeID == TrackingStatusType.AdversaryGroupProcessing) ||
                                        ((m.RespAttOfficeCode != _login.Office.ToString()) || 
                                         (m.RespAttOfficeCode == _login.Office.ToString()&& m.Tracking.APApproved.HasValue && m.Tracking.APApproved.Value)) &&
                                        ((!_login.IsMemberOfPracticeGroup(m.PracCode)) || 
                                         (_login.IsMemberOfPracticeGroup(m.PracCode) && m.Tracking.PGMApproved.HasValue && m.Tracking.PGMApproved.Value))
                                  select m).Distinct().ToList();
            }
            else
            {
                processingList = (from m in memoList
                                  where (m.Tracking.StatusTypeID == TrackingStatusType.AdversaryGroupProcessing) ||
                                        ((isAP && m.RespAttOfficeCode == _login.Office.ToString() && m.Tracking.APApproved.HasValue && m.Tracking.APApproved.Value) ||
                                         (isPGM && _login.IsMemberOfPracticeGroup(m.PracCode) && m.Tracking.PGMApproved.HasValue && m.Tracking.PGMApproved.Value))
                                  //(isAP && m.Tracking.APApproved.HasValue && m.Tracking.APApproved.Value) ||
                                  //(isPGM && m.Tracking.PGMApproved.HasValue && m.Tracking.PGMApproved.Value) ||
                                  //(_setForDualRole && m.Tracking.APApproved.HasValue && m.Tracking.APApproved.Value && m.Tracking.PGMApproved.HasValue && m.Tracking.PGMApproved.Value)
                                  select m).Distinct().ToList();
            }

            memoList = memoList.Except(processingList).ToList();

            List<Memo> primaryList;
            List<Memo> backupList;

            string officeCity = _cms.AllOffices.AsEnumerable().Where(row => row.Field<string>(CMSData.OFFICE_CODE) == _login.Office.ToString()).Single()[CMSData.OFFICE_DESCRIPTION].ToString();
            if (officeCity == "Tech Center")
                officeCity = "DTC";

            primaryList = (from m in memoList.Except(processingList).ToList()
                           from p in _login.AdminPracCodes.DefaultIfEmpty()
                           where (m.RespAttOfficeName == officeCity && (isAP /*|| _setForDualRole*/) && _login.IsPrimaryAP.HasValue && _login.IsPrimaryAP.Value) ||
                                 (m.Tracking.SubmittedOn.HasValue && m.Tracking.SubmittedOn.Value <= DateTime.Now.AddDays(-4)) ||
                                 (p != null && (isPGM /*|| _setForDualRole*/) && m.PracCode == p.PracCode && p.IsPrimary.HasValue && p.IsPrimary.Value)
                           select m).Distinct().ToList();

            backupList = memoList.Except(primaryList).ToList();

            lbProcessing.InnerText += " (click to expand list)";



            if (processingList.Count > 0)
            {
                pnlProcessing.Visible = true;
                lbProcessing.InnerText = processingList.Count + " " + ((processingList.Count == 1) ? "item has" : "items have") + " been approved (click to expand list)";
                repProcessing.Login = _login;
                repProcessing.DataSource = processingList;
                repProcessing.DataBind();
            }

            if (primaryList.Count > 0)
            {
                //lbPrimary.Visible = (backupList.Count > 0);
                pnlPrimary.Visible = true;
                lbPrimary.InnerText = primaryList.Count + " " + ((primaryList.Count == 1) ? "item" : "items") + " found in your Primary Queue";
                repPrimary.Login = _login;
                repPrimary.DataSource = primaryList;
                repPrimary.DataBind();
            }

            if (backupList.Count > 0)
            {
                pnlBackup.Visible = true;
                lbBackup.InnerText = backupList.Count + " " + ((backupList.Count == 1) ? "item" : "items") + " found in your Backup Queue (click to expand list)";
                repBackup.Login = _login;
                repBackup.DataSource = backupList;
                repBackup.DataBind();
            }
        }

        if (_impersonationMode)
        {
            lblCount.Visible = true;
            lblCount.ForeColor = Color.Green;
            lblCount.Text = "Showing queue for " + _login.Login + " - " + memoList.Count + " memos awaiting approval<br/><br/><br/>";
        }
        else if (pnlPrimary.Visible && pnlBackup.Visible)
        {
            lblCount.Visible = true;
            lblCount.ForeColor = Color.Black;
            lblCount.Text = memoList.Count + " total items were found in your queues that are awaiting approval<br/><br/><br/>";
        }
        else if (!pnlPrimary.Visible && !pnlBackup.Visible)
        {
            lblCount.Visible = true;
            lblCount.Font.Size = FontUnit.Larger;
            lblCount.ForeColor = Color.Green;
            if (isAdversary)
                lblCount.Text = "There are no items in the Adversary Approval Queue<br/><br/><br/>";
            else
                lblCount.Text = "There are no items in your queue<br/><br/><br/>";
        }

        else
        {
            lblCount.Visible = false;
        }
        //lbFilter.Visible = (memoList.Count > 0);

        //refreshControl.Enabled = _login.IsAdversary.HasValue && _login.IsAdversary.Value;
    }

 
    private void AddToRow(TableRow tr, string caption, string value, bool extend)
    {
        TableCell td;

        td = new TableCell();
        td.VerticalAlign = VerticalAlign.Top;
        //td.Style.Add("color", "#68849F");
        td.Width = Unit.Pixel(100);
        td.Text = caption;
        tr.Cells.Add(td);

        td = new TableCell();
        td.Style.Add("color", "#68849F");
        if (extend)
        {
            td.Width = Unit.Pixel(200);
            td.ColumnSpan = 3;
        }
        else
        {
            td.Width = Unit.Pixel(200);
        }
        td.Text = value;
        tr.Cells.Add(td);
    }

    
    protected void ddlFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        modalFilter.Show();
        btnRemove.Enabled = true;
        SaveQueueState();
    }

    protected void btnRemove_Click(object sender, EventArgs e)
    {
        Session.Remove(CACHE_KEY);
        try
        {
            ddlPracGroup.SelectedIndex = -1;
            ddlAPOffice.SelectedIndex = -1;
            ddlAssignedTo.SelectedIndex = -1;
            //ddlOpen.SelectedIndex = -1;
            ddlSort.SelectedIndex = -1;
            ddlStatus.SelectedIndex = -1;
        }
        catch { }
        SetDropDownLists(true);
        BindRepeater();
        //modalFilter.Show();
    }

    protected void btnClose_Click(object sender, EventArgs e)
    {
        modalFilter.Hide();
    }
}
