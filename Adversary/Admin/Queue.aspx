﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Queue.aspx.cs" Inherits="Queue" MasterPageFile="~/Master.master" %>
<%@ Register TagPrefix="UC" Assembly="HH" Namespace="HH.UI.ModalPopupForm" %>
<%@ Register TagPrefix="UC" TagName="QRepeater" Src="~/Admin/_QueueRepeater.ascx" %>
<%@ Register TagPrefix="UC" TagName="QRefresh" Src="~/Admin/_QueueRefresh.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContentPlaceHolder" runat="server">
    
    
    <table style="padding: 0 0 0 0; margin: 0 0 0 0; border: none 0 white;">
        
        <!-- FILTER -->
        <tr>
            <td>            
                <asp:LinkButton ID="lbFilter" runat="server" Text="Filter Your Queue" Font-Bold="true" Font-Size="Larger"></asp:LinkButton>   
                <UC:ModalForm ID="modalFilter" runat="server" Width="550px" TargetControlID="lbFilter" Y="200">

                    <asp:UpdatePanel ID="pnlFilter" runat="server">       
                    
                        <ContentTemplate>
                            <br />
                            <br />
                            <table width="550px">
                                <tr>
                                    <td>Office</td>
                                    <td><asp:DropDownList ID="ddlAPOffice" runat="server" AutoPostBack="true" CssClass="sbControl" Width="350px" OnSelectedIndexChanged="ddlFilter_SelectedIndexChanged" /></td>
                                </tr>
                                <tr>
                                    <td>Practice Group</td>
                                    <td><asp:DropDownList ID="ddlPracGroup" runat="server" AutoPostBack="true" CssClass="sbControl" Width="350px" OnSelectedIndexChanged="ddlFilter_SelectedIndexChanged" /></td>
                                </tr>
                                <tr>
                                    <td>Status</td>
                                    <td><asp:DropDownList ID="ddlStatus" runat="server" AutoPostBack="true" CssClass="sbControl" Width="350px" OnSelectedIndexChanged="ddlFilter_SelectedIndexChanged" /></td>
                                </tr>
                                <tr id="trOpen" runat="server" visible="false">
                                    <td>Open Memo Visibility</td>
                                    <td><asp:DropDownList ID="ddlOpen" runat="server" AutoPostBack="true" CssClass="sbControl" Width="350px" OnSelectedIndexChanged="ddlFilter_SelectedIndexChanged" /></td>
                                </tr>
                                <tr>
                                    <td>Assigned To</td>
                                    <td><asp:DropDownList ID="ddlAssignedTo" runat="server" AutoPostBack="true" CssClass="sbControl" Width="350px" OnSelectedIndexChanged="ddlFilter_SelectedIndexChanged" /></td>
                                </tr>
                                <tr>
                                    <td>Sort Order</td>
                                    <td><asp:DropDownList ID="ddlSort" runat="server" AutoPostBack="true" CssClass="sbControl" Width="350px" OnSelectedIndexChanged="ddlFilter_SelectedIndexChanged" /></td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: center;">
                                        <br />
                                        <asp:Button ID="btnRemove" runat="server" Text="Remove Filter" OnClick="btnRemove_Click" Width="120px" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:Button ID="btnClose" runat="server" Text="Close" Width="120px" OnClick="btnClose_Click" /><br />
                                        <br />            
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnRemove" EventName="Click" />  
                            <asp:AsyncPostBackTrigger ControlID="btnClose" EventName="Click" />  
                            <asp:AsyncPostBackTrigger ControlID="ddlAPOffice" EventName="SelectedIndexChanged" />
                            <asp:AsyncPostBackTrigger ControlID="ddlPracGroup" EventName="SelectedIndexChanged" />
                            <asp:AsyncPostBackTrigger ControlID="ddlStatus" EventName="SelectedIndexChanged" />
                            <asp:AsyncPostBackTrigger ControlID="ddlSort" EventName="SelectedIndexChanged" />
                            <asp:AsyncPostBackTrigger ControlID="ddlAssignedTo" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                
                </UC:ModalForm>
            </td>
        </tr>
        
        <!-- HELP SECTION -->
        <tr id="aboutClickRegion" runat="server" visible="false">
            <td style="vertical-align: top;">
                <asp:LinkButton ID="lbAbout" runat="server" Text="About This Screen" Font-Bold="true" Font-Size="Larger" OnClientClick="return false;"></asp:LinkButton>
                <ajax:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="Server"
                    TargetControlID="pnlAbout"
                    CollapsedSize="0"
                    Collapsed="True"
                    ExpandControlID="aboutClickRegion"
                    CollapseControlID="aboutClickRegion"
                    AutoCollapse="False"
                    AutoExpand="False"
                    ScrollContents="False"
                    ExpandDirection="Vertical" />
                <asp:Panel ID="pnlAbout" runat="server" >
                <ul style="font-size: 10pt; text-align: left;">
                    <li><strong>Primary Queue</strong><br />Items in this queue require your approval as Primary Approver <strong>(OR)</strong> are items over four days old that require your approval as Backup Approver.<br /><br /></li>
                    <li><strong>Backup Queue</strong><br />Items in this queue are awaiting approval by the Primary Approver.<br /><br /></li>
                    <li><strong>Approved Queue</strong><br />Items in this queue have already been approved by you or another approver and are awaiting further approval and/or processing by Adversary.<br /><br /></li>
                </ul>
                </asp:Panel>
            </td>
            <td style="vertical-align: top;">
                <img src="../_css/about.gif" />
            </td>
        </tr>
    </table>

</asp:Content>

<asp:Content ID="Content" ContentPlaceHolderID="UpperContentPlaceHolder" runat="server">
  
<asp:UpdatePanel ID="upQueue" runat="server">

    <ContentTemplate>
        
        <asp:Label ID="lblCount" runat="server" Font-Bold="true" Visible="false"></asp:Label>
        
        <!-- Backup Queue -->
        <asp:Panel ID="pnlBackup" runat="server" Visible="false" Width="100%">
        
            <img src="../_css/nothing.gif" id="imgBackup" runat="server" alt="expand/collapse" />&nbsp;&nbsp;
            <a href="#" ID="lbBackup" runat="server" style="font-weight: bold;" onclick="handleClick(this); return false;">Show items in your backup queue</a><br />
            <ajax:CollapsiblePanelExtender ID="cpBackup" runat="Server"
                TargetControlID="pnlBackupRepeater"
                CollapsedSize="0"
                Collapsed="True"
                ExpandControlID="lbBackup"
                CollapseControlID="lbBackup"
                AutoCollapse="False"
                AutoExpand="False"
                ScrollContents="False"
                CollapsedText="Show Details..."
                ExpandedText="Hide Details" 
                ImageControlID="imgBackup"
                ExpandedImage="~/_css/expand.jpg"
                CollapsedImage="~/_css/collapse.jpg"
                ExpandDirection="Vertical" />

            <asp:Panel ID="pnlBackupRepeater" runat="server" Width="100%">
                <br />
                <br />
                <UC:QRepeater ID="repBackup" runat="server"></UC:QRepeater>
                
            </asp:Panel>
            <br />
            <br />
        </asp:Panel>
        
        <!-- Primary Queue -->
        
        <asp:Panel ID="pnlPrimary" runat="server"  Width="100%" Visible="false">
        
            <img src="../_css/nothing.gif" id="imgPrimary" runat="server" alt="expand/collapse" />&nbsp;&nbsp;
            <a href="#" ID="lbPrimary" runat="server" style="font-weight: bold;" onclick="handleClick(this); return false;">Show items in your primary queue</a><br />
            <ajax:CollapsiblePanelExtender ID="cpPrimary" runat="Server"
                TargetControlID="pnlPrimaryRepeater"
                CollapsedSize="0"
                Collapsed="False"
                ExpandControlID="lbPrimary"
                CollapseControlID="lbPrimary"
                AutoCollapse="False"
                AutoExpand="False"
                ScrollContents="False"
                CollapsedText="Show Details..."
                ExpandedText="Hide Details" 
                ImageControlID="imgPrimary"
                ExpandedImage="~/_css/expand.jpg"
                CollapsedImage="~/_css/collapse.jpg"
                ExpandDirection="Vertical" />

            <asp:Panel ID="pnlPrimaryRepeater" runat="server" Width="100%">
                <br />
                <br />
                <UC:QRepeater ID="repPrimary" runat="server"></UC:QRepeater>
                
            </asp:Panel>
            <br />
        </asp:Panel>
        
        <!-- Processing Queue -->
        
        <asp:Panel ID="pnlProcessing" runat="server" Visible="false" Width="100%">
            <hr />
            <br />
            <img src="../_css/nothing.gif" id="imgProcessing" runat="server" alt="expand/collapse" onclientclick="return false;"  />&nbsp;&nbsp;
            <a href="#" ID="lbProcessing" runat="server" style="font-weight: bold;" onclick="handleClick(this); return false;">Show items being processed by Adversary</a>
            <br />
            <ajax:CollapsiblePanelExtender ID="cpProcessing" runat="Server"
                TargetControlID="pnlProcessingRepearter"
                CollapsedSize="0"
                Collapsed="True"
                ExpandControlID="lbProcessing"
                CollapseControlID="lbProcessing"
                AutoCollapse="False"
                AutoExpand="False"
                ScrollContents="False"
                CollapsedText="Show Details..."
                ExpandedText="Hide Details" 
                ImageControlID="imgProcessing"
                ExpandedImage="~/_css/expand.jpg"
                CollapsedImage="~/_css/collapse.jpg"
                ExpandDirection="Vertical" />

            <asp:Panel ID="pnlProcessingRepearter" runat="server" Width="100%">
                <br />
                <br />
                <UC:QRepeater ID="repProcessing" runat="server"></UC:QRepeater>
                
            </asp:Panel>
            <br />
            <br />
        </asp:Panel>
        
        
        <!-- REFRESH -->
        <UC:QRefresh ID="refreshControl" runat="server"></UC:QRefresh>
        
        
        <script type="text/javascript">
        var clickTxt = "(click to expand list)";
        
        function handleClick(elem)
        {
            var text = elem.innerHTML;
            if (text.indexOf(clickTxt) != -1)
                text = text.replace(clickTxt, "");
            elem.innerHTML = text;
        }
        </script>
        
        
        
    </ContentTemplate>
    
    
</asp:UpdatePanel>

    
</asp:Content>