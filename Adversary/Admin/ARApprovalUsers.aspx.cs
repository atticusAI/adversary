﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Data.SqlClient;
using HH;
using HH.Screen;
using HH.Screen;
using HH.Branding;
using HH.SQL;

public partial class ARApprovalUsers : AdminBasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lcUsers.OnEntitySubmitComplete += new HH.Screen.EntitySubmitEventHandler(lcUsers_OnEntitySubmitComplete);

        lcUsers.DataSource = ARApprovalUser.GetAllARApprovalRecipients();
        lcUsers.DataBind();                       
    }

    void lcUsers_OnEntitySubmitComplete(HH.Screen.IListContainerControl sender, object dataSource, object entity, EntitySubmitType type)
    {
        using (SqlConnection conn = AdversaryData.GetConnection())
        {
            DatabaseWorker worker = new DatabaseWorker(conn);
            switch (type)
            {
                case EntitySubmitType.Create:
                    worker.Commit(entity);
                    break;
                case EntitySubmitType.Modify:
                    worker.Commit(entity);
                    break;
                case EntitySubmitType.Delete:
                    worker.Delete(entity);
                    break;
            }

            lcUsers.DataSource = worker.RetrieveToList<ARApprovalUser>();
            lcUsers.DataBind();
            conn.Close();
        }
    }

    

}
