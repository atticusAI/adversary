﻿<%@ Page Title="Adversary Daily List" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="DailyList.aspx.cs" Inherits="Admin_DailyList" %>
<%@ Import Namespace="System.Text" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>


<asp:Content ID="Content2" ContentPlaceHolderID="UpperContentPlaceHolder" Runat="Server">
<asp:UpdatePanel ID="upnlDailyList" runat="server" ChildrenAsTriggers="true">
<Triggers>
    <asp:AsyncPostBackTrigger ControlID="btnSubmit" EventName="Click" />
</Triggers>
<ContentTemplate>
<table width="600">
    <tr>
        <td>
            Start Date/Time:
        </td>
        <td>
            <asp:TextBox ID="tbStartDate" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            End Date/Time:
        </td>
        <td>
            <asp:TextBox ID="tbEndDate" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButtonList ID="rblAllIndividual" runat="server" RepeatDirection="Horizontal">
                <asp:ListItem Text="Just Me" Value="Me" Selected="True"></asp:ListItem>
                <asp:ListItem Text="Everyone" Value="Everyone"></asp:ListItem>
            </asp:RadioButtonList>
        </td>
        <td>
            <asp:CheckBox ID="cbAllMemos" runat="server" Text="All Memos (including items that were not OK'd to be reported in the Daily Memo)" />
        </td>
    </tr>
    <tr>
        <td colspan="2" align="right">
            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
            <asp:UpdateProgress AssociatedUpdatePanelID="upnlDailyList" runat="server" ID="progress1" DisplayAfter="0">
                <ProgressTemplate>Loading....</ProgressTemplate>
            </asp:UpdateProgress>
        </td>
    </tr>
    <tr>
        <td colspan="2">This is a partial list of the completed Adversary Requests for the dates noted above. 
        These are only the requests that were user OK'd to be reported in the Daily Memo. The requests are listed 
        from oldest to youngest. Existing clients are listed last.  </td>
    </tr>
    <tr>
        <td colspan="2"><hr /></td>
    </tr>
    <tr>
        <td colspan="2">
<%--            <asp:Repeater ID="rptDailyListMemosPotential" runat="server">
                <HeaderTemplate>
                    <table>
                    
                </HeaderTemplate>
                <ItemTemplate>
                        <tr>
                            <td valign="top">
                                <%#CreatePartiesString(Eval("ClientName").ToString())%>    
                            </td>
                            <td valign="top">
                                <%#Eval("PracticeCode")%>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                Related:<%#CreatePartiesString((Eval("RelatedParties") ?? "").ToString())%><br />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                Adverse:<%#CreatePartiesString((Eval("AdverseParties") ?? "").ToString())%><br />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                (<%#GetAttorneyName((Eval("AttyLogin") ?? "").ToString()).ToUpper()%>)
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                        </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            <asp:Repeater ID="rptDailyListMemosExisting" runat="server">
                <HeaderTemplate>
                    <table>
                        <tr>
                            <td colspan="2"><strong>SEARCHES DONE ON EXISTING CLIENTS</strong></td>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                        <tr>
                            <td valign="top">
                                <%#CreatePartiesString(Eval("ClientName").ToString())%>    
                            </td>
                            <td valign="top">
                                <%#Eval("PracticeCode")%>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                Related:<%#CreatePartiesString((Eval("RelatedParties") ?? "").ToString())%><br />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                Adverse:<%#CreatePartiesString((Eval("AdverseParties") ?? "").ToString())%><br />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                (<%#GetAttorneyName((Eval("AttyLogin") ?? "").ToString()).ToUpper()%>)
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                        </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
--%>
            <asp:Table ID="tblParties" runat="server"></asp:Table>
        </td>
    </tr>
</table>
</ContentTemplate>
</asp:UpdatePanel>



</asp:Content>


