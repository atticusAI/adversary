﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Net.Mail;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using HH;
using HH.Branding;
using HH.Screen;
using HH.UI.FileList;
using HH.UI.ModalPopupForm;
using System.Data.SqlClient;
using HH.SQL;
using System.Data;

public partial class AdminProcessing : AdminBasePage
{
    protected override void OnInit(EventArgs e)
    {
        _MasterPage.SideBarVisible = false;
        base.OnInit(e);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        _MasterPage.PostBackEvent += new MasterPagePostBackEventHandler(MasterPage_PostBackEvent);
        _MasterPage.PopupYesClick += new EventHandler(MasterPage_PopupConfirm);
        
        if (!ValidateMemo())
        {
            _MasterPage.UpperContent.Visible = false;
            _MasterPage.LowerContent.Visible = false;
            _btnComplete.Enabled = false;
            return;
        }

        CheckAdversaryLocking();   
        BindControls();
    }

    private void CheckAdversaryLocking()
    {
        if (_memo == null)
            return;

        AdversaryLocking locking = AdversaryLocking.Instance;
        
        if (!locking.HasLock(_memo.ScrMemID))
        {
            locking.AddLock(_memo.ScrMemID, new AdversaryLock(_memo.ScrMemID, _login.Login));
            return;
        }
        else
        {
            AdversaryLock al = locking.GetLock(_memo.ScrMemID);

            if (al.Login == _login.Login || !al.Locked)
                return;

            _MasterPage.PopupYesCommandArgument = "handleUnlock";
            _MasterPage.PopupNoCommandArgument = "redirectToQueue";
            _MasterPage.PopupNoClick += new EventHandler(MasterPage_PopupConfirm);
            _MasterPage.ShowPopupDialog("Memo " + _memo.ScrMemID + " is currently locked for edit by " + al.Login + ", would you like to unlock it?", MasterPagePopupDialogMode.ModalYesNo);
            SetFormState(false);
        }
    }

    private void BindControls()
    {
        if (!IsPostBack)
        {
            using (SqlConnection conn = AdversaryData.GetConnection())
            {
                DatabaseWorker worker = new DatabaseWorker(conn);
                DoDataLoad(_tracking, worker);
                DoDataLoad(_memo, worker);
            }
        }
        SetClientMatterNumberStatus();

        _cmList.DataSource = _memo.ClientMatterNumbers;
        _cmList.DataBind();

        _lbSend.Text = (_memo.ClientMatterNumbers.Count > 0) ? "Send Client Matter Numbers to Originating Party" : "Send Status Update to Orignating Party";

        if (IsPostBack) //following controls only bound on initial page load
            return;

        //set the A/R Balance if applicable
        if (this.Balance != null && this.Balance.TotalAR > 0)
        {
            _tdAR.Controls.Add(BuildARTable(this.Balance));
            _trAR.Visible = true;
        }

        //set the top form controls
        _MasterPage.BreadCrumbText = "Screening Memo Approval > " + _memo.GetMemoTypeDescription() + " (Memo #" + _memo.ScrMemID + ")";

        _lblClient.Text = GetClientDisplayName(_memo);
        ///commented by A Reimer 03/21/12 for 'Rejected to Modification Required' change
        //_lblMatter.Text = (_memo.ScrMemType == 6) ? "REJECTED MATTER" :  _memo.MatterName;
        // blmcgee@hollandhart.com 2014-01-10 
        // _lblMatter.Text = (_memo.ScrMemType == 6) ? "MATTER REQUIRES MODIFICATION" : _memo.MatterName;
        _lblMatter.Text = (_memo.ScrMemType == 6) ? "REJECTED MATTER" :  _memo.MatterName;

        if (_tracking.SubmittedOn.HasValue)
            _lblMatter.Text += "&nbsp;&nbsp;&nbsp;&nbsp;<span style=\"color: #68849F;\">(Submitted:&nbsp;&nbsp;" + _tracking.SubmittedOn.Value.ToString() + ")</span>";
        try
        {
            _lblAtty.Text = GetEmployeeDisplayName(_memo.AttEntering);
        }
        catch { }

        if (_memo.Confidential.HasValue && _memo.Confidential.Value)
        {
            _lblConfidential.Text = "THIS MEMO IS CONFIDENTIAL!";
            _lblConfidential.ForeColor = Color.Red;
        }
        else
            _lblConfidential.Text = "No";

        if (!string.IsNullOrEmpty(_tracking.Notes)) _tbNotes.Text = _tracking.Notes;
        //_lblNotes.Text = (string.IsNullOrEmpty(_tracking.Notes)) ? "<em>(empty)</em>" : _tracking.Notes;
        _tbOpened.Text = _tracking.Opened;
        _tbConflicts.Text = _tracking.Conflicts;
        _tbFeeSplits.Text = _tracking.FeeSplits;
        _tbFinalCheck.Text = _tracking.FinalCheck;

        if (!_tracking.AdversaryAdminApproved.HasValue || !_tracking.AdversaryAdminApproved.Value)
        {
            _lblAdvAdminApproval.Text = "Not Approved";
            _lblAdvAdminApproval.ForeColor = Color.Red;
            _MasterPage.FeedbackMessage = "This memo cannot be completed, it has not been approved by the adversary group leader.";
            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Error;
            _MasterPage.PopupYesCommandArgument = "advAdminNotice";
            _btnComplete.Enabled = false;
        }
        else
        {
            _lblAdvAdminApproval.Text = _tracking.AdversaryAdminSignature + " (" + _tracking.AdversaryAdminDate + ")";
            _lblAdvAdminApproval.ForeColor = Color.Green;
        }

        if (_tracking.StatusTypeID.HasValue && ((int)_tracking.StatusTypeID.Value) >= 200)
        {
            _MasterPage.FeedbackMessage = "This memo has been processed, the current status is " + _tracking.GetStatus();
            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Confirm;
            _btnComplete.Enabled = false;
        }
        if (_tracking.StatusTypeID == TrackingStatusType.Completed)
        {
            _MasterPage.FeedbackMessage = "This memo has been processed and completed!";
            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Confirm;
            _btnComplete.Enabled = false;
        }
            

        //bind combo boxes
        AdminLogins[] advs = AdminLogins.GetAdversaryGroup();
        BindApproverComboBox(_comboAdversary, advs, _tracking.AdversaryUserID, ApproverComboBoxType.Adv, _memo);

    }

    private void SetFormState(bool isEnabled)
    {
        _tbNotes.Enabled = isEnabled;
        _comboAdversary.Enabled = isEnabled;
        _tbOpened.Enabled = isEnabled;
        _tbConflicts.Enabled = isEnabled;
        _tbFeeSplits.Enabled = isEnabled;
        _tbFinalCheck.Enabled = isEnabled;
        //_upMain.Update();
    }

    private void SetClientMatterNumberStatus()
    {
        _lblCMTimeStamp.Visible = (_memo.ClientMatterNumbers.Count > 0 || _tracking.CMNumbersSentDate.HasValue);

        if (_lblCMTimeStamp.Visible)
        {
            if (!_tracking.CMNumbersSentDate.HasValue || _tracking.CMNumbersSentDate.Value == DateTime.MinValue)
            {
                _lblCMTimeStamp.Text = "Client Matter Numbers Have Not Been Sent<br/><br/>";
                _lblCMTimeStamp.ForeColor = Color.Red;
            }
            else
            {
                _lblCMTimeStamp.Text = "Client Matter Numbers Sent: " + _tracking.CMNumbersSentDate + "<br/><br/>";
                _lblCMTimeStamp.ForeColor = Color.Green;
            }
        }
    }
    
    
    private bool RefreshGlobals()
    {
        if (!_SessionData.MemoLoaded)
        {
            _MasterPage.ShowPopupDialog("Sorry, your session has expired. Please load this memo again and retry the last action.", MasterPagePopupDialogMode.Popup);
            return false;
        }

        Tracking tracking = _SessionData.MemoData.Tracking;
        tracking.Notes = _tbNotes.Text.Trim();
        tracking.Conflicts = _tbConflicts.Text.Trim();
        tracking.Opened = _tbOpened.Text.Trim();
        tracking.FeeSplits = _tbFeeSplits.Text.Trim();
        tracking.FinalCheck = _tbFinalCheck.Text.Trim();

        if (string.IsNullOrEmpty(tracking.AdversaryAdminSignature))
        {
            _lblAdvAdminApproval.Text = "None";
            _lblAdvAdminApproval.ForeColor = Color.Red;
        }
        else
        {
            _lblAdvAdminApproval.Text = _tracking.AdversaryAdminSignature + " (" + _tracking.AdversaryAdminDate + ")";
            _lblAdvAdminApproval.ForeColor = Color.Green;
        }
        
        
        _tracking = tracking;
        _login = _SessionData.AdminLogin;
        _memo = _SessionData.MemoData;
        return true;
    }

    #region EVENT HANDLERS

    void MasterPage_PopupConfirm(object sender, EventArgs e)
    {
        Button button = (Button)sender;
        switch (button.CommandArgument)
        {
            case "complete":
                CompleteMemo();
                Response.Redirect("AdminQueue.aspx");
                break;
            case "redirectToQueue":
                Response.Redirect("AdminQueue.aspx");
                break;
            case "handleUnlock":
                DoUnlock(false);
                break;
            case "handleUnlockDismiss":
                if (!RefreshGlobals())
                    break;
                SaveTrackingData(_tracking);
                DoUnlock(true);
                
                break;
        }
    }

    private void DoUnlock(bool acknowledged)
    {
        AdversaryLocking locking = AdversaryLocking.Instance;
        if (locking.HasLock(_memo.ScrMemID))
        {
            AdversaryLock al = locking.GetLock(_memo.ScrMemID);
            al.Unlock(_login.Login);
            al.Acknowledged = acknowledged;
        }
        else
        {
            AdversaryLock al = new AdversaryLock(_memo.ScrMemID, _login.Login);
            al.Unlock(_login.Login);
            al.Acknowledged = true;
            locking.AddLock(_memo.ScrMemID, al);
        }
        _timerLocking.Enabled = false;

        Response.Redirect(Request.Path);
    }

    bool MasterPage_PostBackEvent(object sender, MasterPagePostBackEventArgs e)
    {
        try
        {
            if (e.PostBackSource == MasterPagePostBackSource.PopupYes ||
                e.PostBackSource == MasterPagePostBackSource.PopupNo)
                return true;

            if (!RefreshGlobals())
                return true;

            SaveTrackingData(_tracking);

            if (e.RedirectUrl != Request.Path)
                AdversaryLocking.Instance.RemoveLock(_memo.ScrMemID);

            return true;
        }
        catch (Exception ex)
        {
            _MasterPage.ShowError(ex);
            return false;
        }
    }

    protected void combo_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!RefreshGlobals())
            return;

        ComboBox combo = (ComboBox)sender;

        string msg = string.Empty;
        bool unassigned = combo.SelectedValue == "unassigned";
        int? val = (unassigned) ? null : (int?)Int32.Parse(combo.SelectedValue);
        _tracking.AdversaryUserID = val;
        _tracking.AdversaryAcknowledged = !unassigned;
        _tracking.AdversaryAdminAcknowledged = !unassigned && _login.IsAdversaryAdmin.HasValue && _login.IsAdversaryAdmin.Value;
        msg = (unassigned) ? "Adversary Group Assignment Removed" : "Assigned to Adversary Group Member " + combo.SelectedItem.Text;

        SaveTrackingData(_tracking);

        _MasterPage.FeedbackMode = MasterPageFeedbackMode.Success;
        _MasterPage.FeedbackMessage = msg;
        _upMain.Update();
    }

    protected void timer_Tick(object sender, EventArgs e)
    {
        if (_memo == null || _memo.ScrMemID == null)
            return;

        AdversaryLocking locking = AdversaryLocking.Instance;
        AdversaryLock al = locking.GetLock(_memo.ScrMemID);
        if (al == null || al.Acknowledged)
        {
            _timerLocking.Enabled = false;
            return;
        }


        if (!al.Locked && !string.IsNullOrEmpty(al.UnlockedBy) && al.UnlockedBy != _login.Login)
        {
            _MasterPage.PopupYesCommandArgument = "handleUnlockDismiss";
            _MasterPage.ShowPopupDialog(al.UnlockedBy + " has removed your lock on this memo", MasterPagePopupDialogMode.ModalOK);
            _timerLocking.Enabled = false;
        }
    }

    #region APPROVE/COMPLETE/REJECT BUTTONS
    
    protected void btnComplete_Click(object sender, EventArgs e)
    {
        _MasterPage.FeedbackMode = MasterPageFeedbackMode.Warning;
        _MasterPage.ShowPopupDialog("Are you sure you would like to set this memo's status to complete and remove it from the queue?", MasterPagePopupDialogMode.ModalYesNo);
        _MasterPage.PopupYesCommandArgument = "complete";
    }

    protected void lbCMForm_Click(object sender, EventArgs e)
    {
        _modalCMForm.Show();
        _lblCMFormError.Text = string.Empty;
        _tbCM.Text = string.Empty;
    }

    private void CompleteMemo()
    {
        if (!RefreshGlobals())
            return;

        //_tracking.Opened = DateTime.Now;
        _tracking.StatusTypeID = TrackingStatusType.Completed;

        SaveTrackingData(_tracking);
        _upMain.Update();

        try
        {
            //SendTrackingEmail(_memo, false, true);

            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Success;
            _MasterPage.FeedbackMessage = "This memo has been marked as completed.";
        }
        catch (Exception ex)
        {
            HandleError(ex);
        }
    }

    #endregion

    #region LOWER MENU BUTTONS
    protected void lbSend_Click(object sender, EventArgs e)
    {
        if (!RefreshGlobals())
            return;

        SaveTrackingData(_tracking);
        //SendStatusUpdate();
        HandleStatusNotification(_memo);
        SetClientMatterNumberStatus();
    }
    #endregion

    #region MODAL FORM BUTTONS


    protected void cmList_OnEntitySubmitComplete(IListContainerControl sender, object dataSource, object entity, EntitySubmitType type)
    {
        if (!RefreshGlobals())
            return;

        HandleCMEntitySubmit((ClientMatterNumber)entity, type);

        BindControls();
        _upMain.Update();
    }

    protected void lbCMSubmit_Click(object sender, EventArgs e)
    {
        if (!RefreshGlobals())
            return;

        string num = _tbCM.Text.Trim();
        if (string.IsNullOrEmpty(num))
        {
            _lblCMFormError.Text = "Please enter a valid C/M Number to continue";
            _modalCMForm.Show();
            return;
        }
        _lblCMFormError.Text = string.Empty;

        ClientMatterNumber cm = new ClientMatterNumber();
        cm.ClientMatterID = 0;
        cm.ScrMemID = _memo.ScrMemID;
        cm.Number = _tbCM.Text.Trim();
        HandleCMEntitySubmit(cm, EntitySubmitType.Create);
        //HandleCMEntitySubmit((ClientMatterNumber)entity, type);
        
        BindControls();
        _upMain.Update();
    }

    #endregion

    

    #endregion
}
