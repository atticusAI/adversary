﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using HH;
using HH.Screen;
using HH.Branding;
using HH.UI.FileList;

public partial class Impersonate : AdminBasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnView_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(_acApprover.Text.Trim()))
        {
            _MasterPage.FeedbackMessage = "Please enter a name, employee ID or login for the AP, PGL, or Adversary group member you wish to view";
            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Error;
            return;
        }

        if (!_acApprover.Text.Contains(" - "))
        {
            string[] logins = GetAdminList(_acApprover.Text.Trim(), 1, "Admin");
            if (logins.Length != 1)
            {
                _MasterPage.FeedbackMessage = "'" + _acApprover.Text + "' is not a valid AP, PGL, or Adversary group member";
                _MasterPage.FeedbackMode = MasterPageFeedbackMode.Error;
                return;
            }
            _acApprover.Text = logins[0];
        }

        try
        {
            string login = _acApprover.Text.Trim();
            login = _acApprover.Text.Substring(_acApprover.Text.LastIndexOf(" - ") + 3);
            Response.Redirect("Queue.aspx?impersonate=" + login, false);
        }
        catch (Exception ex)
        {
            _MasterPage.FeedbackMessage = "An error occurred. Could not view queue for '" + _acApprover.Text + "'";
            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Error;
            _MasterPage.ShowError(ex);
        }
    }
}
