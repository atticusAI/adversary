﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Drawing;

using HH.Extensions;
using System.Data.SqlClient;
using HH.SQL;

public partial class QueueRepeater : System.Web.UI.UserControl
{
    private CMSData _cms = CMSData.GetInstance();

    public AdminLogins Login { get; set; }
    public List<Memo> DataSource { get; set; }
    

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public override void DataBind()
    {
        base.DataBind();

        repQueue.DataSource = this.DataSource;
        repQueue.DataBind();
    }

    protected void repQueue_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Memo memo = (Memo)e.Item.DataItem;
        bool noticeFlag = false;

        try
        {
            Table tableStatus = (Table)e.Item.FindControl("tableStatus");

            bool isRejectedMatter = memo.ScrMemType == 6;
            bool apApproved = memo.Tracking.APApproved.HasValue && memo.Tracking.APApproved.Value;
            bool apAcknowledged = memo.Tracking.APAcknowledged.HasValue && memo.Tracking.APAcknowledged.Value;
            bool apNoResponse = (!apApproved && !apAcknowledged) && (memo.RespAttOfficeName != "Denver") && !isRejectedMatter;

            bool pgmApproved = memo.Tracking.PGMApproved.HasValue && memo.Tracking.PGMApproved.Value;
            bool pgmAcknowledged = memo.Tracking.PGMAcknowledged.HasValue && memo.Tracking.PGMAcknowledged.Value;
            bool pgmNoResponse = (!pgmApproved && !pgmAcknowledged) && !isRejectedMatter;

            bool isAdversary = Login.IsAdversary.HasValue && Login.IsAdversary.Value;
            bool advApproved = memo.Tracking.AdversaryAdminApproved.HasValue && memo.Tracking.AdversaryAdminApproved.Value;
            bool advAcknowledged = memo.Tracking.AdversaryAdminAcknowledged.HasValue && memo.Tracking.AdversaryAdminAcknowledged.Value;
            bool advNoResponse = (!advApproved && !advAcknowledged);
            
            //header
            //SetLabel(e, "lblScrMemID", memo.ScrMemID.ToString());
            SetLabel(e, "lblClientName", AdminBasePage.GetClientDisplayName(memo));
            SetLabel(e, "lblMatterName", memo.MatterName);
            SetLabel(e, "lblMemoType", memo.GetMemoTypeDescription());
            //SetLabel(e, "lblOffice", _cms.GetOfficeName(memo.OrigOffice));

            if (memo.ScrMemType != 6)
            {
                string city = memo.RespAttOfficeName;
                if (string.IsNullOrEmpty(city))
                {
                    SetNotice(e, "Could not determine the originating office for this memo, the attorney listed (" + memo.RespAttID + ") does not resolve to a valid H&H employee ID", null);
                    noticeFlag = true;
                }
                else
                    SetLabel(e, "lblOffice", memo.RespAttOfficeName);
            }

            if (!string.IsNullOrEmpty(memo.PracCode))
            {
                try
                {
                    string pracName = _cms.GetPracticeName(memo.PracCode);
                    if (!noticeFlag && AdminLogins.GetPGMsByPracticeCode(memo.PracCode, true).Count() == 0)
                    {
                        SetNotice(e, "Could not determine the Primary PGL for this memo, a PGL has not been assigned for " + pracName + " (" + memo.PracCode + ")", null);
                        noticeFlag = true;
                    }
                    
                    SetLabel(e, "lblPracCode", pracName);
                }
                catch
                {
                    SetLabel(e, "lblPracCode", memo.PracCode);
                }
            }

            if (memo.Tracking.SubmittedOn.HasValue)
            {
                int days = 4;
                if (!noticeFlag && 
                    memo.Tracking.SubmittedOn.Value <= DateTime.Now.AddDays(-4))
                {
                    TimeSpan span = DateTime.Now - memo.Tracking.SubmittedOn.Value;
                    SetLabel(e, "lblDate", memo.Tracking.SubmittedOn.Value.ToString(), Color.Red);
                    SetNotice(e, "Memo was submitted over " + span.Days + " days ago", null);
                }
                else
                    SetLabel(e, "lblDate", memo.Tracking.SubmittedOn.Value.ToString());
                
            }

            SetLabel(e, "lblScrMemID", memo.ScrMemID.ToString());
            try
            {
                DataTable dt = _cms.GetUserDataByCode(memo.AttEntering);
                if (dt.Rows.Count > 0)
                    SetLabel(e, "lblAttEntering", dt.Rows[0][CMSData.EMP_LOGIN].ToString());
                else
                    SetLabel(e, "lblAttEntering", memo.AttEntering, Color.Red);

            }
            catch
            {
                SetLabel(e, "lblAttEntering", memo.AttEntering);
            }
            try
            {
                DataTable dt = _cms.GetUserDataByCode(memo.PersonnelID);
                if (dt.Rows.Count > 0)
                    SetLabel(e, "lblPersonnelID", dt.Rows[0][CMSData.EMP_LOGIN].ToString());
            }
            catch
            {
                SetLabel(e, "lblPersonnelID", memo.PersonnelID);
            }
            SetLabel(e, "lblWorkDesc", memo.WorkDesc);

            //System.Web.UI.WebControls.Image img = (System.Web.UI.WebControls.Image)e.Item.FindControl("imgStatus");
            //img.ImageUrl = Request.ApplicationPath +  "/_css/green_check.jpg";
            //img.AlternateText = "You have approved this memo. It will be removed from your queue after it is approved by additional parties.";
            //img.Visible = (apApproved && _login.IsAP.HasValue && _login.IsAP.Value) ||
            //              (pgmApproved && _login.IsPGM.HasValue && _login.IsPGM.Value);

            SetStatusTable(ref tableStatus, "AP", apApproved, apAcknowledged, apNoResponse, memo.Tracking.APDate);
            SetStatusTable(ref tableStatus, "PGL", pgmApproved, pgmAcknowledged, pgmNoResponse, memo.Tracking.PGMDate);
            SetStatusTable(ref tableStatus, "Adv", advApproved, advAcknowledged, advNoResponse, memo.Tracking.AdversaryAdminDate);

            if (!noticeFlag && memo.Tracking.RejectionHold.HasValue && memo.Tracking.RejectionHold.Value)
                ///commented by A Reimer 03/21/12 for 'Rejected to Modification Required' change
                //SetNotice(e, "This memo was rejected and has been resubmitted", Color.DarkOrange);
                SetNotice(e, "This memo requires modification and has been resubmitted", Color.DarkOrange);

            //Buttons need to be added at runtime for event to fire correctly
            PlaceHolder phSubmit = (PlaceHolder)e.Item.FindControl("phSubmit");
            Button btnSubmit = new Button();
            btnSubmit.ID = "btnSubmit___" + e.Item.ItemIndex;
            btnSubmit.Text = "View";
            btnSubmit.Width = Unit.Pixel(80);
            btnSubmit.CommandName = ((isAdversary) || (Login.UserID == memo.Tracking.APUserID) || (Login.UserID == memo.Tracking.PGMUserID)) ? "approve" : "summary";
            btnSubmit.SetDataItem(memo.ScrMemID);
            btnSubmit.OnClientClick = "this.value = 'Loading...';";
            phSubmit.Controls.Add(btnSubmit);
        }
        catch (Exception ex)
        {
            ((AdminBasePage)Page)._MasterPage.ShowError(ex);
        }
    }

    
    private void SetStatusTable(ref Table tableStatus, string typePrefix, bool approved, bool acknowledged, bool noResponse, DateTime? date)
    {
        if (approved)
        {
            try
            {
                string timestamp = (!date.HasValue) ? string.Empty : date.Value.Month + "/" + date.Value.Day; //date.Value.ToShortDateString();// +" " + date.Value.Hour + ":" + date.Value.Minute;

                TableRow tr = new TableRow();
                TableCell td = new TableCell();

                td.Text = typePrefix + " Approved";
                td.ForeColor = Color.Green;
                tr.Cells.Add(td);

                td = new TableCell();
                td.Text = timestamp;
                td.ForeColor = Color.Green;
                tr.Cells.Add(td);

                tableStatus.Rows.Add(tr);
            }
            catch
            {
                TableRow tr = new TableRow();
                TableCell td = new TableCell();

                td.ColumnSpan = 2;
                td.Text = typePrefix + " Approved";
                td.ForeColor = Color.Green;
                tr.Cells.Add(td);

                tableStatus.Rows.Add(tr);
            }
        }
        else if (acknowledged)
        {
            TableRow tr = new TableRow();
            TableCell td = new TableCell();

            td.ColumnSpan = 2;
            td.Text = typePrefix + " Acknowledged";
            td.ForeColor = Color.Blue;
            tr.Cells.Add(td);

            tableStatus.Rows.Add(tr);
        }
        else if (noResponse)
        {
            TableRow tr = new TableRow();
            TableCell td = new TableCell();

            td.ColumnSpan = 2;
            td.Text = "No " + typePrefix + " Response";
            td.ForeColor = Color.Red;
            tr.Cells.Add(td);

            tableStatus.Rows.Add(tr);
        }
    }

    private void SetNotice(RepeaterItemEventArgs e, string text, Color? color)
    {
        try
        {
            SetLabel(e, "lblNotice", "* " + text, color);
            ((HtmlTableRow)e.Item.FindControl("rowError")).Visible = true;
            ((HtmlGenericControl)e.Item.FindControl("divRepeaterBox")).Style.Add("border-color", (color.HasValue) ? color.Value.Name.ToLower() : "red");
        }
        catch { }
    }



    private void SetLabel(RepeaterItemEventArgs e, string id, string text) { SetLabel(e, id, text, null); }
    private void SetLabel(RepeaterItemEventArgs e, string id, string text, Color? foreColor)
    {
        try
        {
            Label lbl = (Label)e.Item.FindControl(id);
            lbl.Text = text;
            lbl.Visible = true;
            if (foreColor.HasValue)
                lbl.ForeColor = foreColor.Value;
        }
        catch { }
    }

    protected void repQueue_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        Button btn = (Button)e.CommandSource;
        int scrMemId = (int)btn.GetDataItem();
        Memo memo;

        using (SqlConnection conn = AdversaryData.GetConnection())
        {
            DatabaseWorker worker = new DatabaseWorker(conn);
            memo = worker.RetrieveSingle<Memo>(scrMemId, "PR_GET_MEMO");
            //bool isAP = (memo.OrigOffice != "10") && _login.IsAPForOffice(memo.OrigOffice);
            bool isAP = false;
            bool isPGM = false;
            try
            {
                //TODO: temp fix for Carson City, remove this when multiple APs allowed
                if (memo.RespAttOfficeName.ToLower().Contains("carson"))
                    isAP = Login.IsAPForOffice("Reno");
                else
                    isAP = (memo.RespAttOfficeName != "Denver") && Login.IsAPForOffice(memo.RespAttOfficeName);
            }
            catch { }
            try
            {
                isPGM = Login.IsMemberOfPracticeGroup(memo.PracCode);
            }
            catch { }
            bool isAdvAdmin = Login.IsAdversaryAdmin.HasValue && Login.IsAdversaryAdmin.Value;
            bool isAdmin = Login.IsAdversary.HasValue && Login.IsAdversary.Value;

            //acknowledge for AP
            if (isAP)
            {
                if (!memo.Tracking.APUserID.HasValue)
                    memo.Tracking.APUserID = Login.UserID;
                memo.Tracking.APAcknowledged = true;
            }

            //acknowledge PGM
            if (isPGM)
            {
                if (!memo.Tracking.PGMUserID.HasValue)
                    memo.Tracking.PGMUserID = Login.UserID;
                memo.Tracking.PGMAcknowledged = true;
            }

            //acknowledge for Adversary group
            if (isAdvAdmin) //admin acknowledge
            {
                memo.Tracking.AdversaryAdminAcknowledged = true;
            }
            //or acknowledge for adversary group member if processing memo
            else if (isAdmin && (memo.Tracking.StatusTypeID == TrackingStatusType.AdversaryGroupProcessing))
            {
                if (!memo.Tracking.AdversaryUserID.HasValue)
                    memo.Tracking.AdversaryUserID = Login.UserID;
                memo.Tracking.AdversaryAcknowledged = true;
            }

            ((AdminBasePage)Page).DoDataSave(memo.Tracking, worker);
            ((AdminBasePage)Page)._SessionData.MemoData = memo;
            ((AdminBasePage)Page)._SessionData.MemoLoaded = true;
            if (((AdminBasePage)Page)._MasterPage.SiteMapProvider.Name == "Main")
                ((AdminBasePage)Page).ToggleMenuEnabled(true);

            conn.Close();
        }

        

        switch (btn.CommandName)
        {
            case "approve":
                Response.Redirect("Approval.aspx?ScrMemID=" + memo.ScrMemID, false);
                break;
            case "summary":
                Response.Redirect("~/Screening/Summary.aspx?type=approve&ScrMemType=" + memo.ScrMemType + "&ScrMemID=" + memo.ScrMemID, false);
                break;
        }
    }
}
