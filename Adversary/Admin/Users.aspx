﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Users.aspx.cs" Inherits="Users" MasterPageFile="~/Master.master" %>
<%@ Register TagPrefix="UC" Assembly="HH" Namespace="HH.UI.ModalPopupForm" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register TagName="Screen" TagPrefix="UC" Src="~/_usercontrols/Screen.ascx" %>

<asp:Content ID="Content" ContentPlaceHolderID="UpperContentPlaceHolder" runat="server">

<asp:UpdatePanel ID="upMain" runat="server" >
    <ContentTemplate>
    
        <asp:GridView ID="gvUserList" runat="server" 
                            AutoGenerateColumns="false" 
                            GridLines="None"
                            BorderStyle="solid" 
                            BorderWidth="1"
                            BorderColor="#F0F0F0"
                            RowStyle-BackColor="#F0F0F0" 
                            AlternatingRowStyle-BackColor="White"
                            RowStyle-Height="30px"
                            Width="95%" >
            <Columns>
                
                <asp:BoundField HeaderText="User Name" DataField="Login" />
                <asp:BoundField HeaderText="Email" DataField="Email" />
                <asp:TemplateField HeaderText="Office">
                    <ItemTemplate><%# GetOfficeName(Eval("Office")) %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Adversary Group" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle">
                    <ItemTemplate>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%# GetAdvStatus(Eval("IsAdversary"), Eval("IsAdversaryAdmin"))%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="AP" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle">
                    <ItemTemplate><%# GetAPStatus(Eval("IsAP"), Eval("IsPrimaryAP"))%></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="PGL" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle">
                    <ItemTemplate><img src="<%# GetCheckboxImage(Eval("IsPGM"))%>" alt="Is PGM?" /></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Practice Groups" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="Left"  ItemStyle-VerticalAlign="Middle">
                    <ItemTemplate><%# GetPracticeGroupList(Eval("AdminPracCodes"))%></ItemTemplate>
                </asp:TemplateField>
            
            </Columns>
        
        </asp:GridView>
        <div style="text-align: right;">(<span style="color: DarkOrange;">P</span> = Primary <span style="color: DarkOrange;">S</span> = Secondary)</div>
        <asp:LinkButton ID="lbAdd" runat="server" OnClick="lbAdd_Click" Text="Add New User" Font-Bold="true"></asp:LinkButton>
        
        <UC:ModalForm ID="modalForm" runat="server" CancelControlID="btnClose" Width="60%" Y="15">
    
            <asp:UpdatePanel ID="upModalForm" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="pnlWrapper" runat="server" Height="525px" ScrollBars="Vertical">        
                    
                        <div style="width: 90%">
                            <asp:Label ID="lblUserHeader" runat="server" Font-Bold="true" ForeColor="#68849F"></asp:Label><br />
                            <table style="text-align: left; white-space: nowrap; width: 100%;">
                                <tr>
                                    <td colspan="3" style="padding: 5px 2px 5px 2px;">
                                        <asp:Label ID="lblError" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label><br />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 125px;" >User ID</td>
                                    <td colspan="2" style="padding: 5px 2px 5px 2px;"><asp:Label ID="lblUserID" runat="server" Font-Bold="true"></asp:Label></td>
                                </tr>
                                <tr >
                                    <td >User Name</td>
                                    <td colspan="2" style="padding: 5px 2px 5px 2px">
                                        <asp:PlaceHolder ID="phLogin" runat="server"></asp:PlaceHolder>
                                    </td>
                                </tr>
                                <tr >
                                    <td >Email</td>
                                    <td colspan="2" style="padding: 5px 2px 5px 2px">
                                        <asp:TextBox ID="tbEmail" runat="server" CssClass="sbControl"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr >
                                    <td >Office</td>
                                    <td colspan="2" style="padding: 5px 2px 5px 2px">
                                        <asp:DropDownList ID="ddlOffice" runat="server" CssClass="sbControl"></asp:DropDownList>
                                    </td>
                                </tr>
                                <asp:Panel ID="pnlUserRoles" runat="server">
                                
                                    <tr>
                                        <td style="padding: 5px 2px 5px 2px;">User is in Adversary Group?</td>
                                        <td style="padding: 5px 2px 5px 2px; width: 40px;">
                                            <asp:CheckBox ID="cbAdv" runat="server" OnCheckedChanged="cbAdv_OnCheckedChanged" AutoPostBack="true" />
                                        </td>
                                        <td style="margin: 5px 2px 5px 2px; text-align: left">
                                            <asp:Panel ID="pnlAdvAdmin" runat="server" Visible="false" Wrap="false">
                                                Is Adversary Group Admin:&nbsp;&nbsp;<asp:CheckBox ID="cbAdvAdmin" runat="server" />
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>User is AP?</td>
                                        <td style="padding: 5px 2px 5px 2px; text-align: left;">
                                            <asp:CheckBox ID="cbAP" runat="server" OnCheckedChanged="cbAP_OnCheckedChanged" AutoPostBack="true"/>
                                        </td>
                                        <td style="margin: 5px 2px 5px 2px; text-align: left;">
                                            <asp:Panel ID="pnlAPType" runat="server" Visible="false" Wrap="false">
                                                AP Type:&nbsp;&nbsp;
                                                <asp:DropDownList ID="ddlAPType" runat="server" CssClass="sbControl" Width="90px">
                                                    <asp:ListItem Text="Primary" Value="primary"></asp:ListItem>
                                                    <asp:ListItem Text="Secondary" Value="secondary"></asp:ListItem>
                                                </asp:DropDownList>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="">User is PGL?</td>
                                        <td style="padding: 5px 2px 5px 2px;">
                                            <asp:CheckBox ID="cbPGM" runat="server" OnCheckedChanged="cbPGM_OnCheckedChanged" AutoPostBack="true" />
                                        </td>
                                        <td >
                                            <asp:Panel ID="pnlPGMPracCodes" runat="server" Visible="false">
                                                <br>
                                                <asp:Repeater ID="repPGMPracCodes" runat="server"></asp:Repeater><br />
                                                <hr width="100%" />
                                                <br />
                                                Add Practice Group: <asp:DropDownList ID="ddlAddPracCode" runat="server" CssClass="sbControl"></asp:DropDownList>&nbsp;&nbsp;
                                                Type:&nbsp;&nbsp;
                                                <asp:DropDownList ID="ddlPGMType" runat="server"  Width="90px" CssClass="sbControl">
                                                    <asp:ListItem Text="Primary" Value="primary"></asp:ListItem>
                                                    <asp:ListItem Text="Secondary" Value="secondary"></asp:ListItem>
                                                </asp:DropDownList>&nbsp;&nbsp;
                                                <asp:Button ID="btnPGMAddType" runat="server" Text="Add" Font-Size="Larger" OnClick="btnPGMAddType_Click" />
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                
                                </asp:Panel>
                                
                            </table>
                    
                        </div>
                        
                    </asp:Panel>
                       
                       
                        
                    <div style="text-align:center; height: 65px; vertical-align: center; " >
                        <br />
                        <asp:Button ID="btnCreate" runat="server" Text="Create User" OnClick="btnSubmitForm_Click" CommandName="Create" />
                        <asp:Button ID="btnSave" runat="server"  Text="Save" OnClick="btnSubmitForm_Click" CommandName="Save" />&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btnClose" runat="server" Text="Close" OnClick="btnClose_Click" />
                        <br />
                    </div>
                         
                            
                </ContentTemplate>
                
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnCreate" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnClose" EventName="Click" />
                    
                    <asp:AsyncPostBackTrigger ControlID="repPGMPracCodes" EventName="ItemCommand" />
                    <asp:AsyncPostBackTrigger ControlID="btnPGMAddType" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="cbAdv" EventName="CheckedChanged" />
                    <asp:AsyncPostBackTrigger ControlID="cbAP" EventName="CheckedChanged" />
                    <asp:AsyncPostBackTrigger ControlID="cbPGM" EventName="CheckedChanged" />
                </Triggers>
            </asp:UpdatePanel>
        
        </UC:ModalForm>
        <script type="text/javascript" language="javascript">
//            function pageLoad()
//            {
//                $find('_modalForm').add_shown(onModalShow);
//            }
//            function onModalShow(sender,args)
//            {
//                $common.setLocation($get("<%=pnlWrapper.ClientID %>"),new Sys.UI.Point(888800 ,377700));
//            }

        </script>

    </ContentTemplate>
    
</asp:UpdatePanel>
    
    
</asp:Content>