﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="AdminQueue.aspx.cs" Inherits="AdminQueue" MasterPageFile="~/Master.master" %>
<%@ Register TagPrefix="UC" Assembly="HH" Namespace="HH.UI.ModalPopupForm" %>
<%@ Register TagPrefix="UC" TagName="QRefresh" Src="~/Admin/_QueueRefresh.ascx" %>

<asp:Content ID="Content" ContentPlaceHolderID="UpperContentPlaceHolder" runat="server">

<asp:UpdatePanel ID="upQueue" runat="server">

    <ContentTemplate>
        <asp:Label ID="_lblCount" runat="server" Font-Bold="true"></asp:Label><br />
        <br />
        
        <asp:GridView ID="_gvResults" runat="server" 
                AutoGenerateColumns="false" 
                GridLines="None"
                BorderStyle="solid" 
                BorderWidth="1"
                BorderColor="#F0F0F0"
                RowStyle-BackColor="#F0F0F0" 
                AlternatingRowStyle-BackColor="White"
                RowStyle-Height="30px" 
                Width="98%" Font-Size="9pt">
        <Columns>
            
            <asp:TemplateField HeaderText="" ItemStyle-Width="70px">
                <ItemTemplate>
                        <span style="padding-left: 10px;">
                            <asp:LinkButton ID="_lbView" runat="server" Text="View    " OnClientClick="this.innerHTML = 'Loading...';" OnClick="lbView_Click" />
                        </span>
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Memo" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate><%# GetBoundContents(Container, AdminQueueDataField.ScrMemID)%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Matter Name" >
                <ItemTemplate><%# GetBoundContents(Container, AdminQueueDataField.MatterName)%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Client"   >
                <ItemTemplate><%# GetBoundContents(Container, AdminQueueDataField.ClientNumber)%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Attorney"  >
                <ItemTemplate><%# GetBoundContents(Container, AdminQueueDataField.AttEntering)%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Entering Employee" >
                <ItemTemplate><%# GetBoundContents(Container, AdminQueueDataField.PersonnelID)%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Fee Split?"  ItemStyle-HorizontalAlign="Center">
                <ItemTemplate><%# GetBoundContents(Container, AdminQueueDataField.FeeSplit)%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="AP Approval" >
                <ItemTemplate><%# GetBoundContents(Container, AdminQueueDataField.AP)%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="PGL Approval" >
                <ItemTemplate><%# GetBoundContents(Container, AdminQueueDataField.PGM)%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Adversary Approval">
                <ItemTemplate><%# GetBoundContents(Container, AdminQueueDataField.AdversaryAdmin)%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="CM #'s Sent" >
                <ItemTemplate><%# GetBoundContents(Container, AdminQueueDataField.CMNumbersSentDate)%></ItemTemplate>
            </asp:TemplateField>
        
        </Columns>
    
    </asp:GridView>
    
    <UC:QRefresh ID="_refreshControl" runat="server"></UC:QRefresh>
    
                    
    </ContentTemplate>
    
    <Triggers>
        
    </Triggers>
    
</asp:UpdatePanel>

    
</asp:Content>