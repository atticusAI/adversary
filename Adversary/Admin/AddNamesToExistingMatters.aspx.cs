﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script;
using System.Web.Script.Services;
using System.Web.Services;
using HH.UI.AutoComplete;
using HH.Branding;
using HH.UI;
using AjaxControlToolkit;
using AjaxControlToolkit.Design;


public partial class Admin_AddNamesToExistingMatters : AdminBasePage
{
    private Button btnSubmit;
    private FileUpload fileUploader;
    private Table tblForm;
    private TextBox tbComments;
    private TextBox tbDate;
    private string[] allowedFileExtensions = new string[] { "pdf", "doc" };
    private string PageMode = "";
    private List<AdditionalName> _AdditionalNames;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        gvAdditionalNames.RowDataBound += new GridViewRowEventHandler(gvAdditionalNames_RowDataBound);
        pnlViewEntries.Visible = true;
        pnlAddNames.Visible = false;
        if (PageMode != "Edit")
        {
            using (AdversaryDataContext aData = new AdversaryDataContext())
            {
                _AdditionalNames = aData.PR_SEARCH_ADDITIONALNAMES(null,
                    null, null, null, null).ToList<AdditionalName>();
                gvAdditionalNames.DataSource = _AdditionalNames;
                gvAdditionalNames.DataBind();
            }
            calEndDate.SelectedDate = DateTime.Today.Date;
            AdminLogins[] advs = AdminLogins.GetAdversaryGroup();
            ddlAdversaryMembers.DataSource = advs;
            ddlAdversaryMembers.DataTextField = "Name";
            ddlAdversaryMembers.DataValueField = "EmployeeID";
            ddlAdversaryMembers.DataBind();
        }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        tblForm = new Table();
        TableRow tr = new TableRow();
        TableCell tc = new TableCell();

        //add client number search box
        AutoCompleteField searchBox = BuildClientSearchAutoComplete("sbClientNumber", "ClientNumber", "");
        searchBox.Clear += new AutoCompleteFieldCleared(ClientSearchClear);
        searchBox.SearchRequest += new AutoCompleteSearchRequest(ClientSearchSelected);

        tc = new TableCell();
        tc.Text = "Enter Client/Matter Number";
        tr.Cells.Add(tc);

        tc = new TableCell();
        tc.Controls.Add(searchBox);
        tr.Cells.Add(tc);
        tblForm.Rows.Add(tr);


        //add upload box
        tr = new TableRow();
        tc = new TableCell();

        Literal litFileUploaderLabel = new Literal();
        litFileUploaderLabel.Text = "Upload Conflicts File:";
        litFileUploaderLabel.ID = "litFileUploaderLabel";
        tc.Controls.Add(litFileUploaderLabel);
        tr.Cells.Add(tc);
        tc = new TableCell();

        this.fileUploader = new FileUpload();
        this.fileUploader.ID = "fileUploader";
        tc.Controls.Add(this.fileUploader);
        tr.Cells.Add(tc);

        tblForm.Rows.Add(tr);

        //add comments box
        tr = new TableRow();
        tc = new TableCell();
        tc.Text = "Add Comments:";
        tr.Cells.Add(tc);
        tc = new TableCell();
        tbComments = new TextBox();
        tbComments.TextMode = TextBoxMode.MultiLine;
        tbComments.Columns = 40;
        tbComments.Rows = 7;
        tbComments.ID = "tbComments";
        tc.Controls.Add(tbComments);
        tr.Cells.Add(tc);
        tblForm.Rows.Add(tr);

        //add date box
        tr = new TableRow();
        tc = new TableCell();
        tc.Text = "Date";
        tr.Cells.Add(tc);
        tc = new TableCell();
        tbDate = new TextBox();
        tbDate.ID = "tbDate";
        tbDate.TextMode = TextBoxMode.SingleLine;

        AjaxControlToolkit.CalendarExtender calExtDate = new CalendarExtender();
        calExtDate.TargetControlID = "tbDate";
        calExtDate.SelectedDate = DateTime.Today;
        calExtDate.PopupPosition = CalendarPosition.BottomLeft;

        tc.Controls.Add(tbDate);
        tc.Controls.Add(calExtDate);
        tr.Cells.Add(tc);
        tblForm.Rows.Add(tr);


        //add submit button
        tr = new TableRow();
        tc = new TableCell();
        tc.ColumnSpan = 2;
        btnSubmit = new Button();
        btnSubmit.Click += new EventHandler(btnSubmit_Click);
        btnSubmit.ID = "btnSubmit";
        btnSubmit.Text = "Submit Additional Names";
        tc.Controls.Add(btnSubmit);

        tr.Cells.Add(tc);
        tblForm.Rows.Add(tr);

        pnlAddNames.Controls.Add(tblForm);

        HiddenField hidEmployeeID = new HiddenField();
        hidEmployeeID.ID = "hidEmployeeID";
        hidEmployeeID.Value = _SessionData.AdminLogin.EmployeeID;
        pnlAddNames.Controls.Add(hidEmployeeID);
    }
        
    private AutoCompleteFieldMode ClientSearchClear(IAutoCompleteField sender)
    {
        _MasterPage.FeedbackMessage = string.Empty;
        
        return AutoCompleteFieldMode.ShowSearchBox;
    }

    private AutoCompleteFieldMode ClientSearchSelected(IAutoCompleteField sender, string text)
    {
        string number = ParseSearchRequestTextForNumber(text);
        if (string.IsNullOrEmpty(number))
        {
            _MasterPage.FeedbackMessage = "Client '" + text + "' was not found";
            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Warning;
            return AutoCompleteFieldMode.ShowSearchBox;
        }
        
        return AutoCompleteFieldMode.ShowResults;
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        //clear out the message
        lblMessage.Text = "";

        string _clientNumber = ((AutoCompleteField)this.tblForm.FindControl("sbClientNumber")).Text;
        _clientNumber = _clientNumber.Substring(_clientNumber.Length - 6, 5);


        string _fileName = ((FileUpload)this.tblForm.FindControl("fileUploader")).FileName.ToString();
        string _comments = ((TextBox)this.tblForm.FindControl("tbComments")).Text;
        string _date = ((TextBox)this.tblForm.FindControl("tbDate")).Text;
        string _employeeID = ((HiddenField)this.tblForm.FindControl("hidEmployeeID")).Value;

        using (AdversaryDataContext advDC = new AdversaryDataContext())
        {
            try
            {
                AdditionalName _AName = new AdditionalName();
                _AName.ClientNumber = _clientNumber;
                _AName.FileName = _fileName;
                _AName.Comments = _comments;
                _AName.UploadDate = DateTime.Parse(_date);
                _AName.UploaderPersonID = _employeeID;
                advDC.AdditionalNames.InsertOnSubmit(_AName);


                //if the extension is not pdf or doc
                if (!this.allowedFileExtensions.Contains(_fileName.Substring(_fileName.Length - 3, 3)))
                {
                    lblMessage.Text += "The uploaded document is not of the appropriate type";
                    return;
                }


                //upload the file to the destination directory
                string _fileUploadDirectory = System.Configuration.ConfigurationManager.AppSettings["FileUploadDirectory"].ToString() + "\\" + _clientNumber;
                if (this.fileUploader.HasFile)
                {
                    if (!System.IO.Directory.Exists(_fileUploadDirectory))
                    {
                        System.IO.Directory.CreateDirectory(_fileUploadDirectory);
                    }
                    fileUploader.SaveAs(_fileUploadDirectory + "\\" + fileUploader.FileName);
                }

                advDC.SubmitChanges();
                lblMessage.Text = "Successful upload of additional names document";
            }
            catch (Exception ex)
            {
                lblMessage.Text = "The application encountered an error processing your request: " + ex.Message;
            }
            
        }
    }

    protected void btnFilter_OnClick(object sender, EventArgs e)
    {
        DateTime? _startDate = null;
        DateTime? _endDate = null;
        if (tbStartDate.Text.Length > 0) _startDate = Convert.ToDateTime(tbStartDate.Text);
        if (tbEndDate.Text.Length > 0) _endDate = Convert.ToDateTime(tbEndDate.Text);

        using (AdversaryDataContext adata = new AdversaryDataContext())
        {
            _AdditionalNames = adata.PR_SEARCH_ADDITIONALNAMES(null, (tbClientNumberFilter.Text.Length > 0 ? tbClientNumberFilter.Text : null),
                _startDate, _endDate, (ddlAdversaryMembers.SelectedIndex > 0 ? ddlAdversaryMembers.SelectedValue : null)).ToList<AdditionalName>();
        }
        gvAdditionalNames.DataSource = _AdditionalNames;
        gvAdditionalNames.DataBind();
    }

    #region gridview event handlers
    void gvAdditionalNames_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CheckBox c = (CheckBox)e.Row.Cells[5].FindControl("cbComplete");
            AdditionalName aName = (AdditionalName)e.Row.DataItem;
            c.Checked = (aName.IsComplete ?? false);

            HyperLink hl = (HyperLink)e.Row.Cells[4].FindControl("hlDownloadFile");
            hl.NavigateUrl = String.Format("DownloadAdditionalNamesFile.aspx?clientID={0}&filename={1}", aName.ClientNumber, aName.FileName);
            hl.Text = aName.FileName;

        }
        if (e.Row.Cells[0].Visible == true) e.Row.Cells[0].Visible = false;
    }

    protected void cbComplete_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox cbComplete = (CheckBox)sender;
        GridViewRow row = (GridViewRow)cbComplete.NamingContainer;

        string additionalNameID = row.Cells[0].Text;
        bool status = cbComplete.Checked;

        using (AdversaryDataContext dc = new AdversaryDataContext())
        {
            AdditionalName a = (from Names in dc.AdditionalNames
                                where Names.AdditionalNameID == int.Parse(additionalNameID)
                                select Names).FirstOrDefault();
            if (a != null)
            {
                a.IsComplete = status;
                dc.SubmitChanges();
            }
        }

    }
    #endregion
}

