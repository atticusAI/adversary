﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="AddNamesToExistingMatters.aspx.cs" Inherits="Admin_AddNamesToExistingMatters" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Src="~/_usercontrols/FileListControl.ascx" TagName="fileListControl" TagPrefix="UC"%>

<asp:Content ID="Content2" ContentPlaceHolderID="UpperContentPlaceHolder" Runat="Server">
   <asp:Label ID="lblMessage" runat="server" />
   <asp:Panel ID="pnlViewEntries" runat="server">
        <table>
            <tr>
                <td>Input by:</td>
                <td><asp:DropDownList ID="ddlAdversaryMembers" runat="server" CssClass="entityEditEnabledControl" /></td>
                <td>Start Date:</td>
                <td>
                    <asp:TextBox ID="tbStartDate" runat="server" CssClass="entityEditEnabledControl" />
                    <ajax:CalendarExtender ID="calStartDate" runat="server"
                        PopupPosition="BottomLeft"
                         TargetControlID="tbStartDate"
                        Format="MM/dd/yyyy" />
                </td>
                <td>End Date:</td>
                <td>
                    <asp:TextBox ID="tbEndDate" runat="server" CssClass="entityEditEnabledControl" />
                    <ajax:CalendarExtender ID="calEndDate" runat="server"
                        PopupPosition="BottomLeft"
                        Format="MM/dd/yyyy"
                        TargetControlID="tbEndDate" />
                </td>
            </tr>
            <tr>
                <td>Client Number:</td>
                <td>
                    <asp:TextBox ID="tbClientNumberFilter" runat="server" CssClass="entityEditEnabledControl" />
                </td>
                <td>
                    <asp:Button ID="btnFilter" runat="server" Text="Filter" OnClick="btnFilter_OnClick" CssClass="entityEditEnabledControl" />
                </td>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="6">
                    <asp:GridView ID="gvAdditionalNames" runat="server" AutoGenerateColumns="false" AllowPaging="true">
                        <Columns>
                            <asp:BoundField DataField="AdditionalNameID" />
                            <asp:BoundField DataField="ClientNumber" HeaderText="Client #" HeaderStyle-CssClass="gridViewHeader"
                                ItemStyle-CssClass="gridViewItem" />
                            <asp:BoundField DataField="UploadDate" DataFormatString="{0:MM/dd/yyyy}" HeaderStyle-CssClass="gridViewHeader"
                                ItemStyle-CssClass="gridViewItem" HeaderText="Upload Date" />
                            <asp:BoundField DataField="Comments" HeaderText="Comments" HeaderStyle-CssClass="gridViewHeader"
                                ItemStyle-CssClass="gridViewItem" />
                            <asp:TemplateField HeaderText="File Name" HeaderStyle-CssClass="gridViewHeader" ItemStyle-CssClass="gridViewItem">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hlDownloadFile" runat="server" />
                                </ItemTemplate>
                           </asp:TemplateField>
                           <asp:TemplateField HeaderText="Complete?" HeaderStyle-CssClass="gridViewHeader" ItemStyle-CssClass="gridViewRow">
                            <ItemTemplate>
                                <asp:CheckBox ID="cbComplete" runat="server" AutoPostBack="true" OnCheckedChanged="cbComplete_CheckedChanged" />
                            </ItemTemplate>
                           </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
        
   </asp:Panel>
   <asp:Panel ID="pnlAddNames" runat="server" />
   <asp:Literal ID="litName" runat="server" />
</asp:Content>


