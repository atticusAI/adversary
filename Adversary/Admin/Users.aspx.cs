﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using HH.Branding;
using HH.Extensions;
using HH.SQL;
using HH.UI.AutoComplete;

public partial class Users : AdminBasePage
{
    private readonly string CACHE_KEY = "pracCodesLogin";
    private static readonly string TB_LOGIN_SEARCH_MSG = " (Type user name, login or employee # to search) ";

    private AutoCompleteField _sbLoginSelect;
    //private AjaxControlToolkit.ModalPopupExtender _modal;
    //private DatabaseWorker _worker;

    private class PracCodeTemplate : ITemplate
    {
        Users _page;
        AdminLogins _login;

        public PracCodeTemplate(Users page, AdminLogins login)
        {
            _page = page;
            _login = login;
        }

        #region ITemplate Members

        public void InstantiateIn(Control container)
        {
            container.DataBinding += new EventHandler(container_DataBinding);
        }

        private void container_DataBinding(object sender, EventArgs e)
        {
            RepeaterItem item = (RepeaterItem)sender;
            AdminPracCodes pracCode = (AdminPracCodes)item.DataItem;
            CMSData cms = CMSData.GetInstance();

            LinkButton lb = new LinkButton();
            lb.ID = "btnRemove___" + _login.UserID;
            lb.Text = "Remove";
            lb.Click += new EventHandler(lbRemove_Click);
            lb.SetDataItem(pracCode);
            item.Controls.Add(lb);

            string html = "&nbsp;&nbsp;&nbsp;" + cms.GetPracticeName(pracCode.PracCode) + " (" + pracCode.PracCode + ")&nbsp;&nbsp;&nbsp;";
            html += (pracCode.IsPrimary != null && (bool)pracCode.IsPrimary) ? "Primary" : "Secondary";
            html += "<br/>";
            item.Controls.Add(new LiteralControl(html));
        }

        private void lbRemove_Click(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            AdminPracCodes pracCode = (AdminPracCodes)lb.GetDataItem();

            using (SqlConnection conn = AdversaryData.GetConnection())
            {
                DatabaseWorker worker = new DatabaseWorker(conn);
                worker.Delete(pracCode);
            }
            _login.AdminPracCodes.Remove(pracCode);
            _page.modalForm.Show();
            _page._sbLoginSelect.Mode = AutoCompleteFieldMode.ShowResultsAndDisable;
            _page.BindLists(false);
        }

        #endregion
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        _MasterPage.ScriptManager.ScriptMode = ScriptMode.Release;
        //_worker = AdversaryData.GetInstance();

        if (!IsPostBack)
        {
            Session.Remove(CACHE_KEY);

            CMSData cms = CMSData.GetInstance();
            ddlOffice.DataSource = cms.AllOffices;
            ddlOffice.DataTextField = CMSData.OFFICE_DESCRIPTION;
            ddlOffice.DataValueField = CMSData.OFFICE_CODE;
            ddlOffice.DataBind();
            ddlOffice.SelectedIndex = 0;
        }

        gvUserList.RowCreated += new GridViewRowEventHandler(gvUserList_RowCreated);
        BindLists(!IsPostBack);

        _sbLoginSelect = new AutoCompleteField("sbLoginSelect", "GetLoginList", "Login");
        _sbLoginSelect.TextBoxSearchMessage = TB_LOGIN_SEARCH_MSG;
        _sbLoginSelect.ClearButtonText = "Clear Login";
        _sbLoginSelect.SearchRequest += new AutoCompleteSearchRequest(SearchSelected);
        _sbLoginSelect.Clear += new AutoCompleteFieldCleared(SearchClear);
        phLogin.Controls.Add(_sbLoginSelect);

    }

    private AutoCompleteFieldMode SearchClear(IAutoCompleteField sender)
    {
        AdminLogins login = (AdminLogins)Session[CACHE_KEY];
        login.Login = string.Empty;
        login.Email = string.Empty;
        login.UserID = 0;
        OpenForm(login, true);
        _sbLoginSelect.Text = TB_LOGIN_SEARCH_MSG;
        return AutoCompleteFieldMode.ShowSearchBox;
    }

    private AutoCompleteFieldMode SearchSelected(IAutoCompleteField sender, string text)
    {
        //if (!ValidEmployeeSelection(text))
        //{
        //    modalForm.Show();
        //    return AutoCompleteFieldMode.ShowSearchBox;
        //}

        AdminLogins login = (AdminLogins)Session[CACHE_KEY];
        
        if (string.IsNullOrEmpty(text))
        {
            OpenForm(login, true);
            lblError.Text = "User cannot be blank";
            _sbLoginSelect.Text = TB_LOGIN_SEARCH_MSG;
            return AutoCompleteFieldMode.ShowSearchBox;
        }

        string personId = string.Empty;
        //string loginText = ParseSearchRequestTextForNumber(text);
        string loginText = string.Empty;
        if (text.Contains("(") && text.Contains(")") && text.Contains(" - "))
        {
            try
            {
                loginText = text.Substring(text.LastIndexOf("(")+1);
                loginText = loginText.Remove(loginText.LastIndexOf(" - "));
            }
            catch
            {
                loginText = string.Empty;
            }
        }
        

        if (loginText == string.Empty)
        {
            //if the user didn't choose a selection from the auto complete list it may still be a valid login name so check it
            personId = GetPersonId(text);
            if (personId != string.Empty)
                loginText = text;
        }
        if (string.IsNullOrEmpty(loginText))
        {
            OpenForm(login, true);
            lblError.Text = "User " + text + " was not a valid entry";
            _sbLoginSelect.Text = TB_LOGIN_SEARCH_MSG;
            return AutoCompleteFieldMode.ShowSearchBox;
        }

        var query = from a in AdminLogins.AllAdmins
                    where !string.IsNullOrEmpty(a.Login) && a.Login.ToLower() == loginText.ToLower()
                    select a;

        if (query.Count() > 0)
        {
            OpenForm(login, true);
            lblError.Text = "User " + query.FirstOrDefault<AdminLogins>().Login + " already exists";
            _sbLoginSelect.Text = TB_LOGIN_SEARCH_MSG;
            return AutoCompleteFieldMode.ShowSearchBox;
        }

        login.Login = loginText;
        
        if (personId == string.Empty)
            personId = GetPersonId(loginText);

        if (personId == string.Empty)
        {
            OpenForm(login, true);
            lblError.Text = "User " + text + " could not be found in the database";
            _sbLoginSelect.Text = TB_LOGIN_SEARCH_MSG;
            return AutoCompleteFieldMode.ShowSearchBox;
        }
        
            
        try
        {
            //if the WS fails leave email address blank
            FirmDirectory fd = new FirmDirectory();
            login.Email = fd.GetEmployeeEmailAddress(personId);
        }
        catch { }
        
        tbEmail.Text = login.Email;
        OpenForm(login, true);
        btnCreate.Enabled = true;
        return AutoCompleteFieldMode.ShowResults;
    }

    private string GetPersonId(string login)
    {
        CMSData cms = CMSData.GetInstance();
        string personId = string.Empty;
        try
        {
            personId = cms.GetUserCodeByLogin(login);
        }
        catch { }
        return personId;
    }

    protected void lbAdd_Click(object sender, EventArgs e)
    {
        AdminLogins login = new AdminLogins();
        login.UserID = 0;

        ddlOffice.SelectedIndex = 0;
        OpenForm(login, true);
        _sbLoginSelect.Text = TB_LOGIN_SEARCH_MSG;
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod]
    public static string[] GetUserList(string prefixText, int count, string contextKey)
    {
        if (string.IsNullOrEmpty(prefixText) ||
            (prefixText.Contains("(") &&
             prefixText.Contains(")") &&
             !prefixText.Contains(TB_LOGIN_SEARCH_MSG)))
            return new string[0];

        CMSData cms = CMSData.GetInstance();
        DataTable dt = cms.DoUserSearch(prefixText);
        List<string> results = new List<string>((dt.Rows.Count > count) ? count : dt.Rows.Count);
        foreach (DataRow dr in dt.Rows)
        {
            results.Add(dr[CMSData.EMP_NAME] + " (" + dr[CMSData.EMP_CODE] + ")");
            if (results.Count == count)
                break;
        }
        return results.ToArray();
    }


    private void OpenForm(AdminLogins login, bool isNewUser)
    {
        //save to cache and bind lists
        Session[CACHE_KEY] = login;
        CMSData cms = CMSData.GetInstance();
        BindLists(false);

        //show the modal form
        //_modal.Show();
        modalForm.Show();

        if (isNewUser) //(login.UserID == null || login.UserID < 1)
        {
            //set headers and controls for creating a new user
            lblUserHeader.Text = "Create a new user account";
            lblUserID.Text = "<User ID unassigned>"; // string.Empty;
            
            //cbAdmin.Enabled = false;
            //cbAP.Enabled = false;
            //cbPGM.Enabled = false;
            pnlUserRoles.Visible = false;
            btnCreate.Visible = true;
            btnCreate.Enabled = false;
            btnSave.Visible = false;

            //set the login autocomplete box
            _sbLoginSelect.Mode = (string.IsNullOrEmpty(login.Login)) ? AutoCompleteFieldMode.ShowSearchBox : AutoCompleteFieldMode.ShowResults;
            if (_sbLoginSelect.Mode == AutoCompleteFieldMode.ShowResults)
                _sbLoginSelect.Text = login.Login;

            //default the office to DEN
            ddlOffice.SelectedIndex = 0;
        }
        else
        {
            //set headers and controls for updating
            lblUserHeader.Text = "User account information for " + login.Login;
            lblUserID.Text = login.UserID.ToString();
            
            //cbAdmin.Enabled = true;
            //cbPGM.Enabled = true;
            //cbAP.Enabled = true;
            pnlUserRoles.Visible = true;
            btnCreate.Visible = false;
            btnSave.Visible = true;

            //disable login selection
            _sbLoginSelect.Text = login.Login;
            _sbLoginSelect.Mode = AutoCompleteFieldMode.ShowResultsAndDisable;
        }

        //set the office
        if (login.Office != null)
            ddlOffice.SelectedValue = login.Office.ToString();

        //set email
        tbEmail.Text = login.Email;

        //set the checkboxes and visibility of any slave panels
        cbAdv.Checked = (login.IsAdversary.HasValue && login.IsAdversary.Value);
        pnlAdvAdmin.Visible = cbAdv.Checked;
        cbAdvAdmin.Checked = (login.IsAdversaryAdmin.HasValue && login.IsAdversaryAdmin.Value);
        cbAP.Checked = (login.IsAP.HasValue && login.IsAP.Value);
        pnlAPType.Visible = cbAP.Checked;
        
        // blmcgee@hollandhart.com 11/8/2013 fix for details screen not showing correct value when opened
        ddlAPType.SelectedValue = (login.IsPrimaryAP ?? false) ? "primary" : "secondary";

        cbPGM.Checked = (login.IsPGM.HasValue && login.IsPGM.Value);
        pnlPGMPracCodes.Visible = cbPGM.Checked;

        //bind the practice code drop down list
        ddlAddPracCode.DataSource = cms.PracticeTypes;
        ddlAddPracCode.DataTextField = CMSData.PRACTICE_TYPES_DESCRIPTION_PLUS_CODE;
        ddlAddPracCode.DataValueField = CMSData.PRACTICE_TYPES_CODE;
        ddlAddPracCode.DataBind();
        ddlAddPracCode.CssClass = "sbControl";
        ddlAddPracCode.Width = Unit.Pixel(200);

        //update
        if (!IsPostBack)
            upModalForm.Update();
    }

    private void BindLists(bool refreshAdminList)
    {
        lblError.Text = string.Empty;
        if (refreshAdminList)
            AdminLogins.RefreshAdminList();

        //phAjaxContainer.Controls.Clear();
        gvUserList.DataSource = AdminLogins.AllAdmins;
        gvUserList.DataBind();

        if (Session[CACHE_KEY] == null)
            return;

        AdminLogins login = (AdminLogins)Session[CACHE_KEY];
        repPGMPracCodes.ItemTemplate = new PracCodeTemplate(this, login);
        repPGMPracCodes.DataSource = login.AdminPracCodes;
        repPGMPracCodes.DataBind();
    }


    #region FORM EVENT HANDLERS
    protected void cbAdv_OnCheckedChanged(object sender, EventArgs e)
    {
        _sbLoginSelect.Mode = AutoCompleteFieldMode.ShowResultsAndDisable;
        pnlAdvAdmin.Visible = cbAdv.Checked;
        modalForm.Show();
    }

    protected void cbAP_OnCheckedChanged(object sender, EventArgs e)
    {
        _sbLoginSelect.Mode = AutoCompleteFieldMode.ShowResultsAndDisable;
        pnlAPType.Visible = cbAP.Checked;
        modalForm.Show();
    }

    protected void cbPGM_OnCheckedChanged(object sender, EventArgs e)
    {
        _sbLoginSelect.Mode = AutoCompleteFieldMode.ShowResultsAndDisable;
        pnlPGMPracCodes.Visible = cbPGM.Checked;
        modalForm.Show();
    }

    protected void btnPGMAddType_Click(object sender, EventArgs e)
    {
        AdminLogins login = (AdminLogins)Session[CACHE_KEY];
        if (login == null)
        {
            _MasterPage.FeedbackMessage = "Sorry an error occurred. Could not update this user";
            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Error;
        }
        var query = from p in login.AdminPracCodes
                    where p.PracCode == ddlAddPracCode.SelectedValue
                    select p;

        if (query.Count() > 0)
        {
            lblError.Text = "User " + login.Login + " has already been added to practice group " + ddlAddPracCode.SelectedItem.Text;
            modalForm.Show();
            return;
        }

        AdminPracCodes pracCode = new AdminPracCodes();
        pracCode.PracCode = ddlAddPracCode.SelectedValue;
        pracCode.UserID = login.UserID;
        pracCode.IsPrimary = (ddlPGMType.SelectedValue == "primary");

        login.AdminPracCodes.Add(pracCode);

        using (SqlConnection conn = AdversaryData.GetConnection())
        {
            DatabaseWorker worker = new DatabaseWorker(conn);
            worker.Commit(pracCode);
        }

        BindLists(false);
        _sbLoginSelect.Mode = AutoCompleteFieldMode.ShowResultsAndDisable;

        Session[CACHE_KEY] = login;
        modalForm.Show();
        upModalForm.Update();
    }

    protected void btnSubmitForm_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;
        //commit the user form to the DB
        AdminLogins login = (AdminLogins)Session[CACHE_KEY];
        //login.Login = tbLogin.Text;
        login.Email = tbEmail.Text;
        login.Office = Int32.Parse(ddlOffice.SelectedValue);

        if (string.IsNullOrEmpty(login.Login))
        {
            BindLists(true);
            modalForm.Hide();
           
            _MasterPage.FeedbackMessage = "Could not save this account, a login name was invalid or unspecified.";
            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Error;
            return;
        }

        if (btn.CommandName == "Save")
        {
            login.IsAdversary = cbAdv.Checked;
            login.IsAdversaryAdmin = cbAdvAdmin.Checked;
            login.IsAP = cbAP.Checked;
            login.IsPrimaryAP = (cbAP.Checked && ddlAPType.SelectedValue == "primary");
            login.IsPGM = cbPGM.Checked;

            //delete all codes first
            //login.RemovePracticeGroupMembership();

            using (SqlConnection conn = AdversaryData.GetConnection())
            {
                DatabaseWorker worker = new DatabaseWorker(conn);
                //add codes
                if (cbPGM.Checked)
                {
                    foreach (AdminPracCodes code in login.AdminPracCodes)
                    {
                        code.UserID = login.UserID;
                        worker.Commit(code);
                    }
                }
                else
                {
                    login.RemovePracticeGroupMembership();
                }

                //save the login
                worker.Commit(login);
            }

            AdminLogins.RefreshAdminList();
            Session.Remove(CACHE_KEY);
            BindLists(true);
            modalForm.Hide();
            //upMain.Update();

            _MasterPage.FeedbackMessage = "User account " + login.Login + " updated";
            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Success;
        }
        else
        {
            login.IsAdversary = false;
            login.IsAdversaryAdmin = false;
            login.IsAP = false;
            login.IsPGM = false;
            //login.Login = ParseSearchRequestTextForNumber(_sbLoginSelect.Text);
        
            //save the login
            using (SqlConnection conn = AdversaryData.GetConnection())
            {
                DatabaseWorker worker = new DatabaseWorker(conn);
                worker.Commit(login);
            }
            AdminLogins.RefreshAdminList();
            OpenForm(login, false);
            
            _MasterPage.FeedbackMessage = "User account " + login.Login + " created";
            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Success;
        }
    }

    protected void btnClose_Click(object sender, EventArgs e)
    {
        AdminLogins login = (AdminLogins)Session[CACHE_KEY];
        Session.Remove(CACHE_KEY);
        BindLists(true);
        modalForm.Hide();
        //uupMain.Update();
    }
    #endregion

    #region User List Methods

    private void gvUserList_RowCreated(object sender, GridViewRowEventArgs e)
    {
        TableCell td = new TableCell();

        if (e.Row.RowType != DataControlRowType.DataRow)
        {
            td.Text = "&nbsp;";
            e.Row.Cells.AddAt(0, td);
            return;
        }

        AdminLogins login = (AdminLogins)e.Row.DataItem;

        LinkButton lbMod = new LinkButton();
        lbMod.ID = "lbMod___" + login.UserID;
        lbMod.SetDataItem(login);
        lbMod.Text = "Modify";
        lbMod.Click += new EventHandler(lbMod_Click);

        //AsyncPostBackTrigger trigger = new AsyncPostBackTrigger();
        //trigger.ControlID = lbMod.ID;
        //trigger.EventName = "Click";
        //upUserList.Triggers.Add(trigger);

        LinkButton lbDel = new LinkButton();
        lbDel.ID = "lbDel___" + login.UserID;
        lbDel.SetDataItem(login);
        lbDel.Text = "Delete";
        lbDel.Attributes.Add("onclick", "return confirm('Are you sure that you want to delete user " + login.Login + "?');");
        lbDel.Click += new EventHandler(lbDel_Click);

        td.Controls.Add(lbMod);
        td.Controls.Add(new LiteralControl("&nbsp;&nbsp;&nbsp;&nbsp;"));
        td.Controls.Add(lbDel);
        e.Row.Cells.AddAt(0, td);
    }

    private void lbMod_Click(object sender, EventArgs e)
    {
        LinkButton lb = (LinkButton)sender;
        AdminLogins login = (AdminLogins)lb.GetDataItem();
        OpenForm(login, false);
    }

    private void lbDel_Click(object sender, EventArgs e)
    {
        LinkButton lb = (LinkButton)sender;
        AdminLogins login = (AdminLogins)lb.GetDataItem();
        
        if (!string.IsNullOrEmpty(login.Login) && login.Login.ToUpper() == _SessionData.AdminLogin.Login.ToUpper())
        {
            _MasterPage.FeedbackMessage = "You can't delete yourself";
            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Error;
            return;
        }

        login.RemovePracticeGroupMembership();
        using (SqlConnection conn = AdversaryData.GetConnection())
        {
            DatabaseWorker worker = new DatabaseWorker(conn); 
            worker.Delete(login);
            BindLists(true);
        }

        _MasterPage.FeedbackMessage = "User " + login.Login + " deleted";
        _MasterPage.FeedbackMode = MasterPageFeedbackMode.Success;
    }

    protected string GetOfficeName(object officeNum)
    {
        if (officeNum == null)
            return string.Empty;

        CMSData cms = CMSData.GetInstance();
        return cms.GetOfficeName(officeNum.ToString());
    }

    protected string GetCheckboxImage(object boolValue)
    {
        return (boolValue != null && Boolean.Parse(boolValue.ToString())) ? "../_css/true.gif" : "../_css/false.gif";
    }

    protected string GetAdvStatus(object isAdv, object isAdvAdmin)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<img src=\"" + GetCheckboxImage(isAdv) + "\" alt=\"Is Adversary Group Member?\" />");
        if (isAdv != null && (bool)isAdv && isAdvAdmin != null && (bool)isAdvAdmin)
            sb.Append("&nbsp;&nbsp;<span style='color: DarkOrange;'>(admin)</span><br/>");
        return sb.ToString();
    }

    protected string GetAPStatus(object isAP, object isPrimary)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<img src=\"" + GetCheckboxImage(isAP) + "\" alt=\"Is AP?\" />");
        if (isAP != null && (bool)isAP && isPrimary != null)
            sb.Append(GetUserTypeIndicator(isPrimary));
        return sb.ToString();
    }

    protected string GetUserTypeIndicator(object boolValue)
    {
        return "<span style='color: DarkOrange;'>(" + (Boolean.Parse(boolValue.ToString()) ? "P" : "S") + ")</span><br/>";
    }

    protected string GetPracticeGroupList(object codeList)
    {
        EntitySet<AdminPracCodes> codes = (EntitySet<AdminPracCodes>)codeList;
        StringBuilder sb = new StringBuilder();
        CMSData cms = CMSData.GetInstance();

        foreach (AdminPracCodes code in codes)
        {
            if (string.IsNullOrEmpty(code.PracCode))
                continue;

            sb.Append(cms.GetPracticeName(code.PracCode) + " - " + code.PracCode);
            if (code.IsPrimary != null)
                sb.AppendLine(GetUserTypeIndicator(code.IsPrimary));
        }

        return sb.ToString();
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod]
    public static string[] GetLoginList(string prefixText, int count, string contextKey)
    {
        if (string.IsNullOrEmpty(prefixText) ||
            ValidEmployeeSelection(prefixText))
            return new string[0];

        CMSData cms = CMSData.GetInstance();
        DataTable dt = cms.DoEmployeeSearch(prefixText);
        List<string> results = new List<string>((dt.Rows.Count > count) ? count : dt.Rows.Count);
        foreach (DataRow dr in dt.Rows)
        {
            results.Add(dr[CMSData.EMP_NAME] + " (" + dr[CMSData.EMP_LOGIN] + " - " + dr[CMSData.EMP_CODE] + ")");
            if (results.Count == count)
                break;
        }
        return results.ToArray();
    }
    #endregion
}
