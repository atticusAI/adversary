﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using HH;
using HH.Branding;
using HH.Extensions;
using System.Data;
using System.Reflection;
using System.Text;

public partial class AdminQueue : AdminBasePage
{
    protected enum AdminQueueDataField
    {
        ScrMemID,
        MatterName,
        ClientNumber,
        AttEntering,
        PersonnelID,
        AP,
        PGM,
        AdversaryAdmin,
        FeeSplit,
        CMNumbersSentDate
    }
    protected override void OnInit(EventArgs e)
    {
        _MasterPage.SideBarVisible = false;
        if (!IsPostBack)
            ClearMemo(); //clear any current memo from memory when hitting the queue for the first time

        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //List<Memo> memoList = Tracking.GetOpenMemos(null, TrackingStatusType.AdversaryGroupProcessing, false);
        List<Memo> memoList = Tracking.GetAdversaryQueue();

        if (memoList.Count == 0)
        {
            _lblCount.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Your queue is currently empty!";
            _lblCount.Font.Size = FontUnit.Larger;
            _lblCount.ForeColor = Color.Green;
            return;
        }


        _refreshControl.RefreshTime = 300000;

        _lblCount.Text = memoList.Count + " Memo(s) Ready for Processing";
        _lblCount.ForeColor = Color.FromName("#68849F");
        _gvResults.RowDataBound += new GridViewRowEventHandler(_gvResults_RowDataBound);
        _gvResults.DataSource = memoList;
        _gvResults.DataBind();
    }

    void _gvResults_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.DataRow)
            return;

        LinkButton lb = (LinkButton)e.Row.Cells[0].FindControl("_lbView");
        lb.SetDataItem(e.Row.DataItem);
    }

    protected string GetBoundContents(IDataItemContainer container, AdminQueueDataField dataField)
    {

        try
        {
            object value = null;
            string text = string.Empty;

            switch (dataField)
            {
                case AdminQueueDataField.AttEntering:
                    value = GetValue(container.DataItem, dataField, typeof(Memo));
                    text = GetEmployeeDisplayName(value);
                    break;
                case AdminQueueDataField.PersonnelID:
                    value = GetValue(container.DataItem, dataField, typeof(Memo));
                    text = GetEmployeeDisplayName(value);
                    break;
                case AdminQueueDataField.ClientNumber:
                    text = GetClientDisplayName((Memo)container.DataItem);
                    break;
                case AdminQueueDataField.AP:
                    text = GetTrackingStatus(container, dataField);
                    break;
                case AdminQueueDataField.PGM:
                    text = GetTrackingStatus(container, dataField);
                    break;
                case AdminQueueDataField.AdversaryAdmin:
                    text = GetTrackingStatus(container, dataField);
                    break;
                case AdminQueueDataField.FeeSplit:
                    Memo memo = (Memo)container.DataItem;
                    text = (memo.FeeSplitStaffs.Count > 0) ? "Y" : "N";
                    break;
                case AdminQueueDataField.CMNumbersSentDate:
                    value = GetValue(((Memo)container.DataItem).Tracking, dataField, typeof(Tracking));
                    if (value != null && value != DBNull.Value)
                        text = "<span style='color: green;'>" + ((DateTime)value).ToShortDateString() + "</span>";
                    break;
                default:
                    if (dataField == AdminQueueDataField.MatterName && ((Memo)container.DataItem).ScrMemType == 6)
                    {
                        ///commented by A Reimer 03/21/12 for 'Rejected to Modification Required' change
                        //text = "&nbsp;<em>(Rejected Matter)</em>";
                        // blmcgee@hollandhart.com 2014-01-13
                        // text = "&nbsp;<em>(Matter Requiring Modification)</em>";
                        text = "&nbsp;<em>(Rejected Matter)</em>";
                    }
                    else
                    {
                        value = GetValue(container.DataItem, dataField, typeof(Memo));
                        text = (value == null || value == DBNull.Value) ? string.Empty : value.ToString();
                    }
                    break;

            }

            return text;
        }
        catch
        {
            return string.Empty;
        }
    }

    private object GetValue(object dataItem, AdminQueueDataField dataField, Type type)
    {
        return type.GetProperty(dataField.ToString()).GetValue(dataItem, null);
    }
    

    private string GetTrackingStatus(IDataItemContainer container, AdminQueueDataField dataField)
    {
        try
        {
            Memo memo = (Memo)container.DataItem;
            Tracking tracking = memo.Tracking;
            Type t = typeof(Tracking);
            string prefix = dataField.ToString();

            PropertyInfo info = t.GetProperty(prefix + "Acknowledged");
            bool? b = (bool?)info.GetValue(tracking, null);
            bool acknolwedged = b.HasValue && b.Value;

            info = t.GetProperty(prefix + "Approved");
            b = (bool?)info.GetValue(tracking, null);
            bool approved = b.HasValue && b.Value;

            if (!acknolwedged && !approved)
            {
                if (memo.ScrMemType == 6 && (prefix == "AP" || prefix == "PGM"))
                    return "N/A";
                if (memo.OrigOffice == "10" && prefix == "AP")
                    return "N/A";
                return string.Empty;
            }

            if (approved)
            {
                info = t.GetProperty(prefix + "Signature");
                string sig = (string)info.GetValue(tracking, null);
                info = t.GetProperty(prefix + "Date");
                DateTime? stamp = (DateTime?)info.GetValue(tracking, null);

                StringBuilder sb = new StringBuilder();
                sb.Append("<span style='color: green;'>");
                //sb.Append("Approved");
                if (!string.IsNullOrEmpty(sig))
                    //sb.Append(": " + sig);
                    sb.Append(sig);
                if (stamp.HasValue)
                    sb.Append(" (" + stamp.Value.ToShortDateString() + ")");
                sb.Append("</span>");
                return sb.ToString();
            }
            else
            {
                return "<span style='color: blue;'>Acknowledged</span>";
                //if (dataField == AdminQueueDataField.AdversaryAdmin)
                //    return "<span style='color: blue;'>Acknowledged</span>";

                //info = t.GetProperty(prefix + "UserID");
                //int? userId = (int?)info.GetValue(tracking, null);

                //if (!userId.HasValue)
                //    return "<span style='color: blue;'>Acknowledged</span>";

                //return "<span style='color: blue;'>Acknowledged " + AdminLogins.GetAdminLoginFromID(userId).Login + "</span>";
            }
        }
        catch (Exception ex)
        {
            SendErrorEmail(ex);
            return "<span style='color: red;'>ERROR</span>";
        }
    }

    protected void lbView_Click(object sender, EventArgs e)
    {
        LinkButton lb = (LinkButton)sender;
        Memo memo = (Memo)lb.GetDataItem();
        memo.Tracking.AdversaryUserID = _login.UserID;
        memo.Tracking.AdversaryAcknowledged = true;

        DoDataSave(memo.Tracking);
        _SessionData.MemoData = memo;
        _SessionData.MemoLoaded = true;
        ToggleMenuEnabled(true);

        Response.Redirect("AdminProcessing.aspx?ScrMemID=" + memo.ScrMemID);
    }
}
