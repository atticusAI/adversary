﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using HH;
using HH.Screen;
using HH.Branding;
using HH.UI.FileList;

public partial class FilesAdmin : AdminBasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!_SessionData.MemoLoaded)
        {
            JSAlertAndRedirect("This page is unviewable because you have not selected an adversary request", "/Admin/Queue.aspx");
            return;
        }

        //fcFiles.FileListTemplate = new FileListTemplate();
        //fcFiles.FileDirectory = GetUploadDirectoryURL(_SessionData.MemoData.ScrMemID);
        fcFiles.FileListItemDataBound += new RepeaterItemEventHandler(base.FileList_ItemDataBound);
        fcFiles.OnFileUploaded += new FileUploaded(fcFiles_OnFileUploaded);
        fcFiles.OnFileEditClick += new FileEditClick(fcFiles_OnFileEditClick);
        fcFiles.OnFileDeleteClick += new FileDeleteClick(fcFiles_OnFileDeleteClick);
        fcFiles.OnFileEditSubmit += new FileEditSubmit(fcFiles_OnFileEditSubmit);

        if (_SessionData.MemoData.ScrMemID.HasValue)
        {
            lblScrMemID.Text = _SessionData.MemoData.ScrMemID.Value.ToString();
        }
        else
        {
            lblScrMemID.ForeColor = Color.Red;
            lblScrMemID.Text = "(Not Found)";
        }

        if (_SessionData.MemoData.Tracking.SubmittedOn.HasValue)
        {
            lblSubmittedOn.Text = _SessionData.MemoData.Tracking.SubmittedOn.Value.ToString();
        }
        else
        {
            lblSubmittedOn.ForeColor = Color.Red;
            lblSubmittedOn.Text = "(Not Found)";
        }

        BindFileListControl();
                            
    }

    private void BindFileListControl()
    {
        fcFiles.DataSource = _SessionData.MemoData.Attachments;
        fcFiles.DataBind();
        fcFiles.EnableDescription = true;
    }

    bool fcFiles_OnFileUploaded(IFileListControl sender, FileUpload file, string description)
    {
        if (!FileListControl_OnFileUploaded(sender, file, description))
            return false;
        BindFileListControl();
        return true;
    }

    bool fcFiles_OnFileEditClick(IFileListControl sender, object dataItem)
    {
        Attachments attachment = (Attachments)dataItem;
        if (!FileListControl_OnFileEditClick(sender, dataItem))
            return false;
        fcFiles.EnableDescription = attachment.FileDescription != "Engagement Letter" &&
                                    attachment.FileDescription != "Written Policy";
        return true;
    }

    bool fcFiles_OnFileDeleteClick(IFileListControl sender, object dataItem)
    {
        if (!FileListControl_OnFileDeleteClick(sender, dataItem))
            return false;
        BindFileListControl();
        return true;
    }

    bool fcFiles_OnFileEditSubmit(IFileListControl sender, object dataItem, string newFileName, string description)
    {
        if (!FileListControl_OnFileEditSubmit(sender, dataItem, newFileName, description))
            return false;
        BindFileListControl();
        return true;
    }

}
