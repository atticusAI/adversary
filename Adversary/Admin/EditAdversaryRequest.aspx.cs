﻿using System;
using System.Data.SqlClient;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using HH.Branding;
using HH.SQL;

public partial class EditAdversaryRequest : AdminBasePage
{
    private static readonly string MARK_COMPLETE_HELP = "An employee login must be selected in the 'Assigned To' and 'Work Completed By' drop down " +
                                                             "lists in order to mark the request as complete. Select an employee and then click " +
                                                             "the Mark Complete button to complete the request. The status can be switched " +
                                                             "back to pending by clicking the 'Click to remove Completed Status' link when visible.";

    bool _complete = false;
    DropDownList _ddlWorkBegunBy = null;
    DropDownList _ddlWorkCompletedBy = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!_SessionData.AdversaryRequestLoaded)
        {
            JSAlertAndRedirect("An adversary request was not selected for editing", "/Request/ViewRequests.aspx?type=my");
            return;
        }

        _complete = _SessionData.AdversaryRequest.IsComplete;
        _MasterPage.FooterNavVisible = false;
        _MasterPage.PopupYesClick += new EventHandler(popupYes_Click);
        if (_complete)
            _MasterPage.PopupNoClick += new EventHandler(popupNo_Click);
        _MasterPage.PostBackEvent += new MasterPagePostBackEventHandler(_MasterPage_PostBackEvent);

        BindScreen();

        //as requested by Adv Group, bold every selected checkbox
        foreach (TableRow tr in _MasterPage.Screen.Form.Rows)
        {
            if (tr.Cells.Count != 2)
                continue;

            foreach (Control c in tr.Cells[1].Controls)
            {
                if (c is CheckBox)
                {
                    CheckBox cb = (CheckBox)c;
                    tr.Cells[0].Font.Bold = (c.ID != "DailyMemo" && cb.Checked);
                }
            }
        }
    }

    private void BindScreen()
    {
        _MasterPage.Screen.ScreenID = "EditAdversaryRequest.aspx";
        _MasterPage.Screen.DataSource = _SessionData.AdversaryRequest;
        _MasterPage.Screen.DataBind();

        if (_ddlWorkBegunBy == null)
            _ddlWorkBegunBy = (DropDownList)_MasterPage.Screen.Form.FindControl("WorkBegunBy");
        if (_ddlWorkCompletedBy == null)
            _ddlWorkCompletedBy = (DropDownList)_MasterPage.Screen.Form.FindControl("WorkCompletedBy");
        //if (!IsPostBack)
        //{
        _ddlWorkBegunBy.Items.Insert(0, new ListItem("(unassigned)", string.Empty));
        _ddlWorkCompletedBy.Items.Insert(0, new ListItem("(unassigned)", string.Empty));
        //}

        _ddlWorkBegunBy.Enabled = !_complete;
        _ddlWorkCompletedBy.Enabled = !_complete;
        _btnComplete.Enabled = !_complete;

        PlaceHolder ph = new PlaceHolder();
        Label lbl = new Label();
        lbl.Font.Bold = true;
        lbl.ForeColor = (_complete) ? Color.Green : Color.Orange;
        lbl.Text = (_complete) ? "Completed " + _SessionData.AdversaryRequest.CompletionDate : "Pending";
        ph.Controls.Add(lbl);

        if (_complete)
        {
            LinkButton btn = new LinkButton();
            btn.Text = "Click to remove Completed Status";
            btn.Click += new EventHandler(lbRemoveStatus_Click);
            ph.Controls.Add(new LiteralControl("&nbsp;&nbsp;&nbsp;("));
            ph.Controls.Add(btn);
            ph.Controls.Add(new LiteralControl(")"));
        }

        _MasterPage.Screen.AddControlToFormAt(0, ph, "<strong>STATUS</strong>", MARK_COMPLETE_HELP);
    }

    internal void lbRemoveStatus_Click(object sender, EventArgs e)
    {
        _MasterPage.PopupYesCommandArgument = "remove";
        _MasterPage.ShowPopupDialog("This request has already been marked as complete. Are you sure that you want to mark as pending and re-open it?", MasterPagePopupDialogMode.ModalOkCancel);
    }

    private bool _MasterPage_PostBackEvent(object sender, MasterPagePostBackEventArgs e)
    {
        try
        {
            UpdateRequest();
            return true;
        }
        catch (Exception ex)
        {
            HandleError(ex);
            return false;
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        Button button = (Button)sender;
        if (button.CommandArgument == "complete" && !_complete)
        {
            if (string.IsNullOrEmpty(_ddlWorkBegunBy.SelectedValue) || string.IsNullOrEmpty(_ddlWorkCompletedBy.SelectedValue))
            {
                _MasterPage.FeedbackMode = MasterPageFeedbackMode.Error;
                _MasterPage.FeedbackMessage = "<br/><br/>Please select a name in the 'Assigned To' and 'Work Completed By' drop down lists in order to mark this request as complete";
                _MasterPage.PopupYesCommandArgument = "warning";
                _MasterPage.ShowPopupDialog(MARK_COMPLETE_HELP, MasterPagePopupDialogMode.ModalOK);
            }
            else
            {
                _MasterPage.PopupYesCommandArgument = "complete";
                _MasterPage.ShowPopupDialog("<br/><br/>This request will be marked as completed by " + _ddlWorkCompletedBy.SelectedValue + ". Please confirm to continue and remove this request from the queue.", MasterPagePopupDialogMode.ModalOkCancel);
            }
        }
        else
        {
            try
            {
                UpdateRequest();
            }
            catch (Exception ex)
            {
                HandleError(ex);
            }
        }


    }

    void popupYes_Click(object sender, EventArgs e)
    {
        Requests request = (Requests)_MasterPage.Screen.DataSource;
        string arg = ((Button)sender).CommandArgument;
        _complete = (arg == "complete");
        request.CompletionDate = (_complete) ? DateTime.Now : (DateTime?)null;
        request.WorkCompletedBy = (_complete) ? _ddlWorkCompletedBy.SelectedValue : null;
        UpdateRequest(request);

        if (arg == "warning")
            return;

        if (_complete)
        {
            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Success;
            _MasterPage.FeedbackMessage = "Adversary request " + request.ReqID + " marked as complete by " + request.WorkCompletedBy;
            SendAdversaryRequestEmail(request);
        }
        else
        {
            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Confirm;
            _MasterPage.FeedbackMessage = "Adversary request " + request.ReqID + " status changed to pending";
            _btnComplete.Enabled = true;
        }

        BindScreen();
    }

    void popupNo_Click(object sender, EventArgs e)
    {
        Requests request = (Requests)_MasterPage.Screen.DataSource;
        UpdateRequest(request);
    }

    private void UpdateRequest() { UpdateRequest((Requests)_MasterPage.Screen.DataSource); }
    private void UpdateRequest(Requests request)
    {
        //Requests request = (Requests)_MasterPage.Screen.DataSource;
        DropDownList ddlBegunBy = (DropDownList)_MasterPage.Screen.Form.FindControl("WorkBegunBy");
        if (ddlBegunBy.SelectedValue == string.Empty)
        {
            request.WorkBegunDate = null;
            request.WorkBegunBy = null;
        }
        else if (request.WorkBegunDate == null)
        {
            request.WorkBegunDate = DateTime.Now;
        }

        using (SqlConnection conn = AdversaryData.GetConnection())
        {
            DatabaseWorker worker = new DatabaseWorker(conn);
            foreach (RequestParties party in request.RequestParties)
            {
                party.ReqID = request.ReqID;
                DoDataSave(party, worker);
            }

            DoDataSave(request, worker);
            conn.Close();
        }
        _SessionData.AdversaryRequest = request;

        _MasterPage.FeedbackMode = MasterPageFeedbackMode.Success;
        _MasterPage.FeedbackMessage = "Request Saved";
        if (_SessionData.AdversaryRequest.IsComplete)
            _MasterPage.FeedbackMessage += " Current status - Completed by " + _SessionData.AdversaryRequest.WorkCompletedBy + ".";
        //else if (!string.IsNullOrEmpty(_SessionData.AdversaryRequest.WorkBegunBy))
        //    _MasterPage.FeedbackMessage += " and Assigned to " + _SessionData.AdversaryRequest.WorkBegunBy + ".";
    }
}
