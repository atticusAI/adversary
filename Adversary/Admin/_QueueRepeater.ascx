﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_QueueRepeater.ascx.cs" Inherits="QueueRepeater" %>


<asp:Repeater ID="repQueue" runat="server" OnItemDataBound="repQueue_ItemDataBound" OnItemCommand="repQueue_ItemCommand">
<ItemTemplate>
    <div id="divRepeaterBox" runat="server" class="repeaterBox" style="overflow: hidden;" >
        <div class="repeaterBoxHeader">
            <table style="width: 100%;">
                <tr runat="server" id="rowError" visible="false">
                    <td colspan="5" style="padding-right: 5px;"><asp:Label ID="lblNotice" runat="server" Font-Bold="true" ForeColor="Red"></asp:Label></td>
                </tr>
                <tr>
                    <td style="width: 25%; padding-right: 5px;"><asp:Label ID="lblClientName" runat="server" Font-Bold="true"></asp:Label></td>
                    <td style="width: 20%; padding-right: 5px;"><asp:Label ID="lblMatterName" runat="server" Font-Bold="true"></asp:Label></td>
                    <td style="width: 20%; padding-right: 5px;"><asp:Label ID="lblMemoType" runat="server" Font-Bold="true"></asp:Label></td>
                    <td style="width: 20%; padding-right: 5px;"><asp:Label ID="lblPracCode" runat="server" Font-Bold="true"></asp:Label></td>
                    <td style="width: 10%;"><asp:Label ID="lblOffice" runat="server" Font-Bold="true"></asp:Label></td>
                </tr>
            </table>
        </div>
        <div>
            <table style="width: 100%;">
                <tr>
                    <td style="width: 4%; padding-right: 5px;">
                        <div style="text-align: center; padding-bottom: 5px;">
                            <asp:PlaceHolder ID="phSubmit" runat="server"></asp:PlaceHolder>
                        </div>
                    </td>
                    <td style="width: 60%; padding-left: 5px;">
                        <table style="width: 100%;" cellpadding="3px">
                            <tr>
                                <td style="width: 5%">Memo#</td><td width="28%"><asp:Label ID="lblScrMemID" runat="server" ForeColor="#68849F"></asp:Label></td>
                                <td style="width: 5%">Date</td><td width="28%" ><asp:Label ID="lblDate" runat="server" ForeColor="#68849F"></asp:Label></td>
                                <td style="width: 5%">Attorney</td><td width="28%" ><asp:Label ID="lblAttEntering" runat="server" ForeColor="#68849F"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>Description</td>
                                <td colspan="3"><asp:Label ID="lblWorkDesc" runat="server" ForeColor="#68849F"></asp:Label></td>
                                <td>Input By</td><td ><asp:Label ID="lblPersonnelID" runat="server" ForeColor="#68849F"></asp:Label></td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 36%; padding-left: 5px; vertical-align: top; ">
                        <asp:Table ID="tableStatus" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</ItemTemplate>
</asp:Repeater>