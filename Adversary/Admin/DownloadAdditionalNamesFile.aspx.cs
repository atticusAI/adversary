﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using HH.Branding;
using HH.Utils;
using System.Configuration;

public partial class Admin_DownloadAdditionalNamesFile : AdminBasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        _MasterPage.MenuMode = HH.Branding.MasterPageMenuMode.None;
        _MasterPage.TabMenuVisible = false;
        _MasterPage.SideBarVisible = false;
        _MasterPage.FooterVisible = false;

        string clientID = Request.QueryString["clientID"];
        string fileName = Request.QueryString["fileName"];
        if (string.IsNullOrEmpty(clientID) || string.IsNullOrEmpty(fileName))
        {
            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Error;
            _MasterPage.FeedbackMessage = "Could not download file, the URL was invalid";
            return;
        }
        try
        {
            string path = ConfigurationManager.AppSettings["FileUploadDirectory"].ToString() +
                "\\" + ConfigurationManager.AppSettings["AdditionalNamesSubdirectory"].ToString() +
                "\\" + clientID + "\\" + fileName;
            FileUtils.DownloadFile(Response, path);
        }
        catch (Exception ex)
        {
            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Error;
            _MasterPage.FeedbackMessage = "Could not download file. Information for technical support: " 
                + ex.Message;
        }
    }
}