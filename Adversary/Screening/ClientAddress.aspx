﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="ClientAddress.aspx.cs" Inherits="ClientAddress" MasterPageFile="~/Master.master" %>
<%@ Register TagPrefix="UC" Assembly="HH" Namespace="HH.UI.ModalPopupForm" %>

<asp:Content ID="UpperContentPlaceHolder" ContentPlaceHolderID="UpperContentPlaceHolder" runat="server">

    
    
</asp:Content>

<asp:Content ID="LowerContentPlaceHolder" ContentPlaceHolderID="LowerContentPlaceHolder" runat="server">

    <UC:ModalForm ID="modalAddressForm" runat="server" TargetControlID="lbAddressList" CancelControlID="lbCancel"  Y="50" Width="60%" >
    
    <asp:Panel ID="pnlWrapper" runat="server"  ScrollBars="Auto" Height="335" Width="100%" >
    
        <asp:UpdatePanel ID="upAddressList" runat="server" Visible="false">
            <ContentTemplate>
                
                <asp:Panel ID="pnlAddressForm" runat="server">
                    <br />
                    <asp:Repeater ID="repAddressList" runat="server" >
                        <ItemTemplate>
                            <asp:Panel ID="pnlBox" runat="server"  CssClass="repeaterBox" BackColor="White" Width="90%" >
                                <asp:Panel ID="pnlAddress" runat="server"></asp:Panel>
                                <hr id="hrSeperator" runat="server" visible="false" style="color: #68849F; height: 1px;" Width="85%" />
                                <asp:Panel ID="pnlPhone" runat="server"></asp:Panel>
                                <asp:Panel ID="pnlSelect" runat="server" HorizontalAlign="Center"></asp:Panel>
                            </asp:Panel>
                        </ItemTemplate>
                    </asp:Repeater>
                    
                </asp:Panel>
                <div style="text-align: center;">
                    <asp:LinkButton ID="lbCancel" runat="server" Text="Cancel"></asp:LinkButton>
                </div>
                
                
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="lbAddressList" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="repAddressList" EventName="ItemCommand" />
            </Triggers>
        
        </asp:UpdatePanel>
    
    </asp:Panel>
    
    </UC:ModalForm>
    <br />
    <br />
    <asp:LinkButton ID="lbAddressList" runat="server" OnClick="lbAddressList_Click" Font-Bold="true" CausesValidation="false"></asp:LinkButton>

</asp:Content>