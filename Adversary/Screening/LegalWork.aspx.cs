﻿using System;
using System.Data;
using HH;
using HH.Branding;
using HH.SQL;
using System.Data.SqlClient;
using HH.UI;

public partial class LegalWork : ScreeningBasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        _MasterPage.PostBackEvent += new MasterPagePostBackEventHandler(_MasterPage_PostBackEvent);
        phEmployeeSearchResults.Controls.Add(UIUtils.BuildAutoComplete(tbEmployeeSearch.ID, "GetEmployeeList", "Search"));
        

        if (!IsPostBack &&
            _SessionData.MemoData.Company != null &&
            _SessionData.MemoData.HHLegalWorkID != null)
        {
            bool company = (bool)_SessionData.MemoData.Company;
            rblWorkType.SelectedValue = (!company) ? "Employee" : "Firm";
            if (!company)
            {
                pnlEmployee.Visible = false;
                pnlEmployeeSelection.Visible = true;
                lblNumber.Text = _SessionData.MemoData.HHLegalWorkID.ToString();
                tbFName.Text = _SessionData.MemoData.NewClient.FName;
                tbMName.Text = _SessionData.MemoData.NewClient.MName;
                tbLName.Text = _SessionData.MemoData.NewClient.LName;
            }
            else
            {
                pnlEmployee.Visible = false;
                pnlEmployeeSelection.Visible = false;
                pnlFirm.Visible = true;
                tbOrg.Text = _SessionData.MemoData.NewClient.CName;
            }
        }
    }

    private bool _MasterPage_PostBackEvent(object sender, MasterPagePostBackEventArgs e)
    {
        string error = string.Empty;
        _MasterPage.FeedbackMessage = error;

        if (string.IsNullOrEmpty(rblWorkType.SelectedValue))
        {
            error = "Please choose the type of Legal Work";
        }
        else if (pnlFirm.Visible)
        {
            if (string.IsNullOrEmpty(tbOrg.Text))
                error = "Please enter an organization";
        }
        else if (pnlEmployee.Visible)
        {
            if (string.IsNullOrEmpty(tbEmployeeSearch.Text))
            {
                error = "Please enter a 4-digit Payroll Number or Name of the Employee to search for";
            }
            else if (ValidEmployeeSelection(tbEmployeeSearch.Text))
            {
                ParseEmployeeSearchTextbox();
            }
            else
            {
                //check the text box for valid data. If valid let the user continue without showing the names table
                CMSData cms = CMSData.GetInstance();
                DataTable dt = cms.DoEmployeeSearch(tbEmployeeSearch.Text.Trim());
                if (dt.Rows.Count != 1)
                {
                    if (dt.Rows.Count > 1)
                        error = "The value that you entered returned more than one result, please re-enter a 4-digit payroll number or name";
                    else
                        error = "The value that you entered did not return any results, please re-enter a 4-digit payroll number or name";
                }
                else
                {
                    tbEmployeeSearch.Text = dt.Rows[0][CMSData.EMP_NAME] + " (" + dt.Rows[0][CMSData.EMP_CODE] + ")";
                    ParseEmployeeSearchTextbox();
                }
            }
        }
        else if (pnlEmployeeSelection.Visible &&
                 string.IsNullOrEmpty(tbLName.Text.Trim()))
        {
            error = "Please enter the employee's last name to continue";
        }

        if (error == string.Empty)
        {
            UpdateSessionData();
            return true;
        }

        _MasterPage.FeedbackMode = MasterPageFeedbackMode.Error;
        _MasterPage.FeedbackMessage = error;
        return false;
    }

    protected void rblWorkType_SelectedIndexChanged(object sender, EventArgs e)
    {
        bool company = (rblWorkType.SelectedValue != "Employee");
        pnlFirm.Visible = company;
        pnlEmployee.Visible = !company;
        pnlEmployeeSelection.Visible = false;
        tbFName.Text = string.Empty;
        tbMName.Text = string.Empty;
        tbLName.Text = string.Empty;
        tbOrg.Text = (company) ? "Holland & Hart" : string.Empty;
        tbEmployeeSearch.Text = string.Empty;
    }

    protected void tbEmployeeSearch_TextChanged(object sender, EventArgs e)
    {
        if (!ValidEmployeeSelection(tbEmployeeSearch.Text))
            return;

        try
        {
            pnlEmployeeSelection.Visible = true;
            pnlEmployee.Visible = false;
            ParseEmployeeSearchTextbox();
            upLegalWork.Update();
        }
        catch { }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        pnlFirm.Visible = false;
        pnlEmployee.Visible = true;
        pnlEmployeeSelection.Visible = false;
        tbEmployeeSearch.Text = string.Empty;
    }

    private void ParseEmployeeSearchTextbox()
    {
        string number = tbEmployeeSearch.Text.Trim();
        number = number.Substring(number.IndexOf("(") + 1);
        number = number.Substring(0, number.IndexOf(")"));

        string name = tbEmployeeSearch.Text.Trim();
        name = name.Substring(0, name.IndexOf("(")).Trim();
        string fname = name.Substring(name.IndexOf(",") + 1).Trim();
        fname = fname[0].ToString();
        string mname = name.Substring(name.IndexOf(",") + 1).Trim();
        mname = (mname.Length > 1) ? mname[1].ToString() : string.Empty;
        string lname = name.Substring(0, name.IndexOf(",")).Trim();

        lblNumber.Text = number;
        tbFName.Text = fname;
        tbMName.Text = mname;
        tbLName.Text = lname;
    }

    private void UpdateSessionData()
    {
        //if the memo has not been saved to the DB yet, then save it to create the memo id
        using (SqlConnection conn = AdversaryData.GetConnection())
        {
            DatabaseWorker worker = new DatabaseWorker(conn);
            if (_SessionData.MemoData.ScrMemID == 0)
                DoDataSave(_SessionData.MemoData, worker);

            //now add the new client info
            bool company = (rblWorkType.SelectedValue != "Employee");
            if (_SessionData.MemoData.NewClient.NwClientID == null)
                _SessionData.MemoData.NewClient.NwClientID = 0;
            _SessionData.MemoData.NewClient.ScrMemID = _SessionData.MemoData.ScrMemID;
            _SessionData.MemoData.Company = company;
            _SessionData.MemoData.HHLegalWorkID = (!company) ? Int32.Parse(lblNumber.Text) : 1;
            _SessionData.MemoData.NewClient.FName = (!company) ? tbFName.Text.Trim() : null;
            _SessionData.MemoData.NewClient.MName = (!company) ? tbMName.Text.Trim() : null;
            _SessionData.MemoData.NewClient.LName = (!company) ? tbLName.Text.Trim() : null;
            _SessionData.MemoData.NewClient.CName = (!company) ? null : tbOrg.Text.Trim();

            //save all
            DoDataSave(_SessionData.MemoData.NewClient,worker);
            DoDataSave(_SessionData.MemoData,worker);
        }
    }
}
