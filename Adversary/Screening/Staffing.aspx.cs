﻿using System;
using System.Web.UI.WebControls;
using HH.UI;
using HH.UI.AutoComplete;


public partial class StaffingPage : ScreeningBasePage
{
    

    protected void Page_Load(object sender, EventArgs e)
    {
        _MasterPage.Screen.Width = Unit.Percentage(50);
        
        AutoCompleteField searchBox = BuildEmployeeSearchAutoComplete("sbBillingAtty", "BillingAtty", _SessionData.MemoData.RespAttID );
        searchBox.SearchRequest += new AutoCompleteSearchRequest(searchBox_OnSearchRequest);
        searchBox.Clear += new AutoCompleteFieldCleared(searchBox_OnClear);
        pnlBillingAtty.Controls.Add(searchBox);
    }


    AutoCompleteFieldMode searchBox_OnSearchRequest(IAutoCompleteField sender, string searchText)
    {
        AutoCompleteFieldMode mode = AutoCompleteFieldMode.ShowResults;
        if (!ValidEmployeeSelection(searchText))
            mode = AutoCompleteFieldMode.ShowSearchBox;

        _SessionData.MemoData.RespAttID = ParseSearchRequestTextForNumber(searchText);
        return mode;
    }

    AutoCompleteFieldMode searchBox_OnClear(IAutoCompleteField sender)
    {
        _SessionData.MemoData.RespAttID = null;
        return AutoCompleteFieldMode.ShowSearchBox;
    }
}
