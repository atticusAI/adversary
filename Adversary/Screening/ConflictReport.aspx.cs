﻿using System;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using HH;
using HH.Branding;
using HH.UI.FileList;
using HH.SQL;
using System.Data.SqlClient;


public partial class ConflictReport : ScreeningBasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        _MasterPage.PostBackEvent += new MasterPagePostBackEventHandler(_MasterPage_PostBackEvent);
        //fcConflict.FileListTemplate = new FileListTemplate();
        //fcConflict.FileDirectory = GetUploadDirectoryURL(_SessionData.MemoData.ScrMemID);
        fcConflict.FileListItemDataBound += new RepeaterItemEventHandler(base.FileList_ItemDataBound);
        fcConflict.OnFileUploaded += new FileUploaded(fc_OnFileUploaded);
        fcConflict.OnFileEditClick += new FileEditClick(fc_OnFileEditClick);
        fcConflict.OnFileDeleteClick += new FileDeleteClick(fc_OnFileDeleteClick);
        fcConflict.OnFileEditSubmit += new FileEditSubmit(fc_OnFileEditSubmit);
        fcConflict.EnableDescription = false;
        fcConflict.FileDescription = "Conflict Report";

        rblConflict.Items[0].Attributes.Add("onclick", "setAccordianPane('" + accConflict.ClientID + "', 0);");
        rblConflict.Items[1].Attributes.Add("onclick", "setAccordianPane('" + accConflict.ClientID + "', 1);");

        BindFileControl();

        if (IsPostBack)
            return;

        if (!string.IsNullOrEmpty(_SessionData.MemoData.ConflictDesc))
            tbConflictDesc.Text = _SessionData.MemoData.ConflictDesc;

        if (_SessionData.MemoData.ConflictReport != null)
        {
            int index = ((bool)_SessionData.MemoData.ConflictReport) ? 0 : 1;
            rblConflict.Items[index].Selected = true;
            accConflict.SelectedIndex = index;
            fcConflict.FileListHeader = (_SessionData.MemoData.ConflictReportFiles.Count == 0) ? "Please Upload a Conflict Report" : "Attached Conflict Report File(s)";
        }
    }

    private bool _MasterPage_PostBackEvent(object sender, MasterPagePostBackEventArgs e)
    {
        try
        {
            _SessionData.MemoData.ConflictDesc = tbConflictDesc.Text.Trim();
            _SessionData.MemoData.ConflictReport = (rblConflict.SelectedValue == "") ? null : (bool?)Boolean.Parse(rblConflict.SelectedValue);

            using (SqlConnection conn = AdversaryData.GetConnection())
            {
                DatabaseWorker worker = new DatabaseWorker(conn);
                foreach (Attachments attachment in _SessionData.MemoData.Attachments)
                    DoDataSave(attachment, worker);
                DoDataSave(_SessionData.MemoData, worker);
            }
            return true;
        }
        catch (Exception ex)
        {
            _MasterPage.ShowError(ex);
            return false;
        }
    }

    private void BindFileControl()
    {
        fcConflict.DataSource = _SessionData.MemoData.ConflictReportFiles;
        fcConflict.DataBind();
    }

    private bool fc_OnFileUploaded(IFileListControl sender, FileUpload file, string description)
    {
        if (!FileListControl_OnFileUploaded(sender, file, description))
            return false;

        BindFileControl();
        return true;
    }

    private bool fc_OnFileEditClick(IFileListControl sender, object dataItem)
    {
        return FileListControl_OnFileEditClick(sender, dataItem);
    }

    private bool fc_OnFileDeleteClick(IFileListControl sender, object dataItem)
    {
        if (!FileListControl_OnFileDeleteClick(sender, dataItem))
            return false;

        BindFileControl();
        return true;
    }

    private bool fc_OnFileEditSubmit(IFileListControl sender, object dataItem, string newFileName, string description)
    {
        if (!FileListControl_OnFileEditSubmit(sender, dataItem, newFileName, description))
            return false;

        //rebind the repeaters
        BindFileControl();
        return true;
    }

}
