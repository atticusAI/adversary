﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_ApprovalTable.ascx.cs" Inherits="ApprovalTable" %>

<table width="100%" border="1" cellspacing = "0">
    <tr>
        <td width="15%"><u><strong>Routing</strong></u></td>
        <td width="25%"><u><strong>Approval</strong></u></td>
        <td width="10%"><u><strong>Date</strong></u></td>
        <td rowspan="5" style="vertical-align: top; background-color: Silver;">
        
            <table width="100%" border="1" cellspacing="0">
                <tr>
	                <td colspan="2">
	                    <strong>Adversary Use Only:</strong>
	                </td>
                </tr>
                <tr>
                    <td style="width: 35%;">
	                    Approved: 
	                </td>
                    <td>
	                    &nbsp;<asp:Label ID="_lblAdvSignature" runat="server"></asp:Label>
	                </td>
	            </tr>
                <tr>			
	                <td>
	                    Client/Matter: 
	                </td>
	                <td>
	                    &nbsp;<asp:Label ID="_lblCM" runat="server"></asp:Label>
	                </td>
                </tr>
                <tr>			
	                <td>
	                    Opened:
	                </td>
	                <td>
	                    &nbsp;<asp:Label ID="_lblOpened" runat="server"></asp:Label>
	                </td>
                </TR>
                <tr>
	                <td>
	                    Conflicts:
	                </td>
	                <td>
	                    &nbsp;<asp:Label ID="_lblConflicts" runat="server"></asp:Label>
	                </td>	
                </tr>
                <tr>			
	                <td>
	                    Fee Splits:
	                </td>
	                <td>
	                    &nbsp;<asp:Label ID="_lblFeeSplits" runat="server"></asp:Label>
	                </td>
                </tr>
                <tr>			
	                <td>
	                    Final Check:
	                </td>
	                <td>
	                    &nbsp;<asp:Label ID="_lblFinalCheck" runat="server"></asp:Label>
	                </td>
                </tr>
                <!--<tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>-->
            </table>
        
        </td>
    </tr>
    <tr>
        <td style="white-space:nowrap;">
            <strong>Administrative Partner</strong>
        </td>
        <td>
            &nbsp;<asp:Label ID="_lblAPName" runat="server"></asp:Label>
        </td>
        <td>
            &nbsp;<asp:Label ID="_lblAPDate" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="white-space:nowrap;">
            <strong>Practice Group Leader</strong>
        </td>
        <td>
            &nbsp;<asp:Label ID="_lblPGMName" runat="server"></asp:Label>
        </td>
        <td>
            &nbsp;<asp:Label ID="_lblPGMDate" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <strong>Approval for A/R over 90 days</strong>
        </td>
        <td>
            &nbsp;<asp:Label ID="_lblARName" runat="server"></asp:Label>
        </td>
        <td>
            &nbsp;<asp:Label ID="_lblARDate" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <strong>Exception approval for signed engagement letter</strong>
        </td>
        <td>
            &nbsp;<asp:Label ID="_lblExceptionName" runat="server"></asp:Label>
        </td>
        <td>
            &nbsp;<asp:Label ID="_lblExceptionDate" runat="server"></asp:Label>
        </td>
    </tr>
</table>
	    