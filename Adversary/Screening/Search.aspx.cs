﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Web.UI;
using HH.Branding;
using HH.SQL;
using HH.UI;
using HH.Utils;
using HH.UI.AutoComplete;


public partial class Search : ScreeningBasePage
{

    protected enum SearchRequestDataField
    {
        ClientNumber,
        AttEntering,
        PersonnelID,
        MatterName,
        ClientMatterNumber,
        ScrMemID
    }


    private readonly string CONTEXT_VS_KEY = "_contextKey";
    private readonly string RESULTS_KEY = "_results";

    private CMSData _cms = CMSData.GetInstance();
    private DataTable _results;

    private bool _approval = false;
    private bool _admin = false;


    protected void Page_Load(object sender, EventArgs e)
    {
        _approval = (Request.QueryString["type"] == "approve");
        _admin = (Request.QueryString["view"] == "admin" && _SessionData.AdminLogin.IsAdversary.HasValue && _SessionData.AdminLogin.IsAdversary.Value);
        if (!IsPostBack && (!_approval || !_admin))
            ClearMemo();

        _divOldSiteNotice.Visible = (!_approval && !_admin) && (DateTime.Now >= DateTime.Parse("12/1/2009"));
        _mlResults.Mode = (_admin) ? ScreeningMemoListMode.AdminSearch : (_approval) ? ScreeningMemoListMode.ApprovalSearch : ScreeningMemoListMode.Search;

        _ucScrMemID.SearchRequest += new AutoCompleteSearchRequest(SearchRequest);
        _ucClient.SearchRequest += new AutoCompleteSearchRequest(SearchRequest);
        _ucAtty.SearchRequest += new AutoCompleteSearchRequest(SearchRequest);
        _ucEmp.SearchRequest += new AutoCompleteSearchRequest(SearchRequest);
        _ucCMNum.SearchRequest += new AutoCompleteSearchRequest(SearchRequest);
        _ucMatterName.SearchRequest += new AutoCompleteSearchRequest(SearchRequest);

        
    }

    #region GRID VIEW
    


    protected string GetBoundContents(IDataItemContainer container, SearchRequestDataField dataField)
    {

        try
        {
            string df = dataField.ToString();
            object value = ((DataRowView)container.DataItem).Row[df];
            if (value == null)
                return string.Empty;

            string text = value.ToString();

            switch (dataField)
            {
                case SearchRequestDataField.AttEntering:
                    text = GetEmployeeDisplayName(value);
                    break;
                case SearchRequestDataField.PersonnelID:
                    text = GetEmployeeDisplayName(value);
                    break;
                case SearchRequestDataField.ClientNumber:
                    text = GetClientDisplayName(value);
                    break;
            }

            string contextKey = (string)ViewState[CONTEXT_VS_KEY];
            if (contextKey == df)
                text = "<span style='color: blue;'>" + text + "</span>";

            return text;
        }
        catch
        {
            return string.Empty;
        }
    }


    

    #endregion

    #region SEARCH HANDLERS
    AutoCompleteFieldMode SearchRequest(IAutoCompleteField sender, string searchText)
    {
        ClearForm();
        sender.Text = searchText;
        ViewState[CONTEXT_VS_KEY] = sender.ContextKey;
        return AutoCompleteFieldMode.ShowSearchBox;
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (ViewState[CONTEXT_VS_KEY] == null)
            {
                _MasterPage.FeedbackMessage = "Please type your search criteria";
                _MasterPage.FeedbackMode = MasterPageFeedbackMode.Error;
                return;
            }

            object searchData = null;
            string searchText = string.Empty;
            string searchType = string.Empty;
            string contextKey = ViewState[CONTEXT_VS_KEY].ToString();
            SearchRequestDataField requestType = (SearchRequestDataField)Enum.Parse(typeof(SearchRequestDataField), contextKey);

            switch (requestType)
            {
                case SearchRequestDataField.AttEntering:
                    searchData = new Memo();
                    searchText = _ucAtty.Text.Trim();
                    searchType = "employee";
                    if (searchText.Length > 2)
                        HandleEmployeeSearchRequest(_ucAtty, ref searchText);
                    break;
                case SearchRequestDataField.PersonnelID:
                    searchData = new Memo();
                    searchText = _ucEmp.Text.Trim();
                    searchType = "employee";
                    if (searchText.Length > 2)
                        HandleEmployeeSearchRequest(_ucEmp, ref searchText);
                    break;
                case SearchRequestDataField.ClientNumber:
                    searchData = new Memo();
                    searchText = _ucClient.Text.Trim();
                    searchType = "client";
                    if (searchText.Length > 2)
                        HandleClientSearchRequest(_ucClient, ref searchText);
                    break;
                case SearchRequestDataField.ClientMatterNumber:
                    searchData = new ClientMatterNumber();
                    searchText = _ucCMNum.Text.Trim();
                    searchType = "client matter number";
                    break;
                case SearchRequestDataField.MatterName:
                    searchData = new Memo();
                    searchText = _ucMatterName.Text.Trim();
                    searchType = "matter name";
                    break;
                case SearchRequestDataField.ScrMemID:
                    searchData = new Memo();
                    searchText = _ucScrMemID.Text.Trim();
                    Regex hasAlpha = new Regex("[^0-9]");
                    if (hasAlpha.IsMatch(searchText))
                    {
                        _MasterPage.FeedbackMessage = "Please enter a valid screening memo number";
                        _MasterPage.FeedbackMode = MasterPageFeedbackMode.Error;
                        return;
                    }
                    break;
            }

            string propertyName = requestType.ToString();
            if (requestType == SearchRequestDataField.ClientMatterNumber)
                propertyName = "Number"; //Property name is number for client matter number class
            ReflectionUtils.CastAndSetProperty(searchData, propertyName, searchText);
            using (SqlConnection conn = AdversaryData.GetConnection())
            {
                DatabaseWorker worker = new DatabaseWorker(conn);
                _results = worker.SearchToDataTable(searchData);
            }
            _results.DefaultView.Sort = "ScrMemID DESC";
            ViewState[RESULTS_KEY] = _results;
            if (_results.Rows.Count == 0)
            {
                _MasterPage.FeedbackMessage = "No search results found for " + searchType + " '" + searchText + "'";
                _MasterPage.FeedbackMode = MasterPageFeedbackMode.Success;
                return;
            }


            //check for a single match and load the memo if one exists, need the LINQ query since the search procedure returns both partial and complete matches in some cases
            var query = from m in _results.AsEnumerable()
                        where m.Field<object>(requestType.ToString()).ToString() == searchText
                        select m;
            
            _mlResults.DataSource = _results;
            _mlResults.DataBind();
            
            _modalResults.Show();
        }
        catch (Exception ex)
        {
            _MasterPage.ShowError(ex);
        }
    }
    private void HandleEmployeeSearchRequest(IAutoCompleteField tb, ref string searchText)
    {
        if (ValidEmployeeSelection(searchText))
        {
            searchText = ParseSearchRequestTextForNumber(searchText);
            return;
        }

        string[] list = GetEmployeeList(searchText, 100, string.Empty);
        if (list.Length == 1)
        {
            tb.Text = list[0];
            searchText = ParseSearchRequestTextForNumber(list[0]);
        }
    }
    private void HandleClientSearchRequest(IAutoCompleteField tb, ref string searchText)
    {
        if (ValidClientSelection(searchText))
        {
            searchText = ParseSearchRequestTextForNumber(searchText);
            return;
        }

        string[] list = GetClientList(searchText, 100, string.Empty);
        if (list.Length == 1)
        {
            tb.Text = list[0];
            searchText = ParseSearchRequestTextForNumber(list[0]);
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        ClearForm();
    }

    protected void lbClose_Click(object sender, EventArgs e)
    {
        _modalResults.Hide();
    }

    #endregion


    private void ClearForm()
    {
        _ucAtty.Text = string.Empty;
        _ucClient.Text = string.Empty;
        _ucCMNum.Text = string.Empty;
        _ucEmp.Text = string.Empty;
        _ucMatterName.Text = string.Empty;
        _ucScrMemID.Text = string.Empty;
        ViewState.Remove(CONTEXT_VS_KEY);
        ViewState.Remove(RESULTS_KEY);
    }
}
