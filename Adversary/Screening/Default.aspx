﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Default.aspx.cs" Inherits="Default" MasterPageFile="~/Master.master" %>
<%@ Register TagPrefix="UC" TagName="MemoList" Src="~/Screening/_ScreeningMemoList.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="Content" ContentPlaceHolderID="UpperContentPlaceHolder" runat="server">

<h4>Please choose an option below:</h4>
<dl> 
    <dt><strong><asp:LinkButton ID="lbNewType1" runat="server" Text="Start a New Matter for a New Client" OnClick="lbNew_Click" CommandArgument="1"></asp:LinkButton></strong></dt>
    <dt><strong><asp:LinkButton ID="lbNewType2" runat="server" Text="Start a New Matter for an Existing Client" OnClick="lbNew_Click" CommandArgument="2"></asp:LinkButton></strong></dt>
    <dt><strong><asp:LinkButton ID="lbNewType3" runat="server" Text="Start a Pro Bono Screening Memo" OnClick="lbNew_Click" CommandArgument="3"></asp:LinkButton></strong></dt>
    <dt><strong><asp:LinkButton ID="lbNewType5" runat="server" Text="Start Legal Work for H&H or an Employee Screening Memo" OnClick="lbNew_Click" CommandArgument="5"></asp:LinkButton></strong></dt>
    <%--commented by A Reimer 03/21/12 for 'Rejected to Modification Required' change --%>
    <%--<dt><strong><asp:LinkButton ID="lbNewType6" runat="server" Text="Start a Rejected Matter Screening Memo" OnClick="lbNew_Click" CommandArgument="6"></asp:LinkButton></strong></dt>--%>
    <%-- blmcgee@hollandhart.com 2014-01-10 rolling back the previous changes per vabecker@hollandhart.com --%>
    <%--<dt><strong><asp:LinkButton ID="lbNewType6" runat="server" Text="Start a Modification Required Screening Memo" OnClick="lbNew_Click" CommandArgument="6"></asp:LinkButton></strong></dt>--%>
    <dt><strong><asp:LinkButton ID="lbNewType6" runat="server" Text="Start a Rejected Matter Screening Memo" OnClick="lbNew_Click" CommandArgument="6"></asp:LinkButton></strong></dt>
</dl>
<br />
<dl>
    <dt><strong><a href="Search.aspx">Search for an existing Screening Memo</a></strong></dt>
</dl>

<dl>
    <dt>
        <asp:UpdatePanel ID="upMyMemos" runat="server">
        
            <ContentTemplate>
                
                <asp:LinkButton ID="lbMyMemos" runat="server" Text="Show my list of previously created Screening Memos" Font-Bold="true" CommandArgument="show" OnClick="lbMyMemos_Click"></asp:LinkButton>&nbsp;&nbsp;<img src="../_css/nothing.gif" id="imgMyMemos" runat="server" alt="expand/collapse" /><br />
                <ajax:CollapsiblePanelExtender ID="cpMyMemos" runat="Server"
                    TargetControlID="pnlMyMemos"
                    CollapsedSize="0"
                    Collapsed="True"
                    ExpandControlID="lbMyMemos"
                    CollapseControlID="lbMyMemos"
                    AutoCollapse="False"
                    AutoExpand="False"
                    ScrollContents="False"
                    CollapsedText="Show Details..."
                    ExpandedText="Hide Details" 
                    ImageControlID="imgMyMemos"
                    ExpandedImage="~/_css/expand.jpg"
                    CollapsedImage="~/_css/collapse.jpg"
                    ExpandDirection="Vertical" />

                <asp:Panel ID="pnlMyMemos" runat="server" Width="90%">
                    <asp:Label ID="lblMyMemosEmpty" runat="server" ForeColor="Blue" Font-Bold="true"></asp:Label>
                    <UC:MemoList ID="mlMyMemoResults" runat="server" Mode="UserList" />  
                </asp:Panel>
                
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="lbMyMemos" EventName="Click" />
            </Triggers>
        
        </asp:UpdatePanel>
    </dt>
</dl>

</asp:Content>