﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="ConflictReport.aspx.cs" Inherits="ConflictReport" MasterPageFile="~/Master.master" %>
<%@ Register TagName="FileListControl" TagPrefix="UC" Src="~/_usercontrols/FileListControl.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="Content" ContentPlaceHolderID="UpperContentPlaceHolder" runat="server">

Please upload a conflict report or specify why one will not be attached to this memo.<br />
<br />
<asp:RadioButtonList ID="rblConflict" runat="server" RepeatDirection="Vertical" AutoPostBack="false">
    <asp:ListItem Text="Upload a conflict report" Value="True" Selected="True"></asp:ListItem>
    <asp:ListItem Text="A conflict report will not be attached to this memo" Value="False"></asp:ListItem>
</asp:RadioButtonList>
<ajax:Accordion ID="accConflict" runat="server" 
                AutoSize="None"
                FadeTransitions="true"
                TransitionDuration="80"
                FramesPerSecond="80"
                SuppressHeaderPostbacks="true"
                RequireOpenedPane="false" 
                SelectedIndex="0">
    <Panes>
        
        <ajax:AccordionPane runat="server" ID="conlictYes">
            <Content>
                <UC:FileListControl ID="fcConflict" runat="server" />
            </Content>
        </ajax:AccordionPane>
        
        <ajax:AccordionPane runat="server" ID="conlictNo">
            <Content>
                <br />
                Reason a conflict report will not be included<br />
                <asp:TextBox ID="tbConflictDesc" runat="server" TextMode="MultiLine" Rows="5" CssClass="sbControl"></asp:TextBox>
            </Content>
        </ajax:AccordionPane>
        
    </Panes>
</ajax:Accordion>



</asp:Content>