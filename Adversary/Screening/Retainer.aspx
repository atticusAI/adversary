﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Retainer.aspx.cs" Inherits="Retainer" MasterPageFile="~/Master.master" %>


<asp:Content ID="Content" ContentPlaceHolderID="UpperContentPlaceHolder" runat="server">

    <asp:UpdatePanel ID="upRetainer" runat="server">
        
        <ContentTemplate>
        
            <table class="screenTable">
                <tr>
                    <td class="formBuilderCapionText">
                        Will this matter have a retainer (new matters require a $5000 retainer)?
                    </td>
                    <td>
                        &nbsp;&nbsp;<asp:PlaceHolder ID="phTooltip" runat="server"></asp:PlaceHolder>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:RadioButtonList ID="rblHasRetainer" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="rblHasRetainer_SelectedIndexChanged" AutoPostBack="true" >
                            <asp:ListItem Text="Yes" Value="True"></asp:ListItem>
                            <asp:ListItem Text="No" Value="False"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr id="trSmallRetainerQuestion" runat="server" visible="false">
                    <td class="formBuilderCapionText" colspan="2">
                        <br />
                        Will the retainer be less than $5000?
                    </td>
                </tr>
                <tr id="trSmallRetainer" runat="server" visible="false">
                    <td colspan="2">
                        <asp:RadioButtonList ID="rblSmallRetainer" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="rblSmallRetainer_SelectedIndexChanged" AutoPostBack="true" >
                            <asp:ListItem Text="Yes" Value="True"></asp:ListItem>
                            <asp:ListItem Text="No" Value="False"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <br />

            <asp:Panel ID="pnlNoRetainer" runat="server" Visible="false">
                
                Please explain why no retainer will be required<br />
                <asp:TextBox ID="tbNoRetainer" runat="server" TextMode="MultiLine" Rows="4" CssClass="sbControl"></asp:TextBox>  
                
            </asp:Panel>

            <asp:Panel ID="pnlSmallRetainer" runat="server" Visible="false">
                
                Please explain why the retainer is less than $5000<br />
                <asp:TextBox ID="tbSmallRetainer" runat="server" TextMode="MultiLine" Rows="4" CssClass="sbControl"></asp:TextBox>  
                
            </asp:Panel>

            <asp:Panel ID="pnlRetainerDesc" runat="server" Visible="false">
            
                <br />
                <br />
                Previous retainer description for this memo:<br />
                <asp:Label ID="lblRetainerDesc" runat="server"></asp:Label>
                
            </asp:Panel>
            
        </ContentTemplate>
        
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="rblHasRetainer" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="rblSmallRetainer" EventName="SelectedIndexChanged" />
        </Triggers>
        
    </asp:UpdatePanel>

</asp:Content>