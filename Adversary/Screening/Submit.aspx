﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Submit.aspx.cs" Inherits="Submit" MasterPageFile="~/Master.master" %>
<%@ Register TagName="FileListControl" TagPrefix="UC" Src="~/_usercontrols/FileListControl.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>


<asp:Content ID="Content" runat="server" ContentPlaceHolderID="UpperContentPlaceHolder">

<br />
<br />
<dl>
    <dt>
        
        <a href="#" id="lbUpload" runat="server" style="font-weight: bold;">Attach additional files to this Screening Memo</a>&nbsp;&nbsp;<img src="../_css/nothing.gif" id="imgUpload" runat="server" alt="expand/collapse" /><br />
        <ajax:CollapsiblePanelExtender ID="cpUpload" runat="Server"
            TargetControlID="pnlUpload"
            CollapsedSize="0"
            Collapsed="True"
            ExpandControlID="lbUpload"
            CollapseControlID="lbUpload"
            AutoCollapse="False"
            AutoExpand="False"
            ScrollContents="False"
            CollapsedText="Show Details..."
            ExpandedText="Hide Details" 
            ImageControlID="imgUpload"
            ExpandedImage="~/_css/expand.jpg"
            CollapsedImage="~/_css/collapse.jpg"
            ExpandDirection="Vertical" />

        <asp:Panel ID="pnlUpload" runat="server" >
            <UC:FileListControl ID="fcFiles" runat="server" />
            <br />
        </asp:Panel>
        
    </dt>
    
    <dt>
    
        <a id="lbPrint" runat="server" style="font-weight: bold;" target="_blank">View Memo in Print View</a><br />
        
    
    </dt>   
    <dt>
    
        <asp:LinkButton ID="lbSubmit" runat="server" CommandName="submit" Text="Submit Screening Memo for Approval" OnClick="lb_Click" Font-Bold="true" OnClientClick="this.innerHTML += '.....(Submitting, please wait)'"></asp:LinkButton>
        
        
    </dt>
</dl>






</asp:Content>
