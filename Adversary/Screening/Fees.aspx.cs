﻿using System;
using System.Configuration;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using HH.Screen;


public partial class Fees : ScreeningBasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        _MasterPage.Screen.Width = Unit.Percentage(100);
        _fees.OnEntitySubmitted += new HH.Screen.EntitySubmitEventHandler(_fees_OnEntitySubmitted);
        _MasterPage.Screen.Visible = (_SessionData.MemoData.FeeSplitStaffs.Count > 0);
    }

    void _fees_OnEntitySubmitted(HH.Screen.IListContainerControl sender, object dataSource, object entity, HH.Screen.EntitySubmitType type)
    {
        _MasterPage.Screen.Visible = (((IList)dataSource).Count > 0);
    }

}
