﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using HH.Screen;

public partial class Office : ScreeningBasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        try
        {
            if (!string.IsNullOrEmpty(_SessionData.MemoData.OrigOffice) ||
                string.IsNullOrEmpty(_SessionData.MemoData.RespAttOfficeCode))
                return;

            DropDownList ddl = (DropDownList)_MasterPage.Screen.FindControl("OrigOffice");
            ddl.SelectedValue = _SessionData.MemoData.RespAttOfficeCode;
        }
        catch { }
    }

}
