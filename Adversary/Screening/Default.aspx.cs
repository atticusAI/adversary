﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using HH.SQL;
using System.Collections.Generic;
using System.Data.SqlClient;

public partial class Default : ScreeningBasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        
    }

    protected void lbNew_Click(object sender, EventArgs e)
    {
        LinkButton lb = (LinkButton)sender;
        int scrMemType = Int16.Parse(lb.CommandArgument);
        Memo memo = new Memo();
        memo.ScrMemID = 0;
        memo.ScrMemType = scrMemType;
        _SessionData.MemoData = memo;
        _SessionData.MemoLoaded = true;
        Response.Redirect("~/Screening/BasicInfo.aspx?ScrMemType=" + scrMemType);
    }

    protected void lbMyMemos_Click(object sender, EventArgs e)
    {
        try
        {
            if (lbMyMemos.CommandArgument == "hide")
            {
                lbMyMemos.CommandArgument = "show";
                return;
            }
            else
                lbMyMemos.CommandArgument = "hide";

            using (SqlConnection conn = AdversaryData.GetConnection())
            {
                DatabaseWorker worker = new DatabaseWorker(conn);
                Memo searchPers = new Memo();
                searchPers.PersonnelID = _SessionData.LoginEmpCode;
                DataTable resultsPers = worker.SearchToDataTable(searchPers);
                Memo searchAtty = new Memo();
                searchAtty.AttEntering = _SessionData.LoginEmpCode;
                DataTable resultsAtty = worker.SearchToDataTable(searchAtty);

                DataTable results = new DataTable();
                results.Merge(resultsPers);
                results.Merge(resultsAtty);

                IEnumerable<DataRow> query = (from r in results.AsEnumerable()
                                              select r).Distinct(new IsEqual());
                query = query.OrderBy(r => r.Field<int>("ScrMemID")).Reverse();


                //var distinctNames = (from row in results
                //                     select row).Distinct();


                if (query.Count() == 0)
                {
                    lblMyMemosEmpty.Text = "<br/>No memos found for user with employee ID " + _SessionData.LoginEmpCode;
                    mlMyMemoResults.Visible = false;
                    return;
                }

                results = query.CopyToDataTable<DataRow>();
                //lblMyMemoResults.Text = results.Rows.Count + " memos found";
                mlMyMemoResults.DataSource = results;
                mlMyMemoResults.DataBind();
            }
        }
        catch (Exception ex)
        {
            HandleError(ex);
        }
    }

    private class IsEqual : IEqualityComparer<DataRow>
    {

        #region IEqualityComparer<DataRow> Members

        public bool Equals(DataRow x, DataRow y)
        {
            return x.Field<int>("ScrMemID") == y.Field<int>("ScrMemID");
        }

        public int GetHashCode(DataRow obj)
        {
            return obj.ToString().GetHashCode(); 
        }

        #endregion
    }
}
