﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_ScreeningMemoList.ascx.cs" Inherits="Screening_ScreeningMemoList" %>

<asp:Panel ID="_pnlResultsContainer" runat="server" ScrollBars="Auto" Height="360px">

         
    <asp:GridView ID="_gvResults" runat="server" 
                AutoGenerateColumns="false" 
                GridLines="None"
                BorderStyle="solid" 
                BorderWidth="1"
                BorderColor="#F0F0F0"
                RowStyle-BackColor="#F0F0F0" 
                AlternatingRowStyle-BackColor="White"
                RowStyle-Height="30px" 
                Width="98%">
        <Columns>
            
            <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                <ItemTemplate>
                        <a id="_aView" runat="server">View</a>
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Memo #" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                <ItemTemplate><%# GetBoundContents(Container, ListDataField.ScrMemID)%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Matter Name" HeaderStyle-Wrap="false" >
                <ItemTemplate><%# GetBoundContents(Container, ListDataField.MatterName)%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Client" HeaderStyle-Wrap="false"  >
                <ItemTemplate><%# GetBoundContents(Container, ListDataField.ClientNumber)%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Attorney" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" >
                <ItemTemplate><%# GetBoundContents(Container, ListDataField.AttEntering)%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Entering Employee" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" >
                <ItemTemplate><%# GetBoundContents(Container, ListDataField.PersonnelID)%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Client Matter #" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" >
                <ItemTemplate><%# GetBoundContents(Container, ListDataField.ClientMatterNumber)%></ItemTemplate>
            </asp:TemplateField>
        
        </Columns>

    </asp:GridView>

</asp:Panel>

<div style="margin: 5px 0 10px 5px;">
    <asp:Label ID="_lblCount" runat="server" ForeColor="#68849F" Font-Bold="true"></asp:Label>
</div>
    