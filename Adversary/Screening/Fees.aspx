﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Fees.aspx.cs" Inherits="Fees" MasterPageFile="~/Master.master" %>
<%@ Register TagName="Staff" TagPrefix="UC" Src="_Staff.ascx" %>

<asp:Content ID="Content" ContentPlaceHolderID="UpperContentPlaceHolder" runat="server">

    <strong>If you are an associate and you claim a fee split, please be sure to include the basis for the Origination, or your request will be denied.</strong>

    <UC:Staff ID="_fees" runat="server" 
            Mode="Fees" 
            Caption="Employee numbers of person(s) claiming origination"
            InformationToolTip="Click the Add an Associate link to add an associate's name to the database." />
    
             <!--"<em>Employee numbers of person(s) claiming origination</em>",
     InformationToolTip="Click the Add a new record link to add an associate's name to the database. " +
                        "Identify multiple associates if applicable. "-->

</asp:Content>