﻿using System;
using System.Web.UI;
using HH.UI;
using HH.UI.AutoComplete;

public partial class ClientDetails : ScreeningBasePage
{
    enum Mode
    {
        New,
        Existing
    }

    private Mode _mode = Mode.New;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["ScrMemType"] == "1")
            return;

        pnlClientType.Visible = true;

        if (IsPostBack && rblClientType.SelectedValue == "New")
            _SessionData.MemoData.ClientNumber = null;

        AutoCompleteField searchBox = BuildClientSearchAutoComplete("sbClientNumber", "ClientNumber", _SessionData.MemoData.ClientNumber);
        searchBox.Clear += new AutoCompleteFieldCleared(ClientSearchClear);
        searchBox.SearchRequest += new AutoCompleteSearchRequest(ClientSearchSelected);
        phExisting.Controls.Add(new LiteralControl("<strong>Client Name</strong>"));
        phExisting.Controls.Add(searchBox);

        if (!string.IsNullOrEmpty(_SessionData.MemoData.ClientNumber) ||
            rblClientType.SelectedValue == "Existing")
            _mode = Mode.Existing;

        rblClientType.SelectedValue = _mode.ToString();

        switch (_mode)
        {
            case Mode.New:
                _SessionData.MemoData.ClientNumber = null;
                _MasterPage.Screen.Visible = true;
                phExisting.Visible = false;
                break;

            case Mode.Existing:
                _MasterPage.Screen.Visible = false;
                phExisting.Visible = true;
                if (!string.IsNullOrEmpty(_SessionData.MemoData.ClientNumber))
                    upAR.ContentTemplateContainer.Controls.Add(BuildARTable(_SessionData.MemoData.ClientNumber));
                break;
        }
    }

    private AutoCompleteFieldMode ClientSearchClear(IAutoCompleteField sender)
    {
        _SessionData.MemoData.ClientNumber = null;
        _MasterPage.FeedbackMessage = string.Empty;
        upAR.ContentTemplateContainer.Controls.Clear();
        return AutoCompleteFieldMode.ShowSearchBox;
    }

    private AutoCompleteFieldMode ClientSearchSelected(IAutoCompleteField sender, string text)
    {
        string clientNumber = ParseSearchRequestTextForNumber(text);
        //if (!ValidClientSelection(text))
        if (string.IsNullOrEmpty(clientNumber))
        {
            _MasterPage.FeedbackMessage = "Client '" + text + "' was not found";
            _MasterPage.FeedbackMode = HH.Branding.MasterPageFeedbackMode.Warning;
            return AutoCompleteFieldMode.ShowSearchBox;
        }

        //string number = text;
        //number = number.Substring(number.IndexOf("(") + 1);
        //number = number.Substring(0, number.IndexOf(")"));

        _SessionData.MemoData.ClientNumber = clientNumber;
        _SessionData.MemoData.Company = null;
        //upAR.ContentTemplateContainer.Controls.Add(BuildARTable(_SessionData.MemoData.ClientNumber));

        return AutoCompleteFieldMode.ShowResults;
    }
}
