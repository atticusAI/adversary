﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HH.Screen;
using HH.UI.FileList;
using System.Text;

public partial class _ScreeningMemoReport : System.Web.UI.UserControl
{
    private CMSData _cms = CMSData.GetInstance();
    private readonly string NO_DATA = "<em><strong>&nbsp;- No Data Available -</strong></em><br/>";
    private bool _locked = false;

    public enum Mode
    {
        Summary,
        Print
    }

    public Mode ReportMode { get; set; }
    public SiteMapNode Node { get; set; }
    public Memo DataSource { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public override void DataBind()
    {
        if ((Node.Title == "Final Review") ||
            (Node.Title == "Docketing" && !DataSource.DocketingInfoRequired()))
            return;

        _locked = DataSource.IsLocked() || (DataSource.Tracking.StatusTypeID == TrackingStatusType.Completed);
        AddSummary(DataSource);
    }

    private void AddSummary(object dataSource)
    {
            
        string screenId = Node.Url;
        screenId = screenId.Substring(screenId.LastIndexOf('/'));
        screenId = screenId.Replace("/", "");

        PlaceHolder ph = new PlaceHolder();
        ph.ID = "phSummary___" + screenId;

        if (ReportMode == Mode.Summary)
        {
            LinkButton link = new LinkButton();
            //link.NavigateUrl = Node.Url;
            link.PostBackUrl = Node.Url + (Node.Url.Contains("?") ? "&ScrMemID=" : "?ScrMemID=") + ((Memo)dataSource).ScrMemID.ToString();
            link.ID = screenId + "Link";
            link.Text = Node.Title;
            link.ToolTip = Node.Description;
            link.Style.Add("font-weight", "bold");
            link.Enabled = !_locked;        
            ph.Controls.Add(new LiteralControl("<br/>"));
            ph.Controls.Add(link);
            ph.Controls.Add(new LiteralControl("<br/><br/>"));
        }

        
        switch (Node.Title)
        {
            case "Basic Information":
                ph.Controls.Add(BuildBasicInfo(screenId));
                break;

            case "Client Information":
                ph.Controls.Add(BuildClientInfo());
                break;

            case "Matter Information":
                ph.Controls.Add(BuildMatterInfo(screenId));
                break;

            case "Docketing":
                ph.Controls.Add(BuildDocketingInfo());
                break;

            case "Client Details":
                ph.Controls.Add(BuildClientInfo());
                break;

            case "Legal Work Information":
                ph.Controls.Add(BuildClientInfo());
                break;

            case "Client Type":
                ph.Controls.Add(BuildClientInfo());
                break;

            case "Contact Information":
                ph.Controls.Add(BuildContactInfo());
                break;

            case "Staffing Information":
                ph.Controls.Add(BuildStaffReport(screenId, DataSource.Staffing, true));
                break;

            case "Originating Office":
                ph.Controls.Add(BuildOriginatingOffice(screenId));
                break;

            case "Fee Split":
                ph.Controls.Add(BuildStaffReport(screenId, DataSource.FeeSplitStaffs, false));
                break;

            case "Engagement Letter":
                ph.Controls.Add(BuildEngagementLetterReport());
                break;

            case "Conflict Report":
                string reason = (DataSource.ConflictReport.HasValue && DataSource.ConflictReport.Value) ? null : DataSource.ConflictDesc;
                ph.Controls.Add(BuildFileReport(Node.Title, DataSource.ConflictReportFiles, reason));
                break;

            case "Related Parties":
                ph.Controls.Add(BuildPartyReport(DataSource.RelatedParties));
                break;

            case "Adverse Parties":
                ph.Controls.Add(BuildPartyReport(DataSource.AdverseParties));
                break;

            case "Retainer":
                ph.Controls.Add(BuildRetainerReport());
                break;

            ///added new case by A Reimer 03/21/12 for 'Rejected to Modification Required' change 
            case "Rejection Letter":
            case "Request for Modification":
                ph.Controls.Add(BuildRejectionLetterReport());
                break;
            

            default:
                IReportBuilder rb = LoadReportBuilder();
                rb.DataSource = dataSource;
                rb.ScreenID = screenId;
                rb.DataBind();
                ph.Controls.Add((UserControl)rb);
                break;
        }

        this.Controls.Add(ph);
    }

    private Control BuildRejectionLetterReport()
    {
        if (DataSource.NoRejectionLetter.HasValue && DataSource.NoRejectionLetter.Value)
        {
            IReportBuilder rb = LoadReportBuilder();
            ///commented out by A Reimer 03/21/12 for 'Rejected to Modification Required' change
            //rb.AddRow("Reason a Rejection Letter will not be attached", (string.IsNullOrEmpty(DataSource.NoRejectionLetterReason)) ? NO_DATA : DataSource.NoRejectionLetterReason);
            //blmcgee@hollandhart.com 2014-01-10
            // rb.AddRow("Reason a Request for Modification Letter will not be attached", (string.IsNullOrEmpty(DataSource.NoRejectionLetterReason)) ? NO_DATA : DataSource.NoRejectionLetterReason);
            rb.AddRow("Reason a Rejection Letter will not be attached", (string.IsNullOrEmpty(DataSource.NoRejectionLetterReason)) ? NO_DATA : DataSource.NoRejectionLetterReason);
            return (UserControl)rb;
        }
        else
            return BuildFileReport(Node.Title, DataSource.RejectionLetterFiles, null);
    }

    private Control BuildContactInfo()
    {
        IReportBuilder rb = LoadReportBuilder();

        foreach (ClientContact c in DataSource.ClientContacts)
        {
            StringBuilder sb = new StringBuilder();
            if (!string.IsNullOrEmpty(c.ContactFName))
                sb.Append(c.ContactFName);
            if (!string.IsNullOrEmpty(c.ContactMName))
            {
                if (sb.Length != 0)
                    sb.Append(" ");
                sb.Append(c.ContactMName);
            }
            if (!string.IsNullOrEmpty(c.ContactLName))
            {
                if (sb.Length != 0)
                    sb.Append(" ");
                sb.Append(c.ContactLName);
            }
            if (!string.IsNullOrEmpty(c.ContactTitle))
            {
                if (sb.Length != 0)
                    sb.Append(" ");
                sb.Append("(" + c.ContactTitle + ")");
            }

            rb.AddRow("Contact", sb.ToString());
        }
        return (UserControl)rb;
    }

    private UserControl BuildBasicInfo(string screenId)
    {
        IReportBuilder rb = LoadReportBuilder();
        rb.DataSource = DataSource;
        rb.ScreenID = screenId;
        rb.DataBind();

        try
        {
            if (!string.IsNullOrEmpty(DataSource.AttEntering))
                rb.AddRow("Attorney Preparing Memo", BasePage.GetEmployeeDisplayName(DataSource.AttEntering));
            if (!string.IsNullOrEmpty(DataSource.PersonnelID))
                rb.AddRow("Input By", BasePage.GetEmployeeDisplayName(DataSource.PersonnelID));
        }
        catch { }

        return (UserControl)rb;
    }

    private UserControl BuildClientInfo()
    {
        IReportBuilder rb = LoadReportBuilder();
        rb.AddRow("Client", BasePage.GetClientDisplayName(DataSource)); //AddClientName(rb);
        if (DataSource.ScrMemType != 5)
            rb.AddRow("Is this client a debtor in a pending bankruptcy?", (DataSource.PendingBankruptcy.HasValue && DataSource.PendingBankruptcy.Value));
        
        
        return (UserControl)rb;
    }

    private Panel BuildPartyReport(List<Party> parties)
    {
        Panel pnlParties = new Panel();
        if (parties.Count == 0)
        {
            if (ReportMode != Mode.Print)
                pnlParties.Controls.Add(new LiteralControl(NO_DATA));
            return pnlParties;
        }

        foreach (Party party in parties)
        {
            IReportBuilder rb = LoadReportBuilder();
            //if (party.PartyType == "Organization")
            //{
            //    rb.AddRow("Organization Name", party.PartyOrganization);
            //}
            //else
            //{
            //    string name = string.Empty;
            //    if (!string.IsNullOrEmpty(party.PartyFName))
            //        name += party.PartyFName;
            //    if (!string.IsNullOrEmpty(party.PartyMName))
            //        name += ((!string.IsNullOrEmpty(name)) ? " " : "") + party.PartyMName;
            //    if (!string.IsNullOrEmpty(party.PartyLName))
            //        name += ((!string.IsNullOrEmpty(name)) ? " " : "") + party.PartyLName;
            //    rb.AddRow("Individual Name", name);
            //}
            //string partyType = party.PartyRelationshipName + " (" + party.PartyRelationshipCode + ")";
            //rb.AddRow("Relationship", partyType);

            string name = string.Empty;
            if (party.PartyType == "Organization")
                name = party.PartyOrganization;
            else
            {
                if (!string.IsNullOrEmpty(party.PartyFName))
                    name += party.PartyFName;
                if (!string.IsNullOrEmpty(party.PartyMName))
                    name += ((!string.IsNullOrEmpty(name)) ? " " : "") + party.PartyMName;
                if (!string.IsNullOrEmpty(party.PartyLName))
                    name += ((!string.IsNullOrEmpty(name)) ? " " : "") + party.PartyLName;
            }


            string partyType = party.PartyRelationshipName + " (" + party.PartyRelationshipCode + ")";
            rb.AddRow(partyType, name);
            pnlParties.Controls.Add((UserControl)rb);
            pnlParties.Controls.Add(new LiteralControl("<br/>"));
        }
        return pnlParties;
    }

    private UserControl BuildMatterInfo(string screenId)
    {
        IReportBuilder rb = LoadReportBuilder();
        rb.DataSource = DataSource;
        rb.ScreenID = screenId;
        rb.DataBind();
        try
        {
            TableCell td = rb.Tables[0].Rows[2].Cells[1];
            string pracCode = td.Text;
            td.Text = _cms.GetPracticeName(pracCode) + " (" + pracCode + ")";
        }
        catch { }

        if (DataSource.ScrMemType != 6)
        {
            try
            {

                TableRow trType = null;
                foreach (TableRow row in rb.Tables[0].Rows)
                {
                    if (row.Cells[0].Text.Contains("N Drive Folder Type"))
                    {
                        trType = row;
                        break;
                    }
                }

                //per Vicky's request, omit the N Drive info in print view
                if (ReportMode == Mode.Print)
                {
                    if (trType != null)
                        trType.Visible = false;

                    TableRow trNDrive = null;
                    foreach (TableRow row in rb.Tables[0].Rows)
                    {
                        if (row.Cells[0].Text.Contains("Create an N Drive folder"))
                        {
                            trNDrive = row;
                            break;
                        }
                    }

                    if (trNDrive != null)
                        trNDrive.Visible = false;
                }
                else
                {
                    if (trType != null)
                    {
                        trType.Visible = DataSource.NDrive.HasValue && DataSource.NDrive.Value;
                        if (trType.Visible)
                        {
                            switch (DataSource.NDriveType)
                            {
                                case null:
                                    trType.Cells[1].Text = NO_DATA;
                                    break;

                                case 0:
                                    trType.Cells[1].Text = NO_DATA;
                                    break;

                                case 4:
                                case 5:
                                    trType.Cells[1].Text += "&nbsp;&nbsp;-&nbsp;&nbsp;Grant Access To:<ul>";
                                    foreach (IStaff staff in DataSource.NDriveAccess)
                                    {
                                        try
                                        {
                                            trType.Cells[1].Text += "<li>" + BasePage.GetEmployeeDisplayName(staff.PersonnelID) + "</li>";
                                        }
                                        catch { }
                                    }
                                    trType.Cells[1].Text += "</ul>";
                                    break;
                            }
                        }
                    }
                }
            }
            catch { }
        }

        if (DataSource.ScrMemType != 1 && DataSource.ScrMemType != 3 && DataSource.ScrMemType != 5)
        {
            //if (ReportMode == Mode.Print)
            //    rb.AddRow("<br/><strong>Client</strong><br/><br/>", "<br/><strong>" + BasePage.GetClientDisplayName(DataSource) + "</strong><br/><br/>"); //AddClientName(rb);
            //else
            if (ReportMode != Mode.Print)
                rb.AddRow("Client", BasePage.GetClientDisplayName(DataSource)); //AddClientName(rb);
        
        }
        
        return (UserControl)rb;
    }

    private UserControl BuildDocketingInfo()
    {
        CMSData cms = CMSData.GetInstance();
        IReportBuilder rb = LoadReportBuilder();
        rb.AddRow("Docketing Attorney", string.IsNullOrEmpty(DataSource.DocketingAtty) ? NO_DATA : BasePage.GetEmployeeDisplayName(DataSource.DocketingAtty));
        rb.AddRow("Docketing Staff", string.IsNullOrEmpty(DataSource.DocketingStaff) ? NO_DATA : BasePage.GetEmployeeDisplayName(DataSource.DocketingStaff));

        if (DataSource.DocketingTeam.Count == 0)
        {
            rb.AddRow("Docketing Team", NO_DATA);
            return (UserControl)rb;
        }

        StringBuilder sb = new StringBuilder();
        foreach (IStaff staff in DataSource.DocketingTeam)
            sb.AppendLine(BasePage.GetEmployeeDisplayName(staff.PersonnelID) + "<br/>");
        rb.AddRow("Docketing Team", sb.ToString());
        return (UserControl)rb;
    }

    //private void AddClientName(IReportBuilder rb)
    //{
    //    try
    //    {
    //        if (!string.IsNullOrEmpty(DataSource.ClientName) || !string.IsNullOrEmpty(DataSource.ClientNumber))
    //            rb.AddRowAt(0, "Client", DataSource.ClientName + " (" + DataSource.ClientNumber + ")");
    //    }
    //    catch { }
    //}

    private UserControl BuildOriginatingOffice(string screenId)
    {
        IReportBuilder rb = LoadReportBuilder();
        rb.DataSource = DataSource;
        rb.ScreenID = screenId;
        rb.DataBind();
        if (!string.IsNullOrEmpty(DataSource.OrigOffice))
            rb.Tables[0].Rows[0].Cells[1].Text = _cms.GetOfficeName(DataSource.OrigOffice);
        return (UserControl)rb;
    }

    private Panel BuildStaffReport(string screenId, IList list, bool isStaffing)
    {
        Panel pnlStaff = new Panel();
        IReportBuilder report = LoadReportBuilder();
        report.DataSource = DataSource;
        report.ScreenID = screenId;
        report.DataBind();
        if (isStaffing)
        {
            string value = NO_DATA;
            if (!string.IsNullOrEmpty(DataSource.RespAttID))
            {
                try
                {
                    value = _cms.GetUserNameByCode(DataSource.RespAttID) + " (" + DataSource.RespAttID + ")";
                }
                catch { }
            }
            report.AddRow("Billing Attorney", value);
        }
        IReportBuilder listReport = LoadReportBuilder();
        listReport.DataSource = list;
        listReport.DataBind();
        pnlStaff.Controls.Add((UserControl)report);
        pnlStaff.Controls.Add(new LiteralControl("<br/>"));
        foreach (Table table in listReport.Tables)
        {
            try
            {
                TableCell td = table.Rows[0].Cells[1];
                string payrollId = td.Text;
                td.Text = _cms.GetUserNameByCode(payrollId) + " (" + payrollId + ")";
            }
            catch { }
        }
        pnlStaff.Controls.Add((UserControl)listReport);
        return pnlStaff;
    }


    private UserControl BuildFileReport(string fileType, List<Attachments> attachments, string reason)
    {
        IReportBuilder rb = LoadReportBuilder();

        if (!string.IsNullOrEmpty(reason))
        {
            rb.AddRow("Reason why a " + fileType + " will not be sent", reason);
        }
        else 
        {
            rb.AddRow("List of attached " + fileType + " files", "&nbsp;");
            TableCell td = rb.Tables[0].Rows[0].Cells[1]; //get the cell for the conflict report and add content as necessary
            if (attachments.Count == 0)
                td.Text = NO_DATA;
            else
                td.Controls.Add(BuildAttachmentList(attachments, string.Empty));
        }
        
        return (UserControl)rb;
    }

    private Panel BuildEngagementLetterReport()
    {
        IReportBuilder rb = LoadReportBuilder();
        Panel pnl = new Panel();
        string reason = (DataSource.EngageLetter.HasValue && DataSource.EngageLetter.Value) ? null : DataSource.EngageDesc;
        pnl.Controls.Add(BuildFileReport("Engagement Letter", DataSource.EngagementLetterFiles, reason));
        pnl.Controls.Add(new LiteralControl("<br/>"));
        pnl.Controls.Add(BuildFileReport("Written Policy", DataSource.WrittenPolicyFiles, null));
        return pnl;

    }

    private UserControl BuildRetainerReport()
    {
        IReportBuilder rb = LoadReportBuilder();
        bool retainer = DataSource.Retainer.HasValue && DataSource.Retainer.Value;
        rb.AddRow("Is there a retainer for this engagement?", (retainer) ? "Yes" : "No");
        if (!retainer)
        {
            rb.AddRow("Reason why no retainer is required", DataSource.RetainerDescNo);
        }
        else
        {
            bool smallRetainer = !string.IsNullOrEmpty(DataSource.RetainerDescYes);
            rb.AddRow("Is the retainer less than $5000?", smallRetainer);
            if (smallRetainer)
                rb.AddRow("Reason why retainer is less than $5000", DataSource.RetainerDescYes);
        }

        if (!string.IsNullOrEmpty(DataSource.RetainerDesc))
            rb.AddRow("Retainer Description", DataSource.RetainerDesc);
            
        if (this.ReportMode == Mode.Print)
        {
            rb.AddRow("If no retainer is requested or if retainer is less than $5000 PGL or AP must sign here:",
                      "X____________________________________________________");
        }

        return (UserControl)rb;
    }

    private Table BuildAttachmentList(List<Attachments> attachments, string caption)
    {
        Table table = new Table();
        TableRow tr;
        TableCell td;

        tr = new TableRow();
        td = new TableCell();
        td.Text = "<em>" + caption + "</em>";
        td.HorizontalAlign = HorizontalAlign.Center;
        tr.Cells.Add(td);
        table.Rows.Add(tr);

        foreach (Attachments attachment in attachments)
        {
            tr = new TableRow();
            td = new TableCell();

            //td.Controls.Add(FileListTemplate.BuildFileListItem(Request.ApplicationPath, attachment));
            attachment.FileURI = ((BasePage)Page).GetAttachmentDownloadURL(attachment);
            td.Controls.Add(new FileListItemControl(attachment));
            td.Controls.Add(new LiteralControl("<br/>"));
            tr.Cells.Add(td);
            table.Rows.Add(tr);
        }

        return table;
    }

    private IReportBuilder LoadReportBuilder()
    {
        IReportBuilder rb = (IReportBuilder)LoadControl("~/_usercontrols/ReportBuilder.ascx");
        rb.ShowNullValues = true;
        if (ReportMode == Mode.Summary)
        {
            rb.CssClass = "gridView";
            rb.RowCssClass = "gridViewRow";
            rb.RowValueCssClass = "reportItemValue";
        }
        else
        {
            rb.Width = Unit.Pixel(800);
            rb.CssClass = string.Empty;
            rb.RowCssClass = string.Empty;
            //rb.RowValueCssClass = string.Empty;
        }
        
        return rb;
    }

}
