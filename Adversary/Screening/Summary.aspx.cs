﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Reflection;
using System.Data.Linq.Mapping;
using HH.Screen;
using HH.Branding;
using HH.UI.FileList;

public partial class Summary : ScreeningBasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        _MasterPage.PreviousButtonEnabled = false;

        if (!_SessionData.MemoLoaded || _SessionData.MemoData.ScrMemType == null)
        {
            _MasterPage.FeedbackMessage = "An error occurred, a memo was not loaded.";
            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Error;
            return;
        }

        _apprTable.Visible = _SessionData.MemoData.Tracking.StatusTypeID != TrackingStatusType.None;

        SiteMapNode node = SiteMap.Providers["Screening"].FindSiteMapNode("~/Screening/Summary.aspx?ScrMemType=" + _SessionData.MemoData.ScrMemType);
        node = node.NextSibling;
        while (node != null)
        {
            _ScreeningMemoReport uc = (_ScreeningMemoReport)LoadControl("~/Screening/_ScreeningMemoReport.ascx");
            uc.ReportMode = _ScreeningMemoReport.Mode.Summary;
            uc.DataSource = _SessionData.MemoData;
            uc.Node = node;
            uc.DataBind();
            _phSummary.Controls.Add(uc);
            node = node.NextSibling;
        }

        var query = from a in _SessionData.MemoData.Attachments
                    where (a.FileDescription != "Conflict Report" &&
                            a.FileDescription != "Engagement Letter" &&
                            a.FileDescription != "Written Policy"  &&
                            a.FileDescription != "Rejection Letter")
                    select a;

        if (query.Count() == 0)
            return;

        Attachments[] attachments = query.ToArray<Attachments>();
        LinkButton link = new LinkButton();
        link.PostBackUrl = "Submit.aspx?ScrMemType=" + _SessionData.MemoData.ScrMemType;
        link.ID = "_lbSubmit";
        link.Text = "Additional Attached Files";
        link.ToolTip = "Final Review";
        link.Style.Add("font-weight", "bold");
        link.Enabled = _SessionData.MemoData.CanSubmit();

        _phSummary.Controls.Add(new LiteralControl("<br/>"));
        _phSummary.Controls.Add(link);
        _phSummary.Controls.Add(new LiteralControl("<hr style='width: 800px;' />"));
            


        foreach (Attachments attachment in attachments)
        {
            attachment.FileURI = GetAttachmentDownloadURL(attachment);
            _phSummary.Controls.Add(new FileListItemControl(attachment));
            _phSummary.Controls.Add(new LiteralControl("<br/>"));
        }
    }
}
