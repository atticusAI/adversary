﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Docketing.aspx.cs" Inherits="Docketing" MasterPageFile="~/Master.master" %>
<%@ Register TagName="Staff" TagPrefix="UC" Src="_Staff.ascx" %>

<asp:Content ID="UpperContent" ContentPlaceHolderID="UpperContentPlaceHolder" runat="server">

<table style="width: 65%;">
    <tr>
        <td style="width: 20%; font-weight: bold;">Docketing Attorney <span style="color: Red; font-size: large;"> * </span></td>
        <td style="width: 80%;"><asp:PlaceHolder ID="_phAtty" runat="server"></asp:PlaceHolder></td>
    </tr>
    <tr>
        <td colspan="2"><br /></td>
    </tr>
    <tr>
        <td style="width: 20%; font-weight: bold;">Docketing Staff <span style="color: Red; font-size: large;"> * </span></td>
        <td style="width: 80%;"><asp:PlaceHolder ID="_phStaff" runat="server"></asp:PlaceHolder></td>
    </tr>
    <tr>
        <td colspan="2">
            <br />
            <br />
            <br />     
            <UC:Staff ID="_docketingTeam" runat="server" 
                Mode="DocketingTeam" 
                Caption="Docketing Team Members<br/><br/>"
                InformationToolTip="Click the Add Staff link to add a staff member's name to the list." />
        
        </td>
    </tr>
</table>
<br />
<span style="color: Red; font-weight: bold;"> * Required Field</span>

</asp:Content>


<asp:Content ID="LowerContent" ContentPlaceHolderID="LowerContentPlaceHolder" runat="server">


</asp:Content>