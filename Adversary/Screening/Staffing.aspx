﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Staffing.aspx.cs" Inherits="StaffingPage" MasterPageFile="~/Master.master" %>
<%@ Register TagName="Staff" TagPrefix="UC" Src="_Staff.ascx" %>

<asp:Content ID="Content" ContentPlaceHolderID="UpperContentPlaceHolder" runat="server">

<table style="width: 100%;">
    <tr>
        <td colspan="2">
            <UC:Staff ID="_staff" runat="server" Caption="Proposed Initial Staffing" Mode="Staff" />
        </td>
    </tr>
    <tr>
        <td width="20%"><strong>Billing Attorney:</strong></td>
        <td><asp:Panel ID="pnlBillingAtty" runat="server"></asp:Panel></td>
    </tr>
</table>


</asp:Content>