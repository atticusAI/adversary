﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using HH.Branding;
using HH.UI;
using HH.UI.AutoComplete;

public partial class CM : ScreeningBasePage
{
    private DropDownList _ddlPracCode;
    private DropDownList _ddlNDriveType;
    private BooleanRadioControl _brcNDrive;
    private _Staff _ucNDriveAccess;

    //added by A Reimer 06/28/2013 for N Drive DOes Apply change request
    private DropDownList _ddlNDriveDoesApply;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        _MasterPage.Screen.Width = Unit.Percentage(80);
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        _upAR.Visible = false;
                
        switch (_SessionData.MemoData.ScrMemType)
        {
            case 1:
                SetPracCodesDDL();
                HookNDriveCheckBox();
                break;
            case 2:
                _upAR.Visible = true;
                AutoCompleteField searchBox = BuildClientSearchAutoComplete("sbClientNumber", "ClientNumber", _SessionData.MemoData.ClientNumber);
                searchBox.Clear += new AutoCompleteFieldCleared(ClientSearchClear);
                searchBox.SearchRequest += new AutoCompleteSearchRequest(ClientSearchSelected);
                _MasterPage.Screen.AddControlToFormAt(1, searchBox, "Client <span style='color: red; font-weight: bold;'>*</span>", "Start typing a client name or number to search");
                SetPracCodesDDL();
                HookNDriveCheckBox();
                break;
            case 3:
                //if pro bono memo type then disable the practice type code drop down list
                _ddlPracCode = (DropDownList)_MasterPage.Screen.FindControl("PracCode");
                _ddlPracCode.Enabled = false;
                _ddlPracCode.SelectedValue = "105X";
                _SessionData.MemoData.PracCode = "105X";
                HookNDriveCheckBox();
                break;
            case 4:
                break;
            case 5:
                SetPracCodesDDL();
                HookNDriveCheckBox();
                break;
            case 6:
                break;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            _MasterPage.SelectedTab.Items["Docketing"].Visible = ((Memo)_MasterPage.Screen.DataSource).DocketingInfoRequired();
        }
        catch (Exception ex)
        {
            //Per Dan's decree, no more empty catch blocks
            if (_DebugMode)
                _MasterPage.ShowError(ex);
        }
        _MasterPage.Screen.ShowInformationToolTips = true;
        _MasterPage.FeedbackMessage = string.Empty;
        if (!string.IsNullOrEmpty(_SessionData.MemoData.ClientNumber))
        {
            //_upAR.Visible = true;
            _upAR.ContentTemplateContainer.Controls.Clear();
            _upAR.ContentTemplateContainer.Controls.Add(BuildARTable(_SessionData.MemoData.ClientNumber));
        }
    }

    private void HookNDriveCheckBox()
    {
        _brcNDrive = (BooleanRadioControl)_MasterPage.Screen.FindControl("NDrive");
        _brcNDrive.AutoPostBack = true;
        _brcNDrive.SelectedIndexChanged += new EventHandler(brcNDrive_SelectedIndexChanged);
        AsyncPostBackTrigger cbTrigger = new AsyncPostBackTrigger();
        cbTrigger.EventName = "SelectedIndexChanged";
        cbTrigger.ControlID = _brcNDrive.ID;
        _MasterPage.Screen.Triggers.Add(cbTrigger);

        _ddlNDriveType = (DropDownList)_MasterPage.Screen.FindControl("NDriveType");
        _ddlNDriveType.AutoPostBack = true;
        _ddlNDriveType.SelectedIndexChanged += new EventHandler(ddlNDriveType_SelectedIndexChanged);
        _ddlNDriveType.Enabled = _brcNDrive.Value;

        //added by A Reimer 06/28/2013 for N Drive does apply change request
        _ddlNDriveDoesApply = (DropDownList)_MasterPage.Screen.FindControl("NDriveDoesApply");
        _ddlNDriveDoesApply.AutoPostBack = true;
        _ddlNDriveDoesApply.SelectedIndexChanged += new EventHandler(_ddlNDriveDoesApply_SelectedIndexChanged);
        _ddlNDriveType.Enabled = _brcNDrive.Value; //same as N Drive Type control

        //per PS request on 7/28/2011, removed "Patent Files" and "Patent Working Files", options will enable in the drop down list for backwards compatability 
        _ddlNDriveType.Items[1].Enabled = (_ddlNDriveType.SelectedValue == "1");
        _ddlNDriveType.Items[2].Enabled = (_ddlNDriveType.SelectedValue == "2");

        if (!_ddlNDriveType.Enabled)
            _ddlNDriveType.SelectedIndex = 0;
        AsyncPostBackTrigger ddlTrigger = new AsyncPostBackTrigger();
        ddlTrigger.EventName = "SelectedIndexChanged";
        ddlTrigger.ControlID = _ddlNDriveType.ID;
        _MasterPage.Screen.Triggers.Add(ddlTrigger);

        //added by A Reimer for N DRive Does Apply - hook an event to the n Drive does apply dropdownlist
        AsyncPostBackTrigger ddlNDriveDoesApplyTrigger = new AsyncPostBackTrigger()
        {
            EventName = "SelectedIndexChanged",
            ControlID = _ddlNDriveDoesApply.ID
        };
        _MasterPage.Screen.Triggers.Add(ddlNDriveDoesApplyTrigger);
        if (!_ddlNDriveDoesApply.Enabled) _ddlNDriveDoesApply.SelectedIndex = 0;





        _ucNDriveAccess = (_Staff)LoadControl("~/Screening/_Staff.ascx");
        _ucNDriveAccess.Mode = _Staff.StaffMode.NDriveAccess;
        _ucNDriveAccess.ID = "_staff";
        _ucNDriveAccess.Caption = "Staff Requiring N Drive Folder Access <span style='color: Red; font-size: large;'> * </span>";
        
        //added by A Reimer 07/03/2013 for N Drive Does Apply - hide the N Drive Access box if the none or select option is selected
        //on the N Drive Does Apply dropdown
        _ucNDriveAccess.Visible = isNDriveAccessVisible();


        ////per PS request on 7/28/2011, added "Patent Record" NDrive option at index 5 and enabled NDrive access list if option is selected 
        //_ucNDriveAccess.Visible = _ddlNDriveType.SelectedValue == "4" || _ddlNDriveType.SelectedValue == "5"; 
        
        
        _MasterPage.Screen.AddControlToForm(_ucNDriveAccess);
    }

    /// <summary>
    /// added by A Reimer 07/05/2013 for N Drive Does apply.  Is the event handler
    /// for the n drive does apply dropdown list.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void _ddlNDriveDoesApply_SelectedIndexChanged(object sender, EventArgs e)
    {
        _SessionData.MemoData.NDriveDoesApply = Int32.Parse(_ddlNDriveDoesApply.SelectedValue);
        _ucNDriveAccess.Visible = isNDriveAccessVisible();

    }


    private void SetPracCodesDDL()
    {
        _ddlPracCode = (DropDownList)_MasterPage.Screen.FindControl("PracCode");
        //per Adversary request add blank
        _ddlPracCode.Items.Insert(0, new ListItem("(select a practice code)", ""));

        try
        {
            //and strip out 101X, 180L, 181L and 182L 
            _ddlPracCode.Items.FindByValue("101X").Enabled = false;
            _ddlPracCode.Items.FindByValue("180L").Enabled = false;
            _ddlPracCode.Items.FindByValue("181L").Enabled = false;
            _ddlPracCode.Items.FindByValue("182L").Enabled = false;

            //strip out 102X per Vanessa Becker 
            if(_ddlPracCode.Items.FindByValue("102X") != null)
                _ddlPracCode.Items.FindByValue("102X").Enabled = false;

            if (!_ddlPracCode.SelectedItem.Enabled && !string.IsNullOrEmpty(_SessionData.MemoData.PracCode))
            {
                _MasterPage.FeedbackMessage = _SessionData.MemoData.PracCode + " is no longer a valid practice code";
                _MasterPage.FeedbackMode = MasterPageFeedbackMode.Error;
                _ddlPracCode.SelectedItem.Enabled = true;
                _ddlPracCode.Enabled = false;
                AddClearButton("* " + _SessionData.MemoData.PracCode + " is no longer valid (<span style='color: blue;'>click here to select a new practice type</span>)", 1);
            }
        }
        catch (Exception ex)
        {
            if (_DebugMode)
                _MasterPage.ShowError(ex);
        }

        if (!IsPostBack && string.IsNullOrEmpty(_SessionData.MemoData.PracCode))
            _ddlPracCode.SelectedIndex = 0;

        _ddlPracCode.AutoPostBack = true;
        _ddlPracCode.SelectedIndexChanged += new EventHandler(ddlPracCode_SelectedIndexChanged);

        AsyncPostBackTrigger trigger = new AsyncPostBackTrigger();
        trigger.ControlID = _ddlPracCode.ID;
        trigger.EventName = "SelectedIndexChanged";
        _MasterPage.Screen.Triggers.Add(trigger);
        _MasterPage.PostBackEvent += new MasterPagePostBackEventHandler(_MasterPage_PostBackEvent);
    }

    private void AddClearButton(string text, int type)
    {

        TableRow tr = _MasterPage.Screen.FindControlRow("PracCode");
        LinkButton lbClear = new LinkButton();
        lbClear.Text = text;
        lbClear.Click += new EventHandler(lbClear_Click);
        lbClear.ForeColor = Color.Red;
        lbClear.Font.Bold = true;
        lbClear.Font.Size = FontUnit.Larger;
        lbClear.Width = Unit.Percentage(70);
        lbClear.CommandArgument = type.ToString();
        tr.Cells[2].Width = Unit.Percentage(30);
        tr.Cells[2].Controls.Add(lbClear);
    }

    bool _MasterPage_PostBackEvent(object sender, MasterPagePostBackEventArgs e)
    {
        if (string.IsNullOrEmpty(_ddlPracCode.SelectedValue))
        {
            _MasterPage.FeedbackMessage = "Please select a Practice Code to Continue";
            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Error;
            Page.Validate();
            return (e.PostBackSource == MasterPagePostBackSource.PopupYes ||
                    e.PostBackSource == MasterPagePostBackSource.PopupNo);
        }
        return !_MasterPage.PopupDialogVisible;
    }

    void lbClear_Click(object sender, EventArgs e)
    {
        LinkButton lb = (LinkButton)sender;
        
        switch (lb.CommandArgument)
        {
            case "1":
                _ddlPracCode.Items.FindByValue(_SessionData.MemoData.PracCode).Enabled = false;
                _ddlPracCode.SelectedIndex = 0;
                _ddlPracCode.Enabled = true;
                _SessionData.MemoData.PracCode = null;
                DoDataSave(_SessionData.MemoData);
                
                break;
            case "2":
                _ddlPracCode.SelectedIndex = 0;
                _ddlPracCode.Enabled = true;
                
                _SessionData.MemoData.PracCode = null;
                _SessionData.MemoData.Tracking.PGMAcknowledged = null;
                _SessionData.MemoData.Tracking.PGMApproved = null;
                _SessionData.MemoData.Tracking.PGMDate = null;
                _SessionData.MemoData.Tracking.PGMUserID = null;
                _SessionData.MemoData.Tracking.PGMSignature = null;


                DoDataSave(_SessionData.MemoData.Tracking);
                DoDataSave(_SessionData.MemoData);
                
                break;
        }

        lb.Visible = false;
    }

    
    private AutoCompleteFieldMode ClientSearchClear(IAutoCompleteField sender)
    {
        _SessionData.MemoData.ClientNumber = null;
        _MasterPage.FeedbackMessage = string.Empty;
        _upAR.ContentTemplateContainer.Controls.Clear();
        _upAR.Update();
        return AutoCompleteFieldMode.ShowSearchBox;
    }

    private AutoCompleteFieldMode ClientSearchSelected(IAutoCompleteField sender, string text)
    {
        string number = ParseSearchRequestTextForNumber(text);
        if (string.IsNullOrEmpty(number))
        {
            _MasterPage.FeedbackMessage = "Client '" + text + "' was not found";
            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Warning;
            return AutoCompleteFieldMode.ShowSearchBox;
        }
        _SessionData.MemoData.ClientNumber = number;
        _upAR.ContentTemplateContainer.Controls.Clear();
        _upAR.ContentTemplateContainer.Controls.Add(BuildARTable(number));
        _upAR.Update();

        return AutoCompleteFieldMode.ShowResults;
    }


    void ddlPracCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddl = (DropDownList)sender;
        string pracCode = ddl.SelectedValue;
        _SessionData.MemoData.PracCode = pracCode;
        try
        {
            _MasterPage.SelectedTab.Items["Docketing"].Visible = _SessionData.MemoData.DocketingInfoRequired();
        }
        catch (Exception ex)
        {
            if (_DebugMode)
                _MasterPage.ShowError(ex);
        }
    }


    void brcNDrive_SelectedIndexChanged(object sender, EventArgs e)
    {
        bool ndrive = ((BooleanRadioControl)sender).Value;
        _SessionData.MemoData.NDrive = ndrive;
        _ddlNDriveType.Enabled = ndrive;

        //added by A Reimer 06/28/2013 for N Drive Does Apply change request
        _ddlNDriveDoesApply.Enabled = ndrive;

        if (!ndrive)
        {
            _ddlNDriveType.SelectedIndex = 0;
            //added by A Reimer 06/28/2013 for N Drive Does Apply change request
            _ddlNDriveDoesApply.SelectedIndex = 0;
        }


        //change by A Reimer 07/03/2013 for N Drive Does Apply - if the "None" option on the N Drive Does apply is selected
        //then there should be no "Staff" displayed.
        _ucNDriveAccess.Visible = isNDriveAccessVisible();


        ////per PS request on 7/28/2011, added "Patent Record" NDrive option at index 5 and enabled NDrive access list if option is selected 
        //_ucNDriveAccess.Visible = _ddlNDriveType.SelectedValue == "4" || _ddlNDriveType.SelectedValue == "5";
    }

    private bool isNDriveAccessVisible()
    {
        bool isVisible = false;
        isVisible = (_ddlNDriveType.SelectedValue == "4" || _ddlNDriveType.SelectedValue == "5") &&
            !(_ddlNDriveDoesApply.SelectedValue == "0") && !(_ddlNDriveDoesApply.SelectedValue == "-1");
        return isVisible;

        //return (_ddlNDriveType.SelectedValue == "4" || _ddlNDriveType.SelectedValue == "5")
        //    && (_ddlNDriveDoesApply.SelectedValue == "0" || _ddlNDriveDoesApply.SelectedValue == "-1");
    }

    void ddlNDriveType_SelectedIndexChanged(object sender, EventArgs e)
    {
        _SessionData.MemoData.NDriveType = Int32.Parse(_ddlNDriveType.SelectedValue);

        //changed by A Reimer for n drive does apply change 07/05/2013
        _ucNDriveAccess.Visible = isNDriveAccessVisible();

        //per PS request on 7/28/2011, added "Patent Record" NDrive option at index 5 and enabled NDrive access list if option is selected 
        //_ucNDriveAccess.Visible = _ddlNDriveType.SelectedValue == "4" || _ddlNDriveType.SelectedValue == "5"; 
    }
}
