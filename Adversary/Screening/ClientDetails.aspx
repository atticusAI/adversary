﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="ClientDetails.aspx.cs" Inherits="ClientDetails" MasterPageFile="~/Master.master" %>

<asp:Content ID="Content" ContentPlaceHolderID="UpperContentPlaceHolder" runat="server">

<asp:Panel ID="pnlClientType" runat="server"  Visible="false">
        
        <asp:UpdatePanel ID="upAR" runat="server" />
        <br />
        Please select if this is New or Existing Client
        <asp:RadioButtonList ID="rblClientType" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" >
            <asp:ListItem Text="New" Value="New"></asp:ListItem>
            <asp:ListItem Text="Existing" Value="Existing"></asp:ListItem>
        </asp:RadioButtonList>
        <br />
        <br />
        <asp:PlaceHolder ID="phExisting" runat="server"></asp:PlaceHolder>

</asp:Panel>

</asp:Content>