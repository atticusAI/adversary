﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Summary.aspx.cs" Inherits="Summary" MasterPageFile="~/Master.master" %>
<%@ Register Src="~/Screening/_ScreeningMemoReport.ascx" TagPrefix="UC" TagName="ScreeningMemoReport" %>
<%@ Register Src="~/Screening/_ApprovalTable.ascx" TagPrefix="UC" TagName="ApprovalTable" %>

<asp:Content ID="Content" ContentPlaceHolderID="UpperContentPlaceHolder" runat="server">

    <div style="width: 800px; "><UC:ApprovalTable ID="_apprTable" runat="server" Visible="false" /></div>
    <asp:PlaceHolder ID="_phSummary" runat="server"></asp:PlaceHolder>

</asp:Content>


