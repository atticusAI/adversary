﻿using System;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using HH;
using HH.Branding;
using HH.UI.FileList;
using HH.SQL;
using System.Data.SqlClient;


public partial class EngagementLetter : ScreeningBasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        _MasterPage.PostBackEvent += new MasterPagePostBackEventHandler(_MasterPage_PostBackEvent);
        //fcEngagement.FileListTemplate = new FileListTemplate();
        //fcEngagement.FileDirectory = GetUploadDirectoryURL(_SessionData.MemoData.ScrMemID);
        fcEngagement.FileListItemDataBound += new RepeaterItemEventHandler(base.FileList_ItemDataBound);
        fcEngagement.OnFileUploaded += new FileUploaded(fc_OnFileUploaded);
        fcEngagement.OnFileEditClick += new FileEditClick(fc_OnFileEditClick);
        fcEngagement.OnFileDeleteClick += new FileDeleteClick(fc_OnFileDeleteClick);
        fcEngagement.OnFileEditSubmit += new FileEditSubmit(fc_OnFileEditSubmit);
        fcEngagement.EnableDescription = false;
        fcEngagement.FileDescription = "Engagement Letter";

        //fcWrittenPolicies.FileListTemplate = new FileListTemplate();
        //fcWrittenPolicies.FileDirectory = GetUploadDirectoryURL(_SessionData.MemoData.ScrMemID);
        fcWrittenPolicies.FileListItemDataBound += new RepeaterItemEventHandler(base.FileList_ItemDataBound);
        fcWrittenPolicies.OnFileUploaded += new FileUploaded(fc_OnFileUploaded);
        fcWrittenPolicies.OnFileEditClick += new FileEditClick(fc_OnFileEditClick);
        fcWrittenPolicies.OnFileDeleteClick += new FileDeleteClick(fc_OnFileDeleteClick);
        fcWrittenPolicies.OnFileEditSubmit += new FileEditSubmit(fc_OnFileEditSubmit);
        fcWrittenPolicies.EnableDescription = false;
        fcWrittenPolicies.FileDescription = "Written Policy";

        rblEngagement.Items[0].Attributes.Add("onclick", "setAccordianPane('" + accEngagement.ClientID + "', 0);");
        rblEngagement.Items[1].Attributes.Add("onclick", "setAccordianPane('" + accEngagement.ClientID + "', 1);");

        rblWrittenPolicies.Items[0].Attributes.Add("onclick", "setAccordianPane('" + accWrittenPolicies.ClientID + "', 0);");
        rblWrittenPolicies.Items[1].Attributes.Add("onclick", "setAccordianPane('" + accWrittenPolicies.ClientID + "', 1);");

        BindFileControls();

        if (IsPostBack)
            return;
        
        if (!string.IsNullOrEmpty(_SessionData.MemoData.EngageDesc))
            tbEngagementDesc.Text = _SessionData.MemoData.EngageDesc;

        if (_SessionData.MemoData.EngageLetter != null)
        {
            int index = ((bool)_SessionData.MemoData.EngageLetter) ? 0 : 1;
            rblEngagement.Items[index].Selected = true;
            accEngagement.SelectedIndex = index;
            fcEngagement.FileListHeader = (_SessionData.MemoData.EngagementLetterFiles.Count == 0) ? "Please Upload an Engagement Letter" : "Attached Engagement Letter Files";
        }

        if (_SessionData.MemoData.WrittenPolicies != null)
        {
            int index = ((bool)_SessionData.MemoData.WrittenPolicies) ? 0 : 1;
            rblWrittenPolicies.Items[index].Selected = true;
            accWrittenPolicies.SelectedIndex = index;
            fcWrittenPolicies.FileListHeader = (_SessionData.MemoData.WrittenPolicyFiles.Count == 0) ? "Please Upload a Written Policy" : "Attached Written Policy Files";
        }
    }

    private bool _MasterPage_PostBackEvent(object sender, MasterPagePostBackEventArgs e)
    {
        try
        {
            _SessionData.MemoData.EngageDesc = tbEngagementDesc.Text.Trim();
            _SessionData.MemoData.EngageLetter = (rblEngagement.SelectedValue == "") ? null : (bool?)Boolean.Parse(rblEngagement.SelectedValue);
            _SessionData.MemoData.WrittenPolicies = (rblWrittenPolicies.SelectedValue == "") ? null : (bool?)Boolean.Parse(rblWrittenPolicies.SelectedValue);

            using (SqlConnection conn = AdversaryData.GetConnection())
            {
                DatabaseWorker worker = new DatabaseWorker(conn);
                foreach (Attachments attachment in _SessionData.MemoData.Attachments)
                    DoDataSave(attachment, worker);

                DoDataSave(_SessionData.MemoData, worker);
            }
            return true;
        }
        catch (Exception ex)
        {
            _MasterPage.ShowError(ex);
            return false;
        }
    }

    private void BindFileControls()
    {
        fcEngagement.DataSource = _SessionData.MemoData.EngagementLetterFiles;
        fcEngagement.DataBind();

        fcWrittenPolicies.DataSource = _SessionData.MemoData.WrittenPolicyFiles;
        fcWrittenPolicies.DataBind();
    }

    private bool fc_OnFileUploaded(IFileListControl sender, FileUpload file, string description)
    {
        if (!FileListControl_OnFileUploaded(sender, file, description))
            return false;

        BindFileControls();
        return true;
    }

    private bool fc_OnFileEditClick(IFileListControl sender, object dataItem)
    {
        return FileListControl_OnFileEditClick(sender, dataItem);
    }

    private bool fc_OnFileDeleteClick(IFileListControl sender, object dataItem)
    {
        if (!FileListControl_OnFileDeleteClick(sender, dataItem))
            return false;

        BindFileControls();
        return true;
    }

    private bool fc_OnFileEditSubmit(IFileListControl sender, object dataItem, string newFileName, string description)
    {
        if (!FileListControl_OnFileEditSubmit(sender, dataItem, newFileName, description))
            return false;

        //rebind the repeaters
        BindFileControls();
        return true;
    }

}
