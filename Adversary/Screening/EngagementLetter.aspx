﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="EngagementLetter.aspx.cs" Inherits="EngagementLetter" MasterPageFile="~/Master.master" %>
<%@ Register TagName="FileListControl" TagPrefix="UC" Src="~/_usercontrols/FileListControl.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="Content" ContentPlaceHolderID="UpperContentPlaceHolder" runat="server">

Will an Engagement Letter be Sent?
<asp:RadioButtonList ID="rblEngagement" runat="server" RepeatDirection="Horizontal" AutoPostBack="false">
    <asp:ListItem Text="Yes" Value="True"></asp:ListItem>
    <asp:ListItem Text="No" Value="False"></asp:ListItem>
</asp:RadioButtonList>
<ajax:Accordion ID="accEngagement" runat="server" 
                AutoSize="None"
                FadeTransitions="true"
                TransitionDuration="80"
                FramesPerSecond="80"
                SuppressHeaderPostbacks="true"
                RequireOpenedPane="false" 
                SelectedIndex="-1">
    <Panes>
        
        <ajax:AccordionPane runat="server" ID="engagementYes">
            <Content>
                <UC:FileListControl ID="fcEngagement" runat="server" />
            </Content>
        </ajax:AccordionPane>
        
        <ajax:AccordionPane runat="server" ID="engagementNo">
            <Content>
                <br />
                Reason an engagement letter will not be sent<br />
                <asp:TextBox ID="tbEngagementDesc" runat="server" TextMode="MultiLine" Rows="5" CssClass="sbControl"></asp:TextBox>
            </Content>
        </ajax:AccordionPane>
        
    </Panes>
</ajax:Accordion>
<br />
<p>
<strong>REMINDER:</strong> Whenever an engagement letter is written, the attorney should consider 
the wisdom of including a clause giving Holland & Hart the reserved right to be adverse in future, 
unrelated matters. Such clauses should be included only if the attorney can identify the types of 
future, unrelated matters in which Holland & Hart is likely to be adverse to the client.
</p><br />
<br />
Does this client or potential client require outside counsel to follow written policies or procedures relating to billing or other matters?<br />
<asp:RadioButtonList ID="rblWrittenPolicies" runat="server" RepeatDirection="Horizontal">
    <asp:ListItem Text="Yes" Value="True"></asp:ListItem>
    <asp:ListItem Text="No" Value="False"></asp:ListItem>
</asp:RadioButtonList>
<ajax:Accordion ID="accWrittenPolicies" runat="server" 
                AutoSize="None"
                FadeTransitions="true"
                TransitionDuration="80"
                FramesPerSecond="80"
                SuppressHeaderPostbacks="true"
                RequireOpenedPane="false" 
                SelectedIndex="-1">
    <Panes>
        
        <ajax:AccordionPane runat="server" ID="writtenPoliciesYes">
            <Content>
                
                <ul>
                    <li>Attach a copy of the written policy to this screening memo </li>
                    <li>Do not send an engangement letter until these policies have been reviewed and approved by the Financial Partner and the Firm Screener. </li>
                </ul>
                <UC:FileListControl ID="fcWrittenPolicies" runat="server" />
                
            </Content>
        </ajax:AccordionPane>
        
    </Panes>
</ajax:Accordion>



</asp:Content>