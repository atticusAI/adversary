﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Drawing;

public enum ScreeningMemoListMode
{
    UserList,
    Search,
    ApprovalSearch,
    AdminSearch
}


public partial class Screening_ScreeningMemoList : System.Web.UI.UserControl
{
    protected enum ListDataField
    {
        ClientNumber,
        AttEntering,
        PersonnelID,
        MatterName,
        ClientMatterNumber,
        ScrMemID
    }

    
    private readonly string CONTEXT_VS_KEY = "_contextKey";
    private readonly string RESULTS_KEY = "_results";

    private bool _hasDataBoundHandler = false;

    public DataTable DataSource { get; set; }
    public ScrollBars ScrollBars { get; set; }
    public ScreeningMemoListMode Mode { get; set; }

    public Screening_ScreeningMemoList()
    {
        ScrollBars = ScrollBars.Auto;
        Mode = ScreeningMemoListMode.Search;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (ViewState[RESULTS_KEY] != null)
        {
            DataSource = (DataTable)ViewState[RESULTS_KEY];
            DataBind();
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        _pnlResultsContainer.ScrollBars = ScrollBars;
    }

    #region GRID VIEW
    public override void DataBind()
    {
        base.DataBind();

        if (DataSource == null) throw new Exception("Could not perform data bind, no screening memo list found as a data source.");
        int rowCount = DataSource.Rows.Count;
        if (rowCount == 1)
        {
            Redirect(DataSource.Rows[0]);
            return;
        }

        if (!_hasDataBoundHandler) _gvResults.RowDataBound += new GridViewRowEventHandler(gvResults_RowDataBound);
        _hasDataBoundHandler = true;

        if (!DataSource.Columns.Contains("ClientMatterNumber"))
            _gvResults.Columns[6].Visible = false;

        _gvResults.DataSource = DataSource;
        _gvResults.DataBind();
        if (rowCount == 0)
        {
            if (Mode == ScreeningMemoListMode.UserList)
                _lblCount.Text = "No memos found for user with employee ID " + ((BasePage)Page)._SessionData.LoginEmpCode;
            else
                _lblCount.Text = "No memos found for this search";
            _lblCount.ForeColor = Color.Blue;
        }
        else
        {
            _lblCount.Text = rowCount + " memos were found";
            _lblCount.ForeColor = Color.FromName("68849F");
        }
    }

    void gvResults_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        TableCell td = new TableCell();
        e.Row.Cells.AddAt(0, td);

        if (e.Row.DataItem == null)
            return;

        DataRow dr = ((DataRowView)e.Row.DataItem).Row;
        HtmlAnchor link = (HtmlAnchor)e.Row.FindControl("_aView");
        link.HRef = BuildUrl(dr);
    }

    protected string GetBoundContents(IDataItemContainer container, ListDataField dataField)
    {

        try
        {
            if (!_gvResults.Columns[6].Visible && dataField == ListDataField.ClientMatterNumber)
                return string.Empty;

            string df = dataField.ToString();
            DataRowView drv = (DataRowView)container.DataItem;
            if (!drv.Row.Table.Columns.Contains(df))
                return string.Empty;

            object value = drv.Row[df];
            if (value == null)
                return string.Empty;

            string text = value.ToString();

            switch (dataField)
            {
                case ListDataField.AttEntering:
                    text = BasePage.GetEmployeeDisplayName(value);
                    break;
                case ListDataField.PersonnelID:
                    text = BasePage.GetEmployeeDisplayName(value);
                    break;
                case ListDataField.ClientNumber:
                    text = BasePage.GetClientDisplayName(value);
                    break;
            }

            string contextKey = (string)ViewState[CONTEXT_VS_KEY];
            if (contextKey == df)
                text = "<span style='color: blue;'>" + text + "</span>";

            return text;
        }
        catch
        {
            return string.Empty;
        }
    }

    #endregion

    private void Redirect(DataRow dr)
    {
        if (ViewState[RESULTS_KEY] != null)
        {
            _gvResults.DataSource = (DataTable)ViewState[RESULTS_KEY];
            _gvResults.DataBind();
        }
        Response.Redirect(BuildUrl(dr), true);
    }

    private string BuildUrl(DataRow dr)
    {
        string baseUrl;
        switch (Mode)
        {
            case ScreeningMemoListMode.AdminSearch:
                baseUrl = "../Admin/AdminProcessing.aspx";
                break;
            case ScreeningMemoListMode.ApprovalSearch:
                baseUrl = "../Admin/Approval.aspx";
                break;
            default:
                baseUrl = "Summary.aspx";
                break;
        }

        return baseUrl + "?ScrMemID=" + dr["ScrMemID"] + "&ScrMemType=" + dr["ScrMemType"];
    }

}
