﻿using System;
using HH.Branding;

using HH.SQL;
using HH.UI;
using System.Data.SqlClient;

public partial class Retainer : ScreeningBasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        phTooltip.Controls.Add(new InformationTooltip("If no retainer is requested or if retainer is less than $5000 PGL or AP signature is required."));
        _MasterPage.PostBackEvent += new MasterPagePostBackEventHandler(_MasterPage_PostBackEvent);

        if (IsPostBack)
            return;

        if (_SessionData.MemoData.Retainer.HasValue)
        {
            bool hasRetainer = _SessionData.MemoData.Retainer.Value;
            rblHasRetainer.SelectedValue = hasRetainer.ToString();
            SetHasRetainerVisibility();

            bool hasSmallRetainer = !string.IsNullOrEmpty(_SessionData.MemoData.RetainerDescYes);
            rblSmallRetainer.SelectedValue = hasSmallRetainer.ToString();
            SetSmallRetainerVisibility();
        }

        if (!string.IsNullOrEmpty(_SessionData.MemoData.RetainerDesc) &&
            _SessionData.MemoData.RetainerDesc != _SessionData.MemoData.RetainerDescYes &&
            _SessionData.MemoData.RetainerDescNo != _SessionData.MemoData.RetainerDescNo)
        {
            pnlRetainerDesc.Visible = true;
            lblRetainerDesc.Text = _SessionData.MemoData.RetainerDesc;
        }

        if (!string.IsNullOrEmpty(_SessionData.MemoData.RetainerDescYes))
            tbSmallRetainer.Text = _SessionData.MemoData.RetainerDescYes;

        if (!string.IsNullOrEmpty(_SessionData.MemoData.RetainerDescNo))
            tbNoRetainer.Text = _SessionData.MemoData.RetainerDescNo;

        //if (!IsPostBack)
        //{
        //    if (_SessionData.MemoData.Retainer.HasValue)
        //        //rblRetainer.Items.FindByValue(_SessionData.MemoData.Retainer.ToString().ToLower()).Selected = true;
        //        rblRetainer.SelectedIndex = (_SessionData.MemoData.Retainer.Value) ? 0 : 1;
        //    else
        //        return;

        //    //pnlReason.Visible = (rblRetainer.SelectedValue == "True");
        //    //tbReason.Text = _SessionData.MemoData.RetainerDescYes;
        //}
    }

    private bool _MasterPage_PostBackEvent(object sender, MasterPagePostBackEventArgs e)
    {
        if (string.IsNullOrEmpty(rblHasRetainer.SelectedValue))
        {
            _SessionData.MemoData.Retainer = null;
            _SessionData.MemoData.RetainerDesc = null;
            _SessionData.MemoData.RetainerDescNo = null;
            _SessionData.MemoData.RetainerDescYes = null;
            DoDataSave(_SessionData.MemoData);
            return true;
        }

        bool hasRetainer = rblHasRetainer.SelectedValue == "True";
        if (hasRetainer)
        {
            _SessionData.MemoData.Retainer = true;
            _SessionData.MemoData.RetainerDescYes = null;
            _SessionData.MemoData.RetainerDescNo = null;
        }
        else
        {
            string text = tbNoRetainer.Text.Trim();
            if (string.IsNullOrEmpty(text))
            {
                _MasterPage.FeedbackMode = MasterPageFeedbackMode.Warning;
                _MasterPage.FeedbackMessage = "Enter a reason why there will not be a retainer";
                return false;
            }
            _SessionData.MemoData.Retainer = false;
            _SessionData.MemoData.RetainerDescNo = text;
            _SessionData.MemoData.RetainerDescYes = null;
        }

        bool smallRetainer = hasRetainer && rblSmallRetainer.SelectedValue == "True";
        if (smallRetainer)
        {
            string text = tbSmallRetainer.Text.Trim();
            if (string.IsNullOrEmpty(text))
            {
                _MasterPage.FeedbackMode = MasterPageFeedbackMode.Warning;
                _MasterPage.FeedbackMessage = "Enter a reason why the retainer will be less than $5000";
                return false;
            }
            _SessionData.MemoData.RetainerDescNo = null;
            _SessionData.MemoData.RetainerDescYes = text;
        }
        else
        {
            _SessionData.MemoData.RetainerDescYes = null;
        }

        DoDataSave(_SessionData.MemoData);
        return true;

        //if (smallRetainer.HasValue && smallRetainer.Value)
        //{
        //    string text = tbSmallRetainer.Text.Trim();
        //    if (string.IsNullOrEmpty(text))
        //    {
        //        _MasterPage.FeedbackMode = MasterPageFeedbackMode.Warning;
        //        _MasterPage.FeedbackMessage = "Enter a reason why the retainer will be less than $5000";
        //        return false;
        //    }
        //    _SessionData.MemoData.RetainerDescNo = string.Empty;
        //    _SessionData.MemoData.RetainerDescYes = text;
        //}
        //else if (!hasRetainer)
        //{
        //    string text = tbNoRetainer.Text.Trim();
        //    if (string.IsNullOrEmpty(text))
        //    {
        //        _MasterPage.FeedbackMode = MasterPageFeedbackMode.Warning;
        //        _MasterPage.FeedbackMessage = "Enter a reason why there will not be a retainer";
        //        return false;
        //    }
        //    _SessionData.MemoData.RetainerDescNo = text;
        //    _SessionData.MemoData.RetainerDescYes = string.Empty;
        //}


    }

    

    private void SetHasRetainerVisibility()
    {
        bool hasRetainer = (rblHasRetainer.SelectedValue != "False");
        pnlNoRetainer.Visible = !hasRetainer;

        trSmallRetainerQuestion.Visible = hasRetainer;
        trSmallRetainer.Visible = hasRetainer;
        pnlSmallRetainer.Visible = hasRetainer && (rblSmallRetainer.SelectedValue == "True");
    }

    private void SetSmallRetainerVisibility()
    {
        pnlSmallRetainer.Visible = (rblSmallRetainer.SelectedValue == "True");
    }

    protected void rblHasRetainer_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetHasRetainerVisibility();
    }

    protected void rblSmallRetainer_SelectedIndexChanged(object sender, EventArgs e)
    {
        pnlNoRetainer.Visible = false;
        SetSmallRetainerVisibility();
    }

}
