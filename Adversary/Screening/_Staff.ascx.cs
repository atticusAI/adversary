﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using System.Linq;
using System.Xml.Linq;
using HH;
using HH.Branding;
using HH.Screen;
using HH.SQL;
using HH.UI;

public partial class _Staff : System.Web.UI.UserControl
{
    public event EntitySubmitEventHandler OnEntitySubmitted;

    public enum StaffMode
    {
        Staff,
        Fees,
        NDriveAccess,
        DocketingTeam
    }

    private ScreeningBasePage _scrPage = null;
    private ScreeningBasePage ScreeningPage
    {
        get
        {
            if (_scrPage == null)
                _scrPage = (ScreeningBasePage)Page;
            return _scrPage;
        }
    }

    public StaffMode Mode { get; set; }
    public string Caption { get; set; }
    public string InformationToolTip { get; set; }
    
   
    public _Staff()
    {
        Caption = string.Empty;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        list.Caption = Caption;
        list.InformationToolTip = this.InformationToolTip; 
        switch (Mode)
        {
            case StaffMode.Fees:
                list.AddButtonText = "Add an Associate";
                break;
            case StaffMode.DocketingTeam:
                list.AddButtonText = "Add Team Members";
                break;
            default:
                list.AddButtonText = "Add Staff";
                break;
        }
        list.AllowModify = false;
        list.AutoGenerateColumns = false;
        list.FieldsAdding += new GridFieldsAdding(list_FieldsAdding);
        list.RowDataBound += new GridViewRowEventHandler(Staff_RowDataBound);
        list.ScreenRowCreated += new ScreenRowCreatedEventHandler(Staff_ScreenRowCreated);
        list.OnEntitySubmitted += new EntitySubmitEventHandler(Staff_OnEntitySubmitted);
        list.ScreenID = ScreeningPage.ParseSearchRequestTextForNumber(Request.Path);
        list.EntityID = Mode.ToString();
        list.DataSource = GetDataSource();
        list.DataBind();
    }

    void list_FieldsAdding(GridView sender, object dataSource)
    {
        BoundField bf = new BoundField();
        bf.DataField = "PersonnelID";
        bf.SortExpression = "PersonnelID";
        bf.HeaderText = "Staff";
        sender.Columns.Add(bf);
    }

    void Staff_OnEntitySubmitted(IListContainerControl sender, object dataSource, object entity, EntitySubmitType type)
    {
        IStaff staff = (IStaff)entity;
        IList entitySet = (IList)dataSource;
        staff.ScrMemID = ScreeningPage._SessionData.MemoData.ScrMemID;

        switch (type)
        {
            case EntitySubmitType.Create:

                if (!ResolvePersonnelID(staff, true))
                    break;

                //ScreeningPage.SaveData(staff, AdversaryData.GetInstance());
                ScreeningPage.DoDataSave(staff);

                int index = entitySet.IndexOf(staff);
                if (index == -1)
                    entitySet.Add(staff);
                else
                {
                    entitySet.RemoveAt(index);
                    entitySet.Insert(index, staff);
                }
                break;

            case EntitySubmitType.Delete:
                DoDelete(staff);
                entitySet.Remove(staff);
                break;
        }

        list.DataSource = entitySet;
        list.DataBind();

        if (OnEntitySubmitted != null)
            OnEntitySubmitted(sender, dataSource, entity, type);
    }

    void Staff_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.DataRow)
            return;

        IStaff staff = (IStaff)e.Row.DataItem;
        CMSData cms = CMSData.GetInstance();
        if (!ResolvePersonnelID(staff, false))
        {
            e.Row.Cells[1].Text = staff.PersonnelID + " <span style='color:red; font-weight:bold;'>* Not a valid H&H employee</span>";
            return;
        }

        try
        {
            string name = cms.GetUserNameByCode(staff.PersonnelID);
            e.Row.Cells[1].Text = name + " (" + staff.PersonnelID + ")";
        }
        catch
        {
            e.Row.Cells[1].Text = staff.PersonnelID + " <span style='color:red; font-weight:bold;'>* An error occurred while looking up this user in CMS</span>";
        }

        //
        //Regex regex = new Regex("^[0-9]");
        //if (regex.IsMatch(staff.PersonnelID))
        //{
        //    try
        //    {
        //        string name = cms.GetUserNameByCode(staff.PersonnelID);
        //        e.Row.Cells[1].Text = name + " (" + staff.PersonnelID + ")";
        //    }
        //    catch
        //    {
        //        e.Row.Cells[1].Text = staff.PersonnelID + " <span style='color:red; font-weight:bold;'>* Not a valid H&H employee</span>";
        //    }
        //}
        //else
        //{
        //    DataTable dt = cms.DoEmployeeSearch(staff.PersonnelID);
        //    if (dt.Rows.Count == 1)
        //    {
        //        DataRow dr = dt.Rows[0];
        //        staff.PersonnelID = dr[CMSData.EMP_CODE].ToString();
        //        e.Row.Cells[1].Text = dr[CMSData.EMP_NAME] + " (" + dr[CMSData.EMP_CODE] + ")";
        //    }
        //    else
        //    {
        //        e.Row.Cells[1].Text = staff.PersonnelID + " <span style='color:red; font-weight:bold;'>* Not a valid H&H employee</span>";
        //    }
        //}
    }

    void Staff_ScreenRowCreated(PropertyInfo info, object dataSource, object propertyValue, TableRow tr)
    {
        ((TextBox)tr.Cells[1].Controls[0]).Attributes.Add("autocomplete", "off");
        tr.Cells[1].Controls.Add(UIUtils.BuildAutoComplete(info.Name, "GetEmployeeList", info.Name));
    }

    private object GetDataSource()
    {
        switch (Mode)
        {
            case StaffMode.Fees:
                return ScreeningPage._SessionData.MemoData.FeeSplitStaffs;
            case StaffMode.NDriveAccess:
                return ScreeningPage._SessionData.MemoData.NDriveAccess;
            case StaffMode.DocketingTeam:
                return ScreeningPage._SessionData.MemoData.DocketingTeam;
            default: //case StaffMode.Staff:
                return ScreeningPage._SessionData.MemoData.Staffing;
        }
    }

    private void DoDelete(IStaff staff)
    {
        using (SqlConnection conn = AdversaryData.GetConnection())
        {
            DatabaseWorker worker = new DatabaseWorker(conn);
            SqlCommand cmd = worker.BuildStoredProcedure(staff, SPType.Delete);
            //cmd.Parameters.AddWithValue("PersonnelID", staff.PersonnelID);
            cmd.Connection = conn;
            cmd.ExecuteNonQuery();
        }
    }

    private bool ResolvePersonnelID(IStaff staff, bool isSubmit)
    {
        if (AjaxBasePage.ValidEmployeeSelection(staff.PersonnelID))
            staff.PersonnelID = ScreeningPage.ParseSearchRequestTextForNumber(staff.PersonnelID);
        CMSData cms = CMSData.GetInstance();
        DataTable results = cms.DoEmployeeSearch(staff.PersonnelID);
        var query = from r in results.AsEnumerable()
                    where !r.Field<string>(CMSData.EMP_NAME).ToUpper().Contains("AMEX")
                    select r;
        if (query.Count() == 0)
        {
            ScreeningPage._MasterPage.FeedbackMode = MasterPageFeedbackMode.Warning;
            ScreeningPage._MasterPage.FeedbackMessage = staff.PersonnelID + " could not be found as a valid employee name or number";
            return false;
        }
        DataTable dt = query.CopyToDataTable();
        if (dt.Rows.Count > 1)
        {
            ScreeningPage._MasterPage.FeedbackMode = MasterPageFeedbackMode.Warning;
            ScreeningPage._MasterPage.FeedbackMessage = "More than one match found for entry " + staff.PersonnelID;
            return false;
        }
        
        staff.PersonnelID = dt.Rows[0][CMSData.EMP_CODE].ToString();
        return true;


        //string personnelId = staff.PersonnelID;
        //Regex regex = new Regex("^[0-9]");
        //if (!regex.IsMatch(personnelId))
        //{
        //    personnelId = ScreeningPage.ParseSearchRequestTextForNumber(personnelId);
        //    if (!regex.IsMatch(personnelId))
        //    {
        //        CMSData cms = CMSData.GetInstance();
        //        DataTable dt = cms.DoEmployeeSearch(personnelId);
        //        if (dt.Rows.Count == 1)
        //        {
        //            if (!isSubmit)
        //                DoDelete(staff);
        //            staff.PersonnelID = dt.Rows[0][CMSData.EMP_CODE].ToString();
        //            if (!isSubmit)
        //                ScreeningPage.SaveData(staff);
        //            return true;
        //        }
        //        else if (isSubmit)
        //        {
        //            ScreeningPage._MasterPage.FeedbackMode = MasterPageFeedbackMode.Warning;
        //            ScreeningPage._MasterPage.FeedbackMessage = personnelId + " could not be found as a valid employee name or number";
        //        }
        //        return false;
        //    }
        //    staff.PersonnelID = personnelId;
        //    return true;
        //}
        //staff.PersonnelID = personnelId;
        //return true;
    }
}
