﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="RelatedParties.aspx.cs" Inherits="RelatedParties" MasterPageFile="~/Master.master" %>
<%@ Register TagPrefix="UC" TagName="Parties" Src="~/Screening/_Parties.ascx" %>

<asp:Content ID="Content" ContentPlaceHolderID="UpperContentPlaceHolder" runat="server">
<br />
<!--<strong>Please be careful to enter ALL of the correct Related Organization(s).</strong><br />-->
<strong>Please be careful to enter the correct Individual(s)/Organization(s) and their Relationship(s). Referring Organization may also be entered.</strong>
<br /><br /><strong>Please use code 102 “Co-Client Affiliate” if your client is an organization and you also represent one of its related organizations, such as a subsidiary, or sister company.  Use code 101 “Co-Client (Non Affiliate)” for all other multiple representations.</strong>
<UC:Parties ID="_parties" runat="server" />


</asp:Content>