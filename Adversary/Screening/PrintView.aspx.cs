﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;

using HH.Screen;
using HH.Branding;
using HH.UI.FileList;

public partial class PrintView : ScreeningBasePage
{
    protected void Page_PreInit(object sender, EventArgs e)
    {
        _MasterPage.MenuMode = MasterPageMenuMode.None;
        _MasterPage.SideBarVisible = false;
        _MasterPage.TabMenuVisible = false;
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        _MasterPage.BreadCrumbText = string.Empty;

        if (!_SessionData.MemoLoaded)
        {
            _MasterPage.FeedbackMessage = "Print view is unavailable, no memo selected";
            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Warning;
            return;
        }

        _lblHeader.Text = "Screening Memo - " + _SessionData.MemoData.ScrMemID + " - " + _SessionData.MemoData.GetMemoTypeDescription();


        string scrMemType = _SessionData.MemoData.ScrMemType.ToString();
        SiteMapNode node = _MasterPage.SiteMapProvider.FindSiteMapNode("~/Screening/Summary.aspx?ScrMemType=" + scrMemType);
        node = node.NextSibling;
        while (node != null)
        {
            if (node.Title == "Final Review" ||
                node.Title == "Attachments" ||
                node.Title == "Docketing" ||
                (node.Title == "Client Address" && scrMemType != "6") ||
                (node.Title == "Contact Information" && scrMemType != "6") ||
                node.Title == "Referring Party")
            {
                node = node.NextSibling;
                continue;
            }

            if (node.Title == "Legal Work Information")
            {
                HandleLegalWork(node);
                node = node.NextSibling;
                continue;
            }

            //per adversary group's request, move the client address so that it follows client or matter info
            if ((node.Title == "Client Information" ||
                (node.Title == "Matter Information" && scrMemType != "1" && scrMemType != "5")))
                HandleMatterInfo(scrMemType, node);
            else
                AddReport(node);

            node = node.NextSibling;
        }

        if (scrMemType != "6" && _SessionData.MemoData.DocketingInfoRequired())
        {
            try
            {
                SiteMapNode docketing = _MasterPage.SiteMapProvider.FindSiteMapNode("~/Screening/Docketing.aspx?ScrMemType=" + scrMemType);
                AddReport(docketing);
            }
            catch { }
        }

        if (_SessionData.MemoData.Files.Count > 0)
        {
            //Table table = new Table();
            //table.Width = Unit.Pixel(600);
            //ScreenControlBuilder.PopulateReport(_SessionData.MemoData.Files, ref table, string.Empty, false, true);
            IReportBuilder uc = (IReportBuilder)LoadControl("~/_usercontrols/ReportBuilder.ascx");
            uc.CssClass = string.Empty;
            uc.RowCssClass = string.Empty;
            uc.RowValueCssClass = string.Empty;
            uc.ShowNullValues = false;
            uc.DataSource = _SessionData.MemoData.Files;
            uc.DataBind();

            _pnlSummary.Controls.Add(new LiteralControl("<strong><em>File Information</em></strong><br/>"));
            _pnlSummary.Controls.Add((UserControl)uc);
            _pnlSummary.Controls.Add(new LiteralControl("<hr/>"));
        }

        if (_SessionData.MemoData.Attachments.Count > 0)
        {
            Table table = new Table();
            table.Width = Unit.Pixel(600);
            _pnlSummary.Controls.Add(new LiteralControl("<strong><em>Attached Files</em></strong><br/><br/>"));
            foreach (Attachments attachment in _SessionData.MemoData.Attachments)
            {
                //_pnlSummary.Controls.Add(FileListTemplate.BuildFileListItem(Request.ApplicationPath, attachment));
                attachment.FileURI = GetAttachmentDownloadURL(attachment);
                _pnlSummary.Controls.Add(new FileListItemControl(attachment));
                _pnlSummary.Controls.Add(new LiteralControl("<br/>"));
            }
            _pnlSummary.Controls.Add(new LiteralControl("<hr style=\"width: 98%\"/>"));
        }
    }

    private void HandleMatterInfo(string scrMemType, SiteMapNode node)
    {
        Table table = new Table();
        table.Width = Unit.Pixel(800);

        //set the column width
        TableRow tr = new TableRow();
        TableCell td = new TableCell();
        td.Width = Unit.Pixel(250);
        tr.Cells.Add(td);
        td = new TableCell();
        td.Width = Unit.Pixel(550);
        tr.Cells.Add(td);
        table.Rows.Add(tr);

        SiteMapNode clientAddr = _MasterPage.SiteMapProvider.FindSiteMapNode("~/Screening/ClientAddress.aspx?ScrMemType=" + scrMemType);
        SiteMapNode contactInfo = _MasterPage.SiteMapProvider.FindSiteMapNode("~/Screening/Contact.aspx?ScrMemType=" + scrMemType);

        _ScreeningMemoReport ucMatterInfo = BuildReport(node);
        _ScreeningMemoReport ucContactInfo = BuildReport(contactInfo);

        IReportBuilder rb = (IReportBuilder)LoadControl("~/_usercontrols/ReportBuilder.ascx");
        rb.ShowNullValues = false;
        rb.Width = Unit.Pixel(800);
        rb.CssClass = string.Empty;
        rb.RowCssClass = string.Empty;

        if (clientAddr != null)
        {
            string screenId = clientAddr.Url;
            screenId = screenId.Substring(screenId.LastIndexOf('/'));
            screenId = screenId.Replace("/", "");
            rb.DataSource = _SessionData.MemoData;
            rb.ScreenID = screenId;
            rb.DataBind();
        }

        rb.AddRowAt(0, "Client", GetClientDisplayName(_SessionData.MemoData));
        rb.AddRowAt(1, "<br/>", "<br/>");

        //add client + addresss
        tr = new TableRow();
        td = new TableCell();
        td.Controls.Add((UserControl)rb);
        tr.Cells.Add(td);
        table.Rows.Add(tr);

        //spacer
        tr = new TableRow();
        td = new TableCell();
        td.Text = "<br/>";
        tr.Cells.Add(td);
        table.Rows.Add(tr);

        //contact info
        tr = new TableRow();
        td = new TableCell();
        td.Controls.Add(ucContactInfo);
        tr.Cells.Add(td);
        table.Rows.Add(tr);

        AddReport("Client Info", table);
        table = new Table();
        table.Width = Unit.Pixel(800);

        //set the column width
        tr = new TableRow();
        td = new TableCell();
        td.Width = Unit.Pixel(250);
        tr.Cells.Add(td);
        td = new TableCell();
        td.Width = Unit.Pixel(550);
        tr.Cells.Add(td);
        table.Rows.Add(tr);

        //the rest of the matter info
        tr = new TableRow();
        td = new TableCell();
        td.Controls.Add(ucMatterInfo);
        tr.Cells.Add(td);
        table.Rows.Add(tr);

        AddReport("Matter Info", table);
    }

    private void HandleLegalWork(SiteMapNode node)
    {
        Table table = new Table();
        table.Width = Unit.Pixel(800);

        //set the column width
        TableRow tr = new TableRow();
        TableCell td = new TableCell();
        td.Width = Unit.Pixel(250);
        tr.Cells.Add(td);
        td = new TableCell();
        td.Width = Unit.Pixel(550);
        tr.Cells.Add(td);
        table.Rows.Add(tr);

        IReportBuilder rb = (IReportBuilder)LoadControl("~/_usercontrols/ReportBuilder.ascx");
        rb.ShowNullValues = false;
        rb.Width = Unit.Pixel(800);
        rb.CssClass = string.Empty;
        rb.RowCssClass = string.Empty;
        string workFor = "<em>(unspecified)</em>";
        string clientName = "<em>(unspecified)</em>";
        if (_SessionData.MemoData.Company.HasValue)
        {
            bool employee = !_SessionData.MemoData.Company.Value;
            workFor = (employee) ? "Employee" : "Firm";
            clientName = GetClientDisplayName(_SessionData.MemoData);
            if (employee && _SessionData.MemoData.HHLegalWorkID.HasValue)
                clientName = clientName + " (" + _SessionData.MemoData.HHLegalWorkID + ")";

        }
        rb.AddRow("Legal Work For: ", workFor);
        rb.AddRow("Client: ", clientName);

        tr = new TableRow();
        td = new TableCell();
        td.Controls.Add((UserControl)rb);
        tr.Cells.Add(td);
        table.Rows.Add(tr);

        AddReport(node, table);
    }

    private void AddReport(SiteMapNode node) { AddReport(node, BuildTable(node)); }
    private void AddReport(SiteMapNode node, Table table) { AddReport(node.Title, table); }
    private void AddReport(string title, Table table)
    {
        title = "<strong><em>" + title + "</em></strong>";
        _pnlSummary.Controls.Add(new LiteralControl(title + "<br/>"));
        _pnlSummary.Controls.Add(table);
        _pnlSummary.Controls.Add(new LiteralControl("<hr style=\"width: 98%\"/>"));
    }

    private Table BuildTable(SiteMapNode node)
    {
        Table table = new Table();
        table.Width = Unit.Pixel(800);

        //set the column width
        TableRow tr = new TableRow();
        TableCell td = new TableCell();
        td.Width = Unit.Pixel(250);
        tr.Cells.Add(td);
        td = new TableCell();
        td.Width = Unit.Pixel(550);
        tr.Cells.Add(td);
        table.Rows.Add(tr);

        _ScreeningMemoReport uc = BuildReport(node);
        td.Controls.Add(uc);
        tr.Cells.Add(td);
        table.Rows.Add(tr);
        return table;
    }
    private _ScreeningMemoReport BuildReport(SiteMapNode node)
    {
        _ScreeningMemoReport uc = (_ScreeningMemoReport)LoadControl("~/Screening/_ScreeningMemoReport.ascx");
        uc.ReportMode = _ScreeningMemoReport.Mode.Print;
        uc.DataSource = _SessionData.MemoData;
        uc.Node = node;
        uc.DataBind();
        return uc;
    }

}
