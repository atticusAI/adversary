﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="_Parties.ascx.cs" Inherits="_Parties" %>
<%@ Register TagPrefix="UC" TagName="ListContainerControl" Src="~/_usercontrols/ListContainerControl.ascx" %>
<%@ Register TagPrefix="UC" Assembly="HH" Namespace="HH.UI.ModalPopupForm" %>

<br />
<br />
<UC:ListContainerControl ID="list" runat="server" AllowAdd="false" />
<asp:LinkButton ID="lbShowForm" runat="server" OnClick="lbShowForm_Click" Text="Add Parties" Font-Bold="true"></asp:LinkButton>

<UC:ModalForm ID="modalForm" runat="server" TargetControlID="lbShowForm" CancelControlID="lbCancel" Width="75%" Y="25" RepositionMode="None">


<asp:UpdatePanel ID="upForm" runat="server" UpdateMode="Conditional">
    
    <ContentTemplate>
    
    <asp:Panel ID="pnlWrapper" runat="server" ScrollBars="Auto" Height="350" Width="100%">
        
        <asp:Label ID="lblFeedback" runat="server" Font-Bold="true" ForeColor="Red"></asp:Label>
            
        <asp:Panel ID="pnlNames" runat="server" style="margin: 0 0 0 6px;">
        
            <strong>Please type one or more party names here</strong>
            <ul style="font-style: italic; font-size: small; font-weight: lighter;">
               <li>Use [Enter] after each party to list one party per line</li>
               <li>Enter a maximum of three names (i.e. First Middle & Last name) per individual</li>
               <li>You will be able to select party type and relationship after submitting your party name(s)</li>
            </ul>
            <asp:TextBox ID="taParties" runat="server" TextMode="MultiLine" CssClass="sbControl" Rows="6" Width="70%" style="margin: 0 0 0 25px;"></asp:TextBox>
        
        </asp:Panel>
         
        <asp:Panel ID="pnlRelationships" runat="server">
        
            <strong>Please choose relationships for the list of parties</strong><br />
            <br />
            <asp:GridView ID="gvRelationships" runat="server" 
                        AutoGenerateColumns="false" 
                        GridLines="None"
                        RowStyle-Height="30px"
                        Width="90%"
                        OnRowDataBound="gvRelationships_RowDataBound" >
                <Columns>
                    <asp:TemplateField HeaderText="Name" ItemStyle-CssClass="gridViewItem" ItemStyle-Width="30%" >
                        <ItemTemplate>
                            <asp:TextBox ID="tbName" runat="server" CssClass="sbControl" Width="100%"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Party Type" ItemStyle-CssClass="gridViewItem" ItemStyle-Wrap="false" ItemStyle-Width="15%" >
                        <ItemTemplate>
                            <asp:DropDownList ID="ddlType" runat="server" CssClass="sbControl" Width="100%"></asp:DropDownList>
                        </ItemTemplate>
                    </asp:TemplateField> 
                    <asp:TemplateField HeaderText="Relationship" ItemStyle-CssClass="gridViewItem" ItemStyle-Wrap="false" ItemStyle-Width="35%"> 
                        <ItemTemplate>
                            <asp:DropDownList ID="ddlRelationship" runat="server" CssClass="sbControl" Width="100%"></asp:DropDownList>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Affiliates?" ItemStyle-CssClass="gridViewItem" ItemStyle-Wrap="false" ItemStyle-Width="20%">
                        <ItemTemplate>
                            <asp:TextBox ID="tbAffiliates" runat="server" CssClass="sbControl" Width="100%"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        
        </asp:Panel>
        
        <br />                            
        <div style="text-align: center;">
            <asp:LinkButton ID="lbBack" runat="server" Text="<< Back" CommandName="Back" OnClick="lbBack_Click" />&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="lbSubmit" runat="server" Text="Submit" CommandName="Create" OnClick="lbSubmitForm_Click" />&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="lbCancel" runat="server" Text="Close" CommandName="Cancel" OnClick="lbCancelForm_Click"  />
        </div>
        <br />
    
    </asp:Panel>
    
    </ContentTemplate>
    
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="lbBack" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="lbSubmit" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="lbCancel" EventName="Click" />
    </Triggers>
    
</asp:UpdatePanel>

</UC:ModalForm>