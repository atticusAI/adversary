﻿using System;
using System.Data.SqlClient;
using HH.UI;
using HH.SQL;
using HH.UI.AutoComplete;

public partial class Docketing : ScreeningBasePage
{
    AutoCompleteField _acDocketingAtty; 
    AutoCompleteField _acDocketingStaff;

    protected void Page_Init(object sender, EventArgs e)
    {
        _acDocketingAtty = BuildEmployeeSearchAutoComplete("_acDocketingAtty", "DocketingAtty", _SessionData.MemoData.DocketingAtty);
        _acDocketingAtty.SearchRequest += new AutoCompleteSearchRequest(ac_OnSearchRequest);
        _acDocketingAtty.Clear += new AutoCompleteFieldCleared(ac_OnClear);
        _phAtty.Controls.Add(_acDocketingAtty);

        _acDocketingStaff = BuildEmployeeSearchAutoComplete("_acDocketingStaff", "DocketingStaff", _SessionData.MemoData.DocketingStaff);
        _acDocketingStaff.SearchRequest += new AutoCompleteSearchRequest(ac_OnSearchRequest);
        _acDocketingStaff.Clear += new AutoCompleteFieldCleared(ac_OnClear);
        _phStaff.Controls.Add(_acDocketingStaff);
    }

    
    protected void Page_Load(object sender, EventArgs e)
    {
        _MasterPage.PostBackEvent += new HH.Branding.MasterPagePostBackEventHandler(MasterPage_PostBackEvent);
    }

    bool MasterPage_PostBackEvent(object sender, HH.Branding.MasterPagePostBackEventArgs e)
    {
        try
        {
            DoDataSave(_SessionData.MemoData);
            return true;
        }
        catch (Exception ex)
        {
            _MasterPage.ShowError(ex);
            return false;
        }
    }


    AutoCompleteFieldMode ac_OnSearchRequest(IAutoCompleteField sender, string searchText)
    {
        if (!ValidEmployeeSelection(searchText))
        {
            _MasterPage.FeedbackMessage = searchText + " is not a valid employee";
            _MasterPage.FeedbackMode = HH.Branding.MasterPageFeedbackMode.Error;
            return AutoCompleteFieldMode.ShowSearchBox;
        }

        if (sender == _acDocketingAtty)
            _SessionData.MemoData.DocketingAtty = ParseSearchRequestTextForNumber(searchText);
        else
            _SessionData.MemoData.DocketingStaff = ParseSearchRequestTextForNumber(searchText);
        return AutoCompleteFieldMode.ShowResults;
    }

    AutoCompleteFieldMode ac_OnClear(IAutoCompleteField sender)
    {
        if (sender == _acDocketingAtty)
            _SessionData.MemoData.DocketingAtty = null;
        else
            _SessionData.MemoData.DocketingStaff = null;

        return AutoCompleteFieldMode.ShowSearchBox;
    }
}
