﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="LegalWork.aspx.cs" Inherits="LegalWork" MasterPageFile="~/Master.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="Content" ContentPlaceHolderID="UpperContentPlaceHolder" runat="server">

<asp:UpdatePanel ID="upLegalWork" runat="server">
    <ContentTemplate>
    
        Please choose the type of Legal Work:<br />
        <asp:RadioButtonList ID="rblWorkType" runat="server" RepeatDirection="Vertical" OnSelectedIndexChanged="rblWorkType_SelectedIndexChanged" AutoPostBack="true">
            <asp:ListItem Text="Work for H&H As A Firm" Value="Firm"></asp:ListItem>
            <asp:ListItem Text="Work for an H&H Employee or Partner" Value="Employee"></asp:ListItem>
        </asp:RadioButtonList>
        <br />
        <br />
        
        <asp:Panel ID="pnlFirm" runat="server" Visible="false">
            
            Verify and complete the name of the organization that work will be performed for<br />
            <br />
            <asp:TextBox ID="tbOrg" runat="server" CssClass="sbControl" TextMode="MultiLine" Rows="2"></asp:TextBox>
            
        </asp:Panel>
        
        <asp:Panel ID="pnlEmployee" runat="server" Visible="false">
            
            4-digit Payroll Number or Name of the Employee to search for
            <asp:TextBox ID="tbEmployeeSearch" runat="server" Width="250px" autocomplete="off" AutoPostBack="true" OnTextChanged="tbEmployeeSearch_TextChanged"></asp:TextBox><br />
            <asp:PlaceHolder ID="phEmployeeSearchResults" runat="server"></asp:PlaceHolder>
            
        </asp:Panel>
        
        <asp:Panel ID="pnlEmployeeSelection" runat="server" Visible="false">
        
            Verify and complete the H&H Employee or Partner's information that work will be performed for<br />
            <br />
            <table>
                <tr>
                    <td style="width: 150px;">Employee Number</td>
                    <td style="width: 250px;"><asp:Label ID="lblNumber" runat="server" Font-Bold="false"></asp:Label></td>
                </tr>
                <tr>
                    <td>First Name</td>
                    <td><asp:TextBox ID="tbFName" runat="server" CssClass="sbControl"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Middle Name</td>
                    <td><asp:TextBox ID="tbMName" runat="server" CssClass="sbControl"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Last Name</td>
                    <td><asp:TextBox ID="tbLName" runat="server" CssClass="sbControl"></asp:TextBox></td>
                </tr>
                <tr>
                    <td colspan="2" style=" text-align: center; font-weight: bold;"><br /><asp:LinkButton ID="btnClear" runat="server" Text="Clear Form & Search Employees Again" OnClick="btnClear_Click"></asp:LinkButton></td>
                </tr>
            </table>
            
        </asp:Panel>
        
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="rblWorkType" EventName="SelectedIndexChanged" />
    </Triggers>
</asp:UpdatePanel>

</asp:Content>