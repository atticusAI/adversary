﻿using System;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using HH.Screen;
using HH.UI;
using HH.UI.AutoComplete;

public partial class BasicInfo : ScreeningBasePage
{
    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);

        if (!IsPostBack)
        {
            if (_SessionData.MemoData.RefDate == null)
                _MasterPage.Screen.ScreenRowControlCreated += new ScreenRowControlCreatedEventHandler(Screen_ScreenRowControlCreated);
            if (_SessionData.MemoData.Confidential == null)
                _SessionData.MemoData.Confidential = false;
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        AutoCompleteField sbAttEntering = BuildEmployeeSearchAutoComplete("sbAttEntering", "AttEntering", _SessionData.MemoData.AttEntering);
        sbAttEntering.SearchRequest += new AutoCompleteSearchRequest(AttEnteringSearchSelected);
        sbAttEntering.Clear += new AutoCompleteFieldCleared(AttEnteringSearchClear);
        _MasterPage.Screen.AddControlToFormAt(3, sbAttEntering, "Attorney Preparing Memo", "Enter a last name or employee ID");

        if (_SessionData.MemoData.ScrMemID == 0 &&
            string.IsNullOrEmpty(_SessionData.MemoData.PersonnelID) &&
            !IsPostBack)
            _SessionData.MemoData.PersonnelID = _SessionData.LoginEmpCode;
        AutoCompleteField sbPersonnelID = BuildEmployeeSearchAutoComplete("sbPersonnelID", "PersonnelID", _SessionData.MemoData.PersonnelID);
        sbPersonnelID.SearchRequest += new AutoCompleteSearchRequest(PersonnelIDSearchSelected);
        sbPersonnelID.Clear += new AutoCompleteFieldCleared(PersonnelIDSearchClear);
        _MasterPage.Screen.AddControlToFormAt(4, sbPersonnelID, "Input By", "Enter a last name or employee ID");
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    private void Screen_ScreenRowControlCreated(ScreenBuilder sb, PropertyInfo info, object dataSource, object value, Control control)
    {
        if (info.Name == "RefDate")
            ((TextBox)control).Text = DateTime.Now.ToShortDateString();
    }

    private AutoCompleteFieldMode AttEnteringSearchClear(IAutoCompleteField sender)
    {
        _SessionData.MemoData.AttEntering = null;
        return AutoCompleteFieldMode.ShowSearchBox;
    }

    private AutoCompleteFieldMode AttEnteringSearchSelected(IAutoCompleteField sender, string text)
    {
        AutoCompleteFieldMode mode = AutoCompleteFieldMode.ShowResults;
        if (!ValidEmployeeSelection(text))
            mode = AutoCompleteFieldMode.ShowSearchBox;

        _SessionData.MemoData.AttEntering = ParseSearchRequestTextForNumber(text);
        return mode;
    }

    private AutoCompleteFieldMode PersonnelIDSearchClear(IAutoCompleteField sender)
    {
        _SessionData.MemoData.PersonnelID = null;
        return AutoCompleteFieldMode.ShowSearchBox;
    }

    private AutoCompleteFieldMode PersonnelIDSearchSelected(IAutoCompleteField sender, string text)
    {
        AutoCompleteFieldMode mode = AutoCompleteFieldMode.ShowResults;
        if (!ValidEmployeeSelection(text))
            mode = AutoCompleteFieldMode.ShowSearchBox;

        _SessionData.MemoData.PersonnelID = ParseSearchRequestTextForNumber(text);
        return mode;
    }

}
