﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="RejectionLetter.aspx.cs" Inherits="RejectionLetter" MasterPageFile="~/Master.master" %>
<%@ Register TagName="FileListControl" TagPrefix="UC" Src="~/_usercontrols/FileListControl.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="Content" ContentPlaceHolderID="UpperContentPlaceHolder" runat="server">

<%--commented out by A Reimer 03/21/12 for 'Rejected to Modification Required' change  --%>
<%--Specify whether a rejection letter will be attached--%>
<%--blmcgee@hollandhart.com 2014-01-10--%>
<%--Specify whether a request for modification will be attached--%>
Specify whether a rejection letter will be attached
<asp:RadioButtonList ID="rblRejection" runat="server" RepeatDirection="Horizontal" AutoPostBack="false">
    <asp:ListItem Text="Yes" Value="True"></asp:ListItem>
    <asp:ListItem Text="No" Value="False"></asp:ListItem>
</asp:RadioButtonList>
<ajax:Accordion ID="accRejection" runat="server" 
                AutoSize="None"
                FadeTransitions="true"
                TransitionDuration="80"
                FramesPerSecond="80"
                SuppressHeaderPostbacks="true"
                RequireOpenedPane="false" 
                SelectedIndex="-1">
    <Panes>
    
        <ajax:AccordionPane runat="server" ID="rejectionYes" >
            <Content>
                <br />
                <br />
                <br />
                <UC:FileListControl ID="fcRejection" runat="server" />
            </Content>
       </ajax:AccordionPane>
       
       <ajax:AccordionPane runat="server" ID="rejectionNo">
            <Content>
                <br />
                <br />
                <br />
                <%--commented out by A Reimer 03/21/12 for 'Rejected to Modification Required' change  --%>
               <%-- Reason why a rejection letter will not be attached<br />--%>
               <%--blmcgee@hollandhart.com 2014-01-10 
               Reason why a request for modification letter will not be attached<br />--%>
               Reason why a rejection letter will not be attached<br />
                <br />
                <asp:TextBox ID="tbReason" runat="server" Rows="5" TextMode="MultiLine" CssClass="sbControl"></asp:TextBox>
            </Content>
       </ajax:AccordionPane>
       
   </Panes>
   
</ajax:Accordion>

</asp:Content>