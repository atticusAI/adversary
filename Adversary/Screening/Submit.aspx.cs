﻿using System;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using HH.Branding;
using HH.UI.FileList;

public partial class Submit : ScreeningBasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        bool isLocked = _SessionData.MemoData.IsLocked();
        //_MasterPage.FooterNavVisible = false;
        //fcFiles.FileListTemplate = new FileListTemplate();
        //fcFiles.FileDirectory = GetUploadDirectoryURL(_SessionData.MemoData.ScrMemID);
        fcFiles.FileListItemDataBound += new RepeaterItemEventHandler(base.FileList_ItemDataBound);
        fcFiles.AllowFileEdit = !isLocked;
        fcFiles.AllowFileDelete = !isLocked;

        BindFileListControl();
        ShowStatus();

        if (isLocked)
        {
            fcFiles.Mode = FileListControlMode.FileListOnly;
            lbUpload.InnerText = "Show/Hide Uploaded Files";
        }
        else
        {
            fcFiles.OnFileUploaded += new FileUploaded(fcFiles_OnFileUploaded);
            fcFiles.OnFileEditClick += new FileEditClick(fcFiles_OnFileEditClick);
            fcFiles.OnFileDeleteClick += new FileDeleteClick(fcFiles_OnFileDeleteClick);
            fcFiles.OnFileEditSubmit += new FileEditSubmit(fcFiles_OnFileEditSubmit);
        }

        lbSubmit.Visible = _SessionData.MemoData.CanSubmit();
        //lbPrint.HRef = "javascript:openExternalWindow('PrintView.aspx?ScrMemType=" + _SessionData.MemoData.ScrMemType + "')";
        lbPrint.HRef = "PrintView.aspx?ScrMemType=" + _SessionData.MemoData.ScrMemType + "&ScrMemID=" + _SessionData.MemoData.ScrMemID.ToString();
    }

    private void BindFileListControl()
    {
        fcFiles.DataSource = _SessionData.MemoData.Attachments;
        fcFiles.DataBind();
        fcFiles.EnableDescription = true;
    }

    bool fcFiles_OnFileUploaded(IFileListControl sender, FileUpload file, string description)
    {
        if (!FileListControl_OnFileUploaded(sender, file, description))
            return false;
        BindFileListControl();
        ShowStatus();
        return true;
    }

    bool fcFiles_OnFileEditClick(IFileListControl sender, object dataItem)
    {
        Attachments attachment = (Attachments)dataItem;
        if (!FileListControl_OnFileEditClick(sender, dataItem))
            return false;
        fcFiles.EnableDescription = attachment.FileDescription != "Conflict Report" &&
                                    attachment.FileDescription != "Engagement Letter" &&
                                    attachment.FileDescription != "Written Policy";
        ShowStatus();
        return true;
    }

    bool fcFiles_OnFileDeleteClick(IFileListControl sender, object dataItem)
    {
        if (!FileListControl_OnFileDeleteClick(sender, dataItem))
            return false;
        BindFileListControl();
        ShowStatus();
        return true;
    }

    bool fcFiles_OnFileEditSubmit(IFileListControl sender, object dataItem, string newFileName, string description)
    {
        if (!FileListControl_OnFileEditSubmit(sender, dataItem, newFileName, description))
            return false;
        BindFileListControl();
        ShowStatus();
        return true;
    }


    protected void lb_Click(object sender, EventArgs e)
    {
        switch (((LinkButton)sender).CommandName)
        {
            case "print":
                OpenPrintView();
                break;

            case "submit":

                if (!_SessionData.MemoData.CanSubmit())
                {
                    _MasterPage.FeedbackMessage = "This memo has already been submitted for approval and cannot be submitted again";
                    _MasterPage.FeedbackMode = MasterPageFeedbackMode.Error;
                    return;
                }

                if (!_SessionData.MemoData.Tracking.TrackingID.HasValue)
                    _SessionData.MemoData.Tracking.TrackingID = 0;
                _SessionData.MemoData.Tracking.ScrMemID = _SessionData.MemoData.ScrMemID;
                _SessionData.MemoData.Tracking.StatusTypeID = TrackingStatusType.Submitted;
                bool isResubmit = (_SessionData.MemoData.Tracking.RejectionHold.HasValue && _SessionData.MemoData.Tracking.RejectionHold.Value);
                if (!isResubmit)
                    _SessionData.MemoData.Tracking.SubmittedOn = DateTime.Now;
                DoDataSave(_SessionData.MemoData.Tracking);

                lbSubmit.Visible = false;

                ShowStatus();

                try
                {
                    //SendTrackingEmail(_SessionData.MemoData, true, true);
                    SendApproversEmail(_SessionData.MemoData, isResubmit);
                    SendStatusEmail(_SessionData.MemoData, isResubmit);
                }
                catch (Exception ex)
                {
                    _MasterPage.FeedbackMode = MasterPageFeedbackMode.Warning;
                    _MasterPage.ShowError("Your screening memo has been submitted but an error occurred while attempting to send an email to the intended recipients. Please notify the adversary group of this issue.");
                    HandleError(ex);
                }

                break;
        }
    }


}
