﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Search.aspx.cs" Inherits="Search" EnableEventValidation="false" MasterPageFile="~/Master.master" %>

<%@ Register TagPrefix="UC" TagName="MemoList" Src="~/Screening/_ScreeningMemoList.ascx" %>
<%@ Register TagPrefix="UC" Assembly="HH" Namespace="HH.UI.ModalPopupForm" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register TagPrefix="UC" Assembly="HH" Namespace="HH.UI.AutoComplete" %>

<asp:Content ID="Content" ContentPlaceHolderID="UpperContentPlaceHolder" runat="server">

<asp:UpdatePanel ID="_upSearch" runat="server" RenderMode="Block" UpdateMode="Conditional">
    <ContentTemplate>

        <script language="javascript" type="text/javascript">
        
        function DoSearchAnimation()
        {
            $get('<%=_resultsImg.ClientID%>').style.visibility = 'visible';
            $get('<%=_btnSearch.ClientID%>').style.disabled = true;
            $get('<%=_btnClear.ClientID%>').style.disabled = true;
        }
        </script>


        <%--<div style="font-weight: bold; color: Red; width:50%;" id="_divOldSiteNotice" runat="server">
            ** Looking for a memo that was created prior to December 1st, 2010? If you used the old screening memo site to create your memo click the link below to search for it:<br />
            <br />
            <a href="http://vhhcf01.hollandhart.com/samedi/website/htdocs/acct/adversary/scrmem/dev/" target="_blank">http://vhhcf01.hollandhart.com/samedi/website/htdocs/acct/adversary/scrmem/dev/</a>
        </div>--%>
        <div style="font-weight:bold;color:Red;width:50%;" id="_divOldSiteNotice" runat="server">
            ** Looking for a memo that was created prior to December 1st, 2010? Please use the link below to ExpertImage to search for it:<br />
            <a href="http://expertimage.hollandhart.com" target="_blank">http://expertimage.hollandhart.com</a>
        </div>
        <br />
        <br />
        <table cellpadding="2px;">
            <tr>
                <td style="font-weight: bold; vertical-align:top;">Client</td>
                <td style=" vertical-align:top;">
                    <UC:AutoCompleteField ID="_ucClient" runat="server" TextBoxSearchMessage="" ServiceMethod="GetClientList" ContextKey="ClientNumber" Mode="ShowSearchBox" MinimumCharacters="2" ClearButtonText="Clear Client" TextChangeCausesPostBack="false" />
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold; vertical-align: top;">Matter Name</td>
                <td style=" vertical-align:top;">
                    <UC:AutoCompleteField ID="_ucMatterName" runat="server" TextBoxSearchMessage="" ServiceMethod="GetMemoList" ContextKey="MatterName" Mode="ShowSearchBox" MinimumCharacters="2" ClearButtonText="Clear Client" TextChangeCausesPostBack="false" />
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Client Matter Number</td>
                <td>
                    <UC:AutoCompleteField ID="_ucCMNum" runat="server" TextBoxSearchMessage="" ServiceMethod="GetClientMatterNumberList" ContextKey="ClientMatterNumber" Mode="ShowSearchBox" MinimumCharacters="2" ClearButtonText="Clear Client" TextChangeCausesPostBack="false" />
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Attorney Preparing Memo</td>
                <td>
                    <UC:AutoCompleteField ID="_ucAtty" runat="server" TextBoxSearchMessage="" ServiceMethod="GetEmployeeList" ContextKey="AttEntering" Mode="ShowSearchBox" MinimumCharacters="2" ClearButtonText="Clear Client" TextChangeCausesPostBack="false" />
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Input By</td>
                <td>
                    <UC:AutoCompleteField ID="_ucEmp" runat="server" TextBoxSearchMessage="" ServiceMethod="GetEmployeeList" ContextKey="PersonnelID" Mode="ShowSearchBox" MinimumCharacters="2" ClearButtonText="Clear Employee" TextChangeCausesPostBack="false" />
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Screening Memo Number</td>
                <td>
                    <UC:AutoCompleteField ID="_ucScrMemID" runat="server" TextBoxSearchMessage="" ServiceMethod="GetMemoList" ContextKey="ScrMemID" Mode="ShowSearchBox" MinimumCharacters="4" ClearButtonText="Clear Employee" TextChangeCausesPostBack="false" />
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center;">
                    <br />
                    <asp:Button ID="_btnSearch" runat="server" Text="Search" Width="100px" OnClick="btnSearch_Click" OnClientClick="DoSearchAnimation();" />
                    <span style="vertical-align: bottom;"><img id="_resultsImg" runat="server" alt="loading" src="~/_css/ajax-loader.gif" width="25" height="25" style="visibility: hidden;" /></span>
                    <asp:Button ID="_btnClear" runat="server" Text="Clear" Width="100px" OnClick="btnClear_Click" />
                </td>
            </tr>            
        </table>
        
        
        <!-- Loading Image -->
        <ajax:AnimationExtender id="animResultsImg" runat="server" TargetControlID="_btnSearch" BehaviorID="animateImgBehavior">
            <Animations>
                <OnClick>
                    <Sequence fps="50">
                        <StyleAction AnimationTarget="_resultsImg" Attribute="visibility" Value="visible" /> 
                        <EnableAction AnimationTarget="_btnSearch" Enabled="false" /> 
                        <EnableAction AnimationTarget="_btnClear" Enabled="false" /> 
                    </Sequence>
                </OnClick>
            </Animations>
        </ajax:AnimationExtender>


        <!-- Results GridView -->
        <UC:ModalForm ID="_modalResults" runat="server" Width="75%" Y="50" CancelControlID="_lbClose">
            <asp:Panel ID="_upResults" runat="server">

                <asp:Panel ID="_pnlResultsContainer" runat="server">

                    <UC:MemoList ID="_mlResults" runat="server" />

                </asp:Panel>

                <div style='text-align: center;'>
                    <asp:LinkButton ID="_lbClose" runat="server" Text="Close" OnClick="lbClose_Click"></asp:LinkButton>
                </div>

            </asp:Panel>
        </UC:ModalForm>
         
    </ContentTemplate>
    
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="_btnSearch" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="_btnClear" EventName="Click" />
    </Triggers>

</asp:UpdatePanel>



</asp:Content>


