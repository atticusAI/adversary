﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class ApprovalTable : System.Web.UI.UserControl
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!((BasePage)Page)._SessionData.MemoLoaded)
            return;

        Memo m = ((BasePage)Page)._SessionData.MemoData;
        Tracking t = m.Tracking;
        if (t.StatusTypeID != TrackingStatusType.None)
        {
            if (t.APApproved.HasValue && t.APApproved.Value)
            {
                _lblAPName.Text = (!string.IsNullOrEmpty(t.APSignature)) ? t.APSignature : string.Empty;
                _lblAPDate.Text = (t.APDate.HasValue) ? t.APDate.Value.ToShortDateString() : string.Empty;
            }
            if (t.PGMApproved.HasValue && t.PGMApproved.Value)
            {
                _lblPGMName.Text = (!string.IsNullOrEmpty(t.PGMSignature)) ? t.PGMSignature : string.Empty;
                _lblPGMDate.Text = (t.PGMDate.HasValue) ? t.PGMDate.Value.ToShortDateString() : string.Empty;
            }
            if (t.AdversaryAdminApproved.HasValue && t.AdversaryAdminApproved.Value)
            {
                _lblAdvSignature.Text = (!string.IsNullOrEmpty(t.AdversaryAdminSignature)) ? t.AdversaryAdminSignature : string.Empty;
                _lblAdvSignature.Text += (_lblAdvSignature.Text != string.Empty && t.AdversaryAdminDate.HasValue) ? "&nbsp;&nbsp;&nbsp;" + t.AdversaryAdminDate.Value.ToShortDateString() : string.Empty;
            }
            
            if (!string.IsNullOrEmpty(t.APARSignature))
                _lblARName.Text = t.APARSignature;
            if (!string.IsNullOrEmpty(t.PGMARSignature))
                _lblARName.Text = t.PGMARSignature;
            if (!string.IsNullOrEmpty(t.APExceptionSignature))
                _lblExceptionName.Text = t.APExceptionSignature;
            if (!string.IsNullOrEmpty(t.PGMExceptionSignature))
                _lblExceptionName.Text = t.PGMExceptionSignature;

            _lblCM.Text = string.Empty;
            foreach (ClientMatterNumber cm in m.ClientMatterNumbers)
            {
                if (_lblCM.Text != string.Empty)
                    _lblCM.Text += ", ";
                _lblCM.Text += cm.Number;
            }

            _lblOpened.Text = (!string.IsNullOrEmpty(t.Opened)) ? t.Opened : string.Empty;
            _lblConflicts.Text = (!string.IsNullOrEmpty(t.Conflicts)) ? t.Conflicts : string.Empty;
            _lblFeeSplits.Text = (!string.IsNullOrEmpty(t.FeeSplits)) ? t.FeeSplits : string.Empty;
            _lblFinalCheck.Text = (!string.IsNullOrEmpty(t.FinalCheck)) ? t.FinalCheck : string.Empty;
        }

    }
}
