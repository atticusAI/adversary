﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using HH;
using HH.Branding;
using HH.Screen;
using HH.SQL;
using HH.Extensions;
using System.Data.SqlClient;
using HH.UI;


public partial class _Parties : System.Web.UI.UserControl
{
    private enum FormMode
    {
        Names,
        Relationships
    }

    //private List<string> _names = new List<string>();
    //private FormMode _mode = FormMode.Names;

    private ScreeningBasePage _scrPage = null;
    private ScreeningBasePage ScreeningPage
    {
        get
        {
            if (_scrPage == null)
                _scrPage = (ScreeningBasePage)Page;
            return _scrPage;
        }
    }

    public PartyCodes PartyCode { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        list.AutoGenerateColumns = false;
        list.FieldsAdding += new GridFieldsAdding(Parties_FieldsAdding);
        list.RowCreated += new GridViewRowEventHandler(Parties_RowCreated);
        list.OnEntitySubmitted += new EntitySubmitEventHandler(list_OnEntitySubmitted);
        list.ConnectionString = AdversaryData.ConnectionString;
        list.ScreenID = ScreeningPage.ParseURLForScreenID(Request.Path);
        list.EntityID = (PartyCode == PartyCodes.Related) ? "RelatedParties" : "AdverseParties";// PartyCode.ToString();

        list.DataSource = GetDataSource();
        list.DataBind();

        if (IsPostBack)
            return;

        SetFormVisibility(FormMode.Names);
        taParties.Text = string.Empty;
    }


    void Parties_FieldsAdding(GridView sender, object dataSource)
    {
        BoundField bf;
        
        bf = new BoundField();
        bf.DataField = "PartyRelationshipName";
        bf.SortExpression = "PartyRelationshipName";
        bf.HeaderText = "Relationship";
        sender.Columns.Add(bf);

        bf = new BoundField();
        bf.DataField = "PartyType";
        bf.SortExpression = "PartyType";
        bf.HeaderText = "Type";
        sender.Columns.Add(bf);
    }

    void Parties_RowCreated(object sender, GridViewRowEventArgs e)
    {
        TableCell td = new TableCell();

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Party party = (Party)e.Row.DataItem;
            if (party.PartyType == PartyTypes.Individual.ToString())
            {
                if (!string.IsNullOrEmpty(party.PartyFName))
                    td.Text += party.PartyFName;
                if (!string.IsNullOrEmpty(td.Text) && !td.Text.EndsWith(" "))
                    td.Text += " ";

                if (!string.IsNullOrEmpty(party.PartyMName))
                    td.Text += party.PartyMName;
                if (!string.IsNullOrEmpty(td.Text) && !td.Text.EndsWith(" "))
                    td.Text += " ";

                if (!string.IsNullOrEmpty(party.PartyLName))
                    td.Text += party.PartyLName;
            }
            else
            {
                td.Text = party.PartyOrganization;
            }
        }
        else
        {
            td.Text = "<strong>Party Name</strong>";
        }

        e.Row.Cells.AddAt(e.Row.Cells.Count, td);
    }

    public List<Party> GetDataSource()
    {
        return (PartyCode == PartyCodes.Related) ? ScreeningPage._SessionData.MemoData.RelatedParties : ScreeningPage._SessionData.MemoData.AdverseParties;
    }

    private void list_OnEntitySubmitted(IListContainerControl sender, object dataSource, object entity, HH.Screen.EntitySubmitType type)
    {
        using (SqlConnection conn = AdversaryData.GetConnection())
        {
            DatabaseWorker worker = new DatabaseWorker(conn);
            Party party = (Party)entity;
            party.ScrMemID = ScreeningPage._SessionData.MemoData.ScrMemID;

            switch (type)
            {
                case EntitySubmitType.Create:
                    worker.Commit(party);
                    ScreeningPage._SessionData.MemoData.Parties.Add(party);
                    break;
                case EntitySubmitType.Modify:
                    int modIndex = GetIndex(party);
                    worker.Commit(party);
                    ScreeningPage._SessionData.MemoData.Parties.RemoveAt(modIndex);
                    ScreeningPage._SessionData.MemoData.Parties.Insert(modIndex, party);
                    break;
                case EntitySubmitType.Delete:
                    int delIndex = GetIndex(party);
                    worker.Delete(party);
                    ScreeningPage._SessionData.MemoData.Parties.RemoveAt(delIndex);
                    break;
            }
        }
        list.DataSource = GetDataSource();
        list.DataBind();
    }

    private int GetIndex(Party party)
    {
        IList<Party> parties = ScreeningPage._SessionData.MemoData.Parties;
        for (int i = 0; i < parties.Count; i++)
        {
            if (parties[i].PartyID == party.PartyID)
                return i;
        }

        throw new Exception("Could not find party information with ID " + party.PartyID);
    }

    protected void gvRelationships_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.DataRow)
            return;

        string name = e.Row.DataItem.ToString();
        
        TextBox tbName = (TextBox)e.Row.FindControl("tbName");
        tbName.Text = name;

        DropDownList ddlType = (DropDownList)e.Row.FindControl("ddlType");
        UIUtils.BindEnumToListControl(ddlType, typeof(PartyTypes));

        DropDownList ddlRelationship = (DropDownList)e.Row.FindControl("ddlRelationship");
        ddlRelationship.DataSource = (PartyCode == PartyCodes.Related) ? Party.PartyStatusRelated : Party.PartyStatusAdverse;
        ddlRelationship.DataTextField = CMSData.PARTY_STATUS_PLUS_CODE;
        ddlRelationship.DataValueField = CMSData.PARTY_STATUS_CODE;
        ddlRelationship.DataBind();
    }

    private void SetFormVisibility(FormMode mode)
    {
        lblFeedback.Text = string.Empty;
        pnlNames.Visible = (mode == FormMode.Names);
        pnlRelationships.Visible = (mode == FormMode.Relationships);
        lbBack.Visible = (mode == FormMode.Relationships);
    }

    protected void lbShowForm_Click(object sender, EventArgs e)
    {
        SetFormVisibility(FormMode.Names);
        modalForm.Show();
    }

    protected void lbBack_Click(object sender, EventArgs e)
    {
        SetFormVisibility(FormMode.Names);
    }

    protected void lbCancelForm_Click(object sender, EventArgs e)
    {
        SetFormVisibility(FormMode.Names);
        modalForm.Hide();
    }

    protected void lbSubmitForm_Click(object sender, EventArgs e)
    {
        if (pnlRelationships.Visible)
            SubmitRelationships();
        else
            SubmitParties();
    }

    private void SubmitParties()
    {
        string text = taParties.Text.Trim();
        if (text == string.Empty)
        {
            lblFeedback.Text = "Please type in one or more names to continue";
            SetFormVisibility(FormMode.Names);
            return;
        }

        List<string> names = new List<string>();
        string[] values = text.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
        foreach (string val in values)
        {
            string v = val.Trim();
            if (!string.IsNullOrEmpty(v))
                names.Add(v);
        }

        gvRelationships.DataSource = names;
        gvRelationships.DataBind();

        SetFormVisibility(FormMode.Relationships);
    }

    private void SubmitRelationships()
    {
        int partyCount = ScreeningPage._SessionData.MemoData.Parties.Count;

        using (SqlConnection conn = AdversaryData.GetConnection())
        {
            DatabaseWorker worker = new DatabaseWorker(conn);
            foreach (GridViewRow gvr in gvRelationships.Rows)
            {
                TextBox tbName = (TextBox)gvr.FindControl("tbName");
                DropDownList ddlType = (DropDownList)gvr.FindControl("ddlType");
                DropDownList ddlRelationship = (DropDownList)gvr.FindControl("ddlRelationship");
                string fullName = tbName.Text.Trim();

                TextBox tbAffiliates = (TextBox)gvr.FindControl("tbAffiliates");
                string partyAffiliates = tbAffiliates.Text;


                PartyTypes partyType = (PartyTypes)Enum.Parse(typeof(PartyTypes), ddlType.SelectedValue);
                Party party = new Party();
                party.PartyID = 0;
                party.ScrMemID = ScreeningPage._SessionData.MemoData.ScrMemID;
                party.PartyRelationshipName = ddlRelationship.SelectedItem.Text;
                party.PartyRelationshipCode = ddlRelationship.SelectedItem.Value;
                party.PartyType = partyType.ToString();
                party.PC = (int)PartyCode;

                bool add = true;
                if (partyType == PartyTypes.Individual)
                {
                    string[] names = fullName.Split(new char[] { ' ' });
                    switch (names.Length)
                    {
                        case 0:
                            //ignore
                            add = false;
                            break;
                        case 1:
                            party.PartyLName = names[0];
                            break;
                        case 2:
                            party.PartyFName = names[0];
                            party.PartyLName = names[1];
                            break;
                        case 3:
                            party.PartyFName = names[0];
                            party.PartyMName = names[1];
                            party.PartyLName = names[2];
                            break;
                        default:
                            party.PartyFName = names[0];
                            party.PartyMName = names[1];
                            party.PartyLName = names[2];
                            for (int i = 3; i < names.Length; i++)
                                party.PartyLName += " " + names[i];
                            break;
                    }
                }
                else
                {
                    party.PartyOrganization = fullName;
                    //party.PartyAffiliates = partyAffiliates;
                }

                if (add)
                {
                    party.PartyAddressInfo.PartyAddressID = null;
                    worker.Commit(party);
                    ScreeningPage._SessionData.MemoData.Parties.Add(party);
                }
            }
        }

        if (ScreeningPage._SessionData.MemoData.Parties.Count > partyCount)
        {
            ScreeningPage._MasterPage.FeedbackMode = MasterPageFeedbackMode.Success;
            ScreeningPage._MasterPage.FeedbackMessage = "Your submission was successful";

            list.DataSource = GetDataSource();
            list.DataBind();
            list.Update();
        }
        
        SetFormVisibility(FormMode.Names);
        taParties.Text = string.Empty;
        modalForm.Hide();
    }
}
