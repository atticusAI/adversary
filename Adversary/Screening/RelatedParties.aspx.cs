﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using HH.Screen;

using HH.Utils;
using HH;

public partial class RelatedParties : ScreeningBasePage
{

    protected void Page_Load(object sender, EventArgs e)
    {
        _parties.PartyCode = PartyCodes.Related;
    }
}
