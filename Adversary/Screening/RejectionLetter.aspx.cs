﻿using System;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using HH;
using HH.Branding;
using HH.UI.FileList;
using HH.SQL;
using System.Data.SqlClient;


public partial class RejectionLetter : ScreeningBasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        _MasterPage.PostBackEvent += new MasterPagePostBackEventHandler(_MasterPage_PostBackEvent);
        //fcRejection.FileListTemplate = new FileListTemplate();
        //fcRejection.FileDirectory = GetUploadDirectoryURL(_SessionData.MemoData.ScrMemID);
        fcRejection.FileListItemDataBound += new RepeaterItemEventHandler(base.FileList_ItemDataBound);
        fcRejection.OnFileUploaded += new FileUploaded(fc_OnFileUploaded);
        fcRejection.OnFileEditClick += new FileEditClick(fc_OnFileEditClick);
        fcRejection.OnFileDeleteClick += new FileDeleteClick(fc_OnFileDeleteClick);
        fcRejection.OnFileEditSubmit += new FileEditSubmit(fc_OnFileEditSubmit);
        fcRejection.EnableDescription = false;
        ///commented out by A Reimer 03/21/12 for 'Rejected to Modification Required' change
        //fcRejection.FileDescription = "Rejection Letter";
        //blmcgee@hollandhart.com 2014-01-13
        // fcRejection.FileDescription = "Request for Modification";
        fcRejection.FileDescription = "Rejection Letter";

        rblRejection.Items[0].Attributes.Add("onclick", "setAccordianPane('" + accRejection.ClientID + "', 0);");
        rblRejection.Items[1].Attributes.Add("onclick", "setAccordianPane('" + accRejection.ClientID + "', 1);");


        BindFileControls();

        if (IsPostBack)
            return;
        ///commented out by A Reimer 03/21/12 for 'Rejected to Modification Required' change
        //fcRejection.FileListHeader = (_SessionData.MemoData.RejectionLetterFiles.Count == 0) ? "Please Upload a Rejection Letter" : "Attached Rejection Letter Files";
        // blmcgee@hollandhart.com 2014-01-13
        // fcRejection.FileListHeader = (_SessionData.MemoData.RejectionLetterFiles.Count == 0) ? "Please Upload a Request for Modification Letter" : "Attached Request for Modification Letter Files";
        fcRejection.FileListHeader = (_SessionData.MemoData.RejectionLetterFiles.Count == 0) ? "Please Upload a Rejection Letter" : "Attached Rejection Letter Files";

        if (!_SessionData.MemoData.NoRejectionLetter.HasValue)
            return;

        int index = _SessionData.MemoData.NoRejectionLetter.Value ? 1 : 0;
        rblRejection.Items[index].Selected = true;
        accRejection.SelectedIndex = index;
        ///commented out by A Reimer 03/21/12 for 'Rejected to Modification Required' change
        //fcRejection.FileListHeader = (_SessionData.MemoData.EngagementLetterFiles.Count == 0) ? "Please Upload a Rejection Letter" : "Attached Rejection Letter Files";
        // blmcgee@hollandhart.com
        // fcRejection.FileListHeader = (_SessionData.MemoData.EngagementLetterFiles.Count == 0) ? "Please Upload a Request for Modification Letter" : "Attached Request for Modification Letter Files";
        fcRejection.FileListHeader = (_SessionData.MemoData.EngagementLetterFiles.Count == 0) ? "Please Upload a Rejection Letter" : "Attached Rejection Letter Files";
        tbReason.Text = _SessionData.MemoData.NoRejectionLetterReason;
    }

    private bool _MasterPage_PostBackEvent(object sender, MasterPagePostBackEventArgs e)
    {
        try
        {
            if (rblRejection.SelectedIndex >= 0)
            {
                _SessionData.MemoData.NoRejectionLetter = (rblRejection.SelectedIndex == 1);
                _SessionData.MemoData.NoRejectionLetterReason = tbReason.Text.Trim();
            }

            using (SqlConnection conn = AdversaryData.GetConnection())
            {
                DatabaseWorker worker = new DatabaseWorker(conn);
                foreach (Attachments attachment in _SessionData.MemoData.Attachments)
                    DoDataSave(attachment, worker);
                DoDataSave(_SessionData.MemoData,worker);
            }
            return true;
        }
        catch (Exception ex)
        {
            _MasterPage.ShowError(ex);
            return false;
        }
    }

    private void BindFileControls()
    {
        fcRejection.DataSource = _SessionData.MemoData.RejectionLetterFiles;
        fcRejection.DataBind();
    }

    private bool fc_OnFileUploaded(IFileListControl sender, FileUpload file, string description)
    {
        if (!FileListControl_OnFileUploaded(sender, file, description))
            return false;

        BindFileControls();
        return true;
    }

    private bool fc_OnFileEditClick(IFileListControl sender, object dataItem)
    {
        return FileListControl_OnFileEditClick(sender, dataItem);
    }

    private bool fc_OnFileDeleteClick(IFileListControl sender, object dataItem)
    {
        if (!FileListControl_OnFileDeleteClick(sender, dataItem))
            return false;

        BindFileControls();
        return true;
    }

    private bool fc_OnFileEditSubmit(IFileListControl sender, object dataItem, string newFileName, string description)
    {
        if (!FileListControl_OnFileEditSubmit(sender, dataItem, newFileName, description))
            return false;

        //rebind the repeaters
        BindFileControls();
        return true;
    }

}
