﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using HH;
using HH.Extensions;

public partial class ClientAddress : ScreeningBasePage
{
    private bool _hasAddress = false;
    List<Address> _addresses = new List<Address>();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        _MasterPage.Screen.ValidationEnabled = !string.IsNullOrEmpty(_SessionData.MemoData.Address.Address1) ||
                                               !string.IsNullOrEmpty(_SessionData.MemoData.Address.City) ||
                                               !string.IsNullOrEmpty(_SessionData.MemoData.Address.Phone);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (_SessionData.MemoData.Address.AddressID == null)
            _SessionData.MemoData.Address.AddressID = 0;

        if (string.IsNullOrEmpty(_SessionData.MemoData.ClientNumber))
            return;

        CMSData cms = CMSData.GetInstance();
        _addresses = cms.DoClientAddressLookup(_SessionData.MemoData.ClientNumber);
        _addresses = _addresses.Distinct().ToList<Address>();
        if (_addresses.Count == 0)
            return;

        upAddressList.Visible = true;
        lbAddressList.Text = "View Available Addresses for " + _SessionData.MemoData.ClientName;
        repAddressList.ItemDataBound += new RepeaterItemEventHandler(repAddressList_ItemDataBound);

        repAddressList.DataSource = _addresses;
        repAddressList.DataBind();

        if (!_hasAddress)
        {
            upAddressList.Visible = false;
            lbAddressList.Visible = false;
        }
            
    }

    void repAddressList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Address address = (Address)e.Item.DataItem;
        Panel pnlAddress = (Panel)e.Item.FindControl("pnlAddress");
        HtmlGenericControl hr = (HtmlGenericControl)e.Item.FindControl("hrSeperator");
        Panel pnlPhone = (Panel)e.Item.FindControl("pnlPhone");
        Panel pnlSelect = (Panel)e.Item.FindControl("pnlSelect");

        if (!string.IsNullOrEmpty(address.Address1))
            pnlAddress.Controls.Add(new LiteralControl(address.Address1 + "<br/>"));
        if (!string.IsNullOrEmpty(address.Address2))
            pnlAddress.Controls.Add(new LiteralControl(address.Address2 + "<br/>"));
        if (!string.IsNullOrEmpty(address.Address3))
            pnlAddress.Controls.Add(new LiteralControl(address.Address3 + "<br/>"));

        bool hasCity = false;
        if (!string.IsNullOrEmpty(address.City))
        {
            hasCity = true;
            pnlAddress.Controls.Add(new LiteralControl(address.City));
        }
        bool hasState = false;
        if (!string.IsNullOrEmpty(address.State))
        {
            hasState = true;
            if (hasCity)
                pnlAddress.Controls.Add(new LiteralControl(", "));
            pnlAddress.Controls.Add(new LiteralControl(address.State));
        }
        if (!string.IsNullOrEmpty(address.Zip))
        {
            if (hasCity || hasState)
                pnlAddress.Controls.Add(new LiteralControl(" "));
            pnlAddress.Controls.Add(new LiteralControl(address.Zip));
        }
        if (!string.IsNullOrEmpty(address.Country))
            pnlAddress.Controls.Add(new LiteralControl(address.Country));
        

        if (!string.IsNullOrEmpty(address.Phone))
            pnlPhone.Controls.Add(new LiteralControl("Phone: " + address.Phone + "<br/>"));
        if (!string.IsNullOrEmpty(address.AltPhone))
            pnlPhone.Controls.Add(new LiteralControl("Alt Phone: " + address.AltPhone + "<br/>"));
        if (!string.IsNullOrEmpty(address.Fax))
            pnlPhone.Controls.Add(new LiteralControl("Fax: " + address.Fax + "<br/>"));
        hr.Visible = pnlPhone.Controls.Count > 0;

        if (pnlAddress.Controls.Count == 0 && pnlPhone.Controls.Count == 0)
            return;
        
        _hasAddress = true;
        LinkButton lb = new LinkButton();
        lb.ID = "lbAddress___" + e.Item.ItemIndex;
        lb.SetDataItem(address);
        lb.Text = "Select";
        lb.Click += new EventHandler(lb_Click);
        lb.CausesValidation = false;
        pnlSelect.Controls.Add(lb);
    }

    void lb_Click(object sender, EventArgs e)
    {
        LinkButton lb = (LinkButton)sender;
        Address address = (Address)lb.GetDataItem();

        address.AddressID = _SessionData.MemoData.Address.AddressID;
        if (address.AddressID == null || address.AddressID < 0)
            address.AddressID = 0;

        _SessionData.MemoData.Address = address;
        _MasterPage.Screen.DataSource = _SessionData.MemoData;
        _MasterPage.Screen.DataBind();
        modalAddressForm.Hide();
    }

    protected void lbAddressList_Click(object sender, EventArgs e)
    {
        modalAddressForm.Show();
    }
}
