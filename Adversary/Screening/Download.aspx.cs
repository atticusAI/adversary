﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using HH.Branding;
using HH.Utils;

public partial class Download : ScreeningBasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        _MasterPage.MenuMode = MasterPageMenuMode.None;
        _MasterPage.TabMenuVisible = false;
        _MasterPage.SideBarVisible = false;
        _MasterPage.FooterVisible = false;

        string scrMemId = Request.QueryString["ScrMemID"];
        string fileName = Request.QueryString["FileName"];

        if (string.IsNullOrEmpty(scrMemId) || string.IsNullOrEmpty(fileName))
        {
            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Error;
            _MasterPage.FeedbackMessage = "Could not download file, the URL was invalid";
            return;
        }

        try
        {
            string path = GetAttachmentDirectoryPath(scrMemId) + "\\" + fileName;
            FileUtils.DownloadFile(Response, path);
        }
        catch (Exception ex)
        {
            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Error;
            _MasterPage.FeedbackMessage = "Could not download file. " + ex.Message;
        }
    }
}
