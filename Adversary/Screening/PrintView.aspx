﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PrintView.aspx.cs" Inherits="PrintView" MasterPageFile="~/Master.master" %>
<%@ Register TagPrefix="UC" TagName="ApprTable" Src="~/Screening/_ApprovalTable.ascx" %>

<asp:Content ID="Content" runat="server" ContentPlaceHolderID="UpperContentPlaceHolder">

<style type="text/css">
BODY, TABLE, TR, TD{
	font-size : 9pt;
	font-family : Arial;
}
</style>

<table border="0" >
    <tr>
	    <td style="text-align: center; width: 80%;">
	        <strong>
		        <asp:Label ID="_lblHeader" runat="server"></asp:Label><span style="color: Red;">&nbsp;&nbsp;&nbsp;(Please electronically submit your memo using the submit link)</span><br />
	        </strong>
	    </td>
    </tr>
    <tr>
	    <td style="text-align:right; width: 80%;">
	        <a href="javascript:this.close();">Close</a>
	    </td>
    </tr>
    <tr>
	    <td style="width: 80%;" >
	        <UC:ApprTable ID="_apprTable" runat="server"></UC:ApprTable>
	    </td>
    </tr>
    <tr>
        <td style="width: 80%;" >
            <br />
            <asp:Panel ID="_pnlSummary" runat="server"></asp:Panel>
        </td>
    </tr>
</table>


</asp:Content>
