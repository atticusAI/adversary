﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Default.aspx.cs" Inherits="Default" MasterPageFile="~/Master.master" %>

<asp:Content ID="Content" ContentPlaceHolderID="UpperContentPlaceHolder" runat="server">


<br />
<br />
<dl>
    <dt><strong><a href="<%=Request.ApplicationPath%>/Screening/Default.aspx">Create Screening Memo</a></strong></dt>
    <dt><strong><a href="<%=Request.ApplicationPath%>/Screening/Search.aspx">Search Screening Memos</a></strong></dt>
    <%-- blmcgee@hollandhart.com 11/4/2013 remove all Adversary Request links --%>
	<%--
	<dt><strong><a href="<%=Request.ApplicationPath%>/Request/Default.aspx">Adversary Requests</a></strong></dt>
	--%>
    <dt id="dtApproval" runat="server" visible="false"><strong><a href="<%=Request.ApplicationPath%>/Admin/Queue.aspx">View Your Approval Queue</a></strong></dt>
    <dt><strong><a href="?logout=true&type=Default">Logout of New Business Intake</a></strong></dt>
</dl>
<br />
<br />



    
</asp:Content>