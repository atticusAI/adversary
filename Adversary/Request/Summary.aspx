﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Summary.aspx.cs" Inherits="Summary" MasterPageFile="~/Master.master" %>
<%@ Register TagPrefix="UC" TagName="ReportBuilder" Src="~/_usercontrols/ReportBuilder.ascx" %>
<%@ Register TagPrefix="UC" TagName="RequestSummary" Src="~/_usercontrols/RequestSummary.ascx" %>

<asp:Content ID="Content" ContentPlaceHolderID="UpperContentPlaceHolder" runat="server">

    <%--using the new request summary control  --%>
   <%-- <UC:ReportBuilder ID="_results" runat="server" CssClass="gridView"></UC:ReportBuilder>--%>
    <UC:RequestSummary ID="_requestSummary" runat="server" CssClass="gridView" />

</asp:Content>


