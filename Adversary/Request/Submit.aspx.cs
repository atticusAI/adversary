﻿using System;
using System.Linq;
using HH.Branding;
using HH.SQL;
using System.Data.SqlClient;

public partial class Submit : AdversaryRequestBasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        _MasterPage.NextButtonEnabled = false;
        lbSubmit.Enabled = _SessionData.AdversaryRequest.CanSubmit();

        //a reimer 08/11/11 - removed summaryView control and added the new requestSUmmary
        //_summaryView.RowCssClass = "gridViewRow";
        //_summaryView.ShowNullValues = true;
        //_summaryView.ScreenID = "EditAdversaryRequest.aspx";
        //_summaryView.DataSource = _SessionData.AdversaryRequest;
        //_summaryView.DataBind();
        _requestSummary.RowCssClass = "gridViewRow";
        _requestSummary.ShowNullValues = true;
        _requestSummary.ScreenID = "EditAdversaryRequest.aspx";
        _requestSummary.DataSource = _SessionData.AdversaryRequest;
        _requestSummary.DataBind();




    }

    protected void lbSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            RequestParties[] parties = _SessionData.AdversaryRequest.RequestParties.ToArray();
            _SessionData.AdversaryRequest.SubmittalDate = DateTime.Now;
            using (SqlConnection conn = AdversaryData.GetConnection())
            {
                DatabaseWorker worker = new DatabaseWorker(conn);
                DoDataSave(_SessionData.AdversaryRequest, worker);

                foreach (RequestParties party in _SessionData.AdversaryRequest.RequestParties)
                {
                    party.ReqID = _SessionData.AdversaryRequest.ReqID;
                    DoDataSave(party, worker);
                }
            }

            SendAdversaryRequestEmail(_SessionData.AdversaryRequest);

            _MasterPage.FeedbackMessage = (!string.IsNullOrEmpty(_SessionData.AdversaryRequest.WorkBegunBy)) ? "Your Adversary Request Has Been Submitted" : "Your Adversary Request Has Been Updated";
            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Success;
            lbSubmit.Enabled = false;
        }
        catch (Exception ex)
        {
            HandleError(ex);
        }
    }
}
