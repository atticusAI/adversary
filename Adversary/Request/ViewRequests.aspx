﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="ViewRequests.aspx.cs" Inherits="ViewRequests" MasterPageFile="~/Master.master" EnableEventValidation="false" %>

<asp:Content ID="Content" ContentPlaceHolderID="UpperContentPlaceHolder" runat="server">

<div align="right" style="text-align: right;">
    <table align="right" style="text-align: left;">
        <tr>
            <td>Show All Requests Submitted After:</td>
            <td>
                <asp:TextBox ID="tbMinDate" runat="server" CssClass="sbControl" Width="200px"></asp:TextBox>&nbsp;&nbsp;
                <asp:Button ID="btnMinDate" runat="server" Text="Go" OnClick="btnMinDate_Click" />
            </td>
        </tr>
        <tr>
            <td>Show All Requests That Are:</td>
            <td>
                <asp:DropDownList ID="ddlShow" runat="server" AutoPostBack="true" CssClass="sbControl" Width="200px" OnSelectedIndexChanged="ddlShow_IndexChanged">
                    <asp:ListItem Text="Pending" Value="pending"></asp:ListItem>
                    <asp:ListItem Text="Pending & Completed" Value="both"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
    </table>
</div>
<asp:UpdatePanel ID="upRequests" runat="server">

    <ContentTemplate>
        <div style="clear: both;">      
            <br />
            <asp:GridView ID="gvRequests" 
                runat="server" 
                AutoGenerateColumns="false"
                DataKeyNames="ReqID"
                GridLines="Vertical"
                BorderStyle="solid" 
                BorderWidth="1"
                BorderColor="#F0F0F0"
                RowStyle-BackColor="#F0F0F0" 
                AlternatingRowStyle-BackColor="White"
                AllowPaging="true" 
                PagerSettings-Mode="NumericFirstLast" 
                PagerSettings-Position="Bottom" 
                PageSize="10" 
                PagerStyle-BorderStyle="solid" 
                PagerStyle-BorderWidth="1"
                PagerStyle-BorderColor="#F0F0F0"
                PagerStyle-BackColor="White"
                PagerSettings-FirstPageText="First" 
                PagerSettings-PreviousPageText="Previous"
                PagerSettings-NextPageText="Next"
                PagerSettings-LastPageText="Last" 
                PagerSettings-PageButtonCount="10"
                CellPadding="0"
                CellSpacing="2">
                <Columns>
                    <asp:TemplateField HeaderText="&nbsp;" ItemStyle-Wrap="false" /> 
                    <asp:BoundField DataField="ReqID" HeaderText="Request #" SortExpression="ReqID" ItemStyle-CssClass="gridViewItem"/>
                    <asp:TemplateField HeaderText="Assigned To" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Panel ID="pnlAssigned" runat="server" HorizontalAlign="Left"></asp:Panel>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="ClientName" HeaderText="Client" SortExpression="ClientName" />
                    <asp:BoundField DataField="ClientMatter"  HeaderText="Client Matter" SortExpression="ClientMatter" />
                    <asp:BoundField DataField="PracticeCode" HeaderText="Code" SortExpression="PracticeCode" />
                    <asp:BoundField DataField="AttyLogin" HeaderText="From" SortExpression="AttyLogin" />
                    <asp:BoundField DataField="TypistLogin" HeaderText="Typist" SortExpression="Typist" />
                    <asp:BoundField DataField="SubmittalDate" HeaderText="Submitted On" SortExpression="SubmittalDate" />
                    <asp:BoundField DataField="DeadlineDate" HeaderText="Deadline" SortExpression="DeadlineDate" Visible="false" />
                    <asp:TemplateField HeaderText="Completed" ItemStyle-Wrap="false" /> 
                </Columns>

            </asp:GridView>
            <asp:Label ID="lblCount" runat="server"></asp:Label>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="gvRequests" EventName="RowCommand" />
        <asp:AsyncPostBackTrigger ControlID="btnMinDate" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="ddlShow" EventName="SelectedIndexChanged" />
    </Triggers>
</asp:UpdatePanel>

</asp:Content>