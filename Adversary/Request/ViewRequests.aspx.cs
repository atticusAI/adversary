﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using HH.Branding;
using HH.Extensions;

using HH.SQL;

public partial class ViewRequests : AdversaryRequestBasePage
{
    private class ReqComboBox : AjaxControlToolkit.ComboBox
    {
        private ViewRequests _parent;
        
        public ReqComboBox(Requests req, ViewRequests parent)
        {
            this.SetDataItem(req);
            _parent = parent;

            this.ID = "combo___" + req.ReqID;
            this.CssClass = "CBAquaStyle";
            this.AutoPostBack = true;
            this.AutoCompleteMode = AjaxControlToolkit.ComboBoxAutoCompleteMode.Suggest;
            this.CaseSensitive = false;
            this.DropDownStyle = AjaxControlToolkit.ComboBoxStyle.DropDownList;
            this.DataTextField = "Login";
            this.DataValueField = "Login";
            this.DataSource = AdminLogins.GetAdversaryGroup();
            this.DataBind();
            this.Items.Insert(0, new ListItem("(Unassigned)", "unassigned"));
            this.SelectedIndexChanged += new EventHandler(combo_SelectedIndexChanged);
            try
            {
                if (!string.IsNullOrEmpty(req.WorkBegunBy))
                    this.SelectedValue = req.WorkBegunBy;
                else
                {
                    //maybe this will work
                    this.ForeColor = Color.Red;
                    this.SelectedIndex = 0;
                }
            }
            catch
            {
                this.SelectedIndex = 0;
            }
        }

        private void combo_SelectedIndexChanged(object sender, EventArgs e)
        {
            Requests req = (Requests)this.GetDataItem();
            if (this.SelectedValue == "unassigned")
            {
                req.WorkBegunBy = null;
                req.WorkBegunDate = null;
            }
            else
            {
                req.WorkBegunBy = this.SelectedValue;
                if (req.WorkBegunDate == null)
                    req.WorkBegunDate = DateTime.Now;
            }

            _parent.DoDataSave(req);
            _parent.BindRequestList();
            _parent._MasterPage.FeedbackMessage = "Request #" + req.ReqID + " assigned to " + ((req.WorkBegunBy == null) ? "none" : req.WorkBegunBy);
            _parent._MasterPage.FeedbackMode = MasterPageFeedbackMode.Success;
        }

    }

    List<Requests> _requests;

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!_SessionData.AdversaryRequestLoaded)
        //{
        //    try
        //    {
        //        //disable menu items
        //        _MasterPage.Tabs[4].Items[2].Enabled = false;
        //        _MasterPage.Tabs[4].Items[3].Enabled = false;
        //    }
        //    catch { }
        //}

        gvRequests.RowDataBound += new GridViewRowEventHandler(gvRequests_RowDataBound);
        gvRequests.PageIndexChanging += new GridViewPageEventHandler(gvRequests_PageIndexChanging);
        BindRequestList();
    }

    void gvRequests_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvRequests.PageIndex = e.NewPageIndex;
        gvRequests.DataBind();
    }

    private void BindRequestList()
    {
        if (!IsPostBack)
            tbMinDate.Text = DateTime.Now.AddDays(-7).ToShortDateString();

        List<SqlParameter> parameters = new List<SqlParameter>();
        parameters.Add(new SqlParameter("PendingOnly", ddlShow.SelectedValue == "pending"));

        try
        {
            parameters.Add(new SqlParameter("SubmittalDate", DateTime.Parse(tbMinDate.Text.Trim())));
        }
        catch
        {
            parameters.Add(new SqlParameter("SubmittalDate", DateTime.Now.AddMonths(-2).ToShortDateString()));
        }

        if (!string.IsNullOrEmpty(Request.QueryString["type"]))
        {
            if(Request.QueryString["type"].ToLower() == "my")
                parameters.Add(new SqlParameter("WorkBegunBy", _SessionData.LoginEmpName));
            else if (Request.QueryString["type"].ToLower() == "submitted")
                parameters.Add(new SqlParameter("TypistLogin", _SessionData.LoginEmpCode));
        }

        using (SqlConnection conn = AdversaryData.GetConnection())
        {
            DatabaseWorker worker = new DatabaseWorker(conn);
            _requests = worker.RetrieveToList<Requests>("PR_GET_REQUESTS", parameters.ToArray());
        }
        gvRequests.DataSource = _requests;
        gvRequests.DataBind();

        if (_requests.Count == 0)
        {
            _MasterPage.FeedbackMessage = "No requests found at this time";
            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Success;
        }
        else
        {
            //remove the completed row if just showing pending requests
            gvRequests.Columns[8].Visible = (ddlShow.SelectedValue == "pending");
            lblCount.Text = "Total Requests Found: " + _requests.Count;
        
        }

        
    }

    private void gvRequests_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.DataRow)
            return;

        Requests req = (Requests)e.Row.DataItem;
        bool isAdversary = (Request.QueryString["type"] == "all") && 
                           (_SessionData.AdminLogin.IsAdversary.HasValue && _SessionData.AdminLogin.IsAdversary.Value);
        LinkButton lb;

        lb = new LinkButton();
        lb.ID = "lbSummary___" + req.ReqID;
        lb.SetDataItem(req);
        lb.Text = "Summary";
        lb.CommandName = "Summary";
        lb.Click += new EventHandler(lb_Click);
        e.Row.Cells[0].Controls.Add(lb);

        //a reimer 08/09/11 added if to eliminate user's ability to edit requests that
        //have been assigned -- allow administrators to edit any request any time.
        if ((isAdversary || ((bool)_SessionData.AdminLogin.IsAdversaryAdmin.HasValue && (bool)_SessionData.AdminLogin.IsAdversaryAdmin.Equals(true))) 
            || !String.IsNullOrEmpty(req.WorkBegunBy))
        {
            lb = new LinkButton();
            lb.ID = "lbEditReq___" + req.ReqID;
            lb.SetDataItem(req);
            lb.Text = "Edit";
            lb.CommandName = "EditReq";
            lb.Click += new EventHandler(lb_Click);
            e.Row.Cells[0].Controls.Add(new LiteralControl("<br/>"));
            e.Row.Cells[0].Controls.Add(lb);
        }

        if (isAdversary && string.IsNullOrEmpty(req.WorkBegunBy))
        {
            lb = new LinkButton();
            lb.ID = "lbTakeReq___" + req.ReqID;
            lb.SetDataItem(req);
            lb.Text = "Assign to Me";
            lb.CommandName = "AssignReq";
            lb.Click += new EventHandler(lb_Click);
            e.Row.Cells[0].Controls.Add(new LiteralControl("<br/>"));
            e.Row.Cells[0].Controls.Add(lb);
        }

        if (isAdversary)
            e.Row.Cells[2].FindControl("pnlAssigned").Controls.Add(new ReqComboBox(req, this));
        else
            e.Row.Cells[2].FindControl("pnlAssigned").Controls.Add(new LiteralControl(req.WorkBegunBy));

        //a reimer 08/10/11 commented out to remove deadline date column
        //if (req.DeadlineDate <= DateTime.Now && !req.IsComplete)
        //{
        //    e.Row.Cells[9].ForeColor = Color.Red;
        //    e.Row.Cells[9].Font.Bold = true;
        //}

        //a reimer 08/10/11 added to turn cell red where the request is "unassigned"
        //if (req.WorkBegunBy == null)
        //{
        //    e.Row.Cells[2].BackColor = Color.FromArgb(200, 60, 80);
        //}

        if ((ddlShow.SelectedValue == "both") && (req.CompletionDate.HasValue || !string.IsNullOrEmpty(req.WorkCompletedBy)))
            e.Row.Cells[10].Text = req.CompletionDate + "<br/>By: " + ((string.IsNullOrEmpty(req.WorkCompletedBy)) ? "(unknown)" : req.WorkCompletedBy);
    }


    protected void lb_Click(object sender, EventArgs e)
    {
        LinkButton lb = (LinkButton)sender;
        Requests req = (Requests)lb.GetDataItem();
        _SessionData.AdversaryRequest = req;
        _SessionData.AdversaryRequestLoaded = true;

        if (_SessionData.AdminLogin.IsAdversary.HasValue && _SessionData.AdminLogin.IsAdversary.Value &&
            (Request.QueryString["type"] == "all" || Request.QueryString["type"] == "my"))
        {
            switch (lb.CommandName)
            {
                case "Summary":
                    Response.Redirect("Summary.aspx?type=admin");
                    break;
                case "EditReq":
                    Response.Redirect("~/Admin/EditAdversaryRequest.aspx");
                    break;
                case "AssignReq":
                    req.WorkBegunBy = _SessionData.AdminLogin.Login;
                    if (req.WorkBegunDate == null)
                        req.WorkBegunDate = DateTime.Now;
                    DoDataSave(req);
                    Response.Redirect("ViewRequests.aspx?type=my");
                    break;
            }
        }
        else
        {
            switch (lb.CommandName)
            {
                case "Summary":
                    Response.Redirect("Summary.aspx?type=submitted");
                    break;
                case "EditReq":
                    Response.Redirect("BasicInfo.aspx");
                    break;
            }
        }
    }


    protected void btnMinDate_Click(object sender, EventArgs e)
    {
        DateTime minVal;

        try
        {
            minVal = DateTime.Parse(tbMinDate.Text.Trim());
        }
        catch 
        {
            _MasterPage.FeedbackMessage = "The date you entered was invalid";
            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Error;
            minVal = DateTime.Now.AddMonths(-1);
        }

        tbMinDate.Text = minVal.ToShortDateString();
        BindRequestList();
    }

    protected void ddlShow_IndexChanged(object sender, EventArgs e)
    {
        BindRequestList();
    }

    private Requests GetRequest(string reqId)
    {
        foreach (Requests req in _requests)
        {
            if (req.ReqID.ToString() == reqId)
                return req;
        }
        return null;
    }
}
