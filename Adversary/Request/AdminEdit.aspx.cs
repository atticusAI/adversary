﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using HH.Branding;
using HH.SQL;
using System.Data.SqlClient;

public partial class AdminEdit : AdversaryRequestBasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        _MasterPage.FooterNavVisible = false;

        if (_SessionData.AdversaryRequest.ReqID == null ||
            _SessionData.AdversaryRequest.ReqID == 0)
        {
            _MasterPage.FeedbackMessage = "An error occurred, a request was no selected for edit. " +
                                          "Please <a href=\"ViewRequests.aspx?type=all\">Go Back</a> and select a " +
                                          "request to edit";
            _MasterPage.FeedbackMode = MasterPageFeedbackMode.Warning;
            return;
        }

        Button btn = new Button();
        btn.Text = "Update Request";
        btn.Click += new EventHandler(btn_Click);
        _MasterPage.FooterControls.Add(btn);
        _MasterPage.Screen.ScreenID = "EditAdversaryRequest.aspx";
        _MasterPage.Screen.DataSource = _SessionData.AdversaryRequest;
        _MasterPage.Screen.DataBind();
    }

    private void btn_Click(object sender, EventArgs e)
    {
        _MasterPage.FeedbackMode = MasterPageFeedbackMode.Success;
        _MasterPage.FeedbackMessage = "Request updated.";
        if (_SessionData.AdversaryRequest.IsComplete)
            _MasterPage.FeedbackMessage += " Marked as completed by " + _SessionData.AdversaryRequest.WorkCompletedBy + ".";
        else if (!string.IsNullOrEmpty(_SessionData.AdversaryRequest.WorkBegunBy))
            _MasterPage.FeedbackMessage += " Assigned to " + _SessionData.AdversaryRequest.WorkBegunBy + ".";
        
        using (SqlConnection conn = AdversaryData.GetConnection())
        {
            DatabaseWorker worker = new DatabaseWorker(conn);
            foreach (RequestParties party in _SessionData.AdversaryRequest.RequestParties)
                DoDataSave(party, worker);

            DoDataSave(_SessionData.AdversaryRequest, worker);
        }
    }
}
