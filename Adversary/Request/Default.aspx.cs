﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Default : AdversaryRequestBasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    protected void lbNew_Click(object sender, EventArgs e)
    {
        LinkButton lb = (LinkButton)sender;
        Requests request = new Requests();
        _SessionData.AdversaryRequest = request;
        _SessionData.AdversaryRequestLoaded = true;
        Response.Redirect("~/Request/BasicInfo.aspx");
    }
}
