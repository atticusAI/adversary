﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using HH.Screen;
using HH.Branding;
using HH.UI.ModalPopupForm;
using HH.SQL;
using System.Data.SqlClient;

public partial class AdditionalInfo : AdversaryRequestBasePage
{
    CheckBox _cbNone;

    protected void Page_Load(object sender, EventArgs e)
    {
        _MasterPage.Screen.ScreenID = "AdditionalInfo.aspx";
        _MasterPage.Screen.Width = Unit.Percentage(90);
        _MasterPage.Screen.DataSource = _SessionData.AdversaryRequest;
        _MasterPage.Screen.DataBind();

        _MasterPage.PopupYesClick += new EventHandler(confirm_Click);
        _MasterPage.PopupNoClick += new EventHandler(_MasterPage_PopupNoClick);
        _MasterPage.PostBackEvent += new MasterPagePostBackEventHandler(MasterPage_PostBackEvent);

        _cbNone = (CheckBox)_MasterPage.Screen.Form.FindControl("NoneOfTheAbove");
        _cbNone.AutoPostBack = true;
        //_cbNone.CheckedChanged += new EventHandler(this.cb_CheckedChanged);
        //AsyncPostBackTrigger trigger = new AsyncPostBackTrigger();
        //trigger.ControlID = _cbNone.ID;
        //trigger.EventName = "CheckedChanged";
        //_MasterPage.Screen.Triggers.Add(trigger);


        ClearCheckBoxes(!_cbNone.Checked);
        _SessionData.AdversaryRequest.NoneOfTheAbove = _cbNone.Checked;

        if (_cbNone.Checked)
            ShowPopup();
    }

   

    private bool MasterPage_PostBackEvent(object sender, MasterPagePostBackEventArgs e)
    {
        using (SqlConnection conn = AdversaryData.GetConnection())
        {
            DatabaseWorker worker = new DatabaseWorker(conn);
            if (HasAdditionalCriteria())
            {
                _SessionData.AdversaryRequest = (Requests)_MasterPage.Screen.DataSource;
                DoDataSave(_SessionData.AdversaryRequest, worker);
                return true;
            }
            if (_cbNone.Checked)
            {
                _SessionData.AdversaryRequest = (Requests)_MasterPage.Screen.DataSource;
                DoDataSave(_SessionData.AdversaryRequest, worker);
                return true;
            }
        }

        _MasterPage.FeedbackMessage = "You have not checked any boxes. If the client or adverse party does not fit any of the criteria listed please select 'None of the above'.";
        _MasterPage.FeedbackMode = MasterPageFeedbackMode.Error;
        return !e.RedirectUrl.Contains("AdditionalInfo.aspx") && !e.RedirectUrl.Contains("Submit.aspx");
    }

    private bool HasAdditionalCriteria()
    {
        foreach (TableRow row in _MasterPage.Screen.Form.Rows)
        {
            foreach (TableCell td in row.Cells)
            {
                foreach (Control c in td.Controls)
                {
                    if (c is CheckBox)
                    {
                        CheckBox cb = (CheckBox)c;
                        if (cb.Checked)
                            return true;
                    }
                }
            }
        }

        return false;
    }


    protected void cb_CheckedChanged(object sender, EventArgs e)
    {
        ClearCheckBoxes(!_cbNone.Checked);
        _SessionData.AdversaryRequest.NoneOfTheAbove = _cbNone.Checked;

        if (!_cbNone.Checked)
            return;

        ShowPopup();
    }

    void confirm_Click(object sender, EventArgs e)
    {
        AssignCheckBoxes(true);
    }

    void _MasterPage_PopupNoClick(object sender, EventArgs e)
    {
        AssignCheckBoxes(false);
    }

    private void AssignCheckBoxes(bool isNoneOfTheAboveChecked)
    {
        foreach (TableRow row in _MasterPage.Screen.Form.Rows)
        {
            foreach (TableCell td in row.Cells)
            {
                foreach (Control c in td.Controls)
                {
                    if (c is CheckBox)
                    {
                        CheckBox cb = (CheckBox)c;
                        if (isNoneOfTheAboveChecked)
                        {
                            if (!c.ID.Equals("NoneOfTheAbove"))
                            {
                                cb.Checked = !isNoneOfTheAboveChecked;
                                cb.Enabled = !isNoneOfTheAboveChecked;
                            }
                            else
                            {
                                cb.Checked = isNoneOfTheAboveChecked;
                            }
                        }
                        else
                        {
                            if (c.ID.Equals("NoneOfTheAbove"))
                            {
                                cb.Checked = false;
                            }
                            else
                            {
                                cb.Enabled = true;
                            }
                        }
                    }
                }
            }
        }
    
    }

    private void ClearCheckBoxes(bool isEnabled)
    {
        foreach (TableRow row in _MasterPage.Screen.Form.Rows)
        {
            foreach (TableCell td in row.Cells)
            {
                foreach (Control c in td.Controls)
                {
                    if (c is CheckBox && c.ID != "NoneOfTheAbove")
                    {
                        CheckBox cb = (CheckBox)c;
                        if (!isEnabled)
                            cb.Checked = false;
                        cb.Enabled = isEnabled;
                        break;
                    }
                }
            }
        }
    }
    private void ClearCheckBoxes(bool isEnabled, bool confirmClickYes)
    {
        foreach (TableRow row in _MasterPage.Screen.Form.Rows)
        {
            foreach (TableCell td in row.Cells)
            {
                foreach (Control c in td.Controls)
                {
                    if (c is CheckBox)
                    {
                        CheckBox cb = (CheckBox)c;
                        if (c.ID == "NoneOfTheAbove")
                        {
                            cb.Checked = true;
                            cb.Enabled = true;
                        }
                        else
                        {
                            cb.Checked = false;
                            cb.Enabled = isEnabled;
                        }
                        break;
                    }
                }
            }
        }
    }

    private void ShowPopup()
    {
        if(this.IsPostBack)
        _MasterPage.ShowPopupDialog("Are you sure that the client or adverse party does not fit any of the criteria listed above?", MasterPagePopupDialogMode.ModalYesNo);
    }
}
