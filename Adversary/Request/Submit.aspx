﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Submit.aspx.cs" Inherits="Submit" MasterPageFile="~/Master.master" %>
<%@ Register TagPrefix="UC" TagName="SummaryView" Src="~/_usercontrols/ReportBuilder.ascx" %>
<%@ Register TagPrefix="UC" TagName="RequestSummary" Src="~/_usercontrols/RequestSummary.ascx" %>
<asp:Content ID="Content" ContentPlaceHolderID="UpperContentPlaceHolder" runat="server">
    
    <%--<UC:SummaryView ID="_summaryView" CssClass="gridView" runat="server" />--%>
    <UC:RequestSummary ID="_requestSummary" CssClass="gridView" runat="server" />
    <br />
    <%--<asp:LinkButton ID="lbSubmit" runat="server" Text="Submit this Adversary Request" Font-Bold="true" OnClick="lbSubmit_Click" />--%>
    <div align="right"><asp:Button ID="lbSubmit" runat="server" Text="Submit this Adversary Request" OnClick="lbSubmit_Click" /></div>

</asp:Content>