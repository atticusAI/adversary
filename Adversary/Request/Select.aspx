﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Select.aspx.cs" Inherits="Select" MasterPageFile="~/Master.master" %>

<asp:Content ID="Content" ContentPlaceHolderID="UpperContentPlaceHolder" runat="server">

    <asp:PlaceHolder ID="phGV" runat="server"></asp:PlaceHolder>
        <asp:GridView ID="_gvRequests" runat="server" Width="75%" AutoGenerateColumns="false"
                      GridLines="Both"
                      CssClass="gridView"
                      EmptyDataText="No data available"
                      OnRowCreated="gvRequests_RowCreated" >
            <HeaderStyle CssClass="gridViewHeader" />
            <EmptyDataRowStyle ForeColor="Red" />
            <Columns>
                <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                    <ItemTemplate>
                        <a id="_aView" runat="server" style="font-weight: bold;">Select</a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="ReqID" HeaderText="Request ID" />
                <asp:BoundField DataField="ClientName" HeaderText="Client" SortExpression="ClientName" />
                <asp:BoundField DataField="AttyLogin" HeaderText="From" />
                <asp:BoundField DataField="WorkBegunBy" HeaderText="Assigned" />
            </Columns>
        </asp:GridView>

</asp:Content>


