﻿using System;
using System.Data;
using System.Web.UI.WebControls;

using HH.SQL;
using HH.UI;
using System.Data.SqlClient;

public partial class Select : AdversaryRequestBasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        _MasterPage.FooterNavVisible = false;

        Requests request = new Requests();
        request.TypistLogin = _SessionData.LoginEmpCode;
        DataTable reqs;
        using (SqlConnection conn = AdversaryData.GetConnection())
        {
            DatabaseWorker worker = new DatabaseWorker(conn);
            reqs = worker.SearchToDataTable(request);
        }
        _gvRequests.DataSource = reqs;
        _gvRequests.DataBind();
    }

    protected void gvRequests_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType != DataControlRowType.DataRow)
            return;
        string url = "Summary.aspx?ReqID=" + ((System.Data.DataRowView)e.Row.DataItem)["ReqID"];
        e.Row.Attributes.Add("onmouseover", "this.className='gridViewRowHover';");
        e.Row.Attributes.Add("onmouseout", "this.className='gridViewRow';");
        e.Row.Attributes.Add("onclick", "window.location.href='" + url + "';");
    }

        
}
