﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Default.aspx.cs" Inherits="Default" MasterPageFile="~/Master.master" %>

<asp:Content ID="Content" ContentPlaceHolderID="UpperContentPlaceHolder" runat="server">

<h4>Please choose an option below:</h4>
<dl>
    <dt><strong><asp:LinkButton ID="lbNewType" runat="server" Text="Start a New Adversary Request" OnClick="lbNew_Click" CommandArgument="1"></asp:LinkButton></strong></dt>
    <dt><strong><a href="ViewRequests.aspx?type=submitted">Select an existing Adversary Request</a></strong></dt>
</dl>

</asp:Content>