﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using HH.UI.AutoComplete;

public partial class BasicInfo : AdversaryRequestBasePage
{
    private DropDownList ddlClientType;
    private TextBox tbClientMatterNumber;



    protected void Page_Init(object sender, EventArgs e)
    {
 	     if (_SessionData.AdversaryRequest.ReqID == null)// ||
             //_SessionData.AdversaryRequest.ReqID == 0)
         {
             _MasterPage.PreviousButtonEnabled = false;
             _SessionData.AdversaryRequest.ReqID = 0;
             _SessionData.AdversaryRequest.TypistLogin = _SessionData.LoginEmpCode;
             //_SessionData.AdversaryRequest.SubmittalDate = DateTime.Now;
             _SessionData.AdversaryRequest.DeadlineDate = DateTime.Now.AddDays(1);
         }
         _MasterPage.Screen.ScreenID = "BasicInfo.aspx";
         _MasterPage.Screen.DataSource = _SessionData.AdversaryRequest;
         _MasterPage.Screen.DataBind();

         AutoCompleteField sbTypistLogin = BuildEmployeeSearchAutoComplete("sbTypistLogin", "TypistLogin", _SessionData.AdversaryRequest.TypistLogin);
         sbTypistLogin.SearchRequest += new AutoCompleteSearchRequest(searchBox_OnSearchRequest);
         sbTypistLogin.Clear += new AutoCompleteFieldCleared(searchBox_OnClear);
         sbTypistLogin.ContextKey = "TypistLogin";
         _MasterPage.Screen.AddControlToFormAt(4, sbTypistLogin, "Submitted By Payroll ID", "Start typing an employee name or number to search");
        
         AutoCompleteField sbAttyLogin = BuildEmployeeSearchAutoComplete("sbAttyLogin", "AttyLogin", _SessionData.AdversaryRequest.AttyLogin);
         sbAttyLogin.SearchRequest += new AutoCompleteSearchRequest(searchBox_OnSearchRequest);
         sbAttyLogin.Clear += new AutoCompleteFieldCleared(searchBox_OnClear);
         sbAttyLogin.ContextKey = "AttyLogin";
         _MasterPage.Screen.AddControlToFormAt(5, sbAttyLogin, "Attorney Payroll ID", "Start typing an employee name or number to search");
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    protected override void Render(HtmlTextWriter writer)
    {
        this.ddlClientType = new DropDownList();
        this.tbClientMatterNumber = new TextBox();
        //added this routine in render to eliminate 101X from the practice type dropdown as well as add the "Please choose..." listitem.
        foreach (TableRow row in _MasterPage.Screen.Form.Rows)
        {
            foreach (TableCell td in row.Cells)
            {
                foreach (Control c in td.Controls)
                {
                    if (c is DropDownList)
                    {
                        if (c.ID == "PracticeCode")
                        {
                            DropDownList D = (DropDownList)c;
                            ListItem _item101X = D.Items.FindByValue("101X");
                            D.Items.Remove(_item101X);
                            ListItem _itemBlank = new ListItem("Please choose a practice code...", String.Empty);
                            D.Items.Insert(0, _itemBlank);
                        }
                        else if(c.ID == "ClientCategory")
                        {
                            this.ddlClientType = (DropDownList)c;
                            //ddlClientType.Items.Insert(0, new ListItem("Please choose Existing or Potential client...", String.Empty));
                            if (_SessionData.AdversaryRequest != null)
                            {
                                if (_SessionData.AdversaryRequest.ClientCategory != null && ddlClientType.Items.Count > 0)
                                {
                                    ddlClientType.Items.FindByValue(_SessionData.AdversaryRequest.ClientCategory.ToString()).Selected = true;
                                }
                            }
                        }
                    }
                    else if(c is TextBox)
                    {
                        if(c.ID == "ClientMatter")
                        {
                            this.tbClientMatterNumber = (TextBox)c;
                        }
                    }
                }
            }
        }

        //a reimer 08/11/11 - added for request to disallow entry if the client matter is potential, but allow if the client is existing.
        
        //build javascript string;
        StringBuilder sbJavascript;
        if (this.ddlClientType.ClientID != null)
        {
            if (this.ddlClientType.ClientID.Length > 0 && tbClientMatterNumber.ClientID.Length > 0)
            {
                //start the client matter disabled
                if (_SessionData.AdversaryRequest.ClientCategory == "Potential" || String.IsNullOrEmpty(_SessionData.AdversaryRequest.ClientCategory))
                {
                    tbClientMatterNumber.Enabled = false;
                    tbClientMatterNumber.BackColor = System.Drawing.Color.FromKnownColor(System.Drawing.KnownColor.AliceBlue);
                }
                else
                {
                    tbClientMatterNumber.Enabled = true;
                    tbClientMatterNumber.BackColor = System.Drawing.Color.Transparent;
                }
                

                //add event
                this.ddlClientType.Attributes.Add("OnChange", "javascript:clientMatterBox();");

                //create javascript
                sbJavascript = new StringBuilder("<script language=\"javascript\" type=\"text/javascript\">");
                sbJavascript.Append("function clientMatterBox(){"); //start function
                sbJavascript.Append("var clientSelect = $get('" + this.ddlClientType.ClientID + "');");
                sbJavascript.Append("var clientMatter = $get('" + this.tbClientMatterNumber.ClientID + "');");
                sbJavascript.Append("var clientSelectValue = clientSelect.value;");
                sbJavascript.Append("if(clientSelectValue=='Potential'){");
                sbJavascript.Append("clientMatter.value='';");
                sbJavascript.Append("clientMatter.disabled=true;");
                sbJavascript.Append("clientMatter.style.background=\"#F0F8FF\";");
                sbJavascript.Append("}");
                sbJavascript.Append("else{");
                sbJavascript.Append("clientMatter.disabled=false;");
                //sbJavascript.Append("clientMatter.style.backgroundColor=\"#FFFFFF\";");
                sbJavascript.Append("clientMatter.style.backgroundColor=\"transparent\";");
                sbJavascript.Append("}");
                sbJavascript.Append("}"); //end function
                sbJavascript.Append("</script>");

                //register script
                ClientScript.RegisterClientScriptBlock(typeof(String), "clientMatterBox", sbJavascript.ToString());
            }
        }
        
        
        base.Render(writer);
    }

    AutoCompleteFieldMode searchBox_OnSearchRequest(IAutoCompleteField sender, string searchText)
    {
        AutoCompleteFieldMode mode = AutoCompleteFieldMode.ShowResults;
        if (!ValidEmployeeSelection(searchText))
            mode = AutoCompleteFieldMode.ShowSearchBox;

        string val = ParseSearchRequestTextForNumber(searchText);
        if (sender.ContextKey == "TypistLogin")
            _SessionData.AdversaryRequest.TypistLogin = val;
        else
            _SessionData.AdversaryRequest.AttyLogin = val;

        return mode;
    }

    AutoCompleteFieldMode searchBox_OnClear(IAutoCompleteField sender)
    {
        if (sender.ContextKey == "TypistLogin")
            _SessionData.AdversaryRequest.TypistLogin = null;
        else
            _SessionData.AdversaryRequest.AttyLogin = null;
        return AutoCompleteFieldMode.ShowSearchBox;
    }
}
