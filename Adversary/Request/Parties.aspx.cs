﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using HH.Screen;

public partial class Parties : AdversaryRequestBasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        _MasterPage.Screen.ScreenID = "Parties.aspx";
        _MasterPage.Screen.DataSource = _SessionData.AdversaryRequest;
        _MasterPage.Screen.DataBind();
    }
}
