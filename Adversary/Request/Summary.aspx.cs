﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Reflection;
using System.Data.Linq.Mapping;
using HH.Screen;

public partial class Summary : AdversaryRequestBasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        _MasterPage.PreviousButtonEnabled = false;
        string reqId = Request.QueryString["ReqID"];
        if (!string.IsNullOrEmpty(reqId) && !IsPostBack)
        {
            Requests request = new Requests();
            request.ReqID = Convert.ToInt32(reqId);
            DoDataLoad(request);

        }

        //ScreenControlBuilder.PopulateReport("EditAdversaryRequest.aspx", _SessionData.AdversaryRequest, ref tableResults, "gridViewRow", true, true);

        //commented out the _results control to use the new request summary
        //_results.RowCssClass = "gridViewRow";
        //_results.ShowNullValues = true;
        //_results.ScreenID = "EditAdversaryRequest.aspx";
        //_results.DataSource = _SessionData.AdversaryRequest;
        //_results.DataBind();
        _requestSummary.RowCssClass = "gridViewRow";
        _requestSummary.ShowNullValues = true;
        _requestSummary.ScreenID = "EditAdversaryRequest.aspx";
        _requestSummary.DataSource = _SessionData.AdversaryRequest;
        _requestSummary.DataBind();
    }

        
}
